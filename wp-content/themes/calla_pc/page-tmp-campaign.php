<?php
/*
Template Name: キャンペーン用テンプレート
*/
?>
<?php get_template_part('header'); ?>
<?php breadcrumb(); ?>
    <div class="wrapper">
      <?php
        if (have_posts()) : while (have_posts()) : the_post();
          the_content();
        endwhile; endif;
       ?>
     </div>
<?php get_template_part('footer'); ?>
