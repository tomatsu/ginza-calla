<!-- aboutページ -->
<?php if(is_page_group('about')){ ?>
  <div class="sidebar-list">
    <p class="sidebar-list-head">銀座カラーについて</p>
    <ul>
      <li><a href="<?php bloginfo('url') ?>/about/research" class="trans">銀座カラーが人気の理由</a></li>
      <li><a href="<?php bloginfo('url') ?>/about/anshin" class="trans">7つの安心宣言</a></li>
      <li><a href="<?php bloginfo('url') ?>/about/premium-service" class="trans">プレミアムサービス</a></li>
      <li><a href="<?php bloginfo('url') ?>/about/reservation" class="trans">予約がとりやすい理由</a></li>
      <li><a href="<?php bloginfo('url') ?>/about/jishin03" class="trans">リーズナブルな価格</a></li>

      <li class="title">銀座カラーの脱毛</li>
      <li><a href="<?php bloginfo('url') ?>/about/counsel" class="text-indent trans">無料カウンセリングまでの流れ</a></li>
      <li><a href="<?php bloginfo('url') ?>/about/step" class="text-indent trans">ご来店から施術までの流れ</a></li>
      <li><a href="<?php bloginfo('url') ?>/about/jishin01" class="text-indent trans">保湿ケア「美肌潤美」</a></li>
      <li><a href="<?php bloginfo('url') ?>/about/jishin02" class="text-indent trans">高結果サポート</a></li>

    </ul>
  </div><!-- /.sidebar-list -->
<?php }; ?>
<!-- aboutページここまで -->


<!-- careページ -->
<?php if(is_page_group('care')){ ?>
  <div class="sidebar-list">
    <p class="sidebar-list-head sidebar-line">脱毛について</p>
    <p class="side-list-subtitle">脱毛のきっかけ</p>
    <ul>
      <li><a href="<?php bloginfo('url') ?>/care/01-care" class="trans">脱毛してビキニラインを<br />気にせず海へ！</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/02-care" class="trans">ワキ毛を脱毛したら、<br />ノースリーブも怖くない！</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/03-care" class="trans">時短効果＋いつもツルツル<br />で気分爽快！</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/04-care" class="trans">自己投資！自分磨き<br />でツルツルのお肌に☆</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/05-care" class="trans">ブライダル脱毛！<br />結婚式はキラキラ笑顔</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/06-care" class="trans">脱毛で解消？！<br />悩める毛穴の黒ずみ対処法</a></li>
    </ul>
    <p class="side-list-subtitle">脱毛の基礎</p>
    <ul>
      <li><a href="<?php bloginfo('url') ?>/care/07-care" class="trans">毛周期・脱毛の回数に<br />ついて知りたい</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/08-care" class="trans">生理・妊娠。脱毛と<br />ホルモンバランスの関係</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/09-care" class="trans">「光脱毛」と「自己処理」<br />違いは何？</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/10-care" class="trans">専門サロンとクリニックの<br />違いって？</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/11-care" class="trans">見学必至!<br />安心して通えるサロン選び</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/12-care" class="trans">施術までの事前準備は<br />何をすればいいの？</a></li>
    </ul>
    <p class="side-list-subtitle">自己処理について</p>
    <ul>
      <li><a href="<?php bloginfo('url') ?>/care/13-care" class="trans">脱毛中の自己処理<br />脱毛後の自宅ケアについて</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/14-care" class="trans">自己処理の<br />メリット・デメリット</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/15-care" class="trans">定番！ワキ脱毛と<br />自己処理による肌トラブル</a></li>
    </ul>
    <p class="side-list-subtitle">脱毛の疑問</p>
    <ul>
      <li><a href="<?php bloginfo('url') ?>/care/16-care" class="trans">毛穴が閉じて美肌に！<br />顔脱毛のメリット</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/17-care" class="trans">えり足脱毛の<br />効果と注意点</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/18-care" class="trans">本音！VIOラインの<br />処理について</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/19-care" class="trans">ニオイの原因！<br />脱毛とワキ汗の秘密</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/20-care" class="trans">えり足、指毛…<br />細かい気になる毛は大丈夫？</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/21-care" class="trans">白髪や脱色した毛、<br />うぶ毛も脱毛できる？</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/22-care" class="trans">敏感肌のワタシでも<br />光脱毛できますか？</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/23-care" class="trans">早いほどお得！？<br /> 脱毛の年齢制限</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/24-care" class="trans">ニキビも治ってツルツル！？<br />背中脱毛の効果</a></li>
      <li><a href="<?php bloginfo('url') ?>/care/25-care" class="trans">剛毛・毛深い悩みについて<br />知りたい</a></li>
    </ul>
  </div><!-- /.sidebar-list -->
<?php }; ?>
<!-- careページここまで -->


<!-- planページ -->
<?php
  $get_plan_id = get_page_by_path("plan")->ID;
  $get_part_id = get_page_by_path("plan/part")->ID;
  $get_course_id = get_page_by_path("plan/course")->ID;
?>
<?php if((is_page('plan') || is_page_group('part')) || (in_array($get_part_id, get_post_ancestors($post->ID)))){ ?>
  <?php if(is_page_group('course')){}else{ ?>
    <div class="sidebar-list">
      <p class="sidebar-list-head">コース・料金</p>
      <ul>
<!--
        <li><a href="<?php bloginfo('url') ?>/plan/course/" class="trans">コース</a></li>
-->
        <li><a href="<?php bloginfo('url') ?>/plan/part/" class="trans">施術箇所</a></li>
      </ul>
    </div><!-- /.sidebar-list -->
  <?php }; ?>
<?php }; ?>
<?php if(is_page_group('course') || in_array($get_course_id, get_post_ancestors($post->ID))){ ?>
<div class="sidebar-list">
  <p class="sidebar-list-head">コース一覧</p>
  <ul>
<!--
    <li class="title">フリーチョイス脱毛コース</li>
    <li><a href="<?php bloginfo('url') ?>/plan/course/freechoice/#delicate" class="text-indent trans">デリケートチョイス</a></li>
    <li><a href="<?php bloginfo('url') ?>/plan/course/freechoice/#standard" class="text-indent trans">スタンダードチョイス</a></li>
    <li class="title">全身脱毛コース</li>
-->
    <li><a href="<?php bloginfo('url') ?>/plan/course/zenshin/" class="text-indent trans">全身脱毛</a></li>
<!--
    <li><a href="<?php bloginfo('url') ?>/plan/course/perfect/" class="text-indent trans">全身パーフェクト脱毛</a></li>
    <li><a href="<?php bloginfo('url') ?>/plan/course/half/" class="text-indent trans">全身ハーフ脱毛</a></li>
    <li class="title">定番セット</li>
    <li><a href="<?php bloginfo('url') ?>/plan/course/face/" class="text-indent trans">顔セット</a></li>
    <li><a href="<?php bloginfo('url') ?>/plan/course/ude/" class="text-indent trans">腕全セット</a></li>
    <li><a href="<?php bloginfo('url') ?>/plan/course/ashi/" class="text-indent trans">足全セット</a></li>
    <li><a href="<?php bloginfo('url') ?>/plan/course/vio/" class="text-indent trans">VIOセット</a></li>
-->
  </ul>
</div><!-- /.sidebar-list -->
<?php }; ?>
<!-- planページここまで -->


<!-- faqページ -->
<?php if(is_page_group('faq')){ ?>
  <div class="sidebar-list">
    <p class="sidebar-list-head">脱毛に関するQ&A</p>
    <ul>
      <li><a href="<?php bloginfo('url') ?>/faq/01-index" class="height trans">無料カウンセリング<br />予約について</a></li>
      <li><a href="<?php bloginfo('url') ?>/faq/02-index" class="height trans">サービスについて</a></li>
      <li><a href="<?php bloginfo('url') ?>/faq/03-index" class="height trans">脱毛について</a></li>
      <li><a href="<?php bloginfo('url') ?>/faq/04-index" class="height trans">会員様向け</a></li>
    </ul>
  </div><!-- /.sidebar-list -->
<?php }; ?>
<!-- faqページここまで -->


<!-- companyページ -->
<?php if(is_page_group('company')){ ?>
  <div class="sidebar-list">
    <p class="sidebar-list-head">会社概要</p>
    <ul>
      <li><a href="<?php bloginfo('url') ?>/company/" class="text-indent trans">会社概要</a></li>
      <li><a href="<?php bloginfo('url') ?>/company/history" class="text-indent trans">銀座カラーの歩み</a></li>
      <!-- <li><a href="<?php bloginfo('url') ?>/company/sitemap" class="text-indent trans">サイトマップ</a></li>
      <li><a href="<?php bloginfo('url') ?>/company/policy" class="text-indent trans">プライバシーポリシー</a></li> -->
    </ul>
  </div><!-- /.sidebar-list -->
<?php }; ?>
<!-- companyページここまで -->

<!-- gakuwariページ
<?php if(is_page_group('gakuwari') || is_page_group('report')){ ?>
  <div class="sidebar-list">
    <p class="sidebar-list-head">脱毛学割</p>
    <ul>
      <li><a href="<?php bloginfo('url') ?>/gakuwari/" class="trans">脱毛学割とは？</a></li>
      <li><a href="<?php bloginfo('url') ?>/gakuwari/report/" class="trans">ミスキャン☆サロンレポート</a></li>
    </ul>
  </div>
<?php }; ?>
-->

<!-- productページ -->
<?php if(is_page_group('product')){ ?>
  <div class="sidebar-list">
	  <p class="sidebar-list-head">オリジナルケア商品の<br />ご案内</p>
    <ul>
      <li><a href="<?php bloginfo('url') ?>/product/original/" class="trans">オリジナルケアシリーズ</a></li>
      <li><a href="<?php bloginfo('url') ?>/product/koisuru/" class="trans">恋する肌シリーズ</a></li>
      <li><a href="<?php bloginfo('url') ?>/product/rich/" class="trans">リッチシリーズ</a></li>
    </ul>
  </div><!-- /.sidebar-list -->
<?php }; ?>
<!-- productページここまで -->

<!-- voiceページ -->
<?php if(is_page_group('voice') && !$post->post_parent ){ ?>
  <div class="sidebar-list">
    <p class="sidebar-list-head">お客様の声</p>
    <span class="js-tab">
      <span class="js-tab-nav">
      <ul>
        <li><a href="#all" class="trans">お客様の声全て</a></li>
        <li><a href="#salon" class="trans">サロンについてのお客様の声</a></li>
        <li><a href="#course" class="trans">コースについてのお客様の声</a></li>
        <li><a href="#part" class="trans">箇所についてのお客様の声</a></li>
      </ul>
      </span>
    </span>
  </div><!-- /.sidebar-list -->
<?php }; ?>
<!-- voiceページここまで -->
