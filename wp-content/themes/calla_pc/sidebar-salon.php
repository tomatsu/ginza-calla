<?php
if(is_page('voice')){
  $parent_id = $post->post_parent;
  $arrays = get_field('near_shop',$parent_id);
}else{
  $arrays = get_field('near_shop');
};
if($arrays){
?>
  <div class="sidebar-list">
    <p class="sidebar-list-head">周辺の店舗</p>
    <ul>
      <?php
          foreach ($arrays as $array){
            $pageinfo = get_page_by_title($array);
      ?>
      <li>
         <a href="<?php bloginfo('url') ?>/salon/<?php echo $pageinfo->post_name; ?>" class="trans">
            <?php echo $array; ?>
         </a>
      </li>
      <?php }; ?>
    </ul>
  </div><!-- /.sidebar-list -->
<?php }; ?>
