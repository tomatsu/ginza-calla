<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/js/lib/common.js?191227"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/js/common.js?191227"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/js/migrate-plugin.js?191227"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/js/migrate-common.js?191227"></script>

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/resource/migrate/js/slick.css"/>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/resource/migrate/js/slick-theme.css"/>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/migrate/js/slick.js?191227"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/migrate/js/jquery.inview.js?191227"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/migrate/js/common.js?191227"></script>


<!-- <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/js/lib/jquery.appendMore.min.js?123"></script> -->
<!-- end Common Script -->
<?php if(is_home()){ ?>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/js/migrate-index.js?191227"></script>
<?php }; ?>
<?php if(is_page('about')){ ?>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/js/migrate-about.js?191227"></script>
<?php }; ?>
<?php if(is_page('salon')){ ?>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/js/migrate-salon.js?191227"></script>
<?php }; ?>
<?php if(is_page('adgallery')){ ?>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/js/adgallery.js?191227"></script>
<?php }; ?>

<?php
  $get_plan_id = get_page_by_path("plan")->ID;
  if(in_array($get_plan_id, get_post_ancestors($post->ID))){
 ?>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/js/plan-part.js?191227"></script>
<?php }; ?>

<?php if(is_page_group('salon') || is_page_group('area')){  ?>
  <script type="text/javascript">
    var geturl = "<?php echo get_page_uri($post->ID); ?>";
    var get_url_array = geturl.split("/");
    var path = get_url_array[get_url_array.length -1];

    var jsonurl = "<?php bloginfo('template_url'); ?>/resource/js/area-json/" + path + ".json?191227";
  </script>
  <!--<script type="text/javascript"src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCk7O_Ioiv5AFDtWJTODg1hiU9Kng76tMk"></script>-->
  <script type="text/javascript"src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKHfmp22QBX2BukRJsQkIAENWqPeAOIfI"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/js/map.js?191227"></script>
  <script>
  function mapInitialize() {
    // MAPデータ
    var elem = document.getElementById("map-canvas");
    var lat = <?php the_field('lat'); ?>;
    var lng = <?php the_field('lng'); ?>;
    var zoom = 16;
    // 店舗情報
    var salon = "<?php the_field('g_salon_name'); ?>";
    var businessHour = "<?php the_field('g_open_time'); ?>";
    var closed = '<?php the_field('g_closed'); ?>';
    var tel = "<?php the_field('g_tel'); ?>";
    // MAPの生成
    lat = parseFloat( lat );
    lng = parseFloat( lng );
    if ( isNaN( lat ) || isNaN( lng ) ) return false;
    var center = new google.maps.LatLng( lat, lng );
    var options = {
      center: center,
      zoom: zoom,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(elem, options);
    // マーカーの生成
    var marker = new google.maps.Marker({
      position: center,
      map: map,
      title: salon
    });
    // 情報ウィンドウのテンプレート
    var formatedTel = formatTel( tel );
    var content = '';
    content += '<div id="content">';
    content += '<p>' + salon + '</p>';
    content += '<p>【営業時間】' + businessHour + '</p>';
    content += '<p>【定休日】' + closed + '</p>';
    content += '<p>【電話番号】<a href="tel:' + formatedTel + '" style="color: #393939;">' + tel + '</a></p>';
    content += '</div>';
    // 情報ウィンドウの生成
    var infowindow = new google.maps.InfoWindow({
      content: content
    });
    setTimeout(function() {
      infowindow.open(map, marker);
    }, 1000);
    // マーカーのクリック処理
    google.maps.event.addListener(marker, 'click', function(event) {
      if ( infowindow.getMap() ) {
        infowindow.close();
      } else {
        infowindow.open(marker.getMap(), marker);
      }
    });
    // 電話番号フォーマット調整処理
    function formatTel( string ) {
      var num = string.replace(/[‐－―ー-]/gi, '');       // ハイフンを削除
      num = num.replace(/[０-９]/g, function(s) {     // 全角を半角に変換
        return String.fromCharCode( s.charCodeAt(0)-0xFEE0);
      });
      return num;
    }
  }
  // イベントハンドラの設定
  window.addEventListener('load', mapInitialize);
  </script>
<?php }; ?>

<?php if(is_page_group('voice')){  ?>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/js/plan-part.js?191227"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/js/lib/jquery.appendMore.min.js?191227"></script>
<?php }; ?>

<?php if(is_page_group('selfcheck')){  ?>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/js/lib/jquery.ad-gallery.min.js"></script>
  <script type="text/javascript">
  $(function() {
      var galleries = $('.ad-gallery').adGallery({
          loader_image : '<?php bloginfo('template_url'); ?>/resource/img/selfcheck/loader.gif',
          start_at_index : 0,
          description_wrapper : false,
          thumb_opacity : 1.0,
          animate_first_image : false,
          animation_speed : 400,
          width : 628,
          height : 500,
          display_next_and_prev : false,
          display_back_and_forward : true,
          slideshow : {
              enable : true,
              autostart : false,
              speed : 500,
              start_label : 'Start',
              stop_label : 'Stop',
              countdown_prefix : '(',
              countdown_sufix : ')'
          },
          effect : 'slide-hori',
          enable_keyboard_move : false,
          cycle : true,
          callbacks: {
              init: false,
              afterImageVisible: false,
              beforeImageVisible: function(){
                  var galleries_list_num = $('.ad-gallery .ad-thumbs .ad-thumb-list li').length;
                  if ($('.ad-gallery .ad-thumbs li:eq(0) a').hasClass('ad-active')) {
                      $('.ad-gallery .ad-back').css({ display : 'none' });
                      $('.ad-gallery .ad-forward').css({ display : 'block' });
                  }
                  else if ($('.ad-gallery .ad-thumbs li:eq(' + (galleries_list_num - 1) + ') a').hasClass('ad-active')) {
                      $('.ad-gallery .ad-back').css({ display : 'block' });
                      $('.ad-gallery .ad-forward').css({ display : 'none' });
                  }
                  else {
                      $('.ad-gallery .ad-back').css({ display : 'block' });
                      $('.ad-gallery .ad-forward').css({ display : 'block' });
                  }
                  if (galleries_list_num <= 5) {
                      $('.ad-gallery .ad-back, .ad-gallery .ad-forward').css({ display : 'none' });
                  }
              }
          }
      });
      $('#switch-effect').change(
          function() {
              galleries[0].settings.effect = $(this).val();
              return false;
          }
      );
      $('#toggle-slideshow').click(
          function() {
              galleries[0].slideshow.toggle();
              return false;
          }
      );
      $('#toggle-description').click(
          function() {
              if(!galleries[0].settings.description_wrapper) {
                  galleries[0].settings.description_wrapper = $('#descriptions');
              } else {
                  galleries[0].settings.description_wrapper = false;
              }
              return false;
          }
      );
  });
  </script>
<?php }; ?>
<!-- end Partial Script -->
