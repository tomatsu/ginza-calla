<?php get_template_part('header'); ?>
<?php breadcrumb(); ?>
  <div class="wrapper container">
    <div class="main">
      <div class="block-white">
        <?php if(is_page('voice')){ ?>
          <h1 class="page-head icon-voice-head"><span>銀座カラー</span><?php the_title(); ?></h1>
        <?php }else if(is_parent_slug() == 'course'){ ?>
          <h1 class="page-head <?php the_field('h1_logo') ?>"><?php the_title(); ?><!--<span></span>--></h1>
        <?php }else{ ?>
          <?php if(get_field('h1_logo') !='' ){ ?>
            <h1 class="page-head <?php the_field('h1_logo') ?>"><!--<span></span>--><?php the_title(); ?></h1>
          <?php }else{ ?>
            <h1 class="page-head"><!--<span></span>--><?php the_title(); ?></h1>
          <?php }; ?>
        <?php }; ?>
      <?php if((is_page_group('product') && !is_page('product')) || (is_page_group('part') && !is_page('part'))){}else{  ?>
        <div class="page-body">
       <?php }; ?>

        <?php
          if (have_posts()) : while (have_posts()) : the_post();
            the_content();
          endwhile; endif;
         ?>

      <?php if((is_page_group('product') && !is_page('product')) || (is_page_group('part') && !is_page('part'))){}else{  ?>
        </div><!-- /.page-body -->
      <?php }; ?>

        <?php if(is_page_group('care') && !is_page('care')){  ?>
          <?php
            $str = split('[/-]', get_the_permalink());
            $count = count($str) - ARRAY_BACK_NUM;
            $page_num = $str[$count];
          ?>
          <ul class="mod-btns">
            <?php if($page_num == 1){}else{ ?>
              <?php if(2 <= $page_num && $page_num <= 10){ ?>
                <li><a href="<?php bloginfo('url'); ?>/care/0<?php echo $page_num - 1 ?>-care" class="btn-stripe-left trans">前の記事へ</a></li>
              <?php }else{ ?>
                <li><a href="<?php bloginfo('url'); ?>/care/<?php echo $page_num - 1 ?>-care" class="btn-stripe-left trans">前の記事へ</a></li>
              <?php }; ?>
            <?php }; ?>
            <?php if($page_num == CARE_PAGE_NUM){}else{ ?>
              <?php if(1 <= $page_num && $page_num <= 8){ ?>
                <li><a href="<?php bloginfo('url'); ?>/care/0<?php echo $page_num + 1 ?>-care" class="btn-stripe trans">次の記事へ</a></li>
              <?php }else{ ?>
                <li><a href="<?php bloginfo('url'); ?>/care/<?php echo $page_num + 1 ?>-care" class="btn-stripe trans">次の記事へ</a></li>
              <?php }; ?>
            <?php }; ?>
          </ul>
        <?php }; ?>

      </div><!-- /.block-white -->


      <?php if(is_page('gakuwari')){  ?>

        <!--<div class="block-bnr-under20">
          <a href="<?php bloginfo('url'); ?>/campaign/under-20">
            <dl class="trans">
              <dt>未成年<br>限定プラン!!</dt>
              <dd>Under-20プラン<br><em>都度払い脱毛</em></dd>
            </dl>
          </a>
        </div>--><!-- /.block-bnr-under20 -->

       <?php }; ?>

       <?php
         $get_part_id = get_page_by_path("plan/part")->ID;
         if (in_array($get_part_id, get_post_ancestors($post->ID))){
           if(is_page('ranking') || is_page('voice')){}else{
        ?>
        <div class="mod-btn-counsel">
          <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0016&amp;_ga=1.202452817.1907874184.1471244650" class="btn-stripe btn-shadow btn-counsel btn-counsel-large trans" target="_blank"><span>無料カウンセリング</span>ご予約はこちら</a>
        <!-- /.mod-btn-counsel --></div>
        <div class="block-white height-space">

      <div class="page-body">
          <div class="block-region js-tab-region">
            <div class="js-tab-nav">
              <ul class="block-tab-region">
              	<li class="left-tab on"><a href="#front">正面</a></li>
              	<li class="right-tab"><a href="#back">背面</a></li>
              </ul>
            <!-- /.js-tab-nav --></div>
            <div class="block-region-body js-tab-body" id="front">
              <ul class="block-region-btns">
              	<li><a href="/plan/part/face/" class="height trans btn-face">顔</a></li>
              	<li><a href="/plan/part/under-nose/" class="height trans btn-nose">鼻下</a></li>
              	<li><a href="/plan/part/ryowaki/" class="height trans btn-ryowaki">両ワキ</a></li>
              	<li><a href="/plan/part/areola/" class="height trans btn-areola">乳輪周り</a></li>
              	<li><a href="/plan/part/belly/" class="height trans btn-belly">お腹</a></li>
              	<li><a href="/plan/part/navel/" class="height trans btn-navel ">へそ周り</a></li>
              	<li><a href="/plan/part/shell-finger/" class="height trans btn-shell-finger">両手の甲・指</a></li>
              	<li><a href="/plan/part/on-vline/" class="height trans btn-on-vline">Vライン( 上部 )</a></li>
              	<li><a href="/plan/part/vline-i/" class="trans btn-vline-i">Vライン<br>( iライン )</a></li>
              	<li><a href="/plan/part/shell-feet/" class="height trans btn-shell-feet">両足の甲・指</a></li>
              </ul>
              <div class="block-region-image">
                <img src="<?php bloginfo('template_url'); ?>/resource/img/plan/part/index/img-normal.png" alt="" width="362" height="504">
              </div>
              <ul class="block-region-btns">
              	<li><a href="/plan/part/sideburns/" class="height trans btn-sideburns">もみあげ</a></li>
              	<li><a href="/plan/part/under-mouth/" class="height trans btn-under-mouth">口下（あご）</a></li>
              	<li><a href="/plan/part/breast/" class="height trans btn-breast">胸</a></li>
              	<li><a href="/plan/part/on-elbow/" class="height trans btn-on-elbow">両ヒジ上</a></li>
              	<li><a href="/plan/part/under-elbow/" class="height trans btn-under-elbow">両ヒジ下</a></li>
              	<li><a href="/plan/part/navel-under/" class="height trans btn-navel-under">へそ下</a></li>
              	<li><a href="/plan/part/vline-s/" class="height trans btn-vline-s">Vライン( サイド )</a></li>
              	<li><a href="/plan/part/on-knees/" class="height trans btn-on-knees">両ひざ上</a></li>
              	<li><a href="/plan/part/knees/" class="height trans btn-knees">両ひざ</a></li>
              	<li><a href="/plan/part/under-knees/" class="height trans btn-under-knees">両ひざ下</a></li>
              </ul>
            <!-- /#front --></div>

            <div class="js-tab-body" id="back" style="display: none;">
              <ul class="block-region-btns">
              	<li><a href="/plan/part/collar-leg/" class="height trans btn-back-collar-leg">えり足・うなじ</a></li>
              	<li><a href="/plan/part/on-elbow/" class="height trans btn-back-on-elbow">両ヒジ上</a></li>
              	<li><a href="/plan/part/under-elbow/" class="height trans btn-back-under-elbow">両ヒジ下</a></li>
              	<li><a href="/plan/part/hips/" class="height trans btn-back-hips">ヒップ（お尻）</a></li>
              	<li><a href="/plan/part/hips-back/" class="height trans btn-back-hips-back ">ヒップ奥(Oライン)</a></li>
              </ul>
              <div class="block-region-image">
                <img src="<?php bloginfo('template_url'); ?>/resource/img/plan/part/index/img-back-normal.png" alt="" width="362" height="504">
              </div>
              <ul class="block-region-btns">
              	<li><a href="/plan/part/on-back/" class="height trans btn-back-on-back">背中上</a></li>
              	<li><a href="/plan/part/under-back/" class="height trans btn-back-under-back">背中下</a></li>
              	<li><a href="/plan/part/waist/" class="height trans btn-back-waist">腰</a></li>
              	<li><a href="/plan/part/on-knees/" class="height trans btn-back-on-knees">両ひざ上</a></li>
              	<li><a href="/plan/part/under-knees/" class="height trans btn-back-under-knees">両ひざ下</a></li>
              </ul>
            <!-- /#back --></div>
          <!-- /.js-tab-region --></div>
      <!-- /.page-body --></div>
    <!-- /.block-white --></div>
    <?php
        };
      };
     ?>
      <?php
        if( !get_field('new_cust_banner') ){
          if(is_page('voice')){}else{
            get_template_part('tmp_course_set_list');
          };
        };
       ?>
       <?php if(is_page('course')){ ?>
         <div class="block-bnr-under20">
           <a href="/campaign/under-20.html">
         <dl class="trans">
         	<dt>未成年<br>限定プラン!!</dt>
         	<dd>Under-20プラン<br><em>都度払い脱毛</em></dd>
         </dl>
       </a>
       <div class="mod-btn-counsel">
         <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0016" class="btn-stripe btn-shadow btn-counsel btn-counsel-large trans"><span>無料カウンセリング</span>ご予約はこちら</a>
       <!-- /.mod-btn-counsel --></div>
     <!-- /.block-bnr-under20 --></div>
       <?php }else{ ?>
         <?php if( !get_field('free_banner') ){ ?>
         <div class="mod-btn-counsel">
           <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0016" class="btn-stripe btn-shadow btn-counsel btn-counsel-large trans" target="_blank"><span>無料カウンセリング</span>ご予約はこちら</a>
         </div> <!-- /.mod-btn-counsel -->
         <?php }; ?>
       <?php }; ?>
    </div><!-- /.main -->
    <?php get_template_part('sidebar'); ?>
  </div><!-- /.wrapper -->
<?php get_template_part('footer'); ?>
