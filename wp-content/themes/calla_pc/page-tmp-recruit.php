<?php
/*
Template Name: リクルートページ用テンプレート
*/
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="content-language" content="ja" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />
<?php if(get_field('_aioseop_title')){ ?>
	<?php if(is_page('guideline2')){ ?>
		<title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」 採用ページ</title>
	<?php }else{ ?>
		<title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」 採用ページ</title>
	<?php }; ?>
<?php }else{ ?>
	<title><?php the_title(); ?>｜脱毛サロンに就職！脱毛の銀座カラー 採用ページ</title>
<?php }; ?>
<meta property="og:title" content="脱毛サロンに就職！｜脱毛の銀座カラー 採用ページ">
<meta property="og:type" content="website">
<meta property="og:description" content="脱毛専門店の銀座カラーの採用募集要項。「きらきら輝けるあなたへ」誰もがきらきら輝ける職場であなたも一緒に働いてみませんか？">
<meta property="og:url" content="http://ginza-calla.jp/recruit/">
<meta property="og:image" content="http://www.ginzacalla-gakuwari.jp/common/images/og_image.jpg">
<meta property="og:site_name" content="脱毛の銀座カラー 採用ページ">
<link rel="alternate" media="handheld" href="http://ginza-calla.jp/recruit/sp/">
<link rel="canonical" href="http://ginza-calla.jp/recruit/">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/recruit/css/include.css?20160815">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/recruit/css/flexslider.css?20160815">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/recruit/js/colorbox/colorbox.css?20160815">



<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/recruit/layout.css?20aaaaxaaaa">
<?php if(is_page('conditions')){ ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/recruit/conditions.css" type="text/css" media="all">
<?php }; ?>
<?php if(is_page('salonlist')){ ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/recruit/salonlist.css" type="text/css" media="all">
<?php }; ?>
<?php if(is_page('conditions')||is_page('salonlist')){}else{ ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/recruit/skrollr.css?20160815">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/resource/recruit/guidelines/css/base.css" media="screen,print" />
<?php }; ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/resource/recruit/guidelines/css/responsive.css" media="all" />
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/recruit/guidelines/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/recruit/guideline2/js/common.js"></script>
<!-- js -->
<?php if(is_page('recruit')){ ?>
<script>
var _uastr = navigator.userAgent.toLowerCase();

if( _uastr.indexOf("iphone") >= 0 || _uastr.indexOf("android ") >= 0 && _uastr.indexOf("mobile") >= 0){
	location.href='/recruit/sp/';
} else if( _uastr.indexOf("ipad") >= 0 || _uastr.indexOf("android ") >= 0){
} else {

}
</script>
<script type="text/javascript" charset="utf-8" src="<?php bloginfo('template_url'); ?>/resource/recruit/js/jquery.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php bloginfo('template_url'); ?>/resource/recruit/js/colorbox/jquery.colorbox.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php bloginfo('template_url'); ?>/resource/recruit/js/jquery.flexslider.js"></script>
<script type="text/javascript" charset="utf-8">
	$(window).load(function() {
		$("#cboxSalon").colorbox({rel:'group1'});
		$("#cboxJob").colorbox({iframe:true, width:"60%", height:"92%"});

		$('.topSlide').flexslider({
			animation: "fade",
			slideshow : true,
			slideshowSpeed : 2300,
			animationDuration: 1000,
			directionNav: false,
			controlNav: false,
			prevText: "",
			nextText: ""
		});
		$('.topSlide2').flexslider({
			animation: "fade",
			slideshow : true,
			slideshowSpeed : 50000,
			animationDuration: 7000,
			directionNav: false,
			controlNav: false,
			prevText: "",
			nextText: ""
		});
		$('.flexslider').flexslider({
			animation: "slide",
			slideshow : false,
			slideshowSpeed : 5000,
			animationDuration: 500,
			directionNav: true,
			controlNav: true,
			prevText: "",
			nextText: ""
		});

		$('#s5_nav1').css("cursor","pointer");
		$('#s5_nav2').css("cursor","pointer");
		$('#s5_nav3').css("cursor","pointer");
		$('#s5_nav4').css("cursor","pointer");
		$('#s5_nav5').css("cursor","pointer");
		$('#s5_nav6').css("cursor","pointer");

		$(document).on('click', '#s5_nav1', function(){
			$('#s5_nav1').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n_on.png");
			$('#s5_nav2').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2.png");
			$('#s5_nav3').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t.png");
			$('#s5_nav4').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4.png");
			$('#s5_nav5').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5.png");
			$('#s5_nav6').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav6_n.png");
			$('.flex-control-nav li:eq(0) a').click();
			return false;
		});
		$(document).on('click', '#s5_nav2', function(){
			$('.flex-control-nav li:eq(1) a').click();
			$('#s5_nav1').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n.png");
			$('#s5_nav2').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2_on.png?190402");
			$('#s5_nav3').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t.png");
			$('#s5_nav4').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4.png");
			$('#s5_nav5').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5.png");
			$('#s5_nav6').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav6_n.png");
			return false;
		});
		$(document).on('click', '#s5_nav3', function(){
			$('.flex-control-nav li:eq(2) a').click();
			$('#s5_nav1').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n.png");
			$('#s5_nav2').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2.png");
			$('#s5_nav3').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t_on.png");
			$('#s5_nav4').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4.png");
			$('#s5_nav5').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5.png");
			$('#s5_nav6').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav6_n.png");
			return false;
		});
		$(document).on('click', '#s5_nav4', function(){
			$('.flex-control-nav li:eq(3) a').click();
			$('#s5_nav1').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n.png");
			$('#s5_nav2').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2.png");
			$('#s5_nav3').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t.png");
			$('#s5_nav4').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4_on.png");
			$('#s5_nav5').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5.png");
			$('#s5_nav6').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav6_n.png");
			return false;
		});
		$(document).on('click', '#s5_nav5', function(){
			$('.flex-control-nav li:eq(4) a').click();
			$('#s5_nav1').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n.png");
			$('#s5_nav2').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2.png");
			$('#s5_nav3').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t.png");
			$('#s5_nav4').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4.png");
			$('#s5_nav5').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5_on.png");
			$('#s5_nav6').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav6_n.png");
			return false;
		});
		$(document).on('click', '#s5_nav6', function(){
			$('.flex-control-nav li:eq(5) a').click();
			$('#s5_nav1').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n.png");
			$('#s5_nav2').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2.png");
			$('#s5_nav3').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t.png");
			$('#s5_nav4').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4.png");
			$('#s5_nav5').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5.png");
			$('#s5_nav6').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav6_n_on.png");
			return false;
		});
		$(document).on('click', '.flex-control-nav li a', function(){
			var index = $(this).text();
			index = parseInt(index);
			if ( index == 1){
				$('#s5_nav1').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n_on.png");
				$('#s5_nav2').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2.png");
				$('#s5_nav3').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t.png");
				$('#s5_nav4').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4.png");
				$('#s5_nav5').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5.png");
				$('#s5_nav6').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav6_n.png");
			}
		});
		$(document).on('click', '.flex-control-nav li a', function(){
			var index = $(this).text();
			index = parseInt(index);
			if ( index == 2){
			$('#s5_nav1').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n.png");
			$('#s5_nav2').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2_on.png?190402");
			$('#s5_nav3').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t.png");
			$('#s5_nav4').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4.png");
			$('#s5_nav5').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5.png");
			$('#s5_nav6').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav6_n.png");
			}
		});
		$(document).on('click', '.flex-control-nav li a', function(){
			var index = $(this).text();
			index = parseInt(index);
			if ( index == 3){
			$('#s5_nav1').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n.png");
			$('#s5_nav2').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2.png");
			$('#s5_nav3').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t_on.png");
			$('#s5_nav4').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4.png");
			$('#s5_nav5').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5.png");
			$('#s5_nav6').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav6_n.png");
			}
		});
		$(document).on('click', '.flex-control-nav li a', function(){
			var index = $(this).text();
			index = parseInt(index);
			if ( index == 4){
			$('#s5_nav1').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n.png");
			$('#s5_nav2').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2.png");
			$('#s5_nav3').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t.png");
			$('#s5_nav4').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4_on.png");
			$('#s5_nav5').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5.png");
			$('#s5_nav6').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav6_n.png");
			}
		});
		$(document).on('click', '.flex-control-nav li a', function(){
			var index = $(this).text();
			index = parseInt(index);
			if ( index == 5){
			$('#s5_nav1').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n.png");
			$('#s5_nav2').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2.png");
			$('#s5_nav3').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t.png");
			$('#s5_nav4').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4.png");
			$('#s5_nav5').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5_on.png");
			$('#s5_nav6').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav6_n.png");
			}
		});
		$(document).on('click', '.flex-control-nav li a', function(){
			var index = $(this).text();
			index = parseInt(index);
			if ( index == 6){
			$('#s5_nav1').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n.png");
			$('#s5_nav2').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2.png");
			$('#s5_nav3').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t.png");
			$('#s5_nav4').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4.png");
			$('#s5_nav5').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5.png");
			$('#s5_nav6').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav6_n_on.png");
			}
		});
		$("#base a img").hover(
		  function () {
			$(this).css("opacity","0.5");
		  },
		  function () {
			$(this).css("opacity","1");
		  }
		);

		$(document).on('click', '.flex-next, .flex-prev', function(){
			var activeNum = 0;

			activeNum = $('.flex-active').text();
			activeNum = parseInt(activeNum);
			//alert(activeNum);
			$('#s5_nav1').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n.png");
			$('#s5_nav2').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2.png");
			$('#s5_nav3').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t.png");
			$('#s5_nav4').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4.png");
			$('#s5_nav5').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5.png");
			$('#s5_nav6').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav6_n.png");

			switch (activeNum){
			  case 1:
				$('#s5_nav1').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n_on.png");
				break;
			  case 2:
				$('#s5_nav2').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2_on.png?190402");
				break;
			  case 3:
				$('#s5_nav3').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t_on.png");
				break;
			  case 4:
				$('#s5_nav4').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4_on.png");
				break;
			  case 5:
				$('#s5_nav5').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5_on.png");
				break;
			  case 6:
				$('#s5_nav6').attr("src","<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav6_n_on.png");
				break;
			}

		});

$("#navGlobal #nav1").click(function () {
	$('html,body').animate({ scrollTop: 0 }, 'fast');
	return false;
});
$("#navGlobal #nav2").click(function () {
	$('html,body').animate({ scrollTop: 2050 }, 'fast');
	return false;
});
$("#navGlobal #nav3").click(function () {
	$('html,body').animate({ scrollTop: 2950 }, 'fast');
	return false;
});
$("#navGlobal #nav4").click(function () {
	$('html,body').animate({ scrollTop: 4850 }, 'fast');
	return false;
});
$("#navGlobal #nav5").click(function () {
	$('html,body').animate({ scrollTop: 7450 }, 'fast');
	return false;
});
$("#navGlobal #nav6").click(function () {
	$('html,body').animate({ scrollTop: 880 }, 'fast');
	return false;
});
	});
</script>
<?php }; ?>
<!-- js -->
<!--[if lt IE 9]>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<?php wp_head(); ?>
</head>
<?php if(is_page('conditions')||is_page('salonlist')){ ?>
	<body style="padding: 30px" cz-shortcut-listen="true">
<?php }else{ ?>
  <body>
<?php }; ?>
  <?php
    if(is_page('recruit')){
      get_template_part('tmp-recruit-index');
    }else{
      if (have_posts()) : while (have_posts()) : the_post();
        the_content();
      endwhile; endif;
    };
   ?>

	<?php if(is_page('recruit')){ ?>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/resource/recruit/skrollr.min.js"></script>
  <!--[if lt IE 9]>
  <script type="text/javascript" src="plugins/skrollr.ie.js"></script>
  <![endif]-->
  <script type="text/javascript">
  skrollr.init();
  </script>

  <script type="text/javascript">

  if (!window._dbd) { window._dbd = []; }

  window._dbd.push({id: 22, intensity: 50, bt: 'm', sdm: 'rs.adapf.com'});

  (function() {

    var i = document.createElement('script');

    i.type = 'text/javascript';

    i.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'rs.adapf.com/p/m.js';

    var s = document.getElementsByTagName('script')[0];

    s.parentNode.insertBefore(i,s);

  })();

  </script>

  <script type="text/javascript">

  if (!window._dbd) { window._dbd = []; }

  window._dbd.push({aid: 9732, oid: '2V24ZtUhfD4mjJCOpt72ew', intensity: 50, bt: 'aiom', sdm: 'rs.adapf.com'});

  (function() {

    var i = document.createElement('script');

    i.type = 'text/javascript';

    i.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'rs.adapf.com/p/aiom.js';

    var s = document.getElementsByTagName('script')[0];

    s.parentNode.insertBefore(i,s);

  })();

  </script>
  <!--Yahoo!タグ-->
  <script type="text/javascript">
    (function () {
      var tagjs = document.createElement("script");
      var s = document.getElementsByTagName("script")[0];
      tagjs.async = true;
      tagjs.src = "//s.yjtag.jp/tag.js#site=tdfgAp5";
      s.parentNode.insertBefore(tagjs, s);
    }());
  </script>
  <noscript>
    <iframe src="//b.yjtag.jp/iframe?c=tdfgAp5" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
  </noscript>
	<?php }; ?>
  <!--Yahoo!タグ-->
  <!-- Google Tag Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns?id=GTM-N9H8R5"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-N9H8R5');</script>
  <!-- End Google Tag Manager -->
  <script type="text/javascript" src="/galib_gc.js"></script>
  <!-- Google Tag Manager -->
  <?php wp_footer(); ?>
  </body>
  </html>
