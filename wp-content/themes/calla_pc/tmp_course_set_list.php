<div class="block-white block-promotions">
  <section class="mod-value2 col1Ver">
    <h2 class="title-head icon-ninki-course">人気のコース</h2>
    <ul>
      <li>
        <a href="<?php bloginfo('url') ?>/plan/" class="trans">
          <img src="<?php bloginfo('template_url') ?>/resource/img/common/2001_btn-camp-pc.png" alt="全6回おまとめ予約！人気の顔・VIOもコミコミの24ヵ所全身脱毛！ 8ヵ月卒業プラン 6回完了＋1年間メンテナンス付き 月額5200円" width="628" height="90">
        </a>
      </li>
    </ul>
  </section><!-- mod-value --><!-- end 人気のコース -->

  <section class="mod-price">
    <h2 class="title-head icon-plan">おトクなキャンペーン</h2>
    <div class="mod-price-body">

<div class="cvset-mb">
      <a href="<?php bloginfo('url') ?>/campaign/waribiki.html#norikae-cp" class="mod-price-block wide-twothirds trans">
        <section class="mod-price-recommend">
          <div class="wide-twothirds-inner" style="height: 62px;">
            <div class="wide-twothirds-title">
            	<em>一番人気</em>
            	<h3>のりかえ割</h3>
            </div>
            <div class="wide-twothirds-comm">
	            <ul>
	              <li>他サロンから乗り換えで</li>
	              <li>10,000円OFF</li>
	            </ul>
            </div>
          </div>
        </section>
      </a><!-- end のりかえ -->

      <a href="<?php bloginfo('url') ?>/gakuwari/" class="mod-price-block wide-twothirds trans">
        <section class="mod-price-recommend">
          <div class="wide-twothirds-inner" style="height: 62px;">
            <div class="wide-twothirds-title">
              <em>学生のうちに</em>
              <h3>脱毛学割</h3>
            </div>
            <div class="wide-twothirds-comm">
              <ul>
                <li>学生証のご提示でOK!</li>
                <li>10,000円OFF</li>
              </ul>
            </div>
          </div>
        </section>
      </a><!-- end 脱毛学割 -->

</div>

<div>
      <a href="<?php bloginfo('url') ?>/campaign/waribiki.html#pair-cp" class="mod-price-block wide-twothirds trans">
        <section class="mod-price-recommend">
          <div class="wide-twothirds-inner" style="height: 62px;">
            <div class="wide-twothirds-title">
            	<em>友達・親子OK</em>
            	<h3>ペア割</h3>
            </div>
            <div class="wide-twothirds-comm">
	            <ul>
	              <li>ご一緒の契約で</li>
	              <li>計20,000円OFF</li>
	            </ul>
            </div>
          </div>
        </section>
      </a><!-- end ペア割 -->

      <a href="<?php bloginfo('url') ?>/campaign/shokai.html" class="mod-price-block wide-twothirds trans">
        <section class="mod-price-recommend">
          <div class="wide-twothirds-inner" style="height: 62px;padding:7px 12px;">
            <div class="wide-twothirds-title">
            	<em>紹介キャンペーン</em>
            	<h3 style="font-size:17px;">おともだち紹介</h3>
            </div>
            <div class="wide-twothirds-comm" style="width:146px;">
	            <ul>
	              <li style="text-align:justify;font-size:12px;">ご紹介で入会された方に10,000円OFF・ご紹介してくれた方へも豪華特典！</li>
	            </ul>
            </div>
          </div>
        </section>
      </a><!-- end 祝割 -->
</div>

    </div><!-- /.mod-plan-body -->
    <ul class="mod-btns">
      <li><a href="<?php bloginfo('url') ?>/campaign/" class="btn-stripe trans">キャンペーン一覧</a></li>
	  <!-- <li><a href="<?php bloginfo('url') ?>/plan/part/" class="btn-stripe trans">部位一覧</a></li> -->
    </ul>
  </section><!-- /.mod-price --><!-- end プラン料金 -->
  </div><!-- block-promotions -->
