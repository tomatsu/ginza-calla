<?php
  $current_category = get_queried_object();
  $catid = $current_category->term_id;
  if (get_field('cat_meta_title','post_tag_'.$catid) == ''){
    $title = $current_category->name;
  }else{
    $title = get_field('cat_meta_title','post_tag_'.$catid);
  }
?>
<?php get_template_part('header'); ?>
<?php breadcrumb(); ?>
  <div class="wrapper container">
    <div class="main">
      <div class="block-white">
      <h1 class="page-head icon-column"><span>銀座カラー</span><?php echo $title; ?></h1>
      <div class="page-body">
        <div class="block-entries js-appendmore-contents">
        <?php if(have_posts()): while(have_posts()):the_post(); ?>
        <section class="block-category block-category-contents">
          <div class="block-category-body block-category-body-contents">
            <div class="text">
              <em class="lead"><?php the_title(); ?></em>
              <p> <?php the_excerpt(); ?></p>
              <div class="block-category-links-contents">
                <ul class="block-category-links block-category-contents-ul-01">
                  <?php the_category(); ?>
                  <?php the_tags('','',''); ?>
                </ul>
                <ul class="block-category-links block-category-contents-ul-02">
                  <li><a href="<?php the_permalink(); ?>" class="btn-column btn-red trans">記事を読む</a></li>
                </ul>
              </div>
            </div>
            <figure class="image">
              <img src="<?php echo catch_that_image(); ?>" alt="" width="245">
            </figure>
          </div>
        </section>
        <?php endwhile; endif; ?>
        <ul class="mod-btns block-entry-more">
          <li><a href="#" class="btn-stripe btn-icon-plus trans">もっとみる</a></li>
        </ul>
        </div>
        <div class="block-etc-contents block-etc-contents-tags">
          <h2 class="icon-cont icon-cont-tags">人気おすすめタグ</h2>
          <div class="">
            <?php
                 $alltags = get_terms('post_tag');
                 foreach($alltags as $taginfo):
            ?>
            <a href="<?php echo get_term_link($taginfo->term_id,'post_tag'); ?>"><?php echo $taginfo->name; ?></a>
            <?php endforeach; wp_reset_postdata(); ?>
          </div>
        </div>
        <div class="block-etc-contents block-etc-contents-category">
          <h2 class="icon-cont icon-cont-cate">すべてのカテゴリー</h2>
          <div class="">
            <?php
            $parent_id = get_category_by_slug("contents")->cat_ID;
            $args = array(
              'orderby'       => '',
              'parent'        => $parent_id
                );
                $allcate = get_terms('category',$args);
                foreach($allcate as $cates):
            ?>
            <a href="<?php echo get_term_link($cates->term_id,'category'); ?>"><?php echo $cates->name; ?></a>
            <?php endforeach; wp_reset_postdata(); ?>
          </div>
        </div>
      </div><!-- /.page-body -->
      </div><!-- /.block-white -->
      <div class="mod-btn-counsel">
        <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0016" class="btn-stripe btn-shadow btn-counsel btn-counsel-large trans" target="_blank"><span>無料カウンセリング</span>ご予約はこちら</a>
      </div> <!-- /.mod-btn-counsel -->
    </div>
  <?php get_template_part('sidebar'); ?>
  </div>
<?php get_template_part('footer'); ?>
