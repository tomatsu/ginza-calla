<?php
/*
Template Name: サロンページ用テンプレート
*/
?>
<?php get_template_part('header'); ?>
<?php breadcrumb(); ?>
  <div class="wrapper container">
    <div class="main">
      <div class="block-white">
        <h1 class="page-head" itemprop="name"><?php the_title(); ?></h1>
        <div class="page-body">
		  <p><?php the_field('shop_text'); ?></p>
  			<div class="shop-info">
  				<dl class="shop-info-data">
  					<dt>【住所】</dt>
  					<dd itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><?php the_field('address'); ?></dd>
  					<dt>【営業時間】</dt>
  					<dd><span itemprop="openingHours" ><?php the_field('open_time'); ?></span><br/></dd>
  					<dt>【ベッド数】</dt>
  					<dd><?php the_field('bed'); ?>台</dd>
  					<dt>【駐車場】</dt>
  					<?php if (get_field('parking')){ ?>
              <?php if (get_field('parking_text') == ""){ ?>
  						<dd>有</dd>
              <?php }else{ ?>
              <dd><?php the_field('parking_text'); ?></dd>
              <?php }; ?>
  					<?php }else{ ?>
  						<dd>無</dd>
  					<?php }; ?>
  				<!-- /.shop-info-data --></dl>
         		<div class="shop-image">
          			<p><img src="<?php bloginfo('template_url') ?>/resource/<?php the_field('shop_image_main'); ?>" itemprop="image"></p>
          			<div class="shop-image-inner">
              		<p class="shop-image-left">
                     	<img src="<?php bloginfo('template_url') ?>/resource/<?php the_field('shop_image_sub1'); ?>" itemprop="image">
                    </p>
            		<p class="shop-image-right">
            			<img src="<?php bloginfo('template_url') ?>/resource/<?php the_field('shop_image_sub2'); ?>" itemprop="image">
            		</p>
            		<!-- /.shop-image-inner --></div>
  				<!-- /.shop-image --></div>
  			<!-- /.shop-info --></div>
  			<div class="shop-access">
    			<div class="shop-map">
    				<p class="shop-map-image"><img src="<?php bloginfo('template_url') ?>/resource/<?php the_field('map_image'); ?>" itemprop="image"></p>
    				<div class="shop-g-map" id="map-canvas"><!-- /.shop-g-map --></div>
    			<!-- /.shop-map --></div>
    			<div class="shop-map-blln">
    		  	<p><?php the_field('map_text'); ?></p>
    		  <!-- /.shop-map-blln --></div>
          <?php if (get_field('parent_area')) { ?>
            <?php
              $field = get_field_object('parent_area_name');
              $value = get_field('parent_area_name');
           ?>
            <ul class="mod-btns">
              <li><a href="<?php bloginfo('url') ?>/salon/area/<?php the_field('parent_area_name') ?>" itemprop="url" class="btn-stripe trans"><?php echo $field["choices"][$value]; ?>周辺の店舗Map</a></li>
            </ul>
          <?php }; ?>
  		  <!-- /.shop-access --></div>
        </div><!-- /.page-body -->
      </div><!-- /.block-white -->
       <div class="mod-btn-counsel">
         <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0016" class="btn-stripe btn-shadow btn-counsel btn-counsel-large trans" target="_blank"><span>無料カウンセリング</span>ご予約はこちら</a>
       </div> <!-- /.mod-btn-counsel -->
       <div class="block-white salon-voice" itemtype="http://schema.org/BeautySalon">
        <?php
          if (have_posts()) : while (have_posts()) : the_post();
          if(get_the_content() != ""){
        ?>
       	<section class="mod-voice">
     	  	<h2 class="title-head icon-voice"><?php the_title(); ?>のお客様の声</h2>
			       <div class="mod-voice-body">
           <?php the_content();  ?>
		       <ul class="mod-btns">
				 	<li><a class="btn-stripe trans" href="<?php the_permalink(); ?>/voice">その他の声を見る</a></li>
				</ul>
   		    </div>
       	<!-- /.mod-voice --></section>
        <?php
          };
          endwhile; endif;
         ?>
       <?php if(get_field('message')){ ?>
	       <section class="mod-message">
	  			<h2 class="title-head icon-mail">サロンからのメッセージ</h2>
	  			<div class="mod-message-body">
	    			<p><?php the_field('message'); ?></p>
	  			<!-- /.mod-message-body --></div>
	  		<!-- /.mod-message --></section>
	  		<!-- /.end サロンからのメッセージ -->
		<?php }; ?>
      <?php if(get_field('blog_url')){ ?>
      <ul class="mod-btns">
        <?php if(get_field('blog_url_br')){ ?>
          <li><a href="<?php the_field('blog_url'); ?>" target=”_blank” class="btn-stripe trans"><?php the_title(); ?>の<br>スタッフブログ</a></li>
        <?php }else{ ?>
          <li><a href="<?php the_field('blog_url'); ?>" target=”_blank” class="btn-stripe trans"><?php the_title(); ?>のスタッフブログ</a></li>
        <?php }; ?>
      </ul>
      <?php }; ?>
       <!-- /.block-white --></div>
     <?php
       if( !get_field('new_cust_banner') ){
         get_template_part('tmp_course_set_list');
       };
      ?>
      <?php if( !get_field('free_banner') ){ ?>
      <div class="mod-btn-counsel">
        <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0016" class="btn-stripe btn-shadow btn-counsel btn-counsel-large trans" target="_blank"><span>無料カウンセリング</span>ご予約はこちら</a>
      </div> <!-- /.mod-btn-counsel -->
      <?php }; ?>
    </div><!-- /.main -->
    <?php get_template_part('sidebar'); ?>
  </div><!-- /.wrapper -->
<?php get_template_part('footer'); ?>
