<a id="nav1" name="nav1"></a>
<div id="navGlobal">
<img src="<?php bloginfo('template_url') ?>/resource/recruit/images/menu-170127.png?180213" alt="" width="208" height="384" usemap="#Map"  data-0="" data-9300=""/>
  <map name="Map">
    <area shape="rect" coords="55,121,135,142" href="#nav1" id="nav1">
    <area shape="rect" coords="55,143,138,165" href="#nav2" id="nav2">
    <area shape="rect" coords="55,165,140,188" href="#nav3" id="nav3">
    <area shape="rect" coords="55,189,139,210" href="#nav4" id="nav4">
    <area shape="rect" coords="56,211,141,231" href="#nav5" id="nav5">
    <area shape="rect" coords="19,272,177,309" href="#scene_08" id="nav6">
    <area shape="rect" coords="41,313,163,334" href="https://www.facebook.com/ginza.calla" onclick="var myUrl=this.href; _gaq.push(function(){var tracker=_gaq._getAsyncTracker(); window.open(tracker._getLinkerUrl(myUrl))}); return false;" target="_blank">
    <area shape="rect" coords="41,346,163,422" href="#scene_08" id="nav6">
    <area shape="rect" coords="29,2,165,64" href="#nav1">
  </map>
</div>
<div id="base">
<!--<div id="base" data-0="top:0px;" data-9300="top:-9300px;">-->

    <div id="scene_01">
      <div id="scene_01_box">
      <div id="topSliderOuter">
      <div class="topSlide clearFix"><ul class="slides">
		<!-- <li id="s1_slide1">関東　エステティシャン募集再開　2016年9月以降入社者</li> -->
        <li id="s1_slide2">きらきら輝きたいあなたへ</li>
        <li id="s1_slide3-3">エステティシャン募集</li>
		<!--<li id="s1_slide9png">2018年度　新卒採用　3月1日よりエントリー開始</li>-->
        <li id="s1_slide5">写真</li>
        <li id="s1_slide6">写真</li>
        <li id="s1_slide7">写真</li>
        <li id="s1_slide8">写真</li>
        </ul></div></div>
      </div>
    </div>


<!-- エントリーはこちらから -->
<a id="nav6" name="nav6"></a>
  <div id="scene_10">
    <div id="scene_10_box">
		<div id="s8_btn1"><a href="https://job.mynavi.jp/20/pc/search/corp102257/outline.html" target="_blank"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s8_btn1812.png" width="240" height="85" alt="新卒採用"/></a></div>
      <div id="s8_btn4"><a href="https://ginza-calla-recruit.net/jobfind-pc/" target="_blank"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s8_btn4.png?0219" width="240" height="85" alt="銀座カラー　中途採用(正社員)募集サイト"/></a></div>

<p style="padding:410px 0 0 270px; font-size:125%; color:#fff; letter-spacing:0.1em;">お電話でのご応募・お問い合わせは　<span style="font-size:110%;">0120-999-870</span></p>
    </div>
  </div>
<!-- //エントリーはこちらから// -->


    <div id="scene_02">
      <div id="scene_02_box">
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s1_gpc1.png" id="s1_gpc1" width="197" height="135" alt="" data-400="transform[bounce]:scale(0);" data-450="transform[bounce]:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s1_gpc2.png" id="s1_gpc2" width="296" height="219" alt="" data-200="transform[bounce]:scale(0);" data-250="transform[bounce]:scale(1);"/>
      </div>
    </div>

<a id="nav2" name="nav2"></a>
  <div id="scene_03">
    <div id="scene_03_box">
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s3_gpc2.png" id="s3_gpc2" width="61" height="47" alt="" data-900="transform[bounce]:scale(0);" data-950="transform[bounce]:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s3_gpc1.png" id="s3_gpc1" width="297" height="402" alt="" data-1100="transform[bounce]:scale(0);" data-1150="transform[bounce]:scale(1);"/>
      <p id="s3_tex1">銀座カラーは現在国内に52店舗、海外に28店舗（2019年10月時点）<br>
        展開し、会員様の人数は約40万人にのぼる脱毛専門サロンです。<br>
        これほど多くの会員様に選ばれ、そして喜んでいただけるのも、<br>
        銀座カラーを支えるスタッフの力があってこそ。<br>
        高い技術を持って、笑顔で接客するスタッフは、企業にとって<br>
        大切な宝物です。<br>
        誰もが最初は笑顔も曇りがちですが、サロンの経験を積み重ねる<br>
        ことが自信につながり、笑顔の輝きは一段と増していきます。<br>
        私は誰よりも実感しています。<br>
        銀座カラーは誰もがきらきら輝ける職場。あなたの笑顔を、成長を、<br>
      活躍を、どの企業よりも高い期待を持ってお待ちしています。</p>

    </div>
    </div>

<a id="nav3" name="nav3"></a>
    <div id="scene_04" >
      <div id="scene_04_box">
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc0.png" width="49" height="79" alt="" id="s4_gpc0" data-1800="transform:scale(0);" data-1850="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc1.png" width="291" height="393" alt="" id="s4_gpc1" data-2050="transform:scale(0);" data-2100="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc2.png" width="220" height="290" alt="" id="s4_gpc2" data-2150="transform:scale(0);" data-2200="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc3.png" width="182" height="245" alt="" id="s4_gpc3" data-2200="transform:scale(0);" data-2250="transform:scale(1);"/>

      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc4.png?1805" width="63" height="50" alt="" id="s4_gpc4" data-2400="opacity:0;top:635px" data-2450="opacity:1;top:665px;"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc5.png?1905" width="148" height="51" alt="" id="s4_gpc5" data-2500="opacity:0;top:781px" data-2550="opacity:1;top:811px;"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc6.png?1905" width="116" height="60" alt="" id="s4_gpc6" data-2600="opacity:0;top:948px" data-2650="opacity:1;top:978px;"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc7.png?1905" width="320" height=" 220" alt="" id="s4_gpc7" data-2750="opacity:0;width:10px" data-2800="opacity:1;width:320px;"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc8.png?191016" width="70" height="50" alt="" id="s4_gpc8" data-3150="opacity:0;top:1409px" data-3200="opacity:1;top:1439px;"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc27.png" width="70" height="50" alt="" id="s4_gpc9" data-3250="opacity:0;top:1478px" data-3300="opacity:1;top:1508px;"/>
      <a href="<?php bloginfo('url') ?>/recruit/salonlist" target="_blank"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc10.png" width="225" height="36" alt="銀座カラー サロンリスト" id="s4_gpc10"/></a>

      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc11.png" width="71" height="113" alt="" id="s4_gpc11" data-3350="transform:scale(0);" data-3450="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc12.png?190312" width="70" height="109" alt="" id="s4_gpc12" data-3390="transform:scale(0);" data-3490="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc13.png" width="95" height="144" alt="" id="s4_gpc13" data-3270="transform:scale(0);" data-3370="transform:scale(1);"/><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc16.png?190326" width="93" height="142" alt="" id="s4_gpc16" data-3240="transform:scale(0);" data-3260="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc14.png" width="71" height="113" alt="" id="s4_gpc14" data-3310="transform:scale(0);" data-3410="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc15.png" width="70" height="110" alt="" id="s4_gpc15" data-3410="transform:scale(0);" data-3510="transform:scale(1);"/><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc17.png" width="92" height="142" alt="" id="s4_gpc17" data-3140="transform:scale(0);" data-3260="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc18.png" width="93" height="142" alt="" id="s4_gpc18" data-3290="transform:scale(0);" data-3310="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc19.png" width="70" height="113" alt="" id="s4_gpc19" data-3240="transform:scale(0);" data-3260="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc20.png" width="71" height="113" alt="" id="s4_gpc20" data-3300="transform:scale(0);" data-3400="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s4_gpc21.png" width="70" height="113" alt="" id="s4_gpc21" data-3400="transform:scale(0);" data-3500="transform:scale(1);"/>
      <p id="s4_tex1">1993年の創業以来、銀座カラーは企業として成長をし続けています。売上高や店舗数が物語るのは、創業26年間で培ってきた信頼と実績。<br>
        つまり企業としての誇りです。</p>
      </div>
    </div>

<a id="nav4" name="nav4"></a>
  <!-- voice -->
    <div id="scene_05">
      <div id="scene_05_box">
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s5_gpc1.png" width="60" height="49" alt="" id="s5_gpc1" data-3700="transform:scale(0);" data-3800="transform:scale(1);"/>
      <div>
        <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav1_n_on.png" width="126" height="81" alt="" id="s5_nav1"/>
        <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav2.png?190402" width="131" height="81" alt="" id="s5_nav2"/>
        <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav3_t.png" width="131" height="80" alt="" id="s5_nav3"/>
        <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav4.png" width="127" height="82" alt="" id="s5_nav4"/>
<!--
        <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s5_nav5.png" width="129" height="80" alt="" id="s5_nav5"/>
-->
      </div>

<!-- Interview -->
<div id="flexsliderOuter">
<div class="flexslider clearFix">
<ul class="slides clearFix">

<!-- Salon 中川
<li>
<div>
<table>
<tr><td><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide1_1.gif" width="370" height="140" alt="銀座カラーは女性が輝く職場です 新宿東口店 店長 中川靖菜" style="margin:40px 0 0 50px;" /><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide1_m1.gif" width="340" height="44" alt="スタッフの喜びこそ店長のやりがい" style="margin:10px 0 5px 42px;" /></td><td rowspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide1_3.jpg" width="380" height="490" alt="写真：中川靖菜" style="margin:40px 50px 0 0;" /></td></tr>
<tr><td class="text1_1">
<p>新宿東口店は施術ルームが14部屋もあり、毎日多くのお客様が訪れる大型サロンです。現在は、お客様のお手入れやカウンセリングのほか、お店全体を見てスタッフ指導をし、売上げ管理を行ったり、毎日楽しく幅広い業務を担当しています。<br>
店長として一番心がけているのは『笑顔』。常に笑顔でいることで、お客様はもちろんスタッフの心地よさにもつながると思います。店長とはいえスタッフに近い存在でありたいので、スタッフの声に耳を傾けてフォローをしたり、嬉しいことはとことん一緒に喜びます。私にとって、その嬉しそうな姿を見れることが何よりもやりがいになっています。</p>
</td></tr>
</table>
<table>
<tr><td rowspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide1_4.jpg" width="210" height="240" alt="写真：中川靖菜" style="margin:40px 20px 0 60px;" /></td><td style="height: 65px;"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide1_m2.gif" width="263" height="44" alt="憧れていたキャリアアップ" style="margin:25px 0 5px 0;" /></td></tr>
<tr><td class="text1_2">
	<p>入社の決め手は、頑張った分だけ評価してくれるキャリアアップ制度。<br>常に向上したいという気持ちがあるので、頑張り次第で評価してくれるところは魅力的でしたし、自分の力を試してみたいという気持ちが強かったです。もちろん実際に、上司がちゃんと見ていてくれ「頑張って、期待してるよ」という声を掛けてくれたときは、すごく嬉しかったです。その気持ちに答えたいって思いましたね。<br>今は私も見習って、スタッフの頑張りや熱意にきちんと目を向け、褒めることは大切にしています。</p>
</td></tr>
</table>
<br><br>
<table>
<tr><td colspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide1_7.jpg" width="880" height="570" alt="1日のスケジュール"/></td></tr>
</table>
</div>
</li>
 -->

<!-- チーフ 阿部 -->
<li>
<div class="voice_r">
<table>
<tr><td rowspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide2_2.jpg" width="380" height="490" alt="写真：阿部まゆみ" style="margin:40px 0 0 65px;" /></td><td><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide2_1.png?181022" width="436" height="255" alt="やりがいがあって働きやすい、自慢の職場です 新宿店 チーフ 阿部 まゆみ"/></td></tr>
<tr><td class="text1_1">
	<p>新宿店のチーフとなり、責任感が増したことで、さらに仕事がおもしろくなってきました。<br>
チーフは「お店全体をみる」のが必要条件なので、お客様への対応はもちろん、後輩の指導にも力を入れています。私から頻繁に話しかけたり、ランチに誘ったり、たくさんコミュニケーションをとることで、後輩が質問をしやすい雰囲気を作るよう心がけています。<br>
また注意など言いにくいことはスパッと短い言葉で伝えて、その後に笑顔でフォロー。実はこの方法は、尊敬する先輩を見て学んだもの。いつか後輩が、この指導法を受け継いでくれるとうれしいですね。</p>
</td></tr>
</table>
<table>
<tr><td style="height: 65px;"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide2_3.png" width="590" height="65" alt="脱毛サロンならお客様と深く関わることができる"/></td><td rowspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide2_4.jpg" width="210" height="240" alt="写真：阿部まゆみ" style="margin:45px 0 0 20px;" /></td></tr>
<tr><td class="text1_2">
<p>前職はアパレルの販売員でした。仕事を通じて接客のおもしろさを知り、もっとお客様と深く関わりたいと思ったことがきっかけで、銀座カラーのエステティシャンに転職しました。<br>
脱毛サロンのお客様は何度も繰り返して通ってくださるので、回を重ねるごとに笑顔が増えたり、会話が盛り上がったり、距離がどんどんと近づいていくのがわかります。この感覚は、エステティシャンならではの喜びだと思います。お客様との時間を楽しいものにしたくて、お客様の趣味に合わせて、ファッションや美容の情報を事前にチェックしておくことも。「楽しい時間をありがとう」と言っていただけると、思わずガッツポーズをしたくなるくらい嬉しいです。</p>
</td></tr>
</table>
<table>
<tr><td colspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide2_5.png" width="880" height="590" alt="1日のスケジュール"/></td></tr>
</table>
</div>
</li>

<!-- Salon 保坂 -->
<li>
<div>
<table>
<tr><td><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide3_1.gif?190402" width="370" height="140" alt="お客様が笑顔で帰って頂く瞬間にやりがいを感じます　池袋サンシャイン通り店 リーダー 保坂 双葉 （2015年4月 新卒入社）" style="margin:40px 0 0 50px;" /><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide3_m1.gif" width="280" height="44" alt="毎日が楽しく成長出来る環境" style="margin:10px 0 5px 42px;" /></td><td rowspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide3_2.jpg" width="380" height="490" alt="写真：保坂 双葉" style="margin:40px 50px 0 0;" /></td></tr>
<tr><td class="text1_1">
<p>就職活動の際に、さまざまな業界を見る中で以前から美容には興味があったので、脱毛業界も調べていました。そこで最初に目にしたのが“銀座カラー”。説明会の時にお客様を大切にするのはもちろん”一緒に働く仲間も大事”との話を聴いて、ここで働きたい！と強く思いました。上下関係はしっかりしつつもスタッフ同士仲良く一緒にご飯に行ったり、美容情報を交換して盛り上がったりと、毎日楽しく過ごしています。<br>また、研修も充実しているので一から敬語やマナーなどを学び、大人の女性としての立ち振る舞いや考え方を教えて頂いたことで安心してサロンに立つことが出来ました。</p>
</td></tr>
</table>
<table>
<tr><td rowspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide3_4.jpg" width="210" height="240" alt="写真：保坂 双葉" style="margin:40px 20px 0 60px;" /></td><td style="height: 65px;"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide3_m2.gif" width="300" height="44" alt="お客様と接することのしあわせ" style="margin:25px 0 5px 0;" /></td></tr>
<tr><td class="text1_2">
<p>大学で、日本語コミュニケーションを専攻していたので接客の時にとても役に立っています。技術面では常に目標を立てて頑張ることで、今では全ての部位のお手入れをさせていただいています。接客が好きでお客様のお手入れ中、いろいろなお話ができることがとても楽しいですね。お客様からいただける「ありがとう」の言葉は本当にやりがいを感じます。よくあるお話かもしれませんが(笑)頑張ったからこそすごく響きましたし、また次も頑張ろうと思いました。今は、後輩の新卒エステティシャンも入ってきたので憧れの先輩になれるよう積極的に勉強しています。</p>
</td></tr>
</table>
<br><br>
<table>
<tr><td colspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide3_5.jpg" width="880" height="570" alt="1日のスケジュール"/></td></tr>
</table>
</div>
</li>

<!-- Salon 岡村 -->
<li>
<div class="voice_r">
<table>
<tr><td rowspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide4_2.jpg" width="380" height="490" alt="写真：岡村なつみ" style="margin:40px 0 0 65px;" /></td><td><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide4_1.gif?181022" width="370" height="140" alt="成長するほど、また新しいやりがいが見つかります 新宿店 主任 岡村なつみ（2013年4月 新卒入社）" style="margin:40px 0 0 20px;" /><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide4_m1.gif" width="360" height="44" alt="未経験でもチャレンジ出来る" style="margin:10px 0 5px 10px;" /></td></tr>
<tr><td class="text1_1" width="400">
<p>主任という立場になり、後輩スタッフをまとめるポジションである大切な役割だと実感しています。<br>
身近な後輩スタッフのちょっとした変化にも気付いて気持ちを汲み取ってあげられるよう、日々の会話を通してコミュニケーションを図るようにしています。また、私が実践してお客様に喜んでいただけた行動を、積極的に後輩にも伝えることで、自信を持って接客に入ってもらっています。成長していく姿を近くで見れることがとても嬉しいですし、私ももっと頑張ろうという刺激にもなりますね。スタッフ全員で喜びを共有していけるように、先輩と後輩スタッフの架け橋のような存在でありたいです。</p>
</td></tr>
</table>
<table>
<tr><td style="height:65px;"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide4_m2.gif" width="420" height="44" alt="リフレッシュ休暇を利用してカリフォルニアに" style="margin:30px 0 0 55px;" /></td><td rowspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide4_4.jpg" width="210" height="240" alt="写真：阿部まゆみ" style="margin:45px 0 0 20px;" /></td></tr>
<tr><td class="text1_2">
<p>銀座カラーでは約7日間の連休がとれる「リフレッシュ休暇制度」があり、その制度を利用して、昨年カリフォルニアのディズニーランドに遊びにいってきました。<br>
毎年がんばった自分へのご褒美に、旅行を満喫。とても気分がリフレッシュ出来るので、新たな気持ちで仕事に取り組めます。<br>
今の目標は、お客様に笑顔になって頂くのはもちろん後輩にこの仕事の楽しさを伝えていくこと。そして私自身もさらなるスキルアップも目指します。これから全力で仕事をして、今年はハワイかプーケットに行きたいです。キレイな海で気持ちよく癒される予定。このメリハリのある働き方が、私に合っているのだと思います。</p>
</td>
</tr>
</table>
<table>
<tr><td colspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide4_5.png" width="880" height="590" alt="1日のスケジュール"/></td></tr>
</table>
</div>
</li>

<!-- SV 長谷尾 -->
<li>
<div>
<table>
	<tr><td><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide5_1.gif" width="370" height="140" alt="「努力の数だけ、着実にキャリアアップできます」スーパーバイザー 長谷尾　聡子" style="margin:40px 0 0 50px;" /><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide5_m1.gif" width="368" height="44" alt="毎日が楽しく成長出来る環境" style="margin:10px 0 5px 42px;" /></td></td><td rowspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide5_2.jpg" width="380" height="490" alt="写真：長谷尾 聡子" style="margin:40px 50px 0 0;" /></td></tr>
<tr><td class="text1_1">
	<p>現在はスーパーバイザーとして担当している店舗のスタッフの育成や売上の管理を行っています。<br>
特に大切にしていることは、スタッフ一人ひとりの成長をみながら、次のステップに進むためのサポートをすることと、働きやすい環境を整えてあげることです。<br>
銀座カラーは評価制度が明確なため、がんばった分だけ着実にキャリアが重ねられます。目標を一つひとつ達成していくステップは楽しく、充実した毎日ではありますが、私もまだまだこれから。結婚や出産を経験したとしても、会社のバックアップ制度を利用しながら、さらなるキャリアアップを目指したいです。</p>
</td></tr>
</table>
<table>
<tr><td rowspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide5_3.jpg" width="210" height="240" alt="写真：長谷尾 聡子" style="margin:20px 20px 0 60px;" /></td><td style="height: 65px;"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide5_m2.gif" width="243" height="44" alt="理想の職場だった銀座カラー" style="margin:25px 0 5px 0;"/></td></tr>
<tr><td class="text1_2">
<p style="margin:20px 0 0 0;" />入社して一年間は覚えることが多く、目まぐるしい日々を送っていました。そんな私の支えだったのが、当時指導してくれていた先輩。「今年はずいぶん成長してくれたね」とメールをもらった時は、嬉しくてすぐに保存。実は今でも時々、見直しては勇気づけられています。私も見習って、スタッフの技術が向上した時や、目標を達成した場合は、褒め言葉を忘れないようにしています。指導してきたスタッフの成長は、何よりの喜びなんですね。</p>
</td></tr>
</table>
<br><br>
<table>
<tr><td colspan="2"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/voice/s5_slide5_5.jpg" width="880" height="570" alt="1日のスケジュール" style="margin: 0 0 0 10px;" /></td></tr>
</table>
</div>
</li>


</ul>
</div></div>

      </div>

    </div>

<a id="nav5" name="nav5"></a>
  <!-- 働く環境についてもっと知りたい-->
  <div id="scene_06">
    <div id="scene_06_box">
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s6_gpc1.png" id="s6_gpc1" width="197" height="135" alt="" data-5800="transform:scale(0);" data-5850="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s6_gpc2.png" id="s6_gpc2" width="296" height="219" alt="" data-5650="transform:scale(0);" data-5700="transform:scale(1);"/>
    </div>
  </div>

  <div id="scene_07">
    <div id="scene_07_box">
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s7_gpc0.png" width="54" height="66" alt="" id="s7_gpc0" data-6250="transform:scale(0);" data-6300="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s7_gpc1.png" width="190" height="150" alt="" id="s7_gpc1" data-6400="transform:scale(0);" data-6500="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s7_gpc2.png" alt="" name="s7_gpc2" width="170" height="170" id="s7_gpc2" data-6600="transform:scale(0);" data-6700="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s7_gpc3.png" width="170" height="170" alt="" id="s7_gpc3" data-6800="transform:scale(0);" data-7000="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s7_gpc4.png" width="170" height="170" alt="" id="s7_gpc4" data-7200="transform:scale(0);" data-7300="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s7_gpc5.png" width="170" height="170" alt="" id="s7_gpc5" data-7600="transform:scale(0);" data-7700="transform:scale(1);"/>
    <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s7_gpc6.png" width="170" height="170" alt="" id="s7_gpc6" data-8050="transform:scale(0);" data-8100="transform:scale(1);"/>
    <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s7_gpc7.png" width="170" height="170" alt="" id="s7_gpc7" data-8400="transform:scale(0);" data-8500="transform:scale(1);"/>
<!-- <a href="<?php bloginfo('url') ?>/recruit/conditions" id="cboxJob"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s7_gpc8.png" width="207" height="31" alt="" id="s7_gpc8" style="padding-top:20px;" /></a> -->
    <p id="s7_tex1">銀座カラーは、正しい脱毛技術を習得するための実技研修はもちろん、<br>
      エステティシャンの心得や脱毛の仕組み、マナー講師によるビジネスマナーなどのさまざまな研修制度で、入社された方々をプロのエステティシャンへと導きます。<br>
      一つひとつ丁寧に指導するため、美容業界が未経験の人でも安心です。<br>
      またお客様により高い満足を提供するために、ヒューマンスキルアップ研修を入社後すぐに実施。マナーや言葉遣いといった接客の基本を身に付けます。研修を終えた方々は、みんな表情が凛としていて、女性としての輝きがいっそう増します。</p>
    <p id="s7_tex2">銀座カラーではその人の意欲や技術力を明確な評価制度にて判断し、着実なキャリアアップを目指していただきます。やる気次第では入社3年で店長になることも可能。まずはスタッフとして経験を積み、その後はリーダー、副主任、主任、チーフ、店長、エリアマネージャーとキャリアを重ねていってください。<br>また定期的に、全国の店舗を対象にした「接客マナーコンテスト」を実施。<br>スタッフのやる気向上にもつながる、銀座カラーのイベントです。</p>
    <p id="s7_tex3" style="padding-top:10px;">笑顔で働くためにも、手当はとても大切だと銀座カラーは考えています。<br>
      確かな技術を身に付けた人には技術手当を、役職に就きスタッフをまとめる人には役職手当をつけ、仕事に対するモチベーションを高めてもらいます。<br>
      そのほか美容手当、誕生月には誕生日手当など、仕事はもちろんプライベートまで充実するユニークな手当がたくさん！<br>
      ほら、銀座カラーで働きたくなってきませんか。</p>
    <p id="s7_tex4" style="padding-top:10px;">福利厚生では、雇用保険や労災保険、健康、厚生年金といった各種社会保険を完備しているのはもちろん、いつも健やかに働いてほしいから年に一度、健康診断も実施しています。さらに社員脱毛制度があり、銀座カラーのスタッフは無料で脱毛を行うことができます。</p>
    <p id="s7_tex5" style="padding-top:15px;">銀座カラーでは結婚している人や小さな子供がいるスタッフがたくさん活躍しています。その理由は、産前産後休暇や育児休暇といった充実の育児支援制度。<br>
      またアルバイトで復帰しているスタッフもいれば、時短勤務制度を使い、社員として復帰しているスタッフもいます。<br>
      がんばるママたちにとって働きやすい環境づくりを目指しています。</p>
      <p id="s7_tex6" style="padding-top:20px;">勤務形態はシフト制で、月9日はしっかりお休みいただけます。<br>
        年末年始の休暇や有給休暇制度、また、有給休暇を利用して最大7日間の大型連休がとれるリフレッシュ休暇制度もあり、スタッフの充実したライフスタイルを応援します。1日の実働時間は8時間。各店舗の業務量に対して適切な人数のスタッフを配置することで、なるべく残業がないよう工夫をしています。</p>
    </div>
  </div>


<!-- エントリーはこちらから -->
  <div id="scene_08">
    <div id="scene_08_box">
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s8_gpc_1.png" id="s8_gpc1" width="62" height="51" alt="" data-8700="transform:scale(0);" data-8800="transform:scale(1);"/>
      <img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s8_gpc_2.png" id="s8_gpc2" width="227" height="134" alt="" data-8700="transform:scale(0);" data-8800="transform:scale(1);"/>
      <div id="s8_btn1"><a href="https://job.mynavi.jp/20/pc/search/corp102257/outline.html" target="_blank"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s8_btn1812.png" width="240" height="85" alt="新卒採用"/></a></div>
      <div id="s8_btn4"><a href="https://ginza-calla-recruit.net/jobfind-pc/" target="_blank"><img src="<?php bloginfo('template_url') ?>/resource/recruit/images/s8_btn4.png?0219" width="240" height="85" alt="銀座カラー　中途採用(正社員)募集サイト"/></a></div>

<p style="padding:410px 0 0 270px; font-size:125%; color:#fff; letter-spacing:0.1em;">お電話でのご応募・お問い合わせは　<span style="font-size:110%;">0120-999-870</span></p>
    </div>
  </div>
<!-- //エントリーはこちらから// -->


  <div id="scene_09">
    <div id="scene_09_box"></div>
  </div>

</div>
