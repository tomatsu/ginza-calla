<?php
/*
Template Name: 移行用ミニマルテンプレート
*/

get_template_part('header');
breadcrumb();
if (have_posts()) {
  while (have_posts()) {
    the_post();
    the_content();
  }
}

get_template_part('footer');