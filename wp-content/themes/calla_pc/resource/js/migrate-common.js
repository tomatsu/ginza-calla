!(function() {
  'use strict'

  /**
   * スクロールで通り過ぎたらクラスを追加
   */
  var addClassOnScroll = function() {
    var CLASS_ACTIVE = 'is-active'
    var $magic = $('.js-magic')
    var controller = new ScrollMagic.Controller();

    $magic.each(function() {
      var $this = $(this)

      new ScrollMagic.Scene({
          triggerElement: this,
          triggerHook: 0.8
        })
        .on('start', function(e) {
          if (e.scrollDirection === 'FORWARD') {
            $this.addClass(CLASS_ACTIVE)
          }
        })
        .addTo(controller);
    })
  }

  /**
   * スクロールで画像をずらす
   */
  var MovingImage = (function() {
    var MovingImage = function() {
      this.$targets = $('.mg-page-moving-frame')
      this.controller = new ScrollMagic.Controller()
      this.scenes = []
    }

    MovingImage.prototype = {
      watch: function() {
        var self = this
        var winHeight = $(window).innerHeight()

        this.$targets.each(function() {
          var $frame = $(this)
          var $image = $frame.find('.mg-page-moving-image')
          var frameHeight = $frame.innerHeight()
          var imageHeight = $image.innerHeight()
          var movingRate = 100 * (imageHeight - frameHeight) / imageHeight
          var watchId = $(this).attr('data-watch')
          var target = watchId ? document.getElementById(watchId) : this
          var direction = $(this).hasClass('mg-mod-reverse') ? 'reverse' : 'normal'

          var instance = new ScrollMagic.Scene({
              triggerElement: target,
              triggerHook: 1,
              duration: winHeight + $(target).innerHeight()
            })
            .on('progress', function(e) {
              var sign = direction === 'reverse' ? -1 : 1
              var y = (sign * movingRate * e.progress) + '%'

              TweenMax.set($image, {y: y})
            })
            .addTo(self.controller)
          
          $image.imagesLoaded({background: true}, function(d) {
            $image.addClass('is-loaded')
          })

          self.scenes.push({
            instance: instance,
            $target: $(target),
            $frame: $frame,
            $image: $image
          })
        })
      },
      bind: function() {
        var self = this
        $(window).on('resize', function() {
          self.updateScene()
        })
      },
      updateScene: function() {
        var winHeight = $(window).innerHeight()

        $.each(this.scenes, function(index, scene) {
          var duration = winHeight + scene.$target.innerHeight()

          scene.instance.duration(duration)
        })
      }
    }

    return MovingImage
  })()

  $(function() {
    var movingImage = new MovingImage()

    movingImage.watch()
    movingImage.bind()
    addClassOnScroll()

    window.MigrateGinza = {
      movingImage: movingImage
    }
  })
})();