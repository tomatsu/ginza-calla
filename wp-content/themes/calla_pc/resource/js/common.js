/*--------------------------
	銀座カラー 共通設定
--------------------------*/
var config = config || {};
config.duration = 400;
config.delay = 200;
config.easing = 'easeOutQuint';





/*--------------------------
	銀座カラー 共通関数
--------------------------*/
var functions = functions || function() {};
functions = function() {
	var funcs = {};


	/*
	 * Name: getBrowser
	 * Description: 現在表示中のブラウザの種類を文字列として返す
	 * Return: [String]
	 *
	 */
	funcs.getBrowser = function() {
		var ua = navigator.userAgent.toLowerCase();
		var ver = navigator.appVersion.toLowerCase();
		var browser = 'unknown';

		if ( ua.indexOf("msie") != -1 ) {
			if ( ver.indexOf("msie 6.") != -1 ) {
				browser = 'ie6';
			} else if ( ver.indexOf("msie 7.") != -1 ) {
				browser = 'ie7';
			} else if ( ver.indexOf("msie 8.") != -1 ) {
				browser = 'ie8';
			} else if ( ver.indexOf("msie 9.") != -1 ) {
				browser = 'ie9';
			} else if ( ver.indexOf("msie 10.") != -1 ) {
				browser = 'ie10';
			} else {
				browser = 'ie';
			}
		} else if ( ua.indexOf('trident/7' ) != -1 ) {
			browser = 'ie11';
		} else if ( ua.indexOf('chrome') != -1 ) {
			browser = 'chrome';
		} else if ( ua.indexOf('safari') != -1 ) {
			browser = 'safari';
		} else if ( ua.indexOf('opera') != -1 ) {
			browser = 'opera';
		} else if ( ua.indexOf('firefox') != -1 ) {
			browser = 'firefox';
		}

		return browser;	
	};



	/*
	 * Name: trim
	 * Description: 文字列の両端の空白を削除する
	 * Argument: [String]
	 * Return: [String]
	 *
	 */
	funcs.trim = function( string ) {
		return string.replace(/^\s+|\s+$/g, '');
	};



	// Export Global Context
	return funcs;
}




/*--------------------------
	銀座カラー 共通処理
--------------------------*/

/* Auto Height
--------------------------*/
$(function(){

  /* プラン・料金
  --------------------------*/
  var $plan = $('.js-autoheight-plan');
  $plan.autoHeight({ column: 3, height: 'height' });

  /* 最新コラム
  --------------------------*/
  var $latest = $('.js-autoheight-latest');
  $latest.autoHeight({ column: 3, height: 'height' });
  
  /* お客様の声
  --------------------------*/
  var $voice = $('.js-autoheight-voice');
  $voice.autoHeight({ column: 2, height: 'height' });
  
  /* 新規出店予定店舗
  --------------------------*/
  var $voice = $('.js-autoheight-new');
  $voice.autoHeight({ column: 2, height: 'height' });
  
  /* プラン一覧
  --------------------------*/
  var $voice = $('.js-autoheight-planlist');
  $voice.autoHeight({ column: 2, height: 'height' });
  
  /* faq一覧
  --------------------------*/
  var $voice = $('.js-autoheight-faqlist');
  $voice.autoHeight({ column: 2, height: 'height' });
  
  /* 施述一覧
  --------------------------*/
  var $voice = $('.js-autoheight-treatment');
  $voice.autoHeight({ column: 4, height: 'height' });
  
  /* 学割　特典1
  --------------------------*/
  var $voice = $('.js-autoheight-special1');
  $voice.autoHeight({ column: 2, height: 'height' });
  
});



/* スムーズスクロール
--------------------------*/
$(function(){
  
  var $trigger = $('a[href^="#link-"]');
  $trigger.on('click', function(e) {
    e.preventDefault();
    var href = $(this).attr('href');
    $('html, body').stop().animate({
      scrollTop: $(href).offset().top
    }, config.duration, config.easing);
  });

});



/* セレクトメニュー
--------------------------*/
$(function(){
  var $select = $('.select > select');
  $select.customSelect({ customClass: 'select-view' });
});



/* タブ
--------------------------*/
$(function(){
	
	var Tab = function( $elem ) {

  	this.$el = $elem || $('.js-tab');
  	this.$tabNav = this.$el.find('.js-tab-nav');
  	this.$tabBody = this.$el.find('.js-tab-body');

    this.init();
    this.listenTo();
    this.changeByUrl();
	}

  Tab.prototype.init = function() {
    this.$tabBody.filter(':gt(0)').hide();
  }

  Tab.prototype.changeByUrl = function() {
    var tabName = this.getTabNameByUrl();
    var $tabAnchor;

    if (tabName) {
      $tabAnchor = this.$tabNav.find('li a[href="#' + tabName + '"]');
      if ($tabAnchor.length) {
        $tabAnchor.trigger('click');
      }
    }
  }

  Tab.prototype.getTabNameByUrl = function() {
    var query = document.location.search.substring(1);
    var params = {};
  
    if (!query) {
      return '';
    }

    query.split('&').forEach(function(param) {
      var kv = param.split('=');
      params[kv[0]] = kv[1];
    })

    return params.hasOwnProperty('tab') ? params.tab : '';
  }
  
  Tab.prototype.listenTo = function() {
    var data = { that: this };
    this.$tabNav.on('click', 'a', data, this.change);
  }

  Tab.prototype.change = function(e) {
    e.preventDefault();
    var href = $(this).attr('href');
    if ( href ) {
      
      e.data.that.$tabNav.find('li').removeClass('on');
      $(this).parent().addClass('on');
      e.data.that.$tabBody.hide()
      $( href ).fadeIn( config.duration );
      var target = $(this).attr("href"); // 0727add
      $("[href = "+target+"]").parent().addClass('on');// 0727add
    }
  }

  Tab.prototype.changeByIndex = function( index ) {
    if ( index ) {
      var that = this;
      e.data.that.$tabNav.find('a').removeClass('on');
      $tabNav.find('a').eq( index ).addClass('on');
      that.$tabBody.hide().eq( index ).fadeIn( config.duration );
    }
  }

  var tab2 = new Tab($('.js-tab-region'));
  var tab = new Tab();

  

});



/* トップボタン
--------------------------*/
$(function() {
    var topBtn = $('.totop');    
    topBtn.hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 500) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});