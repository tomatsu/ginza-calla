!(function() {
  'use strict'

  /**
   * ツールチップ
   *
   * ホバーしたときに吹き出しを出す
   */
  var Tooltip = (function() {
    var Tooltip = function() {
      this.$item = $('.mg-about-item')
      this.selectedClass = 'is-selected'
    }

    Tooltip.prototype = {
      bind: function() {
        var self = this
        this.$item.on({
          mouseenter: function() {
            self.show(this)
          },
          mouseleave: function() {
            self.hide(this)
          }
        })
      },
      show: function(el) {
        var $tooltip = $(el).find('.mg-about-tooltip')
        $(el).addClass(this.selectedClass)
        TweenMax.fromTo($tooltip, 0.36,  {opacity: 0, y: 16}, {opacity: 1, y: 0, ease: Back.easeOut.config(1.4), display: 'block'})
      },
      hide: function(el) {
        var $tooltip = $(el).find('.mg-about-tooltip')
        $(el).removeClass(this.selectedClass)
        TweenMax.to($tooltip, 0.2, {opacity: 0, y: 8, ease: Power2.easeOut, display: 'none'})
      }
    }

    return Tooltip
  })()

  $(function() {
    var tooltip = new Tooltip()

    tooltip.bind()
  })
})();