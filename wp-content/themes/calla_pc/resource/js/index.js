/*--------------------------
	Slider
--------------------------*/
$(function(){

  var $slider = $('.js-slider');
  var prev = $slider.data('prev');
  var next = $slider.data('next');
  var option = {
    auto: true,
    infiniteLoop: true,
    prevText: '<img src="'+tmpurl+prev+'" alt="prev">',
    nextText: '<img src="'+tmpurl+next+'" alt="next">',
    onSlideAfter: function() {
      slider.startAuto();
    }
  }
  slider = $slider.bxSlider(option);

});
