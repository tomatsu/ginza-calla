!(function() {
  'use strict'

  /**
   * ページ内スクロール
   */
  var smoothScroll = function() {
    $('a[href^="#"]').on('click', function() {
      console.log(1111)
      var href = $(this).attr('href')
      var target = href === '#' ? 0 : href

      // 要素がなかったら飛ばない
      if (href !== '#' && !$(target).length) {
        return false
      }

      TweenMax.to(window, 1, {scrollTo: target, ease:Power4.easeOut})

      return false
    })
  }

  $(function() {
    smoothScroll()
  })
})();