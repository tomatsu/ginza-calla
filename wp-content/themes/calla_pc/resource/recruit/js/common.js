
//mouseover -----------------------------------------------------------
//preloaded
preloadedImages = [];
function preloadImage(url){
	var p = preloadedImages;
	var l = p.length;
	p[l] = new Image();
	p[l].src = url;
}

$(function(){
var conf = {
	className : 'rollOver',
    postfix : '_ov'
  };
  $('.rollOver img').each(function(){
    this.originalSrc = this.src;
    this.rolloverSrc = this.originalSrc.replace(/(\.gif|\.jpg|\.png)/, conf.postfix+"$1");
    preloadImage(this.rolloverSrc);
  }).hover(function(){
    this.src = this.rolloverSrc;
  },function(){
    this.src = this.originalSrc;
  });

    $('#headerNavi a').nivoLightbox();
});

