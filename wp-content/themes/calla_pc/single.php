<?php get_template_part('header'); ?>
<?php breadcrumb(); ?>
  <div class="wrapper container">
    <div class="main">
<!-- コラムカテゴリーの時に読み込むテンプレート -->
      <?php if(is_category('column') || in_category('column') || in_category_child( get_term_by( 'slug', 'column', 'category' ))){  ?>
        <?php if(have_posts()): while(have_posts()):the_post(); ?>
          <?php
          $cat = get_the_category();
          $cat = $cat[0];
          if($cat->slug == '01'){
            $icon = 'dinner';
          }elseif ($cat->slug == '02') {
            $icon = 'talk';
          }elseif ($cat->slug == '03') {
            $icon = 'flag';
          }elseif ($cat->slug == '04') {
            $icon = 'book';
          }elseif ($cat->slug == '05') {
            $icon = 'building';
          }else{};
           ?>
        <div class="block-white">
          <h1 class="page-head icon-<?php echo $icon ?>-white"><span>銀座カラー</span><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></h1>
          <div class="page-body">
            <section class="block-article">
              <div class="block-article-head">
                <h2><?php the_title();?></h2>
                <div class="post-content-box">
                  <?php the_content();?>
                </div>
              </div>
              <div class="btn-share clearfix">
                <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                <div class="fb-share-button" data-href="<?php the_permalink();?>" data-layout="button"></div>
              </div>
              <div class="block-article-nav">
              	<ul>
                  <li class="prev"><?php previous_post_link('%link', '前の記事を読む','TRUE'); ?></li>
                	<li class="seperator">｜</li>
                  <li class="archive"><a href="<?php bloginfo('url'); ?>/column/<?php echo $cat->slug; ?>" class="trans">一覧を見る</a></li>
                	<li class="seperator">｜</li>
                	<li class="next"><?php next_post_link('%link', '次の記事を読む','TRUE'); ?></li>
                </ul>
              <!-- /.block-article-nav --></div>
            <!-- /.block-article --></section>
          <!-- /.page-body --></div>
      	<!-- /.block-white --></div>
        <?php endwhile; endif; ?>
      <?php }; ?>
<!-- コラムカテゴリーの時に読み込むテンプレートここまで -->

<!-- コンテンツカテゴリーの時に読み込むテンプレート -->
      <?php if(is_category('contents') || in_category('contents') || in_category_child( get_term_by( 'slug', 'contents', 'category' ))){  ?>
        <?php if(have_posts()): while(have_posts()):the_post(); ?>
          <?php
          $cat = get_the_category();
          $cat = $cat[0];
           ?>
        <div class="block-white">
          <?php if(get_field('html_heading') == ''){  ?>
            <h1 class="page-head"><span>銀座カラー</span><?php the_title();?></h1>
          <?php }else{ ?>
            <h1 class="page-head page-head-large"><span>銀座カラー</span><?php the_field('html_heading');?></h1>
          <?php }; ?>
          <div class="block-article-contents-head">
            <p class="block-article-contents-category"><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></p>
            <p class="block-article-contents-date"><?php the_date("Y.n.j"); ?></p>
          </div>
          <div class="page-body page-body-contents">
            <section class="block-article block-article-contents">
              <div class="block-article-head block-article-head-contents">
                <div class="post-contents-box">
                  <?php the_content();?>
                </div>
              </div>
              <div class="btn-share clearfix">
                <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                <div class="fb-share-button" data-href="<?php the_permalink();?>" data-layout="button"></div>
				<!-- <a href="http://b.hatena.ne.jp/entry/" class="hatena-bookmark-button" data-hatena-bookmark-layout="basic-label-counter" data-hatena-bookmark-lang="ja" title="このエントリーをはてなブックマークに追加"><img src="https://b.st-hatena.com/images/entry-button/button-only@2x.png" alt="このエントリーをはてなブックマークに追加" width="20" height="20" style="border: none;" /></a><script type="text/javascript" src="https://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async="async"></script> -->
              </div>
              <div class="block-article-nav">
              	<?php
              		$prev_post_link = get_field('prev_post_link');
              		$next_post_link = get_field('next_post_link');
              	 ?>
              	<ul>
              		<li class="prev">
              		<?php if($prev_post_link != ""){ ?>
             			<a href="<?php bloginfo('url'); ?>/contents/<?php echo $prev_post_link; ?>" rel="prev">前の記事を読む</a>
             		<?php }; ?>
             		</li>
                	<li class="seperator">｜</li>
                	<li class="archive"><a href="<?php bloginfo('url'); ?>/contents/<?php echo $cat->slug; ?>" class="trans">一覧を見る</a></li>
                	<li class="seperator">｜</li>
             		<li class="next">
             		<?php if($next_post_link != ""){ ?>
             			<a href="<?php bloginfo('url'); ?>/contents/<?php echo $next_post_link; ?>" rel="next">次の記事を読む</a>
             		<?php }; ?>
             		</li>
                </ul>
              <!-- /.block-article-nav --></div>
              <?php if(has_tag()){ ?>
              <div class="block-etc-contents block-etc-contents-tags">
                <h2 class="icon-cont icon-cont-tags">この記事に関するタグ</h2>
                <div class="">
                  <?php the_tags('','',''); ?>
                </div>
              </div>
              <?php }else{}; ?>
              <?php
                $relations = get_field('relation');
                if(empty($relations)){}else{
              ?>
              <div class="block-etc-contents block-etc-contents-relations">
                <h2 class="icon-cont icon-cont-post">関連記事</h2>
                <div class="contents-relations">
                  <?php
                    foreach($relations as $relation):setup_postdata($relation);
                  ?>
                    <a href="<?php echo get_permalink($relation); ?>">
                      <div class="contents-relations-left">
                        <img src="<?php echo catch_that_image_relation($relation); ?>" alt="">
                      </div>
                      <div class="contents-relations-right">
                        <h2><?php echo get_post($relation)->post_title; ?></h2>
                        <p><?php the_excerpt(); ?></p>
                      </div>
                    </a>
                  <?php endforeach; wp_reset_postdata(); ?>
                </div>
              </div>
              <?php }; ?>
              <div class="block-etc-contents block-etc-contents-category">
                <h2 class="icon-cont icon-cont-cate">すべてのカテゴリー</h2>
                <div class="">
                  <?php
                  $parent_id = get_category_by_slug("contents")->cat_ID;
                  $args = array(
                      'orderby'       => '',
                      'parent'        => $parent_id
                      );
                      $allcate = get_terms('category',$args);
                      foreach($allcate as $cates):
                  ?>
                  <a href="<?php echo get_term_link($cates->term_id,'category'); ?>"><?php echo $cates->name; ?></a>
                  <?php endforeach; wp_reset_postdata(); ?>
                </div>
              </div>
            <!-- /.block-article --></section>
          <!-- /.page-body --></div>
      	<!-- /.block-white --></div>
        <?php endwhile; endif; ?>
      <?php }; ?>
<!-- コンテンツカテゴリーの時に読み込むテンプレートここまで -->

<!-- ニュースカテゴリーの時に読み込むテンプレート -->
      <?php if(is_category('news') || in_category('news') || in_category_child( get_term_by( 'slug', 'news', 'category' ))){  ?>
        <div class="block-white">
          <h1 class="page-head"><span>銀座カラー</span><?php the_title(); ?></h1>
          <div class="page-body">
            <section class="block-article">
              <div class="block-article-head">
                <div class="post-content-box">
                  <?php the_content();?>
                </div>
              </div>
              <div class="btn-share clearfix">
                <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                <div class="fb-share-button" data-href="<?php the_permalink();?>" data-layout="button"></div>
              </div>
            <!-- /.block-article --></section>
          <!-- /.page-body --></div>
        <!-- /.block-white --></div>
      <?php }; ?>
<!-- ニュースカテゴリーの時に読み込むテンプレートここまで -->
      <?php if( !get_field('free_banner') ){ ?>
        <div class="mod-btn-counsel">
          <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0016" class="btn-stripe btn-shadow btn-counsel btn-counsel-large trans" target="_blank"><span>無料カウンセリング</span>ご予約はこちら</a>
        </div> <!-- /.mod-btn-counsel -->
      <?php }; ?>
    </div>
    <?php get_template_part('sidebar'); ?>
  </div>
<?php get_template_part('footer'); ?>
