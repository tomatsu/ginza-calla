<!-- columnページ -->
<?php if(is_category('column') || in_category('column') || in_category_child( get_term_by( 'slug', 'column', 'category' ))){ ?>
  <div class="sidebar-list">
    <p class="sidebar-list-head">脱毛や美容に関するコラム</p>
    <ul>
      <?php
        $columns = get_category_by_slug('column');
        $categories = get_categories( array('parent' => $columns->term_id,'orderby'=>'slug')) ;
        foreach($categories as $category) {
      ?>
      <li><a href="<?php echo get_category_link($category->term_id); ?>" class="trans"><?php echo $category -> name;?></a></li>
      <?php } ?>
    </ul>
  </div><!-- /.sidebar-list -->
<?php }; ?>
<!-- columnページここまで -->



<!-- newsページ -->
<?php if(is_category('news') || in_category('news') || in_category_child( get_term_by( 'slug', 'news', 'category' ))){ ?>
  <div class="sidebar-list">
    <p class="sidebar-list-head">お知らせ</p>
    <ul>
      <?php
        $newses = get_category_by_slug('news');
        $categories = get_categories( array('parent' => $newses->term_id,'orderby'=>'slug','order' => 'DESC')) ;
        foreach($categories as $category) {
      ?>
      <li><a href="<?php echo get_category_link($category->term_id); ?>" class="trans"><?php echo $category -> name;?></a></li>
      <?php } ?>
    </ul>
  </div><!-- /.sidebar-list -->
<?php }; ?>
<!-- newsページここまで -->
