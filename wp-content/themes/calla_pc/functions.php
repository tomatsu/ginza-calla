<?php
//==================================================================定数定義
define("CARE_PAGE_NUM", 25);//careページの記事の総数
define("ARRAY_BACK_NUM", 2);//careページの配列の戻りカウント

//==================================================================固定ページ投稿ページの自動<p><br>無効
remove_filter('the_content', 'wpautop');
remove_action('wp_head','rel_canonical');

//==================================================================概要（抜粋）の文字数、省略文字変更
function my_excerpt_length($length) {
	return 99;
}
add_filter('excerpt_length', 'my_excerpt_length');

function my_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'my_excerpt_more');


//==================================================================パーマリンクカテゴリ削除
add_filter('user_trailingslashit', 'remcat_function');
function remcat_function($link) {
    return str_replace("/category/", "/", $link);
}
add_action('init', 'remcat_flush_rules');
function remcat_flush_rules() {
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}
add_filter('generate_rewrite_rules', 'remcat_rewrite');
function remcat_rewrite($wp_rewrite) {
    $new_rules = array('(.+)/page/(.+)/?' => 'index.php?category_name='.$wp_rewrite->preg_index(1).'&paged='.$wp_rewrite->preg_index(2));
    $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
}
//==================================================================記事の最初の画像取得
function catch_that_image() {
    global $post, $posts;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    $first_img = $matches [1] [0];
    $first_img = str_replace('[theme_url]',bloginfo('template_url') . '/resource',$first_img);

    if(empty($first_img)){ //Defines a default image
      $first_img = "piyopiyo.jpg";
    };
    return $first_img;
}
function catch_that_image_relation($postdata) {
		$postdata = get_post($postdata);
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $postdata->post_content, $matches);
    $first_img = $matches [1] [0];
    $first_img = str_replace('[theme_url]',bloginfo('template_url') . '/resource',$first_img);

    if(empty($first_img)){ //Defines a default image
      $first_img = "piyopiyo.jpg";
    };
    return $first_img;
}
//==================================================================親固定ページの子ページ判定関数
//子ページのみ判定
function is_parent_slug() {
  global $post;
  if($post) {
    if ($post->post_parent) {
      $post_data = get_post($post->post_parent);
      return $post_data->post_name;
    }
  }
}
//親子まとめて判定
function is_page_group($page){
  if (is_page($page) || is_parent_slug() === $page ){
    return true;
  }else{
    return false;
  }
}

//==================================================================子カテゴリ判定関数
function in_category_child( $cats, $_post = null ){
  foreach ( (array) $cats as $cat ) {
    $descendants = get_term_children( (int) $cat, 'category');
    if ( $descendants && in_category( $descendants, $_post ) )
      return true;
    }
      return false;
}

//==================================================================ショートコード
//サイトURL
function homeUrl() {
    return get_bloginfo('url');
}
add_shortcode('site_url', 'homeUrl');
//テンプレートURL
function tmpUrl() {
    return get_bloginfo('template_url') . "/resource";
}
add_shortcode('theme_url', 'tmpUrl');
//現在ページ
function pageUrl() {
    return get_permalink();
}
add_shortcode('page_url', 'pageUrl');
//親ページ
function parentUrl() {
		return get_page_link(get_post($post->post_parent)->post_parent);
}
add_shortcode('parent_url', 'parentUrl');
//==================================================================パンくずリスト
function breadcrumb(){
    global $post;
    $str ='';
    $cnt = 2;
    if(!is_home()&&!is_admin()){
        $str.= '<div class="breadcrumb"><ul class="container" itemscope itemtype="https://schema.org/BreadcrumbList">';
        $str.= '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a href="'. home_url() .'" itemprop="item"><span itemprop="name">脱毛の銀座カラーTOP</span></a><meta itemprop="position" content="1" /></li><li class="bread-arrow">&nbsp;&gt;&nbsp;</li>';

        if(is_category()) {
            $cat = get_queried_object();
            if($cat -> parent != 0){
                $ancestors = array_reverse(get_ancestors( $cat -> cat_ID, 'category' ));
                foreach($ancestors as $ancestor){
										if (get_field('cat_meta_title','category_'.$ancestor) == ''){
											$title = get_cat_name($ancestor);
										}else{
											$title = get_field('cat_meta_title','category_'.$ancestor);
										}
                    $str.='<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a href="'. get_category_link($ancestor) .'" itemprop="item"><span itemprop="name">'. $title .'</span></a></li><li class="bread-arrow">&nbsp;&gt;&nbsp;</li>';
                }
								$current_category = get_queried_object();
							  $catid = $current_category->term_id;
							  if (get_field('cat_meta_title','category_'.$catid) == ''){
							    $title = wp_title('', false);
							  }else{
							    $title = get_field('cat_meta_title','category_'.$catid);
							  }
                $str.='<li>'. $title .'</li>';
            }else{
							$current_category = get_queried_object();
						  $catid = $current_category->term_id;
						  if (get_field('cat_meta_title','category_'.$catid) == ''){
						    $title = wp_title('', false);
						  }else{
						    $title = get_field('cat_meta_title','category_'.$catid);
						  }
              $str.='<li>'. $title .'</li>';
            }
        } elseif(is_page()){
            if($post -> post_parent != 0 ){
                $ancestors = array_reverse(get_post_ancestors( $post->ID ));
                foreach($ancestors as $ancestor){
										if(get_the_title($ancestor) == 'エリア'){
											continue;
										}
										if(is_page_group('company')){}else{
											$str.='<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a href="'. get_permalink($ancestor).'" itemprop="item"><span itemprop="name">'. get_the_title($ancestor) .'</span></a><meta itemprop="position" content="' . $cnt . '" /></li><li class="bread-arrow">&nbsp;&gt;&nbsp;</li>';
                      $cnt++;
                    };
                }

                if (get_field('parent_area')) {
									$field = get_field_object('parent_area_name');
									$value = get_field('parent_area_name');
                   $str.='<li><a href="' . get_bloginfo('url') . '/salon/area/' . $value . '">'. $field["choices"][$value] .'</a></li><li class="bread-arrow">&nbsp;&gt;&nbsp;</li>';
                }
								if (get_field('bread_name') != ''){
									$str.='<li>' . get_field('bread_name') . '</li>';
								}else{
									$str.='<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span itemprop="name">'. wp_title('', false) .'</span><meta itemprop="position" content="' . $cnt . '" /></li>';
								}
            }else{
              $str.='<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span itemprop="name">'. wp_title('', false) .'</span><meta itemprop="position" content="' . $cnt . '" /></li>';
            }
        } elseif(is_single()){
            $categories = get_the_category($post->ID);
            $cat = $categories[0];
            if($cat -> parent != 0){
                $ancestors = array_reverse(get_ancestors( $cat -> cat_ID, 'category' ));
                foreach($ancestors as $ancestor){
                    $contents_name = get_cat_name($ancestor);
                    if (get_field('cat_meta_title','category_'.$ancestor) != ""){
                        $contents_name = get_field('cat_meta_title','category_'.$ancestor);
                    };
                    $str.='<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a href="'. get_category_link($ancestor).'" itemprop="item"><span itemprop="name">'. $contents_name . '</span></a><meta itemprop="position" content="' . $cnt . '" /></li><li class="bread-arrow">&nbsp;&gt;&nbsp;</li>';
                    $cnt++;
                }
                $category_name = $cat-> cat_name;
                if (get_field('cat_meta_title','category_'.$cat -> cat_ID) != ""){
                        $category_name = get_field('cat_meta_title','category_'.$cat -> cat_ID);
                    };
                $str.='<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a href="'. get_category_link($cat -> term_id). '" itemprop="item"><span itemprop="name">'. $category_name . '</span></a><meta itemprop="position" content="' . $cnt . '" /></li><li class="bread-arrow">&nbsp;&gt;&nbsp;</li>';
                $cnt++;
                $str.='<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span itemprop="name">'. wp_title('', false) .'</span><meta itemprop="position" content="' . $cnt . '" /></li>';
            }else{
              $str.='<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span itemprop="name">'. wp_title('', false) .'</span><meta itemprop="position" content="' . $cnt . '" /></li>';
            }
        } elseif(is_tag()){
						$contents_name = get_category_by_slug("contents")->cat_ID;
						$contents_name = get_field('cat_meta_title','category_'.$contents_name);
						$current_category = get_queried_object();
						$catid = $current_category->term_id;
						if (get_field('cat_meta_title','post_tag_'.$catid) == ''){
							$title = wp_title('', false);
						}else{
							$title = get_field('cat_meta_title','post_tag_'.$catid);
						}
            $str.='<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a href="'. home_url() .'/contents/" itemprop="item"><span itemprop="name">'.$contents_name.'</span></a><meta itemprop="position" content="' . $cnt . '" /></li><li class="bread-arrow">&nbsp;&gt;&nbsp;</li>';
            $cnt++;
            $str.='<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span itemprop="name">'. wp_title('', false) .'</span><meta itemprop="position" content="' . $cnt . '" /></li>';
        } else{
            $str.='<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span itemprop="name">'. $title .'</span><meta itemprop="position" content="' . $cnt . '" /></li>';
        }
        $str.='</ul></div>';
    }
    echo $str;
}

function get_acf_field( $group, $name ) {
    $target_field = array();
    $found_posts = get_posts( array('post_type'=>'acf') );
    foreach( $found_posts as $p ) {
        if( $p->post_title == $group ) {
            $acf_id = $p->ID;
            break;
        }
    }
    if( $acf_id ) {
        $fields = get_post_meta($acf_id);
        foreach( $fields as $f ) {
            $meta = unserialize($f[0]);
            if( $meta['name'] == $name ) {
                $target_field = $meta;
                break;
            }
        }
    }
    return $target_field;
}


//==================================================================管理画面編集
function my_login_stylesheet() {
wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/style.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

function add_page_columns_name($columns) {
$columns['slug'] = "スラッグ";
return $columns;
}
function add_page_column($column_name, $post_id) {
if( $column_name == 'slug' ) {
$post = get_post($post_id);
$slug = $post->post_name;
echo attribute_escape($slug);
}
}
add_filter( 'manage_pages_columns', 'add_page_columns_name');
add_action( 'manage_pages_custom_column', 'add_page_column', 10, 2);

function remove_menus () {
    global $menu;
    unset($menu[10]); // メディア
    unset($menu[25]); // コメント
}
add_action('admin_menu', 'remove_menus');


// URL末尾スラッシュ
function add_slash_uri_end($uri, $type) {
  if (($type != 'single')) {
		if(is_parent_slug() != 'about'){
		if(is_parent_slug() != 'campaign'){
		if(is_parent_slug() != 'care'){
		if(is_parent_slug() != 'company'){
		if(is_parent_slug() != 'faq'){
		if(is_parent_slug() != 'report'){
		if(is_parent_slug() != 'recruit'){
			$uri = trailingslashit($uri);
		}
		}
		}
		}
		}
		}
		}
  }
  return $uri;
}
add_filter('user_trailingslashit', 'add_slash_uri_end', 10, 2);

//管理画面のタグレイアウト変更

function re_register_post_tag_taxonomy() {
global $wp_rewrite;
$rewrite = array(
'slug' => get_option('tag_base') ? get_option('tag_base') : 'tag',
'with_front' => ! get_option('tag_base') || $wp_rewrite->using_index_permalinks(),
'ep_mask' => EP_TAGS,
);

$labels = array(
'name' => _x( 'Tags', 'taxonomy general name' ),
'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
'search_items' => __( 'Search Tags' ),
'popular_items' => __( 'Popular Tags' ),
'all_items' => __( 'All Tags' ),
'parent_item' => null,
'parent_item_colon' => null,
'edit_item' => __( 'Edit Tag' ),
'view_item' => __( 'View Tag' ),
'update_item' => __( 'Update Tag' ),
'add_new_item' => __( 'Add New Tag' ),
'new_item_name' => __( 'New Tag Name' ),
'separate_items_with_commas' => __( 'Separate tags with commas' ),
'add_or_remove_items' => __( 'Add or remove tags' ),
'choose_from_most_used' => __( 'Choose from the most used tags' ),
'not_found' => __( 'No tags found.' )
);

register_taxonomy( 'post_tag', 'post', array(
'hierarchical' => true,
'query_var' => 'tag',
'rewrite' => $rewrite,
'public' => true,
'show_ui' => true,
'show_admin_column' => true,
'_builtin' => true,
'labels' => $labels
) );
}
add_action( 'init', 're_register_post_tag_taxonomy', 1 );

// パーマリンクカテゴリ削除
// ページネーション機能
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');

function rel_next_prev_link_tags() {
    global $paged;
    $column_group = in_category_child(get_term_by( 'slug', 'column', 'category'));
    $contents_group = in_category_child( get_term_by( 'slug', 'contents', 'category' ));
    $not_news = !in_category('news');
    $not_column = !is_category('column');
    $not_contents = !is_category('contents');
    if(( $column_group || $contents_group ) && $not_news && $not_column && $not_contents ){
        if ( get_previous_posts_link() ){
          echo '<link rel="prev" href="'.get_pagenum_link( $paged - 1 ).'" />'.PHP_EOL;
        }
        if ( get_next_posts_link() ){
          echo '<link rel="next" href="'.get_pagenum_link( $paged + 1 ).'" />'.PHP_EOL;
        }
    }
}
add_action( 'wp_head', 'rel_next_prev_link_tags' );



function pagination($pages = '', $range = 2) {
    $showitems = ($range * 2)+1;
    global $paged;
    if( empty($paged) ) $paged = 1;

    if( $pages == '' ) {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if( !$pages ) {
            $pages = 1;
        }
    }


    if( 1 != $pages ) {
        echo "<div class='pagination-box'><div class='pagination'>";
        if ($paged != 1){
         echo "<a href='".get_pagenum_link($paged - 1)."' class='btn-stripe-left btn-stripe-paging'>前へ</a>";
        }else{
         echo "<span class='btn-stripe-left btn-stripe-paging btn-disabled'>前へ</span>";
        };
        echo "<span class='pagination-box-count'>".$paged."/".$pages."</span>";
        if ($paged != $pages){
         echo "<a href='".get_pagenum_link($paged + 1)."' class='btn-stripe btn-stripe-paging'>次へ</a>";
        }else{
         echo "<span class='btn-stripe btn-stripe-paging btn-disabled'>次へ</span>";
        };
        echo "</div></div>\n";
    }
}

?>
