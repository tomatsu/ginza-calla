<?php get_template_part('header'); ?>

<main class="pc-main">
  <div class="pc_index">
    <section id="key_img">
      <ul>
        <li><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/main.png?191225" alt="脱毛なんて早く終わらせてその分、好きなことをしよう！8ヵ月卒業プラン月額5,200円税抜"></li>
      </ul>
    </section>
    <section id="cont01" class="cont inview_block">
      <div class="inner">
        <h2 class="hannari_font inview_item inview_item1">
          <span class="century_font">Campaign</span>
          割引キャンペーン
        </h2>
        <ul class="inview_item inview_item2">
          <li><a href="/gakuwari/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜キャンペーン情報_ペア割'});"><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/bnr04.svg" alt="脱毛学割10,000円割引"></a></li>
          <li><a href="/campaign/shokai.html" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜キャンペーン情報_のりかえ割'});"><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/bnr01.svg" alt="おともだち紹介ご紹介で入会された方に10,000円割引ご紹介してくれた方にギフトカード15,000円分プレゼント！"></a></li>
          <li><a href="/campaign/waribiki.html#pair-cp" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜キャンペーン情報_脱毛学割'});"><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/bnr02.svg" alt="ペア割計20,000円割引"></a></li>
          <li><a href="/campaign/waribiki.html#norikae-cp" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜キャンペーン情報_おともだち紹介'});"><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/bnr03.svg" alt="のりかえ割10,000円割引"></a></li>
        </ul>
      </div>
    </section>
    <section id="cont02" class="cont">
      <div class="inner inview_block">
        <h2 class="hannari_font inview_item inview_item1">
          <span><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/cont02_img.svg?19122602" alt="最後の一本まで一生ムダ毛0"></span>
          <span class="em">銀座カラーの全身脱毛は、ムダ毛0まで<br>スピーディ。しかもお財布にもやさしい！</span>
        </h2>
        <div class="lead">
          <div>
            <span class="hannari_font inview_item inview_item2 strong">サクッと終わって<span class="em">、</span>ずっとスベスベ！</span>
            <ol class="inview_item inview_item3">
              <li>早く</li>
              <li>安く</li>
              <li>最後の１本まで</li>
            </ol>
            <p class="inview_item inview_item4">
              脱毛をもっと身近に、もっと手軽にするために。銀座カラーは先進のマシンとプロの技術で、施術時間を大幅に短縮。その分費用はググっとおトクに。最後の1本まで残さず脱毛するから、理想的なスベスベ肌が叶います！ ※個人差がございます。
            </p>
          </div>
        </div>
        <p class="img inview_item inview_item5"><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/cont02_img02.svg" alt="いつでもお客様ファースト！"></p>
      </div>
    </section>
    <section id="cont03" class="cont">
      <div class="min_cont min_cont01 inview_block">
        <div class="inner">
          <div class="bg inview_item inview_item1">
            <div class="box">
              <span class="inview_item inview_item2 strong"><span class="em">料金体系</span></span>
              <div>
                <h4 class="inview_item inview_item3"><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/cont03_text01.svg" alt="適正価格の安心と、料金以上の満足を。"></h4>
                <p class="inview_item inview_item4">
                  適正価格は当たり前。私たちが目指すのは、価格以上の価値でお客様にもっと喜んでもらうこと。そのために、顔脱毛もついたおトクな料金プランを新たにご用意しました！
                </p>
              </div>
              <p class="link inview_item inview_item5"><a href="/plan/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜TOP_料金体系を詳しく見る'});">料金体系を詳しく見る</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="min_cont min_cont02 inview_block">
        <div class="inner">
          <div class="bg inview_item inview_item1">
            <div class="box">
              <span class="inview_item inview_item2 strong"><span class="em">予約の<br>とりやすさ</span></span>
              <div>
                <h4 class="inview_item inview_item3"><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/cont03_text02.svg" alt="行きたいときに行ける、予約できない０宣言。"></h4>
                <p class="inview_item inview_item4">
                  行きたい時に行けないなんて、ストレスになるし脱毛サイクルに合わせられないのもマイナスです。だから、銀座カラーは予約枠の確保にとにかく本気。初めのご来店時に6回分すべての予約をとることができるので、自分の行きたいタイミングで通えます。忙しい人でも安心！
                </p>
              </div>
              <p class="link inview_item inview_item5"><a href="/about/reservation.html" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜TOP_予約のとりやすさを詳しく見る'});">予約のとりやすさを詳しく見る</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="min_cont min_cont03 inview_block">
        <div class="inner">
          <div class="bg inview_item inview_item1">
            <div class="box">
              <span class="inview_item inview_item2 strong"><span class="em">通い<br>やすさ</span></span>
              <div>
                <h4 class="inview_item inview_item3"><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/cont03_text03.svg" alt="「通う」を楽しみに変えるおもてなし精神を隅々に。"></h4>
                <p class="inview_item inview_item4">
                  通わなきゃ…という義務感よりも、キレイになる喜びを感じてほしい！だから、銀座カラーには楽しく通ってもらうための工夫がいっぱい。安心の担当者制度や一人では難しい箇所のシェービングサービスなど、新たな取り組みにチャレンジしています！
                </p>
              </div>
              <p class="link inview_item inview_item5"><a href="/feature/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜TOP_通いやすさを詳しく見る'});">通いやすさを詳しく見る</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="min_cont min_cont04 inview_block">
        <div class="inner">
          <div class="bg inview_item inview_item1">
            <div class="box">
              <span class="inview_item inview_item2 strong"><span class="em">脱毛結果</span></span>
              <div>
                <h4 class="inview_item inview_item3"><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/cont03_text04.svg" alt="教育時間を最大限に。だから結果も最大限。"></h4>
                <p class="inview_item inview_item4">脱毛サロンで大切なのは、なんといっても結果です。この結果の裏にあるのは徹底したスタッフ教育。銀座カラーでは、技術研修とマナー研修の後、厳しい最終テストに合格したスタッフのみが、お客様を担当します！</p>
              </div>
              <p class="link inview_item inview_item5"><a href="/result/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜TOP_脱毛結果を詳しく見る'});">脱毛結果を詳しく見る</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="min_cont min_cont05 inview_block">
        <div class="inner">
          <div class="bg inview_item inview_item1">
            <div class="box">
              <span class="inview_item inview_item2 strong"><span class="em">脱毛への<br>ポリシー</span></span>
              <div>
                <h4 class="inview_item inview_item3"><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/cont03_text05.svg" alt="今も昔も変わらない「お客様ファースト」"></h4>
                <p class="inview_item inview_item4">銀座カラーが創業時から大切にしているのは、お客様とまっすぐに向き合うこと。どんな脱毛サロンならベストか、一人ひとりのお客様のお声に耳を傾けながら、一つひとつのお声をカタチにしてきました。これからも偽りなく、正直に、お客様と向きあっていきます！</p>
              </div>
              <p class="link inview_item inview_item5"><a href="/about/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜TOP_脱毛へのポリシーを詳しく見る'});">脱毛へのポリシーを詳しく見る</a></p>
            </div>
          </div>
        </div>
      </div>
      <p class="link"><a class="inview_item inview_item6" href="https://reserve.ginza-calla.jp/form/Reservations?k=0016" target="_blank">無料カウンセリング予約</a></p>
    </section>
    <section id="cont04" class="cont">
      <div class="inner inview_block">
        <h2 class="inview_item inview_item1">
          <span class="century_font">About Us</span>
          銀座カラーについて
        </h2>
        <ul>
          <li class="inview_item inview_item2"><a href="/plan/#cont03" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜TOP_キャンペーン'});"><span><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/cont04_img01.jpg" alt="キャンペーン"></span><em>キャンペーン</em></a></li>
          <li class="inview_item inview_item3"><a href="/salon/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜TOP_店舗一覧'});"><span><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/cont04_img02.jpg" alt="店舗一覧"></span><em>店舗一覧</em></a></li>
          <li class="inview_item inview_item4"><a href="/faq/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜TOP_Q＆A'});"><span><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/cont04_img03.jpg" alt="Q＆A"></span><em>Q＆A</em></a></li>
          <li class="inview_item inview_item5"><a href="/adgallery/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜TOP_広告ギャラリー'});"><span><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/index/cont04_img04.jpg" alt="広告ギャラリー"></span><em>広告ギャラリー</em></a></li>
        </ul>
      </div>
    </section>
    <section id="cont05" class="cont">
      <div class="inner inview_block">
        <h3 class="hannari_font inview_item inview_item1">
          <span>INFORMATION</span>
          銀座カラーからのお知らせ
        </h3>
        <div class="scroll inview_item inview_item2">
          <ul>
            <?php
                $posts = get_posts("posts_per_page=6&category_name=news&orderby=date");
                foreach($posts as $post):
                    setup_postdata($post);
                    $cat = get_the_category();
                    $cat = $cat[0];
                    $cat_slug = $cat->category_nicename;
            ?>
            <li>
              <span><?php the_date("Y.n.j"); ?></span>
              <p> <a href="<?php bloginfo('url') ?>/news/<?php echo $cat_slug ?>#<?php the_ID(); ?>" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜TOP_各お知らせ記事'});"><?php the_title(); ?></a></p>
            </li>
            <?php endforeach; ?>
          </ul>
          <p class="link">
            <a href="/news/">More ></a>
          </p>
        </div>
      </div>
    </section>
  </div>
</main>

<?php get_template_part('footer'); ?>
