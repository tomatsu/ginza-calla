<footer class="mg-footer">
    <p class="mg-footer-tel"><a href="tel:0120360286" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜フッター_電話予約'});"><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/footer/tel.svg" alt="[Free Call]0120-360-286[サロン にハロー] 平日/12:00〜21:00 土日祝/10:00〜19:00"></a></p>
    <ul class="mg-footer-share">
        <li><a href="https://www.facebook.com/ginza.calla" target="_blank" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜フッター_Facebook'});"><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/footer/icon_facebook.svg" alt="Facebook" width="40" height="40"></a></li>
        <li><a href="https://www.instagram.com/ginzacalla_official/" target="_blank" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜フッター_Instagram'});"><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/footer/icon_instagram.svg" alt="Instagram" width="40" height="40"></a></li>
        <li><a href="https://twitter.com/ginza_calla_" target="_blank" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜フッター_Twitter'});"><img src="<?php bloginfo('template_url') ?>/resource/migrate/images/footer/icon_twitter.svg" alt="Twitter" width="40" height="40"></a></li>
    </ul>
    <nav class="mg-footer-nav">
        <ul class="mg-footer-menu">
            <li><a href="/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜フッター_HOME'});">HOME</a></li>
            <li><a href="/recruit/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜フッター_採用情報'});">採用情報</a></li>
            <li><a href="/company/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜フッター_会社概要'});">会社概要</a></li>
            <li><a href="https://reserve.ginza-calla.jp/contact/CallaContacts/" target="_blank" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜フッター_お問い合わせ'});">お問い合わせ</a></li>
            <li><a href="/company/sitemap" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜フッター_サイトマップ'});">サイトマップ</a></li>
            <li><a href="/company/policy" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜フッター_プライバシーポリシー'});">プライバシーポリシー</a></li>
        </ul>
    </nav>
    <ul class="mg-footer-banners">
        <li><a href="https://www.plan-international.jp/girl/" target="_blank" rel="nofollow" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜フッター_I am a girl'});"><img src="https://www.plan-international.jp/common/img/bnr_girl_l.gif" alt="Because I am a Girl 世界の女の子たちに、生きていく力を　国際NGOプラン・インターナショナル" border="0" /></a></li>
    </ul>
    <p class="mg-footer-copyright"><small>copyright &copy; Ginza Calla All Rights Reserved.</small></p>
</footer>

    <script type="text/javascript">
      tmpurl = "<?php bloginfo('template_url'); ?>/";
    </script>
    <?php get_template_part('import','js'); ?>
    <!-- /Yahoo! タグマネージャ -->
  <!--<script type="text/javascript">
    (function () {
      var tagjs = document.createElement("script");
      var s = document.getElementsByTagName("script")[0];
      tagjs.async = true;
      tagjs.src = "//s.yjtag.jp/tag.js#site=tdfgAp5";
      s.parentNode.insertBefore(tagjs, s);
    }());
  </script>
  <noscript>
    <iframe src="//b.yjtag.jp/iframe?c=tdfgAp5" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
  </noscript>-->
  <!-- Google Tag Manager -->
  <!--<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N9H8R5"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <script>
  	(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  	})(window,document,'script','dataLayer','GTM-N9H8R5');
  </script>-->
  <!-- End Google Tag Manager -->
  <!--<script type="text/javascript" src="/galib_gc.js"></script>-->



  <!-- 191017_Yahoo-Geniee -->
  <script type="text/javascript">
    (function () {
      var tagjs = document.createElement("script");
      var s = document.getElementsByTagName("script")[0];
      tagjs.async = true;
      tagjs.src = "//s.yjtag.jp/tag.js#site=82r2Wc4,tdfgAp5&referrer=" + encodeURIComponent(document.location.href) + "";
      s.parentNode.insertBefore(tagjs, s);
    }());
  </script>
  <noscript>
    <iframe src="//b.yjtag.jp/iframe?c=82r2Wc4,tdfgAp5" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
  </noscript>
  <!-- //191017_Yahoo-Geniee -->

  <!-- Google Tag Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N9H8R5"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-N9H8R5');</script>
  <!-- End Google Tag Manager -->
  <?php wp_footer(); ?>
 <!-- アイビス -->
<script>
var _chaq = _chaq || [];
_chaq['_accountID']=1213;
(function(D,s){
    var ca = D.createElement(s)
    ,ss = D.getElementsByTagName(s)[0];
    ca.type = 'text/javascript';
    ca.async = !0;
    ca.setAttribute('charset','utf-8');
    var sr = 'https://st.aibis.biz/aibis.js';
    ca.src = sr + '?' + parseInt((new Date)/60000);
    ss.parentNode.insertBefore(ca, ss);
})(document,'script');
</script>
 <!-- /アイビス -->
 <?php 
 if (is_page('9243')):
 ?>
 <script type="text/javascript">
   var aldGinzaCallaDepilationSimulator = {};
  (function () {
     aldGinzaCallaDepilationSimulator.addElementJsUrl = '<?php bloginfo('template_url') ?>/resource/migrate/js/ginza_calla_depilation_simulator/js/add_element.js';
     aldGinzaCallaDepilationSimulator.simulatorHtmlUrl = '<?php bloginfo('template_url') ?>/resource/migrate/js/ginza_calla_depilation_simulator/simulator.html';
     aldGinzaCallaDepilationSimulator.simulatorImageUrlBase = '<?php bloginfo('template_url') ?>/resource/migrate/js/ginza_calla_depilation_simulator/img';
     var scriptTag = document.createElement("script");
     scriptTag.setAttribute("type","text/javascript");
     scriptTag.setAttribute("src",aldGinzaCallaDepilationSimulator.addElementJsUrl);
     document.getElementsByTagName("head")[0].appendChild(scriptTag);
  }());
 </script>
 <?php 
  endif;
 ?>
  </body>
</html>