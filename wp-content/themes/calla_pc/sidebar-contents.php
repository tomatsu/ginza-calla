  <div class="sidebar-list">
    <p class="sidebar-list-head">カテゴリー一覧</p>
    <ul>
      <?php
        $columns = get_category_by_slug('contents');
        $categories = get_categories( array('parent' => $columns->term_id,'orderby'=>'')) ;
        foreach($categories as $category) {
      ?>
      <li><a href="<?php echo get_category_link($category->term_id); ?>" class="trans"><?php echo $category -> name;?></a></li>
      <?php } ?>
    </ul>
  </div><!-- /.sidebar-list -->
