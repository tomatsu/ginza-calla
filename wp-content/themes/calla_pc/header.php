<?php
if (is_page('area')){
  $url = get_bloginfo('url') . '/salon';
  header("Location: {$url}");
  exit;
};
if (is_page('premium')){
  $url = get_bloginfo('url') . '/plan/course/zenshin/';
  header("Location: {$url}");
  exit;
};
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">

<!-- 0720 googlemapAPI -->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCk7O_Ioiv5AFDtWJTODg1hiU9Kng76tMk"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKHfmp22QBX2BukRJsQkIAENWqPeAOIfI"></script>
<!-- 0720 googlemapAPI  -->


    <?php if(is_home()){ ?>
      <title>脱毛なら美容脱毛サロン「銀座カラー」</title>
    <?php }else if(is_page()){ ?>
      <?php if(get_field('_aioseop_title')){ ?>
        <?php
          $anc = $post->ancestors;
          if($anc){
            $slug = get_page($anc[0])->post_name;
          }
        ?>
        <?php if(is_page('gakuwari')){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else if(is_page_group('care')){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else if(is_page_group('plan')){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else if(is_page_group('salon')){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else if(is_page_group('campaign')){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else if(is_parent_slug() == 'part'){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else if(is_parent_slug() == 'course'){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else{ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }; ?>
      <?php }else{ ?>
        <?php if(is_page('gakuwari')){ ?>
          <title>学生割サービス「脱毛学割」｜美容脱毛サロン「銀座カラー」</title>
        <?php }else{ ?>
          <title><?php the_title(); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }; ?>
      <?php }; ?>
    <?php }else if(is_single()){ ?>
      <?php
        $category = get_the_category();
        $cat_name = $category[0]->cat_name;
        $cat_parent = $category[0]->category_parent;
        $cat_parent = get_category($cat_parent);
        $cat_parent = $cat_parent->slug;
      ?>
      <?php if($cat_parent == 'column'){ ?>
        <?php if(get_field('_aioseop_title')){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜脱毛コラム｜美容脱毛サロン「銀座カラー」</title>
        <?php }else{ ?>
          <title><?php the_title(); ?>｜脱毛コラム｜美容脱毛サロン「銀座カラー」</title>
        <?php }; ?>
      <?php }else{ ?>
        <?php if(get_field('_aioseop_title')){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else{ ?>
          <title><?php the_title(); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }; ?>
      <?php }; ?>
    <?php }else if(is_category()){ ?>
      <?php if(is_category('column')){ ?>
        <title>脱毛コラム｜美容脱毛サロン「銀座カラー」</title>
      <?php }else if(is_category('news')){ ?>
        <title>お知らせ｜美容脱毛サロン「銀座カラー」</title>
      <?php }else{ ?>
        <?php
          $category = get_the_category();
          $cat_name = $category[0]->cat_name;
          $cat_parent = $category[0]->category_parent;
          $cat_parent = get_category($cat_parent);
          $cat_parent = $cat_parent->slug;
        ?>
        <?php if($cat_parent == 'column'){ ?>
          <title><?php echo $cat_name; ?>｜脱毛コラム｜美容脱毛サロン「銀座カラー」</title>
        <?php }else if($cat_parent == 'news'){ ?>
          <title><?php echo $cat_name; ?>のお知らせ｜美容脱毛サロン「銀座カラー」</title>
        <?php }else{ ?>
          <?php
            $current_category = get_queried_object();
            $catid = $current_category->term_id;
            if (get_field('cat_meta_title','category_'.$catid) == ''){
              $title = $current_category->name;
            }else{
              $title = get_field('cat_meta_title','category_'.$catid);
            }
           ?>
          <title><?php echo $title; ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }; ?>
      <?php }; ?>
    <?php }else if(is_tag()){ ?>
      <?php
        $current_category = get_queried_object();
        $catid = $current_category->term_id;
        if (get_field('cat_meta_title','post_tag_'.$catid) == ''){
          $title = $current_category->name;
        }else{
          $title = get_field('cat_meta_title','post_tag_'.$catid);
        }
       ?>
      <title><?php echo $title; ?>｜美容脱毛サロン「銀座カラー」</title>
    <?php }else{ ?>
        <title><?php bloginfo('name'); ?></title>
    <?php }; ?>
    <meta property="og:description" content="銀座カラーは、満足度No.1を追求する女性専用の美容脱毛サロンです。銀座カラーの脱毛は痛みが少なく安全・効果の高いIPL脱毛を採用。お客様ひとりひとりに合った脱毛ケアで赤ちゃんのような素肌へ。丁寧なケアをお約束する脱毛サロンです。">
    <meta property="og:site_name" content="脱毛の銀座カラー">
    <meta property="og:image" content="<?php bloginfo('template_url'); ?>/resource/img/common/head-logo-L.png">
    <meta property="og:type" content="article">
    <meta property="og:locale" content="ja_JP">
    <meta property="og:title" content="脱毛なら美容脱毛サロン「銀座カラー」">
    <meta property="og:url" content="https://ginza-calla.jp">
    <meta property="fb:app_id" content="733790480086051">
    <meta property="fb:admins" content="347882235370392">
    <?php if(is_home()){ ?>
      <meta name="keywords" content="脱毛,サロン,専門店,全身,予約,銀座カラー,女性専用">
      <meta name="description" content="銀座カラーは、満足度No.1を追求する女性専用の美容脱毛サロンです。銀座カラーの脱毛は痛みが少なく安全・効果の高いIPL脱毛を採用。お客様ひとりひとりに合った脱毛ケアで赤ちゃんのような素肌へ。丁寧なケアをお約束する脱毛サロンです。">
    <?php }; ?>
    <!-- End Meta Data -->
    <?php if(is_home()){ ?>
      <link rel="canonical" href="<?php bloginfo('url'); ?>">
    <?php }elseif(is_category('2020')){ ?>
      <link rel="canonical" href="<?php bloginfo('url'); ?>/news/">
    <?php }elseif(is_category()){ ?>
      <link rel="canonical" href="<?php echo get_category_link(get_query_var('cat')); ?>">
    <?php }elseif(is_tag()){ ?>
      <?php foreach (get_the_tags() as $tag):?>
        <link rel="canonical" href="<?php echo get_tag_link($tag->term_id); ?>">
      <?php endforeach; ?>
    <?php }else{ ?>
      <link rel="canonical" href="<?php the_permalink(); ?>">
    <?php }; ?>
    <!-- End Annotation -->
    <?php get_template_part('import','css'); ?>

    <?php if(is_page_group('product')){  ?>
      <!-- YAHOO! ＹＤＮサイトリターゲティングタグ-sep -->
      <!-- Yahoo Code for your Target List -->
      <script type="text/javascript" language="javascript">
      /* <![CDATA[ */
      var yahoo_retargeting_id = 'KESY5A3IWB';
      var yahoo_retargeting_label = '';
      var yahoo_retargeting_page_type = '';
      var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '', quantity: ''}];
      /* ]]> */
      </script>
      <script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
      <!-- //YAHOO! ＹＤＮサイトリターゲティングタグ-sep// -->
    <?php }; ?>
    <?php if(get_field('qa_meta_keywords')){ ?>
      <meta name="keywords" itemprop="keywords" content="<?php the_field('qa_meta_keywords') ?>">
    <?php }; ?>
    <?php if(is_category()){ ?>
      <?php
        $cat_id = get_queried_object()->cat_ID;
        $post_id = 'category_'.$cat_id;
      ?>
      <?php if(get_field('cat_meta_keywords',$post_id)){ ?>
        <meta name="keywords" itemprop="keywords" content="<?php the_field('cat_meta_keywords',$post_id) ?>">
      <?php }; ?>
    <?php }; ?>
    <?php if(is_tag()){ ?>
      <?php foreach (get_the_tags() as $tag):?>
        <?php $tag_id = 'post_tag_'.$tag->term_id;  ?>
        <meta name="keywords" itemprop="keywords" content="<?php the_field('cat_meta_keywords',$tag_id) ?>">
      <?php endforeach; ?>
    <?php }; ?>
    <?php wp_head(); ?>
  </head>
  <body>
    <div id="fb-root"></div>
    <script>
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.4";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>


    <?php
        $navItems = [
            ['title' => 'TOPページ', 'slug' => 'top',       'href' => '/', ],
            ['title' => '料金体系',   'slug' => 'plan', 'href' => '/plan/', ],
            ['title' => '予約のとりやすさ',   'slug' => 'reservation',        'href' => '/about/reservation.html', ],
            ['title' => '通いやすさ',             'slug' => 'feature',         'href' => '/feature/', ],
            ['title' => '脱毛結果',      'slug' => 'result',    'href' => '/result/', ],
            ['title' => '脱毛へのポリシー',      'slug' => 'about',    'href' => '/about/', ],
            ['title' => 'Q&A',      'slug' => 'faq',    'href' => '/faq/', ]
        ];

    ?>
    <header class="mg-header">
        <div class="mg-header-main">
            <?php if(is_home()){ ?>
              <h1 class="mg-header-logo"><span><?php include __DIR__ . '/resource/img/migrate/header/logo.php'; ?></span></h1>
            <?php } else{ ?>
              <p class="mg-header-logo"><a href="/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜ヘッダー_ロゴ'});"><?php include __DIR__ . '/resource/img/migrate/header/logo.php'; ?></a></p>
            <?php }; ?>

			<p class="mg-header-senyo"><span>女性専用脱毛サロン　｜　<a style="color:#eb4158; text-decoration:none;" href="https://mypage.ginza-calla.jp/login/" target="_blank">会員サイト</a></span></p>
            <p class="mg-header-recruit"><a href="/recruit/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜ヘッダー_採用情報'});">採用情報</a></p>
        </div>
        <nav class="mg-gnav">
            <div class="mg-gnav-inside">
                <ul class="mg-gnav-menu">
                    <?php foreach($navItems as $navItem): ?>
                        <li>
                          <?php if($navItem['slug'] == 'top' && is_home()): ?>
                            <a href="<?=$navItem['href']?>" class="is-active">
                          <?php else: ?>
                            <a href="<?=$navItem['href']?>"<?=is_page($navItem['slug']) ? ' class="is-active"' : ''?>>
                          <?php endif; ?>
                            <?=$navItem['title']?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <ul class="mg-gnav-buttons">
                    <li class="mod-salon"><a href="/salon/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜ヘッダー_サロン検索'});">サロン検索</a></li>
                    <li class="mod-reserve"><a href="https://reserve.ginza-calla.jp/form/Reservations?k=0016" target="_blank" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_pc', 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_pc｜ヘッダー_無料カウンセリング'});">無料カウンセリング予約</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <?php
        unset($navItems);
        unset($navItem);
    ?>
