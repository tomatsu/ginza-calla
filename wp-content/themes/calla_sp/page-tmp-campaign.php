<?php
/*
Template Name: キャンペーン用テンプレート
*/
?>
<?php get_template_part('header'); ?>
<div class="main">
  <?php
    if(get_field('sp_page') == ""){
      if (have_posts()) : while (have_posts()) : the_post();
        the_content();
      endwhile; endif;
    }else{
  ?>
    <?php echo apply_filters('the_content', get_post_meta($post->ID, 'sp_page', true)); ?>
  <?php }; ?>
  <?php if( !get_field('free_banner') ){ ?>
    <div class="block-btn">
       <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0047" class="btn-stripe btn-counsel btn-large btn-shadow" target="_blank">
         <span>無料カウンセリング</span><br>ご予約はこちら
        </a>
      <!-- /.block-btn --></div>
  <?php }; ?>
</div>
<?php get_template_part('footer'); ?>
