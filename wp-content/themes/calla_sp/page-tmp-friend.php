<?php
/*
Template Name: 友達紹介用テンプレート
*/
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>おともだち紹介ページ | 脱毛の銀座カラー</title>
<meta name="viewport" content="width=device-width, user-scale=yes,initial-scale=1.0, maximum-scale=5.0" />
<meta name="format-detection" content="telephone=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<link rel="apple-touch-icon" href="apple-touch-icon.png">
<meta http-equiv="Content-Language" content="ja">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="「おともだち紹介キャンペーン」実施中です。お友達がご来店＆脱毛の契約をされると、嬉しい特典が！ぜひお友達、ご家族、お知り合いに、脱毛専門店の銀座カラーをご紹介ください。">
<meta name="keywords" content="脱毛,銀座,脱毛専門店,自己処理,銀座カラー">
<link href="/common/css/import.css" rel="stylesheet" type="text/css" media="all,screen">
<link href="/plan/css/add_style.css" rel="stylesheet" type="text/css" media="all,screen">
<script type="text/javascript" src="/common/js/jquery.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<style type="text/css">
#wrapper {
	text-align: center;
}
#wrapper .txt {
	font-size: 14px;
	margin: 5% 0;
}
#wrapper p {
	clear: both;
	font-size: 10px;
	margin: 5% 0;
}
img {
max-width : 100% ;
height : auto ;
}
</style>
</head>

<!--キャンペーン内容-->
<body>

<div style="background-color:#d01126; padding:3%;">

<img src="<?php bloginfo('template_url'); ?>/resource/images/head-logo.png" width="176px" height="43px" alt="銀座カラー">

<!--<div style="margin:3% 0 4%;">
<img src="<?php bloginfo('template_url'); ?>/resource/images/zenkoi.png" width="100%"　alt="全身で、恋しよう。">
</div>-->

<div style="background-color:#fff; padding:3% 3% 1% 3%; text-align:center;">
<div style="font-weight:bold; color:#d01126; font-size:140%; border-bottom:3px solid #d01126;">おともだち紹介キャンペーン</div>
<p style="font-size:12px;">おともだちに紹介メールをサロンで送信していただくと<br><span style="font-weight:bold; font-size:18px; color:#d01126;">QUOカード300円分</span>その場でプレゼント！</p>
</div>
</div>



<!--//紹介キャンペーン内容//-->

<!--アプリ選択-->
<div id="wrapper">
<p class="txt">普段お使いのアプリをお選びください。</p>
<p class="txt">
<a href="mailto:?subject=銀座カラーおともだち紹介&amp;body=★☆銀座カラーのおともだち紹介キャンペーン☆★一緒にキレイなつるぷる肌を目指そう♪今なら、ご紹介でご入会された方は10,000円OFF、紹介してくれた方にはギフトカードプレゼント中!!詳しくはこちら%0D%0A%0D%0Ahttp://goo.gl/etbdis%0D%0A%0D%0A〈ご来店時に会員番号をスタッフまで／会員番号〉"> <img width="60px" height="60px" src="<?php bloginfo('template_url'); ?>/resource/images/icon_mail.png" alt="mailで送る" style="margin-right:14%;"></a>
<a href="http://line.me/R/msg/text/?★☆銀座カラーのおともだち紹介キャンペーン☆★　一緒にキレイなつるぷる肌を目指そう♪　ご紹介でご入会された方は10,000円OFF、紹介してくれた方にはギフトカードプレゼント中!! 詳しくはこちら⇒ http://goo.gl/etbdis 〈ご来店時に会員番号をスタッフまで／会員番号〉"><img src="<?php bloginfo('template_url'); ?>/resource/images/linebutton_40x40.png" width="60px" height="60px" alt="LINEで送る" /></a>
</p>
<div style="padding:0 10px;text-align:left;"><p style="padding-left:1em;text-indent:-1em;">※メールの方は、メッセージ内にご自身の会員番号を手入力して送信してください。LINEの方はメッセージ送信後にご自身の会員番号を手入力して送信してください。
</p><p style="padding-left:1em;text-indent:-1em;">※QUOカードプレゼントは、お1人様1回限りとなります。</p>
</div>
</div>
<!--//アプリ選択//-->


<!-- ここから下 ヒートマップ計測タグ　-->
<script src="http://f1.nakanohito.jp/lit/index.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">try { var lb = new Vesicomyid.Bivalves("117383"); lb.init(); } catch(err) {} </script>
<!-- ここまで ヒートマップ計測タグ　-->

<!-- 191017_Yahoo-Geniee -->
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=82r2Wc4,tdfgAp5&referrer=" + encodeURIComponent(document.location.href) + "";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//b.yjtag.jp/iframe?c=82r2Wc4,tdfgAp5" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
<!-- //191017_Yahoo-Geniee -->

<!-- Google Tag Manager -->
<noscript>
    <iframe src="//www.googletagmanager.com/ns?id=GTM-N9H8R5"
height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N9H8R5');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager -->
</body>
</html>
