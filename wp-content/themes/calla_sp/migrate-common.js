!(function() {
  'use strict'

  /**
   * ハンバーガー
   *
   * 脱毛のイメージ
   */
  var Nav = (function() {
    var Nav = function() {
      this.isOpen = false
      this.$nav = $('#js-mg-gnav')
      this.$navInside = this.$nav.find('.mg-gnav-inside')
      this.$logo = this.$nav.find('.mg-gnav-logo')
      this.$menu = this.$nav.find('.mg-gnav-menu li')
      this.$logoMenu = this.$logo.add(this.$menu)
      this.$burger = $('#js-mg-burger')
      this.$burgerLine = this.$burger.find('span')
      this.$flower = this.$nav.find('.mg-gnav-flower')
    }

    Nav.prototype = {
      bind: function() {
        var self = this
        this.$burger.on('click', function() {
          self.isOpen ? self.close() : self.open()
        })
        this.$nav.on('click', function() {
          self.isOpen ? self.close() : null
        })
        this.$navInside.on('click', function(e) {
          e.stopPropagation()
        })
      },
      open: function() {
        var burgerSize = this.$burger.innerWidth();

        this.isOpen = true
        TweenMax.fromTo(this.$nav, 0.3, {opacity: 0}, { opacity: 1, display: 'block' })
        TweenMax.to(this.$burgerLine.eq(0), 0.36, {y: burgerSize, ease: Power3.easeIn})
        TweenMax.to(this.$burgerLine.eq(1), 0.24, {y: 4, ease: Power3.easeIn, repeat: 1, yoyo: true})
        TweenMax.to(this.$burgerLine.eq(2), 0.36, {y: burgerSize, ease: Power3.easeIn})
        TweenMax.staggerFromTo(this.$logoMenu, 0.8, {opacity: 0}, {opacity: 0.8, ease: Power3.easeOut}, 0.04)
        TweenMax.fromTo(this.$flower, 2, {opacity: 0, x: '3%', y: '12%'}, { opacity: 1, x: '0%', y: '0%', ease: Power3.easeOut});
      },
      close: function() {
        this.isOpen = false
        TweenMax.fromTo(this.$nav, 0.2, {opacity: 1}, { opacity: 0, display: 'none' })
        TweenMax.to(this.$burgerLine.eq(0), 0.36, {y: '-180%', ease: Back.easeOut.config(1.4)})
        TweenMax.to(this.$burgerLine.eq(1), 0.15, {y: -4, ease: Power3.easeIn, repeat: 1, yoyo: true, delay: 0.05})
        TweenMax.to(this.$burgerLine.eq(2), 0.36, {y: '180%', ease: Back.easeOut.config(1.4)})
        TweenMax.to(this.$flower, 0.06, {opacity: 0,});
      }
    }

    return Nav
  })()

  /**
   * スクロールで通り過ぎたらクラスを追加
   */
  var addClassOnScroll = function() {
    var CLASS_ACTIVE = 'is-active'
    var $magic = $('.js-magic')
    var controller = new ScrollMagic.Controller();

    $magic.each(function() {
      var $this = $(this)

      new ScrollMagic.Scene({
          triggerElement: this,
          triggerHook: 0.8
        })
        .on('start', function(e) {
          if (e.scrollDirection === 'FORWARD') {
            $this.addClass(CLASS_ACTIVE)
          }
        })
        .addTo(controller);
    })
  }

  /**
   * スクロールで画像をずらす
   */
  var MovingImage = (function() {
    var MovingImage = function() {
      this.$targets = $('.mg-page-moving-frame')
      this.controller = new ScrollMagic.Controller()
      this.scenes = []
    }

    MovingImage.prototype = {
      watch: function() {
        var self = this
        var winHeight = $(window).innerHeight()

        this.$targets.each(function() {
          var $frame = $(this)
          var $image = $frame.find('.mg-page-moving-image')
          var frameHeight = $frame.innerHeight()
          var imageHeight = $image.innerHeight()
          var movingRate = 100 * (imageHeight - frameHeight) / imageHeight
          var watchId = $(this).attr('data-watch')
          var target = watchId ? document.getElementById(watchId) : this
          var direction = $(this).hasClass('mg-mod-reverse') ? 'reverse' : 'normal'

          var instance = new ScrollMagic.Scene({
              triggerElement: target,
              triggerHook: 1,
              duration: winHeight + $(target).innerHeight()
            })
            .on('progress', function(e) {
              var sign = direction === 'reverse' ? -1 : 1
              var y = (sign * movingRate * e.progress) + '%'

              TweenMax.set($image, {y: y})
            })
            .addTo(self.controller)

          self.scenes.push({
            instance: instance,
            $target: $(target),
            $frame: $frame,
            $image: $image
          })
        })
      },
      bind: function() {
        var self = this
        $(window).on('resize', function() {
          self.updateScene()
        })
      },
      updateScene: function() {
        var winHeight = $(window).innerHeight()

        $.each(this.scenes, function(index, scene) {
          var duration = winHeight + scene.$target.innerHeight()

          scene.instance.duration(duration)
        })
      }
    }

    return MovingImage
  })()

  /**
   * ページ内スクロール
   */
  var smoothScroll = function() {
    $('a[href^="#"]').on('click', function() {
      var href = $(this).attr('href')
      var target = href === '#' ? 0 : href

      // 要素がなかったら飛ばない
      if (href !== '#' && !$(target).length) {
        return false
      }

      TweenMax.to(window, 1, {scrollTo: target, ease:Power4.easeOut})

      return false
    })
  }

  $(function() {
    var nav = new Nav()
    var movingImage = new MovingImage()

    nav.bind()
    movingImage.watch()
    movingImage.bind()
    smoothScroll()
    addClassOnScroll()

    window.MigrateGinza = {
      movingImage: movingImage
    }
  })
})();