<?php
if (is_page('area')){
  $url = get_bloginfo('url') . '/salon';
  header("Location: {$url}");
  exit;
};
if (is_page('premium')){
  $url = get_bloginfo('url') . '/plan/course/zenshin/';
  header("Location: {$url}");
  exit;
};
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
    <?php if(is_home()){ ?>
      <title>脱毛なら美容脱毛サロン「銀座カラー」</title>
    <?php }else if(is_page()){ ?>
      <?php if(get_field('_aioseop_title')){ ?>
        <?php
          $anc = $post->ancestors;
          if($anc) {
            $slug = get_page($anc[0])->post_name;
          }
        ?>
        <?php if(is_page('gakuwari')){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else if(is_page_group('care')){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else if(is_page_group('plan')){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else if(is_page_group('salon')){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else if(is_page_group('campaign')){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else if(is_parent_slug() == 'part'){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else if(is_parent_slug() == 'course'){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else{ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }; ?>
      <?php }else{ ?>
        <?php if(is_page('gakuwari')){ ?>
          <title>学生割サービス「脱毛学割」｜美容脱毛サロン「銀座カラー」</title>
        <?php }else{ ?>
          <title><?php the_title(); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }; ?>
      <?php }; ?>
    <?php }else if(is_single()){ ?>
      <?php
        $category = get_the_category();
        $cat_name = $category[0]->cat_name;
        $cat_parent = $category[0]->category_parent;
        $cat_parent = get_category($cat_parent);
        $cat_parent = $cat_parent->slug;
      ?>
      <?php if($cat_parent == 'column'){ ?>
        <?php if(get_field('_aioseop_title')){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜脱毛コラム｜美容脱毛サロン「銀座カラー」</title>
        <?php }else{ ?>
          <title><?php the_title(); ?>｜脱毛コラム｜美容脱毛サロン「銀座カラー」</title>
        <?php }; ?>
      <?php }else{ ?>
        <?php if(get_field('_aioseop_title')){ ?>
          <title><?php the_field('_aioseop_title'); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }else{ ?>
          <title><?php the_title(); ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }; ?>
      <?php }; ?>
    <?php }else if(is_category()){ ?>
      <?php if(is_category('column')){ ?>
        <title>脱毛コラム｜美容脱毛サロン「銀座カラー」</title>
      <?php }else if(is_category('news')){ ?>
        <title>お知らせ｜美容脱毛サロン「銀座カラー」</title>
      <?php }else{ ?>
        <?php
          $category = get_the_category();
          $cat_name = $category[0]->cat_name;
          $cat_parent = $category[0]->category_parent;
          $cat_parent = get_category($cat_parent);
          $cat_parent = $cat_parent->slug;
        ?>
        <?php if($cat_parent == 'column'){ ?>
          <title><?php echo $cat_name; ?>｜脱毛コラム｜美容脱毛サロン「銀座カラー」</title>
        <?php }else if($cat_parent == 'news'){ ?>
          <title><?php echo $cat_name; ?>のお知らせ｜美容脱毛サロン「銀座カラー」</title>
        <?php }else{ ?>
          <?php
            $current_category = get_queried_object();
            $catid = $current_category->term_id;
            if (get_field('cat_meta_title','category_'.$catid) == ''){
              $title = $current_category->name;
            }else{
              $title = get_field('cat_meta_title','category_'.$catid);
            }
           ?>
          <title><?php echo $title; ?>｜美容脱毛サロン「銀座カラー」</title>
        <?php }; ?>
      <?php }; ?>
    <?php }else if(is_tag()){ ?>
      <?php
        $current_category = get_queried_object();
        $catid = $current_category->term_id;
        if (get_field('cat_meta_title','post_tag_'.$catid) == ''){
          $title = $current_category->name;
        }else{
          $title = get_field('cat_meta_title','post_tag_'.$catid);
        }
       ?>
      <title><?php echo $title; ?>｜美容脱毛サロン「銀座カラー」</title>
    <?php }else{ ?>
        <title><?php bloginfo('name'); ?></title>
    <?php }; ?>
    <meta property="og:description" content="銀座カラーは、満足度No.1を追求する女性専用の美容脱毛サロンです。銀座カラーの脱毛は痛みが少なく安全・効果の高いIPL脱毛を採用。お客様ひとりひとりに合った脱毛ケアで赤ちゃんのような素肌へ。丁寧なケアをお約束する脱毛サロンです。">
    <meta property="og:site_name" content="脱毛の銀座カラー">
    <meta property="og:image" content="<?php bloginfo('template_url'); ?>/resource/img/common/head-logo-L.png">
    <meta property="og:type" content="article">
    <meta property="og:locale" content="ja_JP">
    <meta property="og:title" content="脱毛なら美容脱毛サロン「銀座カラー」">
    <meta property="og:url" content="https://ginza-calla.jp">
    <meta property="fb:app_id" content="733790480086051">
    <meta property="fb:admins" content="347882235370392">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <?php if(is_home()){ ?>
      <meta name="keywords" content="脱毛,サロン,専門店,全身,予約,銀座カラー,女性専用">
      <meta name="description" content="銀座カラーは、満足度No.1を追求する女性専用の美容脱毛サロンです。銀座カラーの脱毛は痛みが少なく安全・効果の高いIPL脱毛を採用。お客様ひとりひとりに合った脱毛ケアで赤ちゃんのような素肌へ。丁寧なケアをお約束する脱毛サロンです。">
    <?php }; ?>




    <!-- End Meta Data -->
    <?php if(is_home()){ ?>
      <link rel="canonical" href="<?php bloginfo('url'); ?>">
    <?php }elseif(is_category('2020')){ ?>
      <link rel="canonical" href="<?php bloginfo('url'); ?>/news/">
    <?php }elseif(is_category()){ ?>
      <link rel="canonical" href="<?php echo get_category_link(get_query_var('cat')); ?>">
    <?php }elseif(is_tag()){ ?>
      <?php foreach (get_the_tags() as $tag):?>
        <link rel="canonical" href="<?php echo get_tag_link($tag->term_id); ?>">
      <?php endforeach; ?>
    <?php }else{ ?>
      <link rel="canonical" href="<?php the_permalink(); ?>">
    <?php }; ?>
    <!-- End Annotation -->
    <?php get_template_part('import','css'); ?>

    <?php if(is_page_group('product')){  ?>
      <!-- YAHOO! ＹＤＮサイトリターゲティングタグ-sep -->
      <!-- Yahoo Code for your Target List -->
      <script type="text/javascript" language="javascript">
      /* <![CDATA[ */
      var yahoo_retargeting_id = 'KESY5A3IWB';
      var yahoo_retargeting_label = '';
      var yahoo_retargeting_page_type = '';
      var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '', quantity: ''}];
      /* ]]> */
      </script>
      <script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
      <!-- //YAHOO! ＹＤＮサイトリターゲティングタグ-sep// -->
    <?php }; ?>
    <?php if(get_field('qa_meta_keywords')){ ?>
      <meta name="keywords" itemprop="keywords" content="<?php the_field('qa_meta_keywords') ?>">
    <?php }; ?>
    <?php if(is_category()){ ?>
      <?php
        $cat_id = get_queried_object()->cat_ID;
        $post_id = 'category_'.$cat_id;
      ?>
      <?php if(get_field('cat_meta_keywords',$post_id)){ ?>
        <meta name="keywords" itemprop="keywords" content="<?php the_field('cat_meta_keywords',$post_id) ?>">
      <?php }; ?>
    <?php }; ?>
    <?php if(is_tag()){ ?>
      <?php foreach (get_the_tags() as $tag):?>
        <?php $tag_id = 'post_tag_'.$tag->term_id;  ?>
        <meta name="keywords" itemprop="keywords" content="<?php the_field('cat_meta_keywords',$tag_id) ?>">
      <?php endforeach; ?>
    <?php }; ?>
    <?php wp_head(); ?>
    <style media="screen">
      #wpadminbar,.pc-switcher{display: none;}
      html{margin-top:0px!important;}
    </style>
  </head>
  <body>
    <div id="fb-root"></div>
    <script>
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.4";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>

    <?php
        $navItems = [
            ['title' => 'TOPページ', 'slug' => '',       'href' => '/', ],
            ['title' => '料金体系',   'slug' => 'plan', 'href' => '/plan/', ],
            ['title' => '予約のとりやすさ',   'slug' => 'reservation',        'href' => '/about/reservation.html', ],
            ['title' => '通いやすさ',            'slug' => 'feature',         'href' => '/feature/', ],
            ['title' => '脱毛結果',      'slug' => 'result',    'href' => '/result/', ],
            ['title' => '脱毛へのポリシー',      'slug' => 'about',    'href' => '/about/', ],
            ['title' => 'Q&A',         'slug' => 'faq',     'href' => '/faq/', ],
            ['title' => 'サロン一覧',         'slug' => 'salon',     'href' => '/salon/', ]
        ];
    ?>
    <header class="mg-header">
        <div class="mg-header-main">
            <?php if(is_home()){ ?>
              <h1 class="mg-header-logo"><span><img src="<?php bloginfo('template_url'); ?>/resource/img/migrate/header/logo.png" alt="銀座カラー"></span></h1>
            <?php } else{ ?>
              <p class="mg-header-logo"><a href="/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_sp’, 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_sp｜ヘッダー_ロゴ'});"><img src="<?php bloginfo('template_url'); ?>/resource/img/migrate/header/logo.png" alt="銀座カラー"></a></p>
            <?php }; ?>

			<a href="https://reserve.ginza-calla.jp/form/Reservations?k=0047" target="_blank" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_sp’, 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_sp｜ヘッダー_無料カウンセリング予約'});"><p class="header-cvbox flexiblebox">無料カウンセリング<br>予約</p></a>
			<a style="width: 11.5vw;" href="https://mypage.ginza-calla.jp/login/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/resource/img/migrate/header/member-login.png" alt="会員様ログイン"></a>
        </div>
        <nav class="mg-gnav" id="js-mg-gnav">
            <div class="mg-gnav-inside">
                <p class="mg-gnav-logo"><a href="/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_sp’, 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_sp｜ハンバーガー_ロゴ'});"><img src="<?php bloginfo('template_url'); ?>/resource/img/migrate/header/logo.png" alt="銀座カラー"></a></p>
                <ul class="mg-gnav-menu">
                    <?php foreach($navItems as $navItem): ?>
                        <li><a href="<?=$navItem['href']?>"><?=$navItem['title']?></a></li>
                    <?php endforeach; ?>
                </ul>
                <p class="nav_link"><a href="https://reserve.ginza-calla.jp/form/Reservations?k=0047" target="_blank" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_sp’, 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_sp｜ヘッダー_無料カウンセリング予約'});">無料カウンセリング予約</a></p>
            </div>
            <img class="mg-gnav-flower" src="<?php bloginfo('template_url'); ?>/resource/img/migrate/header/flower.png" alt="">
        </nav>
<!-- 下固定
        <ul class="mg-gnav-buttons">
            <li class="mod-salon"><a href="/salon/" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_sp’, 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_sp｜下固定_サロン検索'});">サロン検索</a></li>
            <li class="mod-reserve"><a href="https://reserve.ginza-calla.jp/form/Reservations?k=0047" target="_blank" onclick="dataLayer.push({'event': 'FireEvent_InternalLink', 'EventCatagory': 'HP_sp’, 'EventAction': 'InternalLink', 'EventLabel': 'InternalLink｜HP_sp｜下固定_無料カウンセリング予約'});">無料カウンセリング予約</a></li>
        </ul>
//下固定 -->
        <button class="mg-header-burger" id="js-mg-burger"><span></span><span></span><span></span></button>
    </header>
    <?php
        unset($navItems);
        unset($navItem);
    ?>
