<?php
/*
Template Name: 移行用ミニマルテンプレート
*/

get_template_part('header');

if (get_field('sp_page') == '') {
  if (have_posts()) {
      while (have_posts()) {
          the_post();
          the_content();
      }
  }
} else {
  echo apply_filters('the_content', get_post_meta($post->ID, 'sp_page', true));
}

get_template_part('footer');