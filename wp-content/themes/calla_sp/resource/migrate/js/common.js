(function ($) {

  var commons = $.commons = {
    num:   3500,
    move:  400,
    timer: null,
    timer2: null,
    link:  'next',
    n:     2,
    num:   null,
    width:   null,
    flag1:   true,

    init: function () {
      if ($('.sp_index #key_img').length)
        commons.key_height();
        
      if ($('.popup').length)
        commons.upimg();
        
      if ($('.tab_area').length)
        commons.tab_area();
        
        
      $('.inview_block').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if(isInView){
          $(this).stop().addClass('show_item');
        }
      });
    },

    key_height: function () {
      // $('.sp_index #key_img ul').slick({
      //   infinite: true,
      //   dots:false,
      //   autoplaySpeed: 4000,
      //   autoplay:true, //自動再生
      //   slidesToShow: 1,
      //   arrows: false,
      //   easing:"easeOutSine"
      // });
      
      $(window).load(function () {
        $('.sp_index #key_img ul a').css({'pointer-events': 'inherit'});
      });
      $('.sp_index #cont01 ul').slick({
        infinite: true,
        centerMode: true,
        dots:true,
        autoplaySpeed: 2000,
        autoplay:true, //自動再生
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        easing:"easeOutSine",
        centerPadding:'14.93333vw'
      });
    },
    tab_area: function () {
      var tab_area = $('.tab_area');
      var tab_nav = tab_area.find('#tab_nav');
      var tab_box = tab_area.find('#tab_box');
      tab_nav.find('ul li a').click(function () {
        var ths = $(this);
        var th = $(this).attr('href');
        tab_nav.find('ul li a').removeClass('on');
        ths.addClass('on');
        tab_box.find('.tab_cont').removeClass('show');
        tab_box.find(th).addClass('show');
        return false;
      });
      
      if ($('.tag').length) {
        $('.tag ul li a').click(function () {
          var ths = $(this).attr('href').replace('#tab_cont0', '');
          var th = $(this).attr('href');
          tab_nav.find('ul li a').removeClass('on');
          tab_nav.find('ul li').eq(ths - 1).find('a').addClass('on');
          tab_box.find('.tab_cont').removeClass('show');
          tab_box.find(th).addClass('show');
          $('html,body').animate({scrollTop: $('#cont01').offset().top - $('.mg-header-main').outerHeight(true)},500);
          return false;
        });
      }
      
      $(window).load(function() {
        if($(location.hash).length && location.hash.indexOf('tab_cont') != -1) {
          
          var target = $(location.hash);
          tab_nav.find('ul li a').removeClass('on');
          tab_nav.find("a[href*='" + location.hash + "']").addClass('on');
          tab_box.find('.tab_cont').removeClass('show');
          tab_box.find(target).addClass('show');
          if (location.search != '') {
            var prak = location.search;
            $('html,body').animate({scrollTop: $('[data="'+ prak.replace('?faq=', '') +'"]').offset().top - $('.mg-header-main').outerHeight(true)},400);
          }
          else {
            $('html,body').animate({scrollTop: $('.tab_area').offset().top - $('.mg-header-main').outerHeight(true)},0);
          }
        }
      });
    },

    upimg: function () {
      var index = $('.popup');

      if (!index.size())
        return false;

      index.fancybox({
      });
    }
  };

  $(function () {
    commons.init();
  });
})(jQuery);
