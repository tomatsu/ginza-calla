/*--------------------------
	Slider
--------------------------*/
$(function(){

  var $slider = $('.js-slider');
  var option = {
    auto: true,
    controls: false,
    infiniteLoop: true,
    onSlideAfter: function() {
      slider.startAuto();
    }    
  }
  slider = $slider.bxSlider(option);

});