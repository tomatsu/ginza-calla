/*
* Name: maps.js
* Description: 複数店舗表示用マップコンストラクタ
* Created: 20150626
* Modified: 20150626
*
*
*
*/
;(function( $, window, document, undefined ) {


  // DOM設定データ
  var elem = document.getElementById('map-canvas');
  var lat = parseFloat( elem.getAttribute('data-lat') );
  var lng = parseFloat( elem.getAttribute('data-lng') );
  var zoom = parseFloat( elem.getAttribute('data-zoom') ) || 16;


  // MAPデータ
  var url = jsonurl;
  var map = null;
  var center = null;
  var salons = [];
  var markers = [];
  var latestInfoWindow = null; // 直近で開いたインフォウィンドウオブジェクトを格納


  // MAP初期化処理
  function initialize() {
    if ( hasCenterPositions() ) {

      // MAPビューポートの初期化
      var center = new google.maps.LatLng(lat, lng);
      var options = {
        center: center,
        zoom: zoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map(elem, options);

      // サロンデータの取得
      salons = getSalonsList();

      // マーカー及びインフォウィンドウの設定
      makeMarkers();
      makePopup();
    }
  }


  // マーカー設定処理
  function makeMarkers() {
    var length = salons.length;
    for ( var i=0; i<length; i++ ) {
      var salon = salons[i];
      var position = new google.maps.LatLng(salon.lat, salon.lng);
      var marker = new google.maps.Marker({
        position: position,
        map: map,
        title: salon.title
      });
      markers.push( marker );
    }
  }


  // インフォウィンドウ設定処理
  function makePopup() {
    var length = salons.length;
    for ( var i=0; i<length; i++ ) {
      var salon = salons[i];
      var marker = markers[i]
      var content = getTemplate( salon );
      var position = new google.maps.LatLng(salon.lat, salon.lng);
      var infowindow = new google.maps.InfoWindow({
        content: content,
        position: position
      });
      setMarkerEvent( infowindow, marker );
    }
  }


  // マーカークリックイベント
  function setMarkerEvent( infowindow, marker ) {
    google.maps.event.addListener(marker, 'click', function(event) {

      // クリックしたインフォウィンドウが既に表示状態なら閉じる
      if ( infowindow.getMap() ) {
        infowindow.close();

      } else {
        // 直近のインフォウィンドウがあれば閉じる
        if ( latestInfoWindow ) {
          latestInfoWindow.close();
        }

        // クリックしたインフォウィンドウを開く
        infowindow.open(marker.getMap(), marker);
        latestInfoWindow = infowindow;
      }
    });
  }


  // JSONファイルよりサロン店舗情報の取得
  function getSalonsList() {
    var salons = [];
    $.ajax({
      url: url,
      async: false,
      dataType: 'json',
      success: function( data ) { salons = data; }
    });
    return salons;
  }


  // インフォウィンドウ用テンプレートより各HTMLの取得
  function getTemplate( salon ) {
    if ( !salon ) return false;
    var tel = formatTel( salon.tel );
    var content = '';
    content += '<div id="content">';
    content += '<p>' + salon.title + '</p>';
    content += '<p>【営業時間】' + salon.businessHour + '</p>';
  //  content += '<p>【定休日】' + salon.closed + '</p>';
    content += '<p>【電話番号】<a href="tel:' + tel + '" style="color: #393939;">' + salon.tel + '</a></p>';
    content += '</div>';
    return content;
  }


  function formatTel( string ) {
    var num = string.replace(/[‐－―ー-]/gi, '');       // ハイフンを削除
    num = num.replace(/[０-９]/g, function(s) {     // 全角を半角に変換
      return String.fromCharCode( s.charCodeAt(0)-0xFEE0);
    });
    return num;
  }


  // HTMLより中央位置情報を取得可能かどうか
  function hasCenterPositions() {
    if ( isNaN( lat ) || isNaN( lng ) ) {
      var e = getPositionError();
      e.showErrorMessage();
      return false;
    } else {
      return true;
    }
  }


  // HTMLより中央位置情報を取得できなかった場合のフォールバック処理
  function getPositionError() {
    var error = {
      message: '<p>Sorry, This map is currently not available.</p>',
      style: {
        width: '100%',
        textAlign: 'center',
        fontSize: '12px',
        color: '#DDABB1',
        position: 'absolute',
        bottom: '10px',
        left: '0',
        right: '0'
      },
      showErrorMessage: function() {
        $( elem ).html( this.message ).children().css( this.style );
      }
    }
    return error;
  }


  // イベント設定
  window.addEventListener('load', initialize);

})(jQuery, window, document);
