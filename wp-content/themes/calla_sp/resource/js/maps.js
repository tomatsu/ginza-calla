function initialize() {
    var latlng = new google.maps.LatLng(35.672787,139.764453);
    var mapOptions = {
      center: latlng,
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map-canvas"),
      mapOptions);
    var marker = new google.maps.Marker({
      position: latlng,
      title: '銀座本店'
    });
    marker.setMap(map);
    var contentString = '<div id="content">'+
      '<p>銀座本店</p>'+
      '<p>【営業時間】10：00～19：00</p>'+
      '<p>【定休日】なし</p>'+
      '<p>【電話番号】0120-360-286</p>'+
      '</div>';
    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });
    infowindow.open(map,marker);
  }
  
  
  $(function() {
    $(window).on('load', initialize);
  })