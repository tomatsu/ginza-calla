$(function(){
    $(window).scroll(function (){
        $('.fadein').each(function(){
            var elemPos = jQuery(this).offset().top;
            var scroll = jQuery(window).scrollTop();
            var windowHeight = jQuery(window).height();
            if (scroll > elemPos - windowHeight + 100){
                $(this).addClass('scrollin');
            }
        });
    });

    $('.more-btn').prev().hide();
        $('.more-btn').click(function () {
            if ($(this).prev().is(':hidden')) {
                $(this).prev().slideDown();
                $(this).addClass('close');
            } else {
                $(this).prev().slideUp();
                $(this).removeClass('close');
            }
            return false;
        });


  });
