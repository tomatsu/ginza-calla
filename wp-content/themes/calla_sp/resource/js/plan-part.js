/*--------------------------
	change block-region-image
--------------------------*/
$(function() {
    $('#front .block-region-btns a').hover(function() {	// front
            var targetFileName = '';
            targetFileName = makePartImagePath(this);
            changePartImage(this, targetFileName);
        },
        function() {
            changePartImage(this, tmpurl + '/resource/img/plan/part/index/img-normal.png');
        });

    $('#back .block-region-btns a').hover(function() {	//back
            var targetFileName = '';
            targetFileName = makePartImagePath(this);
            changePartImage(this, targetFileName);
        },
        function() {
            changePartImage(this, tmpurl + '/resource/img/plan/part/index/img-back-normal.png');
        });

    function makePartImagePath(targetObj) {	//makePath
        var targetName = '';
        var classNameArray = targetObj.className.split(' ');
        for (var i = 0; i < classNameArray.length; i++) {
            if (classNameArray[i].indexOf('btn-') >= 0) {
                targetName = classNameArray[i].split('btn-')[1];
            }
        }
        targetFileName = tmpurl + '/resource/img/plan/part/index/img-' + targetName + '.png';
        return targetFileName;
    }

    function changePartImage(targetObj, imagePath) {	//change
        var parentObj = $(targetObj).parent().parent().parent();
        $('.block-region-image img', parentObj).attr('src', imagePath);
    }

});
