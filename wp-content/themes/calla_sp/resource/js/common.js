/*--------------------------
	銀座カラー 共通設定
--------------------------*/
var config = config || {};
config.duration = 400;
config.delay = 200;
config.easing = 'easeOutQuint';





/*--------------------------
	銀座カラー 共通関数
--------------------------*/
var functions = functions || function() {};
functions = function() {
	var funcs = {};


	/*
	 * Name: getBrowser
	 * Description: 現在表示中のブラウザの種類を文字列として返す
	 * Return: [String]
	 *
	 */
	funcs.getBrowser = function() {
		var ua = navigator.userAgent.toLowerCase();
		var ver = navigator.appVersion.toLowerCase();
		var browser = 'unknown';

		if ( ua.indexOf("msie") != -1 ) {
			if ( ver.indexOf("msie 6.") != -1 ) {
				browser = 'ie6';
			} else if ( ver.indexOf("msie 7.") != -1 ) {
				browser = 'ie7';
			} else if ( ver.indexOf("msie 8.") != -1 ) {
				browser = 'ie8';
			} else if ( ver.indexOf("msie 9.") != -1 ) {
				browser = 'ie9';
			} else if ( ver.indexOf("msie 10.") != -1 ) {
				browser = 'ie10';
			} else {
				browser = 'ie';
			}
		} else if ( ua.indexOf('trident/7' ) != -1 ) {
			browser = 'ie11';
		} else if ( ua.indexOf('chrome') != -1 ) {
			browser = 'chrome';
		} else if ( ua.indexOf('safari') != -1 ) {
			browser = 'safari';
		} else if ( ua.indexOf('opera') != -1 ) {
			browser = 'opera';
		} else if ( ua.indexOf('firefox') != -1 ) {
			browser = 'firefox';
		}

		return browser;	
	};



	/*
	 * Name: trim
	 * Description: 文字列の両端の空白を削除する
	 * Argument: [String]
	 * Return: [String]
	 *
	 */
	funcs.trim = function( string ) {
		return string.replace(/^\s+|\s+$/g, '');
	};



	// Export Global Context
	return funcs;
}




/*--------------------------
	銀座カラー 共通処理
--------------------------*/

/* モバイルスライドメニュー
--------------------------*/
$(function(){

	var $body = $(document.body);
	var $menu_btn = $('.header-nav-menu');
	var $menu_list = $('.menu');
	var menu_width = $menu_list.outerWidth();
	var status = false;

	$menu_btn.on('click', function(e) {
  	e.preventDefault();
    status = !status;
    var func = status ? menuOpen : menuClose;
    func();
	});

  function menuOpen() {
    $body.addClass('menu-open').prepend('<div class="main-overlay"></div>');
    $menu_list.addClass('menu-listopen');
    $('.main-overlay').on('click', mainClose);
  }

  function menuClose() {
    $body.removeClass('menu-open');
    $menu_list.removeClass('menu-listopen');
    $('.main-overlay').off('click', mainClose).remove();
  }

  function mainClose(e) {
    e.preventDefault();
    status = !status;
    menuClose();
  }

});



/* スムーズスクロール
--------------------------*/
$(function(){
  
  var $trigger = $('a[href^="#link-"]');
  $trigger.on('click', function(e) {
    e.preventDefault();
    var href = $(this).attr('href');
    $('html, body').stop().animate({
      scrollTop: $(href).offset().top
    }, config.duration, config.easing);
  });

});



/* セレクトメニュー
--------------------------*/
$(function(){
  var $select = $('.select > select');
  $select.customSelect({ customClass: 'select-view' });
});



/* タブ
--------------------------*/
$(function(){
	
	var Tab = function( $elem ) {

  	this.$el = $elem || $('.js-tab');
  	this.$tabNav = this.$el.find('.js-tab-nav');
  	this.$tabBody = this.$el.find('.js-tab-body');

    this.init();
    this.listenTo();
    this.changeByUrl();
	}

  Tab.prototype.init = function() {
   this.$tabBody.filter(':gt(0)').hide();
  }

  Tab.prototype.changeByUrl = function() {
    var tabName = this.getTabNameByUrl();
    var $tabAnchor;

    if (tabName) {
      $tabAnchor = this.$tabNav.find('li a[href="#' + tabName + '"]');
      if ($tabAnchor.length) {
        $tabAnchor.trigger('click');
      }
    }
  }

  Tab.prototype.getTabNameByUrl = function() {
    var query = document.location.search.substring(1);
    var params = {};
  
    if (!query) {
      return '';
    }

    query.split('&').forEach(function(param) {
      var kv = param.split('=');
      params[kv[0]] = kv[1];
    })

    return params.hasOwnProperty('tab') ? params.tab : '';
  }

  Tab.prototype.listenTo = function() {
    var data = { that: this };
    this.$tabNav.on('click', 'a', data, this.change);
  }

  Tab.prototype.change = function(e) {
    e.preventDefault();
    var href = $(this).attr('href');
    if ( href ) {
      if ( e.data.that.$el.hasClass('js-tab-region') ) {
        e.data.that.$tabNav.find('li').removeClass('on');
        $(this).parent().addClass('on');        
      } else {
        e.data.that.$tabNav.find('a').removeClass('on');
        $(this).addClass('on'); 
      }
      e.data.that.$tabBody.hide()
      $( href ).fadeIn( config.duration );
    }
  }

  Tab.prototype.changeByIndex = function( index ) {
    if ( index ) {
      var that = this;
      e.data.that.$tabNav.find('a').removeClass('on');
      $tabNav.find('a').eq( index ).addClass('on');
      that.$tabBody.hide().eq( index ).fadeIn( config.duration );
    }
  }

  var tab = new Tab();
  var tabPart = new Tab( $('.js-tab-region') );

});


/* アコーディオン
--------------------------*/
$(function(){

  var Accordion = function() {
    this.$trigger = $('.js-accordion-trigger');
    this.$body = this.$trigger.next();
    this.init();
    this.listenTo();
  }

  Accordion.prototype.init = function() {
    this.$body.hide();
  }
  
  Accordion.prototype.listenTo = function() {
    var data = { that: this };
    this.$trigger.on('click', data, this.onClick);
  }
  
  Accordion.prototype.onClick = function(e) {
    e.preventDefault();
   // e.data.that.$body.slideUp(config.duration, config.easing);
    $(this).next().slideToggle(config.duration, config.easing);// add
    
   // e.data.that.$trigger.removeClass('on');
    if ( $(this).next().is(':hidden') ) {
      $(this).next().slideToggle(config.duration, config.easing);
      $(this).addClass('on');
    }
  }
  
  var accordion = new Accordion();

});




/* トップボタン
--------------------------*/
$(function() {
    var topBtn = $('.totop');
    if ( topBtn.length > 0 ) {
      topBtn.hide();
      $(window).scroll(function () {
          if ($(this).scrollTop() > 700) {
              topBtn.fadeIn();
          } else {
              topBtn.fadeOut();
          }
      });
      topBtn.click(function () {
          $('body,html').animate({
              scrollTop: 0
          }, 500);
          return false;
      }); 
    }
});