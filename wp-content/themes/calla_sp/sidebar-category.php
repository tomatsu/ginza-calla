<!-- columnページ -->
<?php if(is_category('column') || in_category('column') || in_category_child( get_term_by( 'slug', 'column', 'category' ))){ ?>
  <div class="sidebar-list">
    <p class="sidebar-list-head">脱毛や美容に関するコラム</p>
    <ul>
      <li><a href="<?php bloginfo('url'); ?>/column/01/" class="trans">かんたんカワイイおいしい　今週のおうちごはん</a></li>
      <li><a href="<?php bloginfo('url'); ?>/column/02/" class="trans">名作に学ぼう　今週の、あのセリフ</a></li>
      <li><a href="<?php bloginfo('url'); ?>/column/03/" class="trans">週末どこいく？注目のイベント情報</a></li>
      <li><a href="<?php bloginfo('url'); ?>/column/04/" class="trans">こっそり教えます！プロの専門知識</a></li>
      <li><a href="<?php bloginfo('url'); ?>/column/05/" class="trans">ハッケン！銀座カラーの街</a></li>
    </ul>
  </div><!-- /.sidebar-list -->
<?php }; ?>
<!-- columnページここまで -->



<!-- newsページ -->
<?php if(is_category('news') || in_category('news') || in_category_child( get_term_by( 'slug', 'news', 'category' ))){ ?>
  <div class="sidebar-list">
    <p class="sidebar-list-head">お知らせ</p>
    <ul>
      <li><a href="<?php bloginfo('url'); ?>/news/2016/" class="trans">2016年</a></li>
      <li><a href="<?php bloginfo('url'); ?>/news/2015/" class="trans">2015年</a></li>
      <li><a href="<?php bloginfo('url'); ?>/news/2014/" class="trans">2014年</a></li>
      <li><a href="<?php bloginfo('url'); ?>/news/2013/" class="trans">2013年</a></li>
    </ul>
  </div><!-- /.sidebar-list -->
<?php }; ?>
<!-- newsページここまで -->
