<?php
/*
Template Name: リダイレクト用テンプレート
*/
?>
<script type="text/javascript" src="https://ginza-calla.jp/wp-content/themes/calla_pc/resource/js/lib/common.js?20191225"></script>
<script type="text/javascript">
(function ($) {
  $(function () {
    var num = '';
    if (location.pathname.indexOf('01-') !== -1)
      num = '#tab_cont01';
    else if (location.pathname.indexOf('02-') !== -1)
      num = '#tab_cont02';
    else if (location.pathname.indexOf('03-') !== -1)
      num = '#tab_cont03';
    else if (location.pathname.indexOf('04-') !== -1)
      num = '#tab_cont04';

    var str = '';
    if (location.hash != "")
      str = '?faq=' + location.hash.replace('#', '');

    window.location.href = "/faq/" + str + num

  });
})(jQuery);
</script>
