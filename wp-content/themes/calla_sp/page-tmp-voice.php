<?php
/*
Template Name: ボイスページ用テンプレート
*/
?>
<?php get_template_part('header'); ?>
    <div class="main">
      <h1 class="page-head icon-voice">お客様の声(口コミ・体験談)</h1>
      <div class="block-white">
        <p class="text">
      銀座カラーで脱毛されたお客様の感想や体験談をご紹介します。サロン全体の印象から、各コース、部位毎の脱毛効果まで、銀座カラーの脱毛の評判をこの口コミ情報でチェックしてみてください。
      </p>

      <div class="container js-tab">

        <div class="block-tab-nav js-tab-nav">
          <ul>
            <li><a href="#all" class="on">全て</a></li>
            <li><a href="#salon">サロン</a></li>
            <li><a href="#course">コース</a></li>
            <li><a href="#part">箇所</a></li>
          </ul>
        <!-- /.js-tab-nav --></div>

        <div class="js-tab-body" id="all">
 <div class="block-voice-list no_margin js-appendmore-voice" itemscope="" itemtype="http://schema.org/BeautySalon">
<!-- -->


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金山店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>ひざ下のカミソリ負けが悩みで脱毛サロンを探していました。
仕事の関係で平日は何時に業務が終わるか分からないし、土日も予定が埋まっている事が多く、とにかく予約が取りやすいサロンを探していたところ、友達が銀座カラーがオススメだということで来店しました。名古屋駅周辺で探していたら、金山に新しく店舗がOPENしたというので「銀座カラー金山店」に通っています。駅からも家からも近くて非常に助かっています。
店内も綺麗でスタッフの方も優しい！！施術に関する説明も丁寧な印象があります。今まで、お風呂上がりにはこれでもかってくらい化粧水とボディクリームを塗っていたのですが、脱毛してからは少ない量でも肌荒れしなくなりました。節約にもなってラッキーです(笑)
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金山店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 アパレル<br></span>
</dt>
<dd itemprop="reviewBody"><p>これまでセルフケアをしていたのですが、年々、毛穴の黒ずみがひどくなっている気がして、色々調べてみたらシェービングによる肌荒れが原因なのではということで顔の産毛もサロンで脱毛してみることに。元々他サロンで全身脱毛はしていましたが全然予約がとれなかったので、今回は銀座カラーの顔脱毛コースを申し込みました。2～3回目くらいから見て分かるくらい毛穴の黒ずみも少なくなり、キメの細かい肌になった気がしました。肌触りもツルツルで化粧ノリも良くなった気がします。特に顔は隠せないのでセルフケアではなくサロンで脱毛した方が良いなと改めて思いました。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金山店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>大学生になったら絶対脱毛しようと高校生の時からアルバイトでお金をため、念願の初脱毛サロンは学校の同級生の紹介で学割や紹介キャンペーンがある銀座カラーにしました。
初めての脱毛なので施術内容やお金の問題が不安だったけどお店の方が丁寧に説明をしてくれて、施術中も私が不安そうにしていたら声をかけてくれたりと本当に優しいスタッフの方々で良かったです。施術も全然痛くなかったので一安心です！まだ数回しか行ってないけど、新しく生えてくる毛が段々と薄くなってきている感じがします！
アルバイトと学校で予定が立て込んでいるから週末の時間しか脱毛に行く時間がないけれど、銀座カラーは予約しやすくて助かってます！
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金山店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>転職することになり、新しい会社には生まれ変わった自分で行きたい！
この際だから脱毛をしてキレイになろうと名古屋市内で脱毛サロンを探していたところ、ネットで良い口コミが多かったし、電車広告でよく見ていたので、銀座カラーにしました！
この機会にと全身脱毛のコースで申し込みました。何回か通ううちに肌がすごくスベスベしてきた気がします！お気に入りは施術後の保湿ケア。モチモチで赤ちゃんのような肌に！これは本当におすすめ！！
新しい会社でも肌が綺麗だねって言われて、すでに通って良かったなと実感しています(笑)
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金山店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">30代 主婦<br></span>
</dt>
<dd itemprop="reviewBody"><p>私は敏感肌で肌が弱く、セルフケアでは肌荒れしてしまうことが悩みでした。
ただ、脱毛は肌にダメージを与えてしまうのでないかと、良いイメージがあまりなかったので、まずは無料のカウンセリングに行きました。
銀座カラーのカウンセリングはその人の肌状況、肌質に合ったコースの選択やケア方法を教えてくれます。私自身脱毛サロンへ通うのが初めてということもあり、肌荒れするのが怖いのでたくさん質問をさせていただきましたが、丁寧に回答をしていただき、不安なく施術に進めました。
子供を保育園に預けている間にさっと通わなくてはいけないので、銀座カラー金山店は駅近で非常に助かっています。スタッフの方も気さくで優しく、施術後のスキンケア方法から最近の美容情報、話題のコスメ情報などスタッフの方との会話にも花が咲きます。今では毎回のサロン通いが楽しみです。
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金山店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 保育士<br></span>
</dt>
<dd itemprop="reviewBody"><p>恥ずかしいお話なのですが、保育園の子どもから「先生の腕チクチクする～」と言われ銀座カラーに通うことを決意しました！これまで自分でケアをしていたのですが、毎日仕事がとても忙しいので、肌の露出が少ない冬はついケアを怠ってしまうんですよね。腕がチクチクする先生なんて最悪ですからね(笑)「ツルツル肌の先生」になれるよう、今では定期的にサロンに通っています。
銀座カラー金山店に通うようになったのは、アクセスが良いから！通勤する際に使用している駅だし、いつも仕事終わりの時間に予約しています。
あと初めての脱毛サロンは実績がある有名なところが良いと思い、銀座カラーにしました。通ってみたら施術も保湿ケアもきちんとしていて大満足です！これからも末長くよろしくお願いいたします(笑)
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金山店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 美容師<br></span>
</dt>
<dd itemprop="reviewBody"><p>仕事の関係上、身だしなみには気をつかわなくてはいけないので、以前からセルフケアではなく脱毛サロンでケアをしています。美容師仲間の紹介で銀座カラー金山店に行くことにしました。
同じ接客業という事でつい店員さんの仕事を細かく見てしまっていたのですが...
銀座カラーはとにかく仕事が丁寧！！！私も見習わなくてはいけないなと改めて思いました(笑)
アフターケアの施術メニューも充実していて、キャンペーン中の美肌潤美のお試しをしたのですが、お肌がモチモチで、毎日使いたいと思いセルフケア用のローションも買っちゃいました！ちなみに私は入念に保湿をしたいので「しっとりタイプ」を購入。今では肌荒れもしなくなり大満足です。
</p></dd>
</dl>



<!-- 北千住2nd店 -->
<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住2nd店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  北千住駅は乗り換えで毎日使っていて、仕事終わりに通える脱毛サロンを調べている時に、銀座カラー北千住2nd店のことを知りました。<br>
  ルミネやマルイのある西口側だし、ホームページでみた店内の写真が可愛いかったので、無料カウンセリングを受けてみることにしました。<br>
  カウンセリングをしてくれたスタッフさんがすごく親身に話を聞いてくれて、「ここなら自分の体を任せられる！」と思って契約しました。<br>
  もう何回か通っていますが、施術は丁寧だし、細かく声をかけてくれるので、毎回安心できています！<br>
  これからもしっかり通って、綺麗な肌を目指します！
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住2nd店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  冬の間、ムダ毛のことは全然気にしていなかったのですが、暖かかくなって薄着になっていくにつれてだんだんと気になってきたので、思い切って脱毛サロン通いを始めちゃいました。<br>
  銀座カラーはわりと行きたい時に予約が取れるし、1回あたりの時間も短くてサクッと通えるので助かっています。<br>
  しかも、スキンケアに詳しくない私に、スタッフさんが色々アドバイスをしてくれるのが嬉しい！<br>
  迷っている方はカウンセリングだけでも受けてみるといいですよ！
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住2nd店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  北千住には買い物によく行くので、ショッピングのついでに脱毛ができるのはとても嬉しいです。<br>
  店内はカフェみたいな内装でオシャレだし、スタッフの皆さんの対応はにこやかで癒されます。以前通っていた脱毛サロンはつくりが簡素で冬は寒いのが苦痛だったので、こだわったしっかりとしたつくりなのはポイントが高いです。<br>
  しかも、予約が希望通りに取れることが多いので、予定も立てやすくて助かってます。自分のスタイルに合わせて通えるので、無理なく自己処理のいらない肌を目指せますね！
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住2nd店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  大学の友達が銀座カラーに通っていて、「銀座カラーいいよ！」とおすすめされたので、一回話を聞くだけでもと思ってカウンセリングを予約しました。<br>
  もともと脱毛に興味があったのですが、痛くないかな？とか、高いんじゃないかな？とか不安なことが多くていっぱい質問をしちゃったのですが、全部に丁寧に答えてくれました。<br>
  金額も思ってたよりも安くって、思い切って通うことに決めました。<br>
  通うたびに肌が綺麗になっているような気がして、毎回通うのが楽しみです！<br>
  他の友達にもオススメしようと思います！
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住2nd店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 受付<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  職業柄、人と間近で接する機会が多く、ムダ毛の処理には気を遣っていたのですが、「自己処理に割く時間が短くなれば良いのに」と常に思っていました。<br>
  ある時に、銀座カラーの広告を偶然見かけて、気になって調べてみると口コミの評判が良かったので、銀座カラーに通うことに決めました。<br>
  スタッフの皆さんは細かいところまで気を配ってお声がけしてくださいますし、店内も清潔で、いつも落ち着いて施術を受けられています。<br>
  日々のケアについてもアドバイスをくださるので、いつも参考にしています。回数を重ねるうちに自己処理の時間が徐々に減ってきました。銀座カラーに決めて良かったと心から感じています。
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住2nd店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  友人の結婚式に出席する機会が増えてきて、自己処理では手の届かない背中の処理に悩んでいました。<br>
  せっかくなら、背中だけではなく全身綺麗にしたいと思い、銀座カラーで契約を決めちゃいました。<br>
  銀座カラーの全身脱毛は顔も含まれているのに1回で全身脱毛できるので、通うのが面倒くさくなりません！最近は背中も綺麗になってきたし、顔の脱毛も効果が出てきたのか化粧ノリが良くなった気がします。こうやって通うたびに効果を実感できているので、モチベーションを維持できています。<br>
  この調子なら、自分の結婚式でも焦らずに済みそうです！まだ予定はないですが（笑）
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住2nd店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  就職活動が終わったのを期に、社会人になる準備として、今銀座カラーに通っています。<br>
  私は結構ズボラなタイプで、ムダ毛の自己処理をさぼりがちだったのですが、何回か通っているうちに自己処理の必要がだんだんなくなってきて助かっています。<br>
  個人的には、パウダールームが綺麗なのが嬉しいポイント！<br>
  社会人になると自由に使える時間が今より減ってしまうので、学生のうちに通い始めて良かったなーと思っています！
</p></dd>
</dl>
<!--  //北千住2nd店 -->



<!-- AETA町田店 -->
<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="AETA町田店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 ダンサー<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  町田で遊ぶことが多いので、脱毛サロンも町田駅から近い銀座カラーのAETA町田店に通うことにしました。<br>
  露出した衣装を着る事が多いので、全身つるつるにしたいと思っていましたが、銀座カラーは全身と顔がコースに全て含まれているので追加料金もいらないし希望通りのコースです。<br>
  お店に通い始めてからどんどん美意識が高まり、家でのケアもしっかりするようになりました。<br>
  周りの友達から肌のことを褒められますが、銀座カラーのおかげです!!
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="AETA町田店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務職<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  女子会で「結婚前にやっておいたほうがいいこと」の話になったとき、結婚している友達がこぞって〝脱毛〟って答えていました。私も脱毛サロンに行ってみたいなとは思っていましたが、なかなか勇気がでなくてそのままに…。<br>
  でも今回友達の話を聞いて勢いがつき、やっと無料カウンセリングの予約をしました!<br>
  最寄りの町田駅には、銀座カラーが2店舗ありますが新しいAETA町田店へ行くことに。駅からのアクセスは抜群、店内は女性好みのかわいい作り、スタッフの方はみんな優しくて素敵…いいことづくしでテンションが上がりました。<br>
  不安なこともしっかり相談にのってもらえて、自分に合ったコースを契約できたので通うのがすごく楽しみです!!
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="AETA町田店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 保育士<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  いつも自分でムダ毛の処理をしていますが、少しさぼってしまった日に園の子供から「先生触ると、ちくちくするー」っと言われてしまいました…。<br>
  子供は、正直ですからね。傷つきましたが、いい機会なので脱毛サロンに行ってみようかなって思ったんです。<br>
  姉が銀座カラーに通っていたこともあり、私も銀座カラーAETA町田店に決めました。契約時には、姉からの紹介だと話すと割引もありましたし、紹介者の姉にも特典がついたのですごくラッキーでした!!<br>
  お店の立地がいいし近隣にお買い物スポットが多いので、私は休日に楽しみながら通っています。<br>
  もうチクチクで嫌がられることはないので、自信をもって子供達と触れ合いたいと思います。
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="AETA町田店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務職<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  小さい頃から毛が濃いのが悩みだったので、ボーナスがでたお祝いに銀座カラーで脱毛することにしました。カウンセリングを受けるまでは、すごく高額なのかと思っていましたが意外にリーズナブルなことに驚きました。<br>
  美肌潤美のミストをオプションでつけても想定金額内だったので、自分へのご褒美に全部契約しちゃいました♡<br>
  まだ数回しか施術に通っていませんが、少しずつ肌の調子がよくなってきているので今後の変化も期待大です!!<br>
  今までの悩みが解消されて、銀座カラーさんには本当に感謝しています。<br>
  そして、これからも宜しくお願いします!!
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="AETA町田店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  大学の入学祝いに銀座カラーで契約をしてもらいました!!入学祝いに脱毛なんて珍しいと思いますが、ずっと夢だったんです。<br>
  高校までの学生時代って、プールの授業があるからムダ毛のことが気になって仕方ありませんでした。<br>
  思い切って母にそのことを相談すると、大学生になったら脱毛サロンに通わせてくれると約束してくれました。<br>
  先日やっと約束の日を迎えられたので、嬉しくてたまりません!!母も、学割が適用されてお得になったので喜んでいました。<br>
  AETA町田店は学校の近くにあるので、授業のスケジュールに合わせて無理なく通えそうです。大学生活、思い切り楽しみまーす!!
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="AETA町田店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 美容師<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  住んでいるのは多摩ですが、町田の美容室に務めているので休日もよく町田に遊びに行きます。AETA町田には友達とカラオケをしに行きましたが、その時銀座カラーが入っていることを知りました。ネットで検索したらカウンセリングは無料と書いてあったので、無料の言葉につられてすぐ予約しちゃいました!<br>
  ノープランで行きましたが、カウンセリングを担当してくれた方がとても丁寧に説明してくれたので、私の美意識に火がつき即決しました!!<br>
  私はもっと早くカウンセリングに行けばよかったなと思ったので、迷っている人にはぜひ早く行くことをオススメします。
</p></dd>
</dl>
<!--  //AETA町田店 -->



<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="広島店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>大学生になって、サークルの集まりで海やパーティなど肌を露出する機会が増えました。女子会でもムダ毛の話で盛り上がることが多くなり、半数以上はサロンに通っているとのこと。みんなに乗り遅れたくない！と焦って銀座カラーのカウンセリングに行きました。予算内の提案や肌ケアのアドバイスなどしてもらい、もっともっといろいろ教えてもらいたい！と思ったので、こちらに決めました。お金との兼ね合いもあり、一番気になっているワキと脚からはじめています。いつか全身制覇することが目標です！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="広島店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 保育士<br></span>
</dt>
<dd itemprop="reviewBody"><p>仕事が忙しくてついムダ毛処理を忘れてしまう毎日。とうとう母に、もう少し気を使いなさいと叱られてしまいました。ならばいっそのこと、サロンに行ってしまおう！と、めんどくさがりな私はカウンセリングに行ってみました。すると、肌の状態をチェックしてくれたり、スキンケアの大切さなど丁寧に教えてくれるので、やる気が出てきました！なので、サロンで脱毛すれば自己処理しなくてもいい生活も夢じゃない！と、銀座カラーに通うことに。照射の痛さは軽くパチパチするくらいで私は気になりません。めんどくさがりな人こそ、楽をしてキレイになりたいと思っているはずなので、ぜひ試してもらいたいです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="広島店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>東京にいた頃は銀座カラーさんを利用していたんですが、広島に転勤することになってからは脱毛をお休みしていました。他のサロンに変えようかとも考えていましたが、当時気にしていた毛が目立たなくなってある程度満足していたこともあり、そのままにしていました。ところが、広島店がオープンすると知り、密かに考えていたVIOにチャレンジすることに。銀座カラーの脱毛効果は自分の毛で知っているので、何のためらいもありませんでした。広島のスタッフも親切な方ばかりなので、またはじめてよかったなと思います。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="広島店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 アパレル<br></span>
</dt>
<dd itemprop="reviewBody"><p>私は、小学校の頃から毛が濃いことがコンプレックスで、抜いたり、脱色したり、ワックスを使ったりと色々試していました。でも、これには終わりが無いので、いつかサロンで脱毛がしたいとずっと思っていました。念願のアパレルの仕事にも就いてお給料も安定し、やっと自由に使えるお金ができたので、以前から広告を見て気になっていた銀座カラーだ！と思って、母を連れて相談へ。スタッフさんの印象がとてもよかったので母の承諾も得て、心に決めていた全身脱毛を申し込みました！その後、友達と脱毛の話になって、全身が60分だと言うととっても驚いていて（笑）。初めてだったので、そんなものなのかなと思っていたんですけど、すごいことだったんですね！銀座カラーに行ってみてよかったです！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="静岡店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>私は顔のうぶ毛がコンプレックスで、カミソリで処理していたんですが、怪我をしたりカミソリまけをしたりすることがあるので、ちゃんと脱毛がしたいなと思うようになりました。そんなことを先輩に相談したら、銀座カラーを紹介して頂きました。初めてのサロンで、上品な雰囲気にちょっと緊張してしまいましたが、スタッフの方が丁寧に案内してくれたので安心できました。通ううちに気付いたのですが、顔の明るさと化粧ののりが変わったなと実感するようになったので、行ってみて本当によかったなと思いました。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="静岡店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>プールに通っているんですが、自己処理が甘くて水着に着替えてから発見することがたまに…。友人に相談したところ「脱毛サロンに通っているから、そもそもそんな悩みがない」と言われました。確かに脱毛サロンなら確実そうなのですが、料金が高いとか広告は安くても行ったら勧誘されて高いものを売りつけられるとか、悪いイメージがあり近づきませんでした。友人が「興味あるならカウンセリングだけでも受けてきたら？」と言うので、恐る恐る銀座カラーに行ってみることに。結果、押し売りをしてくるようなことはないし、金額面でも無理のない範囲のプランを提案してくれたので、通うことに決めました！全く後悔していませんし、むしろもっと早く相談して置けばよかったなというのが正直な気持ちです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="静岡店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 主婦<br></span>
</dt>
<dd itemprop="reviewBody"><p>ママ友と噂をしていた銀座カラー。独身の頃に他のサロンでワキ、腕、脚はやっていたのですが、最近どうしても指毛が気になりだして…ママ友と一緒にカウンセリングに行ってみようということになりました。金額も夫に怒られない範囲でしたし、幼稚園の送り迎えの時に通えそうだなと。気にしていた痛みも大したことないようで、手元だけだからあっという間に終了するので、ネックになることが何もありませんでした。こんなに気軽に指毛が消えてくれるならうれしいです！銀座カラーさん、ありがとうございます！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="静岡店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>姉が結婚することになったので、母に許可してもらい気になっていた銀座カラーに姉妹で通うことにしました。姉はドレスのために背中と腕を、私はずっと気にしていたワキと脚をやってもらうことにしました。脱毛は憧れていたし、店内もかわいいので毎回行くだけでテンションが上がっちゃいます！姉の施術箇所はキレイになってきて、背中のブツブツも目立たなくなってきました。私もシェービングの回数が減ってきたので、処理がとても楽！私も結婚する前に背中までキレイにしたいので、少しずつお金をためて、いつか全身ツルツルにしたいです。</p></dd>
</dl>

<!--
<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金沢駅前東店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 受付<br></span>
</dt>
<dd itemprop="reviewBody"><p>脱毛の重要性を実感したのは、ウェディングドレスを試着した時です。ネットで選びに選び抜いたドレスを着て、主人に見せたその時！ボソッと言われたんですよ、「意外と…目立つんだね…背中…」って！！自分でも鏡を使って見てみたら、真っ白くてツルツルの生地からのぞいた私の背中には、想像以上の産毛が！大至急、会社の近くの銀座カラー金沢駅前東店を予約しました。金沢市内しかも北陸ではまだここだけだったので、すぐ予約が取れるか不安だったのですが、すんなり予約が取れました。最新のマシンを使っているので、1人あたりの施術時間が短縮されたんだと聞いて納得しました。金沢でも東京と同じレベルの施術を受けられるなんて！おかげさまで、お気に入りのドレスで挙式できました。</p></dd>
</dl>
-->

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金沢駅前東店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 ネイリスト<br></span>
</dt>
<dd itemprop="reviewBody"><p>ネイリストという職業上、お客様の手を見る機会が多いのですが、せっかく爪をキレイにしたのに指毛が気になる方もいて…。お客様の指毛を気にしていたら、自分の手がとっても気になるようになりました。それから脱毛クリームやシェービングなどで処理を試みたのですが、どうやら私の指毛は多いし太いようで、毛穴がプツプツ目立ってしまって…。他人は気付かないかもしれませんが、やはりお客様の手本となる完璧な手元にしておきたいので、毛穴を目立たなくしたい！と強く思うようになりました。今では、銀座カラーのおかげでだいぶ目立たなくなってきたのでひと安心です。もっと早く気付いてやっておけば、あんなに悩まなくてもよかったのになぁと今更ながら思います。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新潟万代シテイ店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 美容師<br></span>
</dt>
<dd itemprop="reviewBody"><p>まだスタイリスト見習いだった頃に、電車内の広告で銀座カラーを知りました。もっともっと身だしなみには気をつけないと！と思っていたところだったので、運命の出会いですね(笑)。ワキと腕と足でいいかな？VIOはどうしようかな？お金足りるかな？と、いろいろ迷っていたら、予算と施術したい箇所に合うコースをいくつか紹介してくれました。私は痛がりなので、初回はかなりドキドキ。でも実際やってみたらパツッ！という感じの、はじかれたような感覚があっただけ。自分でもツルスベになっていくのがわかるので、サロンに通うのが楽しくなってきちゃいました。ここまで手ごたえがあると、もうやめられません～。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新潟万代シテイ店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>客先に向かう途中で派手に転んでしまい、病院に行ったんです。詳しく検査をすることになり、着替えをしていた時に、足やVIOの処理をさぼっていたことを思い出したんです！普段はパンツスーツが多いし、忙しいし、疲れているし、眠いし。もともとズボラなこともあって…毛の処理をさぼってました。看護師さんもお医者さんも口には出さないけど、「この子、だらしないなあ」って見られているようで恥ずかしくて…。時間を巻き戻せたらいいのに！って思いました。同じ会社の子に話したら、銀座カラーのスタッフさんがすごく親切だって教えてくれたので、すぐにWEBから予約。ズボラな私でも続けられるようにアドバイスしてもらえました。仕事帰りに行きやすい場所にあるので、とても助かっています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新潟万代シテイ店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 デザイナー<br></span>
</dt>
<dd itemprop="reviewBody"><p>お試しで何件か脱毛体験はしていたのですが、そろそろ本格的に脱毛がしたいと思い銀座カラーに通いはじめました。どうせやるなら、しっかりキレイにしたかったので、全身脱毛と保湿ケアを選びました。施術時間は本当に一時間程度で、スタッフの方は丁寧なのに手際がいい！さらに保湿ケアをしてもらったら、自分のお肌じゃないみたいにモッチモチに！時間がきっちりしていて、パウダールームもあるから、お出かけ前でも利用できそうだなと思いました。早くツルツルになって友達に自慢したいです！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新潟万代シテイ店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>学校で噂になっていた銀座カラー。脇毛を剃ってもプツプツしていたり、黒ずんだりしてしまっているところなどが気になっていたので、オープン後すぐに相談にいってきました。プツプツしているところは毛穴が原因で、脱毛をすると毛穴が引き締まってきれいになること、自己処理のメリットとデメリットなど、私が気になっていることを丁寧に教えてくれました。何より、サロンの雰囲気が可愛らしくて、すぐに気に入っちゃったんです！まだ始めたばかりですが、2週間後スルっと毛が取れる感じが快感で、次が楽しみです！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新潟万代シテイ店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>夫の転勤で新潟に引っ越してきたので、サロンを新潟万代シテイ店へ変更してもらいました。新店舗ということもあって、店内はとってもキレイ！施術の質も他店舗と変わらず丁寧＆スタッフの方も話しやすくて大満足です。銀座カラーの技術と雰囲気が気に入っていたので、タイミングよく転勤先に新店舗ができて、本当に運がいいなと思いました！私は理想の肌にだいぶ近くなっているのですが、新潟でお友達ができたらオススメしようと思います。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="つくば店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>ゼミ旅行で温泉に行ったときに、友達の背中がツルツルでビックリ！話を聞くと、「脱毛サロンに通ってるんだー。」って教えてくれました。今まで自己処理だけだったんですけど、サロンで脱毛したら楽になるなーと思って、旅行から帰ったらすぐにネットで調べました。すると、つくばに銀座カラーができてることを発見！茨城県だと水戸にあるのは知ってたのですが、つくばにもできてるなんて！早速つくば店で無料カウンセリングを予約しました。カウンセリングをしてくれたスタッフさんがすごく親切で、脱毛について丁寧に説明してくれたのでとっても安心しました！まだ通い始めたばかりですが、みんなに自慢できるくらいにツルツルなお肌を手に入れたいと思います！</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="つくば店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>大学生になるのをきっかけに銀座カラーで脱毛を始めました！今まではカミソリをつかって自分で処理してたんですけど、キズができたり、荒れちゃったりで、毛が生えてこなければいいのに～って(笑)脱毛ってすごく高いイメージがあったんですけど、銀座カラーだと20歳未満は都度払いが利用できて、アルバイト代でやりくりしながら通えちゃうので助かってます！つくば店のスタッフの方は美意識が高くてお肌もきれいで、脱毛についてだけじゃなくて、美容についても色んなアドバイスをもらっています(笑)今ではノースリーブみたいな肌の出る服を着れる季節が楽しみです！</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="つくば店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>昔から毛が濃いのが悩みでした。脱毛したいとは思っていたのですが、強引な勧誘をされるんじゃないかとか、思った以上の金額になってしまうんじゃないかとか、不安が多くてなかなか踏み出せないでいました。あるとき、友人から銀座カラーを紹介してもらい、話を聞いてみると、とても満足している様子だったので、おそるおそるカウンセリングに行ってみることにしました。銀座カラーは本当に強引な勧誘はなく、私に合わせたコースを提案してくれました。価格も思っていた金額より安いくらいだったので、契約を決めました。最近は目に見えて毛が少なくなってきました。背中を押してくれた友人と、長年の悩みを解決してくれた銀座カラーに感謝です。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="つくば店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>私が今住んでいる茨城県南部のあたりには今まで大きな脱毛サロンがあまりありませんでした。銀座カラーができたことを知って、試しにカウンセリング予約をしてみました。下調べで、ホームページを見て全身脱毛の金額を確認したのですが、本当に全身くまなくやってくれるのか、思わぬ追加料金がかかってしまわないか心配でした。でも実際につくば店に通ってみると、全身余すところ無くやってくれるし、追加料金や強引な勧誘もなく、安心して通えます。周りの人たちにもお勧めしています。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="つくば店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>仕事が忙しく、ムダ毛の自己処理にあまり時間を割けないのが悩みでした。脱毛サロンで脱毛すれば楽になるかもと思い、いろいろなサロンのサイトチェックしてみて、その中でも銀座カラーの全身脱毛が気になり、カウンセリングに行ってみました。他のサロンでは全身を複数回に分けて脱毛するところもあるようですが、銀座カラーは1回で全身脱毛ができるのでわずらわしさがありません。それに1回当たりの時間も短く、予約も取りやすいので、スケジュールが立てやすいのも助かっています。銀座カラーは「女性のために」を体現した脱毛サロンだと思います。女性の頼れる味方です。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="つくば店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>最近、知人の結婚式でドレスを着る機会が多くなってきました。ドレスを着たとき背中が出るので、キレイにしておきたいなーと思い、友達に銀座カラーをオススメしてもらって通い始めました。友達は学生時代に都内で通っていたみたいですが、茨城県にも銀座カラーがあるとは！つくば店は家から近いし、駅からも近いので通いやすいですね。背中は自己処理が難しいので大変助かってます。また、オプションの保湿ケア、美肌潤美のおかげでお肌がもちもちになってきました。これでまた結婚式の案内が来ても安心です！いつか分からないですが、自分の結婚式のときも焦らなくて大丈夫ですね(笑)</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="つくば店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>肌寒くなってきたある日の朝、急いでストッキングをはいて出かけたら、毛が濃いのでストッキングの上からでもすね毛が丸見え…友達にも笑われてとても恥ずかしい思いをしました。ズボラな私は自己処理をサボりがちなので、脱毛サロンでの脱毛に頼ることにしました。つくば店に通っているのですが、内装がオシャレで、スタッフさんも皆さんキレイ！駅から近くて通いやすいので、億劫になることもありません！自己処理から開放されて晴れ晴れとした気分です！あの時笑ってきた友人にも、「銀座カラーいいよ！」ってオススメしておきました！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="水戸駅前店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>物心ついた時から、体毛が濃いことがコンプレックスでした。そんな私の様子を見た母が、ネットで色々と探してくれて大学の合格祝いに銀座カラーに連れて行ってくれたんです！私の場合、自己処理で肌が傷んでいたみたいなのですが、スタッフの方のアドバイスで肌のコンディションを整えることから始めました。通うたびに毛が目立たなくなっていったので自己処理の回数も減り、今では肌の調子もだいぶ良い感じです。毎朝シェービングに費やしていた時間も無くなったので、連れて行ってくれた母にはとても感謝しています。私と同じように毛の濃さで悩まれている方は、一度相談に行ってみたらいいのに！って思っています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="水戸駅前店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 秘書<br></span>
</dt>
<dd itemprop="reviewBody"><p>元々肌の色が薄いため、どうしても脚の毛が目立ってしまうことが私のコンプレックスでした。ストッキングで隠したり、脚用ファンデを使ったりしていたのですが、いつかちゃんと脱毛したいなと。仕事にも慣れてきて、やっと自分のペースで仕事を進められるようになったので、念願のサロンに通うことにしました！ずっと行ってみたかったので迷いや不安はあまりなく、むしろワクワクしていて（笑）。実際、そのワクワク感に裏切りは無く、以前より毛が気にならなくなり、自己処理のペースも減りました。実は最近、もっと欲が出てきて腕も顔もやりたいなと思うようになり検討中です…どうせなら、初めから全身やっておけばよかったかな（笑）。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="水戸駅前店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>休日まったりしていたら、「お前、ひげが生えているぞ(笑)」と急に夫に言われました。それ以来、口の周りの産毛の量が気になりはじめて…。毛のサイクルも以前より早くなったような気がして居ても立っても居られなくなり脱毛サロンに通おうと決心しました。でも、ひげが悩みだなんて恥ずかしくて言いづらいなぁと思っていましたが、スタッフの方はとても親身に対応してくださり、雰囲気もとてもよいところなのでお世話になることにしました。また、サロンというと勧誘がすごいイメージがあり少し怖かったのですが、そんなことは無く、私に合ったコースを提案してくれました。お話もとても勉強になるので、毎回楽しく通わせていただいております。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="水戸駅前店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>20代半ばから急に結婚式ラッシュが始まり、露出が多い服を着る機会が増えました。特にお気に入りのワンピースは背中が開いているので、妹にシェービングを頼んでいました。ある時タイミングが合わなくて自分でやってみたんですが失敗してしまい、傷跡をつくり余計目立ってしまうことに…。これからも何件かパーティがありますし、いずれは自分の結婚式でも背中の毛を気にすることなくドレス選びたい！と思ったので、銀座カラーさんに通うことに決めました。通いだしてから知ったのですが、結婚式の1カ月前の人は施術してもらえないとか。残念ながらまだ結婚の予定はありませんが、今から始めていてよかったなと思いました。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷道玄坂店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 デザイナー<br></span>
</dt>
<dd itemprop="reviewBody"><p>育休が終わって職場復職するので、子どもの送り迎えで利用している渋谷駅周辺でよい脱毛サロンはないかなと探していました。サイトの口コミやママ友の情報を参考にしながら、いくつか脱毛サロンのカウンセリングを受けたのですが、新しくてキレイでリーズナブルなところとスタッフさんの対応が気に入り、銀座カラーの渋谷道玄坂店に決めました。子どもの送迎や通院などに合わせて予約ができるので、とても便利で助かっています。全身と顔をやってもらっているのですが、最近顔色がワントーン上がったようで、ママ友に褒められました！お化粧のりもいい感じで、時短できて助かっています。キレイな働くお母さんを目指して、これからも通い続けます。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="高崎店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>お客様とすごく近い距離で商談をすることが多々あるので、最近、手の甲や指の毛が気になり始めていました。私自身が恥ずかしいのもそうですが、万が一、処理し忘れた手を見られて、お客様にマイナスイメージを与えてしまうのも嫌で…。同僚に相談してみると、以前銀座カラーで脱毛していたとのことで、私も通うことにしました。店内は清潔感があってオシャレで、脱毛のお手入れは痛みもなくてリラックスして受けられます。キレイになっていく自分の肌を見て自信がついてきました。おかげ様で仕事もプライベートも充実しています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="高崎店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>仕事中、パソコンを使っている時に腕まくりをするのですが、ふと伸びをしたときに腕から処理をし忘れた毛がそよそよ…これはマズいと思って脱毛することに。腕だけ脱毛するつもりでカウンセリングに行ったのですが、アウトドア派の私は、夏に海やプールに行くのが好きなのでvio、脚やワキも…と要望が膨らみ、結局後から足すよりもおトクなので、イッキに全身脱毛をすることにしました。高崎店は群馬県最初の店舗だそうで、新しいもの好きの私にはぴったり！まだ通い始めたばかりですが、脱毛後に少し経ってから毛が落ち始めて、ちょっとずつ効果を実感しています！今から夏が来るのが楽しみ～！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="高崎店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>就活が無事に終わって、社会人になる準備として、銀座カラーで脱毛を始めました。お店は私が通学で使っている高崎駅の近くにあるのでとっても通いやすいです！脱毛自体、もっと痛いのかなーと思っていましたが、あたたかい程度で私は全然気にならなかったです。オプションでつけたアフターケアの美肌潤美も気持ちよくてお気に入りです！銀座カラーで脱毛を始めてよかったです！周りの友達にも銀座カラーをおすすめしています！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="高崎店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>サークルの先輩と女子会をしているときに、お肌のケアの話になって、どうしてるのか聞いてみると「脱毛サロンに通ってるんだー！」って。脱毛サロンは社会人になってお金に余裕がある人が行くところだと思い込んでいたので、思わず「えっ！？」って言っちゃいました(汗)詳しく聞いてみると銀座カラーに通っているとの事。広告でみたことあるから私でも知っていました。自己処理が面倒くさいと感じていたのでいい機会だと思って、私も脱毛を始めちゃいました！銀座カラーは学割や都度払いがあって学生にも優しい！バイト代で無理なく通えるので助かってます！スタッフさんは良い方ばかりでお手入れ中はリラックスできちゃいます。高崎店は通学の定期圏内で駅からも近いので通いやすいです！後輩ができたら先輩としてオススメしたいです！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="高崎店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 美容師<br></span>
</dt>
<dd itemprop="reviewBody"><p>仕事中、腕まくりをしている時間が多いので、腕のムダ毛処理は特に気を使っています。自己処理はシェーバーでやっているのですが、時間がかかったり、たまに傷つけてしまったりするのが悩みでした。しかし、群馬県にも銀座カラーができたという話を聞いていくことにしました！銀座カラーに通い始めてから、毛が気にならないぐらいまでになってきました。おまけに美肌潤美のおかげで、乾燥肌が改善してきた気がします！私自身満足度がすごく高いので、同じ美容室の子にも銀座カラーの脱毛をオススメしちゃいました！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="高崎店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>アルバイト先が高崎駅なので、高崎駅で脱毛できるサロンを探していたところ、銀座カラーを発見し、通うことに決めました！銀座カラーは予約が取りやすくて、アルバイトのシフト前やシフト後に効率的に予約を入れてスケジュール管理ができました。脱毛に痛みはほとんどないし、スタッフさんと楽しくお話ししてるとあっという間に終わっちゃう感じです！バイト仲間にもオススメしてます！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="高崎店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>私は顔の産毛の処理をシェーバーでやっていたのですが、冬場すごく乾燥するようになったり、切れてしまったり、トラブルが絶えませんでした。ネットで情報収集していると、脱毛すると自己処理による負担が減り、顔の肌質が良くなると言う情報をみて脱毛を始めました！産毛がなくなり始めて、化粧のノリが格段に良くなりました。アフターケアのミストも気持ちよくって、赤ちゃんみたいな触りたくなる肌になった気がします！顔の自己処理でお悩みの方にオススメしたいです！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷道玄坂店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 フリーター<br></span>
</dt>
<dd itemprop="reviewBody"><p>昨年、学生時代の友人たちと温泉旅行に行くことになり、みんなで温泉に入ったのですが、みんなのアンダーが薄く、自然のままの自分の姿が恥ずかしくなり銀座カラーのカウンセリングを受けることにしました。VIOを処理しているなんて芸能人か意識高い系の人だけかと思っていたけど、普通の子もサロンで処理しているんですね…どうせやるなら、VIOだけではなく全身をやりたい！と探してはみたものの、どこも高く、友人が通っているサロンではなかなか予約ができないから大変だとも言われ…。そんな時目に入ってきたのが銀座カラーの広告で、スタッフの人も感じがよかったのでここに決めました。月々の金額も手が届かないわけでもないし、予約も割りと思い通りに取れ、友達にはじめからいいサロンでずるいと言われてしまいました（笑）これからもよろしくお願いします！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷道玄坂店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>平日のお休みが取りづらいので、駅から近いことと予約の取りやすいところが決め手になりました。全身脱毛が1回60分程で終わるのは、本当に助かります。さらに、HPには痛くないとは書いてあるものの、他のサロンで痛かったので気になっていたんですが、痛さはあまり感じず、ひと安心。そして、何よりお気に入りなのは、オプションで付けた保湿ケアです！以前通っていたサロンでは施術後乾燥しがちだったのに、今は施術前より肌がモチモチになっているので思わず自分で触ってしまいます。自己処理をついサボってしまうズボラ女子なので、早く毛量が減り、女子力が少しでも回復することを期待しています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷道玄坂店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 アパレル<br></span>
</dt>
<dd itemprop="reviewBody"><p>お店に入ってすぐ、雰囲気が気に入りました。新店舗ということもあるかもしれませんが、キレイでおしゃれ！やっぱり、同じ脱毛をするなら、気分が上がるところがいいですよね！しかもお部屋ごとに壁紙が違うみたいで、私が入ったのは淡いブルーの部屋とシックなベージュの部屋でした。どちらも居るだけで女子力がUPしそうな空間で、他にどんなお部屋があるのか知りたくなりました！それにスタッフの方の対応や金額、サービスも魅力的で…。私も接客業なので、やっぱり対応とか気になっちゃうんですよね。でも、心配事や質問にもきちんと答えてくれたので、ここなら安心できるなぁと思いました。まだ始めたばかりですが、母が気になると言っていたので今度一緒に行ってみます！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="錦糸町店">
  <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

    <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">

    <span itemprop="name">20代 事務<br></span>
  </dt>

  <dd itemprop="reviewBody"><p>全身脱毛し放題のプランで錦糸町店に通っています。最初は腕とワキだけにしようと思ってたんですけど、カウンセリングでお話を聞いて、こんなに安くて通い放題なら全身脱毛がいいかな！と思って思い切って全身脱毛にしちゃいました。なによりすごいなーと思ったのが、脱毛のアフターケアの美肌潤美！保湿力バツグンでお肌がぷるぷるになるので感動です！お手入れ後のキレイな肌を見て、銀座カラーにして良かったなーって思いました！
  </p>
  </dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="錦糸町店">
  <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

    <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">

    <span itemprop="name">20代 接客<br></span>
  </dt>

  <dd itemprop="reviewBody"><p>少し前、化粧のりが悪いなーって思ってネットで調べたら、どうやら産毛のせいなのかも…ということで、電車の広告をみて気になっていた、銀座カラーの無料カウンセリングを予約しました。脱毛だけのつもりだったけど、美肌潤美という保湿ケアがあるのを知ってやってみることに。<br>
    いざやってみると、すごーく潤っているのを感じました！通える周期も短いので、効果を実感できるのが早いですよ！パウダールームでお化粧できるのもポイント高いです。最近は化粧のりが良くなってきて、楽しくなっていろんなコスメを集めて試すのがプチブームです！同僚にも「最近キレイになった？」って言われちゃって、心の中でガッツポーズしちゃいました(笑)
  </p>
  </dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="錦糸町店">
  <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

    <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">

    <span itemprop="name">20代 アパレル<br></span>
  </dt>

  <dd itemprop="reviewBody"><p>職業柄、身なりにかなり気を遣うので、美容には時間をかけています。バスタイムのケアやネイル、ムダ毛の処理…もっと時間が節約できたらいいのに～！って思っていました。東京にはたくさん脱毛サロンがありますが、職場の近くに銀座カラーがあったな～と思い出して、行ってみることにしました！ <br>
    錦糸町店のスタッフの皆さんは親切で話しやすくて、自分も見習いたいくらい！ガールズトークに花を咲かせちゃうこともたまにあって…(笑)脱毛の効果が出てきたおかげで、他の事に使える時間が増えました！銀座カラーで脱毛を始めてよかったです！
  </p>
  </dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="錦糸町店">
  <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

    <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">

    <span itemprop="name">20代 接客<br></span>
  </dt>

  <dd itemprop="reviewBody"><p>彼氏と部屋でのんびりしているときにふと、「言いにくいんだけど、ヒゲ生えてるよ…」と言われてショック！意を決して脱毛をすることにしました。<br>
    前に常連のお客様と美容の話になったとき、銀座カラーに通っている話をされたことを思い出し、私もとりあえず行ってみるかとカウンセリングを予約しました。実際に行ってみると店内の雰囲気が良くって、スタッフさんも優しかったので、本当は何店かカウンセリングを受けて比較しようと思ってたのですが、銀座カラーで即決しちゃいました！脱毛は痛みもほとんど感じなく、時間も短いので苦になりません！予約がとりやすいのも助かってます！最近は彼氏に「前よりもっとキレイになったね。」って言われてすごく嬉しかったです！もっと肌をキレイにして、もっと惚れ直してもらおうと思います！
  </p>
  </dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="錦糸町店">
  <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

    <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">

    <span itemprop="name">10代 接客業<br></span>
  </dt>

  <dd itemprop="reviewBody"><p>女子会で脱毛の話になって、周りはみんなやってるのに、自分だけやってないことに気づいてびっくりしました。自分もやらなきゃと思って、CMが印象に残っていた銀座カラーへ行くことにしました。<br>
    脱毛サロン初体験だった私は、何から何までどきどきしっぱなしでしたが、やさしいスタッフさんが声をかけてくださるので安心でした！錦糸町店に通っていくうちに自己処理の負担がぐっと減って感動です！東京には山ほど脱毛サロンがありますが、私は自信を持って銀座カラーの錦糸町店をオススメします！脱毛のついでに映画を見たり、ショッピングを楽しめるのも錦糸町ならではですね！もっときれいな肌になってオシャレを楽しみたいなー！
  </p>
  </dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="錦糸町店">
  <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

    <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">

    <span itemprop="name">20代 営業<br></span>
  </dt>

  <dd itemprop="reviewBody"><p>通勤中、電車のつり革をつかんだときに、最近忙しくてワキの毛の処理をおろそかにしていたことを思い出して、冷や汗がダラダラ…前に座っている人に見られたかもと思うと恥ずかしくて、穴があったら入りたいって感じでした…そのままふと上を見ると、銀座カラーの広告が！あまりのタイミングの良さに運命を感じて、休みの日に行ってみることに。<br>
    話を聞くと、思っていたよりおトクで即決！通っていくうちにどんどん毛が抜けてきて、自己処理の負担がぐっと減りました！今ではつり革も自信をもって掴めます！
  </p>
  </dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="錦糸町店">
  <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

    <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">

    <span itemprop="name">20代 受付<br></span>
  </dt>

  <dd itemprop="reviewBody"><p>夏にサンダルを履いて出かけたら、足の親指から毛が伸びているのを友人に見られました…普段、仕事の時にはパンプスを履いているので、処理をさぼったツケが…。<br>
    腕やワキの自己処理も面倒くさいなぁと感じていたので、良いタイミングだと思って、今は銀座カラーに通っています。錦糸町店は駅に近くて通いやすい！スタッフさんも優しいし、脱毛も痛くない！ついでにショッピングができたり、映画をみたりできるのもいいですね。この調子で通ったら、次の夏には焦る必要なしですね！
  </p>
  </dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="仙台店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 美容師<br></span>
</dt>
<dd itemprop="reviewBody"><p>顔の産毛が濃い家系なので、物心ついた頃1週間に1回はシェービングしていました。ヒゲみたいですね（苦笑）。ニキビができると剃りにくいし、あたると痛くてキズになるし。マスクで隠すわけにもいかず、なるべく目立たない肌色の絆創膏を貼っていたら、お客様に「大丈夫？」って心配されてしまって。それがきっかけで顔の脱毛をしてみようと、職場や友人に聞いてまわったんです。結果、おすすめの声が一番多かったのが、銀座カラー仙台店でした。駅前にあるので、脱毛後にお買い物できるのが嬉しいポイントです。周辺には、美味しいご飯が食べられるお店もたくさんあるので、ついつい寄り道しちゃいます。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="仙台店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>私はズボラな性格なので、ムダ毛に対しては、外出するときに恥ずかしくない程度に仕方なく処理orパンツや長袖でごまかしていました。でも、よく考えてみれば、脱毛して目立たないようにしてしまえば、そもそも処理しなくていいということに気付いたんですよね。幸運なことに帰り道に銀座カラーさんがあるので通うことは苦ではないし、施術中の痛さもほぼ無いので落ち着いた部屋でひと休みする感覚で通っていました。今では特にムダ毛が気にならないのでほぼ放置しています。ズボラ度が増してしまうのでは？と思っていたんですが、ムダ毛を気にせずファッションを楽しむようになりました！ズボラな私にも親身になって対応してくれ、自信が持てる肌にしてくれた銀座カラーさんに感謝です。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="盛岡菜園店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 アパレル<br></span>
</dt>
<dd itemprop="reviewBody"><p>以前は、脱色剤で自己処理していたのですが、脱色中にテレビを見ていたらうっかり忘れてしまい、水ぶくれになってしまったことがありました。それ以来、脱色剤はやめてシェービングで処理していたのですが、たまにカミソリまけしてしまうのが悩みで…。そんな時、同僚に誘われて行ったのが銀座カラーさんでした。HPでは安そうだけど、実際はどうなのかな？とカウンセリングに行ってきたのですが、店内は落ち着いた雰囲気でコースもいろいろあり、自分に合ったものを見つけることができたので通うことにしました。メイクブースもあるので、デートの前でも安心ですね。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="盛岡菜園店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>進学で盛岡に出ることになったのがきっかけで、銀座カラーに通い始めました。正直、痛そうだし怖かったけど、スタッフさんは親切だし少しずつ様子を見ながらやってもらえるので、全然平気です。コンプレックスだった腕の脱毛を始めてみたら、毎日のシェービングがとても面倒で時間の無駄だったことに気づきました。最初は不安でしたけど、通い始めて本当に良かったです。これからも絶対続けます。私たち学生の間でも、サロンでムダ毛のお手入れをすることは常識。どんなサロンが良いかなどよく情報交換しているので、私は銀座カラーを薦めています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="札幌店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 歯科衛生士<br></span>
</dt>
<dd itemprop="reviewBody"><p>患者さんに、数十センチの距離で顔や手先などを見られる仕事なので、顔や指、腕のケアにはシビアになりがち。マスクやグローブ、制服で隠れていますが、ちょっとした動作でずれてしまった時とかに剃り残しがバレてしまうこともあるので油断は禁物です。忙しくなるとサロンに通う時間も限られてしまうので、平日21時までというのも大きなメリットだし、施術時間も他のサロンより短いんじゃないかな？　施術後、そのまま食事に行けるのも、食いしん坊の私には大きな魅力ですね。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="札幌店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>同じヨガ教室に肌がきれいな女性がいるんです。気になって、どこで・どんなお手入れをしているのか聞いてみたら、銀座カラー札幌店の常連さんでした。早速、サイトから予約しました。行きつけのサロンを変えるのは勇気が必要だったけど、後悔していません。最初のカウンセリングが、とても親切だったので安心できたし、なにより自己処理で黒いブツブツができていた両ひじの下が、今はツルツルに！　この間なんか、会社の後輩ちゃんから「○○さん、いつも肌がきれいですよね。どこに通っているんですか？」って聞かれちゃいました。今度は私が見られる番！？ (*/∇＼*)</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>いつか脱毛したい！と高校の時からずっと思ってました。 そして今年から大学に入学し、バイトも始めて自分でお金を少し稼げるようになったのでついに始めようと決心しました。 色んな脱毛サロンをインターネットで調べたのですが、なによりも銀座カラーさんはすすきの駅すぐ近くにあった事が最終の決め手となりました。バイト先がすすきので、大学からも自宅からも南北線直通で来れるし私にとっては立地最高！あと、ホームページが可愛かったので惹かれてしまいました（笑）<br>
正直初めての脱毛で、痛かったらどうしようとかなりドキドキしていたのですが、想像よりも全然痛くなかったです。<br>
私は乾燥肌も悩みの種ですが、銀座カラーさんは保湿ケアもしっかりした施術をしてくださったので、いつもより腕とか乾燥がマシな気がします。<br>
まだこれから長くて寒い冬が来るのに、もうすでに半袖の時期を待ち遠しく思ってしまっています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 ネイリスト<br></span>
</dt>
<dd itemprop="reviewBody"><p>天神に買い物に行くことが多く、そのついでに脱毛がしたいと思って銀座カラーを選びました。昔他店で両脇と足腕を脱毛したのですが、綺麗になりきる前に終わってしまって。ポツポツ生えてくるのが鬱陶しいのと、自己処理し忘れることがあるので、いっそのこと全身綺麗に脱毛しようと決意。広告で銀座カラーを見て、全身脱毛のお得なプランがあるのを知りました。期間限定プランと言われるとつい気になっちゃいます（笑）通ってみて思うのは、店内の内装のセンスの良さと、接客の良さですね。店内は清潔感がありながらガーリーな感じが本当に可愛い。スタッフさんはいつも元気で笑顔なので、こちらまでニコリとしちゃいます。もちろん技術力も申し分なしです。しっかり当ててくれるので当て漏れの心配もないですし、術後のケアや声がけもしっかりされていて好感度アップです。これからも最高のサービスを期待しています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>天神にはたくさん脱毛サロンがあるので、サロン選びが大変でした。でも、銀座カラーの天神店はオープンして新しいし、内装の雰囲気が他のサロンと違って凄い素敵！キラキラのシャンデリアに目を奪われてしまいました。よくよく友達の体験談なんて聞いてみると、銀座カラーの効果の高さや価格の手頃さに満足しているようで、他店を辞めて通っている子もいるくらいだから、人気のお店なんだろうなと。とりあえず話を聞きに行って、駄目そうだったら違うお店に行けばいいかと思って、カウンセリング予約を入れました。図解を使ったスムーズな説明プラス、スタッフさんの笑顔で私としては通う気満々だったのですが、人気店ならではの気になる点、予約が取れるかどうかというところがはっきりしなくては契約できないなと思っていました。実際予約を見たところ、結構埋まってはいました。ただ私は平日休みのこともありますので、複数候補日の中から予約を入れることができたので、予約の点は問題ないかと思いました。これから施術になりますが、自分の肌の変化が楽しみです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>ずっと気になっていたVIOラインを銀座カラーさんで脱毛しました。夏にプールへ遊びに行った時、一応インナーショーツは着用していたけれどやっぱり毛がはみ出てしまっているのではないかと不安で仕方がなかったです。Vラインは自分で少し剃刀を使ってお手入れをしてはいたのですが、I・OはVよりもさらにデリケートな部分になるし自分でやるのは怖かったので一切触らずにいました。しかもちょっと人には相談しにくい部分でもありますし、Vラインも含めて毛の量や形についてどうしようと迷っていたのでスタッフさんのアドバイスを聞きながら自分に合ったコースを選択しました。やっぱり専門の方に相談してよかったです。絶対次の夏までにはVラインの心配がないくらいまで通って、不安なく思いっきり遊びたいと思います。脱毛が完了したら新しい水着を買いたいなと思っているので今からもう楽しみです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>以前東京に出張にいった際に、電車の中や看板で真っ赤な赤ずきんが目に入りました。よく見ると脱毛サロンの広告でした。東京はやっぱりおしゃれだなあと思っていたのですが、それからしばらくして、八丁堀へ遊びに行った時なんとあの看板を発見。聞けば最近できたばかりということで、なんだかうれしくなりました。<br>
私は別の脱毛サロンに通っていたことがあり、脱毛経験者だったのですが、月日が経って少しずつまた脇毛が気になり始めていました。これはいいきっかけだと思いすぐに無料カウンセリングを予約。店員さんは丁寧で明るく、私と同世代だったので話しやすかったです。また、料金の方は私が以前通っていたところよりも安い。もっと前から銀座カラーさんが広島にあれば全身脱毛ももっと安く済んだはずなのに…とすこし悔しい思いをしながらも、脱毛未経験の同僚にいいサロンないかと相談されたので銀座カラーさんを紹介しました。そしたらなんと紹介キャンペーンで私も同僚にも特典が！脇もつるつるになって、店員さんも良い感じで、特典も得られていい事づくめでした。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>好きな人が出来て、綺麗になりたい、可愛くなりたいって思ったんです。その人はバイト先の先輩で私はいろいろ教えてもらう機会が多いからか、今までは全く気にしなかったのに自分の顔のうぶ毛、とくに鼻下が気になり始めました。接近されたときにどうしても剃り残しとかないかが毎回不安で…<br>
思い切って銀座カラーで脱毛することにしました。噂には聞いていましたが、化粧ノリが見違えるくらいに良くなって驚きでした。自分で剃っていた時には剃刀負けして反対に荒れてしまうこともしばしばありましたので、やって本当によかったと思います。よく考えたら自分の肌に刃物を当てているわけですから、そりゃあ肌は傷つきますよね。一番見られるところなのにそういう意味では気を使っていなかったなと反省しています。好きな人とのことはまだ特に進展とかはないのですが、これからもっと自分を磨きたいと思っているので、今は新たに別の箇所も脱毛しようかと検討中です。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">30代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>三重県津市から通っています。どうしても銀座カラーで脱毛したくて、一番行きやすい店舗がここだったので頑張って行っています。地元にも一応脱毛サロンはありますが、肌のことですからやっぱり有名なお店に行った方が何だか安心だと思い、銀座カラーにしました。県外ではありますが、アクセスも良くて助かっています。買い物をして帰るのも楽しみの一つです。私は全身ハーフ脱毛のコースで主に腕、脚、脇をお願いしています。個人的に気になっている部分は網羅していますし、料金にもかなり満足しています。脱毛し放題なので納得できるまで通いたいと思っています。実は一番嬉しかったのが、気になっていた脇の黒ずみが銀座カラーに通い始めてからなくなったことです。夏に半袖Ｔシャツでさえ着るのをためらっていたのですが、今年はなにも気にすることなくノースリーブの洋服も新しく買ったりしてお洒落をいっぱい楽しむことが出来ました！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 デザイナー<br></span>
</dt>
<dd itemprop="reviewBody"><p>名古屋にはたくさんの脱毛サロンがあります。私もかなり迷いました。インターネットで調べもしたし、職場の人の行ってた所はどんな感じだったかとか事前調査は結構したつもりです。その中でなぜ銀座カラーに決めたかというと、いろいろな要素はありますが一番は美肌潤美です。ただ脱毛するだけじゃなくて、高圧ミストマシンで様々な美容成分の蒸気を発生させ施術後のお肌に保湿をおこなってくれるというオプションメニュー。どんなに素敵な洋服を来ても肌の状態が良くなければ魅力は落ちます。もちろん普段から自己ケアはしていますが、どうせなら脱毛と一緒にできる美容というのも是非やりたいと思いました。実際やってみての率直な感想は「すごい！」の一言。これでもかというくらいのミストシャワーが出て肌を包んでくれます。そして照射後のお肌には心地よいくらいのひんやり感。もちもちすべすべのお肌になっていくのを実感しています。銀座カラーにしてよかったです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>友達が銀座カラーに通っていました。ある日「触ってみて」といわれ腕を触ると、つるつるすべすべ！その友達は特に寒い時期いつも乾燥肌に悩まされていたことを知っていたので、本当に驚きました。聞けば脱毛サロンで高圧ミストの保湿ケアをしてもらっているそうで、それのおかげで乾燥を防げているとのこと。私も興味を持ったので、とりあえず無料カウンセリングに行ってみようと予約をしました。友達がやってる美肌潤美という保湿ケアはオプションだったみたいで、最初はどうしようか迷いました。しかし当時全身脱毛のコースを契約した人は無料で1回体験できるキャンペーンをちょうどやっていて、そちらを試しにお願いすることに。いざ体験してみると終わった後のしっとり感ともちもち感が忘れられず、結局オプションで付けてもらうことにしちゃいました！お家ケア用の保湿ローションもあるので、サロンと自宅とダブルサポートで友達のすべすべお肌を追い抜きます（笑）</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 保育士<br></span>
</dt>
<dd itemprop="reviewBody"><p>Facebookで友達が「いいね！」を押していて、銀座カラーの存在を知りました。東京のお店かな？お洒落なサロンだなあ、と思って見ていくと岐阜県に店舗あり。しかも家から結構近い所！ホームページものぞいてみるとキレイそうなので、とりあえず無料だしカウンセリングだけでも行ってみようと本当にただ興味本位で予約しました。私は全く脱毛について無知だったので、剃刀による自己処理が良くないってことも知らなかったですし、脱毛すると毛穴が閉じてお肌もきれいになるということも初めて知って驚きました。カウンセリングにいったその日には決められなかったのですが、スタッフの方は嫌な顔一つせず対応してくれて安心しました。帰ってから自分でもいろいろ調べた結果、脱毛サロンに通うのがやっぱりいいと思い、後日申し込みに行きました。今までいわゆる自分磨きにあまり興味を持たずにここまで来てしまったけれど、お肌の改革から徐々に始めたいと思います。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>サークルの友達何人かにふと脱毛サロン行ったことがあるか聞いてみたところ、通っていたという人が思ったよりも多くてとても驚きました。実は毛が濃い方で、それがすごく嫌で頻繁に剃刀で自己処理をしていました。そのせいで剃刀で荒れてしまっていた肌もどうにかしたかったですし、もちろん根本の原因であるムダ毛の処理も楽になりたいとずっと思っていました。だから、知らない間にみんな脱毛サロンに行っていたと知り、なんだか取り残された気分になりました。今からでも遅くないと思い直し色々調べたり友達の意見を聞いたりして、決めたのが銀座カラーです。私の毛質的に脱毛し放題のコースは必須で、なおかつ施術時間も短いし、荒れたお肌のケアもできるコースが銀座カラーにはあったので言うことなしで決定でした。何度か脱毛に通っているとだんだん毛が少なくなっていくというか、細くなっているのか目立たなくなってきて、それが嬉しくて行くたびに次の施術が待ち遠しく思います。完全なつるつるすべすべお肌まであともう少し。濃い毛に悩んでいた私とはさよならです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">30代 主婦<br></span>
</dt>
<dd itemprop="reviewBody"><p>電車の中でつり革を掴んでいる時に目が合ったんです。女子高生と。しまった。処理してなかった…と気づきました。あまりにも恥ずかしくて、落ち込んで、その反動と勢いで無料カウンセリングを予約しました。平日の昼間旦那はいないし、子供も学校へ行っているのでその時間帯を選びました。自分でも正直今更脱毛サロン行くことに意味はあるのかと予約してから少し悩んだりもしましたが、やっぱりあの日のことが忘れられなくて。ドキドキしながら店舗へ行くと、内装はとっても綺麗で清潔感溢れる雰囲気で、スタッフさんも可愛い。更に緊張が増しました。しかし、カウンセリングは私のペースに合わせて話をしたり質問に答えてくださってよかったです。私と同じくらいの年代の人も通っているらしいと聞いてさらに安心しました。最後の心配の費用についてはパートで貯めたお金から払えるくらいリーズナブルだったので本当に助かりました。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 フリーター<br></span>
</dt>
<dd itemprop="reviewBody"><p>もともと腕や足の毛質はそんなに悩むようなものではなかったので、私は両脇だけの脱毛コースをお願いしていました。実は最後の施術の日、勧誘されるのでは…とちょっとドキドキしていました。私は両脇だけの脱毛コースでしたし、特にこういう場合ってだいたい別の部位の脱毛コースを勧誘されたり、美容品をおすすめされたりすると思っていたので。しかし、いつもの通りに施術が終了して、そのあとも心配していたようなことは一切なく終わりました。勝手に身構えていた分なんだか拍子抜けしてしまいました（笑）ホームページに書いてあった通り無理な勧誘もなく、表示値段以上のお金をとられることも一切ありませんでした。嫌な顔ひとつせず最初から最後まで丁寧にプロのサービスを徹底してくださって本当にうれしかったです。もし、また脱毛したいと思ったら私は絶対銀座カラーにします！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 アパレル<br></span>
</dt>
<dd itemprop="reviewBody"><p>普段仕事が忙しく休みもあまり取れないので、限られた時間のなかで通えるところを探していました。<br>
銀座カラーは全身脱毛1回の施術が60分くらいで完了すると聞いてこちらに決めました。そうはいっても実際は何回かに分けるのではないかと疑っていたのですが、本当に全身を1回60分程度で施術できて驚きでした。初めて行った日の帰りには気になっていた映画も観ることが出来て充実した休日を過ごせた気がします。急にシフトが変わってしまった時にも、24時間いつでもインターネットで簡単に予約可能だったのですごくよかったです。肝心な脱毛の方もばっちり。お風呂のタイミングで以前はムダ毛処理をしていましたが、今ではその時間も必要なくなりました。仕事は相変わらず忙しいけれど、つるつるのお肌をゲットできて身体だけでなく気持ちもすっきりして、モチベーション高く活き活きとした毎日を送れている気がします。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>脱毛して何よりも嬉しかったのが、彼氏に褒められたことです。褒められたといっても、「前よりもつるつるになったやん」とか言って腕などを触ってくる程度ですが（笑）今までは普段そんなにボディタッチもなかったので、ちょっと今さら感もあり恥ずかしく思っています。実際自分で触っても気持ちいいつるつる感と、潤いのある肌にとても満足しています。よくキャッチコピーとかで見る「赤ちゃん肌」や「透明感のある肌」という言葉は他人事だとずっと思っていました。しかし、脱毛に行って少しずつ綺麗になっていっていると感じると、心にも変化が生まれていつもより入念に化粧水を塗ったりボディーローションを使ってマッサージしたりと自分磨きに力が入りました。また、おしゃれにも気を使いたくなってきて、今までだったら選ばなかった雰囲気の服とかも気になり始めています。銀座カラーさんの「全身で恋をしよう」というキャッチコピーの意味をかなり実感しています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 受付<br></span>
</dt>
<dd itemprop="reviewBody"><p>受付の仕事をしているのですが、制服が半袖なんです。職場は空調施設がしっかりしてるので、年中半袖でいても大丈夫なのですが問題はムダ毛でした。学生時代も自分で剃ったりはもちろんしていました。しかし冬場は肌の露出も少ないのでほぼ放置状態でよかったのです。ところが、仕事着は1年中半袖。冬も関係なく腕を出さなければならないわけですから正直とてもムダ毛の処理が面倒でした。そこでもういっそのこと脱毛サロンに通ってしまおうと思い立ちました。どうせだからと選んだのは全身脱毛コースです。個人的にはちょっとV・Iラインが痛かったけれど回を追うごとにマシになった気がします。あとすごく驚いたのがお肌の状態です。自分で処理した際の肌より銀座カラーさんで脱毛した後の肌の方が断然すべすべつるつる肌になりました。秘密は保湿ケアだと教えていただいたので、これから自分でもさぼらずお風呂上りにはボディーローションを塗ってケアしようと思います。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 主婦<br></span>
</dt>
<dd itemprop="reviewBody"><p>夏は暑くて汗をかくし、少し寒くなってから行こうと決めていました。インターネットでいろいろ調べていたら、ちょうど銀座カラーさんがお得なキャンペーン中だったのでそのままカウンセリング予約をしました。店員さん曰く、脱毛治療中はあまり紫外線にあてるのもよくないそうなので、始めた時期は正解だったかなと思っています。私はもともと肌がそんなに強くなく、自分でムダ毛処理していると剃刀負けしてしまうこともよくありました。そういった心配もあったので、カウンセリングの際にはいろいろ質問をさせていただきました。どれも丁寧に答えてくださり、私に合った脱毛コースをちゃんと選ぶことが出来たと思います。美肌潤美というコラーゲンやヒアルロン酸等が入ったミストの保湿ケアもお願いしました。これが本当にびっくり。こんなにもつるすべ潤いのあるお肌が実感できるなんて。店員さんのアドバイスも聞きながら、普段からお風呂上りや乾燥が気になったときは保湿を心掛け、少しずつですが自己ケアの成果も出てきている気がします。ムダ毛もなくなり、美肌もゲットできてすごく嬉しいです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">30代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>私は脱毛といえばとても痛いというイメージを持っていました。昔、脱毛していた友人が痛いと言っていて、怖がりの私は絶対にやりたくないと思っていました。しかし最近の光脱毛なら輪ゴムでパチッとはじかれた程度の痛みで済むと聞き、興味を持ちました。光脱毛は永久脱毛と違って、しばらくするとまた少し毛が生えてくるということでしたが、脱毛し放題プランにすれば気になり始めた時にまた通えます、と説明を受けそちらにしました。やはり施術１回目は不安で、痛がりなことを店員さんに伝えるとジェルを厚めに塗ってくださいました。そのおかげか全然痛みを感じず「もう終わり?!」と思わず言ったほどです。あんなに怖がっていたのが恥ずかしくって、その日は店員さんと目を合わせられませんでした。当初両脇だけでお願いしていたのですが、始めてみてひじ下とひざ下も追加でお願いしようかなと今は考え中です。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 秘書<br></span>
</dt>
<dd itemprop="reviewBody"><p>わたしはすでに別のところで全身脱毛をしていたので、銀座カラーでは顔セットコースを選びました。正直別のサロンへ通っていた時は、顔まで脱毛する意味あるのかなと思っていました。しかし、顔の産毛は結構あって、化粧ノリもそれのせいでだいぶ変るということを知りました。せっかく顔を脱毛するなら、同時に美肌もゲットしたいと思い、肌ケアに力を入れている銀座カラーでお願いすることにしました！やってみると脱毛による美肌効果は本当にあって、以前と全然キメが違います。しかも銀座カラーの保湿ケアをしてもらった後のぷるつや感がすごいです。化粧ノリもよくなったし、透明感のあるお肌がまさか私のモノになるなんて、と感動しています。同僚の子にも、化粧品変えたの？なんて聞かれたりもして、銀座カラーで脱毛し始めたくらいしか変化ないよ！って言ったらとても驚いていました（笑）人に一番見られる部分だからこそ、脱毛してよかったと思います。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>スタッフの方が丁寧で優しくて、とてもよかったです。初めての脱毛だったので、不安もあって別のサロンのカウンセリングにも行ってみたんです。<br>
そこのお店も清潔感があり綺麗だったんですが、カウンセリングを担当したスタッフの人がなんだか不愛想で、ちょっと私は好きじゃなかったのです。そのあと別の日に銀座カラーさんのカウンセリングに行きました。脱毛のコースや料金、店舗の雰囲気含めてこちらの方が気に入ったのですが、何よりもスタッフの方の笑顔というか、対応が気持ちよかったです。施術してもらうには他人に肌を見せなければならないわけですから、スタッフの方との距離感や信用がとても大事になると私は思っていました。たまたま私にあたった人が良くなかったのかもしれませんが、そういう気配りから出来ているお店の方がとても印象は良いです。銀座カラーさんは通っている間もずっと変わらず、接客してくだいましたし安心して身を任せることが出来ました。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>誕生日に友人から銀座カラーの美容パックをプレゼントされました。大好きなゆずの香りのものですごく嬉しかったです。お肌ももっちりして大満足。なんで脱毛サロンなのに、美容パック？と疑問だったんですけど、脱毛だけじゃなく美容にも力を入れてるらしいと聞き、さっそくその友人に紹介してもらいました。むだ毛もなくなってお肌もぷるぷるになるなんて最高です。全身脱毛コースなのにいつも１時間くらいで終わるし、学校帰りに行けるのも嬉しいです。始めはパチパチッていう痛みが少しあったけど最近はそれもあんまりなくなって、時々施術中に寝てしまうくらい！ふと気づいたらもう終わっているなんてことがここ最近続いています（笑）ちなみに銀座カラーに通うきっかけとなった美容パックは行くたびに買って帰ってお家でのケアも怠っていません。どうしても冬場は暗めの服を着るので、お肌も一緒にくすんだ色に見えないように頑張っています！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 飲食業<br></span>
</dt>
<dd itemprop="reviewBody"><p>ずっと背中ニキビがひどくて学生のころからの悩みでした。ニキビケアの塗り薬とかも使用していましたが、すぐにまた別のところに出来てしまったり。背中が見えそうな服は絶対着たくなかったし、毎年夏は嫌でした。しかしある時たまたま銀座カラーさんのホームページを見ていて、脱毛で背中ニキビが治る可能性があるということを知りました。無料カウンセリングで話を聞いてみると、背中ニキビは毛穴にたまった皮脂が炎症を起こして出来てしまったり、こすりすぎて乾燥することが原因の場合もあるようでした。ムダ毛をなくすことで炎症を起こしにくくし、その上で保湿ケアを施していけば治るかもしれないと言われました。店員さんが本当に親身になって話を聞いてくださって、丁寧に説明もしていただいたので、やっぱり銀座カラーさんにお願いしようと思いました。時間はかかるみたいですが、脱毛だけに頼らず店員さんのアドバイスを受けながら治していこうと頑張っています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 デザイナー<br></span>
</dt>
<dd itemprop="reviewBody"><p>プロポーズをOKしてすぐに脱毛サロンを予約しました。式の日取り等その時には具体的に決まってなかったのですが、絶対ウェディングドレスは着たいし、やるなら早く行き始めなくてはとかなり焦っていました（笑）特に背中のうぶ毛が気になっていたのです。せっかくの純白のドレスなのに、お肌がくすんで見えるのはもったいない。一生に一度の晴れ舞台ですから、一番綺麗に見せたい、と思いました。もちろん選んだのは全身脱毛コースです。結婚準備などで忙しくなる中、1回で全身すべて施術してくれるし、しかもそれが60分程度で終わるのは非常に助かりました。幸い、わたしはそんなに毛が濃い方ではなかったし、式までは少し時間があったので、何とか結婚式までにある程度の脱毛は完了し、気になっていた背中も自信をもって見せられました。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>タレント養成所に通うことになって、これからの活動幅も広げるために全身脱毛することにしました。母はまだ自己処理でいいのではないかと言っていたのですが、剃刀でやっているとそれこそお肌にもあまりよくないし、脱毛をすることで肌が綺麗に見えるようになると聞き、もう絶対やる！と半ば強引に予約を申し込みました。<br>
綺麗なお店でスタッフの人もすごく優しくて、よかったです。1回の施術もすぐ終わるし、脱毛すると本当にお肌つるつるになりました。そんな話を家で毎回しているうちに、母もうらやましく思ったのか銀座カラーに通うことに。<br>
紹介キャンペーン利用して2人でかなり得しちゃいました！梅田には2店舗銀座カラーさんがあって、梅田曾根崎店の方が希望日に予約いっぱいでも梅田新道店を予約すればよかったので、2人でどんどん通えることが出来ました。今度ちょっとしたオーディションがあるんです。最終まで残れば水着審査もあるみたいなので、つるすべになったお肌とこれまでの努力を存分にアピールしてオーデション合格してきます!!</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>私は京都から通っています。阪急電車1本で乗換なく行けますし、自宅の最寄り駅から40分ほど。京都にもいくつか脱毛サロンはありますが、美肌潤美という銀座カラーさんオリジナルの保湿ケアに心惹かれたので、つけてもらいました。まだ京都には店舗がないようで、梅田まで毎回行くのはしんどいかなあとも思ったのですが、1〜2ヶ月に1回程度のペースで週末に大阪まで来るというのも気分転換になってます。だいたいいつも午前中に予約をお願いし、ランチをしてお買い物という流れにしています。さすが大阪ということもあってか、スタッフの方はとてもお話が面白く、流行にも敏感で行くたびにいろいろ教えていただいたりもしています。決め手となった保湿ケアもばっちりです。私は寒くなるととても肌荒れして、ひざ下とか粉吹いてしまいますが、今年はたっぷり潤いを得て乾燥に負けない肌で冬を乗り切りたいと思っています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>新しくできた店舗ということで、内装も白を基調とした高級感のある感じがすごくよかったです。なんだか特別なおもてなしをしてもらっている気分になりました。1回の施術で全身をやってもらえたし、1時間程度で終わったのでいい事ばっかりでした。<br>最初は他人に身体を見られるということに対して抵抗や戸惑いもありましたが、施術箇所以外は隠してもらえるし、手早く丁寧にしてくれました。そういう気づかいをしてくれることが何よりも良いサービスだと私は思います。 別の脱毛サロンに通っていた友人は施術にすこしはじかれるような痛みを感じたと言っていましたが私はまったくと言っていいほど感じませんでした。毛の太さや量によって痛みに個人差があるそうです。 施術中ずっと店員さんとしゃべっていて、脱毛や美容、近くのごはん屋さんについてなんかも教えてもらっちゃいました。そういうお話をするのも目的の一つで毎回来るのが楽しみでした。ほんとうに銀座カラーにしてよかったです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>最近、千波公園のテニスコートでテニスを楽しんでいます。テニスをする時は半袖・ハーフパンツなので、ムダ毛の処理は必須。6月、水戸駅前に銀座カラー水戸駅前店ができたのを聞いたので、オープン後すぐ伺いました。昔ソフトバンクショップが入っていたビルにできたらしく、何度か行ったことのある場所だったのですぐわかりました。駅からも近いので、ほぼ迷うことはなくサロンへ辿り着けると思います。当日は一緒にテニスをしている大学時代のサークル仲間と一緒に行ったのですが、スタッフさんが親切に案内してくれて、初めてのエステで緊張していた身体が緩みました。色々と説明を聞いて全身脱毛の方がお得なのがわかったので、私は全身脱毛コース、友人は顔脱毛含む全身パーフェクト脱毛を契約。予約とか取りづらいのかと思ったのですが、予約キャンセルのところに滑りこむことができて次回予約を取るのも簡単でした。今後は早めにネットから予約を取れば大丈夫と聞きましたので、忘れずに予約を入れなくちゃですね。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>今年の夏は暑かったのでポニーテールにすることが多かったのですが、首の後ろの産毛が目立っていたようで同僚に教えてもらいました。自分では見えないのでわからなかったのですが、写真を撮ってもらったところ、結構濃いめに生えていました。髪をアップにするのは止める、という選択肢もあったとは思いますが、私は銀座カラーで脱毛することに決めました。うなじだけじゃなくて他部位も気になっていたので、全身ハーフ脱毛の、うなじ（えり足）・Vライン（上部・サイド）・両ひざ・両ヒジ上下・両ひざ上下を選んで契約しました。気になる部分だけ選んで脱毛できるので、全身脱毛までは必要ないけど部位単位だと高くつく、という私にとっては良いコースでした。先日一回目を終わらせたのですが心配していた痛みや肌の炎症もなく、肌の調子もすごくいい感じです。少しずつ毛が抜けてきたのと、保湿ミストのおかげで自分の肌の触り心地がよくて、ついつい触ってしまっています。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 薬剤師<br></span>
</dt>
<dd itemprop="reviewBody"><p>医療脱毛と光脱毛で迷ったんですが、光脱毛の銀座カラーさんにしました。医療脱毛の方が効果が高いというのは調べてわかったんですが、私はそこまで毛が濃くないので、安価な光脱毛でも十分効果が出ると思ってはじめました。医療脱毛をしたことがないので比較できませんが、私は銀座カラーさんの脱毛方法で十分効果が出ています。脇とひざ下の毛が太めなので特にわかりやすくて、脱毛後1〜2週間くらいに肌を撫でると、ぽろっとムダ毛が抜けます。これが本当に気持ちよくて、脱毛の醍醐味って感じです。抜けた毛の毛穴は小さくなるみたいで、肌のキメが整うような気がします。数回施術してますが、その度にムダ毛が減ってきているので、あと3回くらいやればほぼ生えなくなるんじゃないかなと思ってます。私みたいに医療脱毛と迷っている人もいると思うのですが、光脱毛でも十分効果が出ると思うので、個人的には銀座カラーさんの脱毛をおすすめします。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">10代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>オープン時は、やっと栃木に銀座カラーが来たかって思いました。オープン時はまだ高校生だったので行けなかったのですが、仕事をしはじめて貯金もできてきたので、この前通う決心を。東武周辺にも脱毛サロンはちょこちょこあるんですが、やっぱり脱毛に行きたいのは銀座カラー。理由は、私が好きなアイドルが広告をしているから！これに勝る理由はありません（笑）ただ何回か通って効果が出なかったら途中で解約しようと思っていたのですが、かなり脱毛の効果出てます。初回の脱毛時は肌の上でピカッと機械が光るだけだし、施術後すぐはなんの変化もなく毛も抜けなかったので解約かな〜なんて思っていたのですが、2週間後くらいから毛がスルッと抜けていくんです。試しに一本毛をつまんで引っ張ったら、なんの痛みも違和感もなくスルッと抜けてびっくり。これが脱毛の効果かって思いました。いまじゃ銀座カラーの脱毛結果が気に入って、定期的に通っています。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 美容師<br></span>
</dt>
<dd itemprop="reviewBody">海が近くにあるから、周りに脱毛している人結構多い気がします。私もその内の一人で、しょっちゅう海に行ってビーチでのんびりしたり、気が向いたら泳いでみたりするのが好きです。地元民なので湘南とかは行かず、遠いところの海ばっかりですけど。だから、ムダ毛なんて生やしてはいられないんです。年中ツルツルでいないと不便なので、藤沢店オープン当初から通っています。脱毛の効果もいいんですけど、やっぱり私は美肌潤美の保湿ミストが最高です。海に入ると肌が乾燥しがちで、肌の表面だけじゃなく中からカサカサって感じなんですけど、美肌潤美で保湿してもらうとプルっとするだけじゃなくて、肌がふっくらするんですよね。それだけで女性らしい肌になれるので、私の肌ケアには必需品です。最近美肌潤美のフェイスパックも出たので、それを大量買いして家でも肌ケア頑張ってます。<p>
</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 ネイリスト<br></span>
</dt>
<dd itemprop="reviewBody"><p>新しく始めようと思った趣味が脱毛必須なユニフォームだから、というのがきっかけでした。昔から趣味が無くて人との会話に詰まることがあったので、折角なので湘南の海でできる趣味にしようと思って、サップヨガを体験しました。これが楽しくて！ただこれをはじめるとなると水着とかヨガウエアになるので、露出が多い。なら、家の近くの銀座カラー藤沢店で脱毛しちゃおうということで通い始めました。脱毛って日焼けしているとできないみたいなので、日焼けする危険のない秋からはじめてます。夏からはじめちゃうと日焼けで脱毛できないかもしれないので。秋からはじめれば来年の夏は約1年後。その時はきっと脱毛して、銀座カラーの保湿ケアもして、ツルスベ肌に決まってますよね。ムダ毛がなくなれば水着だってヨガウエアだって好きなものが着れますし、もっとサップヨガが楽しくなるはず。来年の夏目指して、いまから脱毛頑張ります！</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 看護師<br></span>
</dt>
<dd itemprop="reviewBody"><p>毎年多摩川沿いでBBQをするんですが、今年ムダ毛の処理を忘れて大失敗しました。ムダ毛が生えていることに気づいた時は本当に恥ずかしくて、頭が真っ白ってこういうことか〜みたいな。なので、自己処理を忘れても大丈夫なように、銀座カラーで全身脱毛を最近はじめました。予約日の前に剃れる所は剃っていかなきゃなんですが、全身なので結構大変。カミソリだと肌が痛むみたいなので電動シェーバーでやるんですが、カミソリより時間がかかる。根気のいる作業ではあるんですが、簡単に綺麗にはなれないってことだと思って頑張ってます。届かない背中とかは当日エステティシャンさんがやってくれるので大丈夫ですよ。で、全身脱毛はいっぺんに施術するので1時間くらいかかってます。機械をツーっと滑らせる感じでやるので、結構早いです。ちなみに、足腕と比べてVIOと足首はちょっと痛かった。でも、何回かしていれば慣れるみたいなので気楽に構えていようかなって思ってます。来年の夏までにムダ毛の無い肌になれるよう、しっかり通ってみます！</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 受付<br></span>
</dt>
<dd itemprop="reviewBody"><p>川崎店は駅からめっちゃ近いので、夜でも安心して通えます。夜の繁華街を1人歩きするのはやっぱり不安で…。でも川崎店はアゼリアを通ってサロン近くの出口を出れば目の前のビルなんですよね。地上に出るのは一瞬で、すぐサロンに着くので、何度も通ってますが怖い思いをしたことはありません。やっぱり女性専用の脱毛サロンだから、そういう所も気にしてるんですかね。いろいろ考慮しての立地だったらちょっと感動です。川崎店の営業時間は平日は21時までですが、施術時間が短いので遅くなりすぎることはありません。全身脱毛だと約1時間、私は足腕脱毛なのでもっと短い時間で終わります。さっと終わらせて帰れるので、仕事終わりの予約も安心してできます。やっぱり仕事終わりの金曜とかに予約入れたくなりますからね。これからもマメに通って、はやくスッキリさせたいです。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>今、周りの友達の中で銀座カラーに通うのがすごく流行ってます!!流行らせたのは、私なんですけどね!!（笑）荒れやすかった私の肌が、通い始めてから調子が良くなったので、みんなにその理由を教えて欲しいってお願いされました。お得な紹介キャンペーンもあったから、どんどん紹介してみたら、みんなも通うようになったんですよ。サロンに行ってみたいけど勇気が出なかったっていう子もいたので、きっかけづくりができてよかったし、私も豪華な特典がもらえてハッピーです!!
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">30代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>仕事の都合で、毎回平日の夕方に予約をしていますが、いつでもスタッフさんは笑顔で元気に迎えてくれます。私は、いつも疲れ顔でサロンに行くのですが、施術をしながら元気をもらえて帰りは疲れが抜けている気がします。サロンを選ぶポイントは、脱毛の効果や料金もすごく大事だけど、私の場合はスタッフさんとの相性も大切だなって思っています。仕事で疲れた自分へのご褒美に、サロンで脱毛を受けるのってすごくおすすめですよ。私は、横浜エリアしか行ったことはないのですが、銀座カラーはどの店舗も同じように元気に迎えてくれるはずです!
</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>横浜は2店舗あるので、行きたい日の空き状況に合わせて選べとても便利です。会員サイトから簡単に予約も取れるから、仕事の休憩中や移動の電車とかで思いついた時に予約しています。また2店舗とも駅近なのがすごく嬉しい!!駅から離れていると、だんだん通うのが面倒になってしまうので、やっぱり駅近がいいですよね。何度も通うサロンなので、通いやすくないと続けられないですよ。いろんな意味で、銀座カラーは通いやすいからぜひオススメしたいと思います!
</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>脱毛って高いものだと思っていたけど、銀座カラーは金額を見てびっくり!!学割があってお得だから、バイト代で払える範囲内で全身脱毛デビューができちゃいました。脱毛サロンに通えるなんて、大人の女性って感じでテンションあがります!!姉からも銀座カラーを勧められていたので、今回契約の時にその話をしたら紹介の特典もついてラッキーでした。あと学生なので授業のない平日の昼間に通えるから、予約もすごくスムーズ!学校もあるし、友達や彼との約束も大切だから、自分の都合に合わせてサロンに通えるって本当に重要だなって思いました。私の希望をすべて叶えてくれる銀座カラーに通えて良かったです!!</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">30代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>横浜店はずっと前からある印象です。言い換えれば昔から通ってる人がたくさんいるってことなんですよね。それって新しく出来たお店にはない、長く愛されてきた横浜店の良さが出ているのかもって考えてます。エステティシャンさんもたくさんの経験があるのかとても頼り甲斐がありますし、ムダ毛の悩みなども気兼ねなく相談させてもらってます。新しくオープンしたサロンにもいいところってあると思いますが、私は脱毛の歴史を感じるこの雰囲気が結構好きです。もちろん、サロン内は白を基調とした綺麗な店内で、気持ちよく過ごすことができます。掃除も行き届いてますしね。新しくできた脱毛サロンに行って失敗したなって感じたことがある人って結構いると思うんですよ。料金形態が曖昧だったり、スタッフの経験不足から打ち漏れされたりとか。そういう経験がある人は横浜店みたいな昔からあるサロンに一度行ってみたらいいと思います。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">10代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>以前、姉が通っていたので銀座カラーに決めました。姉とは12個歳が離れているので、私からすれば憧れのお姉ちゃん。そんな姉が通っていた横浜店に私も1年前から通っています。横浜には銀座カラーの店舗が3店ありますが、姉と同じ店舗に通いたかったのであえて昔からある横浜店で脱毛しています。
もう数年経っているので姉が通っていた時に聞いていた話とは様変わりしていますね。いまの脱毛マシンで全身脱毛が1時間でできるようになってるよと姉に話すと驚いていました。元々そこまで濃くない私のムダ毛なら6回くらいでほぼ生えなくなってきました。自己処理がないだけで、毎日こんなに楽なんだと感動です。随分前に銀座カラーを卒業した姉ですが、今度新しい機械で脱毛していない部位をやりたいみたいなので、お友達紹介のキャンペーンを利用してみたいと思います。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>人より毛が濃い方なので、毎日お風呂の時間にカミソリで剃っていましたが、肌へのダメージが気になり自己処理を止めたいと思ったのが、銀座カラー柏店に通い始めたきっかけです。<br>カウンセリングの時に、私が今まで行なっていた自己処理について話をすると、かなり肌には良くなかったみたいでゾッとしました。手遅れになる前に脱毛を始められたので、今では肌荒れしにくい綺麗な肌状態をキープできています。<br>また、ムダ毛が多かったので効果が出るかも不安でしたが、しっかり効果が表れているので普段のお手入れが見違えるように楽になって、本当に嬉しいです。<br>脱毛サロンに通うか悩んでる人がいたら、絶対に早く始めた方がいいですよって教えてあげたいですね!</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 事務職<br></span>
</dt>
<dd itemprop="reviewBody"><p>婚活をかねて脱毛サロンに通おうと思い、銀座カラーで契約をしました。<br>職場付近にも店舗はありますが帰宅時間が遅いため、休日に最寄り駅にある柏店へ通っています。<br>お店は、駅からすぐのわかりやすい場所にあり道にも迷わないし、脱毛後にお出かけする時も移動に時間がかからなくて、すごく便利です。<br>またスタッフのみなさんが、いつも丁寧に接客してくれるので、悩み相談もしやすく安心して通えています。私は、プライベートの話もしちゃうので、女子トークが盛り上がって楽しいです!<br>通うたびに美意識が高まるので、おしゃれの幅も広がってきました。これで、婚活も上手くいきそうな気がします!</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>私は雨に濡れるのが大嫌いなので定期的に通うお店は全部駅近と決めています。会社は最寄り駅から5分、美容院は駅ナカ、脱毛サロンは船橋駅から徒歩1分の銀座カラー船橋北口店です。船橋駅直結で東武百貨店があるので、駅に着いたら東武百貨店の店内を抜けてギリギリまで屋根のあるところを歩いてサロンまで向かいます。駅から1分だし店内を抜けるので雨が降っていても関係ありません。この前台風が来ていた時も雨が降っていましたが、ほとんど濡れずにサロンに着くことができました。着いて早々「雨大丈夫でしたか？」とスタッフさんに声をかけてもらえたことに嬉しくてちょっとほっこりしました。足腕脱毛をしてからポロポロ毛が抜けて自己処理がかなり楽になりましたし、ムダ毛がかなり薄くなってきたので「カミソリで剃っておけばよかった。」なんていう後悔もありません。通うのが面倒な人は駅近の脱毛サロンをお勧めします。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>実際に通ってみて予約は普通に取れるし、スタッフさんもいい人なので安心しました。口コミサイトに予約が取れないことがあるって書いてあったのですが、施術後に予約を取るタイミングがあるのでその時に予約を入れれば問題ないと思います。もし都合がつかなくなったらネットで時間を変えればいいだけなので簡単です。結局どこの脱毛サロンに行っても予約の問題は上がっているみたいなので、早めの予約を心がけて、自分で調整していくしかないと思っています。また、スタッフさんは親切丁寧な接客だと思いますし、脱毛効果に関しても問題はありません。人それぞれ捉え方や印象が異なるので仕方のないことかもしれませんが、しっかりお見送りしてくれるし、脱毛する時もキチッと照射してくれます。私自身は船橋北口店も銀座カラーも満足しているので、今契約中の脇と足腕脱毛が完了するまでしっかり通っていきたいと思っています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 主婦<br></span>
</dt>
<dd itemprop="reviewBody"><p>ブライダルエステの一環で銀座カラーの脱毛に通い始めたのがきっかけです。当時全身脱毛の脱毛し放題で契約をしているので、結婚式が終わったいまでも通い続けられています。実際8回くらい通ったところでほとんどのムダ毛は無くなったのですが、たまにひょっこり現れる毛がいるので、いまは気になったタイミングで気になる部位だけを脱毛しています。これが脱毛し放題のいいところ！脱毛し放題じゃなかったら、また契約しなきゃいけないんですもんね。結婚した今、脱毛したいなんてお金がかかってしまうから言えないと思います。もし、ブライダルエステで脱毛する人がいるなら、絶対脱毛し放題にした方がいいですよ！ちょっと費用が高くなるので、旦那さんからNGが出るかも。私のところも20万円の予算をオーバーしてしまったので、自分の貯金を下ろして脱毛し放題プランに変更しました。ここだけ追加で脱毛したいとか、一本生えてきた毛だけ脱毛したいって思うことがあるかもしれないので、初めの契約プランをしっかり考えた方がいいと思います！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">30代 講師<br></span>
</dt>
<dd itemprop="reviewBody"><p>ヨガインストラクターをしています。露出が多めなユニフォームを着たり、両腕をあげるヨガのポーズなどがありますので脱毛は必須です。以前両脇のみ他店でニードル（針）脱毛をしていたのですが、とにかく料金が高い。脇脱毛だけなのに数十万もかかりましたし、痛みが本当に辛くて通うのが嫌で嫌で仕方なかったんです。その他部位は千葉船橋店に通って脱毛していました。銀座カラーさんは料金は比較的手頃だし、痛みもほとんどないし、駅から近いのでめんどくさいなって思う暇もなくサロンに着いちゃいます（笑）足腕の脱毛効果はもちろん、背中や腰の周り、お腹など比較的毛の薄い部位も徐々に薄くなってきて、いまじゃほとんど生えていません。生えていても産毛レベルなので気にならないですし。<br>
私自身ヨガ講師の方に憧れてヨガインストラクターになったようなものなので、私も常に綺麗で憧れられる存在でいるためにも、体型維持と合わせて脱毛も続けて通っていきます。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>彼氏ができたのをきっかけに、千葉店で脱毛をはじめました。彼の元カノはとても可愛くて綺麗な人だったので、付き合い始めたのはいいのですが自分に自信が無くなってしまっていました。私のムダ毛は細めなのですが量が多くて、毎日カミソリで自己処理をしたり、たまに脱色をしたりして、必死に自己処理をしてきたと思っています。自己処理の回数が多い分ケアは徹底していて、保湿クリームを塗ったりコラーゲンドリンクを飲んだり、とにかく元カノに負けないように一生懸命でした。そんな時、友人が銀座カラーを紹介してくれたのです。通いだしてからというもの、簡単に毛が抜けますし、保湿ミストケアで肌のキメが細かくなってきたように感じます。スタッフさんとのおしゃべりも楽しくて、暗く沈んでいた気持ちも明るくなってきました。彼からも「なんか可愛くなってない？」って褒められて自信もつきました！いま思えば自信をつけることなんて簡単なんだから、早く脱毛に通ってればよかったのにって思います。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">10代 アパレル<br></span>
</dt>
<dd itemprop="reviewBody"><p>とにかくたくさんの駅に近いので、通いやすさが抜群です！家から通うときは千葉駅で降りて、彼氏の家から通うときは葭川公園駅で降りて…。あちこち乗り換えがあったりすると電車代が高くなるし、何よりも通うのがめんどくさくなるけど、銀座カラー千葉店はアクセスがいいので小まめに通えてます。料金を基準に脱毛サロンを決めがちですが、やっぱり脱毛は続けなきゃ意味がないので通いやすさってかなり重要だと思いますよ。自宅や職場、彼氏の家とか友達の家などいつもいる場所を基準にサロンを選ぶと後悔しないんじゃないかなと思います。私自身アクセスの良さで千葉店で脱毛をすることにしましたが、スタッフさんの接客は気持ちいいし、サロンは綺麗だし、効果も出てるし、結果的にその他の脱毛サロンを選ぶ重要ポイントもクリアしてるなと。なので、千葉駅周辺の人で脱毛をしたいなら銀座カラーの千葉店を選んだらいいんじゃないかな〜なんて思ってます。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">10代 フリーター<br></span>
</dt>
<dd itemprop="reviewBody"><p>姉の紹介で銀座カラーに通っています。姉が脱毛に通っているのは知っていたのですが、私自身はメイクもほとんどしないですし、自分磨きとは程遠い女子力低めな生活を送っていました。そんな私がなぜ脱毛しているのか不思議ですよね。元々は姉が紹介特典欲しさに私を銀座カラーに連れて行ったことがきっかけでした。私は自分を磨く方法がわからないだけで、メイクや脱毛に興味がありました。姉に背中を押してもらったことで、初めての自分磨きを銀座カラーではじめたのです。美容に関して知識のない私にエステティシャンさんがわかりやすく説明してくださったので、どうして毛が抜けるのか・保湿についてなど理解することができました。私が不安に思っているのを感じ取ってくれたのか、しっかりと説明してくれて助かりました。いま半年通い続けてますが、ムダ毛がないだけで自分の肌がこんなにも変わるのかとびっくりしています。肌が綺麗になってくると自信が付いてきますし、色々なことにチャレンジしたくなってきました。次はメイクにも挑戦したいと思っているので、これからも女子力を磨きます！</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>広告を見て、脱毛するなら銀座カラーへ行こうと思っていました。元々体毛が濃い方で、小学校低学年の時は体操服や水着になるのが恥ずかしく、高学年になったらすぐカミソリで自己処理を始めました。ムダ毛が無くなって嬉しかったのですが、毎日剃らないと次の日にはチクチクするし、だからといって毎日剃っていたら肌を痛めてしまいました。自分で働き始めたら脱毛しようと思っていたので、初任給で家から近い銀座カラー川越駅前店に駆け込みました。全身脱毛を希望していたので、かなりの金額がかかるものだと思っていたのですが、予定より安く済んでびっくり。想定の1/2くらいでしょうか。だからといって効果がない、ということはありません。施術後1週間くらいで毛が抜けてツルッとした肌になります。次の毛周期がくるまではツルツルのままなので、本当に毎日が楽です。お金はかかりますけど、ずーっとムダ毛で悩んでるよりは絶対に気持ちは楽になりますし、自己処理にかける時間が短くなるので余裕ができます。毎日を楽しく過ごすために、銀座カラーでの脱毛は必須です。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>友達に紹介されて、1年前くらいから大宮店に通っています。元々他の脱毛サロンに通っていたのですが、回数が少ない割に高くて。回数終了後、追加で脱毛したくても金額が高くて躊躇していました。脱毛回数は終わっているのに支払いが続くのがなんだか悲しくって。そんな状況を友達に愚痴っていた時、オススメだと銀座カラーを紹介されました。仲の良い友達なので最初から良いサロンだろうと信用していましたが、実際に行ってみて期待以上に感じました。ハーブティーのおもてなしからはじまり、解りやすい説明、広いサロン内に印象の良いエステティシャンさんたち。もちろん効果もしっかり感じられて、毛が抜けるだけじゃなくて保湿ケアで透明感も出てきたように思います。肌に自信が出てきたのでオシャレも楽しくて、ついルミネで買い物しちゃってます。今日も銀座カラーを紹介してくれた友達と、まめの木に集合して買い物です！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">
<span itemprop="name">20代 受付<br></span>
</dt>
<dd itemprop="reviewBody"><p>上尾で仕事をしているので、通勤で大宮を通ります。脱毛に通うのは定期券内がいいなと思っていたので大宮のサロンを探していました。銀座カラーさんはCMがすごい可愛くて印象的だったのと、そのCMを見て私の職場のファンの子が話題にしていたので一度行ってみることにしました。大宮って結構ごちゃごちゃしているので、サロンに行くのに迷わないか心配していたのですが、迷う暇もなくすぐサロンに着きました。歩いて5分くらいですね。サロン自体は新しい店舗より広くて落ち着いている雰囲気で、リラックスして脱毛を受けることができました。いい意味でパパっと施術をしてくれたので、せっかちな私としてはありがたかったです。パパっとできるってことは慣れてるってことだし、逆に安心しました。もちろんエステティシャンさんは親切だし、声掛けもしてくれて常に気遣ってくれたのを感じています。お友達紹介の制度がかなりおトクなので、職場のファンの子に銀座カラーをオススメしておきます！</p></dd>
</dl>

<!-- -->
 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 ネイリスト<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>町田モディの中に脱毛サロンがあるのは本当に便利です！そもそも町田モディが駅の目の前にあるのでアクセスは抜群です。ちょっと雨が降っていた時は傘を持っていなかったのにほとんど濡れずにサロンまで辿り着けましたし、日差しが強い時も紫外線を浴びずに行けます。車で来る時はモディ自体に駐車場がないのでちょっと不便ですが、近場の決まった駐車場に預ければ1時間無料とかあるみたいです。もちろんサロンだけ通うのでは無料にならないので、モディ内のショップでお買い物をしなきゃなんですが、たくさんショップがあるので買うものには困らないですね。<br>
肝心の銀座カラー町田モディ店は、店内は綺麗ですし、スタッフさんも親切でとても感じがいいです。脱毛自体は痛みの少ない施術なので、のんびり寝そべっているだけで終わっちゃいます。私は全身脱毛なので、気がつくと寝ちゃってたなんてこともしばしば。せっかく綺麗になりに行っているのに、痛みが怖かったり、リラックスできないのでは残念ですもんね。その点、銀座カラーさんは気配りが上手だと思います。まだ施術回数が残っていますので、スタッフさんに協力いただいて、しっかりムダ毛を撃退していこうと思います！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 介護士<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>女子会で脱毛の話になり、私だけ未経験だったので銀座カラーで脱毛を始めることにしました。脱毛してないと話についていけなかったり、途中から話の内容がわからなくなってちょっと寂しい思いをしたので、これは自分を磨くタイミングだと思いました。正直自分のムダ毛で嫌な思いをしたことはないですし、気になったこともほとんどないのですが、友人曰く「自分が思っているより、他人はムダ毛のこと見てるよ〜」と話しているのを聞いて怖くなりました。自分だけが気にしてなかったのかも、って。<br>
銀座カラーに通う前は自分に対して無頓着だった私ですが、通い出して変わったと思います！全身脱毛で毛は少なくなりましたし、美肌潤美っていう保湿ケアで肌のキメが整いました。身体が綺麗になったら体型が気になりだして、ダイエットを始めて-3kg。メイク方法を研究しだしてからは、街中で声を掛けられることが増えました！私以外にもたくさんお客さんがいるはずなのに、町田モディ店のスタッフさんが変化に気づいてくれて、すごく嬉しかったです。これからも自分磨きを頑張って、自信をつけていきたいです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 飲食業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>和服を着る機会が多いので、八王子店でうなじを脱毛しています。実家に住んでいた時は妹や母に手入れをしてもらっていたのですが、最近一人暮らしを始めましたので手入れをお願いできる人がいなく困っていました。初めのころは何度か理容店でシェービングをしてもらっていたのですが、一回につき2000円くらいかかっていました。仕上がりはとっても綺麗なのですが、コスパを考えるとちょっと高い。だったら初期投資はかかるものの脱毛した方が得だと思い、銀座カラーの八王子店に通いだしました。他部位の脱毛の紹介などもあったのですが、ちょっとしたご案内って感じだったので、気兼ねなく断れました。うなじの脱毛は見えないところなので自分でシェーバーを買って持っていくのですが、私は予約時間前にサロン目の前のドンキで買いました。うなじは形が結構重要なのですが、エステティシャンさんが綺麗に剃ってくれましたので安心です。まだ3回なのですが、徐々にムダ毛が減ってきたように思います。元々毛が薄いので時間がかかるかもしれませんが、マイペースにしっかり通っていきたいと思います。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ずっと立川店に通っていたんですが、3月に自宅の近くである八王子にサロンが出来たということで、すぐ八王子店に次回予約を入れました！立川駅まで電車で10分程度ではあるのですが、やっぱり近い方が楽なので助かります。こういう時、個人店とか店舗展開の少ないサロンじゃなくて良かったなって思います。だって全国展開していればこうやって近くにサロンができて、一気に通いやすくなるんですもん。便利ですね。<br>
立川店の時からVIO脱毛で通っているんですが、さすがに足腕脱毛よりは時間がかかってます。カウンセリングの際、他部位より時間がかかると聞いていたので本当にその通りって感じです。質も量も太さも全然違うので当たり前ですよね。でも、通う前に比べたら違いが分かりやすく出てきました。Vラインはムダな部分が抜けて形がキープされたままですし、iラインは毛量が減りました。Oライン（ヒップ奥）はほとんど生えてないですし、どんどん理想に近づいていっています。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 秘書<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>立川北口店のエステティシャンさんは年齢が近いような感じがして、安心して脱毛をお願いできています。エステティシャンさんって結構若い方が多くて、受け答えに不安があったり、施術が心配だったりしてしまうのですが、立川北口店の方は私と同じような20代後半の方が多くいらっしゃるみたいで、安心するというか落ち着くというか。銀座カラーさんじゃないんですが、昔行った脱毛サロンは若いエステティシャンが多く、こっちがハラハラしてしまうことがあって以来、できることなら20代後半くらいのエステティシャンがいるところに通うことにしています。やっぱり施術してもらってても安定感があるというか、全身を委ねてしまえる安心感がいいですね。銀座カラーさんは先輩方の教育が良いのか、若いエステティシャンの方もしっかりしているので、どの方に担当してもらっても施術は安定していますし、わからないところを質問してもすぐに回答が返ってきますので安心して任せられています。最近VIO脱毛が気になっているので、信頼できるエステティシャンさんのいる立川北口店で始めてみようと思います。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>毎年夏は海とプールで忙しいので、秋くらいには自己処理と日焼けのダメージで肌がボロボロになります。10代の時は日焼けもファッションの一部だったので気にしていなかったのですが、20代になったら美白とか色白がブームに。当の私は日焼けした色黒肌。しかも、夏のダメージが蓄積して肌はボロボロでした。このままでは30代はシミとシワだらけになる予感。さすがにこのままではまずいと思い、友人に銀座カラーを紹介してもらいました。日焼け肌は脱毛できないとのことだったので、肌が落ち着く秋冬から両脇と足腕脱毛を始めました。友人と同じ立川北口店で脱毛しているのですが、エステティシャンの方はプロって感じで、頼り甲斐があります。脱毛効果はバッチリ出てますし、脱毛後の保湿ケアで乾燥を感じていた私の肌が、プルプルに生まれ変わりました。噴射中、保湿ミストが私の肌にぐんぐん吸収されているような感覚がして、よっぽど乾燥していたんだと反省しました。<br>
最近はエステティシャンの方から肌ケアの方法を教えてもらって、毎日欠かさず行っています。脱毛でツルツルに、保湿ケアでプルプルになった私の肌は、銀座カラーさんのおかげです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 主婦<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>子育てに追われ、自分のことは後まわし。気づいたら1日が終わるような生活だったので、自己処理もままならず。<br>
一言「脱毛したいな〜。」とこぼしたのを聞いていたようで、様々な脱毛サロンを調べてくれて、「銀座カラーって良いらしいし、ここで脱毛しておいで！」と言ってくれました。貯蓄のこともあるし、部位を選んで脱毛しようかとも思ったのですが、「全身脱毛にしなよ！」と言ってもらえたので、勢いで契約しました。立川店は雰囲気が可愛らしいですし、全身脱毛＋全身保湿ケアで久々に女性らしい時間を過ごすことができています。予約日は主人に息子を見ていてもらうので、土日しか予約が入れれないのですが、早めに予約をすれば土日でも問題ありません。しかも、1時間ほどで終わるので、家で留守番をしている主人や息子にも負担を掛けずにいられるので助かっています。今では数日自己処理ができなくても問題ないくらいムダ毛が減ってきていますのでとっても楽です。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>昔から銀座カラーさんにはお世話になっていたのですが、仕事で転勤になりました。地方から立川周辺に引っ越しになりましたので、より家に近い立川店で現在は通っています。立川北口店も結構近いのですが、立川店のナチュラルな雰囲気が好きなのと、信頼しているエステティシャンさんが多くいるので基本的に立川店が多いですね。やっぱり体を預けることになるので、信頼できる人がいるのは心強いですし、安心して脱毛に通うことができます。私は結構痛みに弱くて、毎回ほとんど痛みは感じないもののドキドキして通っているのですが、それを察してか、立川店のエステティシャンさん達はしっかり声がけをしてくれるんです。ドキドキしているのもほっと和らぎますし、通常より痛みも感じなくなります。きっと私みたいに「痛かったらどうしよう。」とか「脱毛って怖い。」って思っている人もいるかと思うのですが、きっと安心できると思うので、一歩勇気を出して欲しいです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>アパレル店員をしているので、常に見た目には気をつけています。食事制限や筋トレ、ボディケアの一環として脱毛を始めたのもアパレル店員になってからでした。季節問わず露出が多くなりがちなので、全身脱毛で抜け目なくケアを行っています。他店は脱毛し放題が付いていないこともありますけど、銀座カラーは脱毛し放題が付いているので、将来肌が変化していっても対応してもらえるので、とても安心感があります。ムダ毛が無くなる前にコースが終了してしまったら、結局追加契約になってしまいますからね。お客さんのことをしっかり考えてくれている証拠だと思っています。<br>
大事な脱毛の効果ですが、一年で8回通えるようになっているので割と早い段階で毛が少なくなってきます。施術の際もしっかりマーキングしてくれるので照射漏れもないですし、もし何か気になっても吉祥寺北口店のエステティシャンさんは優しいし親切なので気軽に質問しても大丈夫ですよ。これからも自信を持って店頭に立てるよう、脱毛を通して自分磨きを続けていきます。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">30代 秘書<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>自転車で30分ほどかかるのですが、吉祥寺北口店に脱毛しに通っています。最近、彼氏の影響で自転車にハマり、ここ数年徒歩や電車を使った移動手段は使わずどこまででもクロスバイクで移動しています。脱毛するきっかけも、この自転車がきっかけでした。彼氏とクロスバイクで浜松まで旅行に行った時、怪我が無いよう長袖とロングパンツを着ていったので、ムダ毛の処理をしっかりしていませんでした。途中Tシャツに着替えたのですが、ポツポツ生えっぱなしのムダ毛に気づいた時に冷や汗が。自己処理を面倒だからとサボりがちなので、また同じことで焦らないよう、プロにお願いするのがいいかと思ったのです。好きな部位を選んで契約することも考えたのですが、やっぱり全身脱毛が今後も楽だなと思って。エステティシャンさんは親切だし手際がいいので、楽しくおしゃべりしているとあっという間に終わりますね。脱毛を始めてからというもの、彼氏から肌が綺麗だと褒められることが増えました。銀座カラーには感謝です。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>大学の友人からの紹介で、銀座カラー北千住店に通っています。そこまで毛が濃いわけではないので脱毛に必然性は感じていなかったものの、紹介したら特典がもらえるということで試しにカウンセリングを受けてみました。脱毛のメリットって毛が抜けるから楽できるってところだけだと思っていたのですが、エステティシャンの方から説明を受けて、脱毛の多くのメリットを知ることができました。自己処理より結果的にお金がかからなかったり、肌トラブルが回避できることを知って、露出頻度の多い両ワキと足腕の脱毛をすることにしました。通う前は自己処理で肌荒れが起きたり、自己処理の面倒くささに途中でカミソリを投げ出したりしたことも。でもいまはほとんどムダ毛が目立たない綺麗な肌になりました。友人も紹介特典がもらえて嬉しそうでしたし、私も特典がもらえてありがたかったです。私も他の友達を紹介して、脱毛仲間を増やしちゃおうかなと思います。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>年齢的にシミが気になるようになってきたので、今年は日焼け止めに日傘で過ごしていました。おかげで日焼け自体はほとんどせず過ごせましたが、なぜか毛穴の炎症が増えてしまったのです。原因は、SPFの高いウォータープルーフの日焼け止めを使っていたからでした。しっかり洗顔できてなかったみたいです。毛穴の炎症はシミにも繋がるのでかなりショックで。改善方法を調べていたら、脱毛して炎症が減ったという記事を見つけました。顔脱毛はしたことなかったので、近場の北千住店で脱毛しようと思って足を運んだという感じです。顔脱毛はメイクを落として施術しなければならないので、スッピンで出歩ける徒歩圏内のサロンであることが絶対条件でした。この前初めて行ったのですが、スタッフさんは優しいし、施術はスムーズ。何より脱毛後の保湿ミストが最高です。保湿ミストの後は肌がもっちりプルン、極上のフェイスパックっていう感じですね。この日はスッピンのまま行って帰って来たのですが、メイクルームがあるようなので、出かける前とか帰りに寄ることもできそうです。しっかり通って美肌目指します！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ポスターデザインの仕事をしているので、休日はアートなどを観て感性を磨くよう心がけています。上野の森美術館もよく行くので、通っていた他店の脱毛サロンを退会して、上野の森美術館に近い銀座カラーの上野御徒町店に通うことにしました。両ワキとVラインは他店で済んでいたので、いまは腕全セット（両手の甲・指、両ヒジ上、両ヒジ下）を契約して脱毛しています。去年の春くらいから脱毛を始めたので、施術回数は6回でしょうか。最近は脱毛する前と比べてかなり毛が薄くなりましたし、生えてくるタイミングも遅くなったと感じています。毎日お風呂場で自己処理しないと翌日には目立っていたムダ毛も、数日放っておいても目立ってきません。今年の夏は自己処理をうっかり忘れてても、気にせずにノースリーブになれました。誰かにムダ毛が生えているのを見られているかも、というプレッシャーから解放されたのは大きいです。毎日を快適に過ごすために、女性は脱毛が必須かと思います。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>デザインの仕事をしているので、休日はアートなどを観て感性を磨くよう心がけています。上野付近は美術館や博物館が多いので、お気に入りのスポットです。<br>
              以前は職場の近くにある別の脱毛サロンに通っていましたが、休日によく行く場所で通いたいと思い銀座カラー上野公園前店に決めました。1度の来店で全身と顔を一緒に脱毛できるのに、施術の時間が短いので遊びにいくついでに通えてとても便利です。<br>
              今までは毎日お風呂場で自己処理しないと翌日には目立っていたムダ毛も、数日放っておいても目立たないようになってきました。しかも、自己処理が減ったらお肌の状態が良くなってきたのですごく嬉しいです。<br>
              銀座カラー上野公園前店なら、このまま楽しく通っていけそうです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 ブロガー<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>デパートコスメが大好きで、新作が出ては購入しブログにアップしています。ついこの前、新作のネイルを塗って写真を撮りブログにアップしたところ、指毛が生えていることに気づいてすぐその記事は削除しました。あんまり自分では気になっていなかったのですが、綺麗なネイルを塗っても指毛で台無しになることに衝撃を受けて、脱毛を決意しました！<br>
よくコスメを買うので、西武池袋に近い池袋店か池袋サンシャイン通り店かで悩んだのですが、池袋サンシャイン通り店の方が新しい店舗みたいだったので、池袋サンシャイン通り店へ通うことに。<br>
脱毛部位は悩んだ末、指毛と、メイクのノリがよくなるよう顔全体の脱毛も契約しちゃいました。脇などに比べて細くて薄い毛なので抜けるまで若干時間が掛かるとは聞いていたのですが、徐々に毛が薄くなってきたのを感じています。<br>
脱毛してみて、顔はファンデーションやパウダーのノリが良いですし、指毛も気にしなくていいので指先まで自信を持って過ごせています。少しずつですがブログのアクセス数も増えてきたので、脱毛の体験記事も書こうと思います。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">30代 主婦<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>若い時に出産・子育てをしているので自分にお金をかけることができず、みんながエステや脱毛に通っているのを羨ましく思っていました。やっと子供が小学校に上がったので、自分の時間が少しずつ持てるようになった今なら自分磨きができるのではと思い、脱毛サロンを探しました。現在東上線沿いに住まいがあるので、電車一本で通える池袋で脱毛を探していたところ、短時間で脱毛できるところを見つけました。それが銀座カラーさんの池袋サンシャイン通り店でした。結婚している手前使えるお金には制限がありますので、両脇と両ひじ下を脱毛することに。念願の脱毛をすることができて本当に嬉しいです。忙しさにかまけて女性らしさを失っていたことに反省です。毎日夫や子供の面倒を見なくてはならないのですが、自己処理をする回数が減ったことで時間に余裕ができましたし、自分に自信がつきました。家族にもより優しく接することができるようになって、夫婦・家族円満にも繋がったと感じています。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 教師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>夏に大学時代の友人たちと海に行った時、前日から飲み会をしてたので、しっかり自己処理ができないまま水着になることになってしまいました。両脇は脱毛済みだったので問題なかったのですが、腕や足、背中やお腹などムダ毛が生えたままだったので水着になるにも躊躇してしまい、みんなが海で楽しんでいる間、私は上着を着たまま一人で待っているだけ...。いざという時に脱げないままではイヤだったので、銀座カラーで全身脱毛をすることに決めました。部位を選んで脱毛する契約に比べれば全身脱毛なので高いですが、水着になったりすると脱毛していないところが目立つので、全身綺麗にしておくべきだと思いました。まだ2回しか通えてませんが、毛が抜けるのが分かりました！特に太くて濃い部分はわかりやすいですね。以前の失敗を活かして、これからはもっと楽しい夏が過ごせるように全身脱毛頑張ります！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>銀座カラーアイズにマツエクで通っていた時、脱毛サロンも経営していることを知りました。<br>
父親が沖縄出身なので私も遺伝なのか毛深くて悩んでいたのですが、脱毛サロンに通いたくてもキッカケがなくて。<br>
そんな時に脱毛サロンのことに気づいたので、すぐ銀座カラーアイズのスタッフさんに話を聞いてみました。その時は簡単な話を聞いただけだったので、改めて携帯から脱毛の予約を取りました。<br>
予約日当日は雨だったのですが、駅から近いのでほとんど濡れずに済みましたし、池袋店は銀座カラーアイズと近い場所にあったので迷うこともなかったです。ビル自体はちょっと古い感じがしたのですが、店舗がある階に降りるととっても女の子らしいキラキラした空間でした。こんな空間で脱毛が出来ると思うとワクワクしました。実際通っていて不便を感じることはないですし、エステティシャンの方も綺麗で優しいので、これからも続けて脱毛していきたいです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">30代 会社経営<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ネイル・マツエク・美容院を1日の中に予定を詰め込む、美容メンテナンスの日を作っています。いま通っているお店が表参道・青山周辺なので、その近くで脱毛サロンを探していました。色々な脱毛サロンがある中で、銀座カラー表参道店が立地で便利だったのと、全身脱毛の施術時間が短かかったので、他の脱毛サロンに比べて都合がつけやすいと思い選びました。<br>
実際に通ってみると豪華なエステサロンのような内装で気分が上がりますし、施術時間も1時間ほどで終わるため、1日の大半が脱毛の施術で終わるなんてことがありません。他の予定に響くことがないですし、前後の予定に余裕がある時は、近くにカフェがたくさんあるので、そこで暇つぶしすることもできます。時間をきっちり使いたい私としては、かなり気持ちのいい予定が組めますし、時間が余っても他にたくさんお店があるので予定を新しく追加することもできるので嬉しいです。<br>
先日美容メンテナンスの日があったのですが、Aoビルに入っている美容院でカットとカラー、銀座カラーで脱毛、青山通り沿いでネイルとマツエクを済ます、というスムーズな1日を過ごすことができました。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 ネイリスト<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>脱毛サロンってお店に入るのに周りの目が気になったり、通っていることを知られたくないなって思うので、路面店の脱毛サロンがあることに最初はびっくりしました。でも、好きなモデルさんが表参道店に通っているブログを見て、私も試しにカウンセリングだけ行ってみようかなと。実際行ってみると、大通りから一本中に入った道にお店があって、その道はまばらに人がいるくらい。お店の外観は綺麗で脱毛サロンとは思えませんでした。路面店は入りにくいと思っていたけど、そんなことありませんでした。昔通っていた、大通り沿いの2Fの脱毛サロンの方が人目が気になって、入りにくかったですね。<br>
初回は無料カウンセリングで伺ったのですが、エステティシャンの方は綺麗な人ばかりですし、かなり丁寧に説明してもらえました。効果を感じてから契約したかったので、その日に契約はしなかったのですが、しつこい勧誘などがなく、スムーズに帰ることができてかなり好印象でした。ここなら自分の身体に合わせて脱毛ができると思って、後日連絡をし契約をさせてもらいました。手頃な金額からはじめましたが、追加で全身脱毛を契約したいと思っています。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>「脱毛したい。」が口癖の友達と一緒に、銀座カラーさんへ通いはじめました。春までは大学生だったので自由に使えるお金がなく、エステや脱毛の贅沢は我慢していたのですが、社会人になり仕事も落ち着いてきた今からなら脱毛に通えると思い、よく遊びにいく渋谷で脱毛サロンを探していました。友達が渋谷道玄坂店を選んでくれたのですが、駅から近くてびっくり！更にサロン内は綺麗だし、やりたかった全身脱毛プランは予算内だし、スタッフさんは可愛いし、友達に感謝です。早速1回目の脱毛を済ませましたが、じっとしているだけで終わるので楽だし、保湿してくれるので更にツルツルだし、毛は抜けてくるしで、このタイミングで脱毛に行ってよかったって思っています！しかも、飲食店が多いので二人で新しいお店を開拓する楽しみも増えました！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>元々は新宿の方で仕事をしていたので、勤務帰りに新宿東口店へ通っていました。夏に転職をし渋谷勤務になったので、渋谷で通いたいとスタッフに相談したところ、新しい店舗ができるということだったので、新しい物好きな私は最近渋谷道玄坂店へ行くことが多くなりました。会社からは歩いて8分くらいなので少し残業しても間に合いますし、仕事帰りに脱毛して、帰りは同僚と待ち合わせて食事に行けるので、時間を有効に使うことができています。新宿東口店の時から合わせると約1年通っていますが、最近「肌が綺麗だね！」とか「ケアの方法を教えて！」と言われることが増えました。昔からムダ毛がコンプレックスだったので、褒められるのが本当に嬉しいです！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">マルキューの好きなショップ店員さんに勧められて通い始めました。その人はおしゃれだし可愛いし、夏の露出の多いファッションも着こなしていて、本当に憧れです。肌もつるつるで綺麗なのでケア方法を聞いたら、「近くの銀座カラーで全身脱毛してるよ！」と聞いたので、初めて脱毛に行く事を決心しました！美容系のサロンに行くこと自体初めてでお店に入るまでかなりドキドキだったのですが、スタッフの人が優しかったし、私と同じような歳の女の子のお客さんもいて、安心しました。まだ学生なので高い契約はできなかったのですが、スタッフの人が本当に親切にしてくれて、脱毛の話じゃなく普段の肌ケアの方法とかも教えてもらえました。これから通うことになりますが、肌が綺麗になっていくのが楽しみで仕方ないです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">渋谷には脱毛サロンがかなり多いので、お店を決めるためにいくつか体験に行ったことがあります。お店によって価格やサービスもバラバラ、良いところもあれば気になるところもある。その点、銀座カラーさんは店内の様子・スタッフさんの接客・技術面など、他の店舗より上回っていた点が多かったように思います。もちろん、改善したらいいのにと思うところもありますが、小さな問題なので、他の店舗に変えようというくらいのアクションには至りません。脱毛の効果は十分実感できています。施術後のミストケアで肌にハリが出てきたような気もしますし、毛はポロポロ抜けてくるし、楽して肌ケアができているので嬉しいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">昔から脱毛には興味があり、職場近くにあった銀座カラーさんで無料カウンセリングをしたのがきっかけです。脱毛は料金が高いというイメージがあったので中々足を運べなかったのですが、良心的な価格で勧誘なし、口コミの評価も高い、そんなネットの情報を疑いつつ、仕事終わりに行かせてもらいました。お店はビルの７階、ビルの見た目からはまた違って、かなり明るく可愛らしい店内でした。平日夜だったのにお客さんが多かったのがとても印象的で、人が多いのであれば安心だなと思ったのを覚えています。スタッフさんより脱毛についての説明をきいて、初めはネットでの評価を疑っていたのですが、カウンセリングが終わるころにはその評価に納得していた自分がいました。良くないなと感じたら契約せずに帰ろうと思っていたのですが、新宿西口店の良さを知ることができたので、納得して足腕脱毛の契約を結ぶことができました。私のように「脱毛は敷居が高い」と感じている人でも、無料なので一度カウンセリングを受けてみるといいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">10代の頃からカミソリで自己処理するのが主だったのですが、20代になって代謝が悪くなってきたのか、皮膚の中に毛が埋まる埋没毛になることが増えてきました。膝より下にできることが多く、毛穴が膨らんだり、赤くなったりして、ストッキングを着用していても目立ってきていたので、カミソリの使用を見直すタイミングでした。埋没毛で皮膚科に相談にいった所、自己処理ではなくエステサロンでの脱毛を勧められ、会社から近い銀座カラーさんへ通うことにしました。まだ一回目の脱毛を経験したばかりなのですが、照射時にあまり痛みはありませんでしたし、思っていたより気軽に通えそうだなと思っています。来年は埋没毛を減らして、堂々と足を出せるようになりたいです。</dd>
          </dl>

 <!--<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛サロンが私の住んでいる地域に少なく、新宿南口店まで約2時間かけて通っています。お店が全く無いわけではないのですが、個人経営や小さな店舗が多いのが現状です。身体を預ける身としては、大きな会社であったり通っている人が多いようなサロンの方が、何となく安心するような気がして、時間をかけてでも通うことに決めました。時間がかかるといっても新宿はターミナル駅なので、地元の駅から乗り換え少なく通えます。私はワキと足腕の脱毛をしているのですが、施術後数日でムダ毛がポロポロ抜け落ちる感覚がたまらなく好きで、この達成感にハマっています。施術中はスタッフさんから新宿の情報を教えてもらいつつ、終了後は新宿を探索して、毎回の都会への遠出が私の楽しみです。</dd>
          </dl>-->

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">周りにおしゃれな友達が多いので、たくさん脱毛サロンの情報が入ってきます。いい情報もあれば、悪い情報も。広告をよく見るお店もありますが、やっぱり店舗を選ぶ時は口コミに頼ってしまいますよね。私は色々な情報を聞いた結果、印象が良かった銀座カラーを選びました。学校帰りでもよく来る新宿にあるのがポイント高いし、サークルの先輩も新宿店に通っているので、一緒の店舗という安心感がありました。実際に通ってみると店内は綺麗だし、美容に良いハーブティーが毎回出るので、美意識も高まります。初めての脱毛だったので不安なことばかりでしたが、スタッフの方がとても丁寧に説明してくださり、施術中もファッションの話で盛り上がったりと、毎回の施術が楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ここ最近低価格な脱毛サロンが増えてきていますが、私は数年前から銀座カラー一筋です。<br>銀座プレミア店に無料カウンセリングで伺った際、担当してくれたスタッフさんが脱毛に関しての質問や相談に笑顔で付き合ってくださって、初めてのサロン通いや契約のやりとりに緊張していた私は、そのスタッフさんのファンになりました。そのうち、どのスタッフさんに担当してもらっても対応が気持ち良く、全員で居心地のいい空間を作ろうとされているのに気付き、銀座カラーのファンになりました。そして重要な施術についてですが、かなりリラックスして受けてますし、施術後のちょっとした肌の変化にもスタッフさんは心配りしてくれます。きっと赤みが出た場合も、丁寧に対応してくれるんだろうなと感じます。そんな接客力に、今後もファンが増えるのではないでしょうか。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>面倒臭がりな私なので、脱毛サロンに通うのは難しいと思って諦めていました。<br>でも銀座駅から徒歩2分、有楽町駅からも徒歩5分の銀座プレミア店は駅近の好立地なので、苦にならず通えています。昔からムダ毛には悩まされてきたので、通える喜びでいっぱいです。当初は足腕脱毛のみの契約だったのですが、スタッフさんの対応の良さや施術の正確性、銀座ならではの客層に安心感を覚えて、VIO脱毛も追加でお願いしています。VIOは足腕より多少痛みはあるものの、施術後の毛の抜けがはっきりしているので達成感があります。銀座プレミア店は落ち着いた大人の空間を用意してくれているので、アラサーな私でも通いやすい雰囲気です。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>いくつかのサロンの無料カウンセリングや施術を受けて感じたのは、銀座カラーは技術力・スタッフ力が高いということです。無料カウンセリングの施術はたったの数発しかありませんが、銀座カラーのスタッフさんはこれから行う施術の説明・段取りの説明などはもちろん、その数発もしっかりと照射してくれました。私が通っている新宿東口店だけでなく、他の店舗に通っている友人の話を聞く限り、どの店舗でもそのような対応をしてくれているようです。無料カウンセリングで本契約を決めた私ですが、ここ数年通っていて、より脱毛効果への取り組みなどが見えてきました。昔より脱毛にかかる施術時間がかなり短くなりましたし、もちろんうち漏れもありません。その新しいマシンの導入で、全体の技術力がアップしたように思います。目に見える変化に安心感を感じました。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ルミネ新宿に好きなショップが多く入っているので、月に2回は新宿に遊びに来ます。<br>元々新宿ではない別の脱毛サロンに通っていたのですが、脱毛だけに出かけるのって面倒だし、交通費も勿体無いじゃないですか。だったら新宿で脱毛サロンを探して、買い物と一緒に通えばいいかと考えました。色々なサロンを検索してみた結果、銀座カラーの乗り換え割（他サロンからの乗り換えで2万円OFF）が一番お得だったので、新宿東口店に決めました。新宿駅から近い・割引があるというだけでサロンを決めてしまったものの、サロン内はもちろん、使う機器もしっかり手入れされている感じがして好印象でした。他店舗を経験している分、スタッフの方の気遣い一つで銀座カラーへのイメージがぐっとアップしましたね。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>銀座はちょっと背伸びしていく場所のイメージがあったので、脱毛サロンも高級なところばかりかと思っていました。でも、銀座カラーには手頃にはじめられる脱毛コースがあることを知ったので、大人気分を味わえる銀座本店に通うことに決めました。立地のいい店舗によくある、場所にお金をかけてサロン自体は簡素・質素という可能性を考え、サロン内の内装は期待していなかったのですが、受付に大きなシャンデリアがあったりしていい意味で期待を裏切られた感じです。清掃も行き届いていたので、より綺麗な空間に感じました。脱毛の効果も実際の肌で感じられていますし、本当に銀座本店を選んでよかったです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>銀座本店近くのショップで勤務しているので、仕事場近くに銀座カラーさんがあって助かっています。<br>アパレル関係なので自分磨きは必須。その中でも脱毛は重要だと考えてる私としては、仕事帰りに寄れるサロン選びが重要なポイントでした。営業時間も平日21時までと長く、仕事を急いで終わらせなければならないというストレスもありません。私は夜遅くから全身脱毛コースの施術をするのですが、一時間ほどで終わるので帰りが遅くなることもありません。技術力の高さももちろんですが、帰宅時間まで配慮してくれる女性に優しいサロンのイメージが銀座カラーさんの良さだと思っています。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>銀座カラーさんに通う前は、ネットで買った家庭用脱毛器を使っていました。サロンに通う手間は省けるし、好きな時間に好きな回数ができるのでいいと思ったのですが、地道に全身照射しなければならないので時間がかかったり、背中などの届かないところは照射できなかったりと、デメリットを感じることがありました。このまま家庭用脱毛器を使っていくことも考えたのですが、ボーナスが出たので思い切ってサロンでの全身脱毛（顔脱毛付き）に切り替えたのです。サロンではエステティシャンの方がやってくれるので照射できない箇所はないですし、時間も一時間ちょっとで終わるので今までの悩みは消えました。しかも照射後のケアをしっかりしてくれるので、肌がしっとりして乾燥しなくなりました。やっぱり自己処理では限界があると思うので、脱毛をしたくて悩んでいる人はプロに任せることをおすすめしたいです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>つい最近結婚した姉から「独身時代に全身脱毛しておくといいよ。」とアドバイスをもらったので、今のうちに終わらせてしまおうと思い、脱毛について調べていました。脱毛するなら頭の先から足先まで全部やってしまいたかったのですが、意外と全身脱毛に顔が含まれているプランを扱っているサロンって多くないですね。その点銀座カラーは顔を含む全身脱毛のプランがあったので、非常に助かりました。今回脱毛箇所が多いので予約がちゃんと取れるのか心配だったのですが、早めの予約を心がければ予約は取れますし、職場近くの店舗と自宅近くの店舗の両方で予約が取れるので、都合のいい方に合わせればより簡単でした。予定通り予約を埋めていって、脱毛完了後は婚活でもしようと思います！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>小学生の時に腕毛の濃さを友達に指摘されて以来、体毛の濃さをコンプレックスに感じていました。自分で処理できるようになる前は夏でも長袖長ズボンを着て隠したり、性格まで暗くなっていた気がします。いまは自己処理をしているのですが、時間がかかって煩わしいですし、なにより剃り残しがないか毎回気が気じゃありません。そんな私を心配して、姉が全身脱毛を勧めてくれました。行ってみたいとは思っていたのですが、最後の一歩を踏み出せずにいたのを感じ取ってくれたのかもしれません。実際に銀座カラーさんへ行って施術をしてもらったのですが、顔を含む全身脱毛なのに一時間弱で照射は終わりましたし、痛みも強くなく驚きました。これからは長年のコンプレックスとしっかり向き合い、肌も心も明るくなりたいと思います。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>自分の特技は水泳なのですが、水着になるとやっぱりムダ毛が気になっていました。自分で剃れる脇や足腕はケアできていたのですが、二の腕の後ろや背中などは難しくて放っておくことが多かったです。自分で見えないので、まあいいかと思って。でも、これからも水泳を続けていくつもりなので、思い切って全身脱毛をしようと決意しました！ただエステって勧誘が激しくて、高価なケア用品とかを買わされるイメージがあったのですが、水泳仲間の口コミで銀座カラーさんはそんなことがないと聞き、絶対ここに通おうと決めたのです。実際、勧誘はほとんどなく、断る苦痛も感じなくてほっとしています。スタッフの方は優しいし、脱毛効果も感じられて、より水泳を楽しむことができそうです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>噂には聞いていたのですが、産後にヒゲが生えました（・・；）ヒゲだけでなく、頬にも太めな毛が生えて。ホルモンバランスの崩れから起きるらしいのですが、数年たっても変わらなかったので、顔脱毛を受けることにしました。顔脱毛を受ける前は自己処理をしていたので、カミソリを子供が触ったりして危ないこともあったのですが、現在はカミソリを処分できました！銀座カラーは友達が通っていたので安心できましたし、紹介キャンペーンがあったのでかなり助かりました。実際スタッフの方も同年代の方が多く、育児の息抜きに通わせてもらってます。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ウエディングフォトの準備のため、銀座カラーさんに顔脱毛をしにきています。昔は自分で処理していたのですが、不器用なのか何度も顔に切り傷を作ってしまった経験があるので、大事なこの時期のためにも、自己処理をやめて銀座カラーへ通わせてもらっています。撮影の時に顔に傷があったらショックですし。実際、顔の産毛がないだけでファンデーションは綺麗にのりますし、パウダーをはたいても粉っぽさがなく、かなりツヤ感が出ると感じています。脱毛をはじめてからというもの、毛が減ったので毛穴も小さくなった気がしますし、脱毛後の保湿ケアで肌の乾燥感もなくなりました。主人もかなり喜んでくれているので、これを機に別の箇所もおねだりしてみようと思っています。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>アップヘアが好きで、毎日凝ったヘアアレンジをしているのですが、いつもおでこの産毛が気になっていました。どこまで処理したら綺麗に見えるのかわからないし、剃りすぎて変になっても嫌なのでほったらかしだったのですが、みんな私のおでこを見ているような気がして、急に恥ずかしくなってしまいました。だからと言ってヘアアレンジをやめたくはないので、脱毛を調べてみたら銀座カラーではおでこもできたので通うことにしました。店内は綺麗だし、スタッフさんは優しいし、顔脱毛の効果もばっちりでした。脱毛後のミストシャワーもあったりして、ちょっと贅沢している気分です。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>百貨店のコスメカウンターで美容部員をしている仕事柄、美容の意識は常に高くいたいと思っています。もちろん脱毛は必須で、顔の産毛はカミソリで自己処理をしていたのですが、肌の負担が心配でした。メイクが映えるようにと思ってやったものの、自己処理で赤みやヒリつきが出たこともあります。肌荒れで仕事に影響が出る前にプロにお任せしようと思い、足と腕で通っていた銀座カラーさんで顔脱毛をお願いしました。私が通っている店舗は平日22：30まで受け付けてくれるので、仕事終わりの疲れた肌に保湿ミストのケアで、一日の疲れもリセットされる気がします。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ずっと全身脱毛をしたいと思っていたけど、強引な勧誘が怖かったり、価格を見て諦めていました。でも銀座カラーなら学割もあるし、勧誘がないと聞いて無料カウンセリングを申し込みました。実際に行ってみると、店員さんはみんな優しくて大満足！心配していた勧誘もないし、分からないことも丁寧に教えてくれて契約することに決めました。今では腕も足も自己処理いらずで満足！友達にも脱毛を勧めてます！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>大学生なので勉強もバイトもあるし、あまり美容に時間をかけられませんでした。でも就職する前に1度は脱毛したいと考えていましたが、なかなか踏ん切りがつきませんでした。同じ気持ちだった友達と一緒にとりあえず話だけ聞きに銀座カラーの無料カウンセリングに行ったんですが、じっくり話を聞いてくれて嬉しかったです。脱毛のことだけじゃなくて料金のことや、どれくらいの間隔でサロンに通えばいいとかも相談できて、安心して契約できました。友達と一緒に契約したので「ペア割キャンペーン」でお得に脱毛できました！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>天神店の近くはオシャレなお店が多いので、お買い物気分で通っています！他のサロンでVIO脱毛をした時に痛くてやめてしまってから、「脱毛は痛い」っていうイメージがありました。でも友達が「銀座カラーは痛くない」というので、行ってみたらほとんど痛くなくて驚きました！スタッフさんの接客態度も良くて快適！家でできる肌ケアのことまで教えてくれました。脱毛後の肌荒れもなくて、銀座カラーを選んで本当に良かったです！思い切って全身契約しちゃいました！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>昔から人よりも毛が太くて気になっていました。仕事で腕まくりをする機会が多くて毎日自己処理が大変だったので脱毛することにしました。銀座カラーは他のサロンに比べて予約が取りやすいと聞いたので行ってみると、本当に予約が取れて安心。しかも施術が1回60分位で終わるので、忙しい私にもピッタリでした！価格はリーズナブルなのに、脱毛効果が高くて腕はツルツル。脱毛した箇所は肌の調子が良くなって、仕事でお客さんに「綺麗な腕ですね」と褒められました。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>仕事が不規則なのでなかなかサロン通いができなくて困っていました。でも銀座カラーなら予約が取りやすいし、天王寺店は駅近で通いやすくて助かっています。美容系のサロンだと勧誘がしつこかったり、高いコースを勧めてくるお店もあるけど、銀座カラーはそういうことがなくてリラックスして施術を受けられます。以前は3日に1回はお風呂場で寒い思いをしながら除毛しましたが、今は月に1回サロンに行くだけなのでとても楽です！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>友達の結婚ラッシュでドレスを着る機会が増えたので、安く脱毛できるところを探していました。知り合いから「銀座カラーは保湿ケアしながら脱毛できる」と聞いて、無料カウンセリングを受けました。店員の接客が明るくて丁寧で好印象。しかも他のサロンだと最低2年はかかるのが、1年で全身脱毛できるのですごく嬉しいです。価格もリーズナブルだからご祝儀貧乏な私には大助かり（笑）心配していた脱毛後の肌トラブルもないし、これならいつでもドレスを着て肌を出せます！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>毛はそんなに濃い方ではないので脱毛に興味はなかったんですが、夏休みにたくさん旅行に行きたくて脱毛を考えました。最初は腕と足だけできればいいかなと思っていたんですが、スタッフさんが「全身脱毛の方がお得ですよ」と教えてくれて、全身にすることにしました。初めての脱毛でちゃんと効果があるのか不安だったんですが、段々と毛が細く少なくなっていくのが分かって満足です。最近では自己処理をしなくてもムダ毛が気にならないので、カミソリがホコリをかぶってます。自分では処理するのが難しかった背中もキレイになって、旅行に行くのが今から楽しみです！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>いろんなサロンのお試し脱毛をやってみましたが、自分に1番あってると思ったので銀座カラーに決めました。他のサロンは追加料金が多くて段々嫌になってしまって...でも銀座カラーは料金がわかりやすくて信頼できます！お店も清潔感があるし、店員さんも親身になって話を聞いてくれるし、ここにしてよかったです。おかげで自己処理でボロボロだった肌がすべすべになってきました！今回は足のコースだけだったので、次は一気に全身のコースをしようかなと思っています。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>友達に誘われて銀座カラーにしました。脱毛には前々から興味はあったんですが、本当に効果があるのか不安で。友達に効果を聞いて、良さそうだったので無料カウンセリングを受けました。カウンセリングでは無理やり高い契約を勧められることもなく、ちゃんと相談して私にあった脱毛コースを考えてもらえて安心感がありました。実際の施術でもスタッフさんのテキパキとした施術で、サッと脱毛できて満足です。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>スタッフさんが優しくて、変に敷居も高くないところが気に入ってます。藤沢店は店内もキレイだと思います。他のサロンだと2年以上かかるのが1年で終われるのも評価高いです。飽きっぽい性格でサロン通いも途中で挫折してしまうんですが、これなら最後まで通い続けられそうかなwあと脱毛後に美容効果のあるハーブティーをいただけたりして、そういうちょっとした気遣いが居心地いいです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>以前に契約したサロンの予約が全然取れなくて、サロン通いに抵抗がありました。でも自己処理でカミソリ負けした肌を見るのが嫌で脱毛サロンを探していました。ネットで「お客様満足度No.1」とあったので調べてみると、予約も取りやすくてお肌のケアもしっかりしてるとあって決めました！脱毛効果はもちろん、「美肌潤美」のおかげでガサガサだった肌がツルスベになって感動です！もっと早く銀座カラーに通えばよかったと後悔してます！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>CMを見て興味がわいたのでカウンセリングを予約しました。お店の雰囲気が良くて気に入ったので通うことにしました。腕と足はもう別のサロンで脱毛していたので、VIOセットを契約。VIOはどうしても他のパーツに比べて痛みを心配していましたが、ほとんど痛くなくてあっという間に脱毛できました！脱毛効果も高いし、痛くないしでこの値段なら本当に安いと思います。駅からのアクセスもいいし、施術時間も短いので、待ち合わせ前のちょっとした時間に行けてとても便利です！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>様々な学割が出て、脱毛学割なんてないかな〜って探してたら、銀座カラーにありました。学生のうちに安く脱毛できて、学生期間に脱毛が終わるし、あいた授業の間に通えるのがメリットだと思います。カウンセリングや施術してくれる人、みなさん優しいし、サロンも出来たばかりで、清潔感があってオシャレなので、癒されながら脱毛しています。来年の春には卒業するので、学割があるうち、いっぱい得しようと思います（笑）銀座カラーさん、これからも宜しくお願いします☆</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ホントに恥ずかしいんだけど、ひげが生えてて、ここだけはどうにかしたい！！と思い、無料カウンセリングへ行きました。ヒゲ、ではなく鼻下っていうみたいですが、みんな悩んでるって聞いて、自分だけじゃないんだなって思えたし、脱毛する後押しにもなり、顔脱毛をはじめました。顔周りの毛って、毛抜きで抜くと涙が出るほど痛くて赤くなるし、カミソリで剃ると、お肌を切っちゃうかもしれないとビクビクして自己処理するには大変な場所なんだけど、銀座カラーで施術してみたら痛みもなくて、あっと言う間に終わりました。毛が目立たなくなったし、お化粧のノリが良くなったと実感しています。プロにお任せして良かったと思います。美肌潤美のフェイスパックを使うと、しっとり度が増して、お肌の状態がよくなります。オススメです！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>彼氏と付き合い始めた頃に、彼から「女の子って、お肌ツルツルなんだね〜」と言われました。どうやら、女子には毛が生えてこないものだと思ってるみたい・・・（笑）これじゃ、ジョリジョリした時にショックうけるだろうし、いつも可愛くてツルツルな彼女と思われてたくて、彼に内緒で脱毛をはじめました。自己処理しても毛穴は目立つし、自分にしか分からないけど剃り残しがあって、脱毛もそんな一時的なもんだと思っていたのですが、銀座カラー通って、みるみる脱毛効果が。毛穴がだんだん目立たなくなってきたし、自己処理の回数って本当に減るんですね。そして、丁寧に施術してくれるので剃り残しの心配もナシ。いつも自慢なツルツル肌の彼女でいれてます。自慢が、私に自信をつけてくれています。「キレイがずっと近くなる」このキャッチコピー、そのまんまなんだなと思いました（笑）</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>学生の頃から毛深くて、剃っても剃っても生えてくるし、自分では処理しにくい背中を母や妹に手伝ってもらっていました。１人暮らしをはじめると、手が届かない背中の処理に困ったので、脱毛専門店に通うことにしました。何回通えばキレイになるのか想像つかなかったし、回数がおわって追加料金が発生するのも嫌だったので、脱毛し放題コースにしました。この前、実家に帰った時に妹に背中を見てもらったら「毛が薄くなってるし、背中ニキビもない！キレイになってきてるね！」と言われて、本当に通って良かったなって思います。それも満足いくまで施術できるなんて、毛深い私には本当にお得なサロンだと思います。キャンペーンにおともだち紹介があるので、脱毛に興味を持った妹に紹介しようと思います。学割があるみたいなので、私ももっと早くはじめたかったな〜（笑）</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>営業職なので担当地域が変わることが多いのですが、サロン移動ができるところがイイです！どの店舗へ行っても、問題なく続けて脱毛できるので、エステティシャンの教育の徹底が伺えます。<br>
今まではカミソリで自己処理をしてきたので肌荒れがひどかったのですが、「美肌潤美」の保湿ケアでつるつるが保てて、乾燥しにくくなりました。高圧噴射で、角質層の奥まで浸透しているという説明も納得できます。肌トラブルが減った上に、毛穴のポツポツも目立たなくてなって、後輩からも「先輩の肌って、すごくキレイですね」と言われて、嬉しかったです。美肌潤美って、素晴らしいですね。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>無名の脱毛サロンがどんどん増えていますが、昔からあるサロンの方が安心感があるので、銀座カラーに決めました。老舗って古いイメージがあったけど実際に行ってみたら、清潔感があって、おしゃれな雰囲気でした。<br>
痛みには敏感のほうだけど、ほんのり温かいと感じるだけで、痛みもなく施術が完了してました☆早さにもビックリ！あっという間に全身の脱毛ができるんですね！！美容成分が入ったハーブティーも美味しかったです。脱毛後のお肌のこともちゃんと考えてくれているって、さすが老舗のこだわりを感じました。こちらに決めて正解です。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>結婚することが決まったので、早く脱毛できるサロンを検討していたところ、銀座カラーのスピード脱毛を見つけました！ウェディングドレス着たときに見える、ワキ・腕・背中・うなじの4ヵ所予定でしたが、無料カウンセリングをうけてみて、結婚式だけではなく将来もずっと綺麗な肌でいたいと思ったので、全身やることにしました。写真の前撮りや結婚式の予定に合わせ、毛が抜けるスケジュールで施術ができたので、予約がちゃんと取れたのがすごく良かったです。他のサロンにはない、脱毛スピードと丁寧に施術してくれるところが最大のポイントだと思います。＾＿＾♪</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>アクセスがいいので、埼玉県から通っています。全身まるごと脱毛し放題ができるのは、とっても嬉しいです。友達に比べると毛深くて、回数制限のあるサロンはキレイになるのか不安でしたが、銀座カラーなら何度でも通えるのが魅力的です。全身の脱毛１回が、１回の来店で施術対応してくれるので、通う回数も少なくて、毛深い私でもストレスフリーで通えています。しつこい勧誘もないので、友達にも勧めやすいです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ミーハーな私は銀座カラーが札幌にできるのを、ずっと楽しみにしていました。オープンする前から事前予約して、実際に銀座カラーに通えた時はとても嬉しかったです。サロンはシャンデリアがあってゴージャスな雰囲気があり、サロンもスタッフもムダ毛のない肌もキラキラしています。名ばかりじゃない、さすがの大手サロン！スタッフの教育からサービスまで力をいれているのが分かるので、毎回気持ちよく通えます。もちろん脱毛効果にも満足しているので、全身つるつるになったら、全身で恋したいです（笑）</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>職場からとても近く、アクセスの便利性と保湿ケアに惹かれて銀座カラーにしました。24時間ネット予約がとれるので、シフトが変更になっても自分の都合にあわせて予約がとれるので気に入っています。私自身、コスメや肌のお手入れに、こだわりを持っているのですが、銀座カラーの美肌潤美にはとても感動しました☆ ミストシャワーをしてもらった肌は、もっちりで誰かに触ってもらいたくなるほどです（笑）自宅でもケアできるローションとフェイスパックは、まるでサロンでケアしてもらったような感じで、ちゃんと肌を考えてくれていると安心して通えます。フェイスパックには種類があり、その時の気分で購入もできるので、ポイント高いです♪</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>他のサロンに通っていたのですが、友達の紹介で銀座カラーにのりかえました！なんといっても通っている友達の紹介だから、脱毛した肌を実際に見て納得してからサロンに行けるので、信頼度が大きいのが決め手でした。職業上、腕まくりをしている事が多いので、お客様によく見られます（笑）美を扱う者として肌もキレイと言われるのは、やっぱり嬉しいです。これからも自信もって接客できそうです。はじめから全身脱毛を銀座カラーでやっていればよかったなって思うけど、友達に紹介してもらって本当に良かったです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>友達との卒業旅行で、海外のリゾート地へ行くことになりました。友達とおそろいの水着を買ったのはいいけど、自分のムダ毛がとても気になったので脱毛をすることしました。はじめは、ワキ、腕、足、アンダーヘアを脱毛できればいいと思っていたけど、全身の脱毛し放題のほうがお得にできそうだと思ったので、全身脱毛コースにしました。脱毛をはじめて2ヶ月ちょっとですが、ちゃんと予約もとれて3回目の脱毛が無事完了。自己処理が減って、効果を実感してきたところです。海外旅行だけではなく、これからは水着シーンが増えそうです。\(´ω` )/ 脱毛をはじめて本当によかったと思います。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">ワキ、腕、脚は自分で見ても、だんだん毛がなくなってきているのを実感していたのですが、先日、友達と温泉に行ったときに「背中、すっごいキレイだね！毛も全然生えてない！！」と褒められてすごい嬉しかったです。背中は自分で見れないので、実感があまりなかったのですが、思わぬところでやっておいてよかったな、と感じました。
夏になると、電車の中や街中でも、背中が開いた服を着ている人の肌は気になって見てしまうのですが、自分も見られていると思うと、そういう服は避けるようにしていました。でも、友達に褒められたことで自信がつきましたし、これからは洋服選びも楽しめそうです。
今通っている渋谷109前の店は、きれいで居心地も良くて、安心して通える雰囲気なので気に入っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">予約がとりやすいサロンを探していたときに、口コミで評判がよかったので銀座カラーに決めました。以前、通っていたサロンは初回予約は問題なかったものの、２回目以降は自分の希望通りには全然予約が取れなくて、すごく困りイヤになりました。
実は1年後に結婚式が決まっているので、それまでにベストな状態にしたかったので、自分の予定どおり脱毛できるのが、マストな条件でした。こちらのお店だと、予約が取りにくいと思ったことはほぼありませんので、このまま続けていけたら式にも間に合いそうですし、当日、キレイな姿で迎えられると思うと今から楽しみです！
通うのも、いろいろ買い物があるついでに来れる渋谷で、しかも駅からも近いので、ストレスもない感じです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラーにしたのは、お店のある場所がよかったのと金額です。マルキューの前は超便利だし、安いのもうれしいです。費用はいくつかのお店と比べてみたけど、かなりお得に感じました。
意外と気になっていたのが足指に３～４本、生えてくる毛でした。気づいた時には、抜くようにしていたのですが、たまたまネイルサロンに行く前にチェックするのを忘れてしまって、恥ずかしい思いをしました。特に何か言われたわけではないのですが、絶対に気づかれている！と思ったら、すごい恥ずかしくて。。。この事件で脱毛しようと決めました。自分で処理していると、どうしても剃り忘れや剃り残しがあるので、キレイでいることに気を遣いたいけど、面倒なのはなるべく避けたいという人には脱毛は本当におススメだと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">全身の脱毛が回数無制限でできるのは、本当にすごいと思います。以前、他の脱毛サロンに行っていましたが、そのサロンでは全身コースといっても、iライン、Oライン、Vライン、えり足などは別料金でした。銀座カラーではそのあたりの箇所も含めて、本当に身体中、全部が施術範囲に含まれていてビックリしました。せっかくなので、iライン、Oライン、Vライン全てまるっと脱毛したいので全身VIPコースで始めました。
場所も渋谷の、しかもよく行く109の前なので行きやすいし、スタッフの方も話しやすくていいと思います。</dd>
          </dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


<span itemprop="name">20代 スポーツインストラクター<br></span>
</dt>
<dd itemprop="reviewBody">全身のムダ毛を早くなくしたいと思い、職場の近くにある銀座カラー柏店に通いました。勝手なイメージで、脱毛サロンはシンプルな内装でつまらない場所と思っていましたが、柏店は、店内がゴージャスだしスタッフの方が綺麗なので、行くだけでワクワクできて楽しく通えました!<br>さらに1回の施術時間も早いし、効果を感じるまでも早かったので、銀座カラーを選んで本当によかったなと思っています。<br>あと私は、オプションの保湿マシンもつけたのですが、そのおかげでひどかった乾燥肌も改善されました。ぜひ、他の方にも体験してもらいたいです!！</dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


<span itemprop="name">20代 美容部員<br></span>
</dt>
<dd itemprop="reviewBody">銀座カラーで脱毛を始めてから、メイクのノリが変わってきたんです。仕事柄、毎日しっかりメイクをするので、肌荒れがあると隠すのに必死...。朝からテンションが下がっていました。<br>でも最近は、肌荒れすることが少なくなってきたので、メイク時間もファンデーションの使用量も、大幅に減ったんです。接客中、お客様から肌が綺麗と、褒めていただけることもあります!!<br>脱毛のおかげで、ムダ毛の自己処理がなくなって楽なのも嬉しいですが、綺麗な肌状態でいられるのがすごく幸せです。
</dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody">柏にある大学に通っています。同じ大学の友達たちが脱毛を始めて、どんどん綺麗になっているので、私も通いたいと思い銀座カラーを紹介してもらいました。<br>バイトのお給料で支払えるか少し不安もありましたが、学割もあってお得に契約できたので、無理なく通えそうです!<br>しかも、紹介してくれた友達にも特典があったので、お互いハッピーになれました。私も、大学の友達にオススメしてみようと思っています。<br>柏店は、大学から近くて通いやすいのが、私には魅力的です!!<br>学校やバイトのスケジュールの合間に通って、どんどん綺麗になっていきます。</dd>
</dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">友達が銀座カラーの銀座のお店に通っていたので、いろいろ話を聞いて紹介してもらいました。前から脱毛に興味はあったのですが、どんな風に施術していくのかイメージがわかなくて、ちょっと不安がありました。でも、友達が実際にやってみての感想を教えてくれたので、「それならやってみたいかも！」と思えるようになりました。
痛みは、聞いた話だけだと分からないですが、施術前にパッチテストというのをやっていただいたおかげで安心できました。実際、思っていたよりもぜんぜん痛みはありませんでした。これなら続けられると思っています。上野の銀座カラーに通っていますが、この間、用事のついでに新宿のサロンでも施術してもらいました。お店がいろいろなエリアにあるので、予約も便利です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">何回かやって毛が減ってきて、もっとキレイにしたい、という欲が出てきました。最初は、ワキから始めて、腕や足など、いつも自己処理をしていた箇所に広げていきました。どこも、自己処理の回数が減ってきて、かなり楽になりました。
一番驚いているのは、今やっている顔です！顔の脱毛ってあまり聞いたことがなかったし、特にやろうと思ってなかったのですが、実際にやってみるとお肌の状態はよくなりましたし、化粧水の入り方も違ってきた気がします。お肌がいい状態に保てているので、化粧ノリもよくなったと思います。脱毛したというより、美容エステに行っている感覚です。サロンも清潔できれいなので、いい気分できれいになれてうれしいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛し放題コースというのにしました。剛毛な私は、どのくらいでキレイになるのかわからなかったので。厚着の時期でも、暖房などで暑くなって、つい腕まくりなどをすることがあるので、１年中腕などは見られるので、最初に集中的にやってもらいました。これまでお手入れが大変だったし、気が抜けなかったけど、今は気にしなくていいので楽です。
夏に向けて、スカートやショートパンツをはいた時に気になる脚をやっていこうと思っています。これまでカミソリで剃っていた話をしたら、このまま続けていくと肌荒れや皮膚の黒ずみの原因になってしまうと、スタッフの方に言われました。もっと早く始めればよかったです。これからでも遅くないみたいなので、がんばりたいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">背中や腰など、自分では処理しにくい部分の脱毛がしたくて、カウンセリングを受けました。スタッフの方はとても丁寧に分かりやすく説明してくれて、実際に自分の背中の毛の生え方などを見てもらって、どういったコースでやっていくのがいいかアドバイスしていただいてよかったと思います。友達や家族でも、肌をまじまじと見られるのはちょっと恥ずかしいですし、プロの方の意見だと素直に聞ける感じがしました。実際、電車に乗っているときなど人の背中をつい見てしまうことも多いので、自分の背中も気になっていました。
脱毛だけじゃなく、美肌効果もあるということでしたが、この間、プールに行ったとき、友達に「背中、超きれいじゃん！」と言われて本当にうれしかったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">スタッフの方たちが皆さんとても仲が良くて、アットホームな雰囲気がそのまま伝わってくるので、居心地がいいです。話しやすいのでプライベートなことも、ガールズトークで盛り上がったりすることもあります。でも、脱毛のことはプロとしての意見やアドバイスをいろいろしてくれるので、楽しいだけではなく、安心感や信頼感もあります。
銀座カラーに通う前は、ずっと自己処理をしていたのですが、一生かかる手間を考えたら脱毛にしてよかったな、と思います。あと、サロンによっては予約が取りにくいところもあるということを聞いたことがあり、始める前は不安でしたが、銀座カラーの場合、ほとんど、自分の希望の時間に予約を取ることができているので予定が立てやすいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">全身コースを申し込みました。1時間でまとめて全身脱毛の施術ができると聞いたので、やってもらいました。最初は時間が長いので大変そうだな、と思ってましたが、やってみると意外とあっという間に終わってしまい、拍子抜けするくらいでした。やっぱり、１日でまとめて受けられると効率がいいので楽です。彼氏や友達との予定もいろいろあるので、通う回数を少なくしたいので。。もちろん、自分の予定で、数回に分けてやることもできます。時間や回数が自由に決められるのは、本当に助かります。
スタッフの方も話しやすくて、友達みたいに楽です。晴れている日には、帰りに井の頭公園まで足をのばして、ゆっくり散歩したりもします。楽しく通えて脱毛もできてよかったと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">住んでいる最寄り駅は吉祥寺ではないのですが、買い物ついでに通えるところがいいな、と思って探して、銀座カラーの吉祥寺北口店に通うことにしました。サロンの周りにパルコやアトレ、キラリナなどがあって便利です。希望通りのお店に出会えました。
回数無制限の脱毛し放題コースを申し込んだのですが、どうしても通えない状況になった時には、お休みしてもOKということだったので、それはとても魅力的だと思いました。何年先も脱毛し放題なんて、すごいと思います！仕事が忙しくなってしまった時や妊娠した時などに、というように、分かりやすい例で説明してくださいました。本当にそういう状況になることもあると思うので安心です。
もう３回ぐらいやりましたが、脱毛だけじゃなく、肌がきれいになっていると感じます。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">駅から徒歩3分という近さなので、通うのがとても楽です。通いやすさが銀座カラーに決めた理由の１つです。最初は、一番気になっていたワキだけのつもりで行ったのですが、やっているうちに他の箇所も気になってきたのと、ワキの毛がなくなっている実感がだんだん持ててきたので、全身脱毛で申し込みました。他店に通っていたことはないので比べることはできないのですが、全身コースでも安いという感覚はありました。ワキだけと比べると全身はやっぱり高いけど、支払い方法も分割を選べて助かりました。分割の手数料がかからないというのも良いサロンと思えた理由です。追加料金もないので安心して通い続けています。スタッフの方は優しくて感じがいいし、吉祥寺の街も好きなので、じっくり通って全身ツルスベをめざします！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">ずっと髪を伸ばしているので、アップにすることが多くて、えり足の毛が気になっていました。自分の、というより、電車やバスで、人のえり足とかを見てると、自分のは大丈夫？？と思うようになり、カウンセリングでお店の人に見てもらいました。その場で自分のえり足も写真に撮って見せてもらい、やっぱりヤバかった！とすぐに脱毛することにしました。この間、美容院に行って、美容師さんに「えり足、きれいだねー」と褒められて、すごく嬉しかったです。毛がなくなるだけじゃなく、肌もきれいになるっていうのが、本当にいいです。自分に自信が持てました。
川崎駅前の銀座カラーは、私の好きなピンク色のインテリアがかわいくて、居心地のいい雰囲気で気に入っています。スタッフの方もやさしいので、声もかけやすくていいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">私が通っている川崎店は、川崎の駅前にあって、すごく通いやすいので気に入ってます。銀座カラーはどこの店でも予約が取りやすいので「予約が取れなくて困る…」みたいな悩みはなくていいですね。しかも都内にも店舗が多いので、今度、他のお店にも試しに行ってみたいです！
私は今まで、お風呂の時間がほぼ毎日ムダ毛処理時間と化していました。結構めんどうだけど、仕方がないと思っていましたが、最近ではその回数もずいぶん減ってきて、肌そのものもすべすべになってきたし、色も白くなってきた気がします。勇気をもって始めてよかったです♪今度の温泉旅行がすごく楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">全身脱毛をしています。最初、ワキと腕だけにしようと思っていたけど、スタッフの方といろいろ話をして、背中とかもやりたくなったので、それなら全身脱毛がお得なので、全身コースに。。。今はやっぱり全身コースにしてよかったと思っています。やりだすと、次から次へ、いろいろやりたくなります。きれいになるので(^^)/
忙しいので、通う回数は少ない方がいいので、1回３時間で全身のお手入れができる予約をよく取りますが、予約が取りづらいと思ったことはありません。３時間は長くて大変かなとも思ったけど、意外と話したりしてるとあっという間です。
川崎駅からすぐなので、通いやすいし、サロンの雰囲気も清潔できれいでいい感じです。スタッフの方もすごく感じがいいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">顔の毛が少し濃いのが子供のころから気になっていたので、思い切って脱毛することにしました。最初、不安だったけど時間をかけて丁寧にカウンセリングしてくれたので、いろいろな脱毛の話を聞けました。脱毛して毛が薄くなることでも嬉しかったのに、施術後の保湿ケアも充実してると聞いて施術を受けることにしました。
今、脱毛して本当によかったと思っています。顔の毛を剃らないといけないことはほとんどなくなりました。フェイスラインもすっきりしてきたし、肌にも透明感が増したような！髪を耳にかけたときの横顔の雰囲気が美人になったと、彼氏にも言われました！今まではどうだったわけ？と思いましたが。。。私の効果を見て、高校のときの友達が一緒に通うと言い出しました。川崎店はきれいで通いやすいので、うれしいです♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">全身脱毛をやりたいと思い、いろいろ調べている中で銀座カラーにたどり着きました。決め手はやはり価格です。他店のコースや料金も見てみましたが、全身コースをやるとなると、ココ！という感じでした。
まず無料カウンセリングを受けましたが、その際に、回数を気にせずに通えること、他店にはない保湿・美肌に力を入れていること、引越しなどで住む場所が変わったとしても違う店舗に移れること、など、いろいろ教えていただきました。料金以外にもさまざまなメリットが感じられて、お願いすることにしました。カウンセリングの時から、スタッフの方の対応は良いし、サロンもきれいで、大丈夫とは思っていましたが、実際に通い出してからも思った通りで、すごく満足しています。脱毛の効果も、美肌の効果もうれしいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">最初にサロンへ行った際、店内の白とピンクのインテリアがとてもかわいくて私好みで、すぐに気に入りました！スタッフの方も、いつもニコニコと笑顔で迎えてくれるので、毎回とても気持ちがいいです。人見知りの私でも話しやすいスタッフの方が多いので、ちょっと聞きにくいこともすんなり聞くことができ、不安に感じることはほとんどありませんでした。あと、脱毛後のアフターケアとして、保湿にはとても気を使っているというのも、いいなと思います。実際、ニキビに悩んでいたのですが、以前ほどひどくならなくなったような気がします。毛がなくなったから、というのが一番なのかもしれませんが、お肌もツルツル美肌になってて、気がつくといつも自分で触っていて、かなりイイ感じです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">他のサロンで体の脱毛はやっていて、銀座カラーでは顔の脱毛をやっています。一番驚いたのは、脱毛することで肌のトーンが明るくなったこと。また化粧ノリもよくなったので、以前よりも薄化粧になったと思います。脱毛をしながら肌もキレイになるとは思っていなかったので、お得な気分（＾＾）前に通っていた店と比べると、顔の施術範囲が広くて、全体的にやってもらえるのはすごく嬉しいです。あと、脱毛とはまったく関係ないのですが、川越駅の駅ナカには、パン、お菓子、スイーツなど美味しい誘惑がたくさんあって、サロンの行き帰りにいつも目を奪われてしまいます。ただ通うだけじゃなくて、こういうおまけもついてくるので、楽しみながら通えています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">初めてサロンに行ったとき、駅から近いし、駅を出てすぐのところに「銀座カラー」の大きな看板もあったので、迷わずたどり着くことができました。方向音痴の私にはとても分かりやすい場所にあり助かります。
カウンセリングを受けたときに、どうせやるなら・・ということで全身コースにしました。追加の心配がいらないので安心して自分が納得するまでできています。
全身で面積が広いためパーツごとに分けて施術をするのかと思っていましたが、銀座カラーさんは1回で全身を施術するので、施術をした日は全身がとてもスッキリした気分になります。パーツごとに脱毛すると、脱毛した部分としていない部分の境目がハッキリとわかってしまうようですが、全身コースだとそんなこと気にせず脱毛してもらえるので仕上がりも大満足です！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">他のサロンに通っていたのですが、銀座カラーに変えました。前のところは、すぐに追加料金と言われ、お金がかかって仕方がないので、バイト代がなくなっちゃうし、友達の話とか口コミもチェックして、銀座カラーがいいと思い無料のカウンセリングに行きました。追加料金ばかりで高くなるのは困ると、いろいろ正直に相談したら、銀座カラーはそういうのがないように、脱毛し放題コースがあると言われ、それにしました。何年通ってもいいなんて、すごいです！1度で全身一気に脱毛してもらうこともできるので、忙しいときは便利です。お店はいつ行っても、いい雰囲気で、スタッフの方も話しやすいです。ストレスなく通えそうなので、全身きれいにしたいと思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">最初は大学の帰りに寄れる銀座のお店に通っていました。地元の船橋にもお店ができたので、最近はこちらに通っています。休みの日でも通いやすくなって、しかも新しいので明るくてきれいで、すごくいいです。スタッフのみなさんも年が近いし、話しやすいので、楽しく通えています。最近、妹も通い始めました！仲がよくて一緒にお風呂に入ったりもするので、私の肌がきれいになるのを見て、うらやましくなったみたいです。自分だけ、ムダ毛処理してるのがイヤになったんだと思います（笑）。実際、バスタイムにやることが減って、ゆっくりお風呂につかれるので、快適です～。
あと、保湿マシンがすごいです！全身に贅沢な美容液がいっぱいしみ渡って、肌がプルプルで感動的です。ぜひ試してみてください。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛し放題コースをやっています。洋服が好きで、おしゃれが好きなので、脱毛は絶対やりたいと思っていました。でも高いので、なかなかできなくて。。銀座カラーなら全身脱毛でも、かなりやりやすい金額だと思います。しかも、期限がなく、やりたいだけ脱毛できるってすごくないですか？！銀座カラーを見つけることができて本当によかったと思っています。
今通っている船橋北口店は新しくてきれいだし、雰囲気も良くて居心地抜群です。結構いろいろな年齢の人もいて、通い始めたときは、ちょっと驚きました。女性はみんな、いくつになっても美容が気になるんだなーと思います。脱毛しながら肌もかなりきれいになり、エステに通ってるみたいで、すごくお得だと思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">初めての脱毛です。最初はいろいろ分からなくて不安もあって、お金のことも心配で、腕だけを申し込みました。でも、肌がすごくきれいになって、脱毛したところとしていないところの境目が気になってしまって、通い始めて不安もなくなったので、全身脱毛に変えました。今となっては費用的にもかなりお得だし、最初から全身脱毛にしておけばよかった。。って感じです。自分がそうだったので、気持ちは分かるけど、勇気を出して最初から全身コースが絶対にいいと思います！
地元の船橋に銀座カラーがあり、便利だしお店の雰囲気もいいです。仕事帰りに銀座のお店に予約して行ったこともありますが、銀座のお店はかなりリッチな雰囲気。でも、私は船橋のお店が落ち着いてリラックスできます～。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛にお金かけるなんてもったいない、と思ってました。でも、剃ったあと、また生えてきたとき、短い毛がチクチクする感じがして、肌も荒れてきて、やっぱり脱毛するしかないかと思い、相談に行きました。最初のカウンセリングで、お金のこと、痛いか、体に何か影響はないか、とか、心配なことはほとんど分かりやすく説明してもらって納得できたので、まずは足から始めることにしました。正直、こんなにきれいになるなんて、と感動しました！3回目で全身脱毛のし放題コースに変更しました。最初からこっちにしておけばよかったです。無制限だし、本当におススメです。全身の脱毛が１日３時間でじっくりできるのもいいです。やっぱり何回も通うより、なるべく１回にまとめられた方が、時間も取りやすくて助かります。仕事のときや休日などで、池袋に行く時と新宿に行くときがあります。どちらのサロンも清潔で、スタッフの対応もいいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">毛が濃い方だし、ムダ毛の処理がいつも面倒で、いつか脱毛したいなーと思ってました。でも光脱毛とかちょっと怖いし、肌にシミとかできたりしない？とか、体に悪いんじゃないか？とかいろいろ考えて、ためらってました。昨年、いちばん仲のいい友達が銀座カラーで脱毛を始めたので、めちゃくちゃいろいろ感想とか聞いて、どうやら大丈夫そう！と思ってカウンセリングに行ってみることに。思ったより全然、サロンの人はやさしいし、きれいなサロンで雰囲気もいいし、値段もそんなに高くないことが分かったし、体にも悪くないみたいなので、始めることにしたんです。先に通い始めてくれた友達に感謝してます～。仲のいい友達の話だから、信用できて勇気も出たので！毛が気にならなくなっただけじゃなくて、肌がきれいになってきました。脱毛はじめてよかったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">全身脱毛し放題コースをやっています。髪の毛の生え際からつま先まで、どこをやっても、何年かけてやっても脱毛し放題ってすごい！と思いました。本当にすごいです。始めてよかったです。肌がすごくきれいになってます。友達に肌を見せるとうらやましがられて気分がいいです。この間、友達と温泉に行ったんですが、前はタオルで隠したりするのが普通だったけど、見せたくなる感じですよ（笑）。友達も、しかも2人も！近々、銀座カラーに通い始める予定です。
池袋サンシャイン通り店のスタッフの人はみんな話してて楽しくて、脱毛してる時間もあっという間に終わっちゃう。サロンもすごくきれいですよー。東口からすぐ近くだから、面倒くさがりの私もがんばって通えてると思います。おススメです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">普段は池袋サンシャイン通り店に通っています。池袋には２つ銀座カラーがあるので、こっちで予約が取れなかったときは、もうひとつのお店に行ったりできて便利です。スタッフの人もどっちの店も感じがいいので、どっちに行っても居心地がいいです。
最初はお金の話になり、お得だとは思ったけど、やっぱりワキと腕だけにしました。でも始めてから、脱毛したとことそうじゃないところが、かなり境目ができてしまって、結局全身脱毛にしました。最初から素直に全身脱毛にしとけばよかったです。。境目ができるのは、脱毛したところの肌がきれいになりすぎるから！想像以上にきれいになって、本当にうれしいです。夏が楽しみ～！！！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">今までいくつかの脱毛サロンで無料カウンセリングを受けてみたけど、どこも広告を見て行っても、そんな安い値段じゃできなかったり、かなり強く高いコースを薦められたりして、結局怖くてやめてました。でもやっぱりムダ毛がかなり気になるので、今度こそ！と思ってCMを見て気になった銀座カラーの町田モディ店に行ってみました。
ここは本当に安いと思います。広告の内容と実際の内容も同じで、そこが信頼できて、思い切ってやってみることにしました。スタッフの方も感じがよかったし、駅近で便利なのも評価高いです。
今はかなりのツルスベ感に感動してます。脱毛で美肌になるってホントだったんだ！って感じです。安いので全身脱毛にしましたが、保湿マシン？みたいなのも、エステみたいで、本当に肌がきれいになります～。彼氏にも褒められました(^^)/</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">全身脱毛でもかなり安いので、逆に大丈夫？って始めるときは不安だったけど、20年以上やっていると聞いて、銀座カラーで脱毛してみることにしました。かなり毛深い方で、子供のころから気になっていたけど、脱毛は高いし無理って思ってました。思い切って脱毛することにして本当によかったです。毛がなくなっただけじゃなく、肌がとてもきれいになりました。今は、半袖やノースリーブの服を思い切り着てます。ずっと長袖ばっかりだったので、本当にうれしいです。
私が通っている町田モディ店のスタッフの皆さんは、みんな気さくで優しくて、ちょっとした質問も気軽にできます。最初は少し緊張していましたが、今は全然平気で、肌がきれいになるのもあるけど、サロンに行く日は結構楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">「美肌潤美」の保湿力にはホントに感動します。実際に体験したので、ウソじゃなく、美肌効果がハンパないです！
全身脱毛の脱毛し放題コースで通ってますが、他のサロンに比べてもかなり安いと思うし、キャンセルの回数がチェックされたりすることもなくて嬉しいです。予約が取れないこともないです。前に通ってたところは全然予約が取れなかったのでやめました。銀座カラーも最初は不安だったけど、全然大丈夫で予約とりやすいと思います。
電車通勤してるので、町田モディの中にある銀座カラーは通いやすいのもいいです。サロンの中もきれいで居心地がよいので、楽しく続けられています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛しつつ肌もきれいになるっていう「美肌潤美」がいいなと思い、銀座カラーで脱毛することにしました。脱毛で保湿ケアもしてくれて、しかもそれが脱毛の費用でできちゃうなんて（追加料金なし！）、かなり魅力的だと思います。
私が通ってる町田モディ店は、駅からも近くて、買い物ついでにも行けて、とても便利です。学校の帰りに寄ることもあります。気楽に通えるのが、続けられる理由かなって思います。スタッフの人もみんな感じが良くて、安心です。
最初は不安だったので、ワキの部分脱毛から始めたけど、全身脱毛の方が断然お得なので、すぐに全身にしちゃいました。肌がきれいになっていくのを実感すると、全身脱毛せずにいられなくなります！今まで避けてた腕や足を出す服も、これからはおしゃれに着れそうですっごく楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">横浜西口店は、スタッフのみなさんがいつも笑顔で迎えてくれますし、施術中も楽しくおしゃべりができるので女子会のような雰囲気で楽しく通えます。サロン内も清潔感があり、パウダールームが充実しているので帰りの支度もバッチリできて、いつでも安心して予約できます。長く通うサロンだからこそ、居心地の良さが重要ですよね。脱毛に関する悩みも、カウンセリングから丁寧に聞いてくれましたし、今後がイメージしやすいように施術の際にアドバイスをしてくれます。まだ始めたばかりだけど、綺麗になった肌を想像してワクワクしちゃいます!!</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛サロンは初めてでいろいろ不安でしたが、最初のカウンセリングが無料だったので行ってみようと思いました。脱毛方法とか料金など基本的なことも教えてもらいましたし、ちょっと相談しづらいVIOの脱毛についても相談にのってもらえたので、不安はかなり解消されました。通い始めてからも、脱毛についての疑問は施術の時に質問していますが、聞けばすぐに答えてくれるので安心できます。信頼できるサロンが見つかって、本当によかったです。
</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">土日はプライベートの予定を優先したいので、仕事帰りに通えるところを探していました。職場近くに銀座カラーがあることを知り調べたら、平日21時まで営業だったので、さっそくカウンセリングを受けて即決しちゃいました。値段がリーズナブルなのに、お店の設備が充実していてびっくりしましたし、スタッフの方々も親切で丁寧な対応をしてくださいました。たまたま見つけたサロンが、こんなに丁寧なところだったのは本当にラッキーです!早く綺麗にならないか、楽しみに通います。
</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務職<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラーの横浜エリアの店舗に通っていますが、横浜西口店は駅の出口からすごく近くて便利です!!面倒くさがりの私でも、利便性の良さもあって雨の日でもちゃんと通えていますよ。脱毛の施術も、スタッフさんがすごく気遣ってくれるので、何も不安なくストレスなしで受けられています。いつもうとうとしている間に終わっているのですが、効果はちゃんと出ているので、安心しておまかせしちゃってます。あとサロンで出してくれるハーブティーが好きなので、職場のデスクで飲むお茶もハーブティーに変えてみましたよ。銀座カラーで教えてもらった良いことは、取り入れていきます!</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">もともと全身脱毛するほど全身が気になっていたわけではなく、全身ハーフ脱毛を契約しようと思っていましたが、ハーフに少しの金額を足したら全身できるのと、ハーフでやった後に全身にしておけばよかったと後悔する方もいるとのことで、どうせやるならお得な方がいいかなと、全身脱毛にしました。やっていくうちに、背中など、普段気にしていなかったところにけっこう産毛が生えていたことがわかって、全身にしておいてホントによかったと思いました。新宿東口店は駅から約3分のビックロの前にあり、新宿での買い物ついでに気軽に通えました。新宿の中でも大型店らしく、若いスタッフさんたちが仲良くお仕事している雰囲気が伝わってきて居心地がよかったです。あと、入ってすぐのシャンデリアが実はお気に入りでした。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">新宿三丁目駅からすぐの新宿東口店。大学の帰りに新宿を通るので、新宿東口店に通うことにしました。全身脱毛は高いのかなと思ったのですが、無料カウンセリングの時に、学割特典があると聞き、やるなら学生のうちだと思い、全身VIP脱毛コースに決めました。他のサロンで予約が取りにくいなど友人から聞いたことがあったのですが、銀座カラーだと3ヶ月に1回なので予約も取りやすく、ストレスなく通うことができて良かったです。全身脱毛をして本当に綺麗になったので、今までの自己処理の大変さを思うと大学生になってすぐに通って本当に良かったなと思います。ムダ毛は一生付き合っていくものだと思っていたので、早いうちに脱毛してしまう方が楽でいいですね。しかも美肌潤美という銀座カラーオリジナルの保湿ケアのおかげで、肌質もしっとりしたのでそれも良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">今回担当頂いた新宿東口店のスタッフさんですが、無駄のない動きで施術もスムーズでした。私はどちらかと言うと施術中に話をしたくないタイプだったので空気を読んで頂いたのか必要最低限のトークで「さすがだな」と思いました。人によって施術中に話したい人、話したくない人が居るので見極めて頂けると助かります。これまで担当してくれた方はみなさん、親切に施術してくれるので有難いです。脱毛は、恥ずかしい部分もお願いするので、淡々と進めてくれるのが一番安心します。脱毛自体が初めてなので、色々なお話やアドバイスが聞けて良かったです！どのスタッフさんもいつも丁寧に対応してくださってとても嬉しいです。良い意味で「スピーディー」な対応で、説明も施術も丁寧で『安心感』がありました。その誠実な対応が素敵でした。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">新宿は学校帰りにも習い事にも休日にも、ほんとによく行く身近な街です。特に伊勢丹やマルイなどがあるエリアが好きなんですが、銀座カラーの赤い看板がいつもなんとなく気になってました。友達とたまたま脱毛の話になった時、なんと！4人も銀座カラーに通っているとわかりました。そのうち2人は新宿東口店、他の2人は家の近くで通ってましたが、私が入るのをきっかけに、みんなで新宿東口店に行くことにしました。ベッドが10台以上あるようで、友達と同じ日に予約出来た事も何度かありました。友達とわいわい行っても、スタッフさんもとてもフレンドリーなので気を遣わなくてすんで楽です。通い始めて1年ぐらい経ちますが、ワキの自己処理はほとんどしてないですね。毛周期に合わせて次の予約が取れるので、脱毛効果を感じてる間に次の予約がくるっていう感じでいいペースです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学が近くにあるので、学校帰りに友達と一緒に通ってます。初めて行った時は、大人可愛い素敵なカフェに来たみたい！と友達と二人で興奮しちゃいました。ちょっと背伸びして大人の世界に足を踏み入れたような気分でした。ハーブティーなんかも出してくれるので、ホントに毎回大人気分です。カウンセリングも友達と一緒にできたし、予約も一緒に取れるので、友達と一緒に通いたい人にはオススメ。私が行った時には「ペア割」というキャンペーンもしていてかなりお得でした。立川はショッピングも楽しめるので、いつも、友達とランチ→銀座カラー→ショッピングのセットで通ってます。脱毛後は、お互いのつるつる、もちもち肌を自慢し合っていつもすごく盛り上がってます。一緒に通うといろいろ共有できていいですよ。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">立川駅といえば中央線が止まるし、デパートやショッピングビルがあったりととても便利なので、駅から徒歩5分の立川北口店で予約をとりました。脱毛サロンはたくさんありますが、銀座カラーに決めた理由は、全身を1回で施術できること、銀座カラーオリジナルの美肌潤美という保湿ケアがとてもいいということでした。脱毛箇所を冷やすことはどのサロンもしているようですが、美肌潤美は脱毛後にミストシャワーで肌の火照りをしっかりクールダウンしてくれつつ、肌を綺麗にしてくれるそうです。実際に美肌潤美のケアをしてもらって、その保湿力の高さに驚きました。通うたびに毛量が減っていくのはもちろんのこと、どんどん肌質が良くなっていくのも感じたのでお得だなと感じました。銀座カラーにして本当に良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">お店の雰囲気や設備、スタッフの接客態度も丁寧なので、とても来やすい感じがしました。
立川北口店の担当の方が、とても笑顔の素敵な方で、丁寧な説明等で安心して施術を受けることができました。人それぞれだと思うのですが、スタッフの方から効果が表れてきてることを伝えてもらったことは嬉しかったです。最後のお茶も些細な楽しみです！ 初めて利用させていただいて大変満足しました。今回担当してくださった方は接客も仕事ぶりも本当に丁寧で、とても好感が持てました。必要以上に気を遣わなくてよい空間が心地よかったです。正直、契約回数や金額に不安があったのですが今日サロンに行ってみて決めて良かったと思いました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">何年も前に他の脱毛サロンでワキの脱毛は済んでいたのですが、最近脱毛が安くなったなと思っていろいろ調べていました。銀座カラーは全身脱毛が安いと評判だったので調べてみると、立川は2店舗あってどちらも駅から5分ぐらい。どちらにしようかとサイトで店内の写真を見たら、立川北口店のロビーの椅子が我が家の椅子とそっくり！インテリアの好みが合うのは居心地がいいかなと思って、立川北口店にカウンセリングに行きました。いろいろ質問して、他の部分脱毛と比べてみても全身の方が断然オトクだったので、即決。美肌潤美という保湿マシンを使ったアフターケアがオススメのようで、そのおかげか、仕上がりがいつもぷるぷる、もちもち肌になります。以前行っていたワキの脱毛の時はタオルで冷やすだけだったので、アフターケアは大違いだと思いました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">キャンペーンを見て家から近い立川店に行きました。安いので接客にはあまり期待していなかったのですが、スタッフさんの対応がとても丁寧で感じがよかったので契約しました。休日には3歳の子どもを連れて家族で昭和記念公園に行くことが多いので、公園に行った後、子どもがお昼寝している時間にサロンに行くように予定を組んでいます。公園に行った日は疲れてお昼寝も3時間コースなので、全身脱毛の1回約3時間の施術とちょうどいい感じでパパとのお留守番も大丈夫そうです。全身脱毛は冬から始めたのでだいぶ効果も出てきていて、今年の夏は昭和記念公園のプールにビキニで行けそうで楽しみです。全身脱毛なら銀座カラー！と、友達にもオススメします。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">立川駅から徒歩2分にある立川店は、仕事帰りに寄るのにとても立地が良かったので、無料カウンセリングに行きました。社会人になり、今後結婚することも考えると、その前に綺麗にしておきたいなという気持ちが強くなり脱毛することに決めました。どうせやるなら全身脱毛をして一生ムダ毛に悩まされないようにしたいなと思っていたのですが、やはり金額的なことや、最後まで通えるかという不安がありカウンセリングのときに相談させていただきました。全身VIP脱毛だと金額的には高いと思いましたが、分割で支払いできること、分割回数によっては手数料もかからないこともあり、安心して契約をすることができました。それに平日は夜9時まで開いているので、仕事帰りに寄れるという手軽さもとても良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">立川店は以前通っていたエステサロンと違い、丁寧で親切で、いつもスタッフの方の対応、笑顔がよく、気持ち良く通わせて頂いております。ワキと腕の照射の際、両手をあげる姿勢が少し辛かったのですが、すぐに声をかけてくれるなど、一つ一つとても丁寧に対応して頂くので毎回行くのが楽しみです！新しい情報を教えて頂き＆提案していただきありがたかったです！今後とも立川店に通いたいです。これからも脱毛に通いたい友達などいたら紹介したいです。サロンを利用するのは銀座カラーさんがはじめてなので不安があったのですが、丁寧に説明してくださるのでずっと利用していきたいと思いました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">年齢とともに鼻下の産毛がヒゲのように濃くなって気になってきました。友達にも「ちょっと濃くない？」と言われ銀座カラーさんを紹介してもらいました。保育士という仕事上、子供たちと顔を近づけてお話することも多いので、子供に「おひげだ～」と言われる前に脱毛をしてしまおう！と思い通うことに決めました。立川店は職場からも近いのでとても通いやすくて、面倒くさがりの私でもさぼることなく通えています。スタッフの方は対応が丁寧でサービスが行き届いているなーと感じました。はじめは顔に機械をあてて脱毛するなんて…と怖かったのですが、一度すると怖さや痛さの不安も全くなくなりました！段々とですが鼻下の毛も薄くなってきて、お化粧するときも気分が良いです♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">金沢で予約の取りやすい脱毛サロンと評判の銀座カラー。近くに駐車場はありますが、有料なので、電車で行くことをおすすめします。土日に通いたい方は前もっての予約が必要そうですが、平日に通えるのであれば、1週間前でも予約が取れます。子どもの予定などで急な予定変更がある私にはとてもありがたいサロンでした。私のとても好きな空間でした。脱毛の痛みもなく、施術は気持ちよく、サロンに通う日はエステに通っている感覚で、自分へのご褒美時間でした。施術がすぐに終わるので、帰りにポルテ金沢でゆっくりお茶して帰れるのもリラックスできる時間でした。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛サロンに入るというのは周りの目が気になるなと思っていたのですが、ポルテの近くなので、ショッピングのついでに入れるので気兼ねなく通えました。無料カウンセリングでお話を伺ったところ、一部が綺麗になると他の箇所も綺麗にしたくなる方が多いようで、金額的にも全身脱毛し放題コースがとてもお得だと聞きました。確かに全身脱毛だと、脱毛の境目も気にならなくなるのでいいなと思いました。分割でのお支払いができること、私が支払いたい回数なら手数料もかからないということもあり、全身脱毛し放題コースに決めました。脱毛をすると乾燥するようですが、銀座カラーオリジナル保湿ケアの美肌潤美のケアをしてもらったので乾燥も気になりませんでした。6回、約1年半通ってとても綺麗になったので大満足です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">いつも施術してくださる金沢駅前東店のスタッフの方はとても接客態度が良く、施術も丁寧で、脱毛に関して不安だったり分からないことなど、いろいろと相談しやすく、こちらも自然と笑顔になるくらい、素晴らしいサロンだと思います。全身脱毛し放題コースの脱毛をしました。今まで自己処理に時間がかかっていたのですが、サロンで脱毛して自由な時間も増え、かなり気持ちが楽になりました。長年毛深いということになやんでいたのですが、脱毛することでその悩みやコンプレックスがなくなって、自分に自信が持てるようになりました。全身脱毛し放題コースを受けて、意識してこまめに保湿をしっかりするようになりました。背中など、見えない部分で手が届かず、自己処理もなかなかできないところだからこそ、サロンで脱毛して本当に良かったなと思いました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学生になったらワキの脱毛をしていいと母と約束していたので、ワキ脱毛に通いました。母から勧められたのは銀座カラー。全身脱毛で有名なお店らしいのですが、母と「最初はワキだけ」と話していたのでそうしました。カウンセリングでは、お店の方からやはり全身脱毛が一番人気との話がありましたが、母との約束を話すと、「10代だし、初めての脱毛はワキからですよね。」と優しく聞いてくれたので安心しました。母からは少し痛みはあるよと聞いていましたが、私は光脱毛は痛みが全然なかったです。目を隠してやるので、何をしているのか見えませんが、いつ光を当てたのか全然わからないうちにいつも終わっていました。ワキがつるつるになったので、自分のバイト代を貯めて、今度は別の部分も脱毛したいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">接客業でお客様にいつも顔を見られる仕事をしているので顔の脱毛をしました。銀座カラー梅田新道店のあるビルには脱毛サロンが3つも入っていたので、全部カウンセリングに行ってみました。カウンセリングだけでは違いがはっきりとはしなかったので、スタッフさんのお肌をじーっと見比べ、一番つるっとしていてお化粧のりもよかった銀座カラーにしました。いくら言葉で説明しても実際のお肌をみればわかりますからね。独自のミストで美肌効果や肌荒れ防止効果があるそうで、あのお肌のつるつる感はこのミストのおかげなんだと納得できました！毛がなくなるだけでなくお肌もキレイになってお得ですよね♪私も、最初にお会いしたスタッフさんのようなつるっとした素肌になりましたよ。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">梅田新道店は赤、白、黒、を使ったモダンで落ち着くサロンでした。スタッフの方が和気あいあいとしていて、よく話しかけてくださるので、緊張もすぐに解けてリラックスすることができました。私は顔の脱毛がしたくてフリーチョイス脱毛のコースで、顔、鼻下の2カ所をお願いしました。クレンジングをしてくださる時にフェイシャルマッサージをしてもらっているようで、とてもリラックスできて気持ち良かったです。そして施術前の美肌潤美ローション、施術後の美肌潤美ミストシャワーのどちらも気持ちよくて、しかも保湿効果がとても高いとのことで、毎回とても楽しみでした。これは銀座カラーオリジナルだそうです。これのおかげもあって、6回終えたあとは、ムダ毛がなくなって肌の色が明るくなっただけではなく、お肌がもちもちになった感じがしてとても良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">梅田新道店は、どのスタッフも笑顔でとても好印象です。今回担当してくださった方は特に親切に、そして丁寧に応対してくださるので、安心してお任せできます。脱毛をして痛かった時にジェルを足してくれたおかげで、痛みがかなり軽減されました。1人のお客さんに対して思いやりがあるので、他の方達にもきっとこの満足度が広がると思います。心のこもった対応が非常に嬉しかったので、これからも利用したいです。銀座カラーさんは値段が安く、大手なので安心して通える上に、接客は他のサロンよりも格段に良く、スタッフ皆さんの接客サービスの質が高くて気に入っています。サロンの清潔さも利便性も予約の取りやすさも言うことなしです。全身脱毛ということで最初は迷っていたのですが、いま思えばもっと早くしておけばよかったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラー梅田新道店は、北新地駅から地下道をとにかくまっすぐ5分ぐらい進み、地上に出たらすぐ看板が見えるところにあります。初めて行った時もわかりやすく、すぐに到着できました。店内が白くて明るく、清掃も行き届いている感じでとてもキレイなので、いつも気持ちよく通えています。脱毛効果は3回目で文句なしの抜け具合です。美肌効果もすごくて、大学の友達に腕がすべすべになったねとほめられたので、今では友達も紹介して通っています。友達はまだ1回目ですが、私が見てもわかるぐらい効果が出てます。紹介キャンペーンで特典もあったので、私も友達も大喜びでした。あと、月々払いができるのが学生には嬉しいです。今年の夏は、友達と海に行くことが増えそうです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラーの横浜店に通ってます。横浜に3店舗あるうちの一番歴史が長いお店らしく、スタッフの技術が確かだという評判を聞いたので決めました。若干地味なビルの中にあるので初めて行った時は心配になりましたが、中に入ってみると白くて明るく綺麗なお店だったので安心しました。カウンセリングの時に、全身脱毛し放題のコースは満足いくまで何十回でも通えるので一番お得という話を聞いたので、脱毛し放題で通ってます。全身脱毛は1回3時間ぐらいで終わりますが、長くて寒いと感じる場合は何回かに分けてできるそうで、寒い時期になったらそうしようかなとも思っています。今のところいっぺんに終わってしまう方がラクなので3時間ぐらいかけてやってもらってます。3回終わったところですが、順調に毛がうすくなってるのでこれからが楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">西口からも近くてお店も明るい印象です。スタッフの方も皆さん優しくて、サロンに行くといつも笑顔で迎えてくださり気さくに話しかけてくださるのでアットホームで居心地抜群でした。脱毛というと痛みが一番の心配だったのですが、最初にパッチテストをしてくださるので、どれ位の痛みかもわかるし、安心して施術をお願いすることができました。私は子どもの頃から毛深いのが悩みで、いつか全身脱毛をしたいと思っていました。背中など自分が見えない部分は自己処理できないのでプロにお任せしたいと思い銀座カラーへ行きました。無料カウンセリングでは親身に相談に乗ってくださったので、脱毛し放題の全身VIP脱毛コースに決めました。これなら何年経っても気になるたびに通えるしずっと安心だなと思いました。ムダ毛がとても気になる私は、脱毛し放題がある銀座カラーにして良かったと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛の施術中ちょっと冷たかったり、箇所によっては痛みもあるところもありますが、担当の方が楽しい話で和ませてくれてうれしかったです。横浜店はあまり営業されない感じの所がすごくいいです。私は全身の脱毛コースにしたのですが、顔と鼻下を脱毛した時にクレンジングが気持ちよくて、とてもリラックスできました。終わったあとも暖かい飲み物を出してくれるし、いつも丁寧な対応で気持ちよくサロンに通えています。ずっと毛深いことが悩みでしたが、今では処理もぜんぜんしなくなったので本当に脱毛して良かったです。
実はエステティシャンになりたいので、来年から銀座カラーの社員としてお世話になります。私も接客していただいた先輩方のように笑顔を絶やさず、お客様の目線に立って接客できるエステティシャンを目指したいと思っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラー横浜店でワキの脱毛をしています。高校時代までは制服は半袖だし、ワキはカミソリで剃るぐらいで十分だと思っていました。大学生になり、電車通学でつり革をつかむようになると、つい、隣のOLさんのワキに目が行くようになりました。OLさんは脱毛してツルツルの方が多くて、私はつり革をつかむのが恥ずかしくなってしまいました。友達に相談したら、銀座カラーに通っているとの事だったので、紹介してもらって一緒に通っています。ワキの脱毛は10分ぐらいであっという間に終わるし、痛くもなくてびっくり。紹介してもらってホントによかったです。横浜店のスタッフさんとはネイルの話やマツエクの話などをして、お姉さんから美容について教えてもらってるみたいで、それも通う楽しみの一つです。どのスタッフさんも丁寧で、素敵なお姉さんばかりです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody"><!--名鉄岐阜駅から徒歩1分という、とても通いやすいサロンでした。-->お店はスタッフの方とお客さんの距離が近いアットホームな雰囲気のサロンでした。私はおしゃべり好きなので、子どもの話や美容の話など、いろいろしゃべっている間にいつも終わってるという感じでした。そんな私のおしゃべりにもいつも丁寧に付き合ってくださる方ばかりで、とても居心地のよいサロンでした。施術後には美肌潤美という銀座カラー独自のミストがセットになっていて、保湿と鎮静効果が高く、いつも潤った肌で帰ることができました。年齢を重ねて、肌の乾燥も気になっていたのですが、脱毛後はぷるぷるです♪残り2回なので、おしゃべりタイムも、気持ちのいいミストタイムも終わってしまうと思うとさみしいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody"><!--私が通った名鉄岐阜駅前店は、岐阜駅から徒歩7分で、とてもわかりやすい場所にありました。-->店内はとても明るく清潔感溢れる心地よいサロンでした。VIOと顔のムダ毛が気になっていて無料カウンセリングを受けたのですが、全身パーフェクト脱毛コースならVIO全部と顔も脱毛できることがわかったので、このコースに決めました。VIOは最初恥ずかしかったのですが、エステティシャンの方はプロで慣れているので、そんなこと全然気にしなくていいんだと思うと、気が楽になり安心して施術をお願いすることができました。VIOをして良かったところは、やはり毎月の生理時の匂いやムレがなくなったこと、そしてどんな水着も安心して着られることです。今までムダ毛がはみ出さないかを基準に選んでいたのが、今ではどんな水着も着られるので海やプールに行くのがとても楽しみになりました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody"><!--名鉄岐阜駅前店はお仕事帰りに寄れるので、とても便利で利用しています。-->担当者の方が、脱毛するときの痛みやお手入れ後のお肌の状態についてなど、不安に感じていることを丁寧に説明してくれたので、安心して施術を受けることができています。全身脱毛し放題コースにしていますが、一回で全身の施術をしてくれるので通うのもとても楽です。今までの中で一番効果が嬉しいのはV上部とサイド。Vラインが清潔になりショーツから毛がはみ出なくなったし、水着も毛をきにせず着れるくらいになっているので大満足！美肌ミストが全部のコースに含まれているのがとても良かったです。特に背中は普段自分でお手入れしにくくて困っていた部分だったので、通うたびにお肌がきれいになることが楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">彼氏ができたので、夏に海やプールに行きたい！と思って、冬の間に脱毛をしようと決めました。私はVラインの毛がとても広範囲に生えていて、下着からもはみ出てしまうのが悩みでした。ショーツを買うときも、水着を買うときも、毛がはみ出ないデザインのものしか買えず、気に入ったものが買えない時もあって残念な感じでした。ショーツからはみ出てしまう毛は抜いて対応していましたが、抜いた後の毛穴がプツプツふくれたり、毛が生えてくる時にかゆくなったり、Vラインの悩みは書き出したらきりがないです。カウンセリングで相談した結果、Vラインだけでなく、VIOというコースを月額払いで通っています。<!--2回目の施術から効果がよくわかり、毎回通うのが楽しみになっています。名鉄岐阜駅前店はお店がいつもキレイなので、行くとテンションが上がるサロンです。名鉄岐阜駅からも近いので、仕事帰りに寄っています。--></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">初めて行った時に、2回目以降はネットで簡単予約できることと予約が取りにくい時は、店舗移動も可能という説明をしてもらったので私は名駅と栄の両方に通っています。白と赤のインテリアが明るい感じの名駅店と、ちょっと大人っぽい雰囲気の栄店、私はどちらもお気に入りなので予定に合わせて通っています。脱毛は初めてだったので痛みが心配でしたが、私がやったワキ脱毛は「え？もう終わったの？」と思うほど痛みもなく、終わるのも早くてびっくりしました。最新の脱毛の機械は痛くないんですね！すぐに終わるので、友達とショッピングの途中で、友達がお茶してる間にちょっと行ってくるっていうこともできました。名古屋はどっちのお店も場所がいいので通いやすくて便利です！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">名古屋駅前店は名古屋駅から徒歩5分という立地の良さで通いやすかったです。清潔感あふれる温かみのある雰囲気のサロンだったので居心地もとても良かったです。無料カウンセリングの際に、女性は3つの首（えり足、手首、足首）を綺麗にしていると若く見えると聞いたので、そこも含めてぜひ脱毛したいと思いました。確かに一年を通して、手首から指先は見えますし、えり足が綺麗だと髪をアップにするのも自信をもって出来るので、本当だなと思いました。元々ヒジ下とひざ下の脱毛を希望していたので、全身VIPハーフ脱毛のコースにして、手の甲・指、えり足も含めてお願いしました。施術の前後に美肌潤美という銀座カラーオリジナルの保湿ケアをしてもらえるのですが、それがとても気持ち良く、帰宅後もしっとり感が続いていてとても良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">私も接客の仕事をしていますが名古屋駅前店はどのスタッフさんも柔らかい笑顔と気遣いがあってとても良いと思います。いつもたのしいお話ができて、気分も二倍良くなります。はじめてお店に行った時はすごく不安でしたが、スタッフさんが話しかけてくれたり、痛くないかなど、こまかく聞いてくださったので安心して脱毛を受けられました！また、色々アドバイスをくれて本当にためになりました！！これからもずっとお願いしたいです。二回目の施術を受けに予約の時間の少し前に来たら、中で待てるようになっていて嬉しかったです。ジェルが冷たいけどすぐに温めてくれたり、毎回「大丈夫ですか？」と声をかけてくれてくれるスタッフの方の気遣いにとても思いやりをを感じました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">今まで自分で処理をしていましたが、肌もブツブツになり毛が埋まってしまったりして、夏場も長袖をきていました。銀座カラーに通ってどんどん肌がきれいになり、立ち振る舞いも美しくなっていく友人の紹介で、私も無料カウンセリングに行きました。値段も手が届かないのかなと思い込んでいましたが、意外にも低価格で、月にかかる料金はスマホの使用料金とたいして変わりはありませんでした！
名古屋駅から徒歩5分の場所で清潔感があり、スタッフの皆さんもいつもアットホームな雰囲気で出迎えてくれるので、とてもリラックスして施術をうけることができます。また、スタッフが私の肌や毛の状態を見て、私にあった通い方をその都度アドバイスしてくれるので、とても助かります。いつかは全身つるつるになり、全身美人を目指して頑張ります。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">腕と足の脱毛をしたくて行ったのですが、カウンセリングの時にお店の方が自分の体験談を話してくれました。はじめはワキ、それから、腕、足と脱毛していくうちに他の部分や境目が気になるようになってきて、結局やる箇所が増えていったと聞いて、絶対私もそうなるだろうなーと想像がつき、おもいきって全身脱毛することにしました。料金も全身脱毛のほうが断然オトクで、後から増やしたくなったら損だと思いました。最初にお店の方の体験談を聞けて、ほんとによかったです。親切なお姉さんに感謝です。お店は千葉駅から5分くらいのビルの中にあって、近くにカフェもあるので、早めに行ってお茶してから通うようにしていました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">千葉店は千葉駅から近くとても通いやすかったです。店内はとても広く天井も高いので、毎回リラックスできる非日常空間でした。最初に無料カウンセリングに行き脱毛したい箇所を数カ所伝えたところ、「それなら全身VIP脱毛の方がかなりお得ですよ」と教えてくださり、価格や痛みなどの不安についても親身になって丁寧に答えてくださったのでとても信頼出来るサロンだなと思いました。しかも全身の場合、2～4回に分けるサロンが多いなか、銀座カラーでは1回で全身の脱毛ができてしまうというのも通う回数が少なくて楽なので魅力的でした。金額も分割での支払いができるので毎月の金額はとても安いところが良かったです。痛みは輪ゴムでパチンと弾いたくらいと聞いていて、実際にそれくらいの痛みだったのであまり気になりませんでした。全身脱毛をして自己処理をしなくてよくなって本当に良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">千葉店に通うのは今回で2回目ですが、2回とも親切で丁寧な接客で非常に満足です☆今日担当して頂いた方は施術がすごく丁寧だったし、 接客もすごく良いので、これからずっと担当してもらいたいくらいです。今まで関わったスタッフみなさん素敵な笑顔で優しくて、利用していてとても気持ちが良いし、次の施術も楽しみになります。店舗に行って嫌な思いは一度もしたことがなく、いつも丁寧な施術には満足しています。初めて行く店舗で迷ってしまったのですが、皆様にとても丁寧に対応して頂き助かりました。帰り道もきちんと教えくれるし、とても安心できました。強引な勧誘もなく、とても居心地が良かったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">千葉店は駅から5分ほど歩いた所にあるので、予約まで時間がある時は近くでウィンドウショッピングやカフェを楽しんでいます。
以前にワキを他店で脱毛していたのですが、他店では施術の熱をとるために冷たいタオルで冷やし、冬場にはとても辛かったです。でも銀座カラーでは、美肌潤美という「ヒアルロン酸とコラーゲン」が入ったミストをかけ、ローションを塗ります。そのために、肌が水分量を含んでぷるぷるとし、フワフワとした柔らかい肌触りに変化していました。施術から数日経った今も、塗布した場所は肌触りが良いので、これからの施術も楽しみになりました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">新宿西口店は小田急ハルクのちょうど裏手にあり、西口のオフィスビル街で働いている私は、会社帰りにとても通いやすかったです。脱毛サロンに行くのに人目が気になる私には、路地を入ったビルにあるところもよかったです。就職して初めてもらうボーナスで全身脱毛をしたいと、学生の頃から思っていたので、全身脱毛といえば銀座カラーというイメージで足を運びました。施術をしながらスタッフさんとお話するのが毎回楽しみで、仕事の悩みなども聞いてくれたのがよかったです。銀座カラーの全身脱毛は、1回の施術で一気に脱毛できるので、1回の時間はかかりますが、何度も通わずにすんでいいと思います。施術して1,2週間後に毛がたくさん抜け落ちる時期があり、それもいつも楽しみでした。今では全身つるつるです。ありがとうございました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">私が通った新宿西口店は新宿駅西口から近く、清潔感の溢れるとても綺麗なサロンで、スタッフの皆さんも優しくてアットホームな脱毛サロンでした。水着を綺麗に着るために、腕、足、背中、Vラインが気になっていたので、それなら全身VIP脱毛の方がお得ですよと教えてもらい全身VIP脱毛にすることにしました。全身脱毛のお値段がお安いということもあり、銀座カラーに来るお客さんは全身脱毛に興味のある方が多いみたいでした。全身の毛が、サロンに行くたびにどんどん薄くなって、自己処理しなくても良くなっていくのは自分でもとても嬉しく、今まで毎週剃っていた大変さを改めて感じました。Vラインの痛みが気になっていたのですが、他の箇所に比べると多少気になる程度で、我慢出来る痛みだったので安心しました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">とても落ち着いてる雰囲気の店舗で良かったです。これからは新宿西口店でお世話になろうと思います。担当の方がとても丁寧で印象が良く、施術もテキパキと素早く、いい気持ちで帰れます。肌が弱く自己処理後の肌トラブルに不安を抱えたまま今後の施術の進み具合に不安を感じていましたが、その日担当していただいた方から丁寧に肌の事や施術の進め方について、説明をしていただきました。とても話しやすく親切な方だったので、いままで疑問に思っていたことも聞く事ができたので今後も安心して施術を進める事が出来ます。何度か担当してもらった方の接客や説明やアドバイスはいつも素晴らしいし、安心感があります。仕事のお休みが少なく、サロンに通う日程調整に苦労していましたが、予約のときに親切に対応してもらったのでとても助かりました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">職場から近く新宿駅の西口から徒歩5分という好立地にあり、平日は21時までやっているので、仕事帰りに施術をお願いすることもあります。場所柄OLさんが多く清潔感あふれるあたたかい雰囲気です。
また、「全身で恋しよう」の広告を見てはっとしました。今片思い中ですが、いつか彼が振り向いてくれた時に心も体も見えるところだけでなく、自信を持てる自分になりたいなと思います。脱毛後の美肌潤美というミストにより脱毛前の肌よりも水分量があがり、プルプルになるのが実感できます。肌に自信がでてきたらいつもより明るい色の洋服を着てみたり、いつでも恋ができるよう自分磨きを頑張ります。</dd>
          </dl>

 <!--<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">仕事で新宿を通るので新宿の脱毛サロンを探していました。ホームページで見て銀座カラー新宿南口店のお店がとても綺麗そうだったので、まずはカウンセリングに行きました。白い壁とオレンジ色のソファがとても素敵で、ハーブティーまで出してくれて、オシャレなカフェに来たような感じでした。スタッフさんの対応もとても丁寧で感じがよかったので、このサロンに決めました。通ったのは「足全セット」です。保育園に通う娘に、お風呂で「ママの足チクチクする」と言われたのがショックで、足脱毛を決めました。膝下だけにするか迷いましたが、膝周りの脱毛をすると、色素沈着した膝もキレイになるとのことだったので、足全部をお願いしました。本当に通えば通うほど膝周りも白くてキレイになり、もちろん膝下も娘から「ママの足つるつるしてきもちいい」と言われるようになりました。</dd>
          </dl>-->

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">新宿店は、JR新宿駅から徒歩3分と駅からとても近かったので便利で良かったです。清潔感溢れるサロンで居心地も抜群！スタッフも若い方が多いので、会話も盛り上がって、あっという間に終わってしまう感じでした。会話の内容も美容系のことが多く、知識が豊富な方が多いので、今流行りの美容方法やコスメの情報を教えてもらえてとても勉強になりました。そして脱毛には保湿が大事ということで、「美肌潤美」という銀座カラーオリジナルの保湿ケアを取り入れていて、施術の前にはローションを、施術後にはミストシャワーをしてくださるので、しっとり肌になれたのがとても良かったです。回数を重ねるごとに肌質も良くなった気がするのでとても嬉しいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">初めての全身脱毛だったのでとても緊張していましたが、担当者の方がメニューだけでなく、脱毛後の乾燥しやすい箇所のケア方法なども教えてくださったので凄く安心しました。新宿店は、いつも気持ち良く利用させてもらってます。とても丁寧に接してくれるので、施術中も寝てしまうほどリラックスできます。全身脱毛で通っていますが、担当の方がご自身の脱毛体験のお話をしてくださりとても参考になりました。顔の脱毛は他のお店だとメニューになかったりしますが、おでこの部分も脱毛できて良かったです。ムダ毛がなくなって、お肌がワントーン明るくなり、化粧ノリも良くなったと実感しています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">JR新宿駅の東口から徒歩3分の通いやすいサロンです。店内は清潔感の中にキラキラと輝く大人可愛い空間が広がっています。脱毛後の「美肌潤美」というお肌に良い様々な美容成分がたっぷり入ったミストを噴射することにより、肌が保湿され、水分量の高いプルプルのお肌になるのも驚きです。脱毛効果だけでなく、肌のトーンも上がったり、肌のキメが細くなったり美肌効果もあるので、このまま継続し頑張って全身すべすべにしていきたいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">最近脱毛サロンが増えているので、正直どこがいいのかわからなくなっていました。おもいきって友達に相談してみたら、一緒に通おうということになり、紹介されたのが銀座カラー神戸元町店さんでした。白くてオシャレな雰囲気のお店で、広すぎずアットホームな感じが伝わって来ました。私は脱毛サロンに行くのが恥ずかしいと思っていたのですが、このお店の雰囲気はとても居心地が良かったので、友達が誘ってくれたのも納得です。初めての脱毛だったので、まずはワキだけにしたのですが、どんどん毛が薄くなっていったのに感動！しかも毛穴や黒ずみもなくなっていき、自分じゃないようなキレイなワキを手に入れました。スタッフさんの感じもよくて、通えば通うほど好きなサロンになっていったので、別の箇所も契約しちゃいました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">神戸元町店は白と木目を基調としたサロンで、行くたびに気持ちが落ち着くサロンです。神戸大丸のすぐ裏にあり、私は毎週というくらい神戸大丸で買い物をしているので、そのついでに寄れるのがとても便利で通いやすかったです。冬の時期にはルミナリエもあり、施術の帰りにルミナリエを見て帰ることもありました。脱毛サロンって少し敷居が高いのかなと思っていたのですが、無料カウンセリングに行った際にもスタッフの方が皆さん笑顔で優しかったですし、アットホームな雰囲気のサロンだったので安心することができました。コースで迷っている時にも、優しいお姉さんという感じで親身になって相談に乗ってもらえたのもとても心強かったです。脱毛が初めてだったので、まずはフリーチョイス脱毛で通って、終わって他にも気になる箇所があれば、増やしていこうかなと思っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛に関しての悩みや、シミは薄くなるのか、肌荒れは良くなるのか・・・といった質問にも丁寧に答えてくれたので安心して通うことができました。神戸元町店の店内のゆったりした雰囲気はとても居心地が良く、スタッフの方もとても話しやすかったです。全身VIP脱毛をしたときの美肌ミストがまるでエステサロンに通ったように気持ちよく、脱毛の回数が進むにつれてお肌の調子が良くなったのもとても嬉しかったです。脱毛に関しての悩みやわからないことなども丁寧に聞いてくれて、おすすめのコースを提案してくれました。腕と足だけを脱毛しようと思っていましたが、いろいろなところが気になっていたので思い切って全身脱毛にして良かったです。今まで気になっていた部分に加えて全身脱毛で気にしていなかった部分も綺麗になって嬉しいので、ムダ毛に悩むお友達にも勧めたいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">大丸の隣にある元町店。元町駅からはもちろん三宮駅からも徒歩で通え、大人の雰囲気漂う落ち着いた脱毛サロンです。
1カ所だけなら安い脱毛のサロンはありますが、私は色々な箇所の脱毛をしてみたいと思ったので銀座カラーを選びました。全身VIPハーフ脱毛というコースで全部で8カ所の部位を選ぶことができます。痛みの感じ方は個人差があり、箇所によってもかなり違うと思いますが、腕と足は全く痛くありませんでした。皮膚の薄いデリケートな部分は少しゴムではじかれたような痛さを感じましたが、我慢できないほど痛い！というものでありませんでした。また、脱毛後に肌を鎮静させるために行われる美肌潤美は、ヒアルロン酸・コラーゲン・マイナスイオンなどの美容成分が入ったミストスプレーのようなもので肌に潤いを与えてくれて、脱毛した肌のほうがプリプリでこれからのお肌が楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">兵庫県内に銀座カラーは2店舗あるようでしたが、ベッド数が多く広いようだったので、こちらを選びました。以前、他の脱毛サロンに通ったことがあるのですが、お店の綺麗さは銀座カラーさんのほうが格段によいです。また、私はアラサーですが、他のお客様やスタッフさんも年齢層が低すぎず、なんだかとても安心して通えました。今回は腕の脱毛をしたのですが、カウンセリング時にスタッフさんがご自分の腕を見せてくださり、「続けているとこれぐらいキレイになりますよ」とおっしゃられて、ゴールが見えた感じでとてもわかりやすかったです。6回のコースで、腕はつるつるすべすべになりました。施術後のミストも肌をキレイにする効果があるそうで、こんなにキレイになるならまた別の箇所もお願いしようかと思っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">三宮でバイトをしていたので三宮で脱毛サロンを探していました。無料カウンセリングに行ってお話を聞いたら、学生には脱毛学割など特典が多かったので銀座カラーに決めちゃいました。三宮駅から徒歩5分というのも魅力的！普段よく見えるヒジ下とひざ下を脱毛したかったのでフリーチョイス脱毛でお願いしたんですけど、両方合わせても1回30～40分と施術時間も短くてあっという間に終わるので、バイト前やバイト帰りに寄ることができて良かったです♪予約の取りやすさとスタッフの技術でランキング1位をとったことがあると聞いたんですが、確かにその通り！予約は取りやすかったし、どのスタッフさんに担当してもらっても毎回納得のいく施術でした～。予約がとれなかったら、自分がやりたいタイミングで施術してもらえなくて困るし、スムーズに6回通えたので本当に良かったです｡</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">三宮店は清潔感があってとても良い印象です。いつも丁寧な対応で気持ちの良い接客をしてもらえるので、これからも通うのが楽しみです。質問したことにも時間をかけて丁寧に対応してくれたので安心して通うことができました。しっかり脱毛したいと思っていた部分のムダ毛がなくなるまでサポートして頂いて嬉しかったです。知人に紹介してもらいお店へ行きましたが、とても丁寧に説明していただけるので安心できました。デリケートゾーンを照射するとき、痛い部分を照射前に教えてくれたり、少しずつ照射してもらったのでいつもより痛みが少なかったり、ちょっとした細かい気遣いをしてもらえたことが嬉しかったです。長時間の作業も笑顔の対応でとても感じが良かったです。スタッフみなさんがとても親切で笑顔が素敵でした。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">バイトと大学と家と活動範囲が広いので、交通の便がいい三宮駅周辺で脱毛できるところを探していました。三宮店は駅から近くて、とてもわかりやすい場所にあって良かったです。母が他サロンの脱毛に通っているんですが、他サロンにはキャンペーンで一部の箇所だけ安いコースがあって、他の箇所を契約しようとした時に結局高くなってしまったと話していて、「やるなら最初から全身脱毛でやりなさい」と言ってくれたので、全身脱毛がお得な銀座カラーに決めました！明るくポップな空間でカジュアルな雰囲気だったので、学生の私でも通いやすくて良かったです(*^^*)スタッフのお姉さんも気さくでいろいろと相談にのってくれて、施術だけじゃなくて会話も楽しめちゃいました。また、ベット数も多くて予約がとりやすいので、希望通りに予約が取れているのも嬉しいです♪このあいだ1回目の施術に行ったんですが、腕や足などとても効果を感じられたので、他の箇所もこれからが楽しみです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラーで脱毛しようと決めてから、会社の近くのお店にしようか休日によく行く銀座にしようか、HPで見比べてみました。銀座プレミア店は、モダンな感じのボルドーのソファが印象的で、他のお店とはインテリアの雰囲気が少し違っていました。落ち着いた感じの銀座らしいインテリアがとても好きで、こちらに通うことに決めました。VIOをお願いしたのですが、下着からはみ出る毛も気にならなくなりましたし、iラインがスッキリしたことで生理の時のムレもなくなりました。デリケートな部分だけに多少の痛みはありましたが、これだけ楽で綺麗になるんだからやってよかったと思っています。スタッフさんも銀座だからか上品な方が多く、脱毛や美容の話以外の無駄話がないのも好感がもてました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座駅から徒歩2分という好立地の銀座プレミア店。脱毛サロンは若い子が通うイメージがあったのですが、銀座という場所柄なのか、店内はとてもラグジュアリーでアラサーの私でも安心して通うことができました。子どもが小学校に通うようになり自分の時間ができるようになったので、少しは自分に手をかけたいなと思って脱毛をしようと思いました。妊娠をした時にお腹周りの毛が濃くなり、水着になるのも億劫になっていたので、そんなことも無料カウンセリングで相談させていただきました。主人が転勤のある職業なので、6回終わるまでに転勤になったらどうしようという不安があったのですが、引越しをしてもその近くの銀座カラーで施術を続けることができると教えていただいたので、そこも銀座カラーに決めたポイントでした。ママ友にも教えてあげたいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座プレミア店は、どのスタッフさんに担当して頂いても皆さん接客や施術が丁寧なので、安心して脱毛を受けられます。口コミで読んだ通り、銀座カラーのスタッフさんは脱毛の無理な勧誘が全然ないのですが、他の箇所を脱毛しようか悩んで相談した時にはきちんと説明してくださったり、今のお肌の状態を見てアドバイスをしてくださるので、安心して相談することが出来ます。ジェルを付ける際に冷たさを紛らわせるために話しかけてくださったり、少し赤みがでた場所を念入りに冷やしてくださったりと、担当者さんの対応が素晴らしかったです。スタッフさんの対応もサロンの雰囲気もとても良くて、脱毛の効果や保湿ケアで肌質も良くなってきているので、最後まできちんと通いたいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座に2店舗ありますが、私は最近できた銀座プレミア店を選びました。ワインレッドのソファーや椅子に囲まれて、他のエステ店と比べても高級感があり、働いてる従業員の方も来ている方も綺麗な方が多く、満足度の高い大人の空間でした。地下鉄銀座駅より徒歩2分と、広い銀座を歩き回る心配もなく、場所もとてもわかりやすかったです。先日1回目の施術で腕・足を受けてきましたが、施術後、毛がするっと抜けていくのにびっくりしました。痛みもほとんど感じず、徹底した肌ケアの効果でお肌もぷるぷるです。毛のサイクルは3ヶ月毎に施術がするのが良いサイクルのようで、帰る際に次の予約をいれてきました。 さすが、楽天リサーチ第1位のお店ですね。これから通うのが楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">就職して半袖の制服を着ることになり、腕の毛を1年中剃らなくてはいけなくなったため、腕全コースの脱毛をしました。どうせやるなら割安の全身がいいかなと迷いお店の方にも勧められましたが、他の場所はそれほど気になっていなかったのでとりあえず腕だけにしました。腕だけと決めた後は、お店の方から他の箇所を勧められることもなく、対応が良いなと思いました。銀座カラーは光脱毛なので痛みもほぼなく、施術後の保湿に力を入れているということで、肌状態がとても良くなりました。毛穴も目立たなくなり、今では自分の腕のキレイさが自慢です。お店は阪急梅田駅から徒歩5分とアクセスも良く通いやすいのが嬉しいポイントです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラー梅田茶屋町店はアクセスも良く通うのにとても便利でした。スタッフさんみんなが一致団結をしているというか仲が良さそうな感じが伝わってきて、アットホームなサロンでとても雰囲気が良かったです。どのスタッフさんも顔見知りみたいになってどの方とも色んな話ができたのも楽しかったです。以前、ワキや腕は脱毛済みだったのですが、仕事でドレスを着る機会があるので、背中の脱毛をお願いしました。背中の上と下で1回約30～40分の施術で、3ヶ月に1度通うだけだったのでとても楽でした。背中の毛の自己処理は見えなくて危ないので一度もしたことがなかったのですが、脱毛をしたら肌のトーンが明るくなったので嬉しかったです。施術前後の保湿ケア、美肌潤美のおかげで肌質も良くなって美肌になったので、とてもお得な気がして嬉しいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">私が通っている梅田茶屋町店のスタッフさんは、いつも丁寧な接客と対応をしてくれるし、施術中の会話もすごく楽しいので、毎回通うのがとても楽しみです。無料カウンセリングに行ったときも、しつこい勧誘もなくて安心して契約をすることができました。回を重ねるごとに脱毛効果を実感できるようになってきているのもとても嬉しいです。初めて脱毛の体験に行ったときからお世話になっている担当者さんは、私の話をよく覚えてくれていて、毎回担当者さんに会えるのが楽しみです。私の希望や悩みを聞いてくれるだけじゃなくて、肌の状態に合わせた脱毛のコースを一緒に考えてくれるところがとても良かったです。気になっていた部分が綺麗になると、他のところも気になってしまうのですが、その度に丁寧なカウンセリングをしてくれるので、安心して通うことができます。梅田茶屋町店にして本当に良かったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学、バイト、サークル活動と毎日が忙しくて、活動範囲も広いので、どこにいても交通の便がいい梅田茶屋町店に決めました！以前、他店でひざ下とワキを施術したのですが、バイトの先輩から「見えるところだけやっても、見えないところのムダ毛の自己処理に時間がかかって大変でしょう。どうせなら全身やった方がいいんじゃない？」とアドバイスをしてもらったので、思い切って全身脱毛をしようと思ったんです。先輩も銀座カラーで全身脱毛をしたみたいで、自己処理しなくていいからとても楽だと言っていました。確かに先輩はお肌がすべすべなんです！そして通ってみたら、ムダ毛がなくなるどころか、銀座カラーでは美肌に力をいれていて、施術後の美肌潤美というミストのおかげでお肌がぷるぷるになったので、私自身も大満足だし、綺麗になった肌を見て彼氏も喜んでくれているので良かったです｡私も先輩みたいにすべすべお肌になれそうです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">以前、会社の近くの銀座カラーさんで契約をしたのですが、2回ほど通った後に異動になってしまい、通うのが大変になってしまいました。予約を取らないまま1年以上過ぎた頃、習い事で表参道に行く機会が増え、その帰りに表参道店を見つけました。習い事の日に予約を合わせればまた通えるかもしれないと思い相談すると、店舗の変更ができるとのこと。表参道店は大人可愛い素敵なサロンで、広くて開放的な感じが私はとても好きでした。以前の店舗と全体的な雰囲気は似ていますが店舗によって広さやスタッフさんの感じが違うので、いくつかの店舗を見比べて自分に合ったサロンに通うのもいいのかなと思いました。施術内容や使っている機械はどこのお店も同じだと思うので、何度も通うなら自分の気に入った雰囲気のサロンを選ぶのがいいのかなと2店舗通ってみて思いました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">私が通った表参道店は表参道駅からすぐだったのでとっても便利でした。店内が明るくキラキラしていて、女子力が上がりそうな雰囲気だったのも嬉しかったです｡大学生になって、サークルのみんなと海に行くことになったので、その前にVラインを脱毛したいと思ったのが無料カウンセリングを受けるきっかけでした。カウンセリングで話を聞いているうちにVIO全てを脱毛したいなと思ったんですけど、VIOセットと全身VIP脱毛が2万円ほどしか変わらなかったことと、学生証の提示での割引もあるということだったので、思い切って全身VIP脱毛で契約をすることにしました！実際に全身脱毛をして、もうどこも自己処理をしなくていいというのは本当に楽で、全身脱毛にして良かったなと心から思っています(*^^*)これでもうムダ毛に悩まされることがないので、全身脱毛をして本当に良かったです｡</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">前回まで他の店舗に通っていたのですが、引越しにより今回から表参道店にお世話になることに。スタッフの方もあまり忙しい感じを見せずに、こちらもゆったりと過ごすことができました。表参道店は気分的にもリフレッシュできる空間だと感じ、とても満足しています。対応してくれた担当者さんが施術も説明もとても丁寧で印象が良かったです。ぜひまたお願いしたいと思いました。初めて意見をお伝えしたいと思うくらい、丁寧な接客でした。とても一生懸命ということが伝わってきましたし、施術中も楽しくお話ができました。表参道店は接客態度がよく清潔感があり良かったです。ドライヤー利用やメイク直しの有無を確認し、パウダールームの場所を丁寧に教えてくれました。また施術中も熱くないか声かけをしてくださるので、安心して施術を受けることができました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学の近くにある表参道店は、地下鉄表参道駅B2出口より徒歩1分のビルにありました。強く勧誘されたらどうしようと思っていたんですが、契約するまで帰さないとか電話で強く勧誘されたりということもなくて安心しました。逆に、無料カウンセリングの時に、ラグジュアリーでキラキラと輝く大人の空間で、アップスタイルにしたきれいなスタッフさんばかりで、私も脱毛して肌が綺麗な素敵な女性になりたいと強く思いました！施術後の美肌潤美のミストが最高に気持ちよくて、乾燥していた肌がしっとりぷるぷるになって嬉しかったです(^^)脱毛に来ているというより、エステに通っている気持ちで通っています。大学の友達にも勧めたいと思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">地元の群馬県に銀座カラーのお店がなかったので、大宮店まで通いました。1時間もかからないので遠くは感じませんでしたが、銀座カラーの脱毛はとてもオススメだし、これから別の箇所も脱毛していこうと思っているので群馬にもお店を作って欲しいです。今回は、一番気になる腕の脱毛のコースにしました。初めての脱毛だったので、痛みのことが一番心配でしたが、腕の脱毛は痛みをほぼ感じませんでした。毎回、スタッフさんとおしゃべりするのがとても楽しく、しゃべっていたらあっという間に終わっていました。アフターケアでしてくれる美肌潤美というミストは保湿効果だけでなく、美肌効果もあるとの説明があったのですが、黒ずみもあった腕が本当に白くすべすべになったのでとても嬉しいです。ノースリーブを着るのも、電車のつり革をつかむのも全然気にならなくなりました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラー大宮店に無料カウンセリングに行って驚いたのは、その広さとサロンの雰囲気の良さです。とても広くスタイリッシュな雰囲気は、私が今まで脱毛サロンに抱いていたイメージをくつがえしてくれました。スタッフの方も皆さん優しくて、いつも笑顔で迎えてくれるので安心して通うことができたのも良かったです。私は夏に向けてVラインが気になっていて相談に乗っていただいたのですが、今まで受けられた方のお話を聞いているうちにVIOセットにしたくなりVIOセットでお願いしました。VラインだけでなくiラインとOラインも一緒にすると、境目も気にしなくていいし、水着もどんなものを選んでも安心して着られるのがすごくいいなと思いました。そして施術後は匂いやムレもなくなり、生理中も快適に過ごすことができるようになったのでとても良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">サロンっていつも緊張してしまうのですが、大宮店の担当者が、気持ちの良い対応でとても安らぎました。すごく丁寧に対応してくれて、次回指名したいほどの最高の接客でした。顔の脱毛の時は、まるでフェイシャルエステのような心地良さでした。施術した箇所は、脱毛はもちろんのこと、肌もキレイになっているので、これからも美肌効果のある脱毛の方針で続けてほしいです。以前より施術の痛みが格段に減ったのもとても嬉しかったです。施術中に寒さを感じるときなどは、電気毛布をしいて私を気遣ってくださり、ベッドが温かくて心地よかったです。またVラインの施術が初めてで不安があったのですが、丁寧に説明してくださり、安心してお任せすることができました。こちらの店舗で施術してもらうのは初めてだったのですが、優しく対応してもらえたので安心して脱毛できました。次回からも安心して通えるサロンだなと感じました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">大宮店は、大宮駅東口から歩いて5分のところにあり駅からとても近いです。以前から脱毛に興味があったのですが、すでに大宮店で全身脱毛を始めている友達が友達紹介キャンペーンで、お互いに商品券がもらえると聞き、無料カウンセリングへ。無料カウンセリングと言っても個室に通されるわけでもなく、間接照明が効いた白×黒のスタイリッシュな空間の広い待合室で、ハーブティーを飲みながら、スタッフの方ととてもフランクな感じで行われます。個室だと無理やり勧誘されてしまうかも・・・という心配もありますが、銀座カラーのカウンセリングはオープンスペースで行われるため、勧誘ではなく、それぞれの悩みに合わせた適切な箇所への脱毛のアドバイスをしてくれます。予約が取れるかが気になっていたのですが、実際通ってみると予約も取りやすく、毎回ゆったりとした気持ちで脱毛施術を受けられているので、銀座カラーにしてとても良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">腕や足などの気になるところだけを脱毛するか全身脱毛するかを迷っていたのですが、友達の紹介で銀座カラーへ行ってみました。行ってみて、サロンの綺麗さに感動！壁も床も白くて明るく、清潔感のある空間がとても気に入りました。ちょっとオトナなサロンにいる気分になれました。スタッフさんと相談して全身脱毛を契約しました。月々の分割支払いもできたので、毎月のお給料から1万円ずつぐらいで通うことができました。お店も綺麗だったので、ネイルサロンや美容院に行くような感覚で毎回通えています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学生になったら全身脱毛をしたいと思っていたので、通いやすい場所にある銀座カラー宇都宮店へ無料カウンセリングに行きました。他のサロンに比べて価格が安いとはいえ全身脱毛は高額なものなので、カウンセリングの時に親身になって話を聞いてくださり不安がなくなったのは本当に良かったです。しかも学生ということで脱毛学割というのがありこんなにお得な金額で全身脱毛できるなんて信じられませんでした。もし全身脱毛したいと思っているなら、学生のうちがお得なのでオススメです。しかも3ヶ月に1度通うだけなので、それも楽で良かったです。1度に全身の施術ができるサロンはなかなかないので銀座カラーにして本当に良かったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">一度に全身脱毛が約3時間でできることと、他の脱毛サロンにはない、Tゾーンを含めてお顔全体まで脱毛ができるということがとても気に入っています。銀座カラーオリジナルの保湿ケア「美肌潤美」も肌質がよくなったと実感できています。また、他の店舗への移動が可能なことも便利で嬉しいです。カウンセリングの際に、「ビキニラインなどの自己処理や痛みについて」、「なぜ脱毛するときに期間を空けなければいけないのか・・・」など、気になる内容についてもとても分かりやすく、丁寧に説明してくれたので安心してサロンに通うことができました。東武宇都宮駅から徒歩5分という便利な場所にあるので、便利で通いやすかったです。店内もお洒落で清潔感あふれる感じがとても心地よく、スタッフの方もいつも笑顔でとても話しやすかったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">東武宇都宮駅から徒歩5分のところにあり、とても便利な銀座カラー宇都宮店。おしゃれでリラックスできる空間です。また、全国転勤がある会社に就職が決まっているので、他店舗間の移動が可能なところも銀座カラーにした決め手でした。全身VIP脱毛にしたのですが、全身1回の施術を数回にわけることなく3時間ほどで施術してくれるので、本当に助かっています。回数を重ねるごとに赤く目立っていた毛穴がどんどん目立たなくなり、美肌潤美という脱毛後のミストやローションでつるつる美肌に生まれ変わって、美容エステ後のお肌のようになっています。これからも全身つるつるを目指して通いたいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">社会人になったら、前から通いたいと思っていた脱毛サロンに行こうと決めていました。周りの友人たちは、学生時代から通っていましたが私は勇気がでずそのままに…。でも就職先がアパレルでオシャレな人ばかりだから、美意識を高くしたいと思い勇気をだしてカウンセリングへ行ったんです。緊張している私に、担当してくれたスタッフの方が優しく説明してくれたので、緊張もほぐれ自然と悩みも相談できました。実際に通ってからも、毎回丁寧に施術をしてくれるので安心して通えています。カウンセリングの時のスタッフの方も、覚えていてくれて話しかけてくれるので嬉しくなります。横浜エスト店のみなさん、これからもよろしくお願いします。
</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">私が通っている横浜エスト店は、友達に紹介してもらったのがきっかけで通うことになりました。学校のお昼休みって、だいたいダイエットとか美容の話になるんですけど、脱毛についてもけっこう話題にでるんですよね。横浜駅の周りには、いろいろ脱毛サロンがあるし、実際通う時は友達の意見を参考にしようと思って、その話を聞いていました。友達の中で、ダントツで人気だったのが銀座カラー!!料金もお手頃なのに、効果がしっかり出て良いっておすすめされましたよ。私も今通っていますが、友達の話通りのサロンで気に入っています。肌がツルツルになることを期待して通い続けます。
</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">横浜エスト店を利用していますが、いつも笑顔で丁寧に対応してくれるので安心して脱毛を受けられています。脱毛の効果も実感出来ているので、通うのがすごく楽しいです!!施術が終わった後はお茶のサービスがあるし、パウダールームもちゃんとあるので、細かいところまで気遣いを感じます。ムダ毛に関しては、昔からすごくコンプレックスがあったので、サロンで肌を見せるのも不安でしたが、今は通って良かったと心から思っています。今度は、横浜西口店にも行ってみます!!</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 主婦<br></span>
            </dt>
            <dd itemprop="reviewBody">人より毛深いので自己処理をしない日はありませんでしたが、子供ができてからお風呂に入るのもバタバタで時間が取れなくなってしまいました。前から脱毛サロンには興味はあったのですが、なんとなくタイミングを逃してそのままに。早く始めていれば良かったと後悔しています。このままだと通う機会がなくなってしまうので、今回は旦那に相談してみたところ育児で疲れているだろうからと、ご褒美でサロンに通うOKが出ました!!嬉しすぎてすぐカウンセリングに行き、契約しちゃいました。これから通うのが楽しみでワクワクしています!
</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">千葉船橋店はルネライラタワー船橋の中にあって船橋駅からすぐ、とアクセス抜群だったので、ここに決めました。施術の後に1階にあるカフェでお茶をして帰るのですが、それも楽しみの1つでした。私が無料カウンセリングに行ったのは、春になると肌の露出が増えるので、その前に足のムダ毛をなんとかしたいと思ったからです。足全セットにはひざ上、ひざ、ひざ下だけでなく、足の甲や指まで入っていることに驚きました。確かに足の指にもうぶ毛が生えていて、気になっていたんです。そんな細かいところまで施術してくれるんだと嬉しくなったのを覚えています。しかも6回で終わるし、月額制の支払いでいいというのも魅力的でした。この金額はとても安いし通いやすかったです。こんなに安くて便利ならもっと早く通えば良かったと思うほどでした。脱毛したおかげで足に自信をもつことができました。友達にもぜひ紹介したいと思いました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛サロンは山ほどあるので、どこのサロンに通うかいろいろとクチコミを参考にしました。銀座カラーは「肌の弱い方には向いているサロンだと思う」というのがあり、乾燥肌でカミソリ負けしてしまうこともある私には向いているかな？と思い、まずは無料カウンセリングに行ってみました。スタッフさんがとても丁寧に説明してくれて、美肌潤美という保湿ケアのことを教えてくれました。つけてもらった後はしっとりとして、時間が経つともちもちの肌になっていて、肌質がとてもよくなるケアでした。やっぱり乾燥肌の私にはここがいいかも！と思い契約しました。毛は順調に少なくなっていって、肌はどんどんすべすべになりました。今では乾燥肌で悩むこともなくなったので大満足です！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">都内にある会社の近くで脱毛を検討しましたが、そのサロンは予約が取りづらかったので、休日でも通いやすいように自宅の近くで脱毛できるサロンを探していました。千葉船橋店は、船橋駅からとても近く自宅からも近かったので、パウダールームでメイク直しをするのが面倒な日でも、すっぴんで帰っても恥ずかしくないなというのが、気持ち的に楽で良かったです。最初は顔とワキと膝下の、人からもよく見えるところのみ施術しようと考えていましたが、対応してくださるスタッフさんの肌がとても綺麗で、産毛がなく輝いた肌をしていたのを見て、自分もそんな肌を手に入れたいなと全身脱毛にチャレンジしています。また、同年代のスタッフさんも多いので、施術中に恋愛相談などもしていて、施術の時間が楽しくてあっという間にすぎます。楽しくて綺麗になれるので、通うのが毎回楽しみで仕方ありません。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">幼い頃から海が近い環境だったので日焼けを気にせずに遊んでいたのですが、就職活動を考えて、日焼けをやめて美白に戻してきました。そうしたら、日焼けしている時は気にならなかったムダ毛が気になりだして、接客業が希望なので社会人になる前に脱毛をしておきたいなと思ったんです。就活とバイトで忙しいので、脱毛するなら予約もとりやすくて、家の近所の船橋駅の近くにある銀座カラーと決めていました！脱毛し放題のコースは期限がなく使えて、何度でも通えて、月額で安く全身脱毛することができるので、大学生の私でもバイト代の中から負担なく始められました(^^)また施術中に、スタッフの方に就活の相談にのって頂いたり、社会人としての身だしなみだけでなく、心構えもアドバイス頂いたりして、とても勉強になったのも嬉しかったです！ありがとうございました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学の友達から「銀座カラーは安くてお肌がつるつるになっていいよ！」と勧められたので無料カウンセリングに行きました。ワキの脱毛は、別の有名チェーン店でしたことがあったんですが、そのサロンはビルの一室で、暗い雰囲気の場所にあって、通うことが恥ずかしいような感じだったんです。でも銀座カラー池袋店は駅から徒歩3分で、お店に入ると窓からの光が差し込む綺麗で開放的なお店で雰囲気もとてもよかったんです。脱毛が恥ずかしいという気持ちじゃなくて、綺麗なサロンに何度も通いたい！という気持ちになりました。スタッフさんも笑顔で明るい方ばかりで、とても居心地がよかったです♪脱毛サロンは数多くあるので、お店の雰囲気や通いやすさは重要だなと思いました。友達の言った通り、脱毛後はお肌つるつるで言うことなしです！別の友達にもおススメします(*^^*)</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">池袋東口から徒歩3分と駅近で、店内が白とベージュで明るく、とても落ち着いた雰囲気の池袋店。通ったきっかけは、もうすぐ夏で水着を着るからです。以前から気になっていた脱毛についてネットで色々調べてみたり、口コミサイトなどを見て、銀座カラーが良さそうだったので無料カウンセリングに行きました。カウンセリングでは脱毛に関する不安に対して丁寧に答えてくださり、水着を着るならVIOセットがお勧めですと言ってもらったので、思い切ってVIOセットで通ってみることにしました。丁寧なカウンセリングや店内の清潔感のある雰囲気が通う決め手となりました。実際に通ってみると、多少の痛みは感じたものの、今までの家でのムダ毛処理数年間の手間を考えたらとっても楽で、思い切って通って本当に良かったと思っています。これで水着を着るのも怖くないです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">美容系サロンが立ち並ぶ池袋でどこの脱毛サロンに行こうか悩んでいましたが、本社と同じビルにある銀座カラー池袋店なら、安心してサービスが受けられるのではないかと思って銀座カラーにしました。スタッフの方も美意識が高くて、脱毛の話だけでなく、美容や恋愛のアドバイスもいただけました。また、別途、シェービング代をとられるようなサロンもあるそうですが、銀座カラーはシェーバーが持ち込みでき、別途料金をとられるようなこともなかったです。現在、大学4年生で、就職先が転勤のある会社なので、今後通えるかどうかの心配もありましたが、配属先の周辺のお店でも施術を受けられると聞いて、安心して通っています。全身の毛がないつるつるボディを目指してがんばります！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラー池袋店は池袋駅東口から歩いて3分くらいの場所にあって、1Fがdocomoショップのビルの7階でとてもわかりやすかったです。無料カウンセリングで行ったのが平日の昼間12時過ぎにもかかわらず、待合室はすでにお客さんで埋まっていて人気の高さが伺えました。こんなに人気だと予約が取りづらいのではと心配になりましたが、ベットが17台もあり、池袋にはもう1店舗あるとのことで、予約が取りづらいと感じたことはありません。施術後の美肌潤美というミストとローションで、乾燥がちだった肌がぷりっぷりに生まれ変わってきました。スタッフの皆さんも、いつも笑顔で暖かい対応をしてくださるので、脱毛に行くというよりリラクゼーションサロンに行くという感じで大満足です！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">仙台店は、仙台駅からアーケードを入ってすぐのところにあり、ショッピングの間に行くこともできる場所だったので、とても通いやすかったです。脱毛して一番よかったのは両ヒジ下です。長袖を着ている冬場でも、仕事中に腕まくりをするのが癖なので、腕の毛はいつも気になっていました。毛の量がそれほど多くはないのですが、剃り忘れたりするとプツプツと毛が生えてくるのが気になるし、自分でも人からもすぐに目に付く場所なので、脱毛して本当によかったと思います！今ではつるすべな腕を見るのが楽しみでもあります。保湿のためのローションはとても潤うので銀座カラーにしてよかったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">今までVラインの事をあまり気にした事がなかったんだけど、大学生になってサークルの仲間と海に行くことが決まってから、海に行く前にVラインの脱毛をしたいと思うようになったんです。それで銀座カラーのVIO脱毛に通うことにしました。それからは下着からムダ毛がはみ出す事もなくなってその違いにとても驚きました！Vラインは毛が濃い部分なので、脱毛して綺麗になると印象が全く違って、本当に嬉しかったです(*^^*)生理中には匂いやムレも気になっていたのが、ムダ毛がなくなったおかげで清潔に保てるので、気にならなくなりました～。特にiラインとOラインは自分では全く見えなくて今まで自己処理をした事がなかったんだけど、綺麗に脱毛できたおかげで、全く気にしなくてよくなったのが本当に嬉しかったです♪これで安心して海で水着になる事ができます(*^^*)ありがとうございました!</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">ずっと腕と足の自己処理をしてきましたが、そのたびに肌が赤くなって毛穴を傷つけてしまい、治った頃にまた自己処理をするので、毛穴がずっと赤い状態になっていました。皮膚科にいっても自己処理による刺激なのでその刺激をやめれば治ると言われましたが、ずっと続けてきたので自己処理をやめると太い毛が出てきて、男性のようなムダ毛になってしまっていました。そんな時に、銀座カラーでは脱毛しながら美肌になれると聞いて、早速カウンセリングに行きました。スタッフの方の、毛穴など見当たらない白い肌を見て、自分もこうなりたいと強く思いました。自己処理が減るごとに肌の赤みは消え、毛穴が目立たない肌になり、何年も悩んでいたムダ毛と肌の悩みから解放されました。早く銀座カラーに通えばよかったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">仕事柄髪を結ぶことが多く、鏡で襟足の形をチェックしているのですが、襟足の形が悪く、結んだ姿に自信がありませんでした。襟足だけきれいになればと思っていましたが、無料カウンセリングに行って、銀座カラーのスタッフの方の肌の綺麗さに衝撃を受けました。襟足だけでなく全身、毛がないと肌の輝きが増すことを目の当たりにしたのと、全身脱毛の中に襟足が入っていると聞き、全身脱毛をすることにしました。施術の度に美肌潤美というミストを使ってもらえるのですが、美肌効果がとても高く、回を追うごとに肌が綺麗になっていくのを実感しています。また、スタッフの皆さんが綺麗で美意識が高く、脱毛の悩みだけでなく美容の悩みにも相談にのってくれて、内面、外面、精神面で綺麗になれるアドバイスをいただけることもあり、とても嬉しいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">年齢的に結婚式に出席することが増えてきて、デコルテや背中を出すドレスを着る機会が増えてきました。背中が綺麗な友人を見たとき、自分の産毛がとても気になってしまい、全身脱毛をすることに決めました。銀座カラーでは全身VIP脱毛というコースがあり、他のサロンに比べて価格設定もとてもお得でした。自分では見えない襟足までとても綺麗にしてもらえたので、普段でも髪をアップにした時に、友達から襟足が綺麗だとほめられるようにもなりました。結婚式でドレスを着る時も人目を気にせず楽しめるようになり、全身脱毛をして本当によかったと思います。まだ結婚の予定はありませんが、これで、自分が結婚する時に慌てて脱毛することもないので、早めにやっておいてよかったなと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">以前、他のサロンでお試しコースを受けてみたらお試しコースは安かったのですが、他の箇所の追加料金がとても高く続けるのが難しい事がありました。そういう経験があったので銀座カラーでも金額の面が心配だったのですが、職場の同僚が通っていたので、私も無料カウンセリングを受けて通う事を決めました。フリーチョイス脱毛6回でヒジ下もひざ下もとても安かったので、金額的にとてもお得な感じがしました。6回通い終えて思ったのは、銀座カラーにして本当に良かったという事です。ヒジ下とひざ下が綺麗になったら、今度はヒジ上やひざ上も気になるようになってきたので、またフリーチョイス脱毛で通いたいなと思っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">他店で脱毛をしていたのですが、盛岡市は近県から脱毛処理に通う人が多く、予約が取りづらくて3年間の間で予約がとれたのはなんと数回！！両腕の脱毛をしているので夏場は日焼けもできずにいつも黒い手袋と日傘が必須で、盛岡の少ない夏の日を思いきり遊べずに困っていました。でも友人が銀座カラーならシステムがきちんとしているので予約がとりやすく、しかも施術するたびに美肌になると教えてくれました。料金も他店より安かったし、美肌になるならと思い切って申し込みました。今のところ順調に脱毛の予約がとれ、施術ができています。施術するごとにどんどん肌もきれいになっています。全身脱毛をしているので、水着を着た時に気になっていたムダ毛も気にしないで、今年は何年かぶりに海で思いきり遊べそうです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">私が住んでいる地域では脱毛してくれるお店がなく、銀座カラーのある盛岡市まで車で1時間かかります。何回も通うようになると面倒だなと思い、脱毛することを悩んでいました。でも銀座カラーでは、他のサロンで数日かかる全身1回分を、一度で施術してくれます。全部なくなるまでには数回施術が必要ですが、それでも全身を1回でやってくれるのでとても助かっています。また、友人が、他店では全身脱毛に襟足などが入っていなかったということで追加料金が発生したと言っていたので心配でしたが、銀座カラーはそういった境目がなく全身をくまなく脱毛でき、料金も他店と比べて安かったので銀座カラーで脱毛をして本当に良かったです。ムダ毛の処理の時間もなくなり楽になったので、もっと早く始めればよかったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">他のサロンで足の脱毛をしたのですが、追加料金が高くて驚きました。今回、銀座カラーさんで残っていた足の毛を脱毛していただいたのですが、お値段も効果もとても納得のいくものだったので、ここにしてよかったです。
初回のカウンセリングでは、前のサロンでの悩みも親身になって聞いてくださったので、安心して通うことができました。お店の雰囲気もとても綺麗で落ち着いていて居心地のよいサロンだったので、はじめからここに来ていればよかったです。子どもがまだ小さいので、お風呂で足の毛を剃っている時間もなかったのですが、今では自己処理いらずの綺麗な足になり大満足です。夏にはスカートをはいて素足を出したいと思っています。足がキレイになったので、ぜひまた違う箇所でも通いたいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">春に向けて腕と足のムダ毛を脱毛したいなと思い、銀座カラーへ無料カウンセリングを受けに行きました。最初に腕と足を綺麗にしたいと伝えたところ、それなら全身VIP脱毛の方がお得ですよと教えてくださり、全身脱毛のコースについても丁寧に説明をしてくださいました。確かに夏には水着になるしVIOも気になるので、全身VIP脱毛のコースに通う方がいいなと思い、全身VIP脱毛コースに決めました。1度の施術で全身の脱毛ができて、3ヶ月に1度通えばいいというのは、忙しい私にとってとても楽でした。どんどんムダ毛が減っていくのを目の当たりにして、自己処理をしなくていいという快適さに早く通えば良かったと思いました。スタッフの方も皆さん優しくて、毎回笑顔で対応してくださるので安心して通えて良かったです。予約の取りやすさや施術、金銭的にもとても良かったので、脱毛をしたいという友人に銀座カラーを勧めたいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">結婚が決まり、結婚式でウエディングドレスを着た時の襟足や背中の産毛を処理してくれるところを探していたら、友達に銀座カラーを勧められ無料カウンセリングに行きました。脱毛というと期間が長くかかる、予約が取りづらいといったイメージがありましたが、銀座カラーでは予約もとりやすく、引越しや転居があっても他の店舗で対応してもらえて、肌のケアもしっかりしてくれるので一生のお肌のケアパートナーとして全身脱毛を申し込みました。実際、施術を受けてからお肌がぷるぷるになっていき、ブライダルエステも受ける必要がなくなりました。自信を持ってウエディングドレスが着られそうです。これからも一生のおつきあいをしていきたいと思っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">ずっと脱毛に興味がありましたが、仕事がら転勤があり、また残業もあるので、期間もかかる脱毛に通うことを諦めていました。でも、名古屋の女性の美しさに触発され、より女性として自信がもてるようになりたいと思い、全身脱毛を考えました。銀座カラーは全国どこの店舗でも施術を行うことができ、支払い後は期間を気にせず通えると聞き、全身脱毛をお願いすることにしました。名古屋栄店は平日は夜9時まで営業していて、当日予約もOKなので仕事が早く終わりそうな時に施術をお願いすることもたまにあります。お風呂タイムにおこなっていたムダ毛処理の時間から解放され、その時間でエクササイズを始めました。銀座カラーのおかげで名古屋ライフをより楽しめそうです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラーの脱毛は、脱毛した後のお肌がすべすべになると友達からすすめられたので行ってみました。脱毛とセットで美肌潤美という保湿ケアがついているとのことで、施術後のお肌が本当にぷるぷると潤ってびっくりしました。毛を無くしていきながら、お肌がどんどん美しくなっていって一石二鳥のサロンだと思います。もともと乾燥が気になる肌質だったのですが、銀座カラーで脱毛してからは、肌の乾燥も気にならなくなりました。スタッフの方の施術が丁寧なのも安心できて良かったです。細かいことですが、手の甲や指の毛などもキレイに無くなったので、ネイルサロンに行ったときなども自信をもって手を出せるようになりました。脱毛してお肌がキレイになったおかげで、いろいろな場面で女性として自信がもてるようになりました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">社会人になり年上の彼氏ができたのをきっかけに、もっと綺麗になりたいと思い脱毛を考えました。今まではワキのみ脱毛をしたことがありましたが、それ以外の部分は自己処理を続けていました。好きな人に見られることを考えると、全身を脱毛している方が自分で見えない背中やヒップなどの部分も楽ができると思ったので、どうせやるなら全身脱毛をやりたいと興味を持ちました。いくつかの脱毛サロンで無料カウンセリングを受けてから銀座カラーを選んだのですが、銀座カラーにした理由は、全身脱毛が1度の施術でできて3ヶ月に1度通えばいいこと、お値段も全身脱毛にしてはお安かったことです。脱毛サロンは予約が取りにくいと聞いていたのですが、銀座カラーは予約も取りやすかったので、そこもとても良かったです。全身脱毛をして自分の体に自信が持てるようになったので、通って本当に良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">接客業でお客様と近くで接することが多く、意外と手の指や甲をみられていることに気づき、始めはそこをキレイにしたくて銀座カラーに無料カウンセリングに行きました。対応してくれたスタッフの方がとても親切で、顔も腕も産毛のないすべすべの肌をしていて、とても美しく輝いていました。産毛がないだけでこんなに肌が輝くことに感動し、その場で全身脱毛に切り替えました。施術の最後に美肌潤美という美容成分がたくさん入った高圧噴射のミストをあててケアしてくれます。そのおかげで、お肌がとてもしっとりし、肌の輝きも変わってきました。私が通った銀座本店は、銀座駅から直結しているので雨の日や寒い日でも通いやすく、とても便利で良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">夏になると腕やひざ上なども出す機会があり、シェーバーで自己処理をしていました。以前、違うサロンでヒジ下とひざ下を脱毛したのですが、それ以外の箇所の自己処理に時間がかかり、一部のみの脱毛では意味がないなと思い、全身脱毛をすることにしました。全身の施術を数回でわけずに1回で行ってくれる銀座カラーはとても魅力的で、無料カウンセリングでお話を聞いて銀座本店に通うことに決めました。腕やひざ上などの出ているところがキレイになればと思っていましたが、意外と効果があるのが背中でした。産毛がなくなったことにより色白になり、肌が一段と明るくなっていきました。銀座本店という名前の通り、サロンではゴージャスな雰囲気も味わえて優雅な気持ちで過ごせてうれしかったです。最初から全身脱毛を銀座カラーでやっていればよかったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">女子大生になったのをきっかけに、素肌をキレイにしたい！と思って顔全体の産毛を脱毛することにしました。高校生の時は、フェイスラインや鼻下は自己処理をしていたのですが、だんだん濃くなってきていたのも気になっていたので、顔全体コースのある銀座カラーを選んだんです。心斎橋店は駅から徒歩1分ととても通いやすくて、お店はとてもキレイで清潔感があって、少し大人な気分が味わえて嬉しかったです(^_^)スタッフさんがとても丁寧でわかりやすく説明してくれて、施術中には大学生活の話も聞いてくれたので、楽しい時間を過ごせました。施術はとても気持ちがよく、フェイシャルエステに来ているみたいでした｡想像していたより痛みもなくて、途中で寝てしまったらあっという間に終わっていたこともありました。お肌がつるつるになったので、もっと早くやっていればよかったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">中学生くらいからムダ毛のことはとても気になっていて、ワキだけは高校生の時に脱毛したんですが、全身だと金額が高いのでしていませんでした。大学生になってバイトをするようになったので、これを機に全身脱毛をしてツルツルで綺麗な素肌を目指したいと思って、銀座カラーの無料カウンセリングへ行きました。やっぱり全身脱毛は高いと言えば高いんですが、銀座カラーを選んだ理由は、他のサロンの全身脱毛より価格が安かったのと、約3ヶ月に1回、しかもその1回で全身ができるので通いやすいというところや、予約のとりやすさなどです。心斎橋店が御堂筋線の心斎橋駅から徒歩１分という立地の良さも決め手の1つでした。やっぱり駅近って助かりますよね。無料カウンセリングをしてくれたお姉さんが、ムダ毛の悩みについて親身に相談に乗ってくれたのも、安心できてよかったです(^^)6回なので約1年半でムダ毛がほとんどなくなって、今はムダ毛処理もほぼ必要ありません！今までのムダ毛処理の面倒さを思い出すと、早く通えば良かったと思うくらいです。これで、彼とのデートでもムダ毛を気にしなくていいので楽で嬉しいです｡</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学生になって彼氏ができたんですが、接近するたびに彼氏のヒゲが気になって、自分もそうかもと思って自己処理を始めました。でも、シェーバーで間違って切ってしまったりにきび跡を傷つけてしまったりと、お肌の状態が悪くなって悩んでいたんです。どんどんお肌がキレイになっていく友達に、銀座カラーを紹介されて私も鼻下と顔の脱毛を始めたんですが、施術はほとんど痛みがなくて、お手入れ後はミストと保湿ローションで整えてくれたのがとても気持ちよかったです(*^^*)毛がなくなったことで、お肌がスベスベで色が白くなって、お化粧のりも良くなったので、まるでエステにきているようでした！脱毛した結果、お肌の状態もよくなって、今では毛穴も目立たない白い肌に生まれ変わりましたよ！！もっと早くに始めていればよかったです～。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">水泳をしており、Vゾーンの処理にいつも悩まされていました。海外セレブで流行っているVIOの部分の脱毛にとても興味がありましたが、デリケートゾーンなだけにたくさんの人に施術してもらうことに抵抗があり悩んでいました。銀座カラーでは、施術は基本1人（2人入ることもある）のスタッフが行い、丁寧に施術してくれます。デリケートな部分なので配慮がきちんとされ、ストレスなく脱毛を行えることはとても助かりました。生理のときの不快感からも解放され、お手入れの心配もなく好きな時に水泳を楽しむことができます。私が通った心斎橋店は、アップルストアの上にあるので迷うことなく通えたのもよかったです。また、予約が当日OKだったりWEBからもできるなど、とりやすく、もっと早くに始めていればよかったなと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">わたしは今までVラインのお手入れなんて気にしたことがありませんでした。温泉に行くときも可愛いショーツをはいているときも。あるときファッション雑誌の読者アンケートコーナーでVラインのお手入れについての記事を見ました。なんと7割の女性がVラインのお手入れをしている！みんなお手入れしてるんだなぁ…と衝撃を受けました。そこからはお手入れを全くしてこなかった自分のVラインが気になり始め、自己処理だと怖かったのもあり脱毛を考えました。銀座カラーさんはVラインだけでも細かくパーツが分かれていて、形を整えるだけでも気軽に始められました。お手入れしてもらうときはきちんとした個室なので安心して施術を受けられました。デリケートな部分の脱毛でも、恥ずかしさをほとんど感じさせないスタッフさんの優しい対応もとても良かったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">スポーツジムに通い始めてからVラインの毛が気になり始め、自分で処理をしていました。でもわたしのVラインの毛は太めなので毎回自己処理が大変で、肌への負担も心配…。そこで脱毛してみようかなと思い始め銀座カラーさんへ！銀座カラーさんは施術前・施術後に保湿をしてくれるので、肌がより潤っていく感じが嬉しいです♪「施術を受けてから1～2週間ぐらいで毛が抜け落ちていきますよ」とサロンの人に言われ、わたしのような太い毛でも抜け落ちてくれるのかなーと思っていました。でも本当に抜け落ちていき、脱毛の効果を実感！半月ほどすると次の毛が生えてきたのですが、以前よりも毛が柔らかくなった気がします。パンツからはみ出す毛も気にならなくなりましたし、ジムでもより一層気持ちよくトレーニングができるようになりました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">友達と温泉旅行に行ったときに、みんなで一緒にお風呂に入って、自分と友達とを比べて、毛の生えてる範囲が広いのかも・・・と思うようになり、ずっと気になっていました。お風呂だけじゃなく、普段からほぼ確実にショーツからはみ出る毛が気になっていたので、毛が生える範囲を狭くするためにVラインのコースをお願いすることに。
自分で剃っていた時はかみそり負けすることもあって結構大変でしたが、施術後は本当に楽になりました（＾＾）この箇所の特徴なのかもしれませんが、抜けるのがわかりやすく段々とキレイになっていくのが実感できました。荒れ気味だった肌の状態も良くなってきたし、きれいな状態になるのが今からとても楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">近所のジムに通いよく泳ぐようになり、水着を着るときにVラインが気になり出しました。基本めんどくさがりで最初は自己処理していたのですが、太ももの付け根部分などはとても太い毛だったので、剃った後でも黒くプツプツと見えて、ちょっと目立つ感じでした。場所的に抜くのは大変だし、なによりもとても痛いので、お店で脱毛するのが一番かな～と思いました。
痛みに慣れていたせいか、施術の時の痛みは個人的にはそれほど気にならなかったです。今では、水着を着た時に気にする部分はVラインではなく、全体の体型？というかスタイル（笑）になりました。スタイルに磨きをかけたら、夏には海やプールでも自信を持って水着を着られると思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">私は小さい時からアトピーなので、脱毛はずっと興味があったけど怖くて、やっていませんでした。おしゃれのためだけだったらガマンできるけど、月に１回必ず来る生理の度にかゆみがひどくてかなり辛くなるので、思い切って銀座カラーさんのカウンセリングで相談しました。
アトピーの人でも肌状態が落ち着いていれば大丈夫と聞き、実際に脱毛されて効果もあり、満足されている方の話も教えてもらい、勇気が出たので脱毛する決心をしました！
今では思い切って脱毛して本当によかったと思っています。生理のときは外出を控えるほどだったのが、今は平気になりました。かゆみだけじゃなくムレも臭いもあまりなくて、ブルーな１週間じゃなくなりました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">Vラインのiライン脱毛って、彼氏以外の人に見せるものでもないし（笑）なんで必要？と思っていたのですが、友達が「下着からはみ出たりしなくなるし、ムレも少なくなってすごくいいよ」と言っていたので興味をもちました。お店の人に詳しく聞いて、なるほど！と思ったので、脱毛し放題コースだし、やってみることにしました。
実際にやってみた感想は、iラインの脱毛、かなりおススメです！！
どうしてもムレる場所なのでかゆくなったり、おまけに生理時は臭いも気になっていたのですが、脱毛してからの改善効果をすごく実感しています。脱毛ってすごいんですね。お姉ちゃんに話したら、脱毛したことのない姉も、脱毛したくなったって言ってました。きっと近いうちにやると思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">生理期間中、大事な部分の肌が荒れてしまうのが長年の悩みでした。ネットでいろいろ調べているときに、脱毛も改善策の１つという口コミを見つけたので試してみることにしました。
口コミだけでは不安だったので、銀座カラーの人に、質問しまくりましたが、どれも納得できる答えだったので、思い切ってやってみることに。
今まで悩んでいたのが嘘みたいに、生理中の肌荒れが改善しました！これは楽！！ムレが気にならなくなったのが一番大きいのかもしれません。スタッフさんも、iラインの脱毛は衛生的にいいと言っていたので、こういうことか！と納得しました。夏場とかは、特によさそうだな、と思っています。こうなるんだったらもう少し早くやっておけばよかったなぁと思ってます。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">Vラインの脱毛をやろうとは思っていなかったけど、全身脱毛コースにしたので、せっかくだからやってみようと思い、お店の人に勧められたVラインのiラインから始めました。
お店の人の話でも、口コミでも、水着を着るときや温泉のときにすごく楽チンと聞いていたので、効果を楽しみにしていました。
つい最近、旅行で海に行ったのですが、着替えるときも海でのんびりと寝転んでいるときも、毛の事を気にせず思いっきり楽しめました！
デリケートな部分だけに、最初はちょっと痛みが強い気がしましたが、効果の方が大きくて満足しています。痛みは、すぐに慣れるので（個人差はありそうだけど・・・）とくに大きく気にするところでもないと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛し放題コースで気になるところはほぼやり終えたので、顔の脱毛をすることにしました。まずは、顔の中でも日頃ムダ毛処理をしている、鼻の下から始めることにしたんです。
顔の脱毛効果もすごいです！！脱毛することで毛穴がきれいになるからか、肌が明るくなってノーメイクでも毛穴がほとんど目立ちません。最近では日中はほぼすっぴんで日焼け止めだけ、ってことが多くなりました。それなのに「すっぴんに見えない！」と友達にうらやましがられます(*^ ^*)
肌に負担が少ないとさらに美肌になってる気がして、すごくうれしいです。
次にやるところはまだ決めてないけど、顔の脱毛も全体的にやってみようと思っています。友達や恋人が顔に近づいても自信持っていられるようにまでなりたいです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">鼻下の毛は顔の中で一番濃いそうで、子供のころからヒゲみたいとからかわれたこともあってずっと気になっていました。でも顔の脱毛、しかもここだけ脱毛するってどうなんだろう？と思っていました。
仲のいい友達にその悩みを話したら、銀座カラーの話になりました。友達はワキの脱毛をしていたのですが、お店の人に聞いてくれて、顔の部分脱毛もあると分かったので、無料カウンセリングに行くことにしました。
顔の脱毛をしている人は結構たくさんいること、美肌効果やニキビ予防にもいいこと、などを聞いてすごく惹かれちゃいました。
でも顔だから、痛みとかは無理かも。。と思っていたら、友達が「大丈夫。そんなに気にするほど痛くないよ！」と言ってくれたので、思い切って脱毛することにしました。今は本当にやってよかったと思っています。メイクの仕上がりが格段に違うし、自分に自信がもてるようになりました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">カラダの脱毛をしているとき、お店の人に「顔の脱毛はどうですか？」と聞かれました。まったくお手入れをしていないかどうか聞かれて、よく考えたら鼻の下の毛はときどき自分で剃るなーと思い、そう伝えたら「少しでも自分でお手入れしているなら、脱毛がおススメですよ」と言われました。すでに脱毛するとすごく楽ちんなことが分かっていたし、肌もきれいになることも実感していたので、やってみることにしました。
ニキビが改善されたり、人によっては美肌効果もあるそうです。もともとニキビはあんまりできないけど、美肌効果は魅力的なので、効果が出てくるのが楽しみです＾＾ 顔の毛は細いせいか、身体の脱毛よりも効果を実感できるのに時間がかかるらしいですが、少なくともすでに前より美肌にはなってると思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">メイクをするときいつも気になって、ほとんど毎日自分で剃っていました。でも、不自然に青く残ってしまって肌も荒れるし、メイクだけでは隠し切れない状態になり、、思い切って脱毛することにしました。
だんだん目立たなくなってきたので、本当にやってよかったと思っています。最初、顔の脱毛は周りでもやっている人がほとんどいなかったのでちょっと不安でした。未知の世界で。脱毛とか怖くないの？？と思って・・・。
そんな不安そうな私を見たからか、スタッフの方がとても明るくやさしく、丁寧に説明してくださったので、安心して脱毛することができました。本当にありがとうございました。今では友達にもおススメしています！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">ワキの脱毛をしているときにお店の人とスキンケアの話をしていて、口の下にニキビができやすいという悩みがあると言ったら、口下脱毛をオススメされました。脱毛のスキンケア効果はワキで十分わかっていたので、アリかも？と思って早速やってみることに。
本当にニキビが出来にくくなって感動してます！脱毛のおかげで毛穴がきれいになってるってことなんですよね～。チョコレートもナッツも、たくさん食べても、もう怖くありません！（食べ過ぎて今度はダイエットが必要になりそう苦笑）
ニキビがなくなって、お化粧もきれいにできて、おしゃれがますます楽しくなりました。友達でニキビに悩んでいる子にぜひぜひ教えてあげたいと思いまーす。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">顔の脱毛はあんまり興味がなかったけど「フェイシャルエステと思ってやってみるといいですよ。」とお店の人に薦められやってもらうことにしました。
顔の脱毛、いいです！今までにない感じ！
特に口の下は、今まで気にしたこともなかったけど、脱毛したことでくすみが消えて、肌のトーンも明るくなって、ファンデーションが薄づきでもすごくきれいに仕上がります。驚きました～♪
今までは、顔全体的にファンデーションが結構厚めだった気がします。脱毛して、ナチュラルメイクできれいに仕上がるようになりました。ファンデがなかなかなくならない！お財布にやさしい効果もうれしいです(^^)/</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">もともと顔の毛が濃いめで、ずっと悩んでいました。顔の脱毛を始めるとき一番気になる部分を聞かれて、真っ先に口の下と答えました。自分で剃っていたからか何だか黒ずんでしまっていて、いつもファンデーションを厚く塗っていました。同じ脱毛でも顔と身体とでは効果も違ってくるんじゃないかな？と思っていたので、どんな感じになるのか正直不安で不安で。。。
何回かやってみて、だんだんと太い毛は生えてこなくなったようです。毛質によっては、ある程度回数をかけないとなくならないという話を聞いていたので、どのくらい通えばいいんだろう・・・と思っていたのですが、このペースだとあともう少しかな？やっと長年の悩みが解消される～！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">顔脱毛のコースを申し込んで、顔全体の毛の状態をチェックしてもらいました。気にしたこともなかった口の下の部分をチェックされて驚きました。みんなも処理し忘れがち、だそうです。私の場合、特に目立つような生え方をしているわけではなかったので、あまり気にしたことはなかったし、だから剃ったこともありませんでした。でも、やっぱりプロの方から見るとそれなりに生えているようで、お手入れした方がいいとアドバイスされました。
実際に脱毛してみると、明らかに肌もワントーン明るくなった感じで、化粧ノリもよくなった気がします。脱毛することで肌質が変わったと思えるくらい、きれいになって嬉しいです。プロのアドバイスはすごいと思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">今までは脱毛器で自己処理していました。肌が弱いのも関係あると思うけど段々と肌荒れが治りにくくなってきて…。これ以上肌に負担をかけたくなかったので、お金はかかるけど脱毛サロンに行くことに決めました。
電車で銀座カラーの脱毛学割を見ていたので、カウンセリング予約をしました。カウンセリングでは肌が弱いことを相談すると、カウンセラーの方は丁寧に対応してくれたのでとても好感が持てました！
本当に肌荒れが怖かったのですが、脱毛前後のケアや家でのアフターケアの方法などを教えてもらい、少しずつ肌が落ち着いてきました。脱毛の回数が進むにつれ肌の状態も良くなってきて、始めからプロに任せれば良かったんだな、と。次回が最後の6回目ですが、7割近くの毛がなくなっている気がします。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">バイトの制服が半袖なので、接客をしている間いつも腕のむだ毛が気になっていました。お客さんにも「あの子の腕ブツブツだな」なんて思われているんじゃないか…と良からぬ想像までしてしまったり。通いやすい場所でないと続かないから、バイト先の近くにある銀座カラーで脱毛することにしました。
実際の脱毛時間は20～30分程度で、エステティシャンの人も手際よく対応してくれ、脱毛の知識も豊富だから質問に答えてもらったり体験談を教えてくれたりと、脱毛している時もお話できて楽しい！私はまだ学生なのでまとまったお金は出せないから月額コースがあるのでとても助かっちゃいました。
この間バイトのスタッフから「腕きれいになったね～」と言われニッコリ。やはりみんな見ているんですね～。次は足の脱毛をするつもりです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">若干毛深いほうではあったのですが、あまり気にせず過ごしていました。
大学に入り上京。東京の大学生の美意識に驚きました。とはいってもまとまったお金もないので、家で自己処理をしていました。
「社会人になる前にキレイにしたい、この手足のチクチクをどうにかしたい！」と思っていたので、大学4年生の春から脱毛をスタートしました！
最初の1回は緊張して痛いのか痛くないのかもよくわからない位でしたが、2回目以降は「こんなもの？」くらいの感じで終わるので、問題なく通えています。
それよりも、1週間くらい経ったら毛がするっと抜けてくるのがすごい楽しい！自分の腕を見て一日中毛を抜きたい衝動にかられています。
このペースでいくと社会人になるまでには全て終わらせることは難しそうですが、1回の時間もかからないので仕事帰りでも無理なく通えそうです♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">小学校の時に同級生の男子から毛深いと言われたのがショックで、ずっとカミソリと毛抜きで自己処理を続けてきました。
長年自己処理しているせいか、カミソリをしてもすぐにチクチク…。埋没した毛もたくさんできて、最悪の状態に。。。
すがるような気持ちでカウンセリングを受けました。
カウンセリング時に私の今までの自己処理方法や今の状態をスタッフの方に伝えると、そうなってしまう理由や改善方法を優しく丁寧に教えてくれました。
ヒジ上にも結構むだ毛があったので、金額的にも腕全セットの方がお得だったため脱毛をすることに。
ずっとコンプレックスだった腕が今では見違えるようにキレイになりました！自己処理の面倒くささからも解放され大満足です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">デスクワークの仕事でいつもパソコンと向かい合う日々。肩は凝るし背中も曲がってきちゃうし、腰も痛くなるのでマッサージ師の彼によくほぐしてもらっています...。
全然知らない人にマッサージしてもらうのとは違い、やっぱり彼だと背中の毛が気になってしまうので、、、背中全体の箇所を選んで脱毛を始めました。特に背中の下は背中全体的に見ると真ん中あたりで、視線が行きやすいので。何回か脱毛を終えてからサロンのスタッフさんに「毛が薄くなってきていると思いますよ」と言われ嬉しくなりました！心置きなく彼のマッサージを受けることもできて、彼からも「背中なんかきれいになったね」と言われとても満足しています（＾＾）</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">背中は鏡で見えづらい箇所なので、気にしていなかったのですが、年齢を重ねていく度に背中のニキビが増えたのと、毛が濃くなっていることに気付きはじめました。ホルモンバランスのせい？と悩み始めていたのですが、自分では手の届かない箇所なのでどうすることも出来ず、ネットで色々調べて銀座カラーさんへ行ってみることに。
背中の上だけやろうと思っていたのですが、上だけキレイになるのも変だと思って、背中の下も一緒にやることにしました。
施術では背中に冷たいジェルを塗られて、最初はかなり寒い！でもそれも最初だけなので、キレイになることを考えて我慢我慢！
続けていくと次第にニキビも無くなってきたので、毛穴から菌が入ることも減ったようです。
今では気になっていた背中の毛がキレイに無くなり、背中が開いた洋服や、水着を堂々と着れることが一番嬉しいです♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">他人を見ていて、ふとした時（物を拾おうと思って屈んだ時とか）に意外と見えてるんだな・・・と気づいた部分が腰の上というか背中の下あたりでした。鏡で見てもほんと見えない部分なんですよね。本当に見えないからスマホのカメラでパシャリとしてみたら、わ～意外にぽつぽつ生えてる～(；_；)　でもこんな部分って脱毛の対象箇所に入ってるのかな？と思ってネットで検索。丁寧な説明を見つけたのが銀座カラーでした。次の日に早速ネット予約をして、カウンセリングを後日受けに行きました。どういった方法で進めていくのかや、費用面など分かりやすくお話してくれたので安心して契約書にサインをしてきました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">背中なんて、誰にも見せない部分だしって気持ちでノーマークな人生を歩んできました。触ると意外に生えてることに気づいたんです。海とかも小学生以来行っていなくてほんとに油断してたのですが大学時代、サークルで海に行こうってなった時がありました。仲間はずれになるのは嫌だ！でも背中に毛が生えている今のままじゃ行けない！ってことで広告で見た銀座カラーへ直接聞きに行こうと思いお店に行って色々相談にのってもらいました。最適な方法やコースをちゃんとこちらの立場になって考えてくれてすごくよかったです。おかげさまで海で堂々と服を脱いで水着になれそうです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">もみあげ周りの産毛が濃いので、ずっとコンプレックスを抱えていました。<br>
いつももみあげを隠すことばかりで、髪の毛を結ぶことはなく、髪型はショートボブ。<br>
大学に入ってからイメチェンしたいと思い、脱毛することを決意したんです。<br>
サロンを探し始めると、銀座カラーにはもみあげの脱毛があるので、嬉しくなってすぐ問い合わせてみました。金額も思っていたほど高くなく、大学生の私でも通えるほどでした。<br>
脱毛は初めての経験だったので、1回目はすごく緊張しましたが、とても親切に対応して頂いたので、その後はリラックスして施術を受けることができました♪ 今ではもみあげ周りのうぶ毛がなくなり、綺麗に整いました。<br>
あれほどコンプレックスだったおだんごヘアやアップスタイルも、毎日楽しめています!</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 サービス業<br></span>
            </dt>
            <dd itemprop="reviewBody">私のもみあげは、すごく濃いというわけではないのですが、産毛が頬の方まで広範囲に生えているので気になります。<br>
飲食店に勤務しているため、仕事中は髪の毛を必ず結び、もみあげ部分は丸見えに…。定期的に自己処理していますが、うっかり忘れてしまうこともあるので、恥ずかしい経験もしました。<br>
もともと顔の脱毛に興味があったのでサロンを調べていたら、銀座カラーではもみあげも脱毛できると知ったので、顔と一緒に脱毛を始めました。<br>
最近では、顔ともみあげ周りの産毛がなくなって、横顔に自信が持てるようになりましたよ!<br>
産毛がなくなったことで、ファンデーションのノリもよくなり、メイクも楽しめています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">髪を耳にかけることが多く、もみあげ部分の産毛が濃いので気にしていました。<br>
他のサロンで、ワキの脱毛に通っていましたが、銀座カラーでもみあげの脱毛を見つけたので、乗り換えちゃいました!<br>
少しずつもみあげ周りの産毛がすっきりしてきて、触った感触も変わってきました。<br>
横顔って自分では見逃しがちだけど、周りの人からは結構見られているので、変化を友達に気づいてもらえた時がすごく嬉しいです!<br>
あと彼氏に触れられた時に、触り心地がいいって褒められたので、とっても幸せです。<br>
これからも定期的に通って、綺麗な状態をキープしたいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">顔全体の産毛が濃くて、化粧をしてもうっすらわかってしまうほど。自己処理をし続けていたら毛穴まで開いてきてブツブツお肌になってしまいました…。もちろんそれだけの理由ではないと思うのですが、肌も敏感肌なので、もう自分では対処法が見つけられなくなっていました。<br>
顔脱毛を検索していたら、銀座カラーは脱毛だけじゃなくて、オプションの保湿ケアがすごい効果がある、と書いてあったので、顔ともみあげをセットでお願いしました。<br>
口コミで書いてあった通り、回数を重ねる毎にお肌がキレイになっていきました！毛穴も気持ち小さくなっている気がします。 <br>
頬からもみあげにかけても、産毛がなくなり綺麗に整えられてきたので、今はアップの髪型をした時のサイドラインがお気に入りです!<br>
アップスタイルがより綺麗になるように、次はうなじの脱毛も始めます。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">わたしの手の甲は親譲りなのか毛深くて毛穴が目立ち、お世辞にも女性らしいきめ細やかな手とは言えません…。小学生の頃から友だちの毛穴のない白い手をとても羨ましく思っていました。季節問わずいつも見えている部分なので、毎日の自己処理では何も改善しないんじゃないか？と思い、脱毛を始めました。
カウンセリングのときにスタッフさんに相談したところ「脱毛をしていくうちに毛は薄くなっていき、そのうち自己処理のいらない肌になりますよ」と言われとても希望が持てました。3回ほど通い、確かに自己処理の回数が減ってきたことを実感しています。脱毛の前後に保湿ケアを行う事で脱毛時の痛みを抑え、その上美肌効果もでるそうなので、今から仕上がりが楽しみです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">長年お付き合いした彼氏からプロポーズを受け結婚をすることになりました。結婚指輪を探している時に店内のきらびやかな照明の光で指の毛が確実に目立って恥ずかしかったのです。指輪をはめてもらう本番のその時までにはなんとかきれいな指にしたいと思って、ブライダルエステの他に脱毛もすることにしました。エステサロンに近い脱毛サロンを探し、銀座カラーに通い始めました。ご結婚されているスタッフさんに担当して頂いて施術以外でも色々アドバイスをもらったりし、ためになる時間を過ごすことができました。先日無事に結婚式を迎え、綺麗さっぱりな指に彼から指輪をはめてもらいました。エステに脱毛にちょっと出費してしまいましたが満足です。脱毛はまだまだ1年くらい残っているのですが、予約は比較的とりやすいので順調に脱毛できそうです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">看護師をしているので患者さんと接近することが多く、手元には気をつかっていましたが脱毛までは考えていませんでした。
仲間の看護師が患者さんに手がキレイだと褒められていたので、後日その話になった時に手を見せてもらったらツルツルで驚きました。
彼女は既に全身脱毛をしているようで、時間はかかるがお手入れの必要がなくなり最高だよ、と勧められました。
とは言ってもお金も時間もかかることなので、まずは手で試してみることにしました。
勤務先の近くのお店、という条件から銀座カラーさんにしました。店内もスタッフの方も感じが良く落ち着くお店です。
手の甲と指だけなのであっという間に終わります。思っていたより痛くないし、今度は腕全セットに挑戦しようかな？と思っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">手には自信がありました。指も長く細くてスラリとしてますし、自分でハンドマッサージも欠かさずしていました。将来は手のタレントさんになりたいな、なんて思っていたこともあります。でも、、、毛深いんです。手の甲と指になかなかの太さの毛が生えていました。
「腕などに比べてみんな大して見ていないでしょ」なんて思っていたのでカミソリで処理していましたが、ある日、彼氏から「指の毛生えてる！」と言われ恥ずかしくてショックでした。すぐにカウンセリング予約をしました。
選択したコースはフリーチョイスです。金額的にも手頃だったので。脱毛範囲が狭いので実際の施術時間も10分程度ですぐに終わりました。
まだ数回しか通っていませんが、通う前と今では確実に毛が薄くなってきています。
今は他の箇所もセットですれば良かったなぁ、なんてちょっと後悔しています。手の甲が終わったら足の甲もお願いしようかな？と思っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">わたしは昔から裸足でいるのが好きでした。そのくせキチンとケアもせずお手入れもしていなかったので、お出かけの時にサンダルを履いた自分の足をみてがっかり…。毛は生えてるわ乾燥しているわで、自分のケア不足を痛感しました。早速毛の処理をする為、銀座カラーさんへ行きました。
足全セットにしたのですが太ももや足下に比べ足の指の毛は少なめだったので、2～3回通うと毛がほとんど気にならなくなりました。銀座カラーのスタッフはカウンセリング時の説明もマニュアルっぽくなく、ちゃんと会話しながら伝えてくれてとても好印象でした。お手入れし忘れていてももう気にならないレベルなので、いつでもお出かけ用のサンダルを履いていけます♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">きっかけは彼からの一言でした。それまで気にしたことなかったのですが「女の人も足の甲に毛はえるんだね」と言われ、すごく恥ずかしかったです。それ以来気になって仕方なくなりカミソリで剃っていたのですが関節などは剃りにくく、何度か切ってしまったことがあります。そんなに広い箇所でもないので自己処理でも良かったのですが、正直小さい範囲だからこそ面倒くさいんですよね。当時はVIPハーフを利用していて、ひざ下の脱毛はあと2回くらいで終わりそうだったので、フリーチョイスで足の甲も追加ですることにしました。スタッフの方も臨機応変に対応してくださってとても居心地がいいので、金額だけで他のサロンにしようとは思わなかったですね。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">この前、爪を切ってる時に足の甲に強そうな毛が数本生えていることに気づいたんです。いつの間に生えたんだろうと思うくらい唐突に見つけてしまいすぐ抜いてしまいました。抜いて数週間経つとまた生えてきてそれをまた抜く。繰り返していたら範囲が広がってきている気が…。抜くより脱毛しちゃったほうがいいかな、と脱毛シートを買ってきて、ベリッとやってみたら真っ赤になってひどい状態になりました。サロンにお世話になるとどのくらいお金がかかるんだろう？と思い調べたら足の甲はたいしてかからないようなので、銀座カラーのフリーチョイスコースを選びました。時間も数十分で終わるので、3ヶ月に1度仕事帰りに行くように予約を取り通いました。毛は周期があるので時間はかかりますが、確実に毛がなくなったので満足です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">恋人が、すごく細かい人で色々なところを気づくんです。この前は、足の指まで「あ、毛穴黒いね・・・」なんて言ってくるんで凄くショックで。なので、見返してやろう！と思い立ってネットでの評判を最優先にサロンを探して出てきたのが銀座カラーでした。名前からしてちょっと高級な感じがするので金額もすごい高いのかな・・・？と思いきや、リーズナブルでそれでいて丁寧かつ親切な接客でした。私も仕事は接客業なので色々勉強になる部分もあり、プラスになりました。そこまで太い毛が生えていたわけではないので、パチッと瞬間痛みがくるだけでほとんど痛みは感じませんでした。足の指と甲もすっきり綺麗になったのでサンダルを履きまくろうと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">私は脱毛には全然興味がなかったんですが、友達と話してると結構みんな脱毛をしていることが分かったんです。カミソリでジョリジョリ処理すれば十分だしって思ってたんですが、話を聞いてるうちにだんだん興味を持つようになりました。子供がいる友達から、子供できたらただでさえ時間がなくなるから子供がいない間にやってた方がいいよ！と言われて、行くなら今しかない！と思って通うことにしました。 友達にも色々と相談し、夏の時期に気になってたワキとおへそ周りにしました。想像してたより少し痛くて、ビクッてなっちゃいますが我慢できないほどではないので耐えられます。完全に毛が生えなくなるまでまだ時間がかかりそうですが、手をあげてもワキが気にならないようになるんだな～とかもうカミソリで剃らなくてよくなるんだ！と思うと頑張って通おうと思えます。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">これまで家庭用の脱毛器を使ってたんですが、あまり高いものじゃないからか効果が感じられず、結局カミソリや脱毛クリームで処理してました。もっと高い脱毛器を購入しようかと悩んだんですが、結局手間もお金もかかるしな～と思ってサロンへ通う事にしました（＞。＜）
たくさんのサロンがキャンペーンをやっているので、面倒だったのですが3店舗ほどカウンセリング受けました。スタッフの感じやお店の雰囲気が自分に合いそうだったので銀座カラーに決めました。
サロンではプロの方にお任せするだけでいいので本当に楽でした。特にワキは毎日のように自分で処理していたのでそれがなくなって本当に楽ちんです。脱毛器を使っての自己処理だと、手が届かないところが自分ではちゃんとしてるつもりでもきれいにできてなかったんだなー、と脱毛後の肌をみて思いました。あの時高い脱毛器を買わず、ちょっと頑張ってサロンに通って正解だったなと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛には前から興味があったんですが、値段が高いのと、友達から結構痛いよって聞いてて怖くて勇気が出せませんでした。この前海外旅行に行った時に、水着を着るたびにカミソリで毛の処理をしてたんですが、とても面倒くさい上に剃り残しがあったりして恥ずかしい思いをしたんです。それをきっかけに、旅行だけじゃなく普段の生活も脱毛した方がやっぱり楽だよなーー！って思い、脱毛サロンへ通い始めました。エステとかサロンとかあまり行ったことがなかったので最初はとても緊張してたんですが、サロンスタッフの方が丁寧に説明してくれて納得できるコースを提案してくれました。私は両ワキとVラインのコースに通ってますが、思ってたより痛くないし少しずつ毛も薄くなってきた気がします。頑張って通ってツルツルになりたいです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">学割制度があったので、思い切って両ワキの脱毛をお願いしました。最初から何も生えていなかったのかと思うくらいすごくキレイになって大満足です！！もっと早くやりたかったです。
今まではワキは自己処理でやっていたのですが、数日たつとすぐチクチクしてしまうし、たまにカミソリでキズをつけてしまったりして面倒だったのが、何もしなくてよくなり本当にラクです。お風呂で体を洗うときもツルツルしたワキが嬉しくて、ついつい触ってしまいます。
それに、朝、ワキのことを気にしないで着る服を決められるので、ノースリーブやキャミワンピを選ぶ回数が増えました！実は、母にも「脱毛って昔はもっと高かったのよー」とうらやましがられています（笑）
今年の夏は、水着を着るのがとっても楽しみです♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">小さい頃から肌が弱く、季節の変わり目や湿度の変化で肌が痒くなります。
VIOラインも例外ではなく、梅雨時期は蒸れて痒くてたまりません。
でも人前で容易に手でかける場所ではないので、本当に困ってました。
毛深い方なのでVラインの毛も結構濃いのが原因の1つでもあるのかも？と、思い切ってVIO脱毛に挑戦しました。
せっかくなのでワキの毛も一緒に脱毛してもらいました（ワキも痒くなるので…）
肌が弱いので大丈夫かな？と心配していましたが、保湿ケアの効果からか、施術後も痒みやかぶれも出ませんでしたし、黒い太い毛が1,2週間後にポロッととれるのがたまらなく気持ちよいです。
毛が少なくなるにつれてあの猛烈な痒みが起こる回数が減り、本当に嬉しい！とにかく嬉しい！勇気を出してVIO脱毛して良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">今まであまり気にしたことはありませんでしたが、大学のサークルで海に行った時、サークル内の女の子達の脱毛ケア率の高さに驚きました！みんなVラインの脱毛もしっかりしていて、ビキニもキレイに着こなしていて…。帰ってきてからすぐに友達に勧められた銀座カラーへカウンセリングの予約をしました。
全てが初めての事なので、カウンセリングから緊張しまくり(-_-;)　でもサロンの方がとても優しかったし、どのコースで申し込むか一緒に考えてくれたので、無事脱毛をスタートできました。
初めて脱毛する時はどれだけ痛いのだろう…とびくびくしていましたが、実際は大して痛くもなかったです。でも、やっぱりちょっと恥ずかしかったかな…(笑)</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">ストレス解消目的で水泳を続けています。肌の露出が多いので、ワキとVラインの脱毛を考えていましたが、元々あまり毛深い方でも無いし、Vラインなどは自分以外の人に処理を頼むのも気恥ずかしい事もあり、その都度自分で処理すればいいや…とやり過ごしてました。しかし仕事やプライベートが忙しくなってきてゆっくり処理する時間も持てず、さすがにこのままでは…と一念発起。それでも初めての事だし、恥ずかしさや痛かったら？など不安ばかり。。。でもスタッフの皆さんが親身になって話を聞いてくれたので、リラックスした状態で施術を受ける事が出来ました。デリケートな部分ですし少し躊躇してましたが、自分で処理をするより衛生的で安心感があるように思いました。これからの季節、水着を着る機会も増えるし楽しみです♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">友達が銀座カラーさんでVライン脱毛をして、すごくよかったと勧められたので利用しました。
Vラインなんてそんな露出するような場所じゃないし、エステティシャンの人に見られるのも恥ずかしいな…と乗り気じゃなかったんですが、彼氏ができたのでそうも言っていられなくなりました、ピーンチ（笑）
最初は少し緊張しましたが、友達から聞いたとおり店内がとてもおしゃれで、スタッフもみなさん気さくで話しやすかったので安心できました。施術について説明もしっかりしてくれて、あっというまに終わったので、恥ずかしさを感じるヒマもありませんでした。予想より痛みも少なかったし、保湿ケアのミストシャワーが気持ちよくて大好きになりました。 Vラインはなかなか自分ではお手入れできない部分だし、彼氏に喜んでもらえたのが何よりうれしいです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">今年は念願の初ハワイ！水着になったり、短めなショートパンツで街を歩いたり、今から楽しみなんですが、むだ毛があっては楽しさ半減。他の箇所はだいたい脱毛しているんですけど、太ももというかひざ上はまだ手つかず。この機会に全部やってしまおう！と、そのまま通っていた銀座カラーの仲の良い店員さんに相談してやってもらうことにしました。個人的には銀座カラーさんでの脱毛は痛いのを全然感じないので、今回もすごく楽にできました。すべすべして気持ちいいので、ぼーっとしてる時につい触っちゃいます。店員さんにも「良い仕上がりです」と褒められました！私の毛の周期は3ヶ月毎に次の予約を入れる位がちょうど良かったので、美容院みたいな感覚で通っていました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">わたしはニーハイが大好きなのでかなり履いてます。悩みなのがわたしの脚はもともと毛穴が目立つんですよね。だから可愛いニーハイを履いても太ももの見えてる部分は毛穴が目立って可愛くない！もちろん毛もまあまあ濃くて、ニーハイを履くごとに毛の処理をしてると肌がブツブツになりかねないかも…。不安になったのでいっそのこと脱毛してみようと思ったのでした。ひざ上だけなら脱毛時間も20分ぐらいでさっと終了するのでバイト前に入れても全然問題ないです。太ももの後ろ側って自分で剃るとき結構見えないんで剃り残しもあったと思うんですが、今ではお手入れいらずまで毛が薄くなりました。毛穴もちょっと目立たなくなったみたいで満足です！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">息子がひざに座ってくるのですが、ある日ショートパンツだった私に突然「ママの足ザラザラする！痛～い。」と大きな声で言ってきました。子供にそういう指摘をされたことがなく（本人的には、素直に思ったことを言っただけだと思いますが）とてもショックな出来事でした。夫にそういうことがあったという報告をしたら「脱毛すればいいんじゃない？」と嬉しい一言。
早速銀座カラーにカウンセリングに行きました。前にVゾーンの脱毛で通っていたことがあるので特に大きな不安もなく始めました。久々でしたがさらっと終わりました。1年後の今は息子も「ママの足、ツルツルしてるねー」と言ってくれるようになりました。育児と仕事の合間の自分メンテナンス、脱毛もできたしストレスからも解放されて最高でした！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">彼氏がなんと・・・体毛が全然ない人なんです。「オレより濃いよね」なんてデリカシーない一言に落ち込んでいました。これはもう脱毛サロンに通ってそんなこと言われない体になってやる！と決心。決めたのが銀座カラー。色々探したんですが銀座カラーを見つける前にチェックしたサロンは予約が取りにくいとか色々マイナスポイントをたくさん聞いて、金額よりも質と時間をムダにしない有効的な感じで通える銀座カラーに決めました。足全セットでお願いしました。最初はちょっと痛みを感じてしまい我慢してたところ、遠慮無く不安なこと言ってくださいね、といってくれる店員さんの言葉がとにかく優しくて。いたわってもらってると感じることが多かったです。緊張していたから敏感になっていただけ？このまま他の箇所も全部やってしまおうかとな～？と考え中です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">電車に乗ったときに隣の女性の二の腕の肌荒れにドン引き。。自己処理失敗したなーこの人、なんて。ケアって大事だなーと思いましたが、実は私も大したケアはしてなくて、自己処理で荒れた肌を服の袖でごまかしていました。元々毛深いほうだからよく見るとバレるので着る服も限られてしまい、思い切ったイメチェンがなかなか出来ず。。トライアルコースもあるので軽い気持ちで銀座カラーさんにお願いすることにしました。 キレイに脱毛できましたし、いちばんの決め手になった保湿もエステのようなお肌のケアで、想像以上に文字通りツルツルスベスベになることができ大満足でした！二の腕のブツブツも気がついたら無くなっていましたよ。友達にも同じ悩みを持っている子がいるので思わずその子にもおすすめしちゃいました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">わたしは結構人目を気にするタイプで毛の処理も毎回丁寧にやっています。毛の処理を始めたのは物心ついた小学生のころからで、大学生の今となっても変わらずカミソリで剃っていました。何年も剃り続けているとさすがに濃くなるわたしのムダ毛…でも夏場のお手入れは欠かせないし。 友だちと濃くなるムダ毛について話をしたところ、なんと友だちはもう銀座カラーに行っていました。友だちの腕を見せてもらいそのさらさら具合に感動し、友だち紹介でわたしも脱毛デビュー！カウンセリングでは緊張していましたが、スタッフの皆さんが感じが良いので、すぐにリラックスできました。特にヒジ上部分なんかは大学の講義中に後ろの席の人に見られるんじゃないかとあまり堂々としていられなかったのですが、もうそんな心配も必要なし！毛を剃る回数も減って夏も楽チンです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">ヒジ上はカミソリで優しく産毛を剃ってごまかしごまかし、自己処理してきました。でも肌が白くて薄いせいか、すぐ赤くなってしまうんです。そんな産毛もだんだん濃くなってきて、今後どうなるんだろ！？とすごく怖くなってきてしまいました。
手遅れになる前にちゃんとしたところで脱毛してもらったほうが良いよ！という友達の勧めもあり、５つくらい候補を挙げて自分にあいそうなサロンを最終的に決めました。それが銀座カラーさんでした。名前が銀座なので敷居がすごく高そうに思っていましたがそんなことはなくて、通ってる方も若い人がとても多くて親近感！。脱毛だけじゃなくて肌のこともアドバイスしてくれるので、脱毛中のエステティシャンとのお話も含め楽しく通っています♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">なんか、おかしなところだけ毛が濃いんです。それはヒジの上辺りから肩にかけて。そこが濃い人なんてそんなにいないと思うのに黒い毛穴がすごく目立つしあんまり半袖にもなりたくなくて毎年、夏が怖かったです。親に相談したら「遺伝かもねー。少し費用負担してあげるから脱毛サロン行ってみたら？」と言ってくれたので通うことにしました。学校に向かう途中に銀座カラーがあり高級感がありそうな店構えだったので前から気になってました。値段も他のサロンと比べてみてリーズナブルだったのと、予約も取りやすいなど他の要素も考慮して通うことに決めました。不安とか全く最初からなかったので、友達に会いに行く感覚で施術を受けに行きました。楽しかったです＾＾</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛って、始めると次から次へとやってしまうんですよね。ハマっちゃうかんじ。一部が綺麗になってくると、次はココをしたい！そして次は！てなっちゃうんです。へそ下はそんなに気にしてなくて、うすーく産毛がある程度だったので、脱毛範囲に入れていなかったのですが担当の方の勧めもあり、せっかくなので全身完璧にしてサロンを卒業したい！と思って、へそ下もお願いすることにしました。ほかの箇所に比べて個人的な感想は痛みが一番少なかったような気がしました。ただ、うぶ毛多めなので結構時間はかかっちゃいましたね。ベルトやゴムの締め付けでできた腰周りの色素沈着も保湿ケアのおかげで薄くなり一石二鳥！もう、ほとんどムダ毛がなくなってきててツルッツル♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">今までは友達と海に行ってばかりだったのでおへそ周りなんて特に気を使っていませんでした。でも彼氏と海に行くなら色気あるビキニを着て行きたい！その為にはむだ毛はお手入れしておかなければ。。。
元々おへそ周りは自己処理をしてましたが、お腹の場合、柔らかいお肉のせいで結構剃りづらいと感じていました。特にへそ下は剃れば剃るほど毛が密集してきて濃くなっているような気がしました。脱毛してもらってからは毛が薄くなり、処理の回数もぐんっと減りました。やっぱりプロの技術は凄いです。へそのむだ毛はうぶ毛と濃い毛が混じっているので、最初太い毛はパチッとするかもしれませんよ、と言われましたが全く気にならなかったです。ビキニOK！な仕上がりで彼氏にも喜んでもらえました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">ふと、気づくときってあるんですよね。ひゃーー！みつけちゃった！！みたいな。。。自分の身体の部分のおかしい所に。まじまじとお腹なんて見ないので、ふとした時に産毛って結構生えてるんだなぁ・・・と見つけてしまった事がありました。
気づいてしまうとホント気になる！気になると触れてしまって、それが更に刺激を与えて生えちゃうかも！って負のスパイラルに陥ってしまって。そんな小さいことで悩むなら脱毛サロンに通おうと思い、帰りに銀座カラーに行きました。カウンセリングではとても丁寧に説明してもらって、その場で契約してきて今通っています。私のオススメは保湿の時のミストシャワー。すっごい気持ちよくてお肌もぷるぷるになりました！ 気になるむだ毛がだんだんなくなって来たので、次はデリケートゾーンもしてみようかな？</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">去年ダイエットに成功し、好きなモデルさんが時々やっているへそ出しファッションをやってみたくトップスを購入しました。が、実際着てみるとあらわになるむだ毛のオンパレード。せっかく洋服を買ったのにまったく無理だ・・・と思って友達が通い始めた銀座カラーにネット予約。来店してカウンセリングを受けることを勧められ若干抵抗がありましたがお店に行ってみました。素人の自分でもわかりやすく説明をしてくれて、アドバイスをくれる友達感覚というかそんな親近感もありました。学割もきくということなのでそれも利用して脱毛ライフを始めました！脱毛中はスタッフの方とお話ししながらできるので楽しいし、あっという間に1時間たっちゃいます。今4回目ですが、へそ下の縦ラインはほぼわからなくなりました！</dd>
          </dl>

 <!--<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">ベリーダンスが趣味で毎週スクールに通っているのですが、衣装がいつもヘソ出しなので、おざなりにしていたお腹のムダ毛をそろそろ退治したくて脱毛することにしました。
そこまで濃いへそ毛ではないのですが、日本人の肌は白く、やっぱり黒いむだ毛は美しくないので…。
スクールが新宿にあるので、近くの新宿南口店にお世話になることにしました。
今回は単純におへそ周りだけお願いしたので、実際の脱毛時間は着替え込みで30～40分程度で終わりました。痛みですが、私は全く気になりませんでした。それより一回で照射できる範囲が広いので時間が短くていいですね。あまりたくさんの時間やらなければいけないと思うと足が遠のきますから。 まだ2回目なのであまり変化を感じませんが、スタッフさんが言っていたようにスルッと毛が抜けているので、ムダ毛は少なくなっていると思います。</dd>
</dl>-->

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">まばらに太い毛が生えているおへそ周り。毛抜きで抜くと皮膚がぷくっと盛り上がった感じになったり、カミソリで剃ると今までうぶ毛だった毛まで濃くなった気がするし。自己処理の限界を感じて銀座カラーへ駆け込みました。おへそ周りって普段は見えない部分ですが、水着の時は絶対NGですよね。丈の短いトップスが流行った時も、自己処理するのを忘れてて本当に恥ずかしかった思い出は忘れることができません。おへそ周りの毛のことを無料カウンセリングで相談するのは恥ずかしいなと思ったのですが、カウンセリングをしてくれた方はさすがに慣れていらっしゃるようで、水着を着る時期になるとおへそ周りの毛やお腹の毛が気になって脱毛をしに来られる方も結構いますよと言われてホッとしました。実際に脱毛をして、丈の短いトップスや水着になる不安や心配がなくなったので、おへそ周りを脱毛して良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">おへその周りを取り囲むように毛！毛！毛！。そしてVラインに向かって縦ラインのむだ毛。スタイルは比較的悪くないのですが（自分で言うな）、トップスからチラッとお腹が見えてしまうこともあるので、いつもカミソリで自己処理していました。産毛よりは濃いけど腕の毛に比べれば薄いという程度のむだ毛ですが、最近徐々に濃くなってきている気がして怖くなり、脱毛をすることにしました。
クリニック系に行くか迷ったのですが、狭い箇所なので時間がかかりそうだと思い、短い時間で対応してくれそうな銀座カラーを選び、VIPハーフコースでお腹周りを中心にお願いしました。まだ6回全部は終わってないですが、なかなか効果がでてます。 おへそ周りのとぐろのような毛は薄くなってきましたし。最近はお腹がチラ見えしてしまっても、どうぞ見てください！って気持ちです。こんなに自信が持てるなんて、脱毛に感謝！かな？（＾－＾）</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">夏休みに海やプールに行きたくてダイエットと脱毛(VIO)を始めています。いい感じに準備が整って来た～と思ったら、あれ、おへそ周りの毛が。今通っているサロンで追加しようと思ったら予約がいっぱいで3ヶ月以上待ち。ネットで探していたところ銀座カラーのホームページにたどり着きました。電車の中吊りが印象的で気になっていたものの、こんなに安くて脱毛できるの？と思っていましたが本当でした！スタッフのみなさんも相談しやすい雰囲気で安心できます。機械が優れているのか脱毛はすぐ終わるし、保湿もしっかりしてもらえるので嬉しいです。おへそ周りも段々とキレイになってきています。はじめから銀座カラーにすれば良かった～。予約も取りやすいのでVIOよりもおへそ周りの方が早くおわっちゃいそうです。妹や友達にも勧めています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">いつもひざ下の毛を剃る時に、ひざの凹凸でひざ周りの毛が剃りにくく、剃り残しがあったり、ひざの皮膚を切ってしまって痛い思いをしたりすることがよくありました。もう、ひざ周りの毛には長年悩まされっぱなしでした。今回ひざ下の脱毛をしようと思った時に、ひざ周りも一緒にすることを勧められ、そうそう、ひざ周りがやっかいなのよね…と思い、一緒に契約しました。通ううちに、毛がなくなるだけではなく、ひざの黒ずみがどんどん美白になっていくのがわかりました。ひざ周りがこんなに綺麗になるとは想像していなかったので、なんだか得した気分です。人に見せたくなるような美脚になったので、流行りのスカートをたくさんはこうと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">春になったらスカートをたくさん履きたいので、冬のうちから脱毛を始めようと思いました。足、特にひざはカーブをしていて骨っぽいし、カミソリで剃るのが恐い場所だったので悩みの種でした。怪我とか怖いし（；。；）そしてひざだけするのは境目が気になりそうだったので、足全で一気に足を脱毛して綺麗にしたいなと思い足全セットにしました。足全体を脱毛して綺麗になったので、スカートやショートパンツをどんどん履きたいと思うようになりました。エステティシャンの方も言っていましたが、ひざはやはり足の中でも一番自己処理をするのが危ないらしく、傷つけてしまう方もたくさんいるそうです。しかもひざは、色々な摩擦によって角質化していることも多いらしく、保湿ケアが何よりも大事だそう。なので保湿ケアを重視している銀座カラーにしてとても良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">ひざ下は以前に脱毛していました。その時はひざもやってもらえるものだと思っていたのですが実際は別料金だったのでやめていました。
でも私は色白で毛深いので、ひざの毛も結構太く目立っていて自己処理をしていました。毛穴の黒さがどうしても嫌で、太ももも含めもう一度脱毛サロンへ行くことにしました。
以前通っていたサロンと違って、とても落ち着いた雰囲気だったのでちょっと場違いかな？年齢層高め？と思ったのですが、カウンセリングをしてくれたスタッフの方は私とそう変わらない年齢っぽかったのでホッとしました。ひざは脂肪があまりないので少し骨に響くというか痛みはありましたが、すぐ慣れました。4回ほどでひざ下とひざの境目がわからなくなってきたました。やっぱり自己処理とは全然違いますね～。残るは足の甲のみ。サンダルの季節までにはチャレンジします！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">ひざの毛穴が目立つことに悩んでいたので、今回チャレンジしました。
ひざは自己処理をしても剃り残しがあったり、毛穴がポツポツ黒ずんで見えるのが悩みでした…。でも、脱毛していくうちに毛が少なくなってくると、毛穴も目立たなくなるのを実感！ザラザラした触り心地だったのもツルッツルッに♪ひざを出すことにためらいがなくなり、好きなファッションを楽しんでいます。
あと、脱毛によりムダ毛がスルッと抜けるんですが、体育座りでその毛を抜くのがめちゃめちゃ楽しいです。あ！ここもう生えてこないぞっ！お！ここもだ！！なんて思いながらついつい探しちゃいました。 来年から就活も始まるので、気になる部分を早めにやっておいて正解でした。毛の周期に合わせて脱毛していくので、結構時間はがかかるため、時間の自由がきく学生のうちにやっておくのがオススメです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">はじめはVIOって何？と思いましたが、周りの友達は皆知ってて、数人は脱毛経験していました。
デリケートゾーンの毛を脱毛するなんて想像もしていませんでしたが、「オシャレしたいから毛は全部なくしたい」と友達が話しているのを聞き触発され、お友達紹介キャンペーンを利用して一緒に通うことにしました。脱毛自体初めてなのにいきなりのVIO脱毛。ドキドキしながらワンピースに着替えました。スタッフさんは手際よく準備を進めていましたが、私がかなり緊張しているのが見て取れたのか、優しく話しかけてくれました。とっっってもやさしかったです。バチッ！という音にビクついていましたが、10分もすればこんな感じなのかーと慣れました。Vの形については特に希望もなかったので全体的に毛が生えている範囲を小さく整えてもらいました。 まだまだ始めたばかりですが大人の女性に1歩前進した気がします。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">水着を着るのにVラインの脱毛をしたいなと思い無料カウンセリングへ行きました。話を聞いているうちに、どうせやるならVIOを一度に脱毛した方が後々楽だな～と思ったので、ヒップ奥（いわゆるOライン）の脱毛をしようと思いました。ヒップ奥は自分では見えないけど、触ると毛があるのはわかっていたし（衝撃）、自己処理できない場所なので脱毛して本当に良かったです。水着やTバックを着る時に全く気にしなくて良くなったので本当に楽です。Vラインを脱毛するなら、ヒップ奥までした方がその境目もわからなくなるのでいいと思いますよー。痛みに関しても、他と比べる事ができないのですが、きちんと6回通えたので、そこまで気になりませんでした。これでブラジリアンビキニも安心して着られます。水着の時にVIOラインを気にしている友人にも教えて紹介してあげよっかなと。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">普段目につかない部分なので完全に自己満足となりますが、どうしてもVIOの毛をなくしたくてカウンセリングを予約させていただきました。
もちろんデリケートな部分なので抵抗はありましたが、カウンセリングの時にしっかりと脱毛の方法について教えていただけました。
腕などと比べて範囲は狭いので痛さなどもさほど気にならなく、というか色々考える前にすぐ終わってしまいました。
やっぱり自己処理が難しい部分だったし、チクチクしなくなったし、普段の生活でいちいち自分でする必要がなくなったのは嬉しく思います。ただやっぱり他人にOラインを見られるのは少し恥ずかしいものですね。
</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">とても恥ずかしいのですが、肛門まわりに結構多めの毛が生えていました。お風呂に入ったときに興味本位で見てみて衝撃！
自己処理しようとしたのですがうまくいかず、数日経つとチクチクしてたまりませんでした。
はじめVライン脱毛を考えていましたが、VIOセットコースに変更。どうせなら全部なくそう、と決めました。 が、1回目はとにかく恥ずかしくて無言…（恥）。エステティシャンの方は毎度のことって感じで淡々と黙々と脱毛してくれるので個人的には楽です。自分で事前処理できない場所なので、シェーバー持ってけば手伝ってくれるそうですよ＾＾</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">お尻の産毛のせいでお尻が黒ずんで見えるのが悩みで、つるっとした白いお尻に憧れて、ヒップ脱毛をしました。やっていくうちに、ビキニラインからお尻の毛がけっこうはみ出て（汗）いることや、産毛だけでなくたまに濃い毛もあったことに気づきました。やり始めるといろいろ気になってしまいますね。自分ではうまく処理できないので、プロに頼んでよかったと思います。銀座カラー独自の保湿ケアのおかげで、脱毛後のお肌はなめらかでつるつるになりました。肌のザラザラ感もすっかりなくなり、お風呂でお尻を洗うときのつるつる感がたまりません。想像以上の綺麗なお尻になって感動です。次に買うビキニは、お尻が綺麗に見えるデザインにしようと思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">Tバックを履きたいなと思った時にヒップの毛が気になり始めました。生えてるんじゃない！？と。決して毛深い方ではないのですが、お風呂やプールなどに入って濡れた時にはやはり気になっていたので、脱毛をすればもう気にしなくていいし、やりたいなと思いました。ヒップは自分ではよく見えない部分なので自己処理もできないし、プロにお任せした方が安心かなーて。以前こちらでVIOセットをした事があったので、安心してお願いできる銀座カラーにしました。エステティシャンの方が美容の知識が豊富で、施術中の会話も美容やメイクについてなどの話題で盛り上がり、毎回あっという間に終わっていました。ヒップの脱毛をして良かったのは、うぶ毛に悩まされることがなくなった事だけでなく、肌ざわりがとても良くなった事です。滑らかという言葉が、まさにピッタリです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラーでVIO脱毛をしている時、エステティシャンの方にヒップの脱毛も結構人気があると聞き興味が出ました。Tバックを履いた時に鏡でお尻にむだ毛があることには気がついていました。見た瞬間「はっ！！！」みたいな。ヒップの下のあたりってビキニを履くと結構な確率で露出しちゃうんですよね。こうなったらVIOを完璧にしようと箇所の追加をしました。
VIOを経験しているのでヒップの脱毛の痛みなんて全く気になりません。あれ？もう終わり？って感じでした。
数回脱毛した後、なんだかヒップ下の黒ずみが薄くなってる？と思い脱毛中に聞いてみると、人によって差はあるけど、薄くなる場合があるようです。むだ毛目的で始めたのにヒップ全体が締まって美尻になり、追加脱毛大成功です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">結構友達が脱毛していることは知ってたんですが、興味はあったものの足を踏み入れる気持ちにまではなっていませんでした。最近よく電車の中やCMとかで脱毛の広告を目にする機会が多くて、やってみようかな～って気持ちになりました。広告とかではそこまで高い料金でもなかったし（高いものもあるんでしょうが）、手が届かないほどではないなと。自由にお金が使えるのは今くらいだしな、と思い、通うことにしました。
施術箇所は前から気になってたヒップにしたんですが、触り心地が全然違うんですね。びっくりしました。あと2回の施術ですが、毎回効果が感じられて満足してます。
最初はすごく恥ずかしいのと怖さがありましたが、気さくなスタッフさんのおかげで緊張もほぐれて施術を受けれるようになりました。あと2回でどんな仕上がりになるか楽しみです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">私はネイリストをしているのですが、お客様との顔の距離が近いため、顔の産毛を見られているんじゃないかと、とても気になるようになりました。ネイルサロンに来るお客様は美意識も高く、脱毛の話をする機会もたくさんありました。その中で、銀座カラーで顔の脱毛をしたという方が何人もいらしたので、私も行ってみることにしました。ちょうど職場の近くにお店があったので、仕事帰りに通うことができ、とても便利でした。顔の脱毛は、フェイシャルエステに通っているような感覚でとても気持ちがよかったです。脱毛後の保湿ミストがさらに気持ちよく、肌が潤って、翌日にはとてもつるっと滑らかな肌になるのが嬉しかったです。脱毛＋保湿ケアのおかげで、脱毛が終わった今でもつるっと赤ちゃんのような素肌になりました。本当にありがとうございました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">最近、ファンデーションを塗った時にうぶ毛が気になるように。自宅でカミソリで剃った事もあったけど、やはりちょくちょく剃らないといけないのはとても面倒…。剃るようになってから生えてきたうぶ毛が以前より濃くなった気がしてたので、脱毛をしよう！と決めました。顔という事もあって肌に悪い影響があると嫌なので無料カウンセリングの時に相談したら、保湿効果が高い美肌潤美という保湿ケアをしてくれるというお話だったので、銀座カラーに決めました。顔は鼻下を除く部分との事だったので、鼻下と一緒に脱毛をしてもらったのですが、お顔全体が1トーン明るくなり、ファンデーションを塗らなくてもいいくらいすっぴんに自信が持てるようになりました。今まででは考えられない事です。顔の印象はとても大事だと思うので、顔の脱毛をして本当に良かったと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">私は元々とても毛深くて、中学校の時は友達にもびっくりされるほどでした...。夜に自己処理しても次の日の朝にはチクチクしてて...すごく悩んでたんです。これまではカミソリはもちろん脱毛クリームなどで頑張ってきたんですが、カミソリ負けしたりすることも多く、思い切って初めて脱毛サロンに行くことにしました。 基本的にほとんどの毛が濃いので、まずは人の目につきやすい顔と手の甲・指の脱毛コースに通っています。顔の脱毛は、、、ちょっと不安でいっぱいでしたが全然そんなことなくって。これからのことを考えれば全然大丈夫です◎。サロンは清潔感もあって落ち着けるし、スタッフの方も優しくて施術中のおしゃべりも楽しくてあっという間に時間が過ぎます。ほかの箇所のことにも相談にのってもらってるので今後も頑張って通いたいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">顔の産毛が濃くて毎回剃るのが面倒になり、顔の脱毛にチャレンジしました！！！
今までにワキや脚の脱毛は経験済みでしたが、顔はたまにニキビができたりするので、脱毛で肌荒れしたりしないかな？と正直ちょっと心配でした。なので最初の無料カウンセリングの時にスタッフの方に色々質問したのですが、すごく丁寧に説明してもらえたので安心できました(^o^)_
実際、施術後にしっかり保湿ケアしてもらえたおかげで、肌トラブルは一度もありませんでした。今ではおでこも頬もツルッツルになり、顔色も明るくなったような気が...。剃っているときは乾燥肌だったのですが、最近は肌の調子も良くてお化粧ノリが違います。
学生なら学割特典もあるので、学生のうちにやっておくのをオススメします！私も次はどの箇所をやろうかと考え中です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">学生時代、髪をアップにすることはあまりなく、自分のうなじなんて気にしたことはありませんでした。でも、就職して毎日髪をアップにするようになり、他のスタッフのうなじが気になるようになりました。産毛がたくさんある人は、アップにした時に襟足が綺麗に見えないと気づき、自分のうなじも綺麗じゃないことに気づきました。うなじがとても綺麗な先輩におもいきって相談してみると、脱毛で綺麗になるとのこと！いくつかのサロンでカウンセリングをしてみて、お店の方の雰囲気がよかった銀座カラーさんに通うことに決めました。スタッフさんも脱毛しているだけあって、悩みもわかってくれ、安心しておまかせすることができました。首周りの皮膚はとてもデリケートですが、痛みもなく、いつも滑らかで潤いのある肌に仕上げてくれました。今では、仕事以外でも髪をアップにすることが増えました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">仕事柄、髪をアップにすることが多いのですが、えり足の毛がまとまらず気になっていました。浴衣などを着た時の色っぽいうなじというものにも憧れていたので、脱毛することにしました。えり足は自分では見えない場所ですし、普段自分で処理できる場所ではないため一度も処理をしたことがなく、脱毛の痛みが不安だったのですが、銀座カラーではIPL脱毛という光が毛根に直接作用する脱毛方法で、肌にやさしく痛みも少ないというお話を伺い、安心して脱毛することができました。個人差があるようですが、輪ゴムではじかれた程度の痛みですよと説明があり、実際に体験してみると、そんな感じでパチンと一瞬痛みを感じるくらいだったのでホッとしました。6回通ってとても綺麗になり、髪の毛をアップにする時もすっきり見えるので本当に良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">ホール接客の仕事をしている為、平日はほぼ毎日着物で過ごしています。髪型もアップにしているので、えり足が毛深い事が気になっていました。
私はえり足の後れ毛がとても長く耳の後ろも毛深いので、希望の形にする為にカウンセリング時に写真などを見てもらいながら相談させていただきました。
ちょっとした後れ毛は色っぽくもあるので、良いバランスで残るようにお願いしました。
効果は3回くらいで実感できました。今までは髪型をセットした時に後れ毛からこぼれ落ちる毛をピンで留めたりと時間がかかっていたのですが、その時間がなくなったのは嬉しいですね。
着物は衣紋から背中上のほうの産毛も少し見えてしまうので、今は背中の脱毛も追加でお願いしています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">人のうなじはよく見るのに、自分のうなじはあまり気にしたことがなく、美容院で髪をアップにセットしてもらって、後ろ姿を鏡で見せられた時にびっくり。
きれいなうなじを想像していましたが、無駄な毛がたくさん生えていて、左右の形も違う！！こんな姿を20年以上さらしていたのかと愕然としました。人の目に触れることが多い仕事なのに、、、
カウンセリングの時に、エステティシャンの方と希望の形を相談し、左右揃った「W型」にすることに決めました。首を細く長くきれいに見せる効果があるそうです。脱毛完了後、彼氏から「うなじに色気を感じるようになって、つい見とれてしまう」と褒めてもらえました。見える部分で女性らしさをアピールするのにうなじがこんなに有効だったんだとあらためて思いました。今年の夏は毎日髪をアップにして、横顔美人を目指します。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">私には胸毛というか、胸周りの産毛がけっこう生えているのが悩みでした。普段は胸の開いた服を着ないようにして隠していましたが、友達の結婚式に出席した時に胸の大きく開いたデザインのウエディングドレスを見て、私も着たいなと思うようになりました。自分が結婚する時に間に合うように、すぐに脱毛してしまいたいと思い、友達からの評判がよかった銀座カラーにカウンセリングに行きました。銀座カラーでは、脱毛と同時に保湿をしてくれて、お肌ケアと、美白も同時にできるとのことで、産毛に悩んでいたデコルテがどんどん白くて綺麗になっていきました。肌も潤うようになり、脱毛以上の効果を感じています。まだ少し先になりますが、結婚式では、絶対に胸の大きく開いたドレスを着たいと思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">ネックレスの着画像を撮る機会があり、デコルテのうぶ毛が気になるようになりました。それまで気にしていなかったのですが、デコルテ部分だけを撮るとなるとやはり目立つものですね。写真を撮る時に気になってからカミソリで剃ってみたのですが、やはりその後に生えてきたうぶ毛が濃くなった気がしたので、これを機に脱毛に通って綺麗にしたいな、という気持ちが強くなりました。うぶ毛って、脱毛すると肌の色が白く感じるし、ずっと綺麗な肌でいられるので、思い切って脱毛に通って本当に良かったです。しかも銀座カラーオリジナルの保湿ケア、美肌潤美のローションやミストシャワーは保湿効果だけでなく、素肌を綺麗にしてくれるので、それもとても良かったです。これで、デコルテのあいた洋服も自信を持って着る事ができそうです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">普段は洋服で隠れることが多い部分なので、あまり気にしませんでしたが、大好きな彼ができ、すべすべの肌を目指していました。
胸の脱毛をするとムダ毛が一掃され、脱毛後の銀座カラーの美肌潤美というローションとミストにより、皮膚の水分量が上がり、弾力が増してきました。水着を着られる夏が待ち遠しいです。彼とプールとか海とか行きたい！
施術時に胸を出すことに抵抗もありましたが、手早く正確に処理をしてくれるので恥ずかしがっている暇もありません。施術のギリギリまでタオルで箇所を隠してくれますし、終わったらまたタオルをかけてくれますので露出している時間は割と短いです。胸脱毛を恥ずかしがっていましたが、もっと早くこのつるつるボディを手に入れればよかったなと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">肌が弱く、日焼け止めを毎日塗り続けるとデコルテの毛穴に日焼け止めが入り、毛穴が一層目立ってしまったり、赤く炎症を起こしてしまったりと、せっかくの夏なのに綺麗なデコルテでいられなくなり、デコルテを出したファッションを楽しめないでいました。
思い切って銀座カラーでデコルテを脱毛したら毛穴が目立たなくなり、美肌潤美というミストとローションでエステに行った後のように、すべすべしたくすみのない肌となり、赤く炎症を起こすこともなくなりました。おかげでおしゃれの幅も広がりました。仕事でパーティーに参加することもあるのですが、デコルテが出るドレスも完璧です。笑</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">お腹の毛は以前から気になっていたのですが、人に見られるのはビキニを着た時ぐらいなのでその都度自分で剃っていました。でも、妊娠してからお腹周りの毛がどんどん濃くなっていき、出産後も薄くなることはありませんでした。このままではビキニを着れないなと思い、脱毛に行きたかったのですが、赤ちゃんを置いて長時間出かけることもできず、また、ホルモンバランスが元に戻る前に脱毛に行くのもどうかな？と思い、産後1年半が過ぎてからようやく通い始めました。私は第二子をすぐに妊娠しなかったので、全部通えて今ではすべすべでキレイなお腹になりましたが、第二子妊娠で脱毛を途中で断念した友人もいました。妊娠して毛が濃くなったという友人はたくさんいるので、学生さんや結婚前の方には、妊娠前に脱毛しておいた方がいいよ！と声を大にして言いたいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">お腹のうぶ毛がとても気になったのは妊娠してからです。今までも気になっていたのですが、うぶ毛だし…と処理もしていなかった私。普段お腹を人に見せる機会もなかなかないですしね。
ところが、妊娠をして妊婦検診の時に毎回男性の先生にお腹を見せるのですが、見せる時はやはり恥ずかしくて。しかも妊娠をしてからお腹の毛も濃くなり、以前より気になるようになりました。さすがに妊娠中は脱毛に行けないので、出産後に脱毛をする事を決意しました。2人目を妊娠するまでに完了させたいと思い頑張って通いました。
その甲斐あって今ではお腹はツルツルですし、ビキニになるのも恥ずかしくなくなりました。（以前はビキニが着れず、ワンピースタイプばかりでした…。）こんな事ならもっと早くに脱毛すれば良かったなと思いました。これでもう誰に見られても恥ずかしくないです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">小さな頃からお腹まわりに濃い毛が生えてきて、自分は病気なのではないかと悩んでいました。
母親も姉もお腹の毛が濃く、遺伝的なものなのかなと思っていました。でも、彼ができて彼より自分のお腹の毛が濃いのにびっくり…。慌ててカミソリや脱色剤で処理を行っていました。毎回の処理に気を使い、また、やはり自分は異常なのではないかと心配になり、我慢できなくなって銀座カラーのカウンセリングに行きました。
お腹の毛が濃いのは病気でなく、遺伝やホルモンの影響を受けやすいとアドバイスをもらい、脱毛を始めました。今ではお腹の毛がなくなり自分の体に自信が持てるようになりました。母や姉にも勧めています。ずっと悩んできた悩みが解決し、銀座カラーに感謝です（＾＾）</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">お腹周りの毛を特に気にしたことがなく、肉眼で見てもそれほど目立たないので、特に脱毛の必要はないなと思っていた箇所でした。でもVIOと足の脱毛をしたかったので、どうせやるなら全身脱毛で！と思いお願いしました。まず、脱毛後お腹周りの変化にびっくり！！！毛がないと思っていたお腹にもたくさんの産毛があったようで、脱毛することにより肌が白くなり皮膚のくすみがとれて、手触りがすべすべに☆
Vラインだけを脱毛するとその近辺の毛がなくなり、逆にお腹の毛が目立ってしまうことがあるそうだけど、境目なく全身くまなく脱毛してくれるのは銀座カラーだからこそ。銀座カラーで全身脱毛を行い、本当によかったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学生になってサークルに入り、合宿や小旅行などでみんなで温泉に入る機会が増えました。もともと乳輪周りの産毛と、時々生えてくる太くて長い毛が気になっていたのですが、友達の綺麗な乳輪周りを見て、自分も見られているんだろうなと思ったらとても恥ずかしくなりました。。いろいろ調べてみて、銀座カラーさんで乳輪周りの脱毛ができると知り、カウンセリングに行ってみました。乳輪にカミソリをあてることには抵抗があったので、自分では太い毛を抜くぐらいしかしていなかったのですが、光脱毛で産毛まで綺麗になると聞いて通い始めました。全6回のコースにしましたが、数回通っただけでもかなりの効果を感じています。もう少しで夏の合宿がありますが、これからは温泉タイムも友達の目を気にせずに入れそうです。残りの回数でどれほど綺麗になるか楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">ムダ毛で気になっていたのは乳輪周りです。濃い毛が数本とうぶ毛のような細い毛が結構生えていました。見つけるたびに毛抜きで抜いていたのですが、毛抜きで抜いていると埋没する事もあると聞いたので怖いなと思うようになりました。うぶ毛などはカミソリで剃る事もありましたが、乳輪周りは皮膚が薄くカミソリの刺激で赤くなったりして良くないと知ってから、どうしようと思っていたんです。それで色々調べていくうちに、乳輪周りも簡単に脱毛する事ができると知り、他の箇所を脱毛するついでに一緒に脱毛することにしました。サロンの無料カウンセリングは、そういうデリケートな悩みも隠さず話せるので、きちんとした知識を教えてもらえ、脱毛するメリットなどもお聞きできたので安心して通うことができました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">乳輪の周りに毛が生えて、、自己処理をしていたらどんどん毛が濃くなっていき、処理をしていないとチクチクして、これが一生続くのかと剃り始めたことをずっと後悔していました。電車の中の広告を見て、思い切って銀座カラーの無料カウンセリングに行きました。なかなか親や友人にも聞けなかったデリケートゾーンの悩みが相談できて、親身になって聞いてくださりとてもよかったです。痛みもなく、脱毛前後の美肌潤美というミストとローションにより、今では、毛がないどころか、肌もきれいになりました。誰に相談していいのかわからなくて、一人で悩んでいる人は、銀座カラーの無料カウンセリングにぜひ行って欲しいですね。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">高校生くらいの時から乳輪のまわりに産毛が生えていて、なかなか人に相談できず、自分だけなのかなと悩んで自己処理をしていました。自己処理で傷つけてしまうこともあり、誰に相談していいのかわからずに過ごしていましたが、銀座カラーの無料カウンセリングでお話ししたらみんなあるとのことで安心しました。乳輪周り以外のデリケートな部分の毛の悩みも相談に乗ってもらい、本当に助かりました。こういった部分はほとんどの方が処理しているとのことでしたので、私もこの機会に全身脱毛をすることに。脱毛後の美肌潤美のミストにより施術後のお肌はしっとり、気になっていたプツプツもだんだん少なくなりお肌も綺麗になって大満足です。もっと早くに銀座カラーに相談に来ていればよかったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">足は基本的に自己処理していたので、埋もれ毛みたくなってしまい、その後医療脱毛を検討していたのですが、痛みが怖くて断念しちゃいました。それから色々調べていくうちに銀座カラーの脱毛は痛みが少ないとか、痛いと思うのは最初だけとか（個人差は、あると思いますが）色々なところで聞いたので思いきって通うことに。契約前の説明もとてもしっかりしていて余計な勧誘とかもなく、大事にされているというか親身になって色々と相談に乗っていただけました。とにかくポツポツと黒ずみが目立ってるひざ下をどうにかしたい！夏にはショートパンツを履きたい一心でサロンの方に相談しました。「予約が取りにくい」みたいなよく聞く声も忘れるくらい取りやすいです。そしてなにより痛みがなかったです（個人的には）楽に気軽に美容院感覚で通えるので、オススメです！☆</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">今まで脱毛サロンというものにあまり縁がなく（興味がなく）カミソリで剃っていたり、脱毛シートなどでなんとなくごまかしてきました。
年々それを繰り返してきた為か、あるときひざ下を触ったら指先に「ザラッ」とした感触を感じたのです。これはちょっとキツイかも…と思い友達にどうしているか相談しました。
友達はいくつか脱毛サロンに通っていて（3回位お店を変えたと言ってました）銀座カラーに落ち着いたということだったので私も試してみようと思いました。友達もひざ下の黒ずみを気にしていて、そのまま処理していくのは大変そう、と思って脱毛を始めたようで、私もひざ下からはじめることにしました。アフターケアをどうしていいかわからずどうしようかと思ってましたが、担当していただいた方から丁寧な説明もあり、とても助かりました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">以前はひざ下にそこまで濃いむだ毛は生えていなく産毛レベルだったのですが、気になるので毎日剃っていました。ふと気づいたときにはもう、ぶつぶつの黒ごまのような毛穴のガサガサ肌に。
友人に相談したところ銀座カラーを勧められたので、通うことにしました。
足全セットで始めたところ、すぐに効果が実感できました。ぶつぶつ黒ごまもすっかりなくなり、6回が終わる頃にはほとんど気にならないくらいの産毛しかありませんでした。専用の保湿ケアでお肌もスベスベになり大満足！私が利用したセットの場合、足の甲も含む足全体が施術対象なのに月額コースで全て対応してもらえるので、とてもお得でした。その上私の場合は学生だったので、学割特典がついていて更にお得でした！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">足のすね毛が濃くて、スカートがはけない…小学生の頃からの悩みでした。
この忌々しい毛をなんとかなくしたい！と市販の脱毛グッズを試しまくりました。が、結果、益々濃くなっている気がして、気がついたらクローゼットからスカートはなくなっていました。仕事でも困るので何としてでも変わりたいと思い、無料カウンセリングの予約をしました。銀座カラーさんでは痛みの少ないIPL脱毛という光脱毛だというお話を聞き、これなら私の濃い毛でも痛みが少なく施術してもらえるかも？と思い、足全セットをお願いしました。実際の施術では、パチッ！と一瞬痛みを感じるものの、それ以外は全く辛くありませんでした。サロンへ通う度に薄くなっていくむだ毛、6回の施術でほとんど気にならなくなりました。もっと早く行けば良かった！最近の私のクローゼットは、スカートばかりになっています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">仕事、仕事、、、の毎日で自分磨きを怠ってからはや3年以上。仕事以外も楽しもうと思った矢先、友だちの紹介がきっかけで彼氏ができました。
怠ってた期間分、身体の色々な所が気になってしまって「この際だから徹底的にやろう！」と決意！自分には見えない部分も心の許した人には見えたりする部分かも、、、と背中の脱毛もすることにしました。実際通い始め、サロンの方も親身に相談に乗ってくださり、他愛もないお話をしたりと、楽しく時間を過ごす事ができたので毎回あっという間の施術でした。
何年も海には行っていませんでしたが、見られてもいいようなツルツルな背中に生まれ変わったので、ずっと夢だった海デートに行って自信をもってビキニを着ようと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">一時期、海外のセレブやモデルのファッションを真似して、背中の大きく開いたトップスが自分的にブームな時期がありました。
そんな時、親友に「背中って自分でチェックしてる？ちょっと目立つかも・・・」と言われました。その一言で、それまで着ていたトップスたちはクローゼットの中で仲良くお留守番を開始・・・。
でもせっかく集めたトップスたちをまったく着なくなるのももったいないし・・・と、背中の脱毛を決心しました。背中は普段自分ではまったく見えない部分なのでとても不安で、とにかく評判がよいサロンを探して友人にも聞いて銀座カラーさんにお世話になることにしました。施術後は、お留守番していたトップスたちを自信を持って着ています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">以前より自己処理を行っていましたが、日に日に濃くなってきている気がして、剃るのにも恐怖心が出ていました。
なんとか安くて効果的な脱毛サロンはないものか…とネットサーフィンを続け、数店舗のカウンセリングを受けに行き、銀座カラーさんに決めました。
カウンセリングでは丁寧に脱毛についての説明をしていただき、背中のニキビの改善も期待できるかも、など脱毛以外の効果まで丁寧に教えていただけとても好感をもったからです。
施術後、本当にニキビもできなくなり背中もツルッツル！数年前の憂鬱な自分が信じられないくらいです。水着もキャミソールも堂々と着られる肌を手に入れ、着られる服のバリエーションが増えて、本当に嬉しいです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">もともと全身毛深いので、小学生の頃から脱毛には興味がありました。まずは目立つ手足から脱毛を、と思いましたが、水着やキャミソールを着たとき、背中の濃い産毛が本当にイヤで暑い季節になると憂鬱でした。でも優先順位としては手足には勝てない、でもどうにかしたい濃い産毛...毎日脱毛の事を考えていました。大学生になった時にアルバイトも始めたので、意を決しカウンセリングを予約しました。
はじめは本当にこのむだ毛達がなくなるのだろうか...と疑心暗鬼でしたが、スタッフさんのお話を聞くうちに、私もむだ毛に悩まされない憧れの毎日が送れるかも！と前向きになり、すぐに施術の予約をしました。何回か通っていくうちに、あんなに濃かったむだ毛がどんどん薄くなっていき、同時にこれで私も堂々と好きな洋服が着れる！と毎日が楽しくなりました。
家族や友達からも「最近明るくなって良い感じだね」と言われるように。思い切って脱毛をして本当に良かったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">元からそんなに毛が濃い方ではなかったので海外で評判の脱毛器でなんとかなってたのですが、そんな中での悩みは直接目視で安全にできない場所、手が届かない場所の脱毛でした。特に腰とかは凄く悩んでました。サロンでやってみようかなと思ったのですが、一番最初に「どうなんだろ？」と思ったのが、普段から腰痛などで敏感になっていて、サロンの機械で施術した場合、振動や刺激で痛くないかってことでした。カウンセリングの時に相談に乗ってもらい、様子をみつつ始めることにしましたが、実際のところ1回目の時点で腰の刺激などはまったくなかったので安心して脱毛しました。サラッと腰の脱毛がおわってるそんな感じで、とっても楽チンです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">性格上、1回気になるととことん気になっちゃうのですが、ある日友人とのランチ後、店を出ようとした瞬間にしゃがみこんだ友人のジーンズから出た腰周りに、うっすらですが気になる毛を発見。 家に帰り鏡で自分の腰を恐る恐る見てみると…ありました。想像以上に濃いむだ毛が。夏に向けて、脱毛しようとしていたのでついでに腰もしてしまえ！と思い、腰の脱毛もお願いすることにしました。ネットで口コミを見てもうひとつのサロンと最終的に迷いましたが、普段は仕事でスケジュールが埋まっているので、予約が取りやすいと評判の銀座カラーにお願いすることにしました。特に痛みもなく、やっていただいてる時間もあっと言う間でした。私自身ローライズパンツは一本も持ってなかったのですが、せっかく腰をスッキリしたので一本購入しようかなと思いました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">以前背中の脱毛はしてましたが範囲が広いので時間がかかって、腰回りまではしないで終わってました。
今回、全身ツルツルにしたいと思って、まだやっていない箇所をお願いしました。
全身脱毛とフリーチョイスでコースの選択に迷ったけど、以前施術した部分に中途半端に残っているむだ毛もあって、全身脱毛コースを選びました。
銀座カラーさんは金額面でも施術でも、私にはとても合ってました。肌はつるつるでキレイになるし、気になるむだ毛はなくなるし、大満足！
腰まわりなんて普段集中してケアをする部分ではないので、少し黒ずみというかくすみがあったのですが、今回の脱毛で肌の色が1トーン上がった気がします！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">ローライズのジーンズやパンツが流行り買って着てはみたものの、想像以上に腰から背中が出ててビックリ！私の腰回り大丈夫かな…？と鏡で見たら産毛がいっぱい(&gt;&lt;)
これじゃ恥ずかしくてローライズなんかはけない！と友達が通っていた脱毛サロンを調べたところ、腰、背中は範囲が広いから金額もお高く…で手が出せず。他のサロンをネットで探していたところ、銀座カラーならいけそう！と思ってカウンセリング予約してみました♪
金額もデリケートチョイスが6回13,000円と学生の私でも払える額だったし、施術時間も短いので学校とバイトの間に通えるのも魅力でした。
これで自信を持ってローライズにトライできます！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛サロンは勝手な先入観で「高い」イメージがありました。なので自分で海外製の機械やシートなどで脱毛をいろいろ試しました。自分でやるにはやはり、限界があって色々な悩みが出てきました。失敗すると、赤くなったり、切れてしまったり・・・・きつかったです。カラダのことなのでお金がかかってもやはりプロにしてもらうべきだと一大決心。箇所を選択してお願いしようと思ってましたが、想定していたより全然金額も安いし、月額制での支払いもできたので、だったら全身してしまえ！と思って全身脱毛コースに申し込みました。期間はどのくらいかかるのか、とかたくさん不安な要素がありましたが、現状のお肌の状態を見てもらったりして、どのくらい時間がかかりそうかなどなど色々アドバイスをしていただけて安心してお任せすることが出来ました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">小学生の時から自分が毛深いということはわかっていたのですが、中学高校と友達の肌を見る度にどんどんコンプレックスに。大学に入り少しずつ脱毛しようと思い、全身一気に始めた方が通う期間もお金もおトクだったので、銀座カラーの全身脱毛コースに申し込みました！脱毛に関する知識はまったくなかったけど、カウンセリング時にスタッフさんから自己処理を続けることによる肌のダメージについてや、銀座カラーの技術の良さについて説明してもらい、信頼して始められました。毛が太いと過敏に反応して痛いと聞いたことがありましたが、思ったよりも痛みが少なくすぐに慣れました。ずーーーっと、剛毛と付き合ってきたので、ガラッと変わった全身がスッキリしすぎて嬉しくなりました。新しい自分を感じながら、やる気がすごくみなぎるようになりました☆</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">小さい頃から人より毛が濃い体質でした。
レーザーの脱毛は剛毛だと反応が強くて痛みが多いと聞いたことがあり、でも光脱毛は効果がでるのか、と選択に悩んでいました。
サロンをいくつか調べていたところで銀座カラーのIPL脱毛の説明を読みました。口コミなども参考にし、一度お話を聞いてみたくて無料カウンセリングを予約しました。
カウンセリングではアンケート用紙に自分で記入した内容を見ながらスタッフの方とお話をし、脱毛の効果や方法について説明いただきました。
スタッフの方がとても丁寧で好感が持てたので、施術をお願いしました。
施術中、痛みがでそうな箇所の脱毛を行う時に声をかけてくださったり、少しだけ休憩をはさんでくださったり、と私を気遣いながら施術をしていただき、信頼してお任せできました。
全身ツルツルになるにはまだ時間はかかりますが、少しずつ効果を感じています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">何よりも痛い事が大嫌いなので「全身脱毛をしたい！」と決めた時、インターネットで痛みの少ないサロンを探しました。
全身完璧に脱毛したかったので、顔も含めた24カ所スペシャルコースで脱毛していただきました。
自宅から近い船橋北口店に通っていたのですが、予約が取りづらいこともなく、1回1時間程度でしかも1ヵ月ごとに通えました。
お部屋もベッドもとても清潔だし、個室なのでデリケートな部分をお願いする場合でもプライバシーが守られているので安心してお願い出来ました。
全身脱毛するのだからどれだけ時間がかかるだろう...と思っていたのですが、あまり苦になりませんでした。
肝心の痛さについては始めは感じました。でも耐えられる程小さな痛みだったので、すぐに慣れちゃいました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">心配性で、恥ずかしがりやの私なので、VIO脱毛をやるのはとても抵抗がありました。でもデリケートな箇所なので、自分で処理しづらい場所だし、毛が太いのでカミソリで剃る時や抜く時に痛みもあるし、毛穴から菌が入って赤みができてしまったり・・・と悩みが尽きなかったので、思い切ってやってみることにしました。実際にサロンに行ってみると、カウンセラーの方が分かりやすく説明してくれて、最初にあった不安は消え、安心感が出てきました。
施術当日は紙ショーツを着用して施術するので、プライバシーも守られてあり、施術してくれたお姉さんと楽しく会話をしている間に終わってしまい、特に痛みも感じずあっという間でした(^-^)　あんなに不安だったのに、今は行くのが楽しみで仕方ありません！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">女子会をした時に脱毛の話になり、VIOをやってるかどうかの話で盛り上がりました。意外とやっている友達が多く、私も水着になる夏前にやっておこうと決意して通う事にしました。
VIOは水着や下着から出る毛が気になっていたので考えていたのですが、それ意外で生理の時に衛生的に良いとか凄く楽になるという話も出ていたので、キレイになるのが楽しみでした。
施術前に毛を全部剃ってくる事にちょっと抵抗はありましたが、1回剃ってしまうと早く毛が無くなって欲しい！と逆に思うように。施術の回数を重ねていく度に濃かった毛が薄くなり、下着選びも以前より楽しくなりました！もちろん衛生面でも清潔になり、ニオイも以前より無くなりました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">以前友人に誘われ、他サロンでワキ脱毛はやった事がありました。
学生の時に水着からムダ毛がはみ出してしまった事もあったり、デリケートゾーンは見えにくい場所なのでセルフケアが大変だったり、カミソリ処理をするのでザラザラ・チクチクしてしまったり。正直なところ悩みしかなかったので、社会人になってVIO脱毛にチャレンジ！
結果、やって良かったです。憂鬱なセルフケアから解放されたし、プールや温泉などでも気にせず堂々としていられます。
ムダ毛のことなど全く気にしないで楽しめているので、もっと早くやっていればはみ出さなかったのにな、と少し後悔かな。
ムダ毛の濃さで言うと場所的に毛が濃い部分なので少し痛みはありましたが、仕上がりもキレイだし絶対に脱毛サロンでの施術をオススメします。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">ショーツをはくときにいつも気になっていたムダ毛。でも病気でもないのに他人にデリケートゾーンを見せるなんて恥ずかしくてできない！と、興味はあるものの決心できずにいました。
去年の夏、友達とプールに行った時に聞いてみたところ、半数以上がVIO脱毛経験者。「はじめは恥ずかしいけど1回行くと慣れちゃうよ」と言われ、遂にVIO脱毛を決心しました。
脱毛自体初めての私はカウンセリングの時緊張しすぎてうまく話せなかったのですが、カウンセラーの方がとても感じよく質問に答えてくだいました。脱毛が始まったとき、やっぱり少し恥ずかしかったのですが、サロンの方はささっと手早く処理していて、あっという間に終わってしまいました。
今現在、5回目が終わったところですが、既に満足いくレベルまで毛が少なくなっています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">夏は腕を露出する回数が増えるので自己処理が毎回面倒だなあと思い、脱毛を考え始めました。初めての脱毛なのでいろんなサロンを調べてみて、銀座カラーには腕全セットがあることを知りました。でも初脱毛でいきなりのセットコースというのも不安だなあと思って、最初のカウンセリングでスタッフさんに相談してみました。脱毛初心者でも気軽に始められるそうで、なんか良さそうと思いこのコースを選びました。また夏から脱毛を始めたので日焼けで施術が受けられなくなるのではないかと不安でしたが、いつも通り日焼け止めクリームを塗ってお風呂上がりには保湿をしていれば問題ないようでした。自己処理だと腕の後ろ側に剃り残しがよくあったのですが、脱毛だと腕360度しっかり施術してもらえるので剃り残しの心配もなくなり、気持ちよくノースリーブが着れるようになりました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">趣味で料理教室へ通っているのですが、長袖を着ていても腕をまくって料理をするので段々と気になるようになり、脱毛を考えるようになりました。教室に通っている方は、みんなキレイな腕をしているんですよね。
毛が太くなるからという理由でカミソリなどで剃る事もしなかったのですが、旦那に「さすがに処理した方がいいんじゃない？」と言われたのがショックで脱毛サロンに通うと決意。
通ってみると予約も取りやすくサロンのお姉さんの対応も素晴らしかったです。料理教室でも話のネタが増えて教室に通っている仲間達ともコミュニケーションが増えたりと良い事づくしです。サロンへ行く前日は「明日行けばまた毛が薄くなる！」と行くのが楽しみになっています。
旦那にもキレイになったと言ってもらえたので、他の箇所もやろうか検討中です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">出産前に脱毛サロンで気になる箇所は脱毛をしていたのですが、出産後、なぜか腕の毛が濃くなってきました。
以前通っていた脱毛サロンがダメなのかホルモンバランスの関係なのかはわかりませんが、半袖から出ている二の腕を見る度に気になって仕方なかったです。
インターネットで腕の脱毛コースを調べていて銀座カラーの腕全セットを見つけました。手の甲や指は脱毛していなかったので、この際だから腕全てやってしまおう！と思いお願いしました。私としては脱毛も良かったのですが、脱毛前後の保湿ケアが気に入りました。
子供がいるのでエステでのんびりなんて時間もなく、短時間で脱毛とお肌ケアをしてくれる銀座カラーは子育ての休息も兼ねられて私にピッタリでした！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">薄着の季節には露出が多くなるので、腕の毛は自己処理していましたが、友人のツルツルの腕を見て「私もツルツルにしたい！」と思い、脱毛することにしました。
「脱毛は痛い」というイメージを持っていましたが、パチッと一瞬痛みを感じる程度で施術のストレスはありませんでした。施術時間も短かったので、お出かけの前や美容院の日と合わせて「セルフケアの日」を作りサロンへ通っていました。
私の場合そこまで毛深くなかったのではじめはあまり効果を感じませんでしたが、回数を3,4回重ねてくると薄くなっていくムダ毛を見てウキウキしました。
エステティシャンの方々も知識が豊富なので、毎回施術中のトークが楽しくて銀座カラーさんに通って良かったなぁと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">夏になると腕や足を出すのでほぼ毎日カミソリで自己処理をしていました。でも剃る回数を重ねるごとに自分の毛が丈夫で濃くなっていき毛穴もより目立ち始めました。よく露出する部分だけでも自己処理しなくてもいいようにしたい！と思い銀座カラーさんにお願いしました。この全身ハーフ脱毛コースだと好きな箇所を8点も選べるので、露出することの多い箇所もまるっと脱毛できてすごくお得だなと思いました。1回目の施術をしてから1～2週間経ちまして、毛がするすると抜け落ちていったので早速効果を体感しています。カミソリで自己処理をしていたときは2日後にはもう毛が伸びてきていたのですが、脱毛をしてからだと次の毛はまだ生えてきてないのも嬉しいです。脱毛回数を重ねると毛が薄くなっていくのはもちろん、毛穴も目立たなくなるそうなので最後の仕上がりがとても楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">気になる箇所がたくさんありつつも、全身脱毛にするにはちょっと勇気がなくて、箇所が選択できるハーフ脱毛で施術することにしました。
1・2カ所だけ脱毛をしても絶対に他の箇所もやりたくなるよ、という友達の意見を聞いたのでやってみたのですが、施術するたびに全身にカミソリを使用する回数がどんどん減っていき、ハーフで良かったと実感！
クリニックの脱毛だと痛みが凄くあると聞いていたので、デリケート部分も希望していた私には、痛みは絶対に耐えられない！と思って銀座カラーにお願いしました。あたたかい光をあてられているようで、ほとんど痛みがなくむしろ気持ちが良いです（笑）
肌も元よりも明るくなった気がするし、つるつるの綺麗な肌になって、彼にもキレイになったね！って喜んでもらえました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">ワキ、ヒジ下、ひざ下、Vライン、と脱毛したい箇所は決まっていたので、セットでできるところを探しました。
3つほど候補サロンがあったのですが、口コミや金額などで銀座カラーさんに決めました。
私は銀座本店を利用していたのですが、やや上品な雰囲気の店内だったので、はじめは緊張しました。
肌が若干弱いので、ジェルでかぶれたりしないかと心配していたのですが、事前にパッチテストをしてもらい問題なかったので安心して脱毛できました。
夏の時期は予約が取りにくいこともあるようですが、年間通じて問題なく予約でき、仕上がりにもとても満足しています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">私はあまり毛深くはないので、気になっている両ワキと両ひざ下、両ヒジ下の3カ所でお願いしようとしたのですが、無料カウンセリングで全身ハーフ脱毛を教えていただき、そこまで価格が変わらなかったのでハーフにしました。
全部で8カ所の脱毛ができたので、結果的にはお得に脱毛できちゃいました。
はじめの1,2回は箇所によっては「本当に毛が少なくなっているのかな？」なんて思っていましたが、4回を終わる頃には効果が目に見えるようになってきて、確実に少なくなっているな、と実感できました。よく他サロンの口コミなどをみていると「勧誘がしつこい」「予約がとれない」などありますが、銀座カラーは全く問題なく予約もスムーズにとれました。
こんなにキレイになるなら、今度は見えないボディ部分にもトライしてみたいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">学生の時に10カ所くらいセットで脱毛をしましたが、20代後半から体質が変わったのかやけに毛が濃くなってきたのでフリーチョイスコースでお腹だけ脱毛しました。
1カ所毎の価格が安いところを調べたのですが、結局は家の近くで評判が良さそうなサロンが一番だと思い、銀座カラーさんにしました。
いつも平日の昼間に予約をしていたのですが、毎回すぐに予約がとれました。以前通っていたところは全くとれなくて半年くらい予約できない時もあったので、銀座カラーは快適でした。
毛が濃いと言っても腕や足ほど濃いわけではない為、効果を感じるには時間はかかりましたが気にならない程に脱毛ができました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛サロンは、コースが決められているものだと思い込んでしまっていました。パソコンの広告でたまたま見かけて好きな部分を選べて脱毛ができることを知りました。土日はいつも遊びたい派なので、平日何とか会社帰りにお願いできるものかなと思ってましたが、定時にあがっても十分間に合う時間に予約が取れますし、なにより会社の近くにあるのがとても助かります。へそ周り、両ワキ、両ひざをお願いしました。少しだけ肌が弱めなので施術後、数日経った時に赤くなったり荒れたりしないかとても心配でしたが、アフターケアも万全で繊細にお肌のことに気をつかってくれているんだなぁと実感しました。おうちの近くに店舗があるので気になったことは直接聞きにも行けますし、通いやすいです。必要に応じたそれぞれの対応をしてくれてそうなので信頼性も◎だと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">顔の脱毛がしたくなり一番安いサロンを見つけて行ったのですが、実際は顔のパーツ毎に細かく料金設定がされていて金額がどんどん上乗せに。騙されてるような気がしてきてカウンセリングを受けただけでやめた経験がありました。今回銀座カラーにお願いしたのは、料金体系が非常に明確だったからです。それだけでも信頼が持てました。
顔のうぶ毛は細いので、毛がなくなるまで結構時間かかりそうだなぁ、と思ったのですが数回で想像以上に毛が少なくなってました。
しかもお肌はツルツル！美肌潤美の保湿ケアもつけてもらったので、潤いたっぷりで、ひりひり感も感じません。美肌潤美のミストが大好きです！
他の箇所もたまに勧められるくらいでしつこい勧誘もなく、短時間でキッチリ終わらせてくれる、あらゆる面で私には合っていました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">当初はクリニックに行こうか、脱毛サロンに行こうか迷っていました。ワキ脱毛だけやりたかったので、金額はさほど気にはしていませんでした。
ただ、候補にあげていたクリニックでの脱毛は痛いという口コミが多く、できるだけ痛くない脱毛を希望していたので脱毛サロンを選択しました。
銀座カラーさんは店内もキレイでくつろげる雰囲気が気に入りました。
痛みも全くないわけではないですが、気にならない程度だったので脱毛サロンにして正解でした。
実は脇の黒ずみも気になってはいたのですが、3回4回と続けていくうちに段々と黒さがなくなっていて驚きでした！
カミソリを使っていると黒ずみが進行するって本当なんだなぁ、と実感しました。
あと、元々ニオイは強くない方ですが脱毛するとニオイがキツくなるという噂もあるので心配していましたが、特にそういうことはありませんでした。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">会社の同僚の大先輩が外見も中身も、デキるキャリアウーマン！という感じで、とても素敵で憧れでした。外見だけでもその先輩に近付きたくて、まずは脱毛から！と電車の中吊りを見かけて電話をかけてみました。
スタッフさんの対応がフレンドリーかつ親切だったので安心して色々な相談をさせていただきました。社会人二年目であまり貯金もないので、予算面等含んだことも相談に乗ってもらい、とりあえず足全体をお願いすることにしました。友達からよく聞いていた「脱毛サロンは予約が取りにくい」なんてことは忘れるくらいすんなり予約が取れて、施術も店員さんの質も申し分ないくらい満足しました。足全体の毛自体がなくなるだけじゃなく、質感や黒くポツポツ気になっていた（毛穴かな？）のも見違えるほどのスベスベ肌になり、なんでもっと早く行かなかったのかと思うくらいでした。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">数ヶ月前にダイエットを開始。着々と体重を落とすことができ、ぽっこりお腹だけではなく脚ヤセにも成功！少し自信を持って人前で肌を出せるようになりました。でも、もっと完璧な足にしたい！と欲が出てきました。ネットや口コミを見ていて予約が取りやすく、足全体すべて脱毛でき、比較的安くて安心できるサロンと贅沢な条件で探していた所、銀座カラーの足全セットにたどり着きました。脱毛サロンは初めてだったのでドキドキしながら通い始めました。痛みやかゆみとかあるのかな・・・なんて不安もたくさんで行きましたがそんな要素は全くなく、あっという間に施術終了！保湿ケア付きにしたので、終わってからも霧のシャワーみたいなのがとても気持ちよくて、最初から最後まで通う前の不安なんてなくなるくらいの時間を過ごすことが出来ました。アフターケアもどうしていいかわからなかったのですが、親切に教えていただけました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">ストッキングからムダ毛が外にはみだしてしまうほど剛毛な私のすね毛。朝カミソリで処理をしても夕方にはもうチクチクしている程。本当に恥ずかしくてシェーバーや毛抜き・脱毛クリームなど色々と試しました。その結果、炎症は起きるわ陥没毛はできるわ、と酷い状態に。
私は肌の色がとても白いので黒い毛穴が本当に憂鬱でした。
友達が銀座カラーに通い始めたと聞いたので、お友達紹介を利用し足全セットでお願いすることにしました。
銀座カラーは比較的予約が取りやすいので、紹介してくれた友達と行く日を合わせて通ったりもできました。
まだ全ては完了していませんが、黒かった毛穴も段々と薄くなってきています。
あんなに恥ずかしいと思っていた自分の足ですが、今は生足でショートパンツが履けるのを楽しみにしています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="158" height="158" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">ミニスカートを履くのが大好きなのですが、ムダ毛がいつも気になっていました。
ムダ毛が気になってくる度にお風呂でカミソリを使って剃っていたのですが、肌も荒れるし段々とブツブツも目立ってきて...。
憧れのモデルさんのようなキレイでピカピカな足になりたくて脱毛を決意しました。
銀座カラーの足全セットは他サロンに比べて価格も安いし、脱毛後のミストシャワーも気持ち良い！
段々自分の足がつるっつるになっているのが嬉しくて母にも自慢していると、「私も行こうかな？」なんて言っていました。行く度に足がキレイになっていくのが実感できて、まるでエステに通っているような気分でした。</dd>
          </dl>







            <div class="block-btn-stripe-lower">
              <a href="#" class="btn-stripe btn-wide btn-icon-plus">もっとみる</a>
            </div>
          </div>

        <!-- /#all --></div>

        <div class="js-tab-body" id="salon" style="display: none;">

          <div class="select js-select">
           <select name="">
             <option value="">店舗を絞り込む</option>

<option value="札幌店">札幌店</option>
<option value="盛岡菜園店">盛岡菜園店</option>
<option value="仙台店">仙台店</option>
<option value="銀座本店">銀座本店</option>
<option value="銀座プレミア店">銀座プレミア店</option>
<option value="新宿東口店">新宿東口店</option>
<option value="新宿店">新宿店</option>
<option value="新宿西口店">新宿西口店</option>
<option value="渋谷109前店">渋谷109前店</option>
<option value="渋谷道玄坂店">渋谷道玄坂店</option>
<option value="表参道店">表参道店</option>
<option value="池袋店">池袋店</option>
<option value="池袋サンシャイン通り店">池袋サンシャイン通り店</option>
<option value="上野公園前店">上野公園前店</option>
<option value="錦糸町店">錦糸町店</option>
<option value="北千住店">北千住店</option>
<option value="北千住2nd店">北千住2nd店</option>
<option value="吉祥寺北口店">吉祥寺北口店</option>
<option value="立川店">立川店</option>
<option value="立川北口店">立川北口店</option>
<option value="八王子店">八王子店</option>
<option value="町田モディ店">町田モディ店</option>
<option value="AETA町田店">AETA町田店</option>
<option value="大宮店">大宮店</option>
<option value="川越駅前店">川越駅前店</option>
<option value="千葉店">千葉店</option>
<option value="千葉船橋店">千葉船橋店</option>
<option value="船橋北口店">船橋北口店</option>
<option value="柏店">柏店</option>
<!--  <option value="横浜店">横浜店</option> -->
<option value="横浜エスト店">横浜エスト店</option>
<option value="横浜西口店">横浜西口店</option>
<option value="川崎店">川崎店</option>
<option value="藤沢店">藤沢店</option>
<option value="宇都宮店">宇都宮店</option>
<option value="つくば店">つくば店</option>
<option value="水戸駅前店">水戸駅前店</option>
<option value="高崎店">高崎店</option>
<option value="新潟万代シテイ店">新潟万代シテイ店</option>
<option value="金沢駅前東店">金沢駅前東店</option>
<option value="静岡店">静岡店</option>
<option value="名鉄岐阜駅前店">名鉄岐阜駅前店</option>
<option value="名古屋栄店">名古屋栄店</option>
<option value="名古屋駅前店">名古屋駅前店</option>
<option value="金山店">金山店</option>
<option value="梅田茶屋町店">梅田茶屋町店</option>
<option value="梅田新道店">梅田新道店</option>
<option value="心斎橋店">心斎橋店</option>
<option value="天王寺店">天王寺店</option>
<option value="神戸元町店">神戸元町店</option>
<option value="三宮店">三宮店</option>
<option value="広島店">広島店</option>
<option value="天神店">天神店</option>

           </select>
         <!-- /.select --></div>

<div class="block-voice-list no_margin js-appendmore-voice" itemscope="" itemtype="http://schema.org/BeautySalon">

<!-- -->


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金山店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>ひざ下のカミソリ負けが悩みで脱毛サロンを探していました。
仕事の関係で平日は何時に業務が終わるか分からないし、土日も予定が埋まっている事が多く、とにかく予約が取りやすいサロンを探していたところ、友達が銀座カラーがオススメだということで来店しました。名古屋駅周辺で探していたら、金山に新しく店舗がOPENしたというので「銀座カラー金山店」に通っています。駅からも家からも近くて非常に助かっています。
店内も綺麗でスタッフの方も優しい！！施術に関する説明も丁寧な印象があります。今まで、お風呂上がりにはこれでもかってくらい化粧水とボディクリームを塗っていたのですが、脱毛してからは少ない量でも肌荒れしなくなりました。節約にもなってラッキーです(笑)
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金山店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 アパレル<br></span>
</dt>
<dd itemprop="reviewBody"><p>これまでセルフケアをしていたのですが、年々、毛穴の黒ずみがひどくなっている気がして、色々調べてみたらシェービングによる肌荒れが原因なのではということで顔の産毛もサロンで脱毛してみることに。元々他サロンで全身脱毛はしていましたが全然予約がとれなかったので、今回は銀座カラーの顔脱毛コースを申し込みました。2～3回目くらいから見て分かるくらい毛穴の黒ずみも少なくなり、キメの細かい肌になった気がしました。肌触りもツルツルで化粧ノリも良くなった気がします。特に顔は隠せないのでセルフケアではなくサロンで脱毛した方が良いなと改めて思いました。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金山店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>大学生になったら絶対脱毛しようと高校生の時からアルバイトでお金をため、念願の初脱毛サロンは学校の同級生の紹介で学割や紹介キャンペーンがある銀座カラーにしました。
初めての脱毛なので施術内容やお金の問題が不安だったけどお店の方が丁寧に説明をしてくれて、施術中も私が不安そうにしていたら声をかけてくれたりと本当に優しいスタッフの方々で良かったです。施術も全然痛くなかったので一安心です！まだ数回しか行ってないけど、新しく生えてくる毛が段々と薄くなってきている感じがします！
アルバイトと学校で予定が立て込んでいるから週末の時間しか脱毛に行く時間がないけれど、銀座カラーは予約しやすくて助かってます！
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金山店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>転職することになり、新しい会社には生まれ変わった自分で行きたい！
この際だから脱毛をしてキレイになろうと名古屋市内で脱毛サロンを探していたところ、ネットで良い口コミが多かったし、電車広告でよく見ていたので、銀座カラーにしました！
この機会にと全身脱毛のコースで申し込みました。何回か通ううちに肌がすごくスベスベしてきた気がします！お気に入りは施術後の保湿ケア。モチモチで赤ちゃんのような肌に！これは本当におすすめ！！
新しい会社でも肌が綺麗だねって言われて、すでに通って良かったなと実感しています(笑)
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金山店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">30代 主婦<br></span>
</dt>
<dd itemprop="reviewBody"><p>私は敏感肌で肌が弱く、セルフケアでは肌荒れしてしまうことが悩みでした。
ただ、脱毛は肌にダメージを与えてしまうのでないかと、良いイメージがあまりなかったので、まずは無料のカウンセリングに行きました。
銀座カラーのカウンセリングはその人の肌状況、肌質に合ったコースの選択やケア方法を教えてくれます。私自身脱毛サロンへ通うのが初めてということもあり、肌荒れするのが怖いのでたくさん質問をさせていただきましたが、丁寧に回答をしていただき、不安なく施術に進めました。
子供を保育園に預けている間にさっと通わなくてはいけないので、銀座カラー金山店は駅近で非常に助かっています。スタッフの方も気さくで優しく、施術後のスキンケア方法から最近の美容情報、話題のコスメ情報などスタッフの方との会話にも花が咲きます。今では毎回のサロン通いが楽しみです。
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金山店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 保育士<br></span>
</dt>
<dd itemprop="reviewBody"><p>恥ずかしいお話なのですが、保育園の子どもから「先生の腕チクチクする～」と言われ銀座カラーに通うことを決意しました！これまで自分でケアをしていたのですが、毎日仕事がとても忙しいので、肌の露出が少ない冬はついケアを怠ってしまうんですよね。腕がチクチクする先生なんて最悪ですからね(笑)「ツルツル肌の先生」になれるよう、今では定期的にサロンに通っています。
銀座カラー金山店に通うようになったのは、アクセスが良いから！通勤する際に使用している駅だし、いつも仕事終わりの時間に予約しています。
あと初めての脱毛サロンは実績がある有名なところが良いと思い、銀座カラーにしました。通ってみたら施術も保湿ケアもきちんとしていて大満足です！これからも末長くよろしくお願いいたします(笑)
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金山店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 美容師<br></span>
</dt>
<dd itemprop="reviewBody"><p>仕事の関係上、身だしなみには気をつかわなくてはいけないので、以前からセルフケアではなく脱毛サロンでケアをしています。美容師仲間の紹介で銀座カラー金山店に行くことにしました。
同じ接客業という事でつい店員さんの仕事を細かく見てしまっていたのですが...
銀座カラーはとにかく仕事が丁寧！！！私も見習わなくてはいけないなと改めて思いました(笑)
アフターケアの施術メニューも充実していて、キャンペーン中の美肌潤美のお試しをしたのですが、お肌がモチモチで、毎日使いたいと思いセルフケア用のローションも買っちゃいました！ちなみに私は入念に保湿をしたいので「しっとりタイプ」を購入。今では肌荒れもしなくなり大満足です。
</p></dd>
</dl>



<!-- 北千住2nd店 -->
<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住2nd店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  北千住駅は乗り換えで毎日使っていて、仕事終わりに通える脱毛サロンを調べている時に、銀座カラー北千住2nd店のことを知りました。<br>
  ルミネやマルイのある西口側だし、ホームページでみた店内の写真が可愛いかったので、無料カウンセリングを受けてみることにしました。<br>
  カウンセリングをしてくれたスタッフさんがすごく親身に話を聞いてくれて、「ここなら自分の体を任せられる！」と思って契約しました。<br>
  もう何回か通っていますが、施術は丁寧だし、細かく声をかけてくれるので、毎回安心できています！<br>
  これからもしっかり通って、綺麗な肌を目指します！
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住2nd店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  冬の間、ムダ毛のことは全然気にしていなかったのですが、暖かかくなって薄着になっていくにつれてだんだんと気になってきたので、思い切って脱毛サロン通いを始めちゃいました。<br>
  銀座カラーはわりと行きたい時に予約が取れるし、1回あたりの時間も短くてサクッと通えるので助かっています。<br>
  しかも、スキンケアに詳しくない私に、スタッフさんが色々アドバイスをしてくれるのが嬉しい！<br>
  迷っている方はカウンセリングだけでも受けてみるといいですよ！
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住2nd店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  北千住には買い物によく行くので、ショッピングのついでに脱毛ができるのはとても嬉しいです。<br>
  店内はカフェみたいな内装でオシャレだし、スタッフの皆さんの対応はにこやかで癒されます。以前通っていた脱毛サロンはつくりが簡素で冬は寒いのが苦痛だったので、こだわったしっかりとしたつくりなのはポイントが高いです。<br>
  しかも、予約が希望通りに取れることが多いので、予定も立てやすくて助かってます。自分のスタイルに合わせて通えるので、無理なく自己処理のいらない肌を目指せますね！
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住2nd店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  大学の友達が銀座カラーに通っていて、「銀座カラーいいよ！」とおすすめされたので、一回話を聞くだけでもと思ってカウンセリングを予約しました。<br>
  もともと脱毛に興味があったのですが、痛くないかな？とか、高いんじゃないかな？とか不安なことが多くていっぱい質問をしちゃったのですが、全部に丁寧に答えてくれました。<br>
  金額も思ってたよりも安くって、思い切って通うことに決めました。<br>
  通うたびに肌が綺麗になっているような気がして、毎回通うのが楽しみです！<br>
  他の友達にもオススメしようと思います！
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住2nd店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 受付<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  職業柄、人と間近で接する機会が多く、ムダ毛の処理には気を遣っていたのですが、「自己処理に割く時間が短くなれば良いのに」と常に思っていました。<br>
  ある時に、銀座カラーの広告を偶然見かけて、気になって調べてみると口コミの評判が良かったので、銀座カラーに通うことに決めました。<br>
  スタッフの皆さんは細かいところまで気を配ってお声がけしてくださいますし、店内も清潔で、いつも落ち着いて施術を受けられています。<br>
  日々のケアについてもアドバイスをくださるので、いつも参考にしています。回数を重ねるうちに自己処理の時間が徐々に減ってきました。銀座カラーに決めて良かったと心から感じています。
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住2nd店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  友人の結婚式に出席する機会が増えてきて、自己処理では手の届かない背中の処理に悩んでいました。<br>
  せっかくなら、背中だけではなく全身綺麗にしたいと思い、銀座カラーで契約を決めちゃいました。<br>
  銀座カラーの全身脱毛は顔も含まれているのに1回で全身脱毛できるので、通うのが面倒くさくなりません！最近は背中も綺麗になってきたし、顔の脱毛も効果が出てきたのか化粧ノリが良くなった気がします。こうやって通うたびに効果を実感できているので、モチベーションを維持できています。<br>
  この調子なら、自分の結婚式でも焦らずに済みそうです！まだ予定はないですが（笑）
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住2nd店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  就職活動が終わったのを期に、社会人になる準備として、今銀座カラーに通っています。<br>
  私は結構ズボラなタイプで、ムダ毛の自己処理をさぼりがちだったのですが、何回か通っているうちに自己処理の必要がだんだんなくなってきて助かっています。<br>
  個人的には、パウダールームが綺麗なのが嬉しいポイント！<br>
  社会人になると自由に使える時間が今より減ってしまうので、学生のうちに通い始めて良かったなーと思っています！
</p></dd>
</dl>
<!-- //北千住2nd店 -->




<!-- AETA町田店 -->
<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="AETA町田店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 ダンサー<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  町田で遊ぶことが多いので、脱毛サロンも町田駅から近い銀座カラーのAETA町田店に通うことにしました。<br>
  露出した衣装を着る事が多いので、全身つるつるにしたいと思っていましたが、銀座カラーは全身と顔がコースに全て含まれているので追加料金もいらないし希望通りのコースです。<br>
  お店に通い始めてからどんどん美意識が高まり、家でのケアもしっかりするようになりました。<br>
  周りの友達から肌のことを褒められますが、銀座カラーのおかげです!!
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="AETA町田店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務職<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  女子会で「結婚前にやっておいたほうがいいこと」の話になったとき、結婚している友達がこぞって〝脱毛〟って答えていました。私も脱毛サロンに行ってみたいなとは思っていましたが、なかなか勇気がでなくてそのままに…。<br>
  でも今回友達の話を聞いて勢いがつき、やっと無料カウンセリングの予約をしました!<br>
  最寄りの町田駅には、銀座カラーが2店舗ありますが新しいAETA町田店へ行くことに。駅からのアクセスは抜群、店内は女性好みのかわいい作り、スタッフの方はみんな優しくて素敵…いいことづくしでテンションが上がりました。<br>
  不安なこともしっかり相談にのってもらえて、自分に合ったコースを契約できたので通うのがすごく楽しみです!!
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="AETA町田店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 保育士<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  いつも自分でムダ毛の処理をしていますが、少しさぼってしまった日に園の子供から「先生触ると、ちくちくするー」っと言われてしまいました…。<br>
  子供は、正直ですからね。傷つきましたが、いい機会なので脱毛サロンに行ってみようかなって思ったんです。<br>
  姉が銀座カラーに通っていたこともあり、私も銀座カラーAETA町田店に決めました。契約時には、姉からの紹介だと話すと割引もありましたし、紹介者の姉にも特典がついたのですごくラッキーでした!!<br>
  お店の立地がいいし近隣にお買い物スポットが多いので、私は休日に楽しみながら通っています。<br>
  もうチクチクで嫌がられることはないので、自信をもって子供達と触れ合いたいと思います。
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="AETA町田店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務職<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  小さい頃から毛が濃いのが悩みだったので、ボーナスがでたお祝いに銀座カラーで脱毛することにしました。カウンセリングを受けるまでは、すごく高額なのかと思っていましたが意外にリーズナブルなことに驚きました。<br>
  美肌潤美のミストをオプションでつけても想定金額内だったので、自分へのご褒美に全部契約しちゃいました♡<br>
  まだ数回しか施術に通っていませんが、少しずつ肌の調子がよくなってきているので今後の変化も期待大です!!<br>
  今までの悩みが解消されて、銀座カラーさんには本当に感謝しています。<br>
  そして、これからも宜しくお願いします!!
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="AETA町田店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  大学の入学祝いに銀座カラーで契約をしてもらいました!!入学祝いに脱毛なんて珍しいと思いますが、ずっと夢だったんです。<br>
  高校までの学生時代って、プールの授業があるからムダ毛のことが気になって仕方ありませんでした。<br>
  思い切って母にそのことを相談すると、大学生になったら脱毛サロンに通わせてくれると約束してくれました。<br>
  先日やっと約束の日を迎えられたので、嬉しくてたまりません!!母も、学割が適用されてお得になったので喜んでいました。<br>
  AETA町田店は学校の近くにあるので、授業のスケジュールに合わせて無理なく通えそうです。大学生活、思い切り楽しみまーす!!
</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="AETA町田店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 美容師<br></span>
</dt>
<dd itemprop="reviewBody"><p>
  住んでいるのは多摩ですが、町田の美容室に務めているので休日もよく町田に遊びに行きます。AETA町田には友達とカラオケをしに行きましたが、その時銀座カラーが入っていることを知りました。ネットで検索したらカウンセリングは無料と書いてあったので、無料の言葉につられてすぐ予約しちゃいました!<br>
  ノープランで行きましたが、カウンセリングを担当してくれた方がとても丁寧に説明してくれたので、私の美意識に火がつき即決しました!!<br>
  私はもっと早くカウンセリングに行けばよかったなと思ったので、迷っている人にはぜひ早く行くことをオススメします。
</p></dd>
</dl>
<!-- //AETA町田店 -->




<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="広島店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>大学生になって、サークルの集まりで海やパーティなど肌を露出する機会が増えました。女子会でもムダ毛の話で盛り上がることが多くなり、半数以上はサロンに通っているとのこと。みんなに乗り遅れたくない！と焦って銀座カラーのカウンセリングに行きました。予算内の提案や肌ケアのアドバイスなどしてもらい、もっともっといろいろ教えてもらいたい！と思ったので、こちらに決めました。お金との兼ね合いもあり、一番気になっているワキと脚からはじめています。いつか全身制覇することが目標です！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="広島店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 保育士<br></span>
</dt>
<dd itemprop="reviewBody"><p>仕事が忙しくてついムダ毛処理を忘れてしまう毎日。とうとう母に、もう少し気を使いなさいと叱られてしまいました。ならばいっそのこと、サロンに行ってしまおう！と、めんどくさがりな私はカウンセリングに行ってみました。すると、肌の状態をチェックしてくれたり、スキンケアの大切さなど丁寧に教えてくれるので、やる気が出てきました！なので、サロンで脱毛すれば自己処理しなくてもいい生活も夢じゃない！と、銀座カラーに通うことに。照射の痛さは軽くパチパチするくらいで私は気になりません。めんどくさがりな人こそ、楽をしてキレイになりたいと思っているはずなので、ぜひ試してもらいたいです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="広島店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>東京にいた頃は銀座カラーさんを利用していたんですが、広島に転勤することになってからは脱毛をお休みしていました。他のサロンに変えようかとも考えていましたが、当時気にしていた毛が目立たなくなってある程度満足していたこともあり、そのままにしていました。ところが、広島店がオープンすると知り、密かに考えていたVIOにチャレンジすることに。銀座カラーの脱毛効果は自分の毛で知っているので、何のためらいもありませんでした。広島のスタッフも親切な方ばかりなので、またはじめてよかったなと思います。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="広島店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 アパレル<br></span>
</dt>
<dd itemprop="reviewBody"><p>私は、小学校の頃から毛が濃いことがコンプレックスで、抜いたり、脱色したり、ワックスを使ったりと色々試していました。でも、これには終わりが無いので、いつかサロンで脱毛がしたいとずっと思っていました。念願のアパレルの仕事にも就いてお給料も安定し、やっと自由に使えるお金ができたので、以前から広告を見て気になっていた銀座カラーだ！と思って、母を連れて相談へ。スタッフさんの印象がとてもよかったので母の承諾も得て、心に決めていた全身脱毛を申し込みました！その後、友達と脱毛の話になって、全身が60分だと言うととっても驚いていて（笑）。初めてだったので、そんなものなのかなと思っていたんですけど、すごいことだったんですね！銀座カラーに行ってみてよかったです！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="静岡店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>私は顔のうぶ毛がコンプレックスで、カミソリで処理していたんですが、怪我をしたりカミソリまけをしたりすることがあるので、ちゃんと脱毛がしたいなと思うようになりました。そんなことを先輩に相談したら、銀座カラーを紹介して頂きました。初めてのサロンで、上品な雰囲気にちょっと緊張してしまいましたが、スタッフの方が丁寧に案内してくれたので安心できました。通ううちに気付いたのですが、顔の明るさと化粧ののりが変わったなと実感するようになったので、行ってみて本当によかったなと思いました。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="静岡店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>プールに通っているんですが、自己処理が甘くて水着に着替えてから発見することがたまに…。友人に相談したところ「脱毛サロンに通っているから、そもそもそんな悩みがない」と言われました。確かに脱毛サロンなら確実そうなのですが、料金が高いとか広告は安くても行ったら勧誘されて高いものを売りつけられるとか、悪いイメージがあり近づきませんでした。友人が「興味あるならカウンセリングだけでも受けてきたら？」と言うので、恐る恐る銀座カラーに行ってみることに。結果、押し売りをしてくるようなことはないし、金額面でも無理のない範囲のプランを提案してくれたので、通うことに決めました！全く後悔していませんし、むしろもっと早く相談して置けばよかったなというのが正直な気持ちです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="静岡店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 主婦<br></span>
</dt>
<dd itemprop="reviewBody"><p>ママ友と噂をしていた銀座カラー。独身の頃に他のサロンでワキ、腕、脚はやっていたのですが、最近どうしても指毛が気になりだして…ママ友と一緒にカウンセリングに行ってみようということになりました。金額も夫に怒られない範囲でしたし、幼稚園の送り迎えの時に通えそうだなと。気にしていた痛みも大したことないようで、手元だけだからあっという間に終了するので、ネックになることが何もありませんでした。こんなに気軽に指毛が消えてくれるならうれしいです！銀座カラーさん、ありがとうございます！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="静岡店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>姉が結婚することになったので、母に許可してもらい気になっていた銀座カラーに姉妹で通うことにしました。姉はドレスのために背中と腕を、私はずっと気にしていたワキと脚をやってもらうことにしました。脱毛は憧れていたし、店内もかわいいので毎回行くだけでテンションが上がっちゃいます！姉の施術箇所はキレイになってきて、背中のブツブツも目立たなくなってきました。私もシェービングの回数が減ってきたので、処理がとても楽！私も結婚する前に背中までキレイにしたいので、少しずつお金をためて、いつか全身ツルツルにしたいです。</p></dd>
</dl>

<!--
<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金沢駅前東店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 受付<br></span>
</dt>
<dd itemprop="reviewBody"><p>脱毛の重要性を実感したのは、ウェディングドレスを試着した時です。ネットで選びに選び抜いたドレスを着て、主人に見せたその時！ボソッと言われたんですよ、「意外と…目立つんだね…背中…」って！！自分でも鏡を使って見てみたら、真っ白くてツルツルの生地からのぞいた私の背中には、想像以上の産毛が！大至急、会社の近くの銀座カラー金沢駅前東店を予約しました。金沢市内しかも北陸ではまだここだけだったので、すぐ予約が取れるか不安だったのですが、すんなり予約が取れました。最新のマシンを使っているので、1人あたりの施術時間が短縮されたんだと聞いて納得しました。金沢でも東京と同じレベルの施術を受けられるなんて！おかげさまで、お気に入りのドレスで挙式できました。</p></dd>
</dl>
-->

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金沢駅前東店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 ネイリスト<br></span>
</dt>
<dd itemprop="reviewBody"><p>ネイリストという職業上、お客様の手を見る機会が多いのですが、せっかく爪をキレイにしたのに指毛が気になる方もいて…。お客様の指毛を気にしていたら、自分の手がとっても気になるようになりました。それから脱毛クリームやシェービングなどで処理を試みたのですが、どうやら私の指毛は多いし太いようで、毛穴がプツプツ目立ってしまって…。他人は気付かないかもしれませんが、やはりお客様の手本となる完璧な手元にしておきたいので、毛穴を目立たなくしたい！と強く思うようになりました。今では、銀座カラーのおかげでだいぶ目立たなくなってきたのでひと安心です。もっと早く気付いてやっておけば、あんなに悩まなくてもよかったのになぁと今更ながら思います。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新潟万代シテイ店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 美容師<br></span>
</dt>
<dd itemprop="reviewBody"><p>まだスタイリスト見習いだった頃に、電車内の広告で銀座カラーを知りました。もっともっと身だしなみには気をつけないと！と思っていたところだったので、運命の出会いですね(笑)。ワキと腕と足でいいかな？VIOはどうしようかな？お金足りるかな？と、いろいろ迷っていたら、予算と施術したい箇所に合うコースをいくつか紹介してくれました。私は痛がりなので、初回はかなりドキドキ。でも実際やってみたらパツッ！という感じの、はじかれたような感覚があっただけ。自分でもツルスベになっていくのがわかるので、サロンに通うのが楽しくなってきちゃいました。ここまで手ごたえがあると、もうやめられません～。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新潟万代シテイ店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>客先に向かう途中で派手に転んでしまい、病院に行ったんです。詳しく検査をすることになり、着替えをしていた時に、足やVIOの処理をさぼっていたことを思い出したんです！普段はパンツスーツが多いし、忙しいし、疲れているし、眠いし。もともとズボラなこともあって…毛の処理をさぼってました。看護師さんもお医者さんも口には出さないけど、「この子、だらしないなあ」って見られているようで恥ずかしくて…。時間を巻き戻せたらいいのに！って思いました。同じ会社の子に話したら、銀座カラーのスタッフさんがすごく親切だって教えてくれたので、すぐにWEBから予約。ズボラな私でも続けられるようにアドバイスしてもらえました。仕事帰りに行きやすい場所にあるので、とても助かっています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新潟万代シテイ店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 デザイナー<br></span>
</dt>
<dd itemprop="reviewBody"><p>お試しで何件か脱毛体験はしていたのですが、そろそろ本格的に脱毛がしたいと思い銀座カラーに通いはじめました。どうせやるなら、しっかりキレイにしたかったので、全身脱毛と保湿ケアを選びました。施術時間は本当に一時間程度で、スタッフの方は丁寧なのに手際がいい！さらに保湿ケアをしてもらったら、自分のお肌じゃないみたいにモッチモチに！時間がきっちりしていて、パウダールームもあるから、お出かけ前でも利用できそうだなと思いました。早くツルツルになって友達に自慢したいです！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新潟万代シテイ店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>学校で噂になっていた銀座カラー。脇毛を剃ってもプツプツしていたり、黒ずんだりしてしまっているところなどが気になっていたので、オープン後すぐに相談にいってきました。プツプツしているところは毛穴が原因で、脱毛をすると毛穴が引き締まってきれいになること、自己処理のメリットとデメリットなど、私が気になっていることを丁寧に教えてくれました。何より、サロンの雰囲気が可愛らしくて、すぐに気に入っちゃったんです！まだ始めたばかりですが、2週間後スルっと毛が取れる感じが快感で、次が楽しみです！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新潟万代シテイ店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>夫の転勤で新潟に引っ越してきたので、サロンを新潟万代シテイ店へ変更してもらいました。新店舗ということもあって、店内はとってもキレイ！施術の質も他店舗と変わらず丁寧＆スタッフの方も話しやすくて大満足です。銀座カラーの技術と雰囲気が気に入っていたので、タイミングよく転勤先に新店舗ができて、本当に運がいいなと思いました！私は理想の肌にだいぶ近くなっているのですが、新潟でお友達ができたらオススメしようと思います。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="つくば店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>ゼミ旅行で温泉に行ったときに、友達の背中がツルツルでビックリ！話を聞くと、「脱毛サロンに通ってるんだー。」って教えてくれました。今まで自己処理だけだったんですけど、サロンで脱毛したら楽になるなーと思って、旅行から帰ったらすぐにネットで調べました。すると、つくばに銀座カラーができてることを発見！茨城県だと水戸にあるのは知ってたのですが、つくばにもできてるなんて！早速つくば店で無料カウンセリングを予約しました。カウンセリングをしてくれたスタッフさんがすごく親切で、脱毛について丁寧に説明してくれたのでとっても安心しました！まだ通い始めたばかりですが、みんなに自慢できるくらいにツルツルなお肌を手に入れたいと思います！</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="つくば店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>大学生になるのをきっかけに銀座カラーで脱毛を始めました！今まではカミソリをつかって自分で処理してたんですけど、キズができたり、荒れちゃったりで、毛が生えてこなければいいのに～って(笑)脱毛ってすごく高いイメージがあったんですけど、銀座カラーだと20歳未満は都度払いが利用できて、アルバイト代でやりくりしながら通えちゃうので助かってます！つくば店のスタッフの方は美意識が高くてお肌もきれいで、脱毛についてだけじゃなくて、美容についても色んなアドバイスをもらっています(笑)今ではノースリーブみたいな肌の出る服を着れる季節が楽しみです！</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="つくば店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>昔から毛が濃いのが悩みでした。脱毛したいとは思っていたのですが、強引な勧誘をされるんじゃないかとか、思った以上の金額になってしまうんじゃないかとか、不安が多くてなかなか踏み出せないでいました。あるとき、友人から銀座カラーを紹介してもらい、話を聞いてみると、とても満足している様子だったので、おそるおそるカウンセリングに行ってみることにしました。銀座カラーは本当に強引な勧誘はなく、私に合わせたコースを提案してくれました。価格も思っていた金額より安いくらいだったので、契約を決めました。最近は目に見えて毛が少なくなってきました。背中を押してくれた友人と、長年の悩みを解決してくれた銀座カラーに感謝です。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="つくば店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>私が今住んでいる茨城県南部のあたりには今まで大きな脱毛サロンがあまりありませんでした。銀座カラーができたことを知って、試しにカウンセリング予約をしてみました。下調べで、ホームページを見て全身脱毛の金額を確認したのですが、本当に全身くまなくやってくれるのか、思わぬ追加料金がかかってしまわないか心配でした。でも実際につくば店に通ってみると、全身余すところ無くやってくれるし、追加料金や強引な勧誘もなく、安心して通えます。周りの人たちにもお勧めしています。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="つくば店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>仕事が忙しく、ムダ毛の自己処理にあまり時間を割けないのが悩みでした。脱毛サロンで脱毛すれば楽になるかもと思い、いろいろなサロンのサイトチェックしてみて、その中でも銀座カラーの全身脱毛が気になり、カウンセリングに行ってみました。他のサロンでは全身を複数回に分けて脱毛するところもあるようですが、銀座カラーは1回で全身脱毛ができるのでわずらわしさがありません。それに1回当たりの時間も短く、予約も取りやすいので、スケジュールが立てやすいのも助かっています。銀座カラーは「女性のために」を体現した脱毛サロンだと思います。女性の頼れる味方です。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="つくば店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>最近、知人の結婚式でドレスを着る機会が多くなってきました。ドレスを着たとき背中が出るので、キレイにしておきたいなーと思い、友達に銀座カラーをオススメしてもらって通い始めました。友達は学生時代に都内で通っていたみたいですが、茨城県にも銀座カラーがあるとは！つくば店は家から近いし、駅からも近いので通いやすいですね。背中は自己処理が難しいので大変助かってます。また、オプションの保湿ケア、美肌潤美のおかげでお肌がもちもちになってきました。これでまた結婚式の案内が来ても安心です！いつか分からないですが、自分の結婚式のときも焦らなくて大丈夫ですね(笑)</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="つくば店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>肌寒くなってきたある日の朝、急いでストッキングをはいて出かけたら、毛が濃いのでストッキングの上からでもすね毛が丸見え…友達にも笑われてとても恥ずかしい思いをしました。ズボラな私は自己処理をサボりがちなので、脱毛サロンでの脱毛に頼ることにしました。つくば店に通っているのですが、内装がオシャレで、スタッフさんも皆さんキレイ！駅から近くて通いやすいので、億劫になることもありません！自己処理から開放されて晴れ晴れとした気分です！あの時笑ってきた友人にも、「銀座カラーいいよ！」ってオススメしておきました！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="水戸駅前店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>物心ついた時から、体毛が濃いことがコンプレックスでした。そんな私の様子を見た母が、ネットで色々と探してくれて大学の合格祝いに銀座カラーに連れて行ってくれたんです！私の場合、自己処理で肌が傷んでいたみたいなのですが、スタッフの方のアドバイスで肌のコンディションを整えることから始めました。通うたびに毛が目立たなくなっていったので自己処理の回数も減り、今では肌の調子もだいぶ良い感じです。毎朝シェービングに費やしていた時間も無くなったので、連れて行ってくれた母にはとても感謝しています。私と同じように毛の濃さで悩まれている方は、一度相談に行ってみたらいいのに！って思っています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="水戸駅前店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 秘書<br></span>
</dt>
<dd itemprop="reviewBody"><p>元々肌の色が薄いため、どうしても脚の毛が目立ってしまうことが私のコンプレックスでした。ストッキングで隠したり、脚用ファンデを使ったりしていたのですが、いつかちゃんと脱毛したいなと。仕事にも慣れてきて、やっと自分のペースで仕事を進められるようになったので、念願のサロンに通うことにしました！ずっと行ってみたかったので迷いや不安はあまりなく、むしろワクワクしていて（笑）。実際、そのワクワク感に裏切りは無く、以前より毛が気にならなくなり、自己処理のペースも減りました。実は最近、もっと欲が出てきて腕も顔もやりたいなと思うようになり検討中です…どうせなら、初めから全身やっておけばよかったかな（笑）。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="水戸駅前店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>休日まったりしていたら、「お前、ひげが生えているぞ(笑)」と急に夫に言われました。それ以来、口の周りの産毛の量が気になりはじめて…。毛のサイクルも以前より早くなったような気がして居ても立っても居られなくなり脱毛サロンに通おうと決心しました。でも、ひげが悩みだなんて恥ずかしくて言いづらいなぁと思っていましたが、スタッフの方はとても親身に対応してくださり、雰囲気もとてもよいところなのでお世話になることにしました。また、サロンというと勧誘がすごいイメージがあり少し怖かったのですが、そんなことは無く、私に合ったコースを提案してくれました。お話もとても勉強になるので、毎回楽しく通わせていただいております。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="水戸駅前店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>20代半ばから急に結婚式ラッシュが始まり、露出が多い服を着る機会が増えました。特にお気に入りのワンピースは背中が開いているので、妹にシェービングを頼んでいました。ある時タイミングが合わなくて自分でやってみたんですが失敗してしまい、傷跡をつくり余計目立ってしまうことに…。これからも何件かパーティがありますし、いずれは自分の結婚式でも背中の毛を気にすることなくドレス選びたい！と思ったので、銀座カラーさんに通うことに決めました。通いだしてから知ったのですが、結婚式の1カ月前の人は施術してもらえないとか。残念ながらまだ結婚の予定はありませんが、今から始めていてよかったなと思いました。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="高崎店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>お客様とすごく近い距離で商談をすることが多々あるので、最近、手の甲や指の毛が気になり始めていました。私自身が恥ずかしいのもそうですが、万が一、処理し忘れた手を見られて、お客様にマイナスイメージを与えてしまうのも嫌で…。同僚に相談してみると、以前銀座カラーで脱毛していたとのことで、私も通うことにしました。店内は清潔感があってオシャレで、脱毛のお手入れは痛みもなくてリラックスして受けられます。キレイになっていく自分の肌を見て自信がついてきました。おかげ様で仕事もプライベートも充実しています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="高崎店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>仕事中、パソコンを使っている時に腕まくりをするのですが、ふと伸びをしたときに腕から処理をし忘れた毛がそよそよ…これはマズいと思って脱毛することに。腕だけ脱毛するつもりでカウンセリングに行ったのですが、アウトドア派の私は、夏に海やプールに行くのが好きなのでvio、脚やワキも…と要望が膨らみ、結局後から足すよりもおトクなので、イッキに全身脱毛をすることにしました。高崎店は群馬県最初の店舗だそうで、新しいもの好きの私にはぴったり！まだ通い始めたばかりですが、脱毛後に少し経ってから毛が落ち始めて、ちょっとずつ効果を実感しています！今から夏が来るのが楽しみ～！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="高崎店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>就活が無事に終わって、社会人になる準備として、銀座カラーで脱毛を始めました。お店は私が通学で使っている高崎駅の近くにあるのでとっても通いやすいです！脱毛自体、もっと痛いのかなーと思っていましたが、あたたかい程度で私は全然気にならなかったです。オプションでつけたアフターケアの美肌潤美も気持ちよくてお気に入りです！銀座カラーで脱毛を始めてよかったです！周りの友達にも銀座カラーをおすすめしています！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="高崎店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>サークルの先輩と女子会をしているときに、お肌のケアの話になって、どうしてるのか聞いてみると「脱毛サロンに通ってるんだー！」って。脱毛サロンは社会人になってお金に余裕がある人が行くところだと思い込んでいたので、思わず「えっ！？」って言っちゃいました(汗)詳しく聞いてみると銀座カラーに通っているとの事。広告でみたことあるから私でも知っていました。自己処理が面倒くさいと感じていたのでいい機会だと思って、私も脱毛を始めちゃいました！銀座カラーは学割や都度払いがあって学生にも優しい！バイト代で無理なく通えるので助かってます！スタッフさんは良い方ばかりでお手入れ中はリラックスできちゃいます。高崎店は通学の定期圏内で駅からも近いので通いやすいです！後輩ができたら先輩としてオススメしたいです！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="高崎店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 美容師<br></span>
</dt>
<dd itemprop="reviewBody"><p>仕事中、腕まくりをしている時間が多いので、腕のムダ毛処理は特に気を使っています。自己処理はシェーバーでやっているのですが、時間がかかったり、たまに傷つけてしまったりするのが悩みでした。しかし、群馬県にも銀座カラーができたという話を聞いていくことにしました！銀座カラーに通い始めてから、毛が気にならないぐらいまでになってきました。おまけに美肌潤美のおかげで、乾燥肌が改善してきた気がします！私自身満足度がすごく高いので、同じ美容室の子にも銀座カラーの脱毛をオススメしちゃいました！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="高崎店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>アルバイト先が高崎駅なので、高崎駅で脱毛できるサロンを探していたところ、銀座カラーを発見し、通うことに決めました！銀座カラーは予約が取りやすくて、アルバイトのシフト前やシフト後に効率的に予約を入れてスケジュール管理ができました。脱毛に痛みはほとんどないし、スタッフさんと楽しくお話ししてるとあっという間に終わっちゃう感じです！バイト仲間にもオススメしてます！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="高崎店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>私は顔の産毛の処理をシェーバーでやっていたのですが、冬場すごく乾燥するようになったり、切れてしまったり、トラブルが絶えませんでした。ネットで情報収集していると、脱毛すると自己処理による負担が減り、顔の肌質が良くなると言う情報をみて脱毛を始めました！産毛がなくなり始めて、化粧のノリが格段に良くなりました。アフターケアのミストも気持ちよくって、赤ちゃんみたいな触りたくなる肌になった気がします！顔の自己処理でお悩みの方にオススメしたいです！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷道玄坂店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 デザイナー<br></span>
</dt>
<dd itemprop="reviewBody"><p>育休が終わって職場復職するので、子どもの送り迎えで利用している渋谷駅周辺でよい脱毛サロンはないかなと探していました。サイトの口コミやママ友の情報を参考にしながら、いくつか脱毛サロンのカウンセリングを受けたのですが、新しくてキレイでリーズナブルなところとスタッフさんの対応が気に入り、銀座カラーの渋谷道玄坂店に決めました。子どもの送迎や通院などに合わせて予約ができるので、とても便利で助かっています。全身と顔をやってもらっているのですが、最近顔色がワントーン上がったようで、ママ友に褒められました！お化粧のりもいい感じで、時短できて助かっています。キレイな働くお母さんを目指して、これからも通い続けます。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷道玄坂店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 フリーター<br></span>
</dt>
<dd itemprop="reviewBody"><p>昨年、学生時代の友人たちと温泉旅行に行くことになり、みんなで温泉に入ったのですが、みんなのアンダーが薄く、自然のままの自分の姿が恥ずかしくなり銀座カラーのカウンセリングを受けることにしました。VIOを処理しているなんて芸能人か意識高い系の人だけかと思っていたけど、普通の子もサロンで処理しているんですね…どうせやるなら、VIOだけではなく全身をやりたい！と探してはみたものの、どこも高く、友人が通っているサロンではなかなか予約ができないから大変だとも言われ…。そんな時目に入ってきたのが銀座カラーの広告で、スタッフの人も感じがよかったのでここに決めました。月々の金額も手が届かないわけでもないし、予約も割りと思い通りに取れ、友達にはじめからいいサロンでずるいと言われてしまいました（笑）これからもよろしくお願いします！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷道玄坂店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>平日のお休みが取りづらいので、駅から近いことと予約の取りやすいところが決め手になりました。全身脱毛が1回60分程で終わるのは、本当に助かります。さらに、HPには痛くないとは書いてあるものの、他のサロンで痛かったので気になっていたんですが、痛さはあまり感じず、ひと安心。そして、何よりお気に入りなのは、オプションで付けた保湿ケアです！以前通っていたサロンでは施術後乾燥しがちだったのに、今は施術前より肌がモチモチになっているので思わず自分で触ってしまいます。自己処理をついサボってしまうズボラ女子なので、早く毛量が減り、女子力が少しでも回復することを期待しています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷道玄坂店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 アパレル<br></span>
</dt>
<dd itemprop="reviewBody"><p>お店に入ってすぐ、雰囲気が気に入りました。新店舗ということもあるかもしれませんが、キレイでおしゃれ！やっぱり、同じ脱毛をするなら、気分が上がるところがいいですよね！しかもお部屋ごとに壁紙が違うみたいで、私が入ったのは淡いブルーの部屋とシックなベージュの部屋でした。どちらも居るだけで女子力がUPしそうな空間で、他にどんなお部屋があるのか知りたくなりました！それにスタッフの方の対応や金額、サービスも魅力的で…。私も接客業なので、やっぱり対応とか気になっちゃうんですよね。でも、心配事や質問にもきちんと答えてくれたので、ここなら安心できるなぁと思いました。まだ始めたばかりですが、母が気になると言っていたので今度一緒に行ってみます！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="仙台店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 美容師<br></span>
</dt>
<dd itemprop="reviewBody"><p>顔の産毛が濃い家系なので、物心ついた頃1週間に1回はシェービングしていました。ヒゲみたいですね（苦笑）。ニキビができると剃りにくいし、あたると痛くてキズになるし。マスクで隠すわけにもいかず、なるべく目立たない肌色の絆創膏を貼っていたら、お客様に「大丈夫？」って心配されてしまって。それがきっかけで顔の脱毛をしてみようと、職場や友人に聞いてまわったんです。結果、おすすめの声が一番多かったのが、銀座カラー仙台店でした。駅前にあるので、脱毛後にお買い物できるのが嬉しいポイントです。周辺には、美味しいご飯が食べられるお店もたくさんあるので、ついつい寄り道しちゃいます。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="仙台店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>私はズボラな性格なので、ムダ毛に対しては、外出するときに恥ずかしくない程度に仕方なく処理orパンツや長袖でごまかしていました。でも、よく考えてみれば、脱毛して目立たないようにしてしまえば、そもそも処理しなくていいということに気付いたんですよね。幸運なことに帰り道に銀座カラーさんがあるので通うことは苦ではないし、施術中の痛さもほぼ無いので落ち着いた部屋でひと休みする感覚で通っていました。今では特にムダ毛が気にならないのでほぼ放置しています。ズボラ度が増してしまうのでは？と思っていたんですが、ムダ毛を気にせずファッションを楽しむようになりました！ズボラな私にも親身になって対応してくれ、自信が持てる肌にしてくれた銀座カラーさんに感謝です。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="盛岡菜園店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 アパレル<br></span>
</dt>
<dd itemprop="reviewBody"><p>以前は、脱色剤で自己処理していたのですが、脱色中にテレビを見ていたらうっかり忘れてしまい、水ぶくれになってしまったことがありました。それ以来、脱色剤はやめてシェービングで処理していたのですが、たまにカミソリまけしてしまうのが悩みで…。そんな時、同僚に誘われて行ったのが銀座カラーさんでした。HPでは安そうだけど、実際はどうなのかな？とカウンセリングに行ってきたのですが、店内は落ち着いた雰囲気でコースもいろいろあり、自分に合ったものを見つけることができたので通うことにしました。メイクブースもあるので、デートの前でも安心ですね。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="盛岡菜園店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>進学で盛岡に出ることになったのがきっかけで、銀座カラーに通い始めました。正直、痛そうだし怖かったけど、スタッフさんは親切だし少しずつ様子を見ながらやってもらえるので、全然平気です。コンプレックスだった腕の脱毛を始めてみたら、毎日のシェービングがとても面倒で時間の無駄だったことに気づきました。最初は不安でしたけど、通い始めて本当に良かったです。これからも絶対続けます。私たち学生の間でも、サロンでムダ毛のお手入れをすることは常識。どんなサロンが良いかなどよく情報交換しているので、私は銀座カラーを薦めています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="札幌店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 歯科衛生士<br></span>
</dt>
<dd itemprop="reviewBody"><p>患者さんに、数十センチの距離で顔や手先などを見られる仕事なので、顔や指、腕のケアにはシビアになりがち。マスクやグローブ、制服で隠れていますが、ちょっとした動作でずれてしまった時とかに剃り残しがバレてしまうこともあるので油断は禁物です。忙しくなるとサロンに通う時間も限られてしまうので、平日21時までというのも大きなメリットだし、施術時間も他のサロンより短いんじゃないかな？　施術後、そのまま食事に行けるのも、食いしん坊の私には大きな魅力ですね。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="札幌店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>同じヨガ教室に肌がきれいな女性がいるんです。気になって、どこで・どんなお手入れをしているのか聞いてみたら、銀座カラー札幌店の常連さんでした。早速、サイトから予約しました。行きつけのサロンを変えるのは勇気が必要だったけど、後悔していません。最初のカウンセリングが、とても親切だったので安心できたし、なにより自己処理で黒いブツブツができていた両ひじの下が、今はツルツルに！　この間なんか、会社の後輩ちゃんから「○○さん、いつも肌がきれいですよね。どこに通っているんですか？」って聞かれちゃいました。今度は私が見られる番！？ (*/∇＼*)</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="札幌店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>いつか脱毛したい！と高校の時からずっと思ってました。 そして今年から大学に入学し、バイトも始めて自分でお金を少し稼げるようになったのでついに始めようと決心しました。 色んな脱毛サロンをインターネットで調べたのですが、なによりも銀座カラーさんはすすきの駅すぐ近くにあった事が最終の決め手となりました。バイト先がすすきので、大学からも自宅からも南北線直通で来れるし私にとっては立地最高！あと、ホームページが可愛かったので惹かれてしまいました（笑）<br>
正直初めての脱毛で、痛かったらどうしようとかなりドキドキしていたのですが、想像よりも全然痛くなかったです。<br>
私は乾燥肌も悩みの種ですが、銀座カラーさんは保湿ケアもしっかりした施術をしてくださったので、いつもより腕とか乾燥がマシな気がします。<br>
まだこれから長くて寒い冬が来るのに、もうすでに半袖の時期を待ち遠しく思ってしまっています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="天神店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 ネイリスト<br></span>
</dt>
<dd itemprop="reviewBody"><p>天神に買い物に行くことが多く、そのついでに脱毛がしたいと思って銀座カラーを選びました。昔他店で両脇と足腕を脱毛したのですが、綺麗になりきる前に終わってしまって。ポツポツ生えてくるのが鬱陶しいのと、自己処理し忘れることがあるので、いっそのこと全身綺麗に脱毛しようと決意。広告で銀座カラーを見て、全身脱毛のお得なプランがあるのを知りました。期間限定プランと言われるとつい気になっちゃいます（笑）通ってみて思うのは、店内の内装のセンスの良さと、接客の良さですね。店内は清潔感がありながらガーリーな感じが本当に可愛い。スタッフさんはいつも元気で笑顔なので、こちらまでニコリとしちゃいます。もちろん技術力も申し分なしです。しっかり当ててくれるので当て漏れの心配もないですし、術後のケアや声がけもしっかりされていて好感度アップです。これからも最高のサービスを期待しています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="天神店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>天神にはたくさん脱毛サロンがあるので、サロン選びが大変でした。でも、銀座カラーの天神店はオープンして新しいし、内装の雰囲気が他のサロンと違って凄い素敵！キラキラのシャンデリアに目を奪われてしまいました。よくよく友達の体験談なんて聞いてみると、銀座カラーの効果の高さや価格の手頃さに満足しているようで、他店を辞めて通っている子もいるくらいだから、人気のお店なんだろうなと。とりあえず話を聞きに行って、駄目そうだったら違うお店に行けばいいかと思って、カウンセリング予約を入れました。図解を使ったスムーズな説明プラス、スタッフさんの笑顔で私としては通う気満々だったのですが、人気店ならではの気になる点、予約が取れるかどうかというところがはっきりしなくては契約できないなと思っていました。実際予約を見たところ、結構埋まってはいました。ただ私は平日休みのこともありますので、複数候補日の中から予約を入れることができたので、予約の点は問題ないかと思いました。これから施術になりますが、自分の肌の変化が楽しみです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="広島店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>ずっと気になっていたVIOラインを銀座カラーさんで脱毛しました。夏にプールへ遊びに行った時、一応インナーショーツは着用していたけれどやっぱり毛がはみ出てしまっているのではないかと不安で仕方がなかったです。Vラインは自分で少し剃刀を使ってお手入れをしてはいたのですが、I・OはVよりもさらにデリケートな部分になるし自分でやるのは怖かったので一切触らずにいました。しかもちょっと人には相談しにくい部分でもありますし、Vラインも含めて毛の量や形についてどうしようと迷っていたのでスタッフさんのアドバイスを聞きながら自分に合ったコースを選択しました。やっぱり専門の方に相談してよかったです。絶対次の夏までにはVラインの心配がないくらいまで通って、不安なく思いっきり遊びたいと思います。脱毛が完了したら新しい水着を買いたいなと思っているので今からもう楽しみです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="広島店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>以前東京に出張にいった際に、電車の中や看板で真っ赤な赤ずきんが目に入りました。よく見ると脱毛サロンの広告でした。東京はやっぱりおしゃれだなあと思っていたのですが、それからしばらくして、八丁堀へ遊びに行った時なんとあの看板を発見。聞けば最近できたばかりということで、なんだかうれしくなりました。<br>
私は別の脱毛サロンに通っていたことがあり、脱毛経験者だったのですが、月日が経って少しずつまた脇毛が気になり始めていました。これはいいきっかけだと思いすぐに無料カウンセリングを予約。店員さんは丁寧で明るく、私と同世代だったので話しやすかったです。また、料金の方は私が以前通っていたところよりも安い。もっと前から銀座カラーさんが広島にあれば全身脱毛ももっと安く済んだはずなのに…とすこし悔しい思いをしながらも、脱毛未経験の同僚にいいサロンないかと相談されたので銀座カラーさんを紹介しました。そしたらなんと紹介キャンペーンで私も同僚にも特典が！脇もつるつるになって、店員さんも良い感じで、特典も得られていい事づくめでした。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名古屋駅前店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>好きな人が出来て、綺麗になりたい、可愛くなりたいって思ったんです。その人はバイト先の先輩で私はいろいろ教えてもらう機会が多いからか、今までは全く気にしなかったのに自分の顔のうぶ毛、とくに鼻下が気になり始めました。接近されたときにどうしても剃り残しとかないかが毎回不安で…<br>
思い切って銀座カラーで脱毛することにしました。噂には聞いていましたが、化粧ノリが見違えるくらいに良くなって驚きでした。自分で剃っていた時には剃刀負けして反対に荒れてしまうこともしばしばありましたので、やって本当によかったと思います。よく考えたら自分の肌に刃物を当てているわけですから、そりゃあ肌は傷つきますよね。一番見られるところなのにそういう意味では気を使っていなかったなと反省しています。好きな人とのことはまだ特に進展とかはないのですが、これからもっと自分を磨きたいと思っているので、今は新たに別の箇所も脱毛しようかと検討中です。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名古屋駅前店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">30代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>三重県津市から通っています。どうしても銀座カラーで脱毛したくて、一番行きやすい店舗がここだったので頑張って行っています。地元にも一応脱毛サロンはありますが、肌のことですからやっぱり有名なお店に行った方が何だか安心だと思い、銀座カラーにしました。県外ではありますが、アクセスも良くて助かっています。買い物をして帰るのも楽しみの一つです。私は全身ハーフ脱毛のコースで主に腕、脚、脇をお願いしています。個人的に気になっている部分は網羅していますし、料金にもかなり満足しています。脱毛し放題なので納得できるまで通いたいと思っています。実は一番嬉しかったのが、気になっていた脇の黒ずみが銀座カラーに通い始めてからなくなったことです。夏に半袖Ｔシャツでさえ着るのをためらっていたのですが、今年はなにも気にすることなくノースリーブの洋服も新しく買ったりしてお洒落をいっぱい楽しむことが出来ました！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名古屋栄店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 デザイナー<br></span>
</dt>
<dd itemprop="reviewBody"><p>名古屋にはたくさんの脱毛サロンがあります。私もかなり迷いました。インターネットで調べもしたし、職場の人の行ってた所はどんな感じだったかとか事前調査は結構したつもりです。その中でなぜ銀座カラーに決めたかというと、いろいろな要素はありますが一番は美肌潤美です。ただ脱毛するだけじゃなくて、高圧ミストマシンで様々な美容成分の蒸気を発生させ施術後のお肌に保湿をおこなってくれるというオプションメニュー。どんなに素敵な洋服を来ても肌の状態が良くなければ魅力は落ちます。もちろん普段から自己ケアはしていますが、どうせなら脱毛と一緒にできる美容というのも是非やりたいと思いました。実際やってみての率直な感想は「すごい！」の一言。これでもかというくらいのミストシャワーが出て肌を包んでくれます。そして照射後のお肌には心地よいくらいのひんやり感。もちもちすべすべのお肌になっていくのを実感しています。銀座カラーにしてよかったです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名古屋栄店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>友達が銀座カラーに通っていました。ある日「触ってみて」といわれ腕を触ると、つるつるすべすべ！その友達は特に寒い時期いつも乾燥肌に悩まされていたことを知っていたので、本当に驚きました。聞けば脱毛サロンで高圧ミストの保湿ケアをしてもらっているそうで、それのおかげで乾燥を防げているとのこと。私も興味を持ったので、とりあえず無料カウンセリングに行ってみようと予約をしました。友達がやってる美肌潤美という保湿ケアはオプションだったみたいで、最初はどうしようか迷いました。しかし当時全身脱毛のコースを契約した人は無料で1回体験できるキャンペーンをちょうどやっていて、そちらを試しにお願いすることに。いざ体験してみると終わった後のしっとり感ともちもち感が忘れられず、結局オプションで付けてもらうことにしちゃいました！お家ケア用の保湿ローションもあるので、サロンと自宅とダブルサポートで友達のすべすべお肌を追い抜きます（笑）</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名鉄岐阜駅前店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 保育士<br></span>
</dt>
<dd itemprop="reviewBody"><p>Facebookで友達が「いいね！」を押していて、銀座カラーの存在を知りました。東京のお店かな？お洒落なサロンだなあ、と思って見ていくと岐阜県に店舗あり。しかも家から結構近い所！ホームページものぞいてみるとキレイそうなので、とりあえず無料だしカウンセリングだけでも行ってみようと本当にただ興味本位で予約しました。私は全く脱毛について無知だったので、剃刀による自己処理が良くないってことも知らなかったですし、脱毛すると毛穴が閉じてお肌もきれいになるということも初めて知って驚きました。カウンセリングにいったその日には決められなかったのですが、スタッフの方は嫌な顔一つせず対応してくれて安心しました。帰ってから自分でもいろいろ調べた結果、脱毛サロンに通うのがやっぱりいいと思い、後日申し込みに行きました。今までいわゆる自分磨きにあまり興味を持たずにここまで来てしまったけれど、お肌の改革から徐々に始めたいと思います。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名鉄岐阜駅前店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>サークルの友達何人かにふと脱毛サロン行ったことがあるか聞いてみたところ、通っていたという人が思ったよりも多くてとても驚きました。実は毛が濃い方で、それがすごく嫌で頻繁に剃刀で自己処理をしていました。そのせいで剃刀で荒れてしまっていた肌もどうにかしたかったですし、もちろん根本の原因であるムダ毛の処理も楽になりたいとずっと思っていました。だから、知らない間にみんな脱毛サロンに行っていたと知り、なんだか取り残された気分になりました。今からでも遅くないと思い直し色々調べたり友達の意見を聞いたりして、決めたのが銀座カラーです。私の毛質的に脱毛し放題のコースは必須で、なおかつ施術時間も短いし、荒れたお肌のケアもできるコースが銀座カラーにはあったので言うことなしで決定でした。何度か脱毛に通っているとだんだん毛が少なくなっていくというか、細くなっているのか目立たなくなってきて、それが嬉しくて行くたびに次の施術が待ち遠しく思います。完全なつるつるすべすべお肌まであともう少し。濃い毛に悩んでいた私とはさよならです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="静岡店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">30代 主婦<br></span>
</dt>
<dd itemprop="reviewBody"><p>電車の中でつり革を掴んでいる時に目が合ったんです。女子高生と。しまった。処理してなかった…と気づきました。あまりにも恥ずかしくて、落ち込んで、その反動と勢いで無料カウンセリングを予約しました。平日の昼間旦那はいないし、子供も学校へ行っているのでその時間帯を選びました。自分でも正直今更脱毛サロン行くことに意味はあるのかと予約してから少し悩んだりもしましたが、やっぱりあの日のことが忘れられなくて。ドキドキしながら店舗へ行くと、内装はとっても綺麗で清潔感溢れる雰囲気で、スタッフさんも可愛い。更に緊張が増しました。しかし、カウンセリングは私のペースに合わせて話をしたり質問に答えてくださってよかったです。私と同じくらいの年代の人も通っているらしいと聞いてさらに安心しました。最後の心配の費用についてはパートで貯めたお金から払えるくらいリーズナブルだったので本当に助かりました。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="静岡店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 フリーター<br></span>
</dt>
<dd itemprop="reviewBody"><p>もともと腕や足の毛質はそんなに悩むようなものではなかったので、私は両脇だけの脱毛コースをお願いしていました。実は最後の施術の日、勧誘されるのでは…とちょっとドキドキしていました。私は両脇だけの脱毛コースでしたし、特にこういう場合ってだいたい別の部位の脱毛コースを勧誘されたり、美容品をおすすめされたりすると思っていたので。しかし、いつもの通りに施術が終了して、そのあとも心配していたようなことは一切なく終わりました。勝手に身構えていた分なんだか拍子抜けしてしまいました（笑）ホームページに書いてあった通り無理な勧誘もなく、表示値段以上のお金をとられることも一切ありませんでした。嫌な顔ひとつせず最初から最後まで丁寧にプロのサービスを徹底してくださって本当にうれしかったです。もし、また脱毛したいと思ったら私は絶対銀座カラーにします！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="三宮店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 アパレル<br></span>
</dt>
<dd itemprop="reviewBody"><p>普段仕事が忙しく休みもあまり取れないので、限られた時間のなかで通えるところを探していました。<br>
銀座カラーは全身脱毛1回の施術が60分くらいで完了すると聞いてこちらに決めました。そうはいっても実際は何回かに分けるのではないかと疑っていたのですが、本当に全身を1回60分程度で施術できて驚きでした。初めて行った日の帰りには気になっていた映画も観ることが出来て充実した休日を過ごせた気がします。急にシフトが変わってしまった時にも、24時間いつでもインターネットで簡単に予約可能だったのですごくよかったです。肝心な脱毛の方もばっちり。お風呂のタイミングで以前はムダ毛処理をしていましたが、今ではその時間も必要なくなりました。仕事は相変わらず忙しいけれど、つるつるのお肌をゲットできて身体だけでなく気持ちもすっきりして、モチベーション高く活き活きとした毎日を送れている気がします。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="三宮店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>脱毛して何よりも嬉しかったのが、彼氏に褒められたことです。褒められたといっても、「前よりもつるつるになったやん」とか言って腕などを触ってくる程度ですが（笑）今までは普段そんなにボディタッチもなかったので、ちょっと今さら感もあり恥ずかしく思っています。実際自分で触っても気持ちいいつるつる感と、潤いのある肌にとても満足しています。よくキャッチコピーとかで見る「赤ちゃん肌」や「透明感のある肌」という言葉は他人事だとずっと思っていました。しかし、脱毛に行って少しずつ綺麗になっていっていると感じると、心にも変化が生まれていつもより入念に化粧水を塗ったりボディーローションを使ってマッサージしたりと自分磨きに力が入りました。また、おしゃれにも気を使いたくなってきて、今までだったら選ばなかった雰囲気の服とかも気になり始めています。銀座カラーさんの「全身で恋をしよう」というキャッチコピーの意味をかなり実感しています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="神戸元町店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 受付<br></span>
</dt>
<dd itemprop="reviewBody"><p>受付の仕事をしているのですが、制服が半袖なんです。職場は空調施設がしっかりしてるので、年中半袖でいても大丈夫なのですが問題はムダ毛でした。学生時代も自分で剃ったりはもちろんしていました。しかし冬場は肌の露出も少ないのでほぼ放置状態でよかったのです。ところが、仕事着は1年中半袖。冬も関係なく腕を出さなければならないわけですから正直とてもムダ毛の処理が面倒でした。そこでもういっそのこと脱毛サロンに通ってしまおうと思い立ちました。どうせだからと選んだのは全身脱毛コースです。個人的にはちょっとV・Iラインが痛かったけれど回を追うごとにマシになった気がします。あとすごく驚いたのがお肌の状態です。自分で処理した際の肌より銀座カラーさんで脱毛した後の肌の方が断然すべすべつるつる肌になりました。秘密は保湿ケアだと教えていただいたので、これから自分でもさぼらずお風呂上りにはボディーローションを塗ってケアしようと思います。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="神戸元町店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 主婦<br></span>
</dt>
<dd itemprop="reviewBody"><p>夏は暑くて汗をかくし、少し寒くなってから行こうと決めていました。インターネットでいろいろ調べていたら、ちょうど銀座カラーさんがお得なキャンペーン中だったのでそのままカウンセリング予約をしました。店員さん曰く、脱毛治療中はあまり紫外線にあてるのもよくないそうなので、始めた時期は正解だったかなと思っています。私はもともと肌がそんなに強くなく、自分でムダ毛処理していると剃刀負けしてしまうこともよくありました。そういった心配もあったので、カウンセリングの際にはいろいろ質問をさせていただきました。どれも丁寧に答えてくださり、私に合った脱毛コースをちゃんと選ぶことが出来たと思います。美肌潤美というコラーゲンやヒアルロン酸等が入ったミストの保湿ケアもお願いしました。これが本当にびっくり。こんなにもつるすべ潤いのあるお肌が実感できるなんて。店員さんのアドバイスも聞きながら、普段からお風呂上りや乾燥が気になったときは保湿を心掛け、少しずつですが自己ケアの成果も出てきている気がします。ムダ毛もなくなり、美肌もゲットできてすごく嬉しいです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="天王寺店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">30代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>私は脱毛といえばとても痛いというイメージを持っていました。昔、脱毛していた友人が痛いと言っていて、怖がりの私は絶対にやりたくないと思っていました。しかし最近の光脱毛なら輪ゴムでパチッとはじかれた程度の痛みで済むと聞き、興味を持ちました。光脱毛は永久脱毛と違って、しばらくするとまた少し毛が生えてくるということでしたが、脱毛し放題プランにすれば気になり始めた時にまた通えます、と説明を受けそちらにしました。やはり施術１回目は不安で、痛がりなことを店員さんに伝えるとジェルを厚めに塗ってくださいました。そのおかげか全然痛みを感じず「もう終わり?!」と思わず言ったほどです。あんなに怖がっていたのが恥ずかしくって、その日は店員さんと目を合わせられませんでした。当初両脇だけでお願いしていたのですが、始めてみてひじ下とひざ下も追加でお願いしようかなと今は考え中です。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="天王寺店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 秘書<br></span>
</dt>
<dd itemprop="reviewBody"><p>わたしはすでに別のところで全身脱毛をしていたので、銀座カラーでは顔セットコースを選びました。正直別のサロンへ通っていた時は、顔まで脱毛する意味あるのかなと思っていました。しかし、顔の産毛は結構あって、化粧ノリもそれのせいでだいぶ変るということを知りました。せっかく顔を脱毛するなら、同時に美肌もゲットしたいと思い、肌ケアに力を入れている銀座カラーでお願いすることにしました！やってみると脱毛による美肌効果は本当にあって、以前と全然キメが違います。しかも銀座カラーの保湿ケアをしてもらった後のぷるつや感がすごいです。化粧ノリもよくなったし、透明感のあるお肌がまさか私のモノになるなんて、と感動しています。同僚の子にも、化粧品変えたの？なんて聞かれたりもして、銀座カラーで脱毛し始めたくらいしか変化ないよ！って言ったらとても驚いていました（笑）人に一番見られる部分だからこそ、脱毛してよかったと思います。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="心斎橋店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>スタッフの方が丁寧で優しくて、とてもよかったです。初めての脱毛だったので、不安もあって別のサロンのカウンセリングにも行ってみたんです。<br>
そこのお店も清潔感があり綺麗だったんですが、カウンセリングを担当したスタッフの人がなんだか不愛想で、ちょっと私は好きじゃなかったのです。そのあと別の日に銀座カラーさんのカウンセリングに行きました。脱毛のコースや料金、店舗の雰囲気含めてこちらの方が気に入ったのですが、何よりもスタッフの方の笑顔というか、対応が気持ちよかったです。施術してもらうには他人に肌を見せなければならないわけですから、スタッフの方との距離感や信用がとても大事になると私は思っていました。たまたま私にあたった人が良くなかったのかもしれませんが、そういう気配りから出来ているお店の方がとても印象は良いです。銀座カラーさんは通っている間もずっと変わらず、接客してくだいましたし安心して身を任せることが出来ました。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="心斎橋店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>誕生日に友人から銀座カラーの美容パックをプレゼントされました。大好きなゆずの香りのものですごく嬉しかったです。お肌ももっちりして大満足。なんで脱毛サロンなのに、美容パック？と疑問だったんですけど、脱毛だけじゃなく美容にも力を入れてるらしいと聞き、さっそくその友人に紹介してもらいました。むだ毛もなくなってお肌もぷるぷるになるなんて最高です。全身脱毛コースなのにいつも１時間くらいで終わるし、学校帰りに行けるのも嬉しいです。始めはパチパチッていう痛みが少しあったけど最近はそれもあんまりなくなって、時々施術中に寝てしまうくらい！ふと気づいたらもう終わっているなんてことがここ最近続いています（笑）ちなみに銀座カラーに通うきっかけとなった美容パックは行くたびに買って帰ってお家でのケアも怠っていません。どうしても冬場は暗めの服を着るので、お肌も一緒にくすんだ色に見えないように頑張っています！</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="梅田新道店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 飲食業<br></span>
</dt>
<dd itemprop="reviewBody"><p>ずっと背中ニキビがひどくて学生のころからの悩みでした。ニキビケアの塗り薬とかも使用していましたが、すぐにまた別のところに出来てしまったり。背中が見えそうな服は絶対着たくなかったし、毎年夏は嫌でした。しかしある時たまたま銀座カラーさんのホームページを見ていて、脱毛で背中ニキビが治る可能性があるということを知りました。無料カウンセリングで話を聞いてみると、背中ニキビは毛穴にたまった皮脂が炎症を起こして出来てしまったり、こすりすぎて乾燥することが原因の場合もあるようでした。ムダ毛をなくすことで炎症を起こしにくくし、その上で保湿ケアを施していけば治るかもしれないと言われました。店員さんが本当に親身になって話を聞いてくださって、丁寧に説明もしていただいたので、やっぱり銀座カラーさんにお願いしようと思いました。時間はかかるみたいですが、脱毛だけに頼らず店員さんのアドバイスを受けながら治していこうと頑張っています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="梅田新道店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 デザイナー<br></span>
</dt>
<dd itemprop="reviewBody"><p>プロポーズをOKしてすぐに脱毛サロンを予約しました。式の日取り等その時には具体的に決まってなかったのですが、絶対ウェディングドレスは着たいし、やるなら早く行き始めなくてはとかなり焦っていました（笑）特に背中のうぶ毛が気になっていたのです。せっかくの純白のドレスなのに、お肌がくすんで見えるのはもったいない。一生に一度の晴れ舞台ですから、一番綺麗に見せたい、と思いました。もちろん選んだのは全身脱毛コースです。結婚準備などで忙しくなる中、1回で全身すべて施術してくれるし、しかもそれが60分程度で終わるのは非常に助かりました。幸い、わたしはそんなに毛が濃い方ではなかったし、式までは少し時間があったので、何とか結婚式までにある程度の脱毛は完了し、気になっていた背中も自信をもって見せられました。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="梅田茶屋町店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>タレント養成所に通うことになって、これからの活動幅も広げるために全身脱毛することにしました。母はまだ自己処理でいいのではないかと言っていたのですが、剃刀でやっているとそれこそお肌にもあまりよくないし、脱毛をすることで肌が綺麗に見えるようになると聞き、もう絶対やる！と半ば強引に予約を申し込みました。<br>
綺麗なお店でスタッフの人もすごく優しくて、よかったです。1回の施術もすぐ終わるし、脱毛すると本当にお肌つるつるになりました。そんな話を家で毎回しているうちに、母もうらやましく思ったのか銀座カラーに通うことに。<br>
紹介キャンペーン利用して2人でかなり得しちゃいました！梅田には2店舗銀座カラーさんがあって、梅田茶屋町店の方が希望日に予約いっぱいでも梅田新道店を予約すればよかったので、2人でどんどん通えることが出来ました。今度ちょっとしたオーディションがあるんです。最終まで残れば水着審査もあるみたいなので、つるすべになったお肌とこれまでの努力を存分にアピールしてオーデション合格してきます!!</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="梅田茶屋町店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>私は京都から通っています。阪急電車1本で乗換なく行けますし、自宅の最寄り駅から40分ほど。京都にもいくつか脱毛サロンはありますが、美肌潤美という銀座カラーさんオリジナルの保湿ケアに心惹かれたので、つけてもらいました。まだ京都には店舗がないようで、梅田まで毎回行くのはしんどいかなあとも思ったのですが、1〜2ヶ月に1回程度のペースで週末に大阪まで来るというのも気分転換になってます。だいたいいつも午前中に予約をお願いし、ランチをしてお買い物という流れにしています。さすが大阪ということもあってか、スタッフの方はとてもお話が面白く、流行にも敏感で行くたびにいろいろ教えていただいたりもしています。決め手となった保湿ケアもばっちりです。私は寒くなるととても肌荒れして、ひざ下とか粉吹いてしまいますが、今年はたっぷり潤いを得て乾燥に負けない肌で冬を乗り切りたいと思っています。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新潟万代シテイ店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>新しくできた店舗ということで、内装も白を基調とした高級感のある感じがすごくよかったです。なんだか特別なおもてなしをしてもらっている気分になりました。1回の施術で全身をやってもらえたし、1時間程度で終わったのでいい事ばっかりでした。<br>最初は他人に身体を見られるということに対して抵抗や戸惑いもありましたが、施術箇所以外は隠してもらえるし、手早く丁寧にしてくれました。そういう気づかいをしてくれることが何よりも良いサービスだと私は思います。 別の脱毛サロンに通っていた友人は施術にすこしはじかれるような痛みを感じたと言っていましたが私はまったくと言っていいほど感じませんでした。毛の太さや量によって痛みに個人差があるそうです。 施術中ずっと店員さんとしゃべっていて、脱毛や美容、近くのごはん屋さんについてなんかも教えてもらっちゃいました。そういうお話をするのも目的の一つで毎回来るのが楽しみでした。ほんとうに銀座カラーにしてよかったです。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="水戸駅前店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>最近、千波公園のテニスコートでテニスを楽しんでいます。テニスをする時は半袖・ハーフパンツなので、ムダ毛の処理は必須。6月、水戸駅前に銀座カラー水戸駅前店ができたのを聞いたので、オープン後すぐ伺いました。昔ソフトバンクショップが入っていたビルにできたらしく、何度か行ったことのある場所だったのですぐわかりました。駅からも近いので、ほぼ迷うことはなくサロンへ辿り着けると思います。当日は一緒にテニスをしている大学時代のサークル仲間と一緒に行ったのですが、スタッフさんが親切に案内してくれて、初めてのエステで緊張していた身体が緩みました。色々と説明を聞いて全身脱毛の方がお得なのがわかったので、私は全身脱毛コース、友人は顔脱毛含む全身パーフェクト脱毛を契約。予約とか取りづらいのかと思ったのですが、予約キャンセルのところに滑りこむことができて次回予約を取るのも簡単でした。今後は早めにネットから予約を取れば大丈夫と聞きましたので、忘れずに予約を入れなくちゃですね。</p></dd>
</dl>


<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="水戸駅前店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>今年の夏は暑かったのでポニーテールにすることが多かったのですが、首の後ろの産毛が目立っていたようで同僚に教えてもらいました。自分では見えないのでわからなかったのですが、写真を撮ってもらったところ、結構濃いめに生えていました。髪をアップにするのは止める、という選択肢もあったとは思いますが、私は銀座カラーで脱毛することに決めました。うなじだけじゃなくて他部位も気になっていたので、全身ハーフ脱毛の、うなじ（えり足）・Vライン（上部・サイド）・両ひざ・両ヒジ上下・両ひざ上下を選んで契約しました。気になる部分だけ選んで脱毛できるので、全身脱毛までは必要ないけど部位単位だと高くつく、という私にとっては良いコースでした。先日一回目を終わらせたのですが心配していた痛みや肌の炎症もなく、肌の調子もすごくいい感じです。少しずつ毛が抜けてきたのと、保湿ミストのおかげで自分の肌の触り心地がよくて、ついつい触ってしまっています。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="宇都宮店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 薬剤師<br></span>
</dt>
<dd itemprop="reviewBody"><p>医療脱毛と光脱毛で迷ったんですが、光脱毛の銀座カラーさんにしました。医療脱毛の方が効果が高いというのは調べてわかったんですが、私はそこまで毛が濃くないので、安価な光脱毛でも十分効果が出ると思ってはじめました。医療脱毛をしたことがないので比較できませんが、私は銀座カラーさんの脱毛方法で十分効果が出ています。脇とひざ下の毛が太めなので特にわかりやすくて、脱毛後1〜2週間くらいに肌を撫でると、ぽろっとムダ毛が抜けます。これが本当に気持ちよくて、脱毛の醍醐味って感じです。抜けた毛の毛穴は小さくなるみたいで、肌のキメが整うような気がします。数回施術してますが、その度にムダ毛が減ってきているので、あと3回くらいやればほぼ生えなくなるんじゃないかなと思ってます。私みたいに医療脱毛と迷っている人もいると思うのですが、光脱毛でも十分効果が出ると思うので、個人的には銀座カラーさんの脱毛をおすすめします。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="宇都宮店">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>オープン時は、やっと栃木に銀座カラーが来たかって思いました。オープン時はまだ高校生だったので行けなかったのですが、仕事をしはじめて貯金もできてきたので、この前通う決心を。東武周辺にも脱毛サロンはちょこちょこあるんですが、やっぱり脱毛に行きたいのは銀座カラー。理由は、私が好きなアイドルが広告をしているから！これに勝る理由はありません（笑）ただ何回か通って効果が出なかったら途中で解約しようと思っていたのですが、かなり脱毛の効果出てます。初回の脱毛時は肌の上でピカッと機械が光るだけだし、施術後すぐはなんの変化もなく毛も抜けなかったので解約かな〜なんて思っていたのですが、2週間後くらいから毛がスルッと抜けていくんです。試しに一本毛をつまんで引っ張ったら、なんの痛みも違和感もなくスルッと抜けてびっくり。これが脱毛の効果かって思いました。いまじゃ銀座カラーの脱毛結果が気に入って、定期的に通っています。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="藤沢店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 美容師<br></span>
</dt>
<dd itemprop="reviewBody"><p>海が近くにあるから、周りに脱毛している人結構多い気がします。私もその内の一人で、しょっちゅう海に行ってビーチでのんびりしたり、気が向いたら泳いでみたりするのが好きです。地元民なので湘南とかは行かず、遠いところの海ばっかりですけど。だから、ムダ毛なんて生やしてはいられないんです。年中ツルツルでいないと不便なので、藤沢店オープン当初から通っています。脱毛の効果もいいんですけど、やっぱり私は美肌潤美の保湿ミストが最高です。海に入ると肌が乾燥しがちで、肌の表面だけじゃなく中からカサカサって感じなんですけど、美肌潤美で保湿してもらうとプルっとするだけじゃなくて、肌がふっくらするんですよね。それだけで女性らしい肌になれるので、私の肌ケアには必需品です。最近美肌潤美のフェイスパックも出たので、それを大量買いして家でも肌ケア頑張ってます。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="藤沢店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 ネイリスト<br></span>
</dt>
<dd itemprop="reviewBody"><p>新しく始めようと思った趣味が脱毛必須なユニフォームだから、というのがきっかけでした。昔から趣味が無くて人との会話に詰まることがあったので、折角なので湘南の海でできる趣味にしようと思って、サップヨガを体験しました。これが楽しくて！ただこれをはじめるとなると水着とかヨガウエアになるので、露出が多い。なら、家の近くの銀座カラー藤沢店で脱毛しちゃおうということで通い始めました。脱毛って日焼けしているとできないみたいなので、日焼けする危険のない秋からはじめてます。夏からはじめちゃうと日焼けで脱毛できないかもしれないので。秋からはじめれば来年の夏は約1年後。その時はきっと脱毛して、銀座カラーの保湿ケアもして、ツルスベ肌に決まってますよね。ムダ毛がなくなれば水着だってヨガウエアだって好きなものが着れますし、もっとサップヨガが楽しくなるはず。来年の夏目指して、いまから脱毛頑張ります！</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="川崎店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 看護師<br></span>
</dt>
<dd itemprop="reviewBody"><p>毎年多摩川沿いでBBQをするんですが、今年ムダ毛の処理を忘れて大失敗しました。ムダ毛が生えていることに気づいた時は本当に恥ずかしくて、頭が真っ白ってこういうことか〜みたいな。なので、自己処理を忘れても大丈夫なように、銀座カラーで全身脱毛を最近はじめました。予約日の前に剃れる所は剃っていかなきゃなんですが、全身なので結構大変。カミソリだと肌が痛むみたいなので電動シェーバーでやるんですが、カミソリより時間がかかる。根気のいる作業ではあるんですが、簡単に綺麗にはなれないってことだと思って頑張ってます。届かない背中とかは当日エステティシャンさんがやってくれるので大丈夫ですよ。で、全身脱毛はいっぺんに施術するので1時間くらいかかってます。機械をツーっと滑らせる感じでやるので、結構早いです。ちなみに、足腕と比べてVIOと足首はちょっと痛かった。でも、何回かしていれば慣れるみたいなので気楽に構えていようかなって思ってます。来年の夏までにムダ毛の無い肌になれるよう、しっかり通ってみます！</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="川崎店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 受付<br></span>
</dt>
<dd itemprop="reviewBody"><p>川崎店は駅からめっちゃ近いので、夜でも安心して通えます。夜の繁華街を1人歩きするのはやっぱり不安で…。でも川崎店はアゼリアを通ってサロン近くの出口を出れば目の前のビルなんですよね。地上に出るのは一瞬で、すぐサロンに着くので、何度も通ってますが怖い思いをしたことはありません。やっぱり女性専用の脱毛サロンだから、そういう所も気にしてるんですかね。いろいろ考慮しての立地だったらちょっと感動です。川崎店の営業時間は平日は21時までですが、施術時間が短いので遅くなりすぎることはありません。全身脱毛だと約1時間、私は足腕脱毛なのでもっと短い時間で終わります。さっと終わらせて帰れるので、仕事終わりの予約も安心してできます。やっぱり仕事終わりの金曜とかに予約入れたくなりますからね。これからもマメに通って、はやくスッキリさせたいです。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜西口店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>今、周りの友達の中で銀座カラーに通うのがすごく流行ってます!!流行らせたのは、私なんですけどね!!（笑）
荒れやすかった私の肌が、通い始めてから調子が良くなったので、みんなにその理由を教えて欲しいってお願いされました。お得な紹介キャンペーンもあったから、どんどん紹介してみたら、みんなも通うようになったんですよ。サロンに行ってみたいけど勇気が出なかったっていう子もいたので、きっかけづくりができてよかったし、私も豪華な特典がもらえてハッピーです!!
</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜西口店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">30代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>仕事の都合で、毎回平日の夕方に予約をしていますが、いつでもスタッフさんは笑顔で元気に迎えてくれます。私は、いつも疲れ顔でサロンに行くのですが、施術をしながら元気をもらえて帰りは疲れが抜けている気がします。サロンを選ぶポイントは、脱毛の効果や料金もすごく大事だけど、私の場合はスタッフさんとの相性も大切だなって思っています。仕事で疲れた自分へのご褒美に、サロンで脱毛を受けるのってすごくおすすめですよ。私は、横浜エリアしか行ったことはないのですが、銀座カラーはどの店舗も同じように元気に迎えてくれるはずです!
</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜エスト店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>横浜は2店舗あるので、行きたい日の空き状況に合わせて選べとても便利です。会員サイトから簡単に予約も取れるから、仕事の休憩中や移動の電車とかで思いついた時に予約しています。また2店舗とも駅近なのがすごく嬉しい!!駅から離れていると、だんだん通うのが面倒になってしまうので、やっぱり駅近がいいですよね。何度も通うサロンなので、通いやすくないと続けられないですよ。いろんな意味で、銀座カラーは通いやすいからぜひオススメしたいと思います!
</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜エスト店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>脱毛って高いものだと思っていたけど、銀座カラーは金額を見てびっくり!!学割があってお得だから、バイト代で払える範囲内で全身脱毛デビューができちゃいました。脱毛サロンに通えるなんて、大人の女性って感じでテンションあがります!!姉からも銀座カラーを勧められていたので、今回契約の時にその話をしたら紹介の特典もついてラッキーでした。あと学生なので授業のない平日の昼間に通えるから、予約もすごくスムーズ!学校もあるし、友達や彼との約束も大切だから、自分の都合に合わせてサロンに通えるって本当に重要だなって思いました。私の希望をすべて叶えてくれる銀座カラーに通えて良かったです!!</p></dd>
</dl>

<!-- <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">30代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>横浜店はずっと前からある印象です。言い換えれば昔から通ってる人がたくさんいるってことなんですよね。それって新しく出来たお店にはない、長く愛されてきた横浜店の良さが出ているのかもって考えてます。エステティシャンさんもたくさんの経験があるのかとても頼り甲斐がありますし、ムダ毛の悩みなども気兼ねなく相談させてもらってます。新しくオープンしたサロンにもいいところってあると思いますが、私は脱毛の歴史を感じるこの雰囲気が結構好きです。もちろん、サロン内は白を基調とした綺麗な店内で、気持ちよく過ごすことができます。掃除も行き届いてますしね。新しくできた脱毛サロンに行って失敗したなって感じたことがある人って結構いると思うんですよ。料金形態が曖昧だったり、スタッフの経験不足から打ち漏れされたりとか。そういう経験がある人は横浜店みたいな昔からあるサロンに一度行ってみたらいいと思います。</p></dd>
</dl> -->

<!-- <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>以前、姉が通っていたので銀座カラーに決めました。姉とは12個歳が離れているので、私からすれば憧れのお姉ちゃん。そんな姉が通っていた横浜店に私も1年前から通っています。横浜には銀座カラーの店舗が3店ありますが、姉と同じ店舗に通いたかったのであえて昔からある横浜店で脱毛しています。
もう数年経っているので姉が通っていた時に聞いていた話とは様変わりしていますね。いまの脱毛マシンで全身脱毛が1時間でできるようになってるよと姉に話すと驚いていました。元々そこまで濃くない私のムダ毛なら6回くらいでほぼ生えなくなってきました。自己処理がないだけで、毎日こんなに楽なんだと感動です。随分前に銀座カラーを卒業した姉ですが、今度新しい機械で脱毛していない部位をやりたいみたいなので、お友達紹介のキャンペーンを利用してみたいと思います。</p></dd>
</dl> -->

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="柏店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody"><p>人より毛が濃い方なので、毎日お風呂の時間にカミソリで剃っていましたが、肌へのダメージが気になり自己処理を止めたいと思ったのが、銀座カラー柏店に通い始めたきっかけです。<br>カウンセリングの時に、私が今まで行なっていた自己処理について話をすると、かなり肌には良くなかったみたいでゾッとしました。手遅れになる前に脱毛を始められたので、今では肌荒れしにくい綺麗な肌状態をキープできています。<br>また、ムダ毛が多かったので効果が出るかも不安でしたが、しっかり効果が表れているので普段のお手入れが見違えるように楽になって、本当に嬉しいです。<br>脱毛サロンに通うか悩んでる人がいたら、絶対に早く始めた方がいいですよって教えてあげたいですね!</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="柏店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務職<br></span>
</dt>
<dd itemprop="reviewBody"><p>婚活をかねて脱毛サロンに通おうと思い、銀座カラーで契約をしました。<br>職場付近にも店舗はありますが帰宅時間が遅いため、休日に最寄り駅にある柏店へ通っています。<br>お店は、駅からすぐのわかりやすい場所にあり道にも迷わないし、脱毛後にお出かけする時も移動に時間がかからなくて、すごく便利です。<br>またスタッフのみなさんが、いつも丁寧に接客してくれるので、悩み相談もしやすく安心して通えています。私は、プライベートの話もしちゃうので、女子トークが盛り上がって楽しいです!<br>通うたびに美意識が高まるので、おしゃれの幅も広がってきました。これで、婚活も上手くいきそうな気がします!</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="船橋北口店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>私は雨に濡れるのが大嫌いなので定期的に通うお店は全部駅近と決めています。会社は最寄り駅から5分、美容院は駅ナカ、脱毛サロンは船橋駅から徒歩1分の銀座カラー船橋北口店です。船橋駅直結で東武百貨店があるので、駅に着いたら東武百貨店の店内を抜けてギリギリまで屋根のあるところを歩いてサロンまで向かいます。駅から1分だし店内を抜けるので雨が降っていても関係ありません。この前台風が来ていた時も雨が降っていましたが、ほとんど濡れずにサロンに着くことができました。着いて早々「雨大丈夫でしたか？」とスタッフさんに声をかけてもらえたことに嬉しくてちょっとほっこりしました。足腕脱毛をしてからポロポロ毛が抜けて自己処理がかなり楽になりましたし、ムダ毛がかなり薄くなってきたので「カミソリで剃っておけばよかった。」なんていう後悔もありません。通うのが面倒な人は駅近の脱毛サロンをお勧めします。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="船橋北口店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 営業<br></span>
</dt>
<dd itemprop="reviewBody"><p>実際に通ってみて予約は普通に取れるし、スタッフさんもいい人なので安心しました。口コミサイトに予約が取れないことがあるって書いてあったのですが、施術後に予約を取るタイミングがあるのでその時に予約を入れれば問題ないと思います。もし都合がつかなくなったらネットで時間を変えればいいだけなので簡単です。結局どこの脱毛サロンに行っても予約の問題は上がっているみたいなので、早めの予約を心がけて、自分で調整していくしかないと思っています。また、スタッフさんは親切丁寧な接客だと思いますし、脱毛効果に関しても問題はありません。人それぞれ捉え方や印象が異なるので仕方のないことかもしれませんが、しっかりお見送りしてくれるし、脱毛する時もキチッと照射してくれます。私自身は船橋北口店も銀座カラーも満足しているので、今契約中の脇と足腕脱毛が完了するまでしっかり通っていきたいと思っています。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="千葉船橋店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 主婦<br></span>
</dt>
<dd itemprop="reviewBody"><p>ブライダルエステの一環で銀座カラーの脱毛に通い始めたのがきっかけです。当時全身脱毛の脱毛し放題で契約をしているので、結婚式が終わったいまでも通い続けられています。実際8回くらい通ったところでほとんどのムダ毛は無くなったのですが、たまにひょっこり現れる毛がいるので、いまは気になったタイミングで気になる部位だけを脱毛しています。これが脱毛し放題のいいところ！脱毛し放題じゃなかったら、また契約しなきゃいけないんですもんね。結婚した今、脱毛したいなんてお金がかかってしまうから言えないと思います。もし、ブライダルエステで脱毛する人がいるなら、絶対脱毛し放題にした方がいいですよ！ちょっと費用が高くなるので、旦那さんからNGが出るかも。私のところも20万円の予算をオーバーしてしまったので、自分の貯金を下ろして脱毛し放題プランに変更しました。ここだけ追加で脱毛したいとか、一本生えてきた毛だけ脱毛したいって思うことがあるかもしれないので、初めの契約プランをしっかり考えた方がいいと思います！</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="千葉船橋店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">30代 講師<br></span>
</dt>
<dd itemprop="reviewBody"><p>ヨガインストラクターをしています。露出が多めなユニフォームを着たり、両腕をあげるヨガのポーズなどがありますので脱毛は必須です。以前両脇のみ他店でニードル（針）脱毛をしていたのですが、とにかく料金が高い。脇脱毛だけなのに数十万もかかりましたし、痛みが本当に辛くて通うのが嫌で嫌で仕方なかったんです。その他部位は千葉船橋店に通って脱毛していました。銀座カラーさんは料金は比較的手頃だし、痛みもほとんどないし、駅から近いのでめんどくさいなって思う暇もなくサロンに着いちゃいます（笑）足腕の脱毛効果はもちろん、背中や腰の周り、お腹など比較的毛の薄い部位も徐々に薄くなってきて、いまじゃほとんど生えていません。生えていても産毛レベルなので気にならないですし。<br>
私自身ヨガ講師の方に憧れてヨガインストラクターになったようなものなので、私も常に綺麗で憧れられる存在でいるためにも、体型維持と合わせて脱毛も続けて通っていきます。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="千葉店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 接客業<br></span>
</dt>
<dd itemprop="reviewBody"><p>彼氏ができたのをきっかけに、千葉店で脱毛をはじめました。彼の元カノはとても可愛くて綺麗な人だったので、付き合い始めたのはいいのですが自分に自信が無くなってしまっていました。私のムダ毛は細めなのですが量が多くて、毎日カミソリで自己処理をしたり、たまに脱色をしたりして、必死に自己処理をしてきたと思っています。自己処理の回数が多い分ケアは徹底していて、保湿クリームを塗ったりコラーゲンドリンクを飲んだり、とにかく元カノに負けないように一生懸命でした。そんな時、友人が銀座カラーを紹介してくれたのです。通いだしてからというもの、簡単に毛が抜けますし、保湿ミストケアで肌のキメが細かくなってきたように感じます。スタッフさんとのおしゃべりも楽しくて、暗く沈んでいた気持ちも明るくなってきました。彼からも「なんか可愛くなってない？」って褒められて自信もつきました！いま思えば自信をつけることなんて簡単なんだから、早く脱毛に通ってればよかったのにって思います。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="千葉店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 アパレル<br></span>
</dt>
<dd itemprop="reviewBody"><p>とにかくたくさんの駅に近いので、通いやすさが抜群です！家から通うときは千葉駅で降りて、彼氏の家から通うときは葭川公園駅で降りて…。あちこち乗り換えがあったりすると電車代が高くなるし、何よりも通うのがめんどくさくなるけど、銀座カラー千葉店はアクセスがいいので小まめに通えてます。料金を基準に脱毛サロンを決めがちですが、やっぱり脱毛は続けなきゃ意味がないので通いやすさってかなり重要だと思いますよ。自宅や職場、彼氏の家とか友達の家などいつもいる場所を基準にサロンを選ぶと後悔しないんじゃないかなと思います。私自身アクセスの良さで千葉店で脱毛をすることにしましたが、スタッフさんの接客は気持ちいいし、サロンは綺麗だし、効果も出てるし、結果的にその他の脱毛サロンを選ぶ重要ポイントもクリアしてるなと。なので、千葉駅周辺の人で脱毛をしたいなら銀座カラーの千葉店を選んだらいいんじゃないかな〜なんて思ってます。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="川越駅前店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">10代 フリーター<br></span>
</dt>
<dd itemprop="reviewBody"><p>姉の紹介で銀座カラーに通っています。姉が脱毛に通っているのは知っていたのですが、私自身はメイクもほとんどしないですし、自分磨きとは程遠い女子力低めな生活を送っていました。そんな私がなぜ脱毛しているのか不思議ですよね。元々は姉が紹介特典欲しさに私を銀座カラーに連れて行ったことがきっかけでした。私は自分を磨く方法がわからないだけで、メイクや脱毛に興味がありました。姉に背中を押してもらったことで、初めての自分磨きを銀座カラーではじめたのです。美容に関して知識のない私にエステティシャンさんがわかりやすく説明してくださったので、どうして毛が抜けるのか・保湿についてなど理解することができました。私が不安に思っているのを感じ取ってくれたのか、しっかりと説明してくれて助かりました。いま半年通い続けてますが、ムダ毛がないだけで自分の肌がこんなにも変わるのかとびっくりしています。肌が綺麗になってくると自信が付いてきますし、色々なことにチャレンジしたくなってきました。次はメイクにも挑戦したいと思っているので、これからも女子力を磨きます！</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="川越駅前店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>広告を見て、脱毛するなら銀座カラーへ行こうと思っていました。元々体毛が濃い方で、小学校低学年の時は体操服や水着になるのが恥ずかしく、高学年になったらすぐカミソリで自己処理を始めました。ムダ毛が無くなって嬉しかったのですが、毎日剃らないと次の日にはチクチクするし、だからといって毎日剃っていたら肌を痛めてしまいました。自分で働き始めたら脱毛しようと思っていたので、初任給で家から近い銀座カラー川越駅前店に駆け込みました。全身脱毛を希望していたので、かなりの金額がかかるものだと思っていたのですが、予定より安く済んでびっくり。想定の1/2くらいでしょうか。だからといって効果がない、ということはありません。施術後1週間くらいで毛が抜けてツルッとした肌になります。次の毛周期がくるまではツルツルのままなので、本当に毎日が楽です。お金はかかりますけど、ずーっとムダ毛で悩んでるよりは絶対に気持ちは楽になりますし、自己処理にかける時間が短くなるので余裕ができます。毎日を楽しく過ごすために、銀座カラーでの脱毛は必須です。</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="大宮店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 事務<br></span>
</dt>
<dd itemprop="reviewBody"><p>友達に紹介されて、1年前くらいから大宮店に通っています。元々他の脱毛サロンに通っていたのですが、回数が少ない割に高くて。回数終了後、追加で脱毛したくても金額が高くて躊躇していました。脱毛回数は終わっているのに支払いが続くのがなんだか悲しくって。そんな状況を友達に愚痴っていた時、オススメだと銀座カラーを紹介されました。仲の良い友達なので最初から良いサロンだろうと信用していましたが、実際に行ってみて期待以上に感じました。ハーブティーのおもてなしからはじまり、解りやすい説明、広いサロン内に印象の良いエステティシャンさんたち。もちろん効果もしっかり感じられて、毛が抜けるだけじゃなくて保湿ケアで透明感も出てきたように思います。肌に自信が出てきたのでオシャレも楽しくて、ついルミネで買い物しちゃってます。今日も銀座カラーを紹介してくれた友達と、まめの木に集合して買い物です！</p></dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="大宮店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">
<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">
<span itemprop="name">20代 受付<br></span>
</dt>
<dd itemprop="reviewBody"><p>上尾で仕事をしているので、通勤で大宮を通ります。脱毛に通うのは定期券内がいいなと思っていたので大宮のサロンを探していました。銀座カラーさんはCMがすごい可愛くて印象的だったのと、そのCMを見て私の職場のファンの子が話題にしていたので一度行ってみることにしました。大宮って結構ごちゃごちゃしているので、サロンに行くのに迷わないか心配していたのですが、迷う暇もなくすぐサロンに着きました。歩いて5分くらいですね。サロン自体は新しい店舗より広くて落ち着いている雰囲気で、リラックスして脱毛を受けることができました。いい意味でパパっと施術をしてくれたので、せっかちな私としてはありがたかったです。パパっとできるってことは慣れてるってことだし、逆に安心しました。もちろんエステティシャンさんは親切だし、声掛けもしてくれて常に気遣ってくれたのを感じています。お友達紹介の制度がかなりおトクなので、職場のファンの子に銀座カラーをオススメしておきます！</p></dd>
</dl>

<!-- -->

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="町田モディ店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 ネイリスト<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>町田モディの中に脱毛サロンがあるのは本当に便利です！そもそも町田モディが駅の目の前にあるのでアクセスは抜群です。ちょっと雨が降っていた時は傘を持っていなかったのにほとんど濡れずにサロンまで辿り着けましたし、日差しが強い時も紫外線を浴びずに行けます。車で来る時はモディ自体に駐車場がないのでちょっと不便ですが、近場の決まった駐車場に預ければ1時間無料とかあるみたいです。もちろんサロンだけ通うのでは無料にならないので、モディ内のショップでお買い物をしなきゃなんですが、たくさんショップがあるので買うものには困らないですね。<br>
肝心の銀座カラー町田モディ店は、店内は綺麗ですし、スタッフさんも親切でとても感じがいいです。脱毛自体は痛みの少ない施術なので、のんびり寝そべっているだけで終わっちゃいます。私は全身脱毛なので、気がつくと寝ちゃってたなんてこともしばしば。せっかく綺麗になりに行っているのに、痛みが怖かったり、リラックスできないのでは残念ですもんね。その点、銀座カラーさんは気配りが上手だと思います。まだ施術回数が残っていますので、スタッフさんに協力いただいて、しっかりムダ毛を撃退していこうと思います！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="町田モディ店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 介護士<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>女子会で脱毛の話になり、私だけ未経験だったので銀座カラーで脱毛を始めることにしました。脱毛してないと話についていけなかったり、途中から話の内容がわからなくなってちょっと寂しい思いをしたので、これは自分を磨くタイミングだと思いました。正直自分のムダ毛で嫌な思いをしたことはないですし、気になったこともほとんどないのですが、友人曰く「自分が思っているより、他人はムダ毛のこと見てるよ〜」と話しているのを聞いて怖くなりました。自分だけが気にしてなかったのかも、って。<br>
銀座カラーに通う前は自分に対して無頓着だった私ですが、通い出して変わったと思います！全身脱毛で毛は少なくなりましたし、美肌潤美っていう保湿ケアで肌のキメが整いました。身体が綺麗になったら体型が気になりだして、ダイエットを始めて-3kg。メイク方法を研究しだしてからは、街中で声を掛けられることが増えました！私以外にもたくさんお客さんがいるはずなのに、町田モディ店のスタッフさんが変化に気づいてくれて、すごく嬉しかったです。これからも自分磨きを頑張って、自信をつけていきたいです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="八王子店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 飲食業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>和服を着る機会が多いので、八王子店でうなじを脱毛しています。実家に住んでいた時は妹や母に手入れをしてもらっていたのですが、最近一人暮らしを始めましたので手入れをお願いできる人がいなく困っていました。初めのころは何度か理容店でシェービングをしてもらっていたのですが、一回につき2000円くらいかかっていました。仕上がりはとっても綺麗なのですが、コスパを考えるとちょっと高い。だったら初期投資はかかるものの脱毛した方が得だと思い、銀座カラーの八王子店に通いだしました。他部位の脱毛の紹介などもあったのですが、ちょっとしたご案内って感じだったので、気兼ねなく断れました。うなじの脱毛は見えないところなので自分でシェーバーを買って持っていくのですが、私は予約時間前にサロン目の前のドンキで買いました。うなじは形が結構重要なのですが、エステティシャンさんが綺麗に剃ってくれましたので安心です。まだ3回なのですが、徐々にムダ毛が減ってきたように思います。元々毛が薄いので時間がかかるかもしれませんが、マイペースにしっかり通っていきたいと思います。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="八王子店">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image" style="display: none;">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ずっと立川店に通っていたんですが、3月に自宅の近くである八王子にサロンが出来たということで、すぐ八王子店に次回予約を入れました！立川駅まで電車で10分程度ではあるのですが、やっぱり近い方が楽なので助かります。こういう時、個人店とか店舗展開の少ないサロンじゃなくて良かったなって思います。だって全国展開していればこうやって近くにサロンができて、一気に通いやすくなるんですもん。便利ですね。<br>
立川店の時からVIO脱毛で通っているんですが、さすがに足腕脱毛よりは時間がかかってます。カウンセリングの際、他部位より時間がかかると聞いていたので本当にその通りって感じです。質も量も太さも全然違うので当たり前ですよね。でも、通う前に比べたら違いが分かりやすく出てきました。Vラインはムダな部分が抜けて形がキープされたままですし、iラインは毛量が減りました。Oライン（ヒップ奥）はほとんど生えてないですし、どんどん理想に近づいていっています。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="立川北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 秘書<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>立川北口店のエステティシャンさんは年齢が近いような感じがして、安心して脱毛をお願いできています。エステティシャンさんって結構若い方が多くて、受け答えに不安があったり、施術が心配だったりしてしまうのですが、立川北口店の方は私と同じような20代後半の方が多くいらっしゃるみたいで、安心するというか落ち着くというか。銀座カラーさんじゃないんですが、昔行った脱毛サロンは若いエステティシャンが多く、こっちがハラハラしてしまうことがあって以来、できることなら20代後半くらいのエステティシャンがいるところに通うことにしています。やっぱり施術してもらってても安定感があるというか、全身を委ねてしまえる安心感がいいですね。銀座カラーさんは先輩方の教育が良いのか、若いエステティシャンの方もしっかりしているので、どの方に担当してもらっても施術は安定していますし、わからないところを質問してもすぐに回答が返ってきますので安心して任せられています。最近VIO脱毛が気になっているので、信頼できるエステティシャンさんのいる立川北口店で始めてみようと思います。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="立川北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>毎年夏は海とプールで忙しいので、秋くらいには自己処理と日焼けのダメージで肌がボロボロになります。10代の時は日焼けもファッションの一部だったので気にしていなかったのですが、20代になったら美白とか色白がブームに。当の私は日焼けした色黒肌。しかも、夏のダメージが蓄積して肌はボロボロでした。このままでは30代はシミとシワだらけになる予感。さすがにこのままではまずいと思い、友人に銀座カラーを紹介してもらいました。日焼け肌は脱毛できないとのことだったので、肌が落ち着く秋冬から両脇と足腕脱毛を始めました。友人と同じ立川北口店で脱毛しているのですが、エステティシャンの方はプロって感じで、頼り甲斐があります。脱毛効果はバッチリ出てますし、脱毛後の保湿ケアで乾燥を感じていた私の肌が、プルプルに生まれ変わりました。噴射中、保湿ミストが私の肌にぐんぐん吸収されているような感覚がして、よっぽど乾燥していたんだと反省しました。<br>
最近はエステティシャンの方から肌ケアの方法を教えてもらって、毎日欠かさず行っています。脱毛でツルツルに、保湿ケアでプルプルになった私の肌は、銀座カラーさんのおかげです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="立川店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 主婦<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>子育てに追われ、自分のことは後まわし。気づいたら1日が終わるような生活だったので、自己処理もままならず。<br>
一言「脱毛したいな〜。」とこぼしたのを聞いていたようで、様々な脱毛サロンを調べてくれて、「銀座カラーって良いらしいし、ここで脱毛しておいで！」と言ってくれました。貯蓄のこともあるし、部位を選んで脱毛しようかとも思ったのですが、「全身脱毛にしなよ！」と言ってもらえたので、勢いで契約しました。立川店は雰囲気が可愛らしいですし、全身脱毛＋全身保湿ケアで久々に女性らしい時間を過ごすことができています。予約日は主人に息子を見ていてもらうので、土日しか予約が入れれないのですが、早めに予約をすれば土日でも問題ありません。しかも、1時間ほどで終わるので、家で留守番をしている主人や息子にも負担を掛けずにいられるので助かっています。今では数日自己処理ができなくても問題ないくらいムダ毛が減ってきていますのでとっても楽です。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="立川店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>昔から銀座カラーさんにはお世話になっていたのですが、仕事で転勤になりました。地方から立川周辺に引っ越しになりましたので、より家に近い立川店で現在は通っています。立川北口店も結構近いのですが、立川店のナチュラルな雰囲気が好きなのと、信頼しているエステティシャンさんが多くいるので基本的に立川店が多いですね。やっぱり体を預けることになるので、信頼できる人がいるのは心強いですし、安心して脱毛に通うことができます。私は結構痛みに弱くて、毎回ほとんど痛みは感じないもののドキドキして通っているのですが、それを察してか、立川店のエステティシャンさん達はしっかり声がけをしてくれるんです。ドキドキしているのもほっと和らぎますし、通常より痛みも感じなくなります。きっと私みたいに「痛かったらどうしよう。」とか「脱毛って怖い。」って思っている人もいるかと思うのですが、きっと安心できると思うので、一歩勇気を出して欲しいです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="吉祥寺北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>アパレル店員をしているので、常に見た目には気をつけています。食事制限や筋トレ、ボディケアの一環として脱毛を始めたのもアパレル店員になってからでした。季節問わず露出が多くなりがちなので、全身脱毛で抜け目なくケアを行っています。他店は脱毛し放題が付いていないこともありますけど、銀座カラーは脱毛し放題が付いているので、将来肌が変化していっても対応してもらえるので、とても安心感があります。ムダ毛が無くなる前にコースが終了してしまったら、結局追加契約になってしまいますからね。お客さんのことをしっかり考えてくれている証拠だと思っています。<br>
大事な脱毛の効果ですが、一年で8回通えるようになっているので割と早い段階で毛が少なくなってきます。施術の際もしっかりマーキングしてくれるので照射漏れもないですし、もし何か気になっても吉祥寺北口店のエステティシャンさんは優しいし親切なので気軽に質問しても大丈夫ですよ。これからも自信を持って店頭に立てるよう、脱毛を通して自分磨きを続けていきます。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="吉祥寺北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">30代 秘書<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>自転車で30分ほどかかるのですが、吉祥寺北口店に脱毛しに通っています。最近、彼氏の影響で自転車にハマり、ここ数年徒歩や電車を使った移動手段は使わずどこまででもクロスバイクで移動しています。脱毛するきっかけも、この自転車がきっかけでした。彼氏とクロスバイクで浜松まで旅行に行った時、怪我が無いよう長袖とロングパンツを着ていったので、ムダ毛の処理をしっかりしていませんでした。途中Tシャツに着替えたのですが、ポツポツ生えっぱなしのムダ毛に気づいた時に冷や汗が。自己処理を面倒だからとサボりがちなので、また同じことで焦らないよう、プロにお願いするのがいいかと思ったのです。好きな部位を選んで契約することも考えたのですが、やっぱり全身脱毛が今後も楽だなと思って。エステティシャンさんは親切だし手際がいいので、楽しくおしゃべりしているとあっという間に終わりますね。脱毛を始めてからというもの、彼氏から肌が綺麗だと褒められることが増えました。銀座カラーには感謝です。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>大学の友人からの紹介で、銀座カラー北千住店に通っています。そこまで毛が濃いわけではないので脱毛に必然性は感じていなかったものの、紹介したら特典がもらえるということで試しにカウンセリングを受けてみました。脱毛のメリットって毛が抜けるから楽できるってところだけだと思っていたのですが、エステティシャンの方から説明を受けて、脱毛の多くのメリットを知ることができました。自己処理より結果的にお金がかからなかったり、肌トラブルが回避できることを知って、露出頻度の多い両ワキと足腕の脱毛をすることにしました。通う前は自己処理で肌荒れが起きたり、自己処理の面倒くささに途中でカミソリを投げ出したりしたことも。でもいまはほとんどムダ毛が目立たない綺麗な肌になりました。友人も紹介特典がもらえて嬉しそうでしたし、私も特典がもらえてありがたかったです。私も他の友達を紹介して、脱毛仲間を増やしちゃおうかなと思います。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>年齢的にシミが気になるようになってきたので、今年は日焼け止めに日傘で過ごしていました。おかげで日焼け自体はほとんどせず過ごせましたが、なぜか毛穴の炎症が増えてしまったのです。原因は、SPFの高いウォータープルーフの日焼け止めを使っていたからでした。しっかり洗顔できてなかったみたいです。毛穴の炎症はシミにも繋がるのでかなりショックで。改善方法を調べていたら、脱毛して炎症が減ったという記事を見つけました。顔脱毛はしたことなかったので、近場の北千住店で脱毛しようと思って足を運んだという感じです。顔脱毛はメイクを落として施術しなければならないので、スッピンで出歩ける徒歩圏内のサロンであることが絶対条件でした。この前初めて行ったのですが、スタッフさんは優しいし、施術はスムーズ。何より脱毛後の保湿ミストが最高です。保湿ミストの後は肌がもっちりプルン、極上のフェイスパックっていう感じですね。この日はスッピンのまま行って帰って来たのですが、メイクルームがあるようなので、出かける前とか帰りに寄ることもできそうです。しっかり通って美肌目指します！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="上野公園前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>デザインの仕事をしているので、休日はアートなどを観て感性を磨くよう心がけています。上野付近は美術館や博物館が多いので、お気に入りのスポットです。<br>
              以前は職場の近くにある別の脱毛サロンに通っていましたが、休日によく行く場所で通いたいと思い銀座カラー上野公園前店に決めました。1度の来店で全身と顔を一緒に脱毛できるのに、施術の時間が短いので遊びにいくついでに通えてとても便利です。<br>
              今までは毎日お風呂場で自己処理しないと翌日には目立っていたムダ毛も、数日放っておいても目立たないようになってきました。しかも、自己処理が減ったらお肌の状態が良くなってきたのですごく嬉しいです。<br>
              銀座カラー上野公園前店なら、このまま楽しく通っていけそうです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="上野公園前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>上野にある大学に通っています。大学の友達たちから脱毛サロンは銀座カラーがいいと聞いて、私も上野公園前店に通うことにしました。<br>
              初めての脱毛サロンだったので、無料カウンセリングに行く前は少し緊張しましたが、一歩中に入るとかわいい店内にわくわくしました。また担当してくれたスタッフの方がとても優しくて、相談しづらいことも安心して話すことができました。<br>
              授業の予定に合わせて予約も取れるので、銀座カラーを選んで本当に大満足です。<br>
              私も、たくさん友達にオススメしたいと思います!!</p></dd>
          </dl>


          <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="錦糸町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

              <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">

              <span itemprop="name">20代 事務<br></span>
            </dt>

            <dd itemprop="reviewBody"><p>全身脱毛し放題のプランで錦糸町店に通っています。最初は腕とワキだけにしようと思ってたんですけど、カウンセリングでお話を聞いて、こんなに安くて通い放題なら全身脱毛がいいかな！と思って思い切って全身脱毛にしちゃいました。なによりすごいなーと思ったのが、脱毛のアフターケアの美肌潤美！保湿力バツグンでお肌がぷるぷるになるので感動です！お手入れ後のキレイな肌を見て、銀座カラーにして良かったなーって思いました！
            </p>
            </dd>
          </dl>


          <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="錦糸町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

              <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">

              <span itemprop="name">20代 接客<br></span>
            </dt>

            <dd itemprop="reviewBody"><p>少し前、化粧のりが悪いなーって思ってネットで調べたら、どうやら産毛のせいなのかも…ということで、電車の広告をみて気になっていた、銀座カラーの無料カウンセリングを予約しました。脱毛だけのつもりだったけど、美肌潤美という保湿ケアがあるのを知ってやってみることに。<br>
              いざやってみると、すごーく潤っているのを感じました！通える周期も短いので、効果を実感できるのが早いですよ！パウダールームでお化粧できるのもポイント高いです。最近は化粧のりが良くなってきて、楽しくなっていろんなコスメを集めて試すのがプチブームです！同僚にも「最近キレイになった？」って言われちゃって、心の中でガッツポーズしちゃいました(笑)
            </p>
            </dd>
          </dl>


          <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="錦糸町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

              <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">

              <span itemprop="name">20代 アパレル<br></span>
            </dt>

            <dd itemprop="reviewBody"><p>職業柄、身なりにかなり気を遣うので、美容には時間をかけています。バスタイムのケアやネイル、ムダ毛の処理…もっと時間が節約できたらいいのに～！って思っていました。東京にはたくさん脱毛サロンがありますが、職場の近くに銀座カラーがあったな～と思い出して、行ってみることにしました！ <br>
              錦糸町店のスタッフの皆さんは親切で話しやすくて、自分も見習いたいくらい！ガールズトークに花を咲かせちゃうこともたまにあって…(笑)脱毛の効果が出てきたおかげで、他の事に使える時間が増えました！銀座カラーで脱毛を始めてよかったです！
            </p>
            </dd>
          </dl>


          <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="錦糸町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

              <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">

              <span itemprop="name">20代 接客<br></span>
            </dt>

            <dd itemprop="reviewBody"><p>彼氏と部屋でのんびりしているときにふと、「言いにくいんだけど、ヒゲ生えてるよ…」と言われてショック！意を決して脱毛をすることにしました。<br>
              前に常連のお客様と美容の話になったとき、銀座カラーに通っている話をされたことを思い出し、私もとりあえず行ってみるかとカウンセリングを予約しました。実際に行ってみると店内の雰囲気が良くって、スタッフさんも優しかったので、本当は何店かカウンセリングを受けて比較しようと思ってたのですが、銀座カラーで即決しちゃいました！脱毛は痛みもほとんど感じなく、時間も短いので苦になりません！予約がとりやすいのも助かってます！最近は彼氏に「前よりもっとキレイになったね。」って言われてすごく嬉しかったです！もっと肌をキレイにして、もっと惚れ直してもらおうと思います！
            </p>
            </dd>
          </dl>


          <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="錦糸町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

              <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">

              <span itemprop="name">10代 接客業<br></span>
            </dt>

            <dd itemprop="reviewBody"><p>女子会で脱毛の話になって、周りはみんなやってるのに、自分だけやってないことに気づいてびっくりしました。自分もやらなきゃと思って、CMが印象に残っていた銀座カラーへ行くことにしました。<br>
              脱毛サロン初体験だった私は、何から何までどきどきしっぱなしでしたが、やさしいスタッフさんが声をかけてくださるので安心でした！錦糸町店に通っていくうちに自己処理の負担がぐっと減って感動です！東京には山ほど脱毛サロンがありますが、私は自信を持って銀座カラーの錦糸町店をオススメします！脱毛のついでに映画を見たり、ショッピングを楽しめるのも錦糸町ならではですね！もっときれいな肌になってオシャレを楽しみたいなー！
            </p>
            </dd>
          </dl>


          <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="錦糸町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

              <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">

              <span itemprop="name">20代 営業<br></span>
            </dt>

            <dd itemprop="reviewBody"><p>通勤中、電車のつり革をつかんだときに、最近忙しくてワキの毛の処理をおろそかにしていたことを思い出して、冷や汗がダラダラ…前に座っている人に見られたかもと思うと恥ずかしくて、穴があったら入りたいって感じでした…そのままふと上を見ると、銀座カラーの広告が！あまりのタイミングの良さに運命を感じて、休みの日に行ってみることに。<br>
              話を聞くと、思っていたよりおトクで即決！通っていくうちにどんどん毛が抜けてきて、自己処理の負担がぐっと減りました！今ではつり革も自信をもって掴めます！
            </p>
            </dd>
          </dl>


          <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="錦糸町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

              <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">

              <span itemprop="name">20代 受付<br></span>
            </dt>

            <dd itemprop="reviewBody"><p>夏にサンダルを履いて出かけたら、足の親指から毛が伸びているのを友人に見られました…普段、仕事の時にはパンプスを履いているので、処理をさぼったツケが…。<br>
              腕やワキの自己処理も面倒くさいなぁと感じていたので、良いタイミングだと思って、今は銀座カラーに通っています。錦糸町店は駅に近くて通いやすい！スタッフさんも優しいし、脱毛も痛くない！ついでにショッピングができたり、映画をみたりできるのもいいですね。この調子で通ったら、次の夏には焦る必要なしですね！
            </p>
            </dd>
          </dl>



 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="池袋サンシャイン通り店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 ブロガー<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>デパートコスメが大好きで、新作が出ては購入しブログにアップしています。ついこの前、新作のネイルを塗って写真を撮りブログにアップしたところ、指毛が生えていることに気づいてすぐその記事は削除しました。あんまり自分では気になっていなかったのですが、綺麗なネイルを塗っても指毛で台無しになることに衝撃を受けて、脱毛を決意しました！<br>
よくコスメを買うので、西武池袋に近い池袋店か池袋サンシャイン通り店かで悩んだのですが、池袋サンシャイン通り店の方が新しい店舗みたいだったので、池袋サンシャイン通り店へ通うことに。<br>
脱毛部位は悩んだ末、指毛と、メイクのノリがよくなるよう顔全体の脱毛も契約しちゃいました。脇などに比べて細くて薄い毛なので抜けるまで若干時間が掛かるとは聞いていたのですが、徐々に毛が薄くなってきたのを感じています。<br>
脱毛してみて、顔はファンデーションやパウダーのノリが良いですし、指毛も気にしなくていいので指先まで自信を持って過ごせています。少しずつですがブログのアクセス数も増えてきたので、脱毛の体験記事も書こうと思います。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="池袋サンシャイン通り店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">30代 主婦<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>若い時に出産・子育てをしているので自分にお金をかけることができず、みんながエステや脱毛に通っているのを羨ましく思っていました。やっと子供が小学校に上がったので、自分の時間が少しずつ持てるようになった今なら自分磨きができるのではと思い、脱毛サロンを探しました。現在東上線沿いに住まいがあるので、電車一本で通える池袋で脱毛を探していたところ、短時間で脱毛できるところを見つけました。それが銀座カラーさんの池袋サンシャイン通り店でした。結婚している手前使えるお金には制限がありますので、両脇と両ひじ下を脱毛することに。念願の脱毛をすることができて本当に嬉しいです。忙しさにかまけて女性らしさを失っていたことに反省です。毎日夫や子供の面倒を見なくてはならないのですが、自己処理をする回数が減ったことで時間に余裕ができましたし、自分に自信がつきました。家族にもより優しく接することができるようになって、夫婦・家族円満にも繋がったと感じています。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="池袋店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 教師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>夏に大学時代の友人たちと海に行った時、前日から飲み会をしてたので、しっかり自己処理ができないまま水着になることになってしまいました。両脇は脱毛済みだったので問題なかったのですが、腕や足、背中やお腹などムダ毛が生えたままだったので水着になるにも躊躇してしまい、みんなが海で楽しんでいる間、私は上着を着たまま一人で待っているだけ...。いざという時に脱げないままではイヤだったので、銀座カラーで全身脱毛をすることに決めました。部位を選んで脱毛する契約に比べれば全身脱毛なので高いですが、水着になったりすると脱毛していないところが目立つので、全身綺麗にしておくべきだと思いました。まだ2回しか通えてませんが、毛が抜けるのが分かりました！特に太くて濃い部分はわかりやすいですね。以前の失敗を活かして、これからはもっと楽しい夏が過ごせるように全身脱毛頑張ります！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="池袋店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>銀座カラーアイズにマツエクで通っていた時、脱毛サロンも経営していることを知りました。<br>
父親が沖縄出身なので私も遺伝なのか毛深くて悩んでいたのですが、脱毛サロンに通いたくてもキッカケがなくて。<br>
そんな時に脱毛サロンのことに気づいたので、すぐ銀座カラーアイズのスタッフさんに話を聞いてみました。その時は簡単な話を聞いただけだったので、改めて携帯から脱毛の予約を取りました。<br>
予約日当日は雨だったのですが、駅から近いのでほとんど濡れずに済みましたし、池袋店は銀座カラーアイズと近い場所にあったので迷うこともなかったです。ビル自体はちょっと古い感じがしたのですが、店舗がある階に降りるととっても女の子らしいキラキラした空間でした。こんな空間で脱毛が出来ると思うとワクワクしました。実際通っていて不便を感じることはないですし、エステティシャンの方も綺麗で優しいので、これからも続けて脱毛していきたいです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="表参道店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">30代 会社経営<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ネイル・マツエク・美容院を1日の中に予定を詰め込む、美容メンテナンスの日を作っています。いま通っているお店が表参道・青山周辺なので、その近くで脱毛サロンを探していました。色々な脱毛サロンがある中で、銀座カラー表参道店が立地で便利だったのと、全身脱毛の施術時間が短かかったので、他の脱毛サロンに比べて都合がつけやすいと思い選びました。<br>
実際に通ってみると豪華なエステサロンのような内装で気分が上がりますし、施術時間も1時間ほどで終わるため、1日の大半が脱毛の施術で終わるなんてことがありません。他の予定に響くことがないですし、前後の予定に余裕がある時は、近くにカフェがたくさんあるので、そこで暇つぶしすることもできます。時間をきっちり使いたい私としては、かなり気持ちのいい予定が組めますし、時間が余っても他にたくさんお店があるので予定を新しく追加することもできるので嬉しいです。<br>
先日美容メンテナンスの日があったのですが、Aoビルに入っている美容院でカットとカラー、銀座カラーで脱毛、青山通り沿いでネイルとマツエクを済ます、というスムーズな1日を過ごすことができました。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="表参道店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 ネイリスト<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>脱毛サロンってお店に入るのに周りの目が気になったり、通っていることを知られたくないなって思うので、路面店の脱毛サロンがあることに最初はびっくりしました。でも、好きなモデルさんが表参道店に通っているブログを見て、私も試しにカウンセリングだけ行ってみようかなと。実際行ってみると、大通りから一本中に入った道にお店があって、その道はまばらに人がいるくらい。お店の外観は綺麗で脱毛サロンとは思えませんでした。路面店は入りにくいと思っていたけど、そんなことありませんでした。昔通っていた、大通り沿いの2Fの脱毛サロンの方が人目が気になって、入りにくかったですね。<br>
初回は無料カウンセリングで伺ったのですが、エステティシャンの方は綺麗な人ばかりですし、かなり丁寧に説明してもらえました。効果を感じてから契約したかったので、その日に契約はしなかったのですが、しつこい勧誘などがなく、スムーズに帰ることができてかなり好印象でした。ここなら自分の身体に合わせて脱毛ができると思って、後日連絡をし契約をさせてもらいました。手頃な金額からはじめましたが、追加で全身脱毛を契約したいと思っています。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷道玄坂店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>「脱毛したい。」が口癖の友達と一緒に、銀座カラーさんへ通いはじめました。春までは大学生だったので自由に使えるお金がなく、エステや脱毛の贅沢は我慢していたのですが、社会人になり仕事も落ち着いてきた今からなら脱毛に通えると思い、よく遊びにいく渋谷で脱毛サロンを探していました。友達が渋谷道玄坂店を選んでくれたのですが、駅から近くてびっくり！更にサロン内は綺麗だし、やりたかった全身脱毛プランは予算内だし、スタッフさんは可愛いし、友達に感謝です。早速1回目の脱毛を済ませましたが、じっとしているだけで終わるので楽だし、保湿してくれるので更にツルツルだし、毛は抜けてくるしで、このタイミングで脱毛に行ってよかったって思っています！しかも、飲食店が多いので二人で新しいお店を開拓する楽しみも増えました！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷道玄坂店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>元々は新宿の方で仕事をしていたので、勤務帰りに新宿東口店へ通っていました。夏に転職をし渋谷勤務になったので、渋谷で通いたいとスタッフに相談したところ、新しい店舗ができるということだったので、新しい物好きな私は最近渋谷道玄坂店へ行くことが多くなりました。会社からは歩いて8分くらいなので少し残業しても間に合いますし、仕事帰りに脱毛して、帰りは同僚と待ち合わせて食事に行けるので、時間を有効に使うことができています。新宿東口店の時から合わせると約1年通っていますが、最近「肌が綺麗だね！」とか「ケアの方法を教えて！」と言われることが増えました。昔からムダ毛がコンプレックスだったので、褒められるのが本当に嬉しいです！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷109前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">マルキューの好きなショップ店員さんに勧められて通い始めました。その人はおしゃれだし可愛いし、夏の露出の多いファッションも着こなしていて、本当に憧れです。肌もつるつるで綺麗なのでケア方法を聞いたら、「近くの銀座カラーで全身脱毛してるよ！」と聞いたので、初めて脱毛に行く事を決心しました！美容系のサロンに行くこと自体初めてでお店に入るまでかなりドキドキだったのですが、スタッフの人が優しかったし、私と同じような歳の女の子のお客さんもいて、安心しました。まだ学生なので高い契約はできなかったのですが、スタッフの人が本当に親切にしてくれて、脱毛の話じゃなく普段の肌ケアの方法とかも教えてもらえました。これから通うことになりますが、肌が綺麗になっていくのが楽しみで仕方ないです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷109前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">渋谷には脱毛サロンがかなり多いので、お店を決めるためにいくつか体験に行ったことがあります。お店によって価格やサービスもバラバラ、良いところもあれば気になるところもある。その点、銀座カラーさんは店内の様子・スタッフさんの接客・技術面など、他の店舗より上回っていた点が多かったように思います。もちろん、改善したらいいのにと思うところもありますが、小さな問題なので、他の店舗に変えようというくらいのアクションには至りません。脱毛の効果は十分実感できています。施術後のミストケアで肌にハリが出てきたような気もしますし、毛はポロポロ抜けてくるし、楽して肌ケアができているので嬉しいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿西口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">昔から脱毛には興味があり、職場近くにあった銀座カラーさんで無料カウンセリングをしたのがきっかけです。脱毛は料金が高いというイメージがあったので中々足を運べなかったのですが、良心的な価格で勧誘なし、口コミの評価も高い、そんなネットの情報を疑いつつ、仕事終わりに行かせてもらいました。お店はビルの７階、ビルの見た目からはまた違って、かなり明るく可愛らしい店内でした。平日夜だったのにお客さんが多かったのがとても印象的で、人が多いのであれば安心だなと思ったのを覚えています。スタッフさんより脱毛についての説明をきいて、初めはネットでの評価を疑っていたのですが、カウンセリングが終わるころにはその評価に納得していた自分がいました。良くないなと感じたら契約せずに帰ろうと思っていたのですが、新宿西口店の良さを知ることができたので、納得して足腕脱毛の契約を結ぶことができました。私のように「脱毛は敷居が高い」と感じている人でも、無料なので一度カウンセリングを受けてみるといいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿西口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">10代の頃からカミソリで自己処理するのが主だったのですが、20代になって代謝が悪くなってきたのか、皮膚の中に毛が埋まる埋没毛になることが増えてきました。膝より下にできることが多く、毛穴が膨らんだり、赤くなったりして、ストッキングを着用していても目立ってきていたので、カミソリの使用を見直すタイミングでした。埋没毛で皮膚科に相談にいった所、自己処理ではなくエステサロンでの脱毛を勧められ、会社から近い銀座カラーさんへ通うことにしました。まだ一回目の脱毛を経験したばかりなのですが、照射時にあまり痛みはありませんでしたし、思っていたより気軽に通えそうだなと思っています。来年は埋没毛を減らして、堂々と足を出せるようになりたいです。</dd>
          </dl>

 <!--<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛サロンが私の住んでいる地域に少なく、新宿南口店まで約2時間かけて通っています。お店が全く無いわけではないのですが、個人経営や小さな店舗が多いのが現状です。身体を預ける身としては、大きな会社であったり通っている人が多いようなサロンの方が、何となく安心するような気がして、時間をかけてでも通うことに決めました。時間がかかるといっても新宿はターミナル駅なので、地元の駅から乗り換え少なく通えます。私はワキと足腕の脱毛をしているのですが、施術後数日でムダ毛がポロポロ抜け落ちる感覚がたまらなく好きで、この達成感にハマっています。施術中はスタッフさんから新宿の情報を教えてもらいつつ、終了後は新宿を探索して、毎回の都会への遠出が私の楽しみです。</dd>
          </dl>-->

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">周りにおしゃれな友達が多いので、たくさん脱毛サロンの情報が入ってきます。いい情報もあれば、悪い情報も。広告をよく見るお店もありますが、やっぱり店舗を選ぶ時は口コミに頼ってしまいますよね。私は色々な情報を聞いた結果、印象が良かった銀座カラーを選びました。学校帰りでもよく来る新宿にあるのがポイント高いし、サークルの先輩も新宿店に通っているので、一緒の店舗という安心感がありました。実際に通ってみると店内は綺麗だし、美容に良いハーブティーが毎回出るので、美意識も高まります。初めての脱毛だったので不安なことばかりでしたが、スタッフの方がとても丁寧に説明してくださり、施術中もファッションの話で盛り上がったりと、毎回の施術が楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="銀座プレミア店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ここ最近低価格な脱毛サロンが増えてきていますが、私は数年前から銀座カラー一筋です。<br>銀座プレミア店に無料カウンセリングで伺った際、担当してくれたスタッフさんが脱毛に関しての質問や相談に笑顔で付き合ってくださって、初めてのサロン通いや契約のやりとりに緊張していた私は、そのスタッフさんのファンになりました。そのうち、どのスタッフさんに担当してもらっても対応が気持ち良く、全員で居心地のいい空間を作ろうとされているのに気付き、銀座カラーのファンになりました。そして重要な施術についてですが、かなりリラックスして受けてますし、施術後のちょっとした肌の変化にもスタッフさんは心配りしてくれます。きっと赤みが出た場合も、丁寧に対応してくれるんだろうなと感じます。そんな接客力に、今後もファンが増えるのではないでしょうか。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="銀座プレミア店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>面倒臭がりな私なので、脱毛サロンに通うのは難しいと思って諦めていました。<br>でも銀座駅から徒歩2分、有楽町駅からも徒歩5分の銀座プレミア店は駅近の好立地なので、苦にならず通えています。昔からムダ毛には悩まされてきたので、通える喜びでいっぱいです。当初は足腕脱毛のみの契約だったのですが、スタッフさんの対応の良さや施術の正確性、銀座ならではの客層に安心感を覚えて、VIO脱毛も追加でお願いしています。VIOは足腕より多少痛みはあるものの、施術後の毛の抜けがはっきりしているので達成感があります。銀座プレミア店は落ち着いた大人の空間を用意してくれているので、アラサーな私でも通いやすい雰囲気です。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿東口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>いくつかのサロンの無料カウンセリングや施術を受けて感じたのは、銀座カラーは技術力・スタッフ力が高いということです。無料カウンセリングの施術はたったの数発しかありませんが、銀座カラーのスタッフさんはこれから行う施術の説明・段取りの説明などはもちろん、その数発もしっかりと照射してくれました。私が通っている新宿東口店だけでなく、他の店舗に通っている友人の話を聞く限り、どの店舗でもそのような対応をしてくれているようです。無料カウンセリングで本契約を決めた私ですが、ここ数年通っていて、より脱毛効果への取り組みなどが見えてきました。昔より脱毛にかかる施術時間がかなり短くなりましたし、もちろんうち漏れもありません。その新しいマシンの導入で、全体の技術力がアップしたように思います。目に見える変化に安心感を感じました。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿東口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ルミネ新宿に好きなショップが多く入っているので、月に2回は新宿に遊びに来ます。<br>元々新宿ではない別の脱毛サロンに通っていたのですが、脱毛だけに出かけるのって面倒だし、交通費も勿体無いじゃないですか。だったら新宿で脱毛サロンを探して、買い物と一緒に通えばいいかと考えました。色々なサロンを検索してみた結果、銀座カラーの乗り換え割（他サロンからの乗り換えで2万円OFF）が一番お得だったので、新宿東口店に決めました。新宿駅から近い・割引があるというだけでサロンを決めてしまったものの、サロン内はもちろん、使う機器もしっかり手入れされている感じがして好印象でした。他店舗を経験している分、スタッフの方の気遣い一つで銀座カラーへのイメージがぐっとアップしましたね。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="銀座本店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>銀座はちょっと背伸びしていく場所のイメージがあったので、脱毛サロンも高級なところばかりかと思っていました。でも、銀座カラーには手頃にはじめられる脱毛コースがあることを知ったので、大人気分を味わえる銀座本店に通うことに決めました。立地のいい店舗によくある、場所にお金をかけてサロン自体は簡素・質素という可能性を考え、サロン内の内装は期待していなかったのですが、受付に大きなシャンデリアがあったりしていい意味で期待を裏切られた感じです。清掃も行き届いていたので、より綺麗な空間に感じました。脱毛の効果も実際の肌で感じられていますし、本当に銀座本店を選んでよかったです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="銀座本店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>銀座本店近くのショップで勤務しているので、仕事場近くに銀座カラーさんがあって助かっています。<br>アパレル関係なので自分磨きは必須。その中でも脱毛は重要だと考えてる私としては、仕事帰りに寄れるサロン選びが重要なポイントでした。営業時間も平日21時までと長く、仕事を急いで終わらせなければならないというストレスもありません。私は夜遅くから全身脱毛コースの施術をするのですが、一時間ほどで終わるので帰りが遅くなることもありません。技術力の高さももちろんですが、帰宅時間まで配慮してくれる女性に優しいサロンのイメージが銀座カラーさんの良さだと思っています。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="天神店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ずっと全身脱毛をしたいと思っていたけど、強引な勧誘が怖かったり、価格を見て諦めていました。でも銀座カラーなら学割もあるし、勧誘がないと聞いて無料カウンセリングを申し込みました。実際に行ってみると、店員さんはみんな優しくて大満足！心配していた勧誘もないし、分からないことも丁寧に教えてくれて契約することに決めました。今では腕も足も自己処理いらずで満足！友達にも脱毛を勧めてます！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="天神店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>大学生なので勉強もバイトもあるし、あまり美容に時間をかけられませんでした。でも就職する前に1度は脱毛したいと考えていましたが、なかなか踏ん切りがつきませんでした。同じ気持ちだった友達と一緒にとりあえず話だけ聞きに銀座カラーの無料カウンセリングに行ったんですが、じっくり話を聞いてくれて嬉しかったです。脱毛のことだけじゃなくて料金のことや、どれくらいの間隔でサロンに通えばいいとかも相談できて、安心して契約できました。友達と一緒に契約したので「ペア割キャンペーン」でお得に脱毛できました！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="天神店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>天神店の近くはオシャレなお店が多いので、お買い物気分で通っています！他のサロンでVIO脱毛をした時に痛くてやめてしまってから、「脱毛は痛い」っていうイメージがありました。でも友達が「銀座カラーは痛くない」というので、行ってみたらほとんど痛くなくて驚きました！スタッフさんの接客態度も良くて快適！家でできる肌ケアのことまで教えてくれました。脱毛後の肌荒れもなくて、銀座カラーを選んで本当に良かったです！思い切って全身契約しちゃいました！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="天神店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>昔から人よりも毛が太くて気になっていました。仕事で腕まくりをする機会が多くて毎日自己処理が大変だったので脱毛することにしました。銀座カラーは他のサロンに比べて予約が取りやすいと聞いたので行ってみると、本当に予約が取れて安心。しかも施術が1回60分位で終わるので、忙しい私にもピッタリでした！価格はリーズナブルなのに、脱毛効果が高くて腕はツルツル。脱毛した箇所は肌の調子が良くなって、仕事でお客さんに「綺麗な腕ですね」と褒められました。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="天王寺店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>仕事が不規則なのでなかなかサロン通いができなくて困っていました。でも銀座カラーなら予約が取りやすいし、天王寺店は駅近で通いやすくて助かっています。美容系のサロンだと勧誘がしつこかったり、高いコースを勧めてくるお店もあるけど、銀座カラーはそういうことがなくてリラックスして施術を受けられます。以前は3日に1回はお風呂場で寒い思いをしながら除毛しましたが、今は月に1回サロンに行くだけなのでとても楽です！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="天王寺店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>友達の結婚ラッシュでドレスを着る機会が増えたので、安く脱毛できるところを探していました。知り合いから「銀座カラーは保湿ケアしながら脱毛できる」と聞いて、無料カウンセリングを受けました。店員の接客が明るくて丁寧で好印象。しかも他のサロンだと最低2年はかかるのが、1年で全身脱毛できるのですごく嬉しいです。価格もリーズナブルだからご祝儀貧乏な私には大助かり（笑）心配していた脱毛後の肌トラブルもないし、これならいつでもドレスを着て肌を出せます！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="天王寺店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>毛はそんなに濃い方ではないので脱毛に興味はなかったんですが、夏休みにたくさん旅行に行きたくて脱毛を考えました。最初は腕と足だけできればいいかなと思っていたんですが、スタッフさんが「全身脱毛の方がお得ですよ」と教えてくれて、全身にすることにしました。初めての脱毛でちゃんと効果があるのか不安だったんですが、段々と毛が細く少なくなっていくのが分かって満足です。最近では自己処理をしなくてもムダ毛が気にならないので、カミソリがホコリをかぶってます。自分では処理するのが難しかった背中もキレイになって、旅行に行くのが今から楽しみです！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="天王寺店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>いろんなサロンのお試し脱毛をやってみましたが、自分に1番あってると思ったので銀座カラーに決めました。他のサロンは追加料金が多くて段々嫌になってしまって...でも銀座カラーは料金がわかりやすくて信頼できます！お店も清潔感があるし、店員さんも親身になって話を聞いてくれるし、ここにしてよかったです。おかげで自己処理でボロボロだった肌がすべすべになってきました！今回は足のコースだけだったので、次は一気に全身のコースをしようかなと思っています。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="藤沢店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>友達に誘われて銀座カラーにしました。脱毛には前々から興味はあったんですが、本当に効果があるのか不安で。友達に効果を聞いて、良さそうだったので無料カウンセリングを受けました。カウンセリングでは無理やり高い契約を勧められることもなく、ちゃんと相談して私にあった脱毛コースを考えてもらえて安心感がありました。実際の施術でもスタッフさんのテキパキとした施術で、サッと脱毛できて満足です。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="藤沢店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>スタッフさんが優しくて、変に敷居も高くないところが気に入ってます。藤沢店は店内もキレイだと思います。他のサロンだと2年以上かかるのが1年で終われるのも評価高いです。飽きっぽい性格でサロン通いも途中で挫折してしまうんですが、これなら最後まで通い続けられそうかなwあと脱毛後に美容効果のあるハーブティーをいただけたりして、そういうちょっとした気遣いが居心地いいです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="藤沢店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>以前に契約したサロンの予約が全然取れなくて、サロン通いに抵抗がありました。でも自己処理でカミソリ負けした肌を見るのが嫌で脱毛サロンを探していました。ネットで「お客様満足度No.1」とあったので調べてみると、予約も取りやすくてお肌のケアもしっかりしてるとあって決めました！脱毛効果はもちろん、「美肌潤美」のおかげでガサガサだった肌がツルスベになって感動です！もっと早く銀座カラーに通えばよかったと後悔してます！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="藤沢店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>CMを見て興味がわいたのでカウンセリングを予約しました。お店の雰囲気が良くて気に入ったので通うことにしました。腕と足はもう別のサロンで脱毛していたので、VIOセットを契約。VIOはどうしても他のパーツに比べて痛みを心配していましたが、ほとんど痛くなくてあっという間に脱毛できました！脱毛効果も高いし、痛くないしでこの値段なら本当に安いと思います。駅からのアクセスもいいし、施術時間も短いので、待ち合わせ前のちょっとした時間に行けてとても便利です！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="八王子店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>様々な学割が出て、脱毛学割なんてないかな〜って探してたら、銀座カラーにありました。学生のうちに安く脱毛できて、学生期間に脱毛が終わるし、あいた授業の間に通えるのがメリットだと思います。カウンセリングや施術してくれる人、みなさん優しいし、サロンも出来たばかりで、清潔感があってオシャレなので、癒されながら脱毛しています。来年の春には卒業するので、学割があるうち、いっぱい得しようと思います（笑）銀座カラーさん、これからも宜しくお願いします☆</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="八王子店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ホントに恥ずかしいんだけど、ひげが生えてて、ここだけはどうにかしたい！！と思い、無料カウンセリングへ行きました。ヒゲ、ではなく鼻下っていうみたいですが、みんな悩んでるって聞いて、自分だけじゃないんだなって思えたし、脱毛する後押しにもなり、顔脱毛をはじめました。顔周りの毛って、毛抜きで抜くと涙が出るほど痛くて赤くなるし、カミソリで剃ると、お肌を切っちゃうかもしれないとビクビクして自己処理するには大変な場所なんだけど、銀座カラーで施術してみたら痛みもなくて、あっと言う間に終わりました。毛が目立たなくなったし、お化粧のノリが良くなったと実感しています。プロにお任せして良かったと思います。美肌潤美のフェイスパックを使うと、しっとり度が増して、お肌の状態がよくなります。オススメです！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="八王子店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>彼氏と付き合い始めた頃に、彼から「女の子って、お肌ツルツルなんだね〜」と言われました。どうやら、女子には毛が生えてこないものだと思ってるみたい・・・（笑）これじゃ、ジョリジョリした時にショックうけるだろうし、いつも可愛くてツルツルな彼女と思われてたくて、彼に内緒で脱毛をはじめました。自己処理しても毛穴は目立つし、自分にしか分からないけど剃り残しがあって、脱毛もそんな一時的なもんだと思っていたのですが、銀座カラー通って、みるみる脱毛効果が。毛穴がだんだん目立たなくなってきたし、自己処理の回数って本当に減るんですね。そして、丁寧に施術してくれるので剃り残しの心配もナシ。いつも自慢なツルツル肌の彼女でいれてます。自慢が、私に自信をつけてくれています。「キレイがずっと近くなる」このキャッチコピー、そのまんまなんだなと思いました（笑）</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="八王子店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>学生の頃から毛深くて、剃っても剃っても生えてくるし、自分では処理しにくい背中を母や妹に手伝ってもらっていました。１人暮らしをはじめると、手が届かない背中の処理に困ったので、脱毛専門店に通うことにしました。何回通えばキレイになるのか想像つかなかったし、回数がおわって追加料金が発生するのも嫌だったので、脱毛し放題コースにしました。この前、実家に帰った時に妹に背中を見てもらったら「毛が薄くなってるし、背中ニキビもない！キレイになってきてるね！」と言われて、本当に通って良かったなって思います。それも満足いくまで施術できるなんて、毛深い私には本当にお得なサロンだと思います。キャンペーンにおともだち紹介があるので、脱毛に興味を持った妹に紹介しようと思います。学割があるみたいなので、私ももっと早くはじめたかったな〜（笑）</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>営業職なので担当地域が変わることが多いのですが、サロン移動ができるところがイイです！どの店舗へ行っても、問題なく続けて脱毛できるので、エステティシャンの教育の徹底が伺えます。<br>
今まではカミソリで自己処理をしてきたので肌荒れがひどかったのですが、「美肌潤美」の保湿ケアでつるつるが保てて、乾燥しにくくなりました。高圧噴射で、角質層の奥まで浸透しているという説明も納得できます。肌トラブルが減った上に、毛穴のポツポツも目立たなくてなって、後輩からも「先輩の肌って、すごくキレイですね」と言われて、嬉しかったです。美肌潤美って、素晴らしいですね。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>無名の脱毛サロンがどんどん増えていますが、昔からあるサロンの方が安心感があるので、銀座カラーに決めました。老舗って古いイメージがあったけど実際に行ってみたら、清潔感があって、おしゃれな雰囲気でした。<br>
痛みには敏感のほうだけど、ほんのり温かいと感じるだけで、痛みもなく施術が完了してました☆早さにもビックリ！あっという間に全身の脱毛ができるんですね！！美容成分が入ったハーブティーも美味しかったです。脱毛後のお肌のこともちゃんと考えてくれているって、さすが老舗のこだわりを感じました。こちらに決めて正解です。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>結婚することが決まったので、早く脱毛できるサロンを検討していたところ、銀座カラーのスピード脱毛を見つけました！ウェディングドレス着たときに見える、ワキ・腕・背中・うなじの4ヵ所予定でしたが、無料カウンセリングをうけてみて、結婚式だけではなく将来もずっと綺麗な肌でいたいと思ったので、全身やることにしました。写真の前撮りや結婚式の予定に合わせ、毛が抜けるスケジュールで施術ができたので、予約がちゃんと取れたのがすごく良かったです。他のサロンにはない、脱毛スピードと丁寧に施術してくれるところが最大のポイントだと思います。＾＿＾♪</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="北千住店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>アクセスがいいので、埼玉県から通っています。全身まるごと脱毛し放題ができるのは、とっても嬉しいです。友達に比べると毛深くて、回数制限のあるサロンはキレイになるのか不安でしたが、銀座カラーなら何度でも通えるのが魅力的です。全身の脱毛１回が、１回の来店で施術対応してくれるので、通う回数も少なくて、毛深い私でもストレスフリーで通えています。しつこい勧誘もないので、友達にも勧めやすいです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="札幌店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ミーハーな私は銀座カラーが札幌にできるのを、ずっと楽しみにしていました。オープンする前から事前予約して、実際に銀座カラーに通えた時はとても嬉しかったです。サロンはシャンデリアがあってゴージャスな雰囲気があり、サロンもスタッフもムダ毛のない肌もキラキラしています。名ばかりじゃない、さすがの大手サロン！スタッフの教育からサービスまで力をいれているのが分かるので、毎回気持ちよく通えます。もちろん脱毛効果にも満足しているので、全身つるつるになったら、全身で恋したいです（笑）</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="札幌店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>職場からとても近く、アクセスの便利性と保湿ケアに惹かれて銀座カラーにしました。24時間ネット予約がとれるので、シフトが変更になっても自分の都合にあわせて予約がとれるので気に入っています。私自身、コスメや肌のお手入れに、こだわりを持っているのですが、銀座カラーの美肌潤美にはとても感動しました☆ ミストシャワーをしてもらった肌は、もっちりで誰かに触ってもらいたくなるほどです（笑）自宅でもケアできるローションとフェイスパックは、まるでサロンでケアしてもらったような感じで、ちゃんと肌を考えてくれていると安心して通えます。フェイスパックには種類があり、その時の気分で購入もできるので、ポイント高いです♪</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="札幌店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>他のサロンに通っていたのですが、友達の紹介で銀座カラーにのりかえました！なんといっても通っている友達の紹介だから、脱毛した肌を実際に見て納得してからサロンに行けるので、信頼度が大きいのが決め手でした。職業上、腕まくりをしている事が多いので、お客様によく見られます（笑）美を扱う者として肌もキレイと言われるのは、やっぱり嬉しいです。これからも自信もって接客できそうです。はじめから全身脱毛を銀座カラーでやっていればよかったなって思うけど、友達に紹介してもらって本当に良かったです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="札幌店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>友達との卒業旅行で、海外のリゾート地へ行くことになりました。友達とおそろいの水着を買ったのはいいけど、自分のムダ毛がとても気になったので脱毛をすることしました。はじめは、ワキ、腕、足、アンダーヘアを脱毛できればいいと思っていたけど、全身の脱毛し放題のほうがお得にできそうだと思ったので、全身脱毛コースにしました。脱毛をはじめて2ヶ月ちょっとですが、ちゃんと予約もとれて3回目の脱毛が無事完了。自己処理が減って、効果を実感してきたところです。海外旅行だけではなく、これからは水着シーンが増えそうです。\(´ω` )/ 脱毛をはじめて本当によかったと思います。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="上野公園前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">アパレルの販売員をしています。都内店舗の異動が多いため、交通の便が良い上野駅付近に住んでいます。<br>
              仕事上、常に身だしなみを整えていないといけないので、休日は美容室・まつ毛エクステ・ネイルなど美容の予定がたくさん。遠くのサロンに行くのは大変なので、すべて近場で済ませたいと思い脱毛サロンも上野駅周辺に変えました。<br>
銀座カラー上野公園前店は、駅から近いのが嬉しいポイント。脱毛予約の前後に他の予定を入れてもスムーズに移動ができて便利です。<br>
また綺麗なパウダールームが完備されていて、ドライヤーなどの貸し出しもあるのでちゃんと身支度ができるから助かっています。<br>
銀座カラーは女性のことを本当に考えてくれているサロンなので、ここに決めてよかったなと思っています。</dd>
          </dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="柏店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


<span itemprop="name">20代 スポーツインストラクター<br></span>
</dt>
<dd itemprop="reviewBody">全身のムダ毛を早くなくしたいと思い、職場の近くにある銀座カラー柏店に通いました。勝手なイメージで、脱毛サロンはシンプルな内装でつまらない場所と思っていましたが、柏店は、店内がゴージャスだしスタッフの方が綺麗なので、行くだけでワクワクできて楽しく通えました!<br>さらに1回の施術時間も早いし、効果を感じるまでも早かったので、銀座カラーを選んで本当によかったなと思っています。<br>あと私は、オプションの保湿マシンもつけたのですが、そのおかげでひどかった乾燥肌も改善されました。ぜひ、他の方にも体験してもらいたいです!</dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="柏店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">

<span itemprop="name">20代 美容部員<br></span>
</dt>
<dd itemprop="reviewBody">銀座カラーで脱毛を始めてから、メイクのノリが変わってきたんです。仕事柄、毎日しっかりメイクをするので、肌荒れがあると隠すのに必死...。朝からテンションが下がっていました。<br>でも最近は、肌荒れすることが少なくなってきたので、メイク時間もファンデーションの使用量も、大幅に減ったんです。接客中、お客様から肌が綺麗と、褒めていただけることもあります!!<br>脱毛のおかげで、ムダ毛の自己処理がなくなって楽なのも嬉しいですが、綺麗な肌状態でいられるのがすごく幸せです。</dd>
</dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="柏店" style="display: none;">
<dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">

<span itemprop="name">20代 学生<br></span>
</dt>
<dd itemprop="reviewBody">柏にある大学に通っています。同じ大学の友達たちが脱毛を始めて、どんどん綺麗になっているので、私も通いたいと思い銀座カラーを紹介してもらいました。<br>バイトのお給料で支払えるか少し不安もありましたが、学割もあってお得に契約できたので、無理なく通えそうです!<br>しかも、紹介してくれた友達にも特典があったので、お互いハッピーになれました。私も、大学の友達にオススメしてみようと思っています。<br>柏店は、大学から近くて通いやすいのが、私には魅力的です!!<br>学校やバイトのスケジュールの合間に通って、どんどん綺麗になっていきます。
</dd>
</dl>


 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷109前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">全身の脱毛が回数無制限でできるのは、本当にすごいと思います。以前、他の脱毛サロンに行っていましたが、そのサロンでは全身コースといっても、iライン、Oライン、Vライン、えり足などは別料金でした。銀座カラーではそのあたりの箇所も含めて、本当に身体中、全部が施術範囲に含まれていてビックリしました。せっかくなので、iライン、Oライン、Vライン全てまるっと脱毛したいので全身VIPコースで始めました。
場所も渋谷の、しかもよく行く109の前なので行きやすいし、スタッフの方も話しやすくていいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷109前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラーにしたのは、お店のある場所がよかったのと金額です。マルキューの前は超便利だし、安いのもうれしいです。費用はいくつかのお店と比べてみたけど、かなりお得に感じました。
意外と気になっていたのが足指に３～４本、生えてくる毛でした。気づいた時には、抜くようにしていたのですが、たまたまネイルサロンに行く前にチェックするのを忘れてしまって、恥ずかしい思いをしました。特に何か言われたわけではないのですが、絶対に気づかれている！と思ったら、すごい恥ずかしくて。。。この事件で脱毛しようと決めました。自分で処理していると、どうしても剃り忘れや剃り残しがあるので、キレイでいることに気を遣いたいけど、面倒なのはなるべく避けたいという人には脱毛は本当におススメだと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷109前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">予約がとりやすいサロンを探していたときに、口コミで評判がよかったので銀座カラーに決めました。以前、通っていたサロンは初回予約は問題なかったものの、２回目以降は自分の希望通りには全然予約が取れなくて、すごく困りイヤになりました。
実は1年後に結婚式が決まっているので、それまでにベストな状態にしたかったので、自分の予定どおり脱毛できるのが、マストな条件でした。こちらのお店だと、予約が取りにくいと思ったことはほぼありませんので、このまま続けていけたら式にも間に合いそうですし、当日、キレイな姿で迎えられると思うと今から楽しみです！
通うのも、いろいろ買い物があるついでに来れる渋谷で、しかも駅からも近いので、ストレスもない感じです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="渋谷109前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">ワキ、腕、脚は自分で見ても、だんだん毛がなくなってきているのを実感していたのですが、先日、友達と温泉に行ったときに「背中、すっごいキレイだね！毛も全然生えてない！！」と褒められてすごい嬉しかったです。背中は自分で見れないので、実感があまりなかったのですが、思わぬところでやっておいてよかったな、と感じました。
夏になると、電車の中や街中でも、背中が開いた服を着ている人の肌は気になって見てしまうのですが、自分も見られていると思うと、そういう服は避けるようにしていました。でも、友達に褒められたことで自信がつきましたし、これからは洋服選びも楽しめそうです。
今通っている渋谷109前の店は、きれいで居心地も良くて、安心して通える雰囲気なので気に入っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="川崎店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">全身脱毛をしています。最初、ワキと腕だけにしようと思っていたけど、スタッフの方といろいろ話をして、背中とかもやりたくなったので、それなら全身脱毛がお得なので、全身コースに。。。今はやっぱり全身コースにしてよかったと思っています。やりだすと、次から次へ、いろいろやりたくなります。きれいになるので(^^)/
忙しいので、通う回数は少ない方がいいので、1回３時間で全身のお手入れができる予約をよく取りますが、予約が取りづらいと思ったことはありません。３時間は長くて大変かなとも思ったけど、意外と話したりしてるとあっという間です。
川崎駅からすぐなので、通いやすいし、サロンの雰囲気も清潔できれいでいい感じです。スタッフの方もすごく感じがいいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="川崎店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">私が通っている川崎店は、川崎の駅前にあって、すごく通いやすいので気に入ってます。銀座カラーはどこの店でも予約が取りやすいので「予約が取れなくて困る…」みたいな悩みはなくていいですね。しかも都内にも店舗が多いので、今度、他のお店にも試しに行ってみたいです！
私は今まで、お風呂の時間がほぼ毎日ムダ毛処理時間と化していました。結構めんどうだけど、仕方がないと思っていましたが、最近ではその回数もずいぶん減ってきて、肌そのものもすべすべになってきたし、色も白くなってきた気がします。勇気をもって始めてよかったです♪今度の温泉旅行がすごく楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="川崎店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">ずっと髪を伸ばしているので、アップにすることが多くて、えり足の毛が気になっていました。自分の、というより、電車やバスで、人のえり足とかを見てると、自分のは大丈夫？？と思うようになり、カウンセリングでお店の人に見てもらいました。その場で自分のえり足も写真に撮って見せてもらい、やっぱりヤバかった！とすぐに脱毛することにしました。この間、美容院に行って、美容師さんに「えり足、きれいだねー」と褒められて、すごく嬉しかったです。毛がなくなるだけじゃなく、肌もきれいになるっていうのが、本当にいいです。自分に自信が持てました。
川崎駅前の銀座カラーは、私の好きなピンク色のインテリアがかわいくて、居心地のいい雰囲気で気に入っています。スタッフの方もやさしいので、声もかけやすくていいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="吉祥寺北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">駅から徒歩3分という近さなので、通うのがとても楽です。通いやすさが銀座カラーに決めた理由の１つです。最初は、一番気になっていたワキだけのつもりで行ったのですが、やっているうちに他の箇所も気になってきたのと、ワキの毛がなくなっている実感がだんだん持ててきたので、全身脱毛で申し込みました。他店に通っていたことはないので比べることはできないのですが、全身コースでも安いという感覚はありました。ワキだけと比べると全身はやっぱり高いけど、支払い方法も分割を選べて助かりました。分割の手数料がかからないというのも良いサロンと思えた理由です。追加料金もないので安心して通い続けています。スタッフの方は優しくて感じがいいし、吉祥寺の街も好きなので、じっくり通って全身ツルスベをめざします！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="吉祥寺北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">住んでいる最寄り駅は吉祥寺ではないのですが、買い物ついでに通えるところがいいな、と思って探して、銀座カラーの吉祥寺北口店に通うことにしました。サロンの周りにパルコやアトレ、キラリナなどがあって便利です。希望通りのお店に出会えました。
回数無制限の脱毛し放題コースを申し込んだのですが、どうしても通えない状況になった時には、お休みしてもOKということだったので、それはとても魅力的だと思いました。何年先も脱毛し放題なんて、すごいと思います！仕事が忙しくなってしまった時や妊娠した時などに、というように、分かりやすい例で説明してくださいました。本当にそういう状況になることもあると思うので安心です。
もう３回ぐらいやりましたが、脱毛だけじゃなく、肌がきれいになっていると感じます。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="吉祥寺北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">全身コースを申し込みました。1時間でまとめて全身脱毛の施術ができると聞いたので、やってもらいました。最初は時間が長いので大変そうだな、と思ってましたが、やってみると意外とあっという間に終わってしまい、拍子抜けするくらいでした。やっぱり、１日でまとめて受けられると効率がいいので楽です。彼氏や友達との予定もいろいろあるので、通う回数を少なくしたいので。。もちろん、自分の予定で、数回に分けてやることもできます。時間や回数が自由に決められるのは、本当に助かります。
スタッフの方も話しやすくて、友達みたいに楽です。晴れている日には、帰りに井の頭公園まで足をのばして、ゆっくり散歩したりもします。楽しく通えて脱毛もできてよかったと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="吉祥寺北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">スタッフの方たちが皆さんとても仲が良くて、アットホームな雰囲気がそのまま伝わってくるので、居心地がいいです。話しやすいのでプライベートなことも、ガールズトークで盛り上がったりすることもあります。でも、脱毛のことはプロとしての意見やアドバイスをいろいろしてくれるので、楽しいだけではなく、安心感や信頼感もあります。
銀座カラーに通う前は、ずっと自己処理をしていたのですが、一生かかる手間を考えたら脱毛にしてよかったな、と思います。あと、サロンによっては予約が取りにくいところもあるということを聞いたことがあり、始める前は不安でしたが、銀座カラーの場合、ほとんど、自分の希望の時間に予約を取ることができているので予定が立てやすいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="上野公園前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学の友達と一緒に違う脱毛サロンに通っていましたが、予約がなかなか取れなかったため、通うことが面倒になり行かなくなってしまいました。<br>
でも薄着になる季節はやっぱりムダ毛が気になるし、友達との旅行などでは自己処理が大変で、もう一度違うサロンで通い直すことを決意しました。<br>
どこかいいサロンがないかたくさん調べ、口コミの良かった銀座カラーに行ってみました。<br>
カウンセリングでは、丁寧に脱毛やお肌のことについて説明してくれましたし、料金についても詳しく教えてくれたのでとてもわかりやすかったです。<br>
また前回のサロンとは違い、予約も取りやすそうなので安心しました。<br>
評判通りのお店だったので、これからは銀座カラー上野公園前店に通って綺麗になりたいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="上野公園前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">30代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">結婚が決まり、来年には挙式を行う予定です。今まで脱毛をやりたいと思いながら、なかなか踏み切れずにいたのでこれを機に始めることにしました。<br>
どうせなら見える箇所だけでなく全身・顔の全てを脱毛したかったのと、ドレスから見える背中やデコルテのお肌ケアも同時にできたらいいなと思い、いろいろサロンを探していました。<br>
銀座カラーは、私の希望通り全身・顔の脱毛がコースに含まれているのと、オプションで脱毛と同時に保湿ケアもできるため即決してしまいました。<br>
今現在も通っていますが、その度に自分の肌が変わっていくのを実感しています。<br>
これなら挙式の時は、つるつるのお肌で迎えられそうです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="上野公園前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客<br></span>
            </dt>
            <dd itemprop="reviewBody">空港で働いているため、京成線沿いに住んでいます。沿線は閑静な住宅街が多いため、休日に友達と遊んだり買い物したりするときは上野に行きます。<br>
脱毛サロンも銀座カラー上野公園前店に通っていますが、京成線からのアクセスがめちゃくちゃ良いところが個人的に大好きです!!<br>
他にも、担当してくれるスタッフの方が美容に対しての知識が豊富で、脱毛施術中にいろいろアドバイスしてくれるのですごく勉強になります。<br>
銀座カラーに通ってから、私の美容意識もどんどん高まっているのでさらに綺麗になりたいと思っています。</dd>
          </dl>

<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="上野公園前店" style="display: none;">
           <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

 <img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


             <span itemprop="name">20代 ネイリスト<br></span>
             </dt>
             <dd itemprop="reviewBody">担当しているお客様で、お肌がすごく綺麗な方がいらっしゃいます。どんなケア方法をしているのか気になり尋ねたところ、銀座カラーで脱毛をしているとのことでした。<br>
私は毛が濃いのが悩みで、極力衣服で隠したり頻繁に自己処理をしたりしてごまかしていましたが、話を聞いて勇気を出しサロンに通ってみようと思いました。<br>
施術の痛みがすごく心配でしたが、いざ始まると眠ってしまうくらい施術は快適で安心しました。少しずつ肌が綺麗になっていく喜びもありますが、大好きなネイルも一段と綺麗に輝いて見えるので嬉しさが二倍です。<br>
銀座カラーを紹介してもらえて、本当に良かったです!!</dd>
           </dl>



 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="船橋北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">他のサロンに通っていたのですが、銀座カラーに変えました。前のところは、すぐに追加料金と言われ、お金がかかって仕方がないので、バイト代がなくなっちゃうし、友達の話とか口コミもチェックして、銀座カラーがいいと思い無料のカウンセリングに行きました。追加料金ばかりで高くなるのは困ると、いろいろ正直に相談したら、銀座カラーはそういうのがないように、脱毛し放題コースがあると言われ、それにしました。何年通ってもいいなんて、すごいです！1度で全身一気に脱毛してもらうこともできるので、忙しいときは便利です。お店はいつ行っても、いい雰囲気で、スタッフの方も話しやすいです。ストレスなく通えそうなので、全身きれいにしたいと思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="川越駅前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">初めてサロンに行ったとき、駅から近いし、駅を出てすぐのところに「銀座カラー」の大きな看板もあったので、迷わずたどり着くことができました。方向音痴の私にはとても分かりやすい場所にあり助かります。
カウンセリングを受けたときに、どうせやるなら・・ということで全身コースにしました。追加の心配がいらないので安心して自分が納得するまでできています。
全身で面積が広いためパーツごとに分けて施術をするのかと思っていましたが、銀座カラーさんは1回で全身を施術するので、施術をした日は全身がとてもスッキリした気分になります。パーツごとに脱毛すると、脱毛した部分としていない部分の境目がハッキリとわかってしまうようですが、全身コースだとそんなこと気にせず脱毛してもらえるので仕上がりも大満足です！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="川越駅前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">他のサロンで体の脱毛はやっていて、銀座カラーでは顔の脱毛をやっています。一番驚いたのは、脱毛することで肌のトーンが明るくなったこと。また化粧ノリもよくなったので、以前よりも薄化粧になったと思います。脱毛をしながら肌もキレイになるとは思っていなかったので、お得な気分（＾＾）前に通っていた店と比べると、顔の施術範囲が広くて、全体的にやってもらえるのはすごく嬉しいです。あと、脱毛とはまったく関係ないのですが、川越駅の駅ナカには、パン、お菓子、スイーツなど美味しい誘惑がたくさんあって、サロンの行き帰りにいつも目を奪われてしまいます。ただ通うだけじゃなくて、こういうおまけもついてくるので、楽しみながら通えています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="川越駅前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">最初にサロンへ行った際、店内の白とピンクのインテリアがとてもかわいくて私好みで、すぐに気に入りました！スタッフの方も、いつもニコニコと笑顔で迎えてくれるので、毎回とても気持ちがいいです。人見知りの私でも話しやすいスタッフの方が多いので、ちょっと聞きにくいこともすんなり聞くことができ、不安に感じることはほとんどありませんでした。あと、脱毛後のアフターケアとして、保湿にはとても気を使っているというのも、いいなと思います。実際、ニキビに悩んでいたのですが、以前ほどひどくならなくなったような気がします。毛がなくなったから、というのが一番なのかもしれませんが、お肌もツルツル美肌になってて、気がつくといつも自分で触っていて、かなりイイ感じです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="川越駅前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">全身脱毛をやりたいと思い、いろいろ調べている中で銀座カラーにたどり着きました。決め手はやはり価格です。他店のコースや料金も見てみましたが、全身コースをやるとなると、ココ！という感じでした。
まず無料カウンセリングを受けましたが、その際に、回数を気にせずに通えること、他店にはない保湿・美肌に力を入れていること、引越しなどで住む場所が変わったとしても違う店舗に移れること、など、いろいろ教えていただきました。料金以外にもさまざまなメリットが感じられて、お願いすることにしました。カウンセリングの時から、スタッフの方の対応は良いし、サロンもきれいで、大丈夫とは思っていましたが、実際に通い出してからも思った通りで、すごく満足しています。脱毛の効果も、美肌の効果もうれしいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="川崎店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">顔の毛が少し濃いのが子供のころから気になっていたので、思い切って脱毛することにしました。最初、不安だったけど時間をかけて丁寧にカウンセリングしてくれたので、いろいろな脱毛の話を聞けました。脱毛して毛が薄くなることでも嬉しかったのに、施術後の保湿ケアも充実してると聞いて施術を受けることにしました。
今、脱毛して本当によかったと思っています。顔の毛を剃らないといけないことはほとんどなくなりました。フェイスラインもすっきりしてきたし、肌にも透明感が増したような！髪を耳にかけたときの横顔の雰囲気が美人になったと、彼氏にも言われました！今まではどうだったわけ？と思いましたが。。。私の効果を見て、高校のときの友達が一緒に通うと言い出しました。川崎店はきれいで通いやすいので、うれしいです♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="町田モディ店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛しつつ肌もきれいになるっていう「美肌潤美」がいいなと思い、銀座カラーで脱毛することにしました。脱毛で保湿ケアもしてくれて、しかもそれが脱毛の費用でできちゃうなんて（追加料金なし！）、かなり魅力的だと思います。
私が通ってる町田モディ店は、駅からも近くて、買い物ついでにも行けて、とても便利です。学校の帰りに寄ることもあります。気楽に通えるのが、続けられる理由かなって思います。スタッフの人もみんな感じが良くて、安心です。
最初は不安だったので、ワキの部分脱毛から始めたけど、全身脱毛の方が断然お得なので、すぐに全身にしちゃいました。肌がきれいになっていくのを実感すると、全身脱毛せずにいられなくなります！今まで避けてた腕や足を出す服も、これからはおしゃれに着れそうですっごく楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="町田モディ店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">「美肌潤美」の保湿力にはホントに感動します。実際に体験したので、ウソじゃなく、美肌効果がハンパないです！
全身脱毛の脱毛し放題コースで通ってますが、他のサロンに比べてもかなり安いと思うし、キャンセルの回数がチェックされたりすることもなくて嬉しいです。予約が取れないこともないです。前に通ってたところは全然予約が取れなかったのでやめました。銀座カラーも最初は不安だったけど、全然大丈夫で予約とりやすいと思います。
電車通勤してるので、町田モディの中にある銀座カラーは通いやすいのもいいです。サロンの中もきれいで居心地がよいので、楽しく続けられています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="町田モディ店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">全身脱毛でもかなり安いので、逆に大丈夫？って始めるときは不安だったけど、20年以上やっていると聞いて、銀座カラーで脱毛してみることにしました。かなり毛深い方で、子供のころから気になっていたけど、脱毛は高いし無理って思ってました。思い切って脱毛することにして本当によかったです。毛がなくなっただけじゃなく、肌がとてもきれいになりました。今は、半袖やノースリーブの服を思い切り着てます。ずっと長袖ばっかりだったので、本当にうれしいです。
私が通っている町田モディ店のスタッフの皆さんは、みんな気さくで優しくて、ちょっとした質問も気軽にできます。最初は少し緊張していましたが、今は全然平気で、肌がきれいになるのもあるけど、サロンに行く日は結構楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="町田モディ店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">今までいくつかの脱毛サロンで無料カウンセリングを受けてみたけど、どこも広告を見て行っても、そんな安い値段じゃできなかったり、かなり強く高いコースを薦められたりして、結局怖くてやめてました。でもやっぱりムダ毛がかなり気になるので、今度こそ！と思ってCMを見て気になった銀座カラーの町田モディ店に行ってみました。
ここは本当に安いと思います。広告の内容と実際の内容も同じで、そこが信頼できて、思い切ってやってみることにしました。スタッフの方も感じがよかったし、駅近で便利なのも評価高いです。
今はかなりのツルスベ感に感動してます。脱毛で美肌になるってホントだったんだ！って感じです。安いので全身脱毛にしましたが、保湿マシン？みたいなのも、エステみたいで、本当に肌がきれいになります～。彼氏にも褒められました(^^)/</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="池袋サンシャイン通り店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">普段は池袋サンシャイン通り店に通っています。池袋には２つ銀座カラーがあるので、こっちで予約が取れなかったときは、もうひとつのお店に行ったりできて便利です。スタッフの人もどっちの店も感じがいいので、どっちに行っても居心地がいいです。
最初はお金の話になり、お得だとは思ったけど、やっぱりワキと腕だけにしました。でも始めてから、脱毛したとことそうじゃないところが、かなり境目ができてしまって、結局全身脱毛にしました。最初から素直に全身脱毛にしとけばよかったです。。境目ができるのは、脱毛したところの肌がきれいになりすぎるから！想像以上にきれいになって、本当にうれしいです。夏が楽しみ～！！！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="池袋サンシャイン通り店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">全身脱毛し放題コースをやっています。髪の毛の生え際からつま先まで、どこをやっても、何年かけてやっても脱毛し放題ってすごい！と思いました。本当にすごいです。始めてよかったです。肌がすごくきれいになってます。友達に肌を見せるとうらやましがられて気分がいいです。この間、友達と温泉に行ったんですが、前はタオルで隠したりするのが普通だったけど、見せたくなる感じですよ（笑）。友達も、しかも2人も！近々、銀座カラーに通い始める予定です。
池袋サンシャイン通り店のスタッフの人はみんな話してて楽しくて、脱毛してる時間もあっという間に終わっちゃう。サロンもすごくきれいですよー。東口からすぐ近くだから、面倒くさがりの私もがんばって通えてると思います。おススメです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="池袋サンシャイン通り店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">毛が濃い方だし、ムダ毛の処理がいつも面倒で、いつか脱毛したいなーと思ってました。でも光脱毛とかちょっと怖いし、肌にシミとかできたりしない？とか、体に悪いんじゃないか？とかいろいろ考えて、ためらってました。昨年、いちばん仲のいい友達が銀座カラーで脱毛を始めたので、めちゃくちゃいろいろ感想とか聞いて、どうやら大丈夫そう！と思ってカウンセリングに行ってみることに。思ったより全然、サロンの人はやさしいし、きれいなサロンで雰囲気もいいし、値段もそんなに高くないことが分かったし、体にも悪くないみたいなので、始めることにしたんです。先に通い始めてくれた友達に感謝してます～。仲のいい友達の話だから、信用できて勇気も出たので！毛が気にならなくなっただけじゃなくて、肌がきれいになってきました。脱毛はじめてよかったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="池袋サンシャイン通り店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛にお金かけるなんてもったいない、と思ってました。でも、剃ったあと、また生えてきたとき、短い毛がチクチクする感じがして、肌も荒れてきて、やっぱり脱毛するしかないかと思い、相談に行きました。最初のカウンセリングで、お金のこと、痛いか、体に何か影響はないか、とか、心配なことはほとんど分かりやすく説明してもらって納得できたので、まずは足から始めることにしました。正直、こんなにきれいになるなんて、と感動しました！3回目で全身脱毛のし放題コースに変更しました。最初からこっちにしておけばよかったです。無制限だし、本当におススメです。全身の脱毛が１日３時間でじっくりできるのもいいです。やっぱり何回も通うより、なるべく１回にまとめられた方が、時間も取りやすくて助かります。仕事のときや休日などで、池袋に行く時と新宿に行くときがあります。どちらのサロンも清潔で、スタッフの対応もいいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="船橋北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">初めての脱毛です。最初はいろいろ分からなくて不安もあって、お金のことも心配で、腕だけを申し込みました。でも、肌がすごくきれいになって、脱毛したところとしていないところの境目が気になってしまって、通い始めて不安もなくなったので、全身脱毛に変えました。今となっては費用的にもかなりお得だし、最初から全身脱毛にしておけばよかった。。って感じです。自分がそうだったので、気持ちは分かるけど、勇気を出して最初から全身コースが絶対にいいと思います！
地元の船橋に銀座カラーがあり、便利だしお店の雰囲気もいいです。仕事帰りに銀座のお店に予約して行ったこともありますが、銀座のお店はかなりリッチな雰囲気。でも、私は船橋のお店が落ち着いてリラックスできます～。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="船橋北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛し放題コースをやっています。洋服が好きで、おしゃれが好きなので、脱毛は絶対やりたいと思っていました。でも高いので、なかなかできなくて。。銀座カラーなら全身脱毛でも、かなりやりやすい金額だと思います。しかも、期限がなく、やりたいだけ脱毛できるってすごくないですか？！銀座カラーを見つけることができて本当によかったと思っています。
今通っている船橋北口店は新しくてきれいだし、雰囲気も良くて居心地抜群です。結構いろいろな年齢の人もいて、通い始めたときは、ちょっと驚きました。女性はみんな、いくつになっても美容が気になるんだなーと思います。脱毛しながら肌もかなりきれいになり、エステに通ってるみたいで、すごくお得だと思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="船橋北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">最初は大学の帰りに寄れる銀座のお店に通っていました。地元の船橋にもお店ができたので、最近はこちらに通っています。休みの日でも通いやすくなって、しかも新しいので明るくてきれいで、すごくいいです。スタッフのみなさんも年が近いし、話しやすいので、楽しく通えています。最近、妹も通い始めました！仲がよくて一緒にお風呂に入ったりもするので、私の肌がきれいになるのを見て、うらやましくなったみたいです。自分だけ、ムダ毛処理してるのがイヤになったんだと思います（笑）。実際、バスタイムにやることが減って、ゆっくりお風呂につかれるので、快適です～。
あと、保湿マシンがすごいです！全身に贅沢な美容液がいっぱいしみ渡って、肌がプルプルで感動的です。ぜひ試してみてください。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿東口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">新宿三丁目駅からすぐの新宿東口店。大学の帰りに新宿を通るので、新宿東口店に通うことにしました。全身脱毛は高いのかなと思ったのですが、無料カウンセリングの時に、学割特典があると聞き、やるなら学生のうちだと思い、全身VIP脱毛コースに決めました。他のサロンで予約が取りにくいなど友人から聞いたことがあったのですが、銀座カラーだと3ヶ月に1回なので予約も取りやすく、ストレスなく通うことができて良かったです。全身脱毛をして本当に綺麗になったので、今までの自己処理の大変さを思うと大学生になってすぐに通って本当に良かったなと思います。ムダ毛は一生付き合っていくものだと思っていたので、早いうちに脱毛してしまう方が楽でいいですね。しかも美肌潤美という銀座カラーオリジナルの保湿ケアのおかげで、肌質もしっとりしたのでそれも良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿東口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">もともと全身脱毛するほど全身が気になっていたわけではなく、全身ハーフ脱毛を契約しようと思っていましたが、ハーフに少しの金額を足したら全身できるのと、ハーフでやった後に全身にしておけばよかったと後悔する方もいるとのことで、どうせやるならお得な方がいいかなと、全身脱毛にしました。やっていくうちに、背中など、普段気にしていなかったところにけっこう産毛が生えていたことがわかって、全身にしておいてホントによかったと思いました。新宿東口店は駅から約3分のビックロの前にあり、新宿での買い物ついでに気軽に通えました。新宿の中でも大型店らしく、若いスタッフさんたちが仲良くお仕事している雰囲気が伝わってきて居心地がよかったです。あと、入ってすぐのシャンデリアが実はお気に入りでした。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜西口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務職<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラーの横浜エリアの店舗に通っていますが、横浜西口店は駅の出口からすごく近くて便利です!!面倒くさがりの私でも、利便性の良さもあって雨の日でもちゃんと通えていますよ。脱毛の施術も、スタッフさんがすごく気遣ってくれるので、何も不安なくストレスなしで受けられています。いつもうとうとしている間に終わっているのですが、効果はちゃんと出ているので、安心しておまかせしちゃってます。あとサロンで出してくれるハーブティーが好きなので、職場のデスクで飲むお茶もハーブティーに変えてみましたよ。銀座カラーで教えてもらった良いことは、取り入れていきます!</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜西口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">土日はプライベートの予定を優先したいので、仕事帰りに通えるところを探していました。職場近くに銀座カラーがあることを知り調べたら、平日21時まで営業だったので、さっそくカウンセリングを受けて即決しちゃいました。値段がリーズナブルなのに、お店の設備が充実していてびっくりしましたし、スタッフの方々も親切で丁寧な対応をしてくださいました。たまたま見つけたサロンが、こんなに丁寧なところだったのは本当にラッキーです!早く綺麗にならないか、楽しみに通います。
</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜西口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛サロンは初めてでいろいろ不安でしたが、最初のカウンセリングが無料だったので行ってみようと思いました。脱毛方法とか料金など基本的なことも教えてもらいましたし、ちょっと相談しづらいVIOの脱毛についても相談にのってもらえたので、不安はかなり解消されました。通い始めてからも、脱毛についての疑問は施術の時に質問していますが、聞けばすぐに答えてくれるので安心できます。信頼できるサロンが見つかって、本当によかったです。
</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜西口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">横浜西口店は、スタッフのみなさんがいつも笑顔で迎えてくれますし、施術中も楽しくおしゃべりができるので女子会のような雰囲気で楽しく通えます。サロン内も清潔感があり、パウダールームが充実しているので帰りの支度もバッチリできて、いつでも安心して予約できます。長く通うサロンだからこそ、居心地の良さが重要ですよね。脱毛に関する悩みも、カウンセリングから丁寧に聞いてくれましたし、今後がイメージしやすいように施術の際にアドバイスをしてくれます。まだ始めたばかりだけど、綺麗になった肌を想像してワクワクしちゃいます!!</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="立川店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">キャンペーンを見て家から近い立川店に行きました。安いので接客にはあまり期待していなかったのですが、スタッフさんの対応がとても丁寧で感じがよかったので契約しました。休日には3歳の子どもを連れて家族で昭和記念公園に行くことが多いので、公園に行った後、子どもがお昼寝している時間にサロンに行くように予定を組んでいます。公園に行った日は疲れてお昼寝も3時間コースなので、全身脱毛の1回約3時間の施術とちょうどいい感じでパパとのお留守番も大丈夫そうです。全身脱毛は冬から始めたのでだいぶ効果も出てきていて、今年の夏は昭和記念公園のプールにビキニで行けそうで楽しみです。全身脱毛なら銀座カラー！と、友達にもオススメします。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="立川北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">何年も前に他の脱毛サロンでワキの脱毛は済んでいたのですが、最近脱毛が安くなったなと思っていろいろ調べていました。銀座カラーは全身脱毛が安いと評判だったので調べてみると、立川は2店舗あってどちらも駅から5分ぐらい。どちらにしようかとサイトで店内の写真を見たら、立川北口店のロビーの椅子が我が家の椅子とそっくり！インテリアの好みが合うのは居心地がいいかなと思って、立川北口店にカウンセリングに行きました。いろいろ質問して、他の部分脱毛と比べてみても全身の方が断然オトクだったので、即決。美肌潤美という保湿マシンを使ったアフターケアがオススメのようで、そのおかげか、仕上がりがいつもぷるぷる、もちもち肌になります。以前行っていたワキの脱毛の時はタオルで冷やすだけだったので、アフターケアは大違いだと思いました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="立川北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">お店の雰囲気や設備、スタッフの接客態度も丁寧なので、とても来やすい感じがしました。
立川北口店の担当の方が、とても笑顔の素敵な方で、丁寧な説明等で安心して施術を受けることができました。人それぞれだと思うのですが、スタッフの方から効果が表れてきてることを伝えてもらったことは嬉しかったです。最後のお茶も些細な楽しみです！ 初めて利用させていただいて大変満足しました。今回担当してくださった方は接客も仕事ぶりも本当に丁寧で、とても好感が持てました。必要以上に気を遣わなくてよい空間が心地よかったです。正直、契約回数や金額に不安があったのですが今日サロンに行ってみて決めて良かったと思いました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="立川北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">立川駅といえば中央線が止まるし、デパートやショッピングビルがあったりととても便利なので、駅から徒歩5分の立川北口店で予約をとりました。脱毛サロンはたくさんありますが、銀座カラーに決めた理由は、全身を1回で施術できること、銀座カラーオリジナルの美肌潤美という保湿ケアがとてもいいということでした。脱毛箇所を冷やすことはどのサロンもしているようですが、美肌潤美は脱毛後にミストシャワーで肌の火照りをしっかりクールダウンしてくれつつ、肌を綺麗にしてくれるそうです。実際に美肌潤美のケアをしてもらって、その保湿力の高さに驚きました。通うたびに毛量が減っていくのはもちろんのこと、どんどん肌質が良くなっていくのも感じたのでお得だなと感じました。銀座カラーにして本当に良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="立川北口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学が近くにあるので、学校帰りに友達と一緒に通ってます。初めて行った時は、大人可愛い素敵なカフェに来たみたい！と友達と二人で興奮しちゃいました。ちょっと背伸びして大人の世界に足を踏み入れたような気分でした。ハーブティーなんかも出してくれるので、ホントに毎回大人気分です。カウンセリングも友達と一緒にできたし、予約も一緒に取れるので、友達と一緒に通いたい人にはオススメ。私が行った時には「ペア割」というキャンペーンもしていてかなりお得でした。立川はショッピングも楽しめるので、いつも、友達とランチ→銀座カラー→ショッピングのセットで通ってます。脱毛後は、お互いのつるつる、もちもち肌を自慢し合っていつもすごく盛り上がってます。一緒に通うといろいろ共有できていいですよ。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿東口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">新宿は学校帰りにも習い事にも休日にも、ほんとによく行く身近な街です。特に伊勢丹やマルイなどがあるエリアが好きなんですが、銀座カラーの赤い看板がいつもなんとなく気になってました。友達とたまたま脱毛の話になった時、なんと！4人も銀座カラーに通っているとわかりました。そのうち2人は新宿東口店、他の2人は家の近くで通ってましたが、私が入るのをきっかけに、みんなで新宿東口店に行くことにしました。ベッドが10台以上あるようで、友達と同じ日に予約出来た事も何度かありました。友達とわいわい行っても、スタッフさんもとてもフレンドリーなので気を遣わなくてすんで楽です。通い始めて1年ぐらい経ちますが、ワキの自己処理はほとんどしてないですね。毛周期に合わせて次の予約が取れるので、脱毛効果を感じてる間に次の予約がくるっていう感じでいいペースです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿東口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">今回担当頂いた新宿東口店のスタッフさんですが、無駄のない動きで施術もスムーズでした。私はどちらかと言うと施術中に話をしたくないタイプだったので空気を読んで頂いたのか必要最低限のトークで「さすがだな」と思いました。人によって施術中に話したい人、話したくない人が居るので見極めて頂けると助かります。これまで担当してくれた方はみなさん、親切に施術してくれるので有難いです。脱毛は、恥ずかしい部分もお願いするので、淡々と進めてくれるのが一番安心します。脱毛自体が初めてなので、色々なお話やアドバイスが聞けて良かったです！どのスタッフさんもいつも丁寧に対応してくださってとても嬉しいです。良い意味で「スピーディー」な対応で、説明も施術も丁寧で『安心感』がありました。その誠実な対応が素敵でした。</dd>
          </dl>

 <!-- <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛の施術中ちょっと冷たかったり、箇所によっては痛みもあるところもありますが、担当の方が楽しい話で和ませてくれてうれしかったです。横浜店はあまり営業されない感じの所がすごくいいです。私は全身の脱毛コースにしたのですが、顔と鼻下を脱毛した時にクレンジングが気持ちよくて、とてもリラックスできました。終わったあとも暖かい飲み物を出してくれるし、いつも丁寧な対応で気持ちよくサロンに通えています。ずっと毛深いことが悩みでしたが、今では処理もぜんぜんしなくなったので本当に脱毛して良かったです。
実はエステティシャンになりたいので、来年から銀座カラーの社員としてお世話になります。私も接客していただいた先輩方のように笑顔を絶やさず、お客様の目線に立って接客できるエステティシャンを目指したいと思っています。</dd>
          </dl> -->

 <!-- <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">西口からも近くてお店も明るい印象です。スタッフの方も皆さん優しくて、サロンに行くといつも笑顔で迎えてくださり気さくに話しかけてくださるのでアットホームで居心地抜群でした。脱毛というと痛みが一番の心配だったのですが、最初にパッチテストをしてくださるので、どれ位の痛みかもわかるし、安心して施術をお願いすることができました。私は子どもの頃から毛深いのが悩みで、いつか全身脱毛をしたいと思っていました。背中など自分が見えない部分は自己処理できないのでプロにお任せしたいと思い銀座カラーへ行きました。無料カウンセリングでは親身に相談に乗ってくださったので、脱毛し放題の全身VIP脱毛コースに決めました。これなら何年経っても気になるたびに通えるしずっと安心だなと思いました。ムダ毛がとても気になる私は、脱毛し放題がある銀座カラーにして良かったと思います。</dd>
          </dl> -->

 <!-- <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラーの横浜店に通ってます。横浜に3店舗あるうちの一番歴史が長いお店らしく、スタッフの技術が確かだという評判を聞いたので決めました。若干地味なビルの中にあるので初めて行った時は心配になりましたが、中に入ってみると白くて明るく綺麗なお店だったので安心しました。カウンセリングの時に、全身脱毛し放題のコースは満足いくまで何十回でも通えるので一番お得という話を聞いたので、脱毛し放題で通ってます。全身脱毛は1回3時間ぐらいで終わりますが、長くて寒いと感じる場合は何回かに分けてできるそうで、寒い時期になったらそうしようかなとも思っています。今のところいっぺんに終わってしまう方がラクなので3時間ぐらいかけてやってもらってます。3回終わったところですが、順調に毛がうすくなってるのでこれからが楽しみです。</dd>
          </dl> -->

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="梅田新道店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラー梅田新道店は、北新地駅から地下道をとにかくまっすぐ5分ぐらい進み、地上に出たらすぐ看板が見えるところにあります。初めて行った時もわかりやすく、すぐに到着できました。店内が白くて明るく、清掃も行き届いている感じでとてもキレイなので、いつも気持ちよく通えています。脱毛効果は3回目で文句なしの抜け具合です。美肌効果もすごくて、大学の友達に腕がすべすべになったねとほめられたので、今では友達も紹介して通っています。友達はまだ1回目ですが、私が見てもわかるぐらい効果が出てます。紹介キャンペーンで特典もあったので、私も友達も大喜びでした。あと、月々払いができるのが学生には嬉しいです。今年の夏は、友達と海に行くことが増えそうです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="梅田新道店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">梅田新道店は、どのスタッフも笑顔でとても好印象です。今回担当してくださった方は特に親切に、そして丁寧に応対してくださるので、安心してお任せできます。脱毛をして痛かった時にジェルを足してくれたおかげで、痛みがかなり軽減されました。1人のお客さんに対して思いやりがあるので、他の方達にもきっとこの満足度が広がると思います。心のこもった対応が非常に嬉しかったので、これからも利用したいです。銀座カラーさんは値段が安く、大手なので安心して通える上に、接客は他のサロンよりも格段に良く、スタッフ皆さんの接客サービスの質が高くて気に入っています。サロンの清潔さも利便性も予約の取りやすさも言うことなしです。全身脱毛ということで最初は迷っていたのですが、いま思えばもっと早くしておけばよかったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="梅田新道店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">梅田新道店は赤、白、黒、を使ったモダンで落ち着くサロンでした。スタッフの方が和気あいあいとしていて、よく話しかけてくださるので、緊張もすぐに解けてリラックスすることができました。私は顔の脱毛がしたくてフリーチョイス脱毛のコースで、顔、鼻下の2カ所をお願いしました。クレンジングをしてくださる時にフェイシャルマッサージをしてもらっているようで、とてもリラックスできて気持ち良かったです。そして施術前の美肌潤美ローション、施術後の美肌潤美ミストシャワーのどちらも気持ちよくて、しかも保湿効果がとても高いとのことで、毎回とても楽しみでした。これは銀座カラーオリジナルだそうです。これのおかげもあって、6回終えたあとは、ムダ毛がなくなって肌の色が明るくなっただけではなく、お肌がもちもちになった感じがしてとても良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="梅田新道店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">接客業でお客様にいつも顔を見られる仕事をしているので顔の脱毛をしました。銀座カラー梅田新道店のあるビルには脱毛サロンが3つも入っていたので、全部カウンセリングに行ってみました。カウンセリングだけでは違いがはっきりとはしなかったので、スタッフさんのお肌をじーっと見比べ、一番つるっとしていてお化粧のりもよかった銀座カラーにしました。いくら言葉で説明しても実際のお肌をみればわかりますからね。独自のミストで美肌効果や肌荒れ防止効果があるそうで、あのお肌のつるつる感はこのミストのおかげなんだと納得できました！毛がなくなるだけでなくお肌もキレイになってお得ですよね♪私も、最初にお会いしたスタッフさんのようなつるっとした素肌になりましたよ。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金沢駅前東店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学生になったらワキの脱毛をしていいと母と約束していたので、ワキ脱毛に通いました。母から勧められたのは銀座カラー。全身脱毛で有名なお店らしいのですが、母と「最初はワキだけ」と話していたのでそうしました。カウンセリングでは、お店の方からやはり全身脱毛が一番人気との話がありましたが、母との約束を話すと、「10代だし、初めての脱毛はワキからですよね。」と優しく聞いてくれたので安心しました。母からは少し痛みはあるよと聞いていましたが、私は光脱毛は痛みが全然なかったです。目を隠してやるので、何をしているのか見えませんが、いつ光を当てたのか全然わからないうちにいつも終わっていました。ワキがつるつるになったので、自分のバイト代を貯めて、今度は別の部分も脱毛したいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金沢駅前東店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">いつも施術してくださる金沢駅前東店のスタッフの方はとても接客態度が良く、施術も丁寧で、脱毛に関して不安だったり分からないことなど、いろいろと相談しやすく、こちらも自然と笑顔になるくらい、素晴らしいサロンだと思います。全身脱毛し放題コースの脱毛をしました。今まで自己処理に時間がかかっていたのですが、サロンで脱毛して自由な時間も増え、かなり気持ちが楽になりました。長年毛深いということになやんでいたのですが、脱毛することでその悩みやコンプレックスがなくなって、自分に自信が持てるようになりました。全身脱毛し放題コースを受けて、意識してこまめに保湿をしっかりするようになりました。背中など、見えない部分で手が届かず、自己処理もなかなかできないところだからこそ、サロンで脱毛して本当に良かったなと思いました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金沢駅前東店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛サロンに入るというのは周りの目が気になるなと思っていたのですが、ポルテの近くなので、ショッピングのついでに入れるので気兼ねなく通えました。無料カウンセリングでお話を伺ったところ、一部が綺麗になると他の箇所も綺麗にしたくなる方が多いようで、金額的にも全身脱毛し放題コースがとてもお得だと聞きました。確かに全身脱毛だと、脱毛の境目も気にならなくなるのでいいなと思いました。分割でのお支払いができること、私が支払いたい回数なら手数料もかからないということもあり、全身脱毛し放題コースに決めました。脱毛をすると乾燥するようですが、銀座カラーオリジナル保湿ケアの美肌潤美のケアをしてもらったので乾燥も気になりませんでした。6回、約1年半通ってとても綺麗になったので大満足です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="金沢駅前東店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">金沢で予約の取りやすい脱毛サロンと評判の銀座カラー。近くに駐車場はありますが、有料なので、電車で行くことをおすすめします。土日に通いたい方は前もっての予約が必要そうですが、平日に通えるのであれば、1週間前でも予約が取れます。子どもの予定などで急な予定変更がある私にはとてもありがたいサロンでした。私のとても好きな空間でした。脱毛の痛みもなく、施術は気持ちよく、サロンに通う日はエステに通っている感覚で、自分へのご褒美時間でした。施術がすぐに終わるので、帰りにポルテ金沢でゆっくりお茶して帰れるのもリラックスできる時間でした。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="立川店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">年齢とともに鼻下の産毛がヒゲのように濃くなって気になってきました。友達にも「ちょっと濃くない？」と言われ銀座カラーさんを紹介してもらいました。保育士という仕事上、子供たちと顔を近づけてお話することも多いので、子供に「おひげだ～」と言われる前に脱毛をしてしまおう！と思い通うことに決めました。立川店は職場からも近いのでとても通いやすくて、面倒くさがりの私でもさぼることなく通えています。スタッフの方は対応が丁寧でサービスが行き届いているなーと感じました。はじめは顔に機械をあてて脱毛するなんて…と怖かったのですが、一度すると怖さや痛さの不安も全くなくなりました！段々とですが鼻下の毛も薄くなってきて、お化粧するときも気分が良いです♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="立川店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">立川店は以前通っていたエステサロンと違い、丁寧で親切で、いつもスタッフの方の対応、笑顔がよく、気持ち良く通わせて頂いております。ワキと腕の照射の際、両手をあげる姿勢が少し辛かったのですが、すぐに声をかけてくれるなど、一つ一つとても丁寧に対応して頂くので毎回行くのが楽しみです！新しい情報を教えて頂き＆提案していただきありがたかったです！今後とも立川店に通いたいです。これからも脱毛に通いたい友達などいたら紹介したいです。サロンを利用するのは銀座カラーさんがはじめてなので不安があったのですが、丁寧に説明してくださるのでずっと利用していきたいと思いました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="立川店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">立川駅から徒歩2分にある立川店は、仕事帰りに寄るのにとても立地が良かったので、無料カウンセリングに行きました。社会人になり、今後結婚することも考えると、その前に綺麗にしておきたいなという気持ちが強くなり脱毛することに決めました。どうせやるなら全身脱毛をして一生ムダ毛に悩まされないようにしたいなと思っていたのですが、やはり金額的なことや、最後まで通えるかという不安がありカウンセリングのときに相談させていただきました。全身VIP脱毛だと金額的には高いと思いましたが、分割で支払いできること、分割回数によっては手数料もかからないこともあり、安心して契約をすることができました。それに平日は夜9時まで開いているので、仕事帰りに寄れるという手軽さもとても良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿西口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">とても落ち着いてる雰囲気の店舗で良かったです。これからは新宿西口店でお世話になろうと思います。担当の方がとても丁寧で印象が良く、施術もテキパキと素早く、いい気持ちで帰れます。肌が弱く自己処理後の肌トラブルに不安を抱えたまま今後の施術の進み具合に不安を感じていましたが、その日担当していただいた方から丁寧に肌の事や施術の進め方について、説明をしていただきました。とても話しやすく親切な方だったので、いままで疑問に思っていたことも聞く事ができたので今後も安心して施術を進める事が出来ます。何度か担当してもらった方の接客や説明やアドバイスはいつも素晴らしいし、安心感があります。仕事のお休みが少なく、サロンに通う日程調整に苦労していましたが、予約のときに親切に対応してもらったのでとても助かりました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿西口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">私が通った新宿西口店は新宿駅西口から近く、清潔感の溢れるとても綺麗なサロンで、スタッフの皆さんも優しくてアットホームな脱毛サロンでした。水着を綺麗に着るために、腕、足、背中、Vラインが気になっていたので、それなら全身VIP脱毛の方がお得ですよと教えてもらい全身VIP脱毛にすることにしました。全身脱毛のお値段がお安いということもあり、銀座カラーに来るお客さんは全身脱毛に興味のある方が多いみたいでした。全身の毛が、サロンに行くたびにどんどん薄くなって、自己処理しなくても良くなっていくのは自分でもとても嬉しく、今まで毎週剃っていた大変さを改めて感じました。Vラインの痛みが気になっていたのですが、他の箇所に比べると多少気になる程度で、我慢出来る痛みだったので安心しました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿西口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">新宿西口店は小田急ハルクのちょうど裏手にあり、西口のオフィスビル街で働いている私は、会社帰りにとても通いやすかったです。脱毛サロンに行くのに人目が気になる私には、路地を入ったビルにあるところもよかったです。就職して初めてもらうボーナスで全身脱毛をしたいと、学生の頃から思っていたので、全身脱毛といえば銀座カラーというイメージで足を運びました。施術をしながらスタッフさんとお話するのが毎回楽しみで、仕事の悩みなども聞いてくれたのがよかったです。銀座カラーの全身脱毛は、1回の施術で一気に脱毛できるので、1回の時間はかかりますが、何度も通わずにすんでいいと思います。施術して1,2週間後に毛がたくさん抜け落ちる時期があり、それもいつも楽しみでした。今では全身つるつるです。ありがとうございました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="千葉店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">千葉店は駅から5分ほど歩いた所にあるので、予約まで時間がある時は近くでウィンドウショッピングやカフェを楽しんでいます。
以前にワキを他店で脱毛していたのですが、他店では施術の熱をとるために冷たいタオルで冷やし、冬場にはとても辛かったです。でも銀座カラーでは、美肌潤美という「ヒアルロン酸とコラーゲン」が入ったミストをかけ、ローションを塗ります。そのために、肌が水分量を含んでぷるぷるとし、フワフワとした柔らかい肌触りに変化していました。施術から数日経った今も、塗布した場所は肌触りが良いので、これからの施術も楽しみになりました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="千葉店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">千葉店に通うのは今回で2回目ですが、2回とも親切で丁寧な接客で非常に満足です☆今日担当して頂いた方は施術がすごく丁寧だったし、 接客もすごく良いので、これからずっと担当してもらいたいくらいです。今まで関わったスタッフみなさん素敵な笑顔で優しくて、利用していてとても気持ちが良いし、次の施術も楽しみになります。店舗に行って嫌な思いは一度もしたことがなく、いつも丁寧な施術には満足しています。初めて行く店舗で迷ってしまったのですが、皆様にとても丁寧に対応して頂き助かりました。帰り道もきちんと教えくれるし、とても安心できました。強引な勧誘もなく、とても居心地が良かったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="千葉店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">千葉店は千葉駅から近くとても通いやすかったです。店内はとても広く天井も高いので、毎回リラックスできる非日常空間でした。最初に無料カウンセリングに行き脱毛したい箇所を数カ所伝えたところ、「それなら全身VIP脱毛の方がかなりお得ですよ」と教えてくださり、価格や痛みなどの不安についても親身になって丁寧に答えてくださったのでとても信頼出来るサロンだなと思いました。しかも全身の場合、2～4回に分けるサロンが多いなか、銀座カラーでは1回で全身の脱毛ができてしまうというのも通う回数が少なくて楽なので魅力的でした。金額も分割での支払いができるので毎月の金額はとても安いところが良かったです。痛みは輪ゴムでパチンと弾いたくらいと聞いていて、実際にそれくらいの痛みだったのであまり気になりませんでした。全身脱毛をして自己処理をしなくてよくなって本当に良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="千葉店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">腕と足の脱毛をしたくて行ったのですが、カウンセリングの時にお店の方が自分の体験談を話してくれました。はじめはワキ、それから、腕、足と脱毛していくうちに他の部分や境目が気になるようになってきて、結局やる箇所が増えていったと聞いて、絶対私もそうなるだろうなーと想像がつき、おもいきって全身脱毛することにしました。料金も全身脱毛のほうが断然オトクで、後から増やしたくなったら損だと思いました。最初にお店の方の体験談を聞けて、ほんとによかったです。親切なお姉さんに感謝です。お店は千葉駅から5分くらいのビルの中にあって、近くにカフェもあるので、早めに行ってお茶してから通うようにしていました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名古屋駅前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">今まで自分で処理をしていましたが、肌もブツブツになり毛が埋まってしまったりして、夏場も長袖をきていました。銀座カラーに通ってどんどん肌がきれいになり、立ち振る舞いも美しくなっていく友人の紹介で、私も無料カウンセリングに行きました。値段も手が届かないのかなと思い込んでいましたが、意外にも低価格で、月にかかる料金はスマホの使用料金とたいして変わりはありませんでした！
名古屋駅から徒歩5分の場所で清潔感があり、スタッフの皆さんもいつもアットホームな雰囲気で出迎えてくれるので、とてもリラックスして施術をうけることができます。また、スタッフが私の肌や毛の状態を見て、私にあった通い方をその都度アドバイスしてくれるので、とても助かります。いつかは全身つるつるになり、全身美人を目指して頑張ります。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名古屋駅前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">私も接客の仕事をしていますが名古屋駅前店はどのスタッフさんも柔らかい笑顔と気遣いがあってとても良いと思います。いつもたのしいお話ができて、気分も二倍良くなります。はじめてお店に行った時はすごく不安でしたが、スタッフさんが話しかけてくれたり、痛くないかなど、こまかく聞いてくださったので安心して脱毛を受けられました！また、色々アドバイスをくれて本当にためになりました！！これからもずっとお願いしたいです。二回目の施術を受けに予約の時間の少し前に来たら、中で待てるようになっていて嬉しかったです。ジェルが冷たいけどすぐに温めてくれたり、毎回「大丈夫ですか？」と声をかけてくれてくれるスタッフの方の気遣いにとても思いやりをを感じました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名古屋駅前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">名古屋駅前店は名古屋駅から徒歩5分という立地の良さで通いやすかったです。清潔感あふれる温かみのある雰囲気のサロンだったので居心地もとても良かったです。無料カウンセリングの際に、女性は3つの首（えり足、手首、足首）を綺麗にしていると若く見えると聞いたので、そこも含めてぜひ脱毛したいと思いました。確かに一年を通して、手首から指先は見えますし、えり足が綺麗だと髪をアップにするのも自信をもって出来るので、本当だなと思いました。元々ヒジ下とひざ下の脱毛を希望していたので、全身VIPハーフ脱毛のコースにして、手の甲・指、えり足も含めてお願いしました。施術の前後に美肌潤美という銀座カラーオリジナルの保湿ケアをしてもらえるのですが、それがとても気持ち良く、帰宅後もしっとり感が続いていてとても良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名古屋駅前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">初めて行った時に、2回目以降はネットで簡単予約できることと予約が取りにくい時は、店舗移動も可能という説明をしてもらったので私は名駅と栄の両方に通っています。白と赤のインテリアが明るい感じの名駅店と、ちょっと大人っぽい雰囲気の栄店、私はどちらもお気に入りなので予定に合わせて通っています。脱毛は初めてだったので痛みが心配でしたが、私がやったワキ脱毛は「え？もう終わったの？」と思うほど痛みもなく、終わるのも早くてびっくりしました。最新の脱毛の機械は痛くないんですね！すぐに終わるので、友達とショッピングの途中で、友達がお茶してる間にちょっと行ってくるっていうこともできました。名古屋はどっちのお店も場所がいいので通いやすくて便利です！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名鉄岐阜駅前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">彼氏ができたので、夏に海やプールに行きたい！と思って、冬の間に脱毛をしようと決めました。私はVラインの毛がとても広範囲に生えていて、下着からもはみ出てしまうのが悩みでした。ショーツを買うときも、水着を買うときも、毛がはみ出ないデザインのものしか買えず、気に入ったものが買えない時もあって残念な感じでした。ショーツからはみ出てしまう毛は抜いて対応していましたが、抜いた後の毛穴がプツプツふくれたり、毛が生えてくる時にかゆくなったり、Vラインの悩みは書き出したらきりがないです。カウンセリングで相談した結果、Vラインだけでなく、VIOというコースを月額払いで通っています。<!--2回目の施術から効果がよくわかり、毎回通うのが楽しみになっています。名鉄岐阜駅前店はお店がいつもキレイなので、行くとテンションが上がるサロンです。名鉄岐阜駅からも近いので、仕事帰りに寄っています。--></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名鉄岐阜駅前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody"><!--名鉄岐阜駅前店はお仕事帰りに寄れるので、とても便利で利用しています。-->担当者の方が、脱毛するときの痛みやお手入れ後のお肌の状態についてなど、不安に感じていることを丁寧に説明してくれたので、安心して施術を受けることができています。全身脱毛し放題コースにしていますが、一回で全身の施術をしてくれるので通うのもとても楽です。今までの中で一番効果が嬉しいのはV上部とサイド。Vラインが清潔になりショーツから毛がはみ出なくなったし、水着も毛をきにせず着れるくらいになっているので大満足！美肌ミストが全部のコースに含まれているのがとても良かったです。特に背中は普段自分でお手入れしにくくて困っていた部分だったので、通うたびにお肌がきれいになることが楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名鉄岐阜駅前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody"><!--私が通った名鉄岐阜駅前店は、岐阜駅から徒歩7分で、とてもわかりやすい場所にありました。-->店内はとても明るく清潔感溢れる心地よいサロンでした。VIOと顔のムダ毛が気になっていて無料カウンセリングを受けたのですが、全身パーフェクト脱毛コースならVIO全部と顔も脱毛できることがわかったので、このコースに決めました。VIOは最初恥ずかしかったのですが、エステティシャンの方はプロで慣れているので、そんなこと全然気にしなくていいんだと思うと、気が楽になり安心して施術をお願いすることができました。VIOをして良かったところは、やはり毎月の生理時の匂いやムレがなくなったこと、そしてどんな水着も安心して着られることです。今までムダ毛がはみ出さないかを基準に選んでいたのが、今ではどんな水着も着られるので海やプールに行くのがとても楽しみになりました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名鉄岐阜駅前店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody"><!--名鉄岐阜駅から徒歩1分という、とても通いやすいサロンでした。-->お店はスタッフの方とお客さんの距離が近いアットホームな雰囲気のサロンでした。私はおしゃべり好きなので、子どもの話や美容の話など、いろいろしゃべっている間にいつも終わってるという感じでした。そんな私のおしゃべりにもいつも丁寧に付き合ってくださる方ばかりで、とても居心地のよいサロンでした。施術後には美肌潤美という銀座カラー独自のミストがセットになっていて、保湿と鎮静効果が高く、いつも潤った肌で帰ることができました。年齢を重ねて、肌の乾燥も気になっていたのですが、脱毛後はぷるぷるです♪残り2回なので、おしゃべりタイムも、気持ちのいいミストタイムも終わってしまうと思うとさみしいです。</dd>
          </dl>

 <!-- <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラー横浜店でワキの脱毛をしています。高校時代までは制服は半袖だし、ワキはカミソリで剃るぐらいで十分だと思っていました。大学生になり、電車通学でつり革をつかむようになると、つい、隣のOLさんのワキに目が行くようになりました。OLさんは脱毛してツルツルの方が多くて、私はつり革をつかむのが恥ずかしくなってしまいました。友達に相談したら、銀座カラーに通っているとの事だったので、紹介してもらって一緒に通っています。ワキの脱毛は10分ぐらいであっという間に終わるし、痛くもなくてびっくり。紹介してもらってホントによかったです。横浜店のスタッフさんとはネイルの話やマツエクの話などをして、お姉さんから美容について教えてもらってるみたいで、それも通う楽しみの一つです。どのスタッフさんも丁寧で、素敵なお姉さんばかりです。</dd>
          </dl> -->

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="銀座プレミア店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座駅から徒歩2分という好立地の銀座プレミア店。脱毛サロンは若い子が通うイメージがあったのですが、銀座という場所柄なのか、店内はとてもラグジュアリーでアラサーの私でも安心して通うことができました。子どもが小学校に通うようになり自分の時間ができるようになったので、少しは自分に手をかけたいなと思って脱毛をしようと思いました。妊娠をした時にお腹周りの毛が濃くなり、水着になるのも億劫になっていたので、そんなことも無料カウンセリングで相談させていただきました。主人が転勤のある職業なので、6回終わるまでに転勤になったらどうしようという不安があったのですが、引越しをしてもその近くの銀座カラーで施術を続けることができると教えていただいたので、そこも銀座カラーに決めたポイントでした。ママ友にも教えてあげたいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="銀座プレミア店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラーで脱毛しようと決めてから、会社の近くのお店にしようか休日によく行く銀座にしようか、HPで見比べてみました。銀座プレミア店は、モダンな感じのボルドーのソファが印象的で、他のお店とはインテリアの雰囲気が少し違っていました。落ち着いた感じの銀座らしいインテリアがとても好きで、こちらに通うことに決めました。VIOをお願いしたのですが、下着からはみ出る毛も気にならなくなりましたし、iラインがスッキリしたことで生理の時のムレもなくなりました。デリケートな部分だけに多少の痛みはありましたが、これだけ楽で綺麗になるんだからやってよかったと思っています。スタッフさんも銀座だからか上品な方が多く、脱毛や美容の話以外の無駄話がないのも好感がもてました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="三宮店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">バイトと大学と家と活動範囲が広いので、交通の便がいい三宮駅周辺で脱毛できるところを探していました。三宮店は駅から近くて、とてもわかりやすい場所にあって良かったです。母が他サロンの脱毛に通っているんですが、他サロンにはキャンペーンで一部の箇所だけ安いコースがあって、他の箇所を契約しようとした時に結局高くなってしまったと話していて、「やるなら最初から全身脱毛でやりなさい」と言ってくれたので、全身脱毛がお得な銀座カラーに決めました！明るくポップな空間でカジュアルな雰囲気だったので、学生の私でも通いやすくて良かったです(*^^*)スタッフのお姉さんも気さくでいろいろと相談にのってくれて、施術だけじゃなくて会話も楽しめちゃいました。また、ベット数も多くて予約がとりやすいので、希望通りに予約が取れているのも嬉しいです♪このあいだ1回目の施術に行ったんですが、腕や足などとても効果を感じられたので、他の箇所もこれからが楽しみです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="三宮店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">三宮店は清潔感があってとても良い印象です。いつも丁寧な対応で気持ちの良い接客をしてもらえるので、これからも通うのが楽しみです。質問したことにも時間をかけて丁寧に対応してくれたので安心して通うことができました。しっかり脱毛したいと思っていた部分のムダ毛がなくなるまでサポートして頂いて嬉しかったです。知人に紹介してもらいお店へ行きましたが、とても丁寧に説明していただけるので安心できました。デリケートゾーンを照射するとき、痛い部分を照射前に教えてくれたり、少しずつ照射してもらったのでいつもより痛みが少なかったり、ちょっとした細かい気遣いをしてもらえたことが嬉しかったです。長時間の作業も笑顔の対応でとても感じが良かったです。スタッフみなさんがとても親切で笑顔が素敵でした。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="三宮店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">三宮でバイトをしていたので三宮で脱毛サロンを探していました。無料カウンセリングに行ってお話を聞いたら、学生には脱毛学割など特典が多かったので銀座カラーに決めちゃいました。三宮駅から徒歩5分というのも魅力的！普段よく見えるヒジ下とひざ下を脱毛したかったのでフリーチョイス脱毛でお願いしたんですけど、両方合わせても1回30～40分と施術時間も短くてあっという間に終わるので、バイト前やバイト帰りに寄ることができて良かったです♪予約の取りやすさとスタッフの技術でランキング1位をとったことがあると聞いたんですが、確かにその通り！予約は取りやすかったし、どのスタッフさんに担当してもらっても毎回納得のいく施術でした～。予約がとれなかったら、自分がやりたいタイミングで施術してもらえなくて困るし、スムーズに6回通えたので本当に良かったです｡</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="三宮店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">兵庫県内に銀座カラーは2店舗あるようでしたが、ベッド数が多く広いようだったので、こちらを選びました。以前、他の脱毛サロンに通ったことがあるのですが、お店の綺麗さは銀座カラーさんのほうが格段によいです。また、私はアラサーですが、他のお客様やスタッフさんも年齢層が低すぎず、なんだかとても安心して通えました。今回は腕の脱毛をしたのですが、カウンセリング時にスタッフさんがご自分の腕を見せてくださり、「続けているとこれぐらいキレイになりますよ」とおっしゃられて、ゴールが見えた感じでとてもわかりやすかったです。6回のコースで、腕はつるつるすべすべになりました。施術後のミストも肌をキレイにする効果があるそうで、こんなにキレイになるならまた別の箇所もお願いしようかと思っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="神戸元町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">大丸の隣にある元町店。元町駅からはもちろん三宮駅からも徒歩で通え、大人の雰囲気漂う落ち着いた脱毛サロンです。
1カ所だけなら安い脱毛のサロンはありますが、私は色々な箇所の脱毛をしてみたいと思ったので銀座カラーを選びました。全身VIPハーフ脱毛というコースで全部で8カ所の部位を選ぶことができます。痛みの感じ方は個人差があり、箇所によってもかなり違うと思いますが、腕と足は全く痛くありませんでした。皮膚の薄いデリケートな部分は少しゴムではじかれたような痛さを感じましたが、我慢できないほど痛い！というものでありませんでした。また、脱毛後に肌を鎮静させるために行われる美肌潤美は、ヒアルロン酸・コラーゲン・マイナスイオンなどの美容成分が入ったミストスプレーのようなもので肌に潤いを与えてくれて、脱毛した肌のほうがプリプリでこれからのお肌が楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="神戸元町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛に関しての悩みや、シミは薄くなるのか、肌荒れは良くなるのか・・・といった質問にも丁寧に答えてくれたので安心して通うことができました。神戸元町店の店内のゆったりした雰囲気はとても居心地が良く、スタッフの方もとても話しやすかったです。全身VIP脱毛をしたときの美肌ミストがまるでエステサロンに通ったように気持ちよく、脱毛の回数が進むにつれてお肌の調子が良くなったのもとても嬉しかったです。脱毛に関しての悩みやわからないことなども丁寧に聞いてくれて、おすすめのコースを提案してくれました。腕と足だけを脱毛しようと思っていましたが、いろいろなところが気になっていたので思い切って全身脱毛にして良かったです。今まで気になっていた部分に加えて全身脱毛で気にしていなかった部分も綺麗になって嬉しいので、ムダ毛に悩むお友達にも勧めたいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="神戸元町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">神戸元町店は白と木目を基調としたサロンで、行くたびに気持ちが落ち着くサロンです。神戸大丸のすぐ裏にあり、私は毎週というくらい神戸大丸で買い物をしているので、そのついでに寄れるのがとても便利で通いやすかったです。冬の時期にはルミナリエもあり、施術の帰りにルミナリエを見て帰ることもありました。脱毛サロンって少し敷居が高いのかなと思っていたのですが、無料カウンセリングに行った際にもスタッフの方が皆さん笑顔で優しかったですし、アットホームな雰囲気のサロンだったので安心することができました。コースで迷っている時にも、優しいお姉さんという感じで親身になって相談に乗ってもらえたのもとても心強かったです。脱毛が初めてだったので、まずはフリーチョイス脱毛で通って、終わって他にも気になる箇所があれば、増やしていこうかなと思っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="神戸元町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">最近脱毛サロンが増えているので、正直どこがいいのかわからなくなっていました。おもいきって友達に相談してみたら、一緒に通おうということになり、紹介されたのが銀座カラー神戸元町店さんでした。白くてオシャレな雰囲気のお店で、広すぎずアットホームな感じが伝わって来ました。私は脱毛サロンに行くのが恥ずかしいと思っていたのですが、このお店の雰囲気はとても居心地が良かったので、友達が誘ってくれたのも納得です。初めての脱毛だったので、まずはワキだけにしたのですが、どんどん毛が薄くなっていったのに感動！しかも毛穴や黒ずみもなくなっていき、自分じゃないようなキレイなワキを手に入れました。スタッフさんの感じもよくて、通えば通うほど好きなサロンになっていったので、別の箇所も契約しちゃいました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">JR新宿駅の東口から徒歩3分の通いやすいサロンです。店内は清潔感の中にキラキラと輝く大人可愛い空間が広がっています。脱毛後の「美肌潤美」というお肌に良い様々な美容成分がたっぷり入ったミストを噴射することにより、肌が保湿され、水分量の高いプルプルのお肌になるのも驚きです。脱毛効果だけでなく、肌のトーンも上がったり、肌のキメが細くなったり美肌効果もあるので、このまま継続し頑張って全身すべすべにしていきたいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">初めての全身脱毛だったのでとても緊張していましたが、担当者の方がメニューだけでなく、脱毛後の乾燥しやすい箇所のケア方法なども教えてくださったので凄く安心しました。新宿店は、いつも気持ち良く利用させてもらってます。とても丁寧に接してくれるので、施術中も寝てしまうほどリラックスできます。全身脱毛で通っていますが、担当の方がご自身の脱毛体験のお話をしてくださりとても参考になりました。顔の脱毛は他のお店だとメニューになかったりしますが、おでこの部分も脱毛できて良かったです。ムダ毛がなくなって、お肌がワントーン明るくなり、化粧ノリも良くなったと実感しています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">新宿店は、JR新宿駅から徒歩3分と駅からとても近かったので便利で良かったです。清潔感溢れるサロンで居心地も抜群！スタッフも若い方が多いので、会話も盛り上がって、あっという間に終わってしまう感じでした。会話の内容も美容系のことが多く、知識が豊富な方が多いので、今流行りの美容方法やコスメの情報を教えてもらえてとても勉強になりました。そして脱毛には保湿が大事ということで、「美肌潤美」という銀座カラーオリジナルの保湿ケアを取り入れていて、施術の前にはローションを、施術後にはミストシャワーをしてくださるので、しっとり肌になれたのがとても良かったです。回数を重ねるごとに肌質も良くなった気がするのでとても嬉しいです。</dd>
          </dl>

 <!--<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">仕事で新宿を通るので新宿の脱毛サロンを探していました。ホームページで見て銀座カラー新宿南口店のお店がとても綺麗そうだったので、まずはカウンセリングに行きました。白い壁とオレンジ色のソファがとても素敵で、ハーブティーまで出してくれて、オシャレなカフェに来たような感じでした。スタッフさんの対応もとても丁寧で感じがよかったので、このサロンに決めました。通ったのは「足全セット」です。保育園に通う娘に、お風呂で「ママの足チクチクする」と言われたのがショックで、足脱毛を決めました。膝下だけにするか迷いましたが、膝周りの脱毛をすると、色素沈着した膝もキレイになるとのことだったので、足全部をお願いしました。本当に通えば通うほど膝周りも白くてキレイになり、もちろん膝下も娘から「ママの足つるつるしてきもちいい」と言われるようになりました。</dd>
          </dl>-->

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新宿西口店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">職場から近く新宿駅の西口から徒歩5分という好立地にあり、平日は21時までやっているので、仕事帰りに施術をお願いすることもあります。場所柄OLさんが多く清潔感あふれるあたたかい雰囲気です。
また、「全身で恋しよう」の広告を見てはっとしました。今片思い中ですが、いつか彼が振り向いてくれた時に心も体も見えるところだけでなく、自信を持てる自分になりたいなと思います。脱毛後の美肌潤美というミストにより脱毛前の肌よりも水分量があがり、プルプルになるのが実感できます。肌に自信がでてきたらいつもより明るい色の洋服を着てみたり、いつでも恋ができるよう自分磨きを頑張ります。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="宇都宮店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学生になったら全身脱毛をしたいと思っていたので、通いやすい場所にある銀座カラー宇都宮店へ無料カウンセリングに行きました。他のサロンに比べて価格が安いとはいえ全身脱毛は高額なものなので、カウンセリングの時に親身になって話を聞いてくださり不安がなくなったのは本当に良かったです。しかも学生ということで脱毛学割というのがありこんなにお得な金額で全身脱毛できるなんて信じられませんでした。もし全身脱毛したいと思っているなら、学生のうちがお得なのでオススメです。しかも3ヶ月に1度通うだけなので、それも楽で良かったです。1度に全身の施術ができるサロンはなかなかないので銀座カラーにして本当に良かったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="宇都宮店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">腕や足などの気になるところだけを脱毛するか全身脱毛するかを迷っていたのですが、友達の紹介で銀座カラーへ行ってみました。行ってみて、サロンの綺麗さに感動！壁も床も白くて明るく、清潔感のある空間がとても気に入りました。ちょっとオトナなサロンにいる気分になれました。スタッフさんと相談して全身脱毛を契約しました。月々の分割支払いもできたので、毎月のお給料から1万円ずつぐらいで通うことができました。お店も綺麗だったので、ネイルサロンや美容院に行くような感覚で毎回通えています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="大宮店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">大宮店は、大宮駅東口から歩いて5分のところにあり駅からとても近いです。以前から脱毛に興味があったのですが、すでに大宮店で全身脱毛を始めている友達が友達紹介キャンペーンで、お互いに商品券がもらえると聞き、無料カウンセリングへ。無料カウンセリングと言っても個室に通されるわけでもなく、間接照明が効いた白×黒のスタイリッシュな空間の広い待合室で、ハーブティーを飲みながら、スタッフの方ととてもフランクな感じで行われます。個室だと無理やり勧誘されてしまうかも・・・という心配もありますが、銀座カラーのカウンセリングはオープンスペースで行われるため、勧誘ではなく、それぞれの悩みに合わせた適切な箇所への脱毛のアドバイスをしてくれます。予約が取れるかが気になっていたのですが、実際通ってみると予約も取りやすく、毎回ゆったりとした気持ちで脱毛施術を受けられているので、銀座カラーにしてとても良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="大宮店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">サロンっていつも緊張してしまうのですが、大宮店の担当者が、気持ちの良い対応でとても安らぎました。すごく丁寧に対応してくれて、次回指名したいほどの最高の接客でした。顔の脱毛の時は、まるでフェイシャルエステのような心地良さでした。施術した箇所は、脱毛はもちろんのこと、肌もキレイになっているので、これからも美肌効果のある脱毛の方針で続けてほしいです。以前より施術の痛みが格段に減ったのもとても嬉しかったです。施術中に寒さを感じるときなどは、電気毛布をしいて私を気遣ってくださり、ベッドが温かくて心地よかったです。またVラインの施術が初めてで不安があったのですが、丁寧に説明してくださり、安心してお任せすることができました。こちらの店舗で施術してもらうのは初めてだったのですが、優しく対応してもらえたので安心して脱毛できました。次回からも安心して通えるサロンだなと感じました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="大宮店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラー大宮店に無料カウンセリングに行って驚いたのは、その広さとサロンの雰囲気の良さです。とても広くスタイリッシュな雰囲気は、私が今まで脱毛サロンに抱いていたイメージをくつがえしてくれました。スタッフの方も皆さん優しくて、いつも笑顔で迎えてくれるので安心して通うことができたのも良かったです。私は夏に向けてVラインが気になっていて相談に乗っていただいたのですが、今まで受けられた方のお話を聞いているうちにVIOセットにしたくなりVIOセットでお願いしました。VラインだけでなくiラインとOラインも一緒にすると、境目も気にしなくていいし、水着もどんなものを選んでも安心して着られるのがすごくいいなと思いました。そして施術後は匂いやムレもなくなり、生理中も快適に過ごすことができるようになったのでとても良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="大宮店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">地元の群馬県に銀座カラーのお店がなかったので、大宮店まで通いました。1時間もかからないので遠くは感じませんでしたが、銀座カラーの脱毛はとてもオススメだし、これから別の箇所も脱毛していこうと思っているので群馬にもお店を作って欲しいです。今回は、一番気になる腕の脱毛のコースにしました。初めての脱毛だったので、痛みのことが一番心配でしたが、腕の脱毛は痛みをほぼ感じませんでした。毎回、スタッフさんとおしゃべりするのがとても楽しく、しゃべっていたらあっという間に終わっていました。アフターケアでしてくれる美肌潤美というミストは保湿効果だけでなく、美肌効果もあるとの説明があったのですが、黒ずみもあった腕が本当に白くすべすべになったのでとても嬉しいです。ノースリーブを着るのも、電車のつり革をつかむのも全然気にならなくなりました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="表参道店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学の近くにある表参道店は、地下鉄表参道駅B2出口より徒歩1分のビルにありました。強く勧誘されたらどうしようと思っていたんですが、契約するまで帰さないとか電話で強く勧誘されたりということもなくて安心しました。逆に、無料カウンセリングの時に、ラグジュアリーでキラキラと輝く大人の空間で、アップスタイルにしたきれいなスタッフさんばかりで、私も脱毛して肌が綺麗な素敵な女性になりたいと強く思いました！施術後の美肌潤美のミストが最高に気持ちよくて、乾燥していた肌がしっとりぷるぷるになって嬉しかったです(^^)脱毛に来ているというより、エステに通っている気持ちで通っています。大学の友達にも勧めたいと思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="表参道店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">前回まで他の店舗に通っていたのですが、引越しにより今回から表参道店にお世話になることに。スタッフの方もあまり忙しい感じを見せずに、こちらもゆったりと過ごすことができました。表参道店は気分的にもリフレッシュできる空間だと感じ、とても満足しています。対応してくれた担当者さんが施術も説明もとても丁寧で印象が良かったです。ぜひまたお願いしたいと思いました。初めて意見をお伝えしたいと思うくらい、丁寧な接客でした。とても一生懸命ということが伝わってきましたし、施術中も楽しくお話ができました。表参道店は接客態度がよく清潔感があり良かったです。ドライヤー利用やメイク直しの有無を確認し、パウダールームの場所を丁寧に教えてくれました。また施術中も熱くないか声かけをしてくださるので、安心して施術を受けることができました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="表参道店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">私が通った表参道店は表参道駅からすぐだったのでとっても便利でした。店内が明るくキラキラしていて、女子力が上がりそうな雰囲気だったのも嬉しかったです｡大学生になって、サークルのみんなと海に行くことになったので、その前にVラインを脱毛したいと思ったのが無料カウンセリングを受けるきっかけでした。カウンセリングで話を聞いているうちにVIO全てを脱毛したいなと思ったんですけど、VIOセットと全身VIP脱毛が2万円ほどしか変わらなかったことと、学生証の提示での割引もあるということだったので、思い切って全身VIP脱毛で契約をすることにしました！実際に全身脱毛をして、もうどこも自己処理をしなくていいというのは本当に楽で、全身脱毛にして良かったなと心から思っています(*^^*)これでもうムダ毛に悩まされることがないので、全身脱毛をして本当に良かったです｡</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="表参道店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">以前、会社の近くの銀座カラーさんで契約をしたのですが、2回ほど通った後に異動になってしまい、通うのが大変になってしまいました。予約を取らないまま1年以上過ぎた頃、習い事で表参道に行く機会が増え、その帰りに表参道店を見つけました。習い事の日に予約を合わせればまた通えるかもしれないと思い相談すると、店舗の変更ができるとのこと。表参道店は大人可愛い素敵なサロンで、広くて開放的な感じが私はとても好きでした。以前の店舗と全体的な雰囲気は似ていますが店舗によって広さやスタッフさんの感じが違うので、いくつかの店舗を見比べて自分に合ったサロンに通うのもいいのかなと思いました。施術内容や使っている機械はどこのお店も同じだと思うので、何度も通うなら自分の気に入った雰囲気のサロンを選ぶのがいいのかなと2店舗通ってみて思いました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="梅田茶屋町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学、バイト、サークル活動と毎日が忙しくて、活動範囲も広いので、どこにいても交通の便がいい梅田茶屋町店に決めました！以前、他店でひざ下とワキを施術したのですが、バイトの先輩から「見えるところだけやっても、見えないところのムダ毛の自己処理に時間がかかって大変でしょう。どうせなら全身やった方がいいんじゃない？」とアドバイスをしてもらったので、思い切って全身脱毛をしようと思ったんです。先輩も銀座カラーで全身脱毛をしたみたいで、自己処理しなくていいからとても楽だと言っていました。確かに先輩はお肌がすべすべなんです！そして通ってみたら、ムダ毛がなくなるどころか、銀座カラーでは美肌に力をいれていて、施術後の美肌潤美というミストのおかげでお肌がぷるぷるになったので、私自身も大満足だし、綺麗になった肌を見て彼氏も喜んでくれているので良かったです｡私も先輩みたいにすべすべお肌になれそうです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="梅田茶屋町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">私が通っている梅田茶屋町店のスタッフさんは、いつも丁寧な接客と対応をしてくれるし、施術中の会話もすごく楽しいので、毎回通うのがとても楽しみです。無料カウンセリングに行ったときも、しつこい勧誘もなくて安心して契約をすることができました。回を重ねるごとに脱毛効果を実感できるようになってきているのもとても嬉しいです。初めて脱毛の体験に行ったときからお世話になっている担当者さんは、私の話をよく覚えてくれていて、毎回担当者さんに会えるのが楽しみです。私の希望や悩みを聞いてくれるだけじゃなくて、肌の状態に合わせた脱毛のコースを一緒に考えてくれるところがとても良かったです。気になっていた部分が綺麗になると、他のところも気になってしまうのですが、その度に丁寧なカウンセリングをしてくれるので、安心して通うことができます。梅田茶屋町店にして本当に良かったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="梅田茶屋町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラー梅田茶屋町店はアクセスも良く通うのにとても便利でした。スタッフさんみんなが一致団結をしているというか仲が良さそうな感じが伝わってきて、アットホームなサロンでとても雰囲気が良かったです。どのスタッフさんも顔見知りみたいになってどの方とも色んな話ができたのも楽しかったです。以前、ワキや腕は脱毛済みだったのですが、仕事でドレスを着る機会があるので、背中の脱毛をお願いしました。背中の上と下で1回約30～40分の施術で、3ヶ月に1度通うだけだったのでとても楽でした。背中の毛の自己処理は見えなくて危ないので一度もしたことがなかったのですが、脱毛をしたら肌のトーンが明るくなったので嬉しかったです。施術前後の保湿ケア、美肌潤美のおかげで肌質も良くなって美肌になったので、とてもお得な気がして嬉しいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="梅田茶屋町店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">就職して半袖の制服を着ることになり、腕の毛を1年中剃らなくてはいけなくなったため、腕全コースの脱毛をしました。どうせやるなら割安の全身がいいかなと迷いお店の方にも勧められましたが、他の場所はそれほど気になっていなかったのでとりあえず腕だけにしました。腕だけと決めた後は、お店の方から他の箇所を勧められることもなく、対応が良いなと思いました。銀座カラーは光脱毛なので痛みもほぼなく、施術後の保湿に力を入れているということで、肌状態がとても良くなりました。毛穴も目立たなくなり、今では自分の腕のキレイさが自慢です。お店は阪急梅田駅から徒歩5分とアクセスも良く通いやすいのが嬉しいポイントです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="銀座プレミア店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座に2店舗ありますが、私は最近できた銀座プレミア店を選びました。ワインレッドのソファーや椅子に囲まれて、他のエステ店と比べても高級感があり、働いてる従業員の方も来ている方も綺麗な方が多く、満足度の高い大人の空間でした。地下鉄銀座駅より徒歩2分と、広い銀座を歩き回る心配もなく、場所もとてもわかりやすかったです。先日1回目の施術で腕・足を受けてきましたが、施術後、毛がするっと抜けていくのにびっくりしました。痛みもほとんど感じず、徹底した肌ケアの効果でお肌もぷるぷるです。毛のサイクルは3ヶ月毎に施術がするのが良いサイクルのようで、帰る際に次の予約をいれてきました。 さすが、楽天リサーチ第1位のお店ですね。これから通うのが楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="銀座プレミア店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座プレミア店は、どのスタッフさんに担当して頂いても皆さん接客や施術が丁寧なので、安心して脱毛を受けられます。口コミで読んだ通り、銀座カラーのスタッフさんは脱毛の無理な勧誘が全然ないのですが、他の箇所を脱毛しようか悩んで相談した時にはきちんと説明してくださったり、今のお肌の状態を見てアドバイスをしてくださるので、安心して相談することが出来ます。ジェルを付ける際に冷たさを紛らわせるために話しかけてくださったり、少し赤みがでた場所を念入りに冷やしてくださったりと、担当者さんの対応が素晴らしかったです。スタッフさんの対応もサロンの雰囲気もとても良くて、脱毛の効果や保湿ケアで肌質も良くなってきているので、最後まできちんと通いたいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="仙台店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">今までVラインの事をあまり気にした事がなかったんだけど、大学生になってサークルの仲間と海に行くことが決まってから、海に行く前にVラインの脱毛をしたいと思うようになったんです。それで銀座カラーのVIO脱毛に通うことにしました。それからは下着からムダ毛がはみ出す事もなくなってその違いにとても驚きました！Vラインは毛が濃い部分なので、脱毛して綺麗になると印象が全く違って、本当に嬉しかったです(*^^*)生理中には匂いやムレも気になっていたのが、ムダ毛がなくなったおかげで清潔に保てるので、気にならなくなりました～。特にiラインとOラインは自分では全く見えなくて今まで自己処理をした事がなかったんだけど、綺麗に脱毛できたおかげで、全く気にしなくてよくなったのが本当に嬉しかったです♪これで安心して海で水着になる事ができます(*^^*)ありがとうございました!</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="仙台店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">仙台店は、仙台駅からアーケードを入ってすぐのところにあり、ショッピングの間に行くこともできる場所だったので、とても通いやすかったです。脱毛して一番よかったのは両ヒジ下です。長袖を着ている冬場でも、仕事中に腕まくりをするのが癖なので、腕の毛はいつも気になっていました。毛の量がそれほど多くはないのですが、剃り忘れたりするとプツプツと毛が生えてくるのが気になるし、自分でも人からもすぐに目に付く場所なので、脱毛して本当によかったと思います！今ではつるすべな腕を見るのが楽しみでもあります。保湿のためのローションはとても潤うので銀座カラーにしてよかったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="池袋店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラー池袋店は池袋駅東口から歩いて3分くらいの場所にあって、1Fがdocomoショップのビルの7階でとてもわかりやすかったです。無料カウンセリングで行ったのが平日の昼間12時過ぎにもかかわらず、待合室はすでにお客さんで埋まっていて人気の高さが伺えました。こんなに人気だと予約が取りづらいのではと心配になりましたが、ベットが17台もあり、池袋にはもう1店舗あるとのことで、予約が取りづらいと感じたことはありません。施術後の美肌潤美というミストとローションで、乾燥がちだった肌がぷりっぷりに生まれ変わってきました。スタッフの皆さんも、いつも笑顔で暖かい対応をしてくださるので、脱毛に行くというよりリラクゼーションサロンに行くという感じで大満足です！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="池袋店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">美容系サロンが立ち並ぶ池袋でどこの脱毛サロンに行こうか悩んでいましたが、本社と同じビルにある銀座カラー池袋店なら、安心してサービスが受けられるのではないかと思って銀座カラーにしました。スタッフの方も美意識が高くて、脱毛の話だけでなく、美容や恋愛のアドバイスもいただけました。また、別途、シェービング代をとられるようなサロンもあるそうですが、銀座カラーはシェーバーが持ち込みでき、別途料金をとられるようなこともなかったです。現在、大学4年生で、就職先が転勤のある会社なので、今後通えるかどうかの心配もありましたが、配属先の周辺のお店でも施術を受けられると聞いて、安心して通っています。全身の毛がないつるつるボディを目指してがんばります！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="池袋店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">池袋東口から徒歩3分と駅近で、店内が白とベージュで明るく、とても落ち着いた雰囲気の池袋店。通ったきっかけは、もうすぐ夏で水着を着るからです。以前から気になっていた脱毛についてネットで色々調べてみたり、口コミサイトなどを見て、銀座カラーが良さそうだったので無料カウンセリングに行きました。カウンセリングでは脱毛に関する不安に対して丁寧に答えてくださり、水着を着るならVIOセットがお勧めですと言ってもらったので、思い切ってVIOセットで通ってみることにしました。丁寧なカウンセリングや店内の清潔感のある雰囲気が通う決め手となりました。実際に通ってみると、多少の痛みは感じたものの、今までの家でのムダ毛処理数年間の手間を考えたらとっても楽で、思い切って通って本当に良かったと思っています。これで水着を着るのも怖くないです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="池袋店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学の友達から「銀座カラーは安くてお肌がつるつるになっていいよ！」と勧められたので無料カウンセリングに行きました。ワキの脱毛は、別の有名チェーン店でしたことがあったんですが、そのサロンはビルの一室で、暗い雰囲気の場所にあって、通うことが恥ずかしいような感じだったんです。でも銀座カラー池袋店は駅から徒歩3分で、お店に入ると窓からの光が差し込む綺麗で開放的なお店で雰囲気もとてもよかったんです。脱毛が恥ずかしいという気持ちじゃなくて、綺麗なサロンに何度も通いたい！という気持ちになりました。スタッフさんも笑顔で明るい方ばかりで、とても居心地がよかったです♪脱毛サロンは数多くあるので、お店の雰囲気や通いやすさは重要だなと思いました。友達の言った通り、脱毛後はお肌つるつるで言うことなしです！別の友達にもおススメします(*^^*)</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="千葉船橋店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">幼い頃から海が近い環境だったので日焼けを気にせずに遊んでいたのですが、就職活動を考えて、日焼けをやめて美白に戻してきました。そうしたら、日焼けしている時は気にならなかったムダ毛が気になりだして、接客業が希望なので社会人になる前に脱毛をしておきたいなと思ったんです。就活とバイトで忙しいので、脱毛するなら予約もとりやすくて、家の近所の船橋駅の近くにある銀座カラーと決めていました！脱毛し放題のコースは期限がなく使えて、何度でも通えて、月額で安く全身脱毛することができるので、大学生の私でもバイト代の中から負担なく始められました(^^)また施術中に、スタッフの方に就活の相談にのって頂いたり、社会人としての身だしなみだけでなく、心構えもアドバイス頂いたりして、とても勉強になったのも嬉しかったです！ありがとうございました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="千葉船橋店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">都内にある会社の近くで脱毛を検討しましたが、そのサロンは予約が取りづらかったので、休日でも通いやすいように自宅の近くで脱毛できるサロンを探していました。千葉船橋店は、船橋駅からとても近く自宅からも近かったので、パウダールームでメイク直しをするのが面倒な日でも、すっぴんで帰っても恥ずかしくないなというのが、気持ち的に楽で良かったです。最初は顔とワキと膝下の、人からもよく見えるところのみ施術しようと考えていましたが、対応してくださるスタッフさんの肌がとても綺麗で、産毛がなく輝いた肌をしていたのを見て、自分もそんな肌を手に入れたいなと全身脱毛にチャレンジしています。また、同年代のスタッフさんも多いので、施術中に恋愛相談などもしていて、施術の時間が楽しくてあっという間にすぎます。楽しくて綺麗になれるので、通うのが毎回楽しみで仕方ありません。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="千葉船橋店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛サロンは山ほどあるので、どこのサロンに通うかいろいろとクチコミを参考にしました。銀座カラーは「肌の弱い方には向いているサロンだと思う」というのがあり、乾燥肌でカミソリ負けしてしまうこともある私には向いているかな？と思い、まずは無料カウンセリングに行ってみました。スタッフさんがとても丁寧に説明してくれて、美肌潤美という保湿ケアのことを教えてくれました。つけてもらった後はしっとりとして、時間が経つともちもちの肌になっていて、肌質がとてもよくなるケアでした。やっぱり乾燥肌の私にはここがいいかも！と思い契約しました。毛は順調に少なくなっていって、肌はどんどんすべすべになりました。今では乾燥肌で悩むこともなくなったので大満足です！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="千葉船橋店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">千葉船橋店はルネライラタワー船橋の中にあって船橋駅からすぐ、とアクセス抜群だったので、ここに決めました。施術の後に1階にあるカフェでお茶をして帰るのですが、それも楽しみの1つでした。私が無料カウンセリングに行ったのは、春になると肌の露出が増えるので、その前に足のムダ毛をなんとかしたいと思ったからです。足全セットにはひざ上、ひざ、ひざ下だけでなく、足の甲や指まで入っていることに驚きました。確かに足の指にもうぶ毛が生えていて、気になっていたんです。そんな細かいところまで施術してくれるんだと嬉しくなったのを覚えています。しかも6回で終わるし、月額制の支払いでいいというのも魅力的でした。この金額はとても安いし通いやすかったです。こんなに安くて便利ならもっと早く通えば良かったと思うほどでした。脱毛したおかげで足に自信をもつことができました。友達にもぜひ紹介したいと思いました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜エスト店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 主婦<br></span>
            </dt>
            <dd itemprop="reviewBody">人より毛深いので自己処理をしない日はありませんでしたが、子供ができてからお風呂に入るのもバタバタで時間が取れなくなってしまいました。前から脱毛サロンには興味はあったのですが、なんとなくタイミングを逃してそのままに。早く始めていれば良かったと後悔しています。このままだと通う機会がなくなってしまうので、今回は旦那に相談してみたところ育児で疲れているだろうからと、ご褒美でサロンに通うOKが出ました!!嬉しすぎてすぐカウンセリングに行き、契約しちゃいました。これから通うのが楽しみでワクワクしています!
</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜エスト店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">横浜エスト店を利用していますが、いつも笑顔で丁寧に対応してくれるので安心して脱毛を受けられています。脱毛の効果も実感出来ているので、通うのがすごく楽しいです!!施術が終わった後はお茶のサービスがあるし、パウダールームもちゃんとあるので、細かいところまで気遣いを感じます。ムダ毛に関しては、昔からすごくコンプレックスがあったので、サロンで肌を見せるのも不安でしたが、今は通って良かったと心から思っています。今度は、横浜西口店にも行ってみます!!</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜エスト店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">私が通っている横浜エスト店は、友達に紹介してもらったのがきっかけで通うことになりました。学校のお昼休みって、だいたいダイエットとか美容の話になるんですけど、脱毛についてもけっこう話題にでるんですよね。横浜駅の周りには、いろいろ脱毛サロンがあるし、実際通う時は友達の意見を参考にしようと思って、その話を聞いていました。友達の中で、ダントツで人気だったのが銀座カラー!!料金もお手頃なのに、効果がしっかり出て良いっておすすめされましたよ。私も今通っていますが、友達の話通りのサロンで気に入っています。肌がツルツルになることを期待して通い続けます。
</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="横浜エスト店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">社会人になったら、前から通いたいと思っていた脱毛サロンに行こうと決めていました。周りの友人たちは、学生時代から通っていましたが私は勇気がでずそのままに…。でも就職先がアパレルでオシャレな人ばかりだから、美意識を高くしたいと思い勇気をだしてカウンセリングへ行ったんです。緊張している私に、担当してくれたスタッフの方が優しく説明してくれたので、緊張もほぐれ自然と悩みも相談できました。実際に通ってからも、毎回丁寧に施術をしてくれるので安心して通えています。カウンセリングの時のスタッフの方も、覚えていてくれて話しかけてくれるので嬉しくなります。横浜エスト店のみなさん、これからもよろしくお願いします。
</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="宇都宮店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">東武宇都宮駅から徒歩5分のところにあり、とても便利な銀座カラー宇都宮店。おしゃれでリラックスできる空間です。また、全国転勤がある会社に就職が決まっているので、他店舗間の移動が可能なところも銀座カラーにした決め手でした。全身VIP脱毛にしたのですが、全身1回の施術を数回にわけることなく3時間ほどで施術してくれるので、本当に助かっています。回数を重ねるごとに赤く目立っていた毛穴がどんどん目立たなくなり、美肌潤美という脱毛後のミストやローションでつるつる美肌に生まれ変わって、美容エステ後のお肌のようになっています。これからも全身つるつるを目指して通いたいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="宇都宮店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">一度に全身脱毛が約3時間でできることと、他の脱毛サロンにはない、Tゾーンを含めてお顔全体まで脱毛ができるということがとても気に入っています。銀座カラーオリジナルの保湿ケア「美肌潤美」も肌質がよくなったと実感できています。また、他の店舗への移動が可能なことも便利で嬉しいです。カウンセリングの際に、「ビキニラインなどの自己処理や痛みについて」、「なぜ脱毛するときに期間を空けなければいけないのか・・・」など、気になる内容についてもとても分かりやすく、丁寧に説明してくれたので安心してサロンに通うことができました。東武宇都宮駅から徒歩5分という便利な場所にあるので、便利で通いやすかったです。店内もお洒落で清潔感あふれる感じがとても心地よく、スタッフの方もいつも笑顔でとても話しやすかったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="心斎橋店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">中学生くらいからムダ毛のことはとても気になっていて、ワキだけは高校生の時に脱毛したんですが、全身だと金額が高いのでしていませんでした。大学生になってバイトをするようになったので、これを機に全身脱毛をしてツルツルで綺麗な素肌を目指したいと思って、銀座カラーの無料カウンセリングへ行きました。やっぱり全身脱毛は高いと言えば高いんですが、銀座カラーを選んだ理由は、他のサロンの全身脱毛より価格が安かったのと、約3ヶ月に1回、しかもその1回で全身ができるので通いやすいというところや、予約のとりやすさなどです。心斎橋店が御堂筋線の心斎橋駅から徒歩１分という立地の良さも決め手の1つでした。やっぱり駅近って助かりますよね。無料カウンセリングをしてくれたお姉さんが、ムダ毛の悩みについて親身に相談に乗ってくれたのも、安心できてよかったです(^^)6回なので約1年半でムダ毛がほとんどなくなって、今はムダ毛処理もほぼ必要ありません！今までのムダ毛処理の面倒さを思い出すと、早く通えば良かったと思うくらいです。これで、彼とのデートでもムダ毛を気にしなくていいので楽で嬉しいです｡</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="心斎橋店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">女子大生になったのをきっかけに、素肌をキレイにしたい！と思って顔全体の産毛を脱毛することにしました。高校生の時は、フェイスラインや鼻下は自己処理をしていたのですが、だんだん濃くなってきていたのも気になっていたので、顔全体コースのある銀座カラーを選んだんです。心斎橋店は駅から徒歩1分ととても通いやすくて、お店はとてもキレイで清潔感があって、少し大人な気分が味わえて嬉しかったです(^_^)スタッフさんがとても丁寧でわかりやすく説明してくれて、施術中には大学生活の話も聞いてくれたので、楽しい時間を過ごせました。施術はとても気持ちがよく、フェイシャルエステに来ているみたいでした｡想像していたより痛みもなくて、途中で寝てしまったらあっという間に終わっていたこともありました。お肌がつるつるになったので、もっと早くやっていればよかったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="銀座本店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">夏になると腕やひざ上なども出す機会があり、シェーバーで自己処理をしていました。以前、違うサロンでヒジ下とひざ下を脱毛したのですが、それ以外の箇所の自己処理に時間がかかり、一部のみの脱毛では意味がないなと思い、全身脱毛をすることにしました。全身の施術を数回でわけずに1回で行ってくれる銀座カラーはとても魅力的で、無料カウンセリングでお話を聞いて銀座本店に通うことに決めました。腕やひざ上などの出ているところがキレイになればと思っていましたが、意外と効果があるのが背中でした。産毛がなくなったことにより色白になり、肌が一段と明るくなっていきました。銀座本店という名前の通り、サロンではゴージャスな雰囲気も味わえて優雅な気持ちで過ごせてうれしかったです。最初から全身脱毛を銀座カラーでやっていればよかったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="銀座本店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">接客業でお客様と近くで接することが多く、意外と手の指や甲をみられていることに気づき、始めはそこをキレイにしたくて銀座カラーに無料カウンセリングに行きました。対応してくれたスタッフの方がとても親切で、顔も腕も産毛のないすべすべの肌をしていて、とても美しく輝いていました。産毛がないだけでこんなに肌が輝くことに感動し、その場で全身脱毛に切り替えました。施術の最後に美肌潤美という美容成分がたくさん入った高圧噴射のミストをあててケアしてくれます。そのおかげで、お肌がとてもしっとりし、肌の輝きも変わってきました。私が通った銀座本店は、銀座駅から直結しているので雨の日や寒い日でも通いやすく、とても便利で良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="銀座本店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">社会人になり年上の彼氏ができたのをきっかけに、もっと綺麗になりたいと思い脱毛を考えました。今まではワキのみ脱毛をしたことがありましたが、それ以外の部分は自己処理を続けていました。好きな人に見られることを考えると、全身を脱毛している方が自分で見えない背中やヒップなどの部分も楽ができると思ったので、どうせやるなら全身脱毛をやりたいと興味を持ちました。いくつかの脱毛サロンで無料カウンセリングを受けてから銀座カラーを選んだのですが、銀座カラーにした理由は、全身脱毛が1度の施術でできて3ヶ月に1度通えばいいこと、お値段も全身脱毛にしてはお安かったことです。脱毛サロンは予約が取りにくいと聞いていたのですが、銀座カラーは予約も取りやすかったので、そこもとても良かったです。全身脱毛をして自分の体に自信が持てるようになったので、通って本当に良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="銀座本店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラーの脱毛は、脱毛した後のお肌がすべすべになると友達からすすめられたので行ってみました。脱毛とセットで美肌潤美という保湿ケアがついているとのことで、施術後のお肌が本当にぷるぷると潤ってびっくりしました。毛を無くしていきながら、お肌がどんどん美しくなっていって一石二鳥のサロンだと思います。もともと乾燥が気になる肌質だったのですが、銀座カラーで脱毛してからは、肌の乾燥も気にならなくなりました。スタッフの方の施術が丁寧なのも安心できて良かったです。細かいことですが、手の甲や指の毛などもキレイに無くなったので、ネイルサロンに行ったときなども自信をもって手を出せるようになりました。脱毛してお肌がキレイになったおかげで、いろいろな場面で女性として自信がもてるようになりました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名古屋栄店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">ずっと脱毛に興味がありましたが、仕事がら転勤があり、また残業もあるので、期間もかかる脱毛に通うことを諦めていました。でも、名古屋の女性の美しさに触発され、より女性として自信がもてるようになりたいと思い、全身脱毛を考えました。銀座カラーは全国どこの店舗でも施術を行うことができ、支払い後は期間を気にせず通えると聞き、全身脱毛をお願いすることにしました。名古屋栄店は平日は夜9時まで営業していて、当日予約もOKなので仕事が早く終わりそうな時に施術をお願いすることもたまにあります。お風呂タイムにおこなっていたムダ毛処理の時間から解放され、その時間でエクササイズを始めました。銀座カラーのおかげで名古屋ライフをより楽しめそうです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名古屋栄店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">結婚が決まり、結婚式でウエディングドレスを着た時の襟足や背中の産毛を処理してくれるところを探していたら、友達に銀座カラーを勧められ無料カウンセリングに行きました。脱毛というと期間が長くかかる、予約が取りづらいといったイメージがありましたが、銀座カラーでは予約もとりやすく、引越しや転居があっても他の店舗で対応してもらえて、肌のケアもしっかりしてくれるので一生のお肌のケアパートナーとして全身脱毛を申し込みました。実際、施術を受けてからお肌がぷるぷるになっていき、ブライダルエステも受ける必要がなくなりました。自信を持ってウエディングドレスが着られそうです。これからも一生のおつきあいをしていきたいと思っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名古屋栄店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">春に向けて腕と足のムダ毛を脱毛したいなと思い、銀座カラーへ無料カウンセリングを受けに行きました。最初に腕と足を綺麗にしたいと伝えたところ、それなら全身VIP脱毛の方がお得ですよと教えてくださり、全身脱毛のコースについても丁寧に説明をしてくださいました。確かに夏には水着になるしVIOも気になるので、全身VIP脱毛のコースに通う方がいいなと思い、全身VIP脱毛コースに決めました。1度の施術で全身の脱毛ができて、3ヶ月に1度通えばいいというのは、忙しい私にとってとても楽でした。どんどんムダ毛が減っていくのを目の当たりにして、自己処理をしなくていいという快適さに早く通えば良かったと思いました。スタッフの方も皆さん優しくて、毎回笑顔で対応してくださるので安心して通えて良かったです。予約の取りやすさや施術、金銭的にもとても良かったので、脱毛をしたいという友人に銀座カラーを勧めたいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="名古屋栄店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">他のサロンで足の脱毛をしたのですが、追加料金が高くて驚きました。今回、銀座カラーさんで残っていた足の毛を脱毛していただいたのですが、お値段も効果もとても納得のいくものだったので、ここにしてよかったです。
初回のカウンセリングでは、前のサロンでの悩みも親身になって聞いてくださったので、安心して通うことができました。お店の雰囲気もとても綺麗で落ち着いていて居心地のよいサロンだったので、はじめからここに来ていればよかったです。子どもがまだ小さいので、お風呂で足の毛を剃っている時間もなかったのですが、今では自己処理いらずの綺麗な足になり大満足です。夏にはスカートをはいて素足を出したいと思っています。足がキレイになったので、ぜひまた違う箇所でも通いたいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="盛岡菜園店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">私が住んでいる地域では脱毛してくれるお店がなく、銀座カラーのある盛岡市まで車で1時間かかります。何回も通うようになると面倒だなと思い、脱毛することを悩んでいました。でも銀座カラーでは、他のサロンで数日かかる全身1回分を、一度で施術してくれます。全部なくなるまでには数回施術が必要ですが、それでも全身を1回でやってくれるのでとても助かっています。また、友人が、他店では全身脱毛に襟足などが入っていなかったということで追加料金が発生したと言っていたので心配でしたが、銀座カラーはそういった境目がなく全身をくまなく脱毛でき、料金も他店と比べて安かったので銀座カラーで脱毛をして本当に良かったです。ムダ毛の処理の時間もなくなり楽になったので、もっと早く始めればよかったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="盛岡菜園店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">他店で脱毛をしていたのですが、盛岡市は近県から脱毛処理に通う人が多く、予約が取りづらくて3年間の間で予約がとれたのはなんと数回！！両腕の脱毛をしているので夏場は日焼けもできずにいつも黒い手袋と日傘が必須で、盛岡の少ない夏の日を思いきり遊べずに困っていました。でも友人が銀座カラーならシステムがきちんとしているので予約がとりやすく、しかも施術するたびに美肌になると教えてくれました。料金も他店より安かったし、美肌になるならと思い切って申し込みました。今のところ順調に脱毛の予約がとれ、施術ができています。施術するごとにどんどん肌もきれいになっています。全身脱毛をしているので、水着を着た時に気になっていたムダ毛も気にしないで、今年は何年かぶりに海で思いきり遊べそうです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="盛岡菜園店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">以前、他のサロンでお試しコースを受けてみたらお試しコースは安かったのですが、他の箇所の追加料金がとても高く続けるのが難しい事がありました。そういう経験があったので銀座カラーでも金額の面が心配だったのですが、職場の同僚が通っていたので、私も無料カウンセリングを受けて通う事を決めました。フリーチョイス脱毛6回でヒジ下もひざ下もとても安かったので、金額的にとてもお得な感じがしました。6回通い終えて思ったのは、銀座カラーにして本当に良かったという事です。ヒジ下とひざ下が綺麗になったら、今度はヒジ上やひざ上も気になるようになってきたので、またフリーチョイス脱毛で通いたいなと思っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="盛岡菜園店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">年齢的に結婚式に出席することが増えてきて、デコルテや背中を出すドレスを着る機会が増えてきました。背中が綺麗な友人を見たとき、自分の産毛がとても気になってしまい、全身脱毛をすることに決めました。銀座カラーでは全身VIP脱毛というコースがあり、他のサロンに比べて価格設定もとてもお得でした。自分では見えない襟足までとても綺麗にしてもらえたので、普段でも髪をアップにした時に、友達から襟足が綺麗だとほめられるようにもなりました。結婚式でドレスを着る時も人目を気にせず楽しめるようになり、全身脱毛をして本当によかったと思います。まだ結婚の予定はありませんが、これで、自分が結婚する時に慌てて脱毛することもないので、早めにやっておいてよかったなと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="仙台店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">仕事柄髪を結ぶことが多く、鏡で襟足の形をチェックしているのですが、襟足の形が悪く、結んだ姿に自信がありませんでした。襟足だけきれいになればと思っていましたが、無料カウンセリングに行って、銀座カラーのスタッフの方の肌の綺麗さに衝撃を受けました。襟足だけでなく全身、毛がないと肌の輝きが増すことを目の当たりにしたのと、全身脱毛の中に襟足が入っていると聞き、全身脱毛をすることにしました。施術の度に美肌潤美というミストを使ってもらえるのですが、美肌効果がとても高く、回を追うごとに肌が綺麗になっていくのを実感しています。また、スタッフの皆さんが綺麗で美意識が高く、脱毛の悩みだけでなく美容の悩みにも相談にのってくれて、内面、外面、精神面で綺麗になれるアドバイスをいただけることもあり、とても嬉しいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="仙台店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">ずっと腕と足の自己処理をしてきましたが、そのたびに肌が赤くなって毛穴を傷つけてしまい、治った頃にまた自己処理をするので、毛穴がずっと赤い状態になっていました。皮膚科にいっても自己処理による刺激なのでその刺激をやめれば治ると言われましたが、ずっと続けてきたので自己処理をやめると太い毛が出てきて、男性のようなムダ毛になってしまっていました。そんな時に、銀座カラーでは脱毛しながら美肌になれると聞いて、早速カウンセリングに行きました。スタッフの方の、毛穴など見当たらない白い肌を見て、自分もこうなりたいと強く思いました。自己処理が減るごとに肌の赤みは消え、毛穴が目立たない肌になり、何年も悩んでいたムダ毛と肌の悩みから解放されました。早く銀座カラーに通えばよかったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="心斎橋店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">水泳をしており、Vゾーンの処理にいつも悩まされていました。海外セレブで流行っているVIOの部分の脱毛にとても興味がありましたが、デリケートゾーンなだけにたくさんの人に施術してもらうことに抵抗があり悩んでいました。銀座カラーでは、施術は基本1人（2人入ることもある）のスタッフが行い、丁寧に施術してくれます。デリケートな部分なので配慮がきちんとされ、ストレスなく脱毛を行えることはとても助かりました。生理のときの不快感からも解放され、お手入れの心配もなく好きな時に水泳を楽しむことができます。私が通った心斎橋店は、アップルストアの上にあるので迷うことなく通えたのもよかったです。また、予約が当日OKだったりWEBからもできるなど、とりやすく、もっと早くに始めていればよかったなと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="心斎橋店" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学生になって彼氏ができたんですが、接近するたびに彼氏のヒゲが気になって、自分もそうかもと思って自己処理を始めました。でも、シェーバーで間違って切ってしまったりにきび跡を傷つけてしまったりと、お肌の状態が悪くなって悩んでいたんです。どんどんお肌がキレイになっていく友達に、銀座カラーを紹介されて私も鼻下と顔の脱毛を始めたんですが、施術はほとんど痛みがなくて、お手入れ後はミストと保湿ローションで整えてくれたのがとても気持ちよかったです(*^^*)毛がなくなったことで、お肌がスベスベで色が白くなって、お化粧のりも良くなったので、まるでエステにきているようでした！脱毛した結果、お肌の状態もよくなって、今では毛穴も目立たない白い肌に生まれ変わりましたよ！！もっと早くに始めていればよかったです～。</dd>
          </dl>


            <div class="block-btn-stripe-lower">
              <a href="#" class="btn-stripe btn-wide btn-icon-plus">もっとみる</a>
            </div>
          </div>

        <!-- /#salon --></div>

        <div class="js-tab-body" id="course" style="display: none;">

          <div class="select js-select">
            <select name="">
              <option value="">コースを絞り込む</option>

           <option value="全身パーフェクト脱毛コース">全身パーフェクト脱毛コース</option>

           <option value="フリーチョイス脱毛コース">フリーチョイス脱毛コース</option>

           <option value="新・全身脱毛コース">新・全身脱毛コース</option>

           <option value="全身ハーフ脱毛コース">全身ハーフ脱毛コース</option>

           <option value="顔セットコース">顔セットコース</option>

           <option value="腕全セットコース">腕全セットコース</option>

           <option value="足全セットコース">足全セットコース</option>

           <option value="VIOセットコース">VIOセットコース</option>

            </select>
            <p style="padding:10px 0;text-align:center;">※一部販売が終了したコースも含まれております。</p>
          <!-- /.select --></div>

          <div class="block-voice-list no_margin js-appendmore-voice" itemscope="" itemtype="http://schema.org/BeautySalon">

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="全身パーフェクト脱毛コース">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>銀座カラーさんに通う前は、ネットで買った家庭用脱毛器を使っていました。サロンに通う手間は省けるし、好きな時間に好きな回数ができるのでいいと思ったのですが、地道に全身照射しなければならないので時間がかかったり、背中などの届かないところは照射できなかったりと、デメリットを感じることがありました。このまま家庭用脱毛器を使っていくことも考えたのですが、ボーナスが出たので思い切ってサロンでの全身脱毛（顔脱毛付き）に切り替えたのです。サロンではエステティシャンの方がやってくれるので照射できない箇所はないですし、時間も一時間ちょっとで終わるので今までの悩みは消えました。しかも照射後のケアをしっかりしてくれるので、肌がしっとりして乾燥しなくなりました。やっぱり自己処理では限界があると思うので、脱毛をしたくて悩んでいる人はプロに任せることをおすすめしたいです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="全身パーフェクト脱毛コース">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>つい最近結婚した姉から「独身時代に全身脱毛しておくといいよ。」とアドバイスをもらったので、今のうちに終わらせてしまおうと思い、脱毛について調べていました。脱毛するなら頭の先から足先まで全部やってしまいたかったのですが、意外と全身脱毛に顔が含まれているプランを扱っているサロンって多くないですね。その点銀座カラーは顔を含む全身脱毛のプランがあったので、非常に助かりました。今回脱毛箇所が多いので予約がちゃんと取れるのか心配だったのですが、早めの予約を心がければ予約は取れますし、職場近くの店舗と自宅近くの店舗の両方で予約が取れるので、都合のいい方に合わせればより簡単でした。予定通り予約を埋めていって、脱毛完了後は婚活でもしようと思います！</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="全身パーフェクト脱毛コース">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>小学生の時に腕毛の濃さを友達に指摘されて以来、体毛の濃さをコンプレックスに感じていました。自分で処理できるようになる前は夏でも長袖長ズボンを着て隠したり、性格まで暗くなっていた気がします。いまは自己処理をしているのですが、時間がかかって煩わしいですし、なにより剃り残しがないか毎回気が気じゃありません。そんな私を心配して、姉が全身脱毛を勧めてくれました。行ってみたいとは思っていたのですが、最後の一歩を踏み出せずにいたのを感じ取ってくれたのかもしれません。実際に銀座カラーさんへ行って施術をしてもらったのですが、顔を含む全身脱毛なのに一時間弱で照射は終わりましたし、痛みも強くなく驚きました。これからは長年のコンプレックスとしっかり向き合い、肌も心も明るくなりたいと思います。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="全身パーフェクト脱毛コース">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>自分の特技は水泳なのですが、水着になるとやっぱりムダ毛が気になっていました。自分で剃れる脇や足腕はケアできていたのですが、二の腕の後ろや背中などは難しくて放っておくことが多かったです。自分で見えないので、まあいいかと思って。でも、これからも水泳を続けていくつもりなので、思い切って全身脱毛をしようと決意しました！ただエステって勧誘が激しくて、高価なケア用品とかを買わされるイメージがあったのですが、水泳仲間の口コミで銀座カラーさんはそんなことがないと聞き、絶対ここに通おうと決めたのです。実際、勧誘はほとんどなく、断る苦痛も感じなくてほっとしています。スタッフの方は優しいし、脱毛効果も感じられて、より水泳を楽しむことができそうです。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="顔セットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>噂には聞いていたのですが、産後にヒゲが生えました（・・；）ヒゲだけでなく、頬にも太めな毛が生えて。ホルモンバランスの崩れから起きるらしいのですが、数年たっても変わらなかったので、顔脱毛を受けることにしました。顔脱毛を受ける前は自己処理をしていたので、カミソリを子供が触ったりして危ないこともあったのですが、現在はカミソリを処分できました！銀座カラーは友達が通っていたので安心できましたし、紹介キャンペーンがあったのでかなり助かりました。実際スタッフの方も同年代の方が多く、育児の息抜きに通わせてもらってます。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="顔セットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>ウエディングフォトの準備のため、銀座カラーさんに顔脱毛をしにきています。昔は自分で処理していたのですが、不器用なのか何度も顔に切り傷を作ってしまった経験があるので、大事なこの時期のためにも、自己処理をやめて銀座カラーへ通わせてもらっています。撮影の時に顔に傷があったらショックですし。実際、顔の産毛がないだけでファンデーションは綺麗にのりますし、パウダーをはたいても粉っぽさがなく、かなりツヤ感が出ると感じています。脱毛をはじめてからというもの、毛が減ったので毛穴も小さくなった気がしますし、脱毛後の保湿ケアで肌の乾燥感もなくなりました。主人もかなり喜んでくれているので、これを機に別の箇所もおねだりしてみようと思っています。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="顔セットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>アップヘアが好きで、毎日凝ったヘアアレンジをしているのですが、いつもおでこの産毛が気になっていました。どこまで処理したら綺麗に見えるのかわからないし、剃りすぎて変になっても嫌なのでほったらかしだったのですが、みんな私のおでこを見ているような気がして、急に恥ずかしくなってしまいました。だからと言ってヘアアレンジをやめたくはないので、脱毛を調べてみたら銀座カラーではおでこもできたので通うことにしました。店内は綺麗だし、スタッフさんは優しいし、顔脱毛の効果もばっちりでした。脱毛後のミストシャワーもあったりして、ちょっと贅沢している気分です。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="顔セットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody"><p>百貨店のコスメカウンターで美容部員をしている仕事柄、美容の意識は常に高くいたいと思っています。もちろん脱毛は必須で、顔の産毛はカミソリで自己処理をしていたのですが、肌の負担が心配でした。メイクが映えるようにと思ってやったものの、自己処理で赤みやヒリつきが出たこともあります。肌荒れで仕事に影響が出る前にプロにお任せしようと思い、足と腕で通っていた銀座カラーさんで顔脱毛をお願いしました。私が通っている店舗は平日22：30まで受け付けてくれるので、仕事終わりの疲れた肌に保湿ミストのケアで、一日の疲れもリセットされる気がします。</p></dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新・全身脱毛コース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">何よりも痛い事が大嫌いなので「全身脱毛をしたい！」と決めた時、インターネットで痛みの少ないサロンを探しました。
全身完璧に脱毛したかったので、顔も含めた24カ所スペシャルコースで脱毛していただきました。
自宅から近い船橋北口店に通っていたのですが、予約が取りづらいこともなく、1回1時間程度でしかも1ヵ月ごとに通えました。
お部屋もベッドもとても清潔だし、個室なのでデリケートな部分をお願いする場合でもプライバシーが守られているので安心してお願い出来ました。
全身脱毛するのだからどれだけ時間がかかるだろう...と思っていたのですが、あまり苦になりませんでした。
肝心の痛さについては始めは感じました。でも耐えられる程小さな痛みだったので、すぐに慣れちゃいました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新・全身脱毛コース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">小さい頃から人より毛が濃い体質でした。
レーザーの脱毛は剛毛だと反応が強くて痛みが多いと聞いたことがあり、でも光脱毛は効果がでるのか、と選択に悩んでいました。
サロンをいくつか調べていたところで銀座カラーのIPL脱毛の説明を読みました。口コミなども参考にし、一度お話を聞いてみたくて無料カウンセリングを予約しました。
カウンセリングではアンケート用紙に自分で記入した内容を見ながらスタッフの方とお話をし、脱毛の効果や方法について説明いただきました。
スタッフの方がとても丁寧で好感が持てたので、施術をお願いしました。
施術中、痛みがでそうな箇所の脱毛を行う時に声をかけてくださったり、少しだけ休憩をはさんでくださったり、と私を気遣いながら施術をしていただき、信頼してお任せできました。
全身ツルツルになるにはまだ時間はかかりますが、少しずつ効果を感じています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新・全身脱毛コース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">小学生の時から自分が毛深いということはわかっていたのですが、中学高校と友達の肌を見る度にどんどんコンプレックスに。大学に入り少しずつ脱毛しようと思い、全身一気に始めた方が通う期間もお金もおトクだったので、銀座カラーの全身脱毛コースに申し込みました！脱毛に関する知識はまったくなかったけど、カウンセリング時にスタッフさんから自己処理を続けることによる肌のダメージについてや、銀座カラーの技術の良さについて説明してもらい、信頼して始められました。毛が太いと過敏に反応して痛いと聞いたことがありましたが、思ったよりも痛みが少なくすぐに慣れました。ずーーーっと、剛毛と付き合ってきたので、ガラッと変わった全身がスッキリしすぎて嬉しくなりました。新しい自分を感じながら、やる気がすごくみなぎるようになりました☆</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="新・全身脱毛コース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛サロンは勝手な先入観で「高い」イメージがありました。なので自分で海外製の機械やシートなどで脱毛をいろいろ試しました。自分でやるにはやはり、限界があって色々な悩みが出てきました。失敗すると、赤くなったり、切れてしまったり・・・・きつかったです。カラダのことなのでお金がかかってもやはりプロにしてもらうべきだと一大決心。箇所を選択してお願いしようと思ってましたが、想定していたより全然金額も安いし、月額制での支払いもできたので、だったら全身してしまえ！と思って全身脱毛コースに申し込みました。期間はどのくらいかかるのか、とかたくさん不安な要素がありましたが、現状のお肌の状態を見てもらったりして、どのくらい時間がかかりそうかなどなど色々アドバイスをしていただけて安心してお任せすることが出来ました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="フリーチョイス脱毛コース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛サロンは、コースが決められているものだと思い込んでしまっていました。パソコンの広告でたまたま見かけて好きな部分を選べて脱毛ができることを知りました。土日はいつも遊びたい派なので、平日何とか会社帰りにお願いできるものかなと思ってましたが、定時にあがっても十分間に合う時間に予約が取れますし、なにより会社の近くにあるのがとても助かります。へそ周り、両ワキ、両ひざをお願いしました。少しだけ肌が弱めなので施術後、数日経った時に赤くなったり荒れたりしないかとても心配でしたが、アフターケアも万全で繊細にお肌のことに気をつかってくれているんだなぁと実感しました。おうちの近くに店舗があるので気になったことは直接聞きにも行けますし、通いやすいです。必要に応じたそれぞれの対応をしてくれてそうなので信頼性も◎だと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="フリーチョイス脱毛コース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">学生の時に10カ所くらいセットで脱毛をしましたが、20代後半から体質が変わったのかやけに毛が濃くなってきたのでフリーチョイスコースでお腹だけ脱毛しました。
1カ所毎の価格が安いところを調べたのですが、結局は家の近くで評判が良さそうなサロンが一番だと思い、銀座カラーさんにしました。
いつも平日の昼間に予約をしていたのですが、毎回すぐに予約がとれました。以前通っていたところは全くとれなくて半年くらい予約できない時もあったので、銀座カラーは快適でした。
毛が濃いと言っても腕や足ほど濃いわけではない為、効果を感じるには時間はかかりましたが気にならない程に脱毛ができました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="全身ハーフ脱毛コース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">私はあまり毛深くはないので、気になっている両ワキと両ひざ下、両ヒジ下の3カ所でお願いしようとしたのですが、無料カウンセリングで全身ハーフ脱毛を教えていただき、そこまで価格が変わらなかったのでハーフにしました。
全部で8カ所の脱毛ができたので、結果的にはお得に脱毛できちゃいました。
はじめの1,2回は箇所によっては「本当に毛が少なくなっているのかな？」なんて思っていましたが、4回を終わる頃には効果が目に見えるようになってきて、確実に少なくなっているな、と実感できました。よく他サロンの口コミなどをみていると「勧誘がしつこい」「予約がとれない」などありますが、銀座カラーは全く問題なく予約もスムーズにとれました。
こんなにキレイになるなら、今度は見えないボディ部分にもトライしてみたいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="全身ハーフ脱毛コース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">ワキ、ヒジ下、ひざ下、Vライン、と脱毛したい箇所は決まっていたので、セットでできるところを探しました。
3つほど候補サロンがあったのですが、口コミや金額などで銀座カラーさんに決めました。
私は銀座本店を利用していたのですが、やや上品な雰囲気の店内だったので、はじめは緊張しました。
肌が若干弱いので、ジェルでかぶれたりしないかと心配していたのですが、事前にパッチテストをしてもらい問題なかったので安心して脱毛できました。
夏の時期は予約が取りにくいこともあるようですが、年間通じて問題なく予約でき、仕上がりにもとても満足しています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="全身ハーフ脱毛コース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">気になる箇所がたくさんありつつも、全身脱毛にするにはちょっと勇気がなくて、箇所が選択できるハーフ脱毛で施術することにしました。
1・2カ所だけ脱毛をしても絶対に他の箇所もやりたくなるよ、という友達の意見を聞いたのでやってみたのですが、施術するたびに全身にカミソリを使用する回数がどんどん減っていき、ハーフで良かったと実感！
クリニックの脱毛だと痛みが凄くあると聞いていたので、デリケート部分も希望していた私には、痛みは絶対に耐えられない！と思って銀座カラーにお願いしました。あたたかい光をあてられているようで、ほとんど痛みがなくむしろ気持ちが良いです（笑）
肌も元よりも明るくなった気がするし、つるつるの綺麗な肌になって、彼にもキレイになったね！って喜んでもらえました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="全身ハーフ脱毛コース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">夏になると腕や足を出すのでほぼ毎日カミソリで自己処理をしていました。でも剃る回数を重ねるごとに自分の毛が丈夫で濃くなっていき毛穴もより目立ち始めました。よく露出する部分だけでも自己処理しなくてもいいようにしたい！と思い銀座カラーさんにお願いしました。この全身ハーフ脱毛コースだと好きな箇所を8点も選べるので、露出することの多い箇所もまるっと脱毛できてすごくお得だなと思いました。1回目の施術をしてから1～2週間経ちまして、毛がするすると抜け落ちていったので早速効果を体感しています。カミソリで自己処理をしていたときは2日後にはもう毛が伸びてきていたのですが、脱毛をしてからだと次の毛はまだ生えてきてないのも嬉しいです。脱毛回数を重ねると毛が薄くなっていくのはもちろん、毛穴も目立たなくなるそうなので最後の仕上がりがとても楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="腕全セットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">薄着の季節には露出が多くなるので、腕の毛は自己処理していましたが、友人のツルツルの腕を見て「私もツルツルにしたい！」と思い、脱毛することにしました。
「脱毛は痛い」というイメージを持っていましたが、パチッと一瞬痛みを感じる程度で施術のストレスはありませんでした。施術時間も短かったので、お出かけの前や美容院の日と合わせて「セルフケアの日」を作りサロンへ通っていました。
私の場合そこまで毛深くなかったのではじめはあまり効果を感じませんでしたが、回数を3,4回重ねてくると薄くなっていくムダ毛を見てウキウキしました。
エステティシャンの方々も知識が豊富なので、毎回施術中のトークが楽しくて銀座カラーさんに通って良かったなぁと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="腕全セットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">出産前に脱毛サロンで気になる箇所は脱毛をしていたのですが、出産後、なぜか腕の毛が濃くなってきました。
以前通っていた脱毛サロンがダメなのかホルモンバランスの関係なのかはわかりませんが、半袖から出ている二の腕を見る度に気になって仕方なかったです。
インターネットで腕の脱毛コースを調べていて銀座カラーの腕全セットを見つけました。手の甲や指は脱毛していなかったので、この際だから腕全てやってしまおう！と思いお願いしました。私としては脱毛も良かったのですが、脱毛前後の保湿ケアが気に入りました。
子供がいるのでエステでのんびりなんて時間もなく、短時間で脱毛とお肌ケアをしてくれる銀座カラーは子育ての休息も兼ねられて私にピッタリでした！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="腕全セットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">趣味で料理教室へ通っているのですが、長袖を着ていても腕をまくって料理をするので段々と気になるようになり、脱毛を考えるようになりました。教室に通っている方は、みんなキレイな腕をしているんですよね。
毛が太くなるからという理由でカミソリなどで剃る事もしなかったのですが、旦那に「さすがに処理した方がいいんじゃない？」と言われたのがショックで脱毛サロンに通うと決意。
通ってみると予約も取りやすくサロンのお姉さんの対応も素晴らしかったです。料理教室でも話のネタが増えて教室に通っている仲間達ともコミュニケーションが増えたりと良い事づくしです。サロンへ行く前日は「明日行けばまた毛が薄くなる！」と行くのが楽しみになっています。
旦那にもキレイになったと言ってもらえたので、他の箇所もやろうか検討中です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="腕全セットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">夏は腕を露出する回数が増えるので自己処理が毎回面倒だなあと思い、脱毛を考え始めました。初めての脱毛なのでいろんなサロンを調べてみて、銀座カラーには腕全セットがあることを知りました。でも初脱毛でいきなりのセットコースというのも不安だなあと思って、最初のカウンセリングでスタッフさんに相談してみました。脱毛初心者でも気軽に始められるそうで、なんか良さそうと思いこのコースを選びました。また夏から脱毛を始めたので日焼けで施術が受けられなくなるのではないかと不安でしたが、いつも通り日焼け止めクリームを塗ってお風呂上がりには保湿をしていれば問題ないようでした。自己処理だと腕の後ろ側に剃り残しがよくあったのですが、脱毛だと腕360度しっかり施術してもらえるので剃り残しの心配もなくなり、気持ちよくノースリーブが着れるようになりました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="VIOセットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">ショーツをはくときにいつも気になっていたムダ毛。でも病気でもないのに他人にデリケートゾーンを見せるなんて恥ずかしくてできない！と、興味はあるものの決心できずにいました。
去年の夏、友達とプールに行った時に聞いてみたところ、半数以上がVIO脱毛経験者。「はじめは恥ずかしいけど1回行くと慣れちゃうよ」と言われ、遂にVIO脱毛を決心しました。
脱毛自体初めての私はカウンセリングの時緊張しすぎてうまく話せなかったのですが、カウンセラーの方がとても感じよく質問に答えてくだいました。脱毛が始まったとき、やっぱり少し恥ずかしかったのですが、サロンの方はささっと手早く処理していて、あっという間に終わってしまいました。
今現在、5回目が終わったところですが、既に満足いくレベルまで毛が少なくなっています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="VIOセットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">以前友人に誘われ、他サロンでワキ脱毛はやった事がありました。
学生の時に水着からムダ毛がはみ出してしまった事もあったり、デリケートゾーンは見えにくい場所なのでセルフケアが大変だったり、カミソリ処理をするのでザラザラ・チクチクしてしまったり。正直なところ悩みしかなかったので、社会人になってVIO脱毛にチャレンジ！
結果、やって良かったです。憂鬱なセルフケアから解放されたし、プールや温泉などでも気にせず堂々としていられます。
ムダ毛のことなど全く気にしないで楽しめているので、もっと早くやっていればはみ出さなかったのにな、と少し後悔かな。
ムダ毛の濃さで言うと場所的に毛が濃い部分なので少し痛みはありましたが、仕上がりもキレイだし絶対に脱毛サロンでの施術をオススメします。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="VIOセットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">女子会をした時に脱毛の話になり、VIOをやってるかどうかの話で盛り上がりました。意外とやっている友達が多く、私も水着になる夏前にやっておこうと決意して通う事にしました。
VIOは水着や下着から出る毛が気になっていたので考えていたのですが、それ意外で生理の時に衛生的に良いとか凄く楽になるという話も出ていたので、キレイになるのが楽しみでした。
施術前に毛を全部剃ってくる事にちょっと抵抗はありましたが、1回剃ってしまうと早く毛が無くなって欲しい！と逆に思うように。施術の回数を重ねていく度に濃かった毛が薄くなり、下着選びも以前より楽しくなりました！もちろん衛生面でも清潔になり、ニオイも以前より無くなりました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="VIOセットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">心配性で、恥ずかしがりやの私なので、VIO脱毛をやるのはとても抵抗がありました。でもデリケートな箇所なので、自分で処理しづらい場所だし、毛が太いのでカミソリで剃る時や抜く時に痛みもあるし、毛穴から菌が入って赤みができてしまったり・・・と悩みが尽きなかったので、思い切ってやってみることにしました。実際にサロンに行ってみると、カウンセラーの方が分かりやすく説明してくれて、最初にあった不安は消え、安心感が出てきました。
施術当日は紙ショーツを着用して施術するので、プライバシーも守られてあり、施術してくれたお姉さんと楽しく会話をしている間に終わってしまい、特に痛みも感じずあっという間でした(^-^)　あんなに不安だったのに、今は行くのが楽しみで仕方ありません！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="足全セットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">ミニスカートを履くのが大好きなのですが、ムダ毛がいつも気になっていました。
ムダ毛が気になってくる度にお風呂でカミソリを使って剃っていたのですが、肌も荒れるし段々とブツブツも目立ってきて...。
憧れのモデルさんのようなキレイでピカピカな足になりたくて脱毛を決意しました。
銀座カラーの足全セットは他サロンに比べて価格も安いし、脱毛後のミストシャワーも気持ち良い！
段々自分の足がつるっつるになっているのが嬉しくて母にも自慢していると、「私も行こうかな？」なんて言っていました。行く度に足がキレイになっていくのが実感できて、まるでエステに通っているような気分でした。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="足全セットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">ストッキングからムダ毛が外にはみだしてしまうほど剛毛な私のすね毛。朝カミソリで処理をしても夕方にはもうチクチクしている程。本当に恥ずかしくてシェーバーや毛抜き・脱毛クリームなど色々と試しました。その結果、炎症は起きるわ陥没毛はできるわ、と酷い状態に。
私は肌の色がとても白いので黒い毛穴が本当に憂鬱でした。
友達が銀座カラーに通い始めたと聞いたので、お友達紹介を利用し足全セットでお願いすることにしました。
銀座カラーは比較的予約が取りやすいので、紹介してくれた友達と行く日を合わせて通ったりもできました。
まだ全ては完了していませんが、黒かった毛穴も段々と薄くなってきています。
あんなに恥ずかしいと思っていた自分の足ですが、今は生足でショートパンツが履けるのを楽しみにしています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="足全セットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">数ヶ月前にダイエットを開始。着々と体重を落とすことができ、ぽっこりお腹だけではなく脚ヤセにも成功！少し自信を持って人前で肌を出せるようになりました。でも、もっと完璧な足にしたい！と欲が出てきました。ネットや口コミを見ていて予約が取りやすく、足全体すべて脱毛でき、比較的安くて安心できるサロンと贅沢な条件で探していた所、銀座カラーの足全セットにたどり着きました。脱毛サロンは初めてだったのでドキドキしながら通い始めました。痛みやかゆみとかあるのかな・・・なんて不安もたくさんで行きましたがそんな要素は全くなく、あっという間に施術終了！保湿ケア付きにしたので、終わってからも霧のシャワーみたいなのがとても気持ちよくて、最初から最後まで通う前の不安なんてなくなるくらいの時間を過ごすことが出来ました。アフターケアもどうしていいかわからなかったのですが、親切に教えていただけました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="足全セットコース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">会社の同僚の大先輩が外見も中身も、デキるキャリアウーマン！という感じで、とても素敵で憧れでした。外見だけでもその先輩に近付きたくて、まずは脱毛から！と電車の中吊りを見かけて電話をかけてみました。
スタッフさんの対応がフレンドリーかつ親切だったので安心して色々な相談をさせていただきました。社会人二年目であまり貯金もないので、予算面等含んだことも相談に乗ってもらい、とりあえず足全体をお願いすることにしました。友達からよく聞いていた「脱毛サロンは予約が取りにくい」なんてことは忘れるくらいすんなり予約が取れて、施術も店員さんの質も申し分ないくらい満足しました。足全体の毛自体がなくなるだけじゃなく、質感や黒くポツポツ気になっていた（毛穴かな？）のも見違えるほどのスベスベ肌になり、なんでもっと早く行かなかったのかと思うくらいでした。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="フリーチョイス脱毛コース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">当初はクリニックに行こうか、脱毛サロンに行こうか迷っていました。ワキ脱毛だけやりたかったので、金額はさほど気にはしていませんでした。
ただ、候補にあげていたクリニックでの脱毛は痛いという口コミが多く、できるだけ痛くない脱毛を希望していたので脱毛サロンを選択しました。
銀座カラーさんは店内もキレイでくつろげる雰囲気が気に入りました。
痛みも全くないわけではないですが、気にならない程度だったので脱毛サロンにして正解でした。
実は脇の黒ずみも気になってはいたのですが、3回4回と続けていくうちに段々と黒さがなくなっていて驚きでした！
カミソリを使っていると黒ずみが進行するって本当なんだなぁ、と実感しました。
あと、元々ニオイは強くない方ですが脱毛するとニオイがキツくなるという噂もあるので心配していましたが、特にそういうことはありませんでした。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="フリーチョイス脱毛コース" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">顔の脱毛がしたくなり一番安いサロンを見つけて行ったのですが、実際は顔のパーツ毎に細かく料金設定がされていて金額がどんどん上乗せに。騙されてるような気がしてきてカウンセリングを受けただけでやめた経験がありました。今回銀座カラーにお願いしたのは、料金体系が非常に明確だったからです。それだけでも信頼が持てました。
顔のうぶ毛は細いので、毛がなくなるまで結構時間かかりそうだなぁ、と思ったのですが数回で想像以上に毛が少なくなってました。
しかもお肌はツルツル！美肌潤美の保湿ケアもつけてもらったので、潤いたっぷりで、ひりひり感も感じません。美肌潤美のミストが大好きです！
他の箇所もたまに勧められるくらいでしつこい勧誘もなく、短時間でキッチリ終わらせてくれる、あらゆる面で私には合っていました。</dd>
          </dl>


            <div class="block-btn-stripe-lower">
              <a href="#" class="btn-stripe btn-wide btn-icon-plus">もっとみる</a>
            </div>
          </div>

        <!-- /#course --></div>

        <div class="js-tab-body" id="part" style="display: none;">

          <div class="select js-select">
            <select name="">
              <option value="">箇所を絞り込む</option>

          <option value="顔(鼻下除く)">顔(鼻下除く)</option>

          <option value="鼻下">鼻下</option>

          <option value="両ワキ">両ワキ</option>

          <option value="乳輪周り">乳輪周り</option>

          <option value="お腹">お腹</option>

          <option value="へそ周り">へそ周り</option>

          <option value="両手の甲・指">両手の甲・指</option>

          <option value="Vライン(上部)">Vライン(上部)</option>

          <option value="Vライン(Iライン)">Vライン(Iライン)</option>

          <option value="両足の甲・指">両足の甲・指</option>

          <option value="もみあげ">もみあげ</option>

          <option value="口下">口下</option>

          <option value="胸">胸</option>

          <option value="両ヒジ上">両ヒジ上</option>

          <option value="両ヒジ下">両ヒジ下</option>

          <option value="へそ下">へそ下</option>

          <option value="Vライン(サイド)">Vライン(サイド)</option>

          <option value="両ひざ上">両ひざ上</option>

          <option value="両ひざ">両ひざ</option>

          <option value="両ひざ下">両ひざ下</option>

          <option value="えり足">えり足</option>

          <option value="ヒップ">ヒップ</option>

          <option value="ヒップ奥">ヒップ奥</option>

          <option value="背中(上)">背中(上)</option>

          <option value="背中(下)">背中(下)</option>

          <option value="腰">腰</option>

            </select>
          <!-- /.select --></div>

          <div class="block-voice-list no_margin js-appendmore-voice" itemscope="" itemtype="http://schema.org/BeautySalon">

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="口下">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">顔の脱毛はあんまり興味がなかったけど「フェイシャルエステと思ってやってみるといいですよ。」とお店の人に薦められやってもらうことにしました。
顔の脱毛、いいです！今までにない感じ！
特に口の下は、今まで気にしたこともなかったけど、脱毛したことでくすみが消えて、肌のトーンも明るくなって、ファンデーションが薄づきでもすごくきれいに仕上がります。驚きました～♪
今までは、顔全体的にファンデーションが結構厚めだった気がします。脱毛して、ナチュラルメイクできれいに仕上がるようになりました。ファンデがなかなかなくならない！お財布にやさしい効果もうれしいです(^^)/</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="口下">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">ワキの脱毛をしているときにお店の人とスキンケアの話をしていて、口の下にニキビができやすいという悩みがあると言ったら、口下脱毛をオススメされました。脱毛のスキンケア効果はワキで十分わかっていたので、アリかも？と思って早速やってみることに。
本当にニキビが出来にくくなって感動してます！脱毛のおかげで毛穴がきれいになってるってことなんですよね～。チョコレートもナッツも、たくさん食べても、もう怖くありません！（食べ過ぎて今度はダイエットが必要になりそう苦笑）
ニキビがなくなって、お化粧もきれいにできて、おしゃれがますます楽しくなりました。友達でニキビに悩んでいる子にぜひぜひ教えてあげたいと思いまーす。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="鼻下">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">メイクをするときいつも気になって、ほとんど毎日自分で剃っていました。でも、不自然に青く残ってしまって肌も荒れるし、メイクだけでは隠し切れない状態になり、、思い切って脱毛することにしました。
だんだん目立たなくなってきたので、本当にやってよかったと思っています。最初、顔の脱毛は周りでもやっている人がほとんどいなかったのでちょっと不安でした。未知の世界で。脱毛とか怖くないの？？と思って・・・。
そんな不安そうな私を見たからか、スタッフの方がとても明るくやさしく、丁寧に説明してくださったので、安心して脱毛することができました。本当にありがとうございました。今では友達にもおススメしています！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="鼻下">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">カラダの脱毛をしているとき、お店の人に「顔の脱毛はどうですか？」と聞かれました。まったくお手入れをしていないかどうか聞かれて、よく考えたら鼻の下の毛はときどき自分で剃るなーと思い、そう伝えたら「少しでも自分でお手入れしているなら、脱毛がおススメですよ」と言われました。すでに脱毛するとすごく楽ちんなことが分かっていたし、肌もきれいになることも実感していたので、やってみることにしました。
ニキビが改善されたり、人によっては美肌効果もあるそうです。もともとニキビはあんまりできないけど、美肌効果は魅力的なので、効果が出てくるのが楽しみです＾＾ 顔の毛は細いせいか、身体の脱毛よりも効果を実感できるのに時間がかかるらしいですが、少なくともすでに前より美肌にはなってると思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="鼻下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">鼻下の毛は顔の中で一番濃いそうで、子供のころからヒゲみたいとからかわれたこともあってずっと気になっていました。でも顔の脱毛、しかもここだけ脱毛するってどうなんだろう？と思っていました。
仲のいい友達にその悩みを話したら、銀座カラーの話になりました。友達はワキの脱毛をしていたのですが、お店の人に聞いてくれて、顔の部分脱毛もあると分かったので、無料カウンセリングに行くことにしました。
顔の脱毛をしている人は結構たくさんいること、美肌効果やニキビ予防にもいいこと、などを聞いてすごく惹かれちゃいました。
でも顔だから、痛みとかは無理かも。。と思っていたら、友達が「大丈夫。そんなに気にするほど痛くないよ！」と言ってくれたので、思い切って脱毛することにしました。今は本当にやってよかったと思っています。メイクの仕上がりが格段に違うし、自分に自信がもてるようになりました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="鼻下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛し放題コースで気になるところはほぼやり終えたので、顔の脱毛をすることにしました。まずは、顔の中でも日頃ムダ毛処理をしている、鼻の下から始めることにしたんです。
顔の脱毛効果もすごいです！！脱毛することで毛穴がきれいになるからか、肌が明るくなってノーメイクでも毛穴がほとんど目立ちません。最近では日中はほぼすっぴんで日焼け止めだけ、ってことが多くなりました。それなのに「すっぴんに見えない！」と友達にうらやましがられます(*^ ^*)
肌に負担が少ないとさらに美肌になってる気がして、すごくうれしいです。
次にやるところはまだ決めてないけど、顔の脱毛も全体的にやってみようと思っています。友達や恋人が顔に近づいても自信持っていられるようにまでなりたいです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="Vライン(Iライン)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">Vラインの脱毛をやろうとは思っていなかったけど、全身脱毛コースにしたので、せっかくだからやってみようと思い、お店の人に勧められたVラインのiラインから始めました。
お店の人の話でも、口コミでも、水着を着るときや温泉のときにすごく楽チンと聞いていたので、効果を楽しみにしていました。
つい最近、旅行で海に行ったのですが、着替えるときも海でのんびりと寝転んでいるときも、毛の事を気にせず思いっきり楽しめました！
デリケートな部分だけに、最初はちょっと痛みが強い気がしましたが、効果の方が大きくて満足しています。痛みは、すぐに慣れるので（個人差はありそうだけど・・・）とくに大きく気にするところでもないと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="Vライン(Iライン)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">生理期間中、大事な部分の肌が荒れてしまうのが長年の悩みでした。ネットでいろいろ調べているときに、脱毛も改善策の１つという口コミを見つけたので試してみることにしました。
口コミだけでは不安だったので、銀座カラーの人に、質問しまくりましたが、どれも納得できる答えだったので、思い切ってやってみることに。
今まで悩んでいたのが嘘みたいに、生理中の肌荒れが改善しました！これは楽！！ムレが気にならなくなったのが一番大きいのかもしれません。スタッフさんも、iラインの脱毛は衛生的にいいと言っていたので、こういうことか！と納得しました。夏場とかは、特によさそうだな、と思っています。こうなるんだったらもう少し早くやっておけばよかったなぁと思ってます。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="Vライン(Iライン)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">Vラインのiライン脱毛って、彼氏以外の人に見せるものでもないし（笑）なんで必要？と思っていたのですが、友達が「下着からはみ出たりしなくなるし、ムレも少なくなってすごくいいよ」と言っていたので興味をもちました。お店の人に詳しく聞いて、なるほど！と思ったので、脱毛し放題コースだし、やってみることにしました。
実際にやってみた感想は、iラインの脱毛、かなりおススメです！！
どうしてもムレる場所なのでかゆくなったり、おまけに生理時は臭いも気になっていたのですが、脱毛してからの改善効果をすごく実感しています。脱毛ってすごいんですね。お姉ちゃんに話したら、脱毛したことのない姉も、脱毛したくなったって言ってました。きっと近いうちにやると思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="Vライン(Iライン)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">私は小さい時からアトピーなので、脱毛はずっと興味があったけど怖くて、やっていませんでした。おしゃれのためだけだったらガマンできるけど、月に１回必ず来る生理の度にかゆみがひどくてかなり辛くなるので、思い切って銀座カラーさんのカウンセリングで相談しました。
アトピーの人でも肌状態が落ち着いていれば大丈夫と聞き、実際に脱毛されて効果もあり、満足されている方の話も教えてもらい、勇気が出たので脱毛する決心をしました！
今では思い切って脱毛して本当によかったと思っています。生理のときは外出を控えるほどだったのが、今は平気になりました。かゆみだけじゃなくムレも臭いもあまりなくて、ブルーな１週間じゃなくなりました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="Vライン(サイド)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">近所のジムに通いよく泳ぐようになり、水着を着るときにVラインが気になり出しました。基本めんどくさがりで最初は自己処理していたのですが、太ももの付け根部分などはとても太い毛だったので、剃った後でも黒くプツプツと見えて、ちょっと目立つ感じでした。場所的に抜くのは大変だし、なによりもとても痛いので、お店で脱毛するのが一番かな～と思いました。
痛みに慣れていたせいか、施術の時の痛みは個人的にはそれほど気にならなかったです。今では、水着を着た時に気にする部分はVラインではなく、全体の体型？というかスタイル（笑）になりました。スタイルに磨きをかけたら、夏には海やプールでも自信を持って水着を着られると思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="Vライン(サイド)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">友達と温泉旅行に行ったときに、みんなで一緒にお風呂に入って、自分と友達とを比べて、毛の生えてる範囲が広いのかも・・・と思うようになり、ずっと気になっていました。お風呂だけじゃなく、普段からほぼ確実にショーツからはみ出る毛が気になっていたので、毛が生える範囲を狭くするためにVラインのコースをお願いすることに。
自分で剃っていた時はかみそり負けすることもあって結構大変でしたが、施術後は本当に楽になりました（＾＾）この箇所の特徴なのかもしれませんが、抜けるのがわかりやすく段々とキレイになっていくのが実感できました。荒れ気味だった肌の状態も良くなってきたし、きれいな状態になるのが今からとても楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="Vライン(サイド)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">スポーツジムに通い始めてからVラインの毛が気になり始め、自分で処理をしていました。でもわたしのVラインの毛は太めなので毎回自己処理が大変で、肌への負担も心配…。そこで脱毛してみようかなと思い始め銀座カラーさんへ！銀座カラーさんは施術前・施術後に保湿をしてくれるので、肌がより潤っていく感じが嬉しいです♪「施術を受けてから1～2週間ぐらいで毛が抜け落ちていきますよ」とサロンの人に言われ、わたしのような太い毛でも抜け落ちてくれるのかなーと思っていました。でも本当に抜け落ちていき、脱毛の効果を実感！半月ほどすると次の毛が生えてきたのですが、以前よりも毛が柔らかくなった気がします。パンツからはみ出す毛も気にならなくなりましたし、ジムでもより一層気持ちよくトレーニングができるようになりました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="Vライン(サイド)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">わたしは今までVラインのお手入れなんて気にしたことがありませんでした。温泉に行くときも可愛いショーツをはいているときも。あるときファッション雑誌の読者アンケートコーナーでVラインのお手入れについての記事を見ました。なんと7割の女性がVラインのお手入れをしている！みんなお手入れしてるんだなぁ…と衝撃を受けました。そこからはお手入れを全くしてこなかった自分のVラインが気になり始め、自己処理だと怖かったのもあり脱毛を考えました。銀座カラーさんはVラインだけでも細かくパーツが分かれていて、形を整えるだけでも気軽に始められました。お手入れしてもらうときはきちんとした個室なので安心して施術を受けられました。デリケートな部分の脱毛でも、恥ずかしさをほとんど感じさせないスタッフさんの優しい対応もとても良かったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="もみあげ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 サービス業<br></span>
            </dt>
            <dd itemprop="reviewBody">私のもみあげは、すごく濃いというわけではないのですが、産毛が頬の方まで広範囲に生えているので気になります。<br>
飲食店に勤務しているため、仕事中は髪の毛を必ず結び、もみあげ部分は丸見えに…。定期的に自己処理していますが、うっかり忘れてしまうこともあるので、恥ずかしい経験もしました。<br>
もともと顔の脱毛に興味があったのでサロンを調べていたら、銀座カラーではもみあげも脱毛できると知ったので、顔と一緒に脱毛を始めました。<br>
最近では、顔ともみあげ周りの産毛がなくなって、横顔に自信が持てるようになりましたよ!<br>
産毛がなくなったことで、ファンデーションのノリもよくなり、メイクも楽しめています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="もみあげ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">もみあげ周りの産毛が濃いので、ずっとコンプレックスを抱えていました。<br>
いつももみあげを隠すことばかりで、髪の毛を結ぶことはなく、髪型はショートボブ。<br>
大学に入ってからイメチェンしたいと思い、脱毛することを決意したんです。<br>
サロンを探し始めると、銀座カラーにはもみあげの脱毛があるので、嬉しくなってすぐ問い合わせてみました。金額も思っていたほど高くなく、大学生の私でも通えるほどでした。<br>
脱毛は初めての経験だったので、1回目はすごく緊張しましたが、とても親切に対応して頂いたので、その後はリラックスして施術を受けることができました♪ 今ではもみあげ周りのうぶ毛がなくなり、綺麗に整いました。<br>
あれほどコンプレックスだったおだんごヘアやアップスタイルも、毎日楽しめています!</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="背中(下)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">背中なんて、誰にも見せない部分だしって気持ちでノーマークな人生を歩んできました。触ると意外に生えてることに気づいたんです。海とかも小学生以来行っていなくてほんとに油断してたのですが大学時代、サークルで海に行こうってなった時がありました。仲間はずれになるのは嫌だ！でも背中に毛が生えている今のままじゃ行けない！ってことで広告で見た銀座カラーへ直接聞きに行こうと思いお店に行って色々相談にのってもらいました。最適な方法やコースをちゃんとこちらの立場になって考えてくれてすごくよかったです。おかげさまで海で堂々と服を脱いで水着になれそうです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="背中(下)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">他人を見ていて、ふとした時（物を拾おうと思って屈んだ時とか）に意外と見えてるんだな・・・と気づいた部分が腰の上というか背中の下あたりでした。鏡で見てもほんと見えない部分なんですよね。本当に見えないからスマホのカメラでパシャリとしてみたら、わ～意外にぽつぽつ生えてる～(；_；)　でもこんな部分って脱毛の対象箇所に入ってるのかな？と思ってネットで検索。丁寧な説明を見つけたのが銀座カラーでした。次の日に早速ネット予約をして、カウンセリングを後日受けに行きました。どういった方法で進めていくのかや、費用面など分かりやすくお話してくれたので安心して契約書にサインをしてきました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="背中(下)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">背中は鏡で見えづらい箇所なので、気にしていなかったのですが、年齢を重ねていく度に背中のニキビが増えたのと、毛が濃くなっていることに気付きはじめました。ホルモンバランスのせい？と悩み始めていたのですが、自分では手の届かない箇所なのでどうすることも出来ず、ネットで色々調べて銀座カラーさんへ行ってみることに。
背中の上だけやろうと思っていたのですが、上だけキレイになるのも変だと思って、背中の下も一緒にやることにしました。
施術では背中に冷たいジェルを塗られて、最初はかなり寒い！でもそれも最初だけなので、キレイになることを考えて我慢我慢！
続けていくと次第にニキビも無くなってきたので、毛穴から菌が入ることも減ったようです。
今では気になっていた背中の毛がキレイに無くなり、背中が開いた洋服や、水着を堂々と着れることが一番嬉しいです♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="背中(下)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">デスクワークの仕事でいつもパソコンと向かい合う日々。肩は凝るし背中も曲がってきちゃうし、腰も痛くなるのでマッサージ師の彼によくほぐしてもらっています...。
全然知らない人にマッサージしてもらうのとは違い、やっぱり彼だと背中の毛が気になってしまうので、、、背中全体の箇所を選んで脱毛を始めました。特に背中の下は背中全体的に見ると真ん中あたりで、視線が行きやすいので。何回か脱毛を終えてからサロンのスタッフさんに「毛が薄くなってきていると思いますよ」と言われ嬉しくなりました！心置きなく彼のマッサージを受けることもできて、彼からも「背中なんかきれいになったね」と言われとても満足しています（＾＾）</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ヒジ下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">小学校の時に同級生の男子から毛深いと言われたのがショックで、ずっとカミソリと毛抜きで自己処理を続けてきました。
長年自己処理しているせいか、カミソリをしてもすぐにチクチク…。埋没した毛もたくさんできて、最悪の状態に。。。
すがるような気持ちでカウンセリングを受けました。
カウンセリング時に私の今までの自己処理方法や今の状態をスタッフの方に伝えると、そうなってしまう理由や改善方法を優しく丁寧に教えてくれました。
ヒジ上にも結構むだ毛があったので、金額的にも腕全セットの方がお得だったため脱毛をすることに。
ずっとコンプレックスだった腕が今では見違えるようにキレイになりました！自己処理の面倒くささからも解放され大満足です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ヒジ下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">若干毛深いほうではあったのですが、あまり気にせず過ごしていました。
大学に入り上京。東京の大学生の美意識に驚きました。とはいってもまとまったお金もないので、家で自己処理をしていました。
「社会人になる前にキレイにしたい、この手足のチクチクをどうにかしたい！」と思っていたので、大学4年生の春から脱毛をスタートしました！
最初の1回は緊張して痛いのか痛くないのかもよくわからない位でしたが、2回目以降は「こんなもの？」くらいの感じで終わるので、問題なく通えています。
それよりも、1週間くらい経ったら毛がするっと抜けてくるのがすごい楽しい！自分の腕を見て一日中毛を抜きたい衝動にかられています。
このペースでいくと社会人になるまでには全て終わらせることは難しそうですが、1回の時間もかからないので仕事帰りでも無理なく通えそうです♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ヒジ下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">バイトの制服が半袖なので、接客をしている間いつも腕のむだ毛が気になっていました。お客さんにも「あの子の腕ブツブツだな」なんて思われているんじゃないか…と良からぬ想像までしてしまったり。通いやすい場所でないと続かないから、バイト先の近くにある銀座カラーで脱毛することにしました。
実際の脱毛時間は20～30分程度で、エステティシャンの人も手際よく対応してくれ、脱毛の知識も豊富だから質問に答えてもらったり体験談を教えてくれたりと、脱毛している時もお話できて楽しい！私はまだ学生なのでまとまったお金は出せないから月額コースがあるのでとても助かっちゃいました。
この間バイトのスタッフから「腕きれいになったね～」と言われニッコリ。やはりみんな見ているんですね～。次は足の脱毛をするつもりです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ヒジ下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">今までは脱毛器で自己処理していました。肌が弱いのも関係あると思うけど段々と肌荒れが治りにくくなってきて…。これ以上肌に負担をかけたくなかったので、お金はかかるけど脱毛サロンに行くことに決めました。
電車で銀座カラーの脱毛学割を見ていたので、カウンセリング予約をしました。カウンセリングでは肌が弱いことを相談すると、カウンセラーの方は丁寧に対応してくれたのでとても好感が持てました！
本当に肌荒れが怖かったのですが、脱毛前後のケアや家でのアフターケアの方法などを教えてもらい、少しずつ肌が落ち着いてきました。脱毛の回数が進むにつれ肌の状態も良くなってきて、始めからプロに任せれば良かったんだな、と。次回が最後の6回目ですが、7割近くの毛がなくなっている気がします。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="口下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">顔脱毛のコースを申し込んで、顔全体の毛の状態をチェックしてもらいました。気にしたこともなかった口の下の部分をチェックされて驚きました。みんなも処理し忘れがち、だそうです。私の場合、特に目立つような生え方をしているわけではなかったので、あまり気にしたことはなかったし、だから剃ったこともありませんでした。でも、やっぱりプロの方から見るとそれなりに生えているようで、お手入れした方がいいとアドバイスされました。
実際に脱毛してみると、明らかに肌もワントーン明るくなった感じで、化粧ノリもよくなった気がします。脱毛することで肌質が変わったと思えるくらい、きれいになって嬉しいです。プロのアドバイスはすごいと思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="口下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">もともと顔の毛が濃いめで、ずっと悩んでいました。顔の脱毛を始めるとき一番気になる部分を聞かれて、真っ先に口の下と答えました。自分で剃っていたからか何だか黒ずんでしまっていて、いつもファンデーションを厚く塗っていました。同じ脱毛でも顔と身体とでは効果も違ってくるんじゃないかな？と思っていたので、どんな感じになるのか正直不安で不安で。。。
何回かやってみて、だんだんと太い毛は生えてこなくなったようです。毛質によっては、ある程度回数をかけないとなくならないという話を聞いていたので、どのくらい通えばいいんだろう・・・と思っていたのですが、このペースだとあともう少しかな？やっと長年の悩みが解消される～！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両足の甲・指" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">わたしは昔から裸足でいるのが好きでした。そのくせキチンとケアもせずお手入れもしていなかったので、お出かけの時にサンダルを履いた自分の足をみてがっかり…。毛は生えてるわ乾燥しているわで、自分のケア不足を痛感しました。早速毛の処理をする為、銀座カラーさんへ行きました。
足全セットにしたのですが太ももや足下に比べ足の指の毛は少なめだったので、2～3回通うと毛がほとんど気にならなくなりました。銀座カラーのスタッフはカウンセリング時の説明もマニュアルっぽくなく、ちゃんと会話しながら伝えてくれてとても好印象でした。お手入れし忘れていてももう気にならないレベルなので、いつでもお出かけ用のサンダルを履いていけます♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両手の甲・指" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">手には自信がありました。指も長く細くてスラリとしてますし、自分でハンドマッサージも欠かさずしていました。将来は手のタレントさんになりたいな、なんて思っていたこともあります。でも、、、毛深いんです。手の甲と指になかなかの太さの毛が生えていました。
「腕などに比べてみんな大して見ていないでしょ」なんて思っていたのでカミソリで処理していましたが、ある日、彼氏から「指の毛生えてる！」と言われ恥ずかしくてショックでした。すぐにカウンセリング予約をしました。
選択したコースはフリーチョイスです。金額的にも手頃だったので。脱毛範囲が狭いので実際の施術時間も10分程度ですぐに終わりました。
まだ数回しか通っていませんが、通う前と今では確実に毛が薄くなってきています。
今は他の箇所もセットですれば良かったなぁ、なんてちょっと後悔しています。手の甲が終わったら足の甲もお願いしようかな？と思っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両手の甲・指" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">看護師をしているので患者さんと接近することが多く、手元には気をつかっていましたが脱毛までは考えていませんでした。
仲間の看護師が患者さんに手がキレイだと褒められていたので、後日その話になった時に手を見せてもらったらツルツルで驚きました。
彼女は既に全身脱毛をしているようで、時間はかかるがお手入れの必要がなくなり最高だよ、と勧められました。
とは言ってもお金も時間もかかることなので、まずは手で試してみることにしました。
勤務先の近くのお店、という条件から銀座カラーさんにしました。店内もスタッフの方も感じが良く落ち着くお店です。
手の甲と指だけなのであっという間に終わります。思っていたより痛くないし、今度は腕全セットに挑戦しようかな？と思っています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両手の甲・指" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">長年お付き合いした彼氏からプロポーズを受け結婚をすることになりました。結婚指輪を探している時に店内のきらびやかな照明の光で指の毛が確実に目立って恥ずかしかったのです。指輪をはめてもらう本番のその時までにはなんとかきれいな指にしたいと思って、ブライダルエステの他に脱毛もすることにしました。エステサロンに近い脱毛サロンを探し、銀座カラーに通い始めました。ご結婚されているスタッフさんに担当して頂いて施術以外でも色々アドバイスをもらったりし、ためになる時間を過ごすことができました。先日無事に結婚式を迎え、綺麗さっぱりな指に彼から指輪をはめてもらいました。エステに脱毛にちょっと出費してしまいましたが満足です。脱毛はまだまだ1年くらい残っているのですが、予約は比較的とりやすいので順調に脱毛できそうです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両手の甲・指" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">わたしの手の甲は親譲りなのか毛深くて毛穴が目立ち、お世辞にも女性らしいきめ細やかな手とは言えません…。小学生の頃から友だちの毛穴のない白い手をとても羨ましく思っていました。季節問わずいつも見えている部分なので、毎日の自己処理では何も改善しないんじゃないか？と思い、脱毛を始めました。
カウンセリングのときにスタッフさんに相談したところ「脱毛をしていくうちに毛は薄くなっていき、そのうち自己処理のいらない肌になりますよ」と言われとても希望が持てました。3回ほど通い、確かに自己処理の回数が減ってきたことを実感しています。脱毛の前後に保湿ケアを行う事で脱毛時の痛みを抑え、その上美肌効果もでるそうなので、今から仕上がりが楽しみです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="もみあげ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">顔全体の産毛が濃くて、化粧をしてもうっすらわかってしまうほど。自己処理をし続けていたら毛穴まで開いてきてブツブツお肌になってしまいました…。もちろんそれだけの理由ではないと思うのですが、肌も敏感肌なので、もう自分では対処法が見つけられなくなっていました。<br>
顔脱毛を検索していたら、銀座カラーは脱毛だけじゃなくて、オプションの保湿ケアがすごい効果がある、と書いてあったので、顔ともみあげをセットでお願いしました。<br>
口コミで書いてあった通り、回数を重ねる毎にお肌がキレイになっていきました！毛穴も気持ち小さくなっている気がします。 <br>
頬からもみあげにかけても、産毛がなくなり綺麗に整えられてきたので、今はアップの髪型をした時のサイドラインがお気に入りです!<br>
アップスタイルがより綺麗になるように、次はうなじの脱毛も始めます。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="もみあげ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">髪を耳にかけることが多く、もみあげ部分の産毛が濃いので気にしていました。<br>
他のサロンで、ワキの脱毛に通っていましたが、銀座カラーでもみあげの脱毛を見つけたので、乗り換えちゃいました!<br>
少しずつもみあげ周りの産毛がすっきりしてきて、触った感触も変わってきました。<br>
横顔って自分では見逃しがちだけど、周りの人からは結構見られているので、変化を友達に気づいてもらえた時がすごく嬉しいです!<br>
あと彼氏に触れられた時に、触り心地がいいって褒められたので、とっても幸せです。<br>
これからも定期的に通って、綺麗な状態をキープしたいと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="Vライン(上部)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">友達が銀座カラーさんでVライン脱毛をして、すごくよかったと勧められたので利用しました。
Vラインなんてそんな露出するような場所じゃないし、エステティシャンの人に見られるのも恥ずかしいな…と乗り気じゃなかったんですが、彼氏ができたのでそうも言っていられなくなりました、ピーンチ（笑）
最初は少し緊張しましたが、友達から聞いたとおり店内がとてもおしゃれで、スタッフもみなさん気さくで話しやすかったので安心できました。施術について説明もしっかりしてくれて、あっというまに終わったので、恥ずかしさを感じるヒマもありませんでした。予想より痛みも少なかったし、保湿ケアのミストシャワーが気持ちよくて大好きになりました。 Vラインはなかなか自分ではお手入れできない部分だし、彼氏に喜んでもらえたのが何よりうれしいです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="Vライン(上部)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">ストレス解消目的で水泳を続けています。肌の露出が多いので、ワキとVラインの脱毛を考えていましたが、元々あまり毛深い方でも無いし、Vラインなどは自分以外の人に処理を頼むのも気恥ずかしい事もあり、その都度自分で処理すればいいや…とやり過ごしてました。しかし仕事やプライベートが忙しくなってきてゆっくり処理する時間も持てず、さすがにこのままでは…と一念発起。それでも初めての事だし、恥ずかしさや痛かったら？など不安ばかり。。。でもスタッフの皆さんが親身になって話を聞いてくれたので、リラックスした状態で施術を受ける事が出来ました。デリケートな部分ですし少し躊躇してましたが、自分で処理をするより衛生的で安心感があるように思いました。これからの季節、水着を着る機会も増えるし楽しみです♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="Vライン(上部)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">今まであまり気にしたことはありませんでしたが、大学のサークルで海に行った時、サークル内の女の子達の脱毛ケア率の高さに驚きました！みんなVラインの脱毛もしっかりしていて、ビキニもキレイに着こなしていて…。帰ってきてからすぐに友達に勧められた銀座カラーへカウンセリングの予約をしました。
全てが初めての事なので、カウンセリングから緊張しまくり(-_-;)　でもサロンの方がとても優しかったし、どのコースで申し込むか一緒に考えてくれたので、無事脱毛をスタートできました。
初めて脱毛する時はどれだけ痛いのだろう…とびくびくしていましたが、実際は大して痛くもなかったです。でも、やっぱりちょっと恥ずかしかったかな…(笑)</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="Vライン(上部)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">小さい頃から肌が弱く、季節の変わり目や湿度の変化で肌が痒くなります。
VIOラインも例外ではなく、梅雨時期は蒸れて痒くてたまりません。
でも人前で容易に手でかける場所ではないので、本当に困ってました。
毛深い方なのでVラインの毛も結構濃いのが原因の1つでもあるのかも？と、思い切ってVIO脱毛に挑戦しました。
せっかくなのでワキの毛も一緒に脱毛してもらいました（ワキも痒くなるので…）
肌が弱いので大丈夫かな？と心配していましたが、保湿ケアの効果からか、施術後も痒みやかぶれも出ませんでしたし、黒い太い毛が1,2週間後にポロッととれるのがたまらなく気持ちよいです。
毛が少なくなるにつれてあの猛烈な痒みが起こる回数が減り、本当に嬉しい！とにかく嬉しい！勇気を出してVIO脱毛して良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ワキ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">学割制度があったので、思い切って両ワキの脱毛をお願いしました。最初から何も生えていなかったのかと思うくらいすごくキレイになって大満足です！！もっと早くやりたかったです。
今まではワキは自己処理でやっていたのですが、数日たつとすぐチクチクしてしまうし、たまにカミソリでキズをつけてしまったりして面倒だったのが、何もしなくてよくなり本当にラクです。お風呂で体を洗うときもツルツルしたワキが嬉しくて、ついつい触ってしまいます。
それに、朝、ワキのことを気にしないで着る服を決められるので、ノースリーブやキャミワンピを選ぶ回数が増えました！実は、母にも「脱毛って昔はもっと高かったのよー」とうらやましがられています（笑）
今年の夏は、水着を着るのがとっても楽しみです♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ワキ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛には前から興味があったんですが、値段が高いのと、友達から結構痛いよって聞いてて怖くて勇気が出せませんでした。この前海外旅行に行った時に、水着を着るたびにカミソリで毛の処理をしてたんですが、とても面倒くさい上に剃り残しがあったりして恥ずかしい思いをしたんです。それをきっかけに、旅行だけじゃなく普段の生活も脱毛した方がやっぱり楽だよなーー！って思い、脱毛サロンへ通い始めました。エステとかサロンとかあまり行ったことがなかったので最初はとても緊張してたんですが、サロンスタッフの方が丁寧に説明してくれて納得できるコースを提案してくれました。私は両ワキとVラインのコースに通ってますが、思ってたより痛くないし少しずつ毛も薄くなってきた気がします。頑張って通ってツルツルになりたいです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ワキ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">これまで家庭用の脱毛器を使ってたんですが、あまり高いものじゃないからか効果が感じられず、結局カミソリや脱毛クリームで処理してました。もっと高い脱毛器を購入しようかと悩んだんですが、結局手間もお金もかかるしな～と思ってサロンへ通う事にしました（＞。＜）
たくさんのサロンがキャンペーンをやっているので、面倒だったのですが3店舗ほどカウンセリング受けました。スタッフの感じやお店の雰囲気が自分に合いそうだったので銀座カラーに決めました。
サロンではプロの方にお任せするだけでいいので本当に楽でした。特にワキは毎日のように自分で処理していたのでそれがなくなって本当に楽ちんです。脱毛器を使っての自己処理だと、手が届かないところが自分ではちゃんとしてるつもりでもきれいにできてなかったんだなー、と脱毛後の肌をみて思いました。あの時高い脱毛器を買わず、ちょっと頑張ってサロンに通って正解だったなと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ワキ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">私は脱毛には全然興味がなかったんですが、友達と話してると結構みんな脱毛をしていることが分かったんです。カミソリでジョリジョリ処理すれば十分だしって思ってたんですが、話を聞いてるうちにだんだん興味を持つようになりました。子供がいる友達から、子供できたらただでさえ時間がなくなるから子供がいない間にやってた方がいいよ！と言われて、行くなら今しかない！と思って通うことにしました。 友達にも色々と相談し、夏の時期に気になってたワキとおへそ周りにしました。想像してたより少し痛くて、ビクッてなっちゃいますが我慢できないほどではないので耐えられます。完全に毛が生えなくなるまでまだ時間がかかりそうですが、手をあげてもワキが気にならないようになるんだな～とかもうカミソリで剃らなくてよくなるんだ！と思うと頑張って通おうと思えます。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両足の甲・指" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">恋人が、すごく細かい人で色々なところを気づくんです。この前は、足の指まで「あ、毛穴黒いね・・・」なんて言ってくるんで凄くショックで。なので、見返してやろう！と思い立ってネットでの評判を最優先にサロンを探して出てきたのが銀座カラーでした。名前からしてちょっと高級な感じがするので金額もすごい高いのかな・・・？と思いきや、リーズナブルでそれでいて丁寧かつ親切な接客でした。私も仕事は接客業なので色々勉強になる部分もあり、プラスになりました。そこまで太い毛が生えていたわけではないので、パチッと瞬間痛みがくるだけでほとんど痛みは感じませんでした。足の指と甲もすっきり綺麗になったのでサンダルを履きまくろうと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両足の甲・指" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">この前、爪を切ってる時に足の甲に強そうな毛が数本生えていることに気づいたんです。いつの間に生えたんだろうと思うくらい唐突に見つけてしまいすぐ抜いてしまいました。抜いて数週間経つとまた生えてきてそれをまた抜く。繰り返していたら範囲が広がってきている気が…。抜くより脱毛しちゃったほうがいいかな、と脱毛シートを買ってきて、ベリッとやってみたら真っ赤になってひどい状態になりました。サロンにお世話になるとどのくらいお金がかかるんだろう？と思い調べたら足の甲はたいしてかからないようなので、銀座カラーのフリーチョイスコースを選びました。時間も数十分で終わるので、3ヶ月に1度仕事帰りに行くように予約を取り通いました。毛は周期があるので時間はかかりますが、確実に毛がなくなったので満足です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両足の甲・指" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">きっかけは彼からの一言でした。それまで気にしたことなかったのですが「女の人も足の甲に毛はえるんだね」と言われ、すごく恥ずかしかったです。それ以来気になって仕方なくなりカミソリで剃っていたのですが関節などは剃りにくく、何度か切ってしまったことがあります。そんなに広い箇所でもないので自己処理でも良かったのですが、正直小さい範囲だからこそ面倒くさいんですよね。当時はVIPハーフを利用していて、ひざ下の脱毛はあと2回くらいで終わりそうだったので、フリーチョイスで足の甲も追加ですることにしました。スタッフの方も臨機応変に対応してくださってとても居心地がいいので、金額だけで他のサロンにしようとは思わなかったですね。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="へそ周り" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">夏休みに海やプールに行きたくてダイエットと脱毛(VIO)を始めています。いい感じに準備が整って来た～と思ったら、あれ、おへそ周りの毛が。今通っているサロンで追加しようと思ったら予約がいっぱいで3ヶ月以上待ち。ネットで探していたところ銀座カラーのホームページにたどり着きました。電車の中吊りが印象的で気になっていたものの、こんなに安くて脱毛できるの？と思っていましたが本当でした！スタッフのみなさんも相談しやすい雰囲気で安心できます。機械が優れているのか脱毛はすぐ終わるし、保湿もしっかりしてもらえるので嬉しいです。おへそ周りも段々とキレイになってきています。はじめから銀座カラーにすれば良かった～。予約も取りやすいのでVIOよりもおへそ周りの方が早くおわっちゃいそうです。妹や友達にも勧めています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="へそ周り" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">おへその周りを取り囲むように毛！毛！毛！。そしてVラインに向かって縦ラインのむだ毛。スタイルは比較的悪くないのですが（自分で言うな）、トップスからチラッとお腹が見えてしまうこともあるので、いつもカミソリで自己処理していました。産毛よりは濃いけど腕の毛に比べれば薄いという程度のむだ毛ですが、最近徐々に濃くなってきている気がして怖くなり、脱毛をすることにしました。
クリニック系に行くか迷ったのですが、狭い箇所なので時間がかかりそうだと思い、短い時間で対応してくれそうな銀座カラーを選び、VIPハーフコースでお腹周りを中心にお願いしました。まだ6回全部は終わってないですが、なかなか効果がでてます。 おへそ周りのとぐろのような毛は薄くなってきましたし。最近はお腹がチラ見えしてしまっても、どうぞ見てください！って気持ちです。こんなに自信が持てるなんて、脱毛に感謝！かな？（＾－＾）</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="へそ周り" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">まばらに太い毛が生えているおへそ周り。毛抜きで抜くと皮膚がぷくっと盛り上がった感じになったり、カミソリで剃ると今までうぶ毛だった毛まで濃くなった気がするし。自己処理の限界を感じて銀座カラーへ駆け込みました。おへそ周りって普段は見えない部分ですが、水着の時は絶対NGですよね。丈の短いトップスが流行った時も、自己処理するのを忘れてて本当に恥ずかしかった思い出は忘れることができません。おへそ周りの毛のことを無料カウンセリングで相談するのは恥ずかしいなと思ったのですが、カウンセリングをしてくれた方はさすがに慣れていらっしゃるようで、水着を着る時期になるとおへそ周りの毛やお腹の毛が気になって脱毛をしに来られる方も結構いますよと言われてホッとしました。実際に脱毛をして、丈の短いトップスや水着になる不安や心配がなくなったので、おへそ周りを脱毛して良かったです。</dd>
          </dl>

 <!--<dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="へそ周り" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">ベリーダンスが趣味で毎週スクールに通っているのですが、衣装がいつもヘソ出しなので、おざなりにしていたお腹のムダ毛をそろそろ退治したくて脱毛することにしました。
そこまで濃いへそ毛ではないのですが、日本人の肌は白く、やっぱり黒いむだ毛は美しくないので…。
スクールが新宿にあるので、近くの新宿南口店にお世話になることにしました。
今回は単純におへそ周りだけお願いしたので、実際の脱毛時間は着替え込みで30～40分程度で終わりました。痛みですが、私は全く気になりませんでした。それより一回で照射できる範囲が広いので時間が短くていいですね。あまりたくさんの時間やらなければいけないと思うと足が遠のきますから。 まだ2回目なのであまり変化を感じませんが、スタッフさんが言っていたようにスルッと毛が抜けているので、ムダ毛は少なくなっていると思います。</dd>
</dl>-->

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="へそ下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">去年ダイエットに成功し、好きなモデルさんが時々やっているへそ出しファッションをやってみたくトップスを購入しました。が、実際着てみるとあらわになるむだ毛のオンパレード。せっかく洋服を買ったのにまったく無理だ・・・と思って友達が通い始めた銀座カラーにネット予約。来店してカウンセリングを受けることを勧められ若干抵抗がありましたがお店に行ってみました。素人の自分でもわかりやすく説明をしてくれて、アドバイスをくれる友達感覚というかそんな親近感もありました。学割もきくということなのでそれも利用して脱毛ライフを始めました！脱毛中はスタッフの方とお話ししながらできるので楽しいし、あっという間に1時間たっちゃいます。今4回目ですが、へそ下の縦ラインはほぼわからなくなりました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="へそ下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">ふと、気づくときってあるんですよね。ひゃーー！みつけちゃった！！みたいな。。。自分の身体の部分のおかしい所に。まじまじとお腹なんて見ないので、ふとした時に産毛って結構生えてるんだなぁ・・・と見つけてしまった事がありました。
気づいてしまうとホント気になる！気になると触れてしまって、それが更に刺激を与えて生えちゃうかも！って負のスパイラルに陥ってしまって。そんな小さいことで悩むなら脱毛サロンに通おうと思い、帰りに銀座カラーに行きました。カウンセリングではとても丁寧に説明してもらって、その場で契約してきて今通っています。私のオススメは保湿の時のミストシャワー。すっごい気持ちよくてお肌もぷるぷるになりました！ 気になるむだ毛がだんだんなくなって来たので、次はデリケートゾーンもしてみようかな？</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="へそ下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">今までは友達と海に行ってばかりだったのでおへそ周りなんて特に気を使っていませんでした。でも彼氏と海に行くなら色気あるビキニを着て行きたい！その為にはむだ毛はお手入れしておかなければ。。。
元々おへそ周りは自己処理をしてましたが、お腹の場合、柔らかいお肉のせいで結構剃りづらいと感じていました。特にへそ下は剃れば剃るほど毛が密集してきて濃くなっているような気がしました。脱毛してもらってからは毛が薄くなり、処理の回数もぐんっと減りました。やっぱりプロの技術は凄いです。へそのむだ毛はうぶ毛と濃い毛が混じっているので、最初太い毛はパチッとするかもしれませんよ、と言われましたが全く気にならなかったです。ビキニOK！な仕上がりで彼氏にも喜んでもらえました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="へそ下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">脱毛って、始めると次から次へとやってしまうんですよね。ハマっちゃうかんじ。一部が綺麗になってくると、次はココをしたい！そして次は！てなっちゃうんです。へそ下はそんなに気にしてなくて、うすーく産毛がある程度だったので、脱毛範囲に入れていなかったのですが担当の方の勧めもあり、せっかくなので全身完璧にしてサロンを卒業したい！と思って、へそ下もお願いすることにしました。ほかの箇所に比べて個人的な感想は痛みが一番少なかったような気がしました。ただ、うぶ毛多めなので結構時間はかかっちゃいましたね。ベルトやゴムの締め付けでできた腰周りの色素沈着も保湿ケアのおかげで薄くなり一石二鳥！もう、ほとんどムダ毛がなくなってきててツルッツル♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ヒジ上" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">なんか、おかしなところだけ毛が濃いんです。それはヒジの上辺りから肩にかけて。そこが濃い人なんてそんなにいないと思うのに黒い毛穴がすごく目立つしあんまり半袖にもなりたくなくて毎年、夏が怖かったです。親に相談したら「遺伝かもねー。少し費用負担してあげるから脱毛サロン行ってみたら？」と言ってくれたので通うことにしました。学校に向かう途中に銀座カラーがあり高級感がありそうな店構えだったので前から気になってました。値段も他のサロンと比べてみてリーズナブルだったのと、予約も取りやすいなど他の要素も考慮して通うことに決めました。不安とか全く最初からなかったので、友達に会いに行く感覚で施術を受けに行きました。楽しかったです＾＾</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ヒジ上" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">ヒジ上はカミソリで優しく産毛を剃ってごまかしごまかし、自己処理してきました。でも肌が白くて薄いせいか、すぐ赤くなってしまうんです。そんな産毛もだんだん濃くなってきて、今後どうなるんだろ！？とすごく怖くなってきてしまいました。
手遅れになる前にちゃんとしたところで脱毛してもらったほうが良いよ！という友達の勧めもあり、５つくらい候補を挙げて自分にあいそうなサロンを最終的に決めました。それが銀座カラーさんでした。名前が銀座なので敷居がすごく高そうに思っていましたがそんなことはなくて、通ってる方も若い人がとても多くて親近感！。脱毛だけじゃなくて肌のこともアドバイスしてくれるので、脱毛中のエステティシャンとのお話も含め楽しく通っています♪</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ヒジ上" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">わたしは結構人目を気にするタイプで毛の処理も毎回丁寧にやっています。毛の処理を始めたのは物心ついた小学生のころからで、大学生の今となっても変わらずカミソリで剃っていました。何年も剃り続けているとさすがに濃くなるわたしのムダ毛…でも夏場のお手入れは欠かせないし。 友だちと濃くなるムダ毛について話をしたところ、なんと友だちはもう銀座カラーに行っていました。友だちの腕を見せてもらいそのさらさら具合に感動し、友だち紹介でわたしも脱毛デビュー！カウンセリングでは緊張していましたが、スタッフの皆さんが感じが良いので、すぐにリラックスできました。特にヒジ上部分なんかは大学の講義中に後ろの席の人に見られるんじゃないかとあまり堂々としていられなかったのですが、もうそんな心配も必要なし！毛を剃る回数も減って夏も楽チンです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ヒジ上" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">電車に乗ったときに隣の女性の二の腕の肌荒れにドン引き。。自己処理失敗したなーこの人、なんて。ケアって大事だなーと思いましたが、実は私も大したケアはしてなくて、自己処理で荒れた肌を服の袖でごまかしていました。元々毛深いほうだからよく見るとバレるので着る服も限られてしまい、思い切ったイメチェンがなかなか出来ず。。トライアルコースもあるので軽い気持ちで銀座カラーさんにお願いすることにしました。 キレイに脱毛できましたし、いちばんの決め手になった保湿もエステのようなお肌のケアで、想像以上に文字通りツルツルスベスベになることができ大満足でした！二の腕のブツブツも気がついたら無くなっていましたよ。友達にも同じ悩みを持っている子がいるので思わずその子にもおすすめしちゃいました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ひざ上" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">彼氏がなんと・・・体毛が全然ない人なんです。「オレより濃いよね」なんてデリカシーない一言に落ち込んでいました。これはもう脱毛サロンに通ってそんなこと言われない体になってやる！と決心。決めたのが銀座カラー。色々探したんですが銀座カラーを見つける前にチェックしたサロンは予約が取りにくいとか色々マイナスポイントをたくさん聞いて、金額よりも質と時間をムダにしない有効的な感じで通える銀座カラーに決めました。足全セットでお願いしました。最初はちょっと痛みを感じてしまい我慢してたところ、遠慮無く不安なこと言ってくださいね、といってくれる店員さんの言葉がとにかく優しくて。いたわってもらってると感じることが多かったです。緊張していたから敏感になっていただけ？このまま他の箇所も全部やってしまおうかとな～？と考え中です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ひざ上" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">息子がひざに座ってくるのですが、ある日ショートパンツだった私に突然「ママの足ザラザラする！痛～い。」と大きな声で言ってきました。子供にそういう指摘をされたことがなく（本人的には、素直に思ったことを言っただけだと思いますが）とてもショックな出来事でした。夫にそういうことがあったという報告をしたら「脱毛すればいいんじゃない？」と嬉しい一言。
早速銀座カラーにカウンセリングに行きました。前にVゾーンの脱毛で通っていたことがあるので特に大きな不安もなく始めました。久々でしたがさらっと終わりました。1年後の今は息子も「ママの足、ツルツルしてるねー」と言ってくれるようになりました。育児と仕事の合間の自分メンテナンス、脱毛もできたしストレスからも解放されて最高でした！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ひざ上" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">わたしはニーハイが大好きなのでかなり履いてます。悩みなのがわたしの脚はもともと毛穴が目立つんですよね。だから可愛いニーハイを履いても太ももの見えてる部分は毛穴が目立って可愛くない！もちろん毛もまあまあ濃くて、ニーハイを履くごとに毛の処理をしてると肌がブツブツになりかねないかも…。不安になったのでいっそのこと脱毛してみようと思ったのでした。ひざ上だけなら脱毛時間も20分ぐらいでさっと終了するのでバイト前に入れても全然問題ないです。太ももの後ろ側って自分で剃るとき結構見えないんで剃り残しもあったと思うんですが、今ではお手入れいらずまで毛が薄くなりました。毛穴もちょっと目立たなくなったみたいで満足です！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ひざ上" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">今年は念願の初ハワイ！水着になったり、短めなショートパンツで街を歩いたり、今から楽しみなんですが、むだ毛があっては楽しさ半減。他の箇所はだいたい脱毛しているんですけど、太ももというかひざ上はまだ手つかず。この機会に全部やってしまおう！と、そのまま通っていた銀座カラーの仲の良い店員さんに相談してやってもらうことにしました。個人的には銀座カラーさんでの脱毛は痛いのを全然感じないので、今回もすごく楽にできました。すべすべして気持ちいいので、ぼーっとしてる時につい触っちゃいます。店員さんにも「良い仕上がりです」と褒められました！私の毛の周期は3ヶ月毎に次の予約を入れる位がちょうど良かったので、美容院みたいな感覚で通っていました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="顔(鼻下除く)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">顔の産毛が濃くて毎回剃るのが面倒になり、顔の脱毛にチャレンジしました！！！
今までにワキや脚の脱毛は経験済みでしたが、顔はたまにニキビができたりするので、脱毛で肌荒れしたりしないかな？と正直ちょっと心配でした。なので最初の無料カウンセリングの時にスタッフの方に色々質問したのですが、すごく丁寧に説明してもらえたので安心できました(^o^)_
実際、施術後にしっかり保湿ケアしてもらえたおかげで、肌トラブルは一度もありませんでした。今ではおでこも頬もツルッツルになり、顔色も明るくなったような気が...。剃っているときは乾燥肌だったのですが、最近は肌の調子も良くてお化粧ノリが違います。
学生なら学割特典もあるので、学生のうちにやっておくのをオススメします！私も次はどの箇所をやろうかと考え中です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="顔(鼻下除く)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">私は元々とても毛深くて、中学校の時は友達にもびっくりされるほどでした...。夜に自己処理しても次の日の朝にはチクチクしてて...すごく悩んでたんです。これまではカミソリはもちろん脱毛クリームなどで頑張ってきたんですが、カミソリ負けしたりすることも多く、思い切って初めて脱毛サロンに行くことにしました。 基本的にほとんどの毛が濃いので、まずは人の目につきやすい顔と手の甲・指の脱毛コースに通っています。顔の脱毛は、、、ちょっと不安でいっぱいでしたが全然そんなことなくって。これからのことを考えれば全然大丈夫です◎。サロンは清潔感もあって落ち着けるし、スタッフの方も優しくて施術中のおしゃべりも楽しくてあっという間に時間が過ぎます。ほかの箇所のことにも相談にのってもらってるので今後も頑張って通いたいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="顔(鼻下除く)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">最近、ファンデーションを塗った時にうぶ毛が気になるように。自宅でカミソリで剃った事もあったけど、やはりちょくちょく剃らないといけないのはとても面倒…。剃るようになってから生えてきたうぶ毛が以前より濃くなった気がしてたので、脱毛をしよう！と決めました。顔という事もあって肌に悪い影響があると嫌なので無料カウンセリングの時に相談したら、保湿効果が高い美肌潤美という保湿ケアをしてくれるというお話だったので、銀座カラーに決めました。顔は鼻下を除く部分との事だったので、鼻下と一緒に脱毛をしてもらったのですが、お顔全体が1トーン明るくなり、ファンデーションを塗らなくてもいいくらいすっぴんに自信が持てるようになりました。今まででは考えられない事です。顔の印象はとても大事だと思うので、顔の脱毛をして本当に良かったと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="顔(鼻下除く)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">私はネイリストをしているのですが、お客様との顔の距離が近いため、顔の産毛を見られているんじゃないかと、とても気になるようになりました。ネイルサロンに来るお客様は美意識も高く、脱毛の話をする機会もたくさんありました。その中で、銀座カラーで顔の脱毛をしたという方が何人もいらしたので、私も行ってみることにしました。ちょうど職場の近くにお店があったので、仕事帰りに通うことができ、とても便利でした。顔の脱毛は、フェイシャルエステに通っているような感覚でとても気持ちがよかったです。脱毛後の保湿ミストがさらに気持ちよく、肌が潤って、翌日にはとてもつるっと滑らかな肌になるのが嬉しかったです。脱毛＋保湿ケアのおかげで、脱毛が終わった今でもつるっと赤ちゃんのような素肌になりました。本当にありがとうございました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="ヒップ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">結構友達が脱毛していることは知ってたんですが、興味はあったものの足を踏み入れる気持ちにまではなっていませんでした。最近よく電車の中やCMとかで脱毛の広告を目にする機会が多くて、やってみようかな～って気持ちになりました。広告とかではそこまで高い料金でもなかったし（高いものもあるんでしょうが）、手が届かないほどではないなと。自由にお金が使えるのは今くらいだしな、と思い、通うことにしました。
施術箇所は前から気になってたヒップにしたんですが、触り心地が全然違うんですね。びっくりしました。あと2回の施術ですが、毎回効果が感じられて満足してます。
最初はすごく恥ずかしいのと怖さがありましたが、気さくなスタッフさんのおかげで緊張もほぐれて施術を受けれるようになりました。あと2回でどんな仕上がりになるか楽しみです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="ヒップ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">銀座カラーでVIO脱毛をしている時、エステティシャンの方にヒップの脱毛も結構人気があると聞き興味が出ました。Tバックを履いた時に鏡でお尻にむだ毛があることには気がついていました。見た瞬間「はっ！！！」みたいな。ヒップの下のあたりってビキニを履くと結構な確率で露出しちゃうんですよね。こうなったらVIOを完璧にしようと箇所の追加をしました。
VIOを経験しているのでヒップの脱毛の痛みなんて全く気になりません。あれ？もう終わり？って感じでした。
数回脱毛した後、なんだかヒップ下の黒ずみが薄くなってる？と思い脱毛中に聞いてみると、人によって差はあるけど、薄くなる場合があるようです。むだ毛目的で始めたのにヒップ全体が締まって美尻になり、追加脱毛大成功です。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="ヒップ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">Tバックを履きたいなと思った時にヒップの毛が気になり始めました。生えてるんじゃない！？と。決して毛深い方ではないのですが、お風呂やプールなどに入って濡れた時にはやはり気になっていたので、脱毛をすればもう気にしなくていいし、やりたいなと思いました。ヒップは自分ではよく見えない部分なので自己処理もできないし、プロにお任せした方が安心かなーて。以前こちらでVIOセットをした事があったので、安心してお願いできる銀座カラーにしました。エステティシャンの方が美容の知識が豊富で、施術中の会話も美容やメイクについてなどの話題で盛り上がり、毎回あっという間に終わっていました。ヒップの脱毛をして良かったのは、うぶ毛に悩まされることがなくなった事だけでなく、肌ざわりがとても良くなった事です。滑らかという言葉が、まさにピッタリです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="ヒップ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">お尻の産毛のせいでお尻が黒ずんで見えるのが悩みで、つるっとした白いお尻に憧れて、ヒップ脱毛をしました。やっていくうちに、ビキニラインからお尻の毛がけっこうはみ出て（汗）いることや、産毛だけでなくたまに濃い毛もあったことに気づきました。やり始めるといろいろ気になってしまいますね。自分ではうまく処理できないので、プロに頼んでよかったと思います。銀座カラー独自の保湿ケアのおかげで、脱毛後のお肌はなめらかでつるつるになりました。肌のザラザラ感もすっかりなくなり、お風呂でお尻を洗うときのつるつる感がたまりません。想像以上の綺麗なお尻になって感動です。次に買うビキニは、お尻が綺麗に見えるデザインにしようと思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="ヒップ奥" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">とても恥ずかしいのですが、肛門まわりに結構多めの毛が生えていました。お風呂に入ったときに興味本位で見てみて衝撃！
自己処理しようとしたのですがうまくいかず、数日経つとチクチクしてたまりませんでした。
はじめVライン脱毛を考えていましたが、VIOセットコースに変更。どうせなら全部なくそう、と決めました。 が、1回目はとにかく恥ずかしくて無言…（恥）。エステティシャンの方は毎度のことって感じで淡々と黙々と脱毛してくれるので個人的には楽です。自分で事前処理できない場所なので、シェーバー持ってけば手伝ってくれるそうですよ＾＾</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="ヒップ奥" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">普段目につかない部分なので完全に自己満足となりますが、どうしてもVIOの毛をなくしたくてカウンセリングを予約させていただきました。
もちろんデリケートな部分なので抵抗はありましたが、カウンセリングの時にしっかりと脱毛の方法について教えていただけました。
腕などと比べて範囲は狭いので痛さなどもさほど気にならなく、というか色々考える前にすぐ終わってしまいました。
やっぱり自己処理が難しい部分だったし、チクチクしなくなったし、普段の生活でいちいち自分でする必要がなくなったのは嬉しく思います。ただやっぱり他人にOラインを見られるのは少し恥ずかしいものですね。
</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="ヒップ奥" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">水着を着るのにVラインの脱毛をしたいなと思い無料カウンセリングへ行きました。話を聞いているうちに、どうせやるならVIOを一度に脱毛した方が後々楽だな～と思ったので、ヒップ奥（いわゆるOライン）の脱毛をしようと思いました。ヒップ奥は自分では見えないけど、触ると毛があるのはわかっていたし（衝撃）、自己処理できない場所なので脱毛して本当に良かったです。水着やTバックを着る時に全く気にしなくて良くなったので本当に楽です。Vラインを脱毛するなら、ヒップ奥までした方がその境目もわからなくなるのでいいと思いますよー。痛みに関しても、他と比べる事ができないのですが、きちんと6回通えたので、そこまで気になりませんでした。これでブラジリアンビキニも安心して着られます。水着の時にVIOラインを気にしている友人にも教えて紹介してあげよっかなと。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="ヒップ奥" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">はじめはVIOって何？と思いましたが、周りの友達は皆知ってて、数人は脱毛経験していました。
デリケートゾーンの毛を脱毛するなんて想像もしていませんでしたが、「オシャレしたいから毛は全部なくしたい」と友達が話しているのを聞き触発され、お友達紹介キャンペーンを利用して一緒に通うことにしました。脱毛自体初めてなのにいきなりのVIO脱毛。ドキドキしながらワンピースに着替えました。スタッフさんは手際よく準備を進めていましたが、私がかなり緊張しているのが見て取れたのか、優しく話しかけてくれました。とっっってもやさしかったです。バチッ！という音にビクついていましたが、10分もすればこんな感じなのかーと慣れました。Vの形については特に希望もなかったので全体的に毛が生えている範囲を小さく整えてもらいました。 まだまだ始めたばかりですが大人の女性に1歩前進した気がします。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ひざ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">ひざの毛穴が目立つことに悩んでいたので、今回チャレンジしました。
ひざは自己処理をしても剃り残しがあったり、毛穴がポツポツ黒ずんで見えるのが悩みでした…。でも、脱毛していくうちに毛が少なくなってくると、毛穴も目立たなくなるのを実感！ザラザラした触り心地だったのもツルッツルッに♪ひざを出すことにためらいがなくなり、好きなファッションを楽しんでいます。
あと、脱毛によりムダ毛がスルッと抜けるんですが、体育座りでその毛を抜くのがめちゃめちゃ楽しいです。あ！ここもう生えてこないぞっ！お！ここもだ！！なんて思いながらついつい探しちゃいました。 来年から就活も始まるので、気になる部分を早めにやっておいて正解でした。毛の周期に合わせて脱毛していくので、結構時間はがかかるため、時間の自由がきく学生のうちにやっておくのがオススメです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ひざ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">ひざ下は以前に脱毛していました。その時はひざもやってもらえるものだと思っていたのですが実際は別料金だったのでやめていました。
でも私は色白で毛深いので、ひざの毛も結構太く目立っていて自己処理をしていました。毛穴の黒さがどうしても嫌で、太ももも含めもう一度脱毛サロンへ行くことにしました。
以前通っていたサロンと違って、とても落ち着いた雰囲気だったのでちょっと場違いかな？年齢層高め？と思ったのですが、カウンセリングをしてくれたスタッフの方は私とそう変わらない年齢っぽかったのでホッとしました。ひざは脂肪があまりないので少し骨に響くというか痛みはありましたが、すぐ慣れました。4回ほどでひざ下とひざの境目がわからなくなってきたました。やっぱり自己処理とは全然違いますね～。残るは足の甲のみ。サンダルの季節までにはチャレンジします！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ひざ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">春になったらスカートをたくさん履きたいので、冬のうちから脱毛を始めようと思いました。足、特にひざはカーブをしていて骨っぽいし、カミソリで剃るのが恐い場所だったので悩みの種でした。怪我とか怖いし（；。；）そしてひざだけするのは境目が気になりそうだったので、足全で一気に足を脱毛して綺麗にしたいなと思い足全セットにしました。足全体を脱毛して綺麗になったので、スカートやショートパンツをどんどん履きたいと思うようになりました。エステティシャンの方も言っていましたが、ひざはやはり足の中でも一番自己処理をするのが危ないらしく、傷つけてしまう方もたくさんいるそうです。しかもひざは、色々な摩擦によって角質化していることも多いらしく、保湿ケアが何よりも大事だそう。なので保湿ケアを重視している銀座カラーにしてとても良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ひざ" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">いつもひざ下の毛を剃る時に、ひざの凹凸でひざ周りの毛が剃りにくく、剃り残しがあったり、ひざの皮膚を切ってしまって痛い思いをしたりすることがよくありました。もう、ひざ周りの毛には長年悩まされっぱなしでした。今回ひざ下の脱毛をしようと思った時に、ひざ周りも一緒にすることを勧められ、そうそう、ひざ周りがやっかいなのよね…と思い、一緒に契約しました。通ううちに、毛がなくなるだけではなく、ひざの黒ずみがどんどん美白になっていくのがわかりました。ひざ周りがこんなに綺麗になるとは想像していなかったので、なんだか得した気分です。人に見せたくなるような美脚になったので、流行りのスカートをたくさんはこうと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="乳輪周り" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">高校生くらいの時から乳輪のまわりに産毛が生えていて、なかなか人に相談できず、自分だけなのかなと悩んで自己処理をしていました。自己処理で傷つけてしまうこともあり、誰に相談していいのかわからずに過ごしていましたが、銀座カラーの無料カウンセリングでお話ししたらみんなあるとのことで安心しました。乳輪周り以外のデリケートな部分の毛の悩みも相談に乗ってもらい、本当に助かりました。こういった部分はほとんどの方が処理しているとのことでしたので、私もこの機会に全身脱毛をすることに。脱毛後の美肌潤美のミストにより施術後のお肌はしっとり、気になっていたプツプツもだんだん少なくなりお肌も綺麗になって大満足です。もっと早くに銀座カラーに相談に来ていればよかったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="乳輪周り" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 デザイナー<br></span>
            </dt>
            <dd itemprop="reviewBody">乳輪の周りに毛が生えて、、自己処理をしていたらどんどん毛が濃くなっていき、処理をしていないとチクチクして、これが一生続くのかと剃り始めたことをずっと後悔していました。電車の中の広告を見て、思い切って銀座カラーの無料カウンセリングに行きました。なかなか親や友人にも聞けなかったデリケートゾーンの悩みが相談できて、親身になって聞いてくださりとてもよかったです。痛みもなく、脱毛前後の美肌潤美というミストとローションにより、今では、毛がないどころか、肌もきれいになりました。誰に相談していいのかわからなくて、一人で悩んでいる人は、銀座カラーの無料カウンセリングにぜひ行って欲しいですね。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="乳輪周り" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">ムダ毛で気になっていたのは乳輪周りです。濃い毛が数本とうぶ毛のような細い毛が結構生えていました。見つけるたびに毛抜きで抜いていたのですが、毛抜きで抜いていると埋没する事もあると聞いたので怖いなと思うようになりました。うぶ毛などはカミソリで剃る事もありましたが、乳輪周りは皮膚が薄くカミソリの刺激で赤くなったりして良くないと知ってから、どうしようと思っていたんです。それで色々調べていくうちに、乳輪周りも簡単に脱毛する事ができると知り、他の箇所を脱毛するついでに一緒に脱毛することにしました。サロンの無料カウンセリングは、そういうデリケートな悩みも隠さず話せるので、きちんとした知識を教えてもらえ、脱毛するメリットなどもお聞きできたので安心して通うことができました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="乳輪周り" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">大学生になってサークルに入り、合宿や小旅行などでみんなで温泉に入る機会が増えました。もともと乳輪周りの産毛と、時々生えてくる太くて長い毛が気になっていたのですが、友達の綺麗な乳輪周りを見て、自分も見られているんだろうなと思ったらとても恥ずかしくなりました。。いろいろ調べてみて、銀座カラーさんで乳輪周りの脱毛ができると知り、カウンセリングに行ってみました。乳輪にカミソリをあてることには抵抗があったので、自分では太い毛を抜くぐらいしかしていなかったのですが、光脱毛で産毛まで綺麗になると聞いて通い始めました。全6回のコースにしましたが、数回通っただけでもかなりの効果を感じています。もう少しで夏の合宿がありますが、これからは温泉タイムも友達の目を気にせずに入れそうです。残りの回数でどれほど綺麗になるか楽しみです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="お腹" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">お腹周りの毛を特に気にしたことがなく、肉眼で見てもそれほど目立たないので、特に脱毛の必要はないなと思っていた箇所でした。でもVIOと足の脱毛をしたかったので、どうせやるなら全身脱毛で！と思いお願いしました。まず、脱毛後お腹周りの変化にびっくり！！！毛がないと思っていたお腹にもたくさんの産毛があったようで、脱毛することにより肌が白くなり皮膚のくすみがとれて、手触りがすべすべに☆
Vラインだけを脱毛するとその近辺の毛がなくなり、逆にお腹の毛が目立ってしまうことがあるそうだけど、境目なく全身くまなく脱毛してくれるのは銀座カラーだからこそ。銀座カラーで全身脱毛を行い、本当によかったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="お腹" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">小さな頃からお腹まわりに濃い毛が生えてきて、自分は病気なのではないかと悩んでいました。
母親も姉もお腹の毛が濃く、遺伝的なものなのかなと思っていました。でも、彼ができて彼より自分のお腹の毛が濃いのにびっくり…。慌ててカミソリや脱色剤で処理を行っていました。毎回の処理に気を使い、また、やはり自分は異常なのではないかと心配になり、我慢できなくなって銀座カラーのカウンセリングに行きました。
お腹の毛が濃いのは病気でなく、遺伝やホルモンの影響を受けやすいとアドバイスをもらい、脱毛を始めました。今ではお腹の毛がなくなり自分の体に自信が持てるようになりました。母や姉にも勧めています。ずっと悩んできた悩みが解決し、銀座カラーに感謝です（＾＾）</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="お腹" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">お腹のうぶ毛がとても気になったのは妊娠してからです。今までも気になっていたのですが、うぶ毛だし…と処理もしていなかった私。普段お腹を人に見せる機会もなかなかないですしね。
ところが、妊娠をして妊婦検診の時に毎回男性の先生にお腹を見せるのですが、見せる時はやはり恥ずかしくて。しかも妊娠をしてからお腹の毛も濃くなり、以前より気になるようになりました。さすがに妊娠中は脱毛に行けないので、出産後に脱毛をする事を決意しました。2人目を妊娠するまでに完了させたいと思い頑張って通いました。
その甲斐あって今ではお腹はツルツルですし、ビキニになるのも恥ずかしくなくなりました。（以前はビキニが着れず、ワンピースタイプばかりでした…。）こんな事ならもっと早くに脱毛すれば良かったなと思いました。これでもう誰に見られても恥ずかしくないです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="お腹" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 看護師<br></span>
            </dt>
            <dd itemprop="reviewBody">お腹の毛は以前から気になっていたのですが、人に見られるのはビキニを着た時ぐらいなのでその都度自分で剃っていました。でも、妊娠してからお腹周りの毛がどんどん濃くなっていき、出産後も薄くなることはありませんでした。このままではビキニを着れないなと思い、脱毛に行きたかったのですが、赤ちゃんを置いて長時間出かけることもできず、また、ホルモンバランスが元に戻る前に脱毛に行くのもどうかな？と思い、産後1年半が過ぎてからようやく通い始めました。私は第二子をすぐに妊娠しなかったので、全部通えて今ではすべすべでキレイなお腹になりましたが、第二子妊娠で脱毛を途中で断念した友人もいました。妊娠して毛が濃くなったという友人はたくさんいるので、学生さんや結婚前の方には、妊娠前に脱毛しておいた方がいいよ！と声を大にして言いたいです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="胸" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">肌が弱く、日焼け止めを毎日塗り続けるとデコルテの毛穴に日焼け止めが入り、毛穴が一層目立ってしまったり、赤く炎症を起こしてしまったりと、せっかくの夏なのに綺麗なデコルテでいられなくなり、デコルテを出したファッションを楽しめないでいました。
思い切って銀座カラーでデコルテを脱毛したら毛穴が目立たなくなり、美肌潤美というミストとローションでエステに行った後のように、すべすべしたくすみのない肌となり、赤く炎症を起こすこともなくなりました。おかげでおしゃれの幅も広がりました。仕事でパーティーに参加することもあるのですが、デコルテが出るドレスも完璧です。笑</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="胸" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 事務<br></span>
            </dt>
            <dd itemprop="reviewBody">普段は洋服で隠れることが多い部分なので、あまり気にしませんでしたが、大好きな彼ができ、すべすべの肌を目指していました。
胸の脱毛をするとムダ毛が一掃され、脱毛後の銀座カラーの美肌潤美というローションとミストにより、皮膚の水分量が上がり、弾力が増してきました。水着を着られる夏が待ち遠しいです。彼とプールとか海とか行きたい！
施術時に胸を出すことに抵抗もありましたが、手早く正確に処理をしてくれるので恥ずかしがっている暇もありません。施術のギリギリまでタオルで箇所を隠してくれますし、終わったらまたタオルをかけてくれますので露出している時間は割と短いです。胸脱毛を恥ずかしがっていましたが、もっと早くこのつるつるボディを手に入れればよかったなと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="胸" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">ネックレスの着画像を撮る機会があり、デコルテのうぶ毛が気になるようになりました。それまで気にしていなかったのですが、デコルテ部分だけを撮るとなるとやはり目立つものですね。写真を撮る時に気になってからカミソリで剃ってみたのですが、やはりその後に生えてきたうぶ毛が濃くなった気がしたので、これを機に脱毛に通って綺麗にしたいな、という気持ちが強くなりました。うぶ毛って、脱毛すると肌の色が白く感じるし、ずっと綺麗な肌でいられるので、思い切って脱毛に通って本当に良かったです。しかも銀座カラーオリジナルの保湿ケア、美肌潤美のローションやミストシャワーは保湿効果だけでなく、素肌を綺麗にしてくれるので、それもとても良かったです。これで、デコルテのあいた洋服も自信を持って着る事ができそうです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="胸" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 フリーター<br></span>
            </dt>
            <dd itemprop="reviewBody">私には胸毛というか、胸周りの産毛がけっこう生えているのが悩みでした。普段は胸の開いた服を着ないようにして隠していましたが、友達の結婚式に出席した時に胸の大きく開いたデザインのウエディングドレスを見て、私も着たいなと思うようになりました。自分が結婚する時に間に合うように、すぐに脱毛してしまいたいと思い、友達からの評判がよかった銀座カラーにカウンセリングに行きました。銀座カラーでは、脱毛と同時に保湿をしてくれて、お肌ケアと、美白も同時にできるとのことで、産毛に悩んでいたデコルテがどんどん白くて綺麗になっていきました。肌も潤うようになり、脱毛以上の効果を感じています。まだ少し先になりますが、結婚式では、絶対に胸の大きく開いたドレスを着たいと思います！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="えり足" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">人のうなじはよく見るのに、自分のうなじはあまり気にしたことがなく、美容院で髪をアップにセットしてもらって、後ろ姿を鏡で見せられた時にびっくり。
きれいなうなじを想像していましたが、無駄な毛がたくさん生えていて、左右の形も違う！！こんな姿を20年以上さらしていたのかと愕然としました。人の目に触れることが多い仕事なのに、、、
カウンセリングの時に、エステティシャンの方と希望の形を相談し、左右揃った「W型」にすることに決めました。首を細く長くきれいに見せる効果があるそうです。脱毛完了後、彼氏から「うなじに色気を感じるようになって、つい見とれてしまう」と褒めてもらえました。見える部分で女性らしさをアピールするのにうなじがこんなに有効だったんだとあらためて思いました。今年の夏は毎日髪をアップにして、横顔美人を目指します。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="えり足" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">ホール接客の仕事をしている為、平日はほぼ毎日着物で過ごしています。髪型もアップにしているので、えり足が毛深い事が気になっていました。
私はえり足の後れ毛がとても長く耳の後ろも毛深いので、希望の形にする為にカウンセリング時に写真などを見てもらいながら相談させていただきました。
ちょっとした後れ毛は色っぽくもあるので、良いバランスで残るようにお願いしました。
効果は3回くらいで実感できました。今までは髪型をセットした時に後れ毛からこぼれ落ちる毛をピンで留めたりと時間がかかっていたのですが、その時間がなくなったのは嬉しいですね。
着物は衣紋から背中上のほうの産毛も少し見えてしまうので、今は背中の脱毛も追加でお願いしています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="えり足" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">仕事柄、髪をアップにすることが多いのですが、えり足の毛がまとまらず気になっていました。浴衣などを着た時の色っぽいうなじというものにも憧れていたので、脱毛することにしました。えり足は自分では見えない場所ですし、普段自分で処理できる場所ではないため一度も処理をしたことがなく、脱毛の痛みが不安だったのですが、銀座カラーではIPL脱毛という光が毛根に直接作用する脱毛方法で、肌にやさしく痛みも少ないというお話を伺い、安心して脱毛することができました。個人差があるようですが、輪ゴムではじかれた程度の痛みですよと説明があり、実際に体験してみると、そんな感じでパチンと一瞬痛みを感じるくらいだったのでホッとしました。6回通ってとても綺麗になり、髪の毛をアップにする時もすっきり見えるので本当に良かったです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="えり足" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">学生時代、髪をアップにすることはあまりなく、自分のうなじなんて気にしたことはありませんでした。でも、就職して毎日髪をアップにするようになり、他のスタッフのうなじが気になるようになりました。産毛がたくさんある人は、アップにした時に襟足が綺麗に見えないと気づき、自分のうなじも綺麗じゃないことに気づきました。うなじがとても綺麗な先輩におもいきって相談してみると、脱毛で綺麗になるとのこと！いくつかのサロンでカウンセリングをしてみて、お店の方の雰囲気がよかった銀座カラーさんに通うことに決めました。スタッフさんも脱毛しているだけあって、悩みもわかってくれ、安心しておまかせすることができました。首周りの皮膚はとてもデリケートですが、痛みもなく、いつも滑らかで潤いのある肌に仕上げてくれました。今では、仕事以外でも髪をアップにすることが増えました！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="腰" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">ローライズのジーンズやパンツが流行り買って着てはみたものの、想像以上に腰から背中が出ててビックリ！私の腰回り大丈夫かな…？と鏡で見たら産毛がいっぱい(&gt;&lt;)
これじゃ恥ずかしくてローライズなんかはけない！と友達が通っていた脱毛サロンを調べたところ、腰、背中は範囲が広いから金額もお高く…で手が出せず。他のサロンをネットで探していたところ、銀座カラーならいけそう！と思ってカウンセリング予約してみました♪
金額もデリケートチョイスが6回13,000円と学生の私でも払える額だったし、施術時間も短いので学校とバイトの間に通えるのも魅力でした。
これで自信を持ってローライズにトライできます！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="腰" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 美容師<br></span>
            </dt>
            <dd itemprop="reviewBody">以前背中の脱毛はしてましたが範囲が広いので時間がかかって、腰回りまではしないで終わってました。
今回、全身ツルツルにしたいと思って、まだやっていない箇所をお願いしました。
全身脱毛とフリーチョイスでコースの選択に迷ったけど、以前施術した部分に中途半端に残っているむだ毛もあって、全身脱毛コースを選びました。
銀座カラーさんは金額面でも施術でも、私にはとても合ってました。肌はつるつるでキレイになるし、気になるむだ毛はなくなるし、大満足！
腰まわりなんて普段集中してケアをする部分ではないので、少し黒ずみというかくすみがあったのですが、今回の脱毛で肌の色が1トーン上がった気がします！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="腰" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">性格上、1回気になるととことん気になっちゃうのですが、ある日友人とのランチ後、店を出ようとした瞬間にしゃがみこんだ友人のジーンズから出た腰周りに、うっすらですが気になる毛を発見。 家に帰り鏡で自分の腰を恐る恐る見てみると…ありました。想像以上に濃いむだ毛が。夏に向けて、脱毛しようとしていたのでついでに腰もしてしまえ！と思い、腰の脱毛もお願いすることにしました。ネットで口コミを見てもうひとつのサロンと最終的に迷いましたが、普段は仕事でスケジュールが埋まっているので、予約が取りやすいと評判の銀座カラーにお願いすることにしました。特に痛みもなく、やっていただいてる時間もあっと言う間でした。私自身ローライズパンツは一本も持ってなかったのですが、せっかく腰をスッキリしたので一本購入しようかなと思いました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="腰" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 アパレル<br></span>
            </dt>
            <dd itemprop="reviewBody">元からそんなに毛が濃い方ではなかったので海外で評判の脱毛器でなんとかなってたのですが、そんな中での悩みは直接目視で安全にできない場所、手が届かない場所の脱毛でした。特に腰とかは凄く悩んでました。サロンでやってみようかなと思ったのですが、一番最初に「どうなんだろ？」と思ったのが、普段から腰痛などで敏感になっていて、サロンの機械で施術した場合、振動や刺激で痛くないかってことでした。カウンセリングの時に相談に乗ってもらい、様子をみつつ始めることにしましたが、実際のところ1回目の時点で腰の刺激などはまったくなかったので安心して脱毛しました。サラッと腰の脱毛がおわってるそんな感じで、とっても楽チンです。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="背中(上)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 受付<br></span>
            </dt>
            <dd itemprop="reviewBody">もともと全身毛深いので、小学生の頃から脱毛には興味がありました。まずは目立つ手足から脱毛を、と思いましたが、水着やキャミソールを着たとき、背中の濃い産毛が本当にイヤで暑い季節になると憂鬱でした。でも優先順位としては手足には勝てない、でもどうにかしたい濃い産毛...毎日脱毛の事を考えていました。大学生になった時にアルバイトも始めたので、意を決しカウンセリングを予約しました。
はじめは本当にこのむだ毛達がなくなるのだろうか...と疑心暗鬼でしたが、スタッフさんのお話を聞くうちに、私もむだ毛に悩まされない憧れの毎日が送れるかも！と前向きになり、すぐに施術の予約をしました。何回か通っていくうちに、あんなに濃かったむだ毛がどんどん薄くなっていき、同時にこれで私も堂々と好きな洋服が着れる！と毎日が楽しくなりました。
家族や友達からも「最近明るくなって良い感じだね」と言われるように。思い切って脱毛をして本当に良かったです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="背中(上)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">以前より自己処理を行っていましたが、日に日に濃くなってきている気がして、剃るのにも恐怖心が出ていました。
なんとか安くて効果的な脱毛サロンはないものか…とネットサーフィンを続け、数店舗のカウンセリングを受けに行き、銀座カラーさんに決めました。
カウンセリングでは丁寧に脱毛についての説明をしていただき、背中のニキビの改善も期待できるかも、など脱毛以外の効果まで丁寧に教えていただけとても好感をもったからです。
施術後、本当にニキビもできなくなり背中もツルッツル！数年前の憂鬱な自分が信じられないくらいです。水着もキャミソールも堂々と着られる肌を手に入れ、着られる服のバリエーションが増えて、本当に嬉しいです！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="背中(上)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 保育士<br></span>
            </dt>
            <dd itemprop="reviewBody">一時期、海外のセレブやモデルのファッションを真似して、背中の大きく開いたトップスが自分的にブームな時期がありました。
そんな時、親友に「背中って自分でチェックしてる？ちょっと目立つかも・・・」と言われました。その一言で、それまで着ていたトップスたちはクローゼットの中で仲良くお留守番を開始・・・。
でもせっかく集めたトップスたちをまったく着なくなるのももったいないし・・・と、背中の脱毛を決心しました。背中は普段自分ではまったく見えない部分なのでとても不安で、とにかく評判がよいサロンを探して友人にも聞いて銀座カラーさんにお世話になることにしました。施術後は、お留守番していたトップスたちを自信を持って着ています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="背中(上)" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">仕事、仕事、、、の毎日で自分磨きを怠ってからはや3年以上。仕事以外も楽しもうと思った矢先、友だちの紹介がきっかけで彼氏ができました。
怠ってた期間分、身体の色々な所が気になってしまって「この際だから徹底的にやろう！」と決意！自分には見えない部分も心の許した人には見えたりする部分かも、、、と背中の脱毛もすることにしました。実際通い始め、サロンの方も親身に相談に乗ってくださり、他愛もないお話をしたりと、楽しく時間を過ごす事ができたので毎回あっという間の施術でした。
何年も海には行っていませんでしたが、見られてもいいようなツルツルな背中に生まれ変わったので、ずっと夢だった海デートに行って自信をもってビキニを着ようと思います。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ひざ下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice3.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 営業<br></span>
            </dt>
            <dd itemprop="reviewBody">足のすね毛が濃くて、スカートがはけない…小学生の頃からの悩みでした。
この忌々しい毛をなんとかなくしたい！と市販の脱毛グッズを試しまくりました。が、結果、益々濃くなっている気がして、気がついたらクローゼットからスカートはなくなっていました。仕事でも困るので何としてでも変わりたいと思い、無料カウンセリングの予約をしました。銀座カラーさんでは痛みの少ないIPL脱毛という光脱毛だというお話を聞き、これなら私の濃い毛でも痛みが少なく施術してもらえるかも？と思い、足全セットをお願いしました。実際の施術では、パチッ！と一瞬痛みを感じるものの、それ以外は全く辛くありませんでした。サロンへ通う度に薄くなっていくむだ毛、6回の施術でほとんど気にならなくなりました。もっと早く行けば良かった！最近の私のクローゼットは、スカートばかりになっています。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ひざ下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice4.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">以前はひざ下にそこまで濃いむだ毛は生えていなく産毛レベルだったのですが、気になるので毎日剃っていました。ふと気づいたときにはもう、ぶつぶつの黒ごまのような毛穴のガサガサ肌に。
友人に相談したところ銀座カラーを勧められたので、通うことにしました。
足全セットで始めたところ、すぐに効果が実感できました。ぶつぶつ黒ごまもすっかりなくなり、6回が終わる頃にはほとんど気にならないくらいの産毛しかありませんでした。専用の保湿ケアでお肌もスベスベになり大満足！私が利用したセットの場合、足の甲も含む足全体が施術対象なのに月額コースで全て対応してもらえるので、とてもお得でした。その上私の場合は学生だったので、学割特典がついていて更にお得でした！</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ひざ下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice2.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">20代 接客業<br></span>
            </dt>
            <dd itemprop="reviewBody">今まで脱毛サロンというものにあまり縁がなく（興味がなく）カミソリで剃っていたり、脱毛シートなどでなんとなくごまかしてきました。
年々それを繰り返してきた為か、あるときひざ下を触ったら指先に「ザラッ」とした感触を感じたのです。これはちょっとキツイかも…と思い友達にどうしているか相談しました。
友達はいくつか脱毛サロンに通っていて（3回位お店を変えたと言ってました）銀座カラーに落ち着いたということだったので私も試してみようと思いました。友達もひざ下の黒ずみを気にしていて、そのまま処理していくのは大変そう、と思って脱毛を始めたようで、私もひざ下からはじめることにしました。アフターケアをどうしていいかわからずどうしようかと思ってましたが、担当していただいた方から丁寧な説明もあり、とても助かりました。</dd>
          </dl>

 <dl itemscope="" itemtype="http://schema.org/Review" itemprop="review" data-salon="両ひざ下" style="display: none;">
            <dt itemprop="author" itemscope="" itemtype="http://schema.org/Person">

<img src="<?php bloginfo('template_url'); ?>/resource/img/common/img-voice1.png" alt="" width="78" height="78" itemprop="image">


            <span itemprop="name">10代 学生<br></span>
            </dt>
            <dd itemprop="reviewBody">足は基本的に自己処理していたので、埋もれ毛みたくなってしまい、その後医療脱毛を検討していたのですが、痛みが怖くて断念しちゃいました。それから色々調べていくうちに銀座カラーの脱毛は痛みが少ないとか、痛いと思うのは最初だけとか（個人差は、あると思いますが）色々なところで聞いたので思いきって通うことに。契約前の説明もとてもしっかりしていて余計な勧誘とかもなく、大事にされているというか親身になって色々と相談に乗っていただけました。とにかくポツポツと黒ずみが目立ってるひざ下をどうにかしたい！夏にはショートパンツを履きたい一心でサロンの方に相談しました。「予約が取りにくい」みたいなよく聞く声も忘れるくらい取りやすいです。そしてなにより痛みがなかったです（個人的には）楽に気軽に美容院感覚で通えるので、オススメです！☆</dd>
          </dl>


            <div class="block-btn-stripe-lower">
              <a href="#" class="btn-stripe btn-wide btn-icon-plus">もっとみる</a>
            </div>
          </div>

        <!-- /#part --></div>

      <!-- /.block-tab --></div>
      <!-- /.block-white --></div>
<section class="block-voice salon block-white">
  <h2 class="title-red">サロンについてのお客様の声</h2>
    <section class="block-arrow-list accordion">
          <h3 class="block-arrow-list-title with-arrow-down js-accordion-trigger">北海道エリア</h3>
          <div style="display: none;">
            <dl>
              <dt>北海道</dt>

            <dd><a href="<?php bloginfo('url'); ?>/salon/sapporo/voice" class="link">札幌店</a></dd>

            </dl>
          </div>
        </section>
    <!-- end 北海道エリア -->

    <section class="block-arrow-list accordion">
          <h3 class="block-arrow-list-title with-arrow-down js-accordion-trigger">東北エリア</h3>
          <div style="display: none;">
            <dl>
              <dt>岩手県</dt>

            <dd><a href="<?php bloginfo('url'); ?>/salon/morioka-saien/voice" class="link">盛岡菜園店</a></dd>

            </dl>
            <dl>
              <dt>宮城県</dt>

            <dd><a href="<?php bloginfo('url'); ?>/salon/sendai/voice" class="link">仙台店</a></dd>

            </dl>
          </div>
        </section>
    <!-- end 東北エリア -->

    <section class="block-arrow-list accordion">
          <h3 class="block-arrow-list-title with-arrow-down js-accordion-trigger">関東エリア</h3>
          <div style="display: none;">
            <dl>
              <dt>東京都</dt>
    <dd><a href="<?php bloginfo('url'); ?>/salon/ginza-honten/voice" class="link">銀座本店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/ginza-premia/voice" class="link">銀座プレミア店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/shinjuku-higashi/voice" class="link">新宿東口店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/shinjuku/voice" class="link">新宿店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/shinjuku-nishi/voice" class="link">新宿西口店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/shibuya109/voice" class="link">渋谷109前店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/shibuya-dogenzaka/voice" class="link">渋谷道玄坂店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/omotesando/voice" class="link">表参道店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/ikebukuro/voice" class="link">池袋店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/ikebukuro-st/voice" class="link">池袋サンシャイン通り店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/ueno-koen/voice" class="link">上野公園前店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/kinshicho/voice" class="link">錦糸町店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/kitasenju/voice" class="link">北千住店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/kitasenju-2nd/voice" class="link">北千住2nd店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/kichijoji-n/voice" class="link">吉祥寺北口店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/tachikawa/voice" class="link">立川店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/tachikawa-kita/voice" class="link">立川北口店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/hachioji/voice" class="link">八王子店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/machida-modi/voice" class="link">町田モディ店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/aeta-machida/voice" class="link">AETA町田店</a></dd>
     </dl>
            <dl>
              <dt>埼玉県</dt>
    <dd><a href="<?php bloginfo('url'); ?>/salon/omiya/voice" class="link">大宮店</a></dd><dd><a href="<?php bloginfo('url'); ?>/salon/kawagoe/voice" class="link">川越駅前店</a></dd>
     </dl>
            <dl>
              <dt>千葉県</dt>
    <dd><a href="<?php bloginfo('url'); ?>/salon/chiba/voice" class="link">千葉店</a></dd><dd><a href="<?php bloginfo('url'); ?>/salon/chiba-funabashi/voice" class="link">千葉船橋店</a></dd><dd><a href="<?php bloginfo('url'); ?>/salon/funabashi-kita/voice" class="link">船橋北口店</a></dd><dd><a href="<?php bloginfo('url'); ?>/salon/kashiwa/voice" class="link">柏店</a></dd>
      <dl>
              <dt>神奈川県</dt>
    <!--  <dd><a href="<?php bloginfo('url'); ?>/salon/yokohama/voice" class="link">横浜店</a></dd> -->
    <dd><a href="<?php bloginfo('url'); ?>/salon/yokohama-est/voice" class="link">横浜エスト店</a></dd><dd><a href="<?php bloginfo('url'); ?>/salon/yokohama-nishi/voice" class="link">横浜西口店</a></dd><dd><a href="<?php bloginfo('url'); ?>/salon/kawasaki/voice" class="link">川崎店</a></dd><dd><a href="<?php bloginfo('url'); ?>/salon/fujisawa/voice" class="link">藤沢店</a></dd>
     <dl>
              <dt>栃木県</dt>
    <dd><a href="<?php bloginfo('url'); ?>/salon/utunomiya/voice" class="link">宇都宮店</a></dd>
            </dl>
     <dl>
              <dt>茨城県</dt>
    <dd><a href="<?php bloginfo('url'); ?>/salon/tsukuba/voice" class="link">つくば店</a></dd>
    <dd><a href="<?php bloginfo('url'); ?>/salon/mito-ekimae/voice" class="link">水戸駅前店</a></dd>
            </dl>

            <dl>
                     <dt>群馬県</dt>
           <dd><a href="<?php bloginfo('url'); ?>/salon/takasaki/voice" class="link">高崎店</a></dd>
                   </dl>

          </dl></dl></div>
        </section>
        <!-- end 関東エリア -->

    <section class="block-arrow-list accordion">
          <h3 class="block-arrow-list-title with-arrow-down js-accordion-trigger">甲信越エリア</h3>
          <div style="display: none;">
            <dl>
              <dt>新潟県</dt>
    <dd><a href="<?php bloginfo('url'); ?>/salon/nigata-bandai/voice" class="link">新潟万代シテイ店</a></dd>
                    </dl>
          </div>
        </section>
        <!-- end 北陸エリア -->

    <section class="block-arrow-list accordion">
          <h3 class="block-arrow-list-title with-arrow-down js-accordion-trigger">北陸エリア</h3>
          <div style="display: none;">
            <dl>
              <dt>石川県</dt>
    <dd><a href="<?php bloginfo('url'); ?>/salon/polte-kanazawa/voice" class="link">金沢駅前東店</a></dd>
                    </dl>
          </div>
        </section>
        <!-- end 北陸エリア -->

        <section class="block-arrow-list accordion">
          <h3 class="block-arrow-list-title with-arrow-down js-accordion-trigger">東海エリア</h3>
          <div style="display: none;">
            <dl>
              <dt>静岡県</dt>
    <dd><a href="<?php bloginfo('url'); ?>/salon/shizuoka/voice" class="link">静岡店</a></dd>
      </dl>
      <dl>
              <dt>岐阜県</dt>
    <dd><a href="<?php bloginfo('url'); ?>/salon/gifu-kandamachi/voice" class="link">名鉄岐阜駅前店</a></dd>
      </dl>
            <dl>
              <dt>愛知県</dt>
    <dd><a href="<?php bloginfo('url'); ?>/salon/nagoya-sakae/voice" class="link">名古屋栄店</a></dd><dd><a href="<?php bloginfo('url'); ?>/salon/nagoya-ekimae/voice" class="link">名古屋駅前店</a></dd><dd><a href="<?php bloginfo('url'); ?>/salon/kanayama/voice" class="link">金山店</a></dd>
     </dl>
          </div>
        </section>
        <!-- end 東海エリア -->

        <section class="block-arrow-list accordion">
          <h3 class="block-arrow-list-title with-arrow-down js-accordion-trigger">近畿エリア</h3>
          <div style="display: none;">
            <dl>
              <dt>大阪府</dt>
    <dd><a href="<?php bloginfo('url'); ?>/salon/osaka-umeda/voice" class="link">梅田茶屋町店</a></dd><dd><a href="<?php bloginfo('url'); ?>/salon/osaka-umedasin/voice" class="link">梅田新道店</a></dd><dd><a href="<?php bloginfo('url'); ?>/salon/shinsaibashi/voice" class="link">心斎橋店</a></dd><dd><a href="<?php bloginfo('url'); ?>/salon/tennoji/voice" class="link">天王寺店</a></dd>
      </dl>
            <dl>
              <dt>兵庫県</dt>
    <dd><a href="<?php bloginfo('url'); ?>/salon/kobe-motomachi/voice" class="link">神戸元町店</a></dd><dd><a href="<?php bloginfo('url'); ?>/salon/kobe-sannomiya/voice" class="link">三宮店</a></dd>
       </dl>
          </div>
        </section>
        <!-- end 近畿エリア -->

    <section class="block-arrow-list accordion">
          <h3 class="block-arrow-list-title with-arrow-down js-accordion-trigger">中国エリア</h3>
          <div style="display: none;">
            <dl>
              <dt>広島県</dt>
    <dd><a href="<?php bloginfo('url'); ?>/salon/hiroshima/voice" class="link">広島店</a></dd>
                    </dl>
          </div>
        </section>
        <!-- end 中国エリア -->

    <section class="block-arrow-list accordion">
          <h3 class="block-arrow-list-title with-arrow-down js-accordion-trigger">九州エリア</h3>
          <div style="display: none;">
            <dl>
              <dt>福岡県</dt>
    <dd><a href="<?php bloginfo('url'); ?>/salon/tenjin/voice" class="link">天神店</a></dd>
                    </dl>
          </div>
        </section>
        <!-- end 九州エリア -->
  <!-- /.block-voice.salon --></section>



  <section class="block-voice course block-white">
      <h2 class="title-red">コースについてのお客様の声</h2>
      <ul>
        <li><a href="<?php bloginfo('url'); ?>/plan/course/zenshin/voice/" class="btn-stripe"><span>全身脱毛</span></a></li>
        <li><a href="<?php bloginfo('url'); ?>/plan/course/ude/voice" class="btn-stripe"><span>腕全セット</span></a></li>
        <li><a href="<?php bloginfo('url'); ?>/plan/course/ashi/voice" class="btn-stripe"><span>足全セット</span></a></li>
        <li><a href="<?php bloginfo('url'); ?>/plan/course/vio/voice" class="btn-stripe"><span>VIOセット</span></a></li>
        <li><a href="<?php bloginfo('url'); ?>/plan/course/new-zenshin/voice" class="btn-stripe"><span>新・全身脱毛</span></a></li>
        <li><a href="<?php bloginfo('url'); ?>/plan/course/half/voice" class="btn-stripe"><span>全身ハーフ脱毛</span></a></li>
      </ul>
            <p style="padding:10px 0;text-align:center;">※一部販売が終了したコースも含まれております。</p>
    <!-- /.block-voice.course --></section>



    <section class="block-voice region block-white">
        <h2 class="title-red">箇所についてのお客様の声</h2>
        <p class="baloon">ご覧になりたい箇所を選択してください。</p>

            <div class="block-region js-tab-region">

              <div class="js-tab-nav">
                <ul class="block-tab-region">
                  <li class="left-tab on"><a href="#front">正面</a></li>
                  <li class="right-tab"><a href="#back">背面</a></li>
                </ul>
              <!-- /.js-tab-nav --></div>

              <div class="js-tab-body" id="front">
                <div class="block-region-body block-plan-body clearfix">
                  <ul class="block-region-btns">
                  <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/face/voice"><span>顔<br>（鼻下除く）</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/under-nose/voice"><span>鼻下</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/ryowaki/voice"><span>両ワキ</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/areola/voice"><span>乳輪周り</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/belly/voice"><span>お腹</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/navel/voice"><span>へそ周り</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/shell-finger/voice"><span>両手の<br>甲・指</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/on-vline/voice"><span>Vライン<br>( 上部 )</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/vline-i/voice"><span>Vライン<br>( iライン )</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/shell-feet/voice"><span>両足の<br>甲・指</span></a></li>
                  </ul>
                  <ul class="block-region-btns region-right">
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/sideburns/voice"><span>もみあげ</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/under-mouth/voice"><span>口下</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/breast/voice"><span>胸</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/on-elbow/voice"><span>両ヒジ上</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/under-elbow/voice"><span>両ヒジ下</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/navel-under/voice"><span>へそ下</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/vline-s/voice"><span>Vライン<br>( サイド )</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/on-knees/voice"><span>両ひざ上</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/knees/voice"><span>両ひざ</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/under-knees/voice"><span>両ひざ下</span></a></li>
                  </ul>
                </div>
              <!-- /#front --></div>

              <div class="js-tab-body" id="back" style="display: none;">
                <div class="block-region-body block-plan-body clearfix">
                  <ul class="block-region-btns">
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/collar-leg/voice"><span>えり足</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/on-elbow/voice"><span>両ヒジ上</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/under-elbow/voice"><span>両ヒジ下</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/hips/voice"><span>ヒップ</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/hips-back/voice"><span>ヒップ奥</span></a></li>
                  </ul>
                  <ul class="block-region-btns region-right">
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/on-back/voice"><span>背中上</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/under-back/voice"><span>背中下</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/waist/voice"><span>腰</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/on-knees/voice"><span>両ひざ上</span></a></li>
                    <li class="btn-stripe"><a href="<?php bloginfo('url'); ?>/plan/part/under-knees/voice"><span>両ひざ下</span></a></li>
                  </ul>
                </div>
              <!-- /#back --></div>

            <!-- /.js-tab-region --></div>

      <!-- /.block-voice.region --></section>




      <?php
        if( !get_field('sp_new_cust_banner') ){
          get_template_part('tmp_course_set_list');
        };
       ?>
         <div class="block-btn">
            <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0047" class="btn-stripe btn-counsel btn-large btn-shadow" target="_blank">
              <span>無料カウンセリング</span><br>ご予約はこちら
             </a>
           <!-- /.block-btn --></div>
    </div>
  </div>
<?php get_template_part('footer'); ?>
