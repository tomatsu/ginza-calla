<link rel="stylesheet" href="https://fonts.googleapis.com/earlyaccess/hannari.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?191227=Amiri|Sawarabi+Mincho|Noto+Serif+JP:200,300,400,500,600,700,900&display=swap">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?191227=Amiri">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/reset.css?191227">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/parts.css?191227">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/module.css?191227">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/layout.css?191227">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/migrate-plugin.css?191227">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/migrate-layout.css?191227">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/migrate-page.css?191227">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/migrate/css/layout.css?1912272">
<!-- End Common Stylesheet -->
<?php if(get_field('sp_import_css01')){ ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/<?php the_field('sp_import_css01'); ?>.css?191227">
<?php }; ?>
<?php if(get_field('sp_import_css02')){ ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/<?php the_field('sp_import_css02'); ?>.css?191227">
<?php }; ?>
<?php if(get_field('sp_import_css03')){ ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/<?php the_field('sp_import_css03'); ?>.css?191227">
<?php }; ?>


<?php if(is_home()){ ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/migrate-index.css?191227">
<?php }; ?>

<?php if(is_page_group('about')){ ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/about.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/anshin.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/counsel.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/research.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/jishin.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/reservation.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/migrate/css/popup.css?191227">
<?php }; ?>

<?php if(is_page_group('care')){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/salon.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/care.css?191227">
<?php }; ?>

<?php if(is_page_group('faq')){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/faq.css?191227">
<?php }; ?>

<?php if(is_page_group('salon') || is_page_group('area')){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/salon.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/salon-top.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/salon-area.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/salon-detail.css?191227">
<?php }; ?>

<?php if(is_page_group('campaign')){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/campaign.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/campaign-payment.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/campaign-shokai.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/campaign-under20.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/campaign-waribiki.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/campaign-otameshi.css?191227">
<?php }; ?>

<?php if(is_page_group('plan')){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/plan.css?191227">
<?php }; ?>

<?php if(is_page_group('course')){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/plan.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/plan-course.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/plan-course-premium.css?191227">
  <?php if(is_page('vio')){ ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/plan-course-vio.css?191227">
  <?php }; ?>
<?php }; ?>

<?php if(is_page_group('part')){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/plan.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/plan-part.css?191227">
<?php }; ?>

<?php if(is_page_group('company')){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/company.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/company-guide.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/company-sitemap.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/company-history.css?191227">
<?php }; ?>

<?php if(is_page_group('gakuwari') || is_page_group('report')){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/gakuwari.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/gakuwari-index.css?191227">
<?php }; ?>

<?php if(is_page_group('product')){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/product.css?191227">
<?php }; ?>

<?php if(is_page_group('ranking')){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/ranking.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/plan.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/plan-course.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/plan-part.css?191227">
<?php }; ?>

<?php if(is_page_group('selfcheck')){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/jquery.ad-gallery.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/selfcheck.css?191227">
<?php }; ?>

<?php if(is_page_group('voice')){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/plan.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/voice.css?191227">
<?php }; ?>

<?php if(is_category('column') || in_category('column') || is_category('contents') || is_category('newcontents') || in_category('contents') || in_category_child( get_term_by( 'slug', 'column', 'category' )) || in_category_child( get_term_by( 'slug', 'contents', 'category' ))){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/column.css?191227">
<?php }; ?>
<?php if(is_category('contents') || is_category('newcontents') || in_category('contents') || in_category_child( get_term_by( 'slug', 'contents', 'category' ))){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/contents.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/contents-post.css?191227">
<?php }; ?>

<?php if(is_category('news') || in_category('news') || in_category_child( get_term_by( 'slug', 'news', 'category' ))){  ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/salon.css?191227">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/news.css?191227">
<?php }; ?>

<?php if(is_404()){ ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/resource/css/error.css?191227">
<?php }; ?>
<!-- End Partial Stylesheet -->
