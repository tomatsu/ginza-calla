<?php
/*
Template Name: サロンページ用テンプレート
*/
?>
<?php get_template_part('header'); ?>
    <div class="main">
      <h1 class="page-head" id="shopName"><span itemprop="name"><?php the_title(); ?></span></h1>
        <div class="page-body">
          <div class="block-white" itemscope itemtype="http://schema.org/BeautySalon" itemref="shopName">
		      <p class="text"><?php the_field('shop_text'); ?></p>
  			<div class="shop-info">
          <ul class="shop-image js-slider">
            <li><img src="<?php bloginfo('template_url') ?>/resource/<?php the_field('sp_shop_image1'); ?>" alt="" ></li>
            <li><img src="<?php bloginfo('template_url') ?>/resource/<?php the_field('sp_shop_image2'); ?>" alt="" ></li>
            <li><img src="<?php bloginfo('template_url') ?>/resource/<?php the_field('sp_shop_image3'); ?>" alt="" ></li>
          </ul>
  				<dl class="shop-info-data">
  					<dt>【住所】</dt>
  					<dd itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><?php the_field('address'); ?></dd>
  					<dt>【営業時間】</dt>
  					<dd><span itemprop="openingHours" ><?php the_field('open_time'); ?></span><br/></dd>
  					<dt>【ベッド数】</dt>
  					<dd><?php the_field('bed'); ?>台</dd>
  					<dt>【駐車場】</dt>
  					<?php if (get_field('parking')){ ?>
              <?php if (get_field('parking_text') == ""){ ?>
  						<dd>有</dd>
              <?php }else{ ?>
              <dd><?php the_field('parking_text'); ?></dd>
              <?php }; ?>
  					<?php }else{ ?>
  						<dd>無</dd>
  					<?php }; ?>
  				<!-- /.shop-info-data --></dl>
  			<!-- /.shop-info --></div>

  			<div class="shop-access">
          <div class="block-map-rect">
            <div id="map-canvas" itemprop="hasMap" itemscope itemtype="http://schema.org/MAP"></div>
          </div>
    			<div class="shop-map-blln">
    		  	<p><?php the_field('map_text'); ?></p>
    		  <!-- /.shop-map-blln --></div>

          <?php
          $arrays = get_field('near_shop');
          if($arrays){
          ?>
          <div class="mod-neighborfood ">
            <h2 class="icon-mail">周辺の店鋪</h2>
            <p class="text">
              <?php
                  $tmp = $arrays;
                  foreach ($arrays as $array){
                    $pageinfo = get_page_by_title($array);
              ?>
                <?php if(next($tmp)){ ?>
                    <a href="<?php bloginfo('url') ?>/salon/<?php echo $pageinfo->post_name; ?>" class="trans">
                       <?php echo $array; ?>
                    </a>/
                <?php }else{ ?>
                    <a href="<?php bloginfo('url') ?>/salon/<?php echo $pageinfo->post_name; ?>" class="trans">
                       <?php echo $array; ?>
                    </a>
                <?php	} ?>
              <?php }; ?>
            </p>
          </div>
            <?php if (get_field('parent_area')) { ?>
              <?php
                $field = get_field_object('parent_area_name');
                $value = get_field('parent_area_name');
             ?>
              <div class="block-btn-stripe-lower">
                <a href="<?php bloginfo('url') ?>/salon/area/<?php the_field('parent_area_name') ?>" class="btn-stripe"><?php echo $field["choices"][$value]; ?>周辺の店舗マップ</a>
              </div>
            <?php }; ?>
          <?php }; ?>

  		  <!-- /.shop-access --></div>


        </div><!-- /.page-body -->
      </div><!-- /.block-white -->

      <?php if( !get_field('free_banner') ){ ?>
        <div class="block-btn">
           <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0047" class="btn-stripe btn-counsel btn-large btn-shadow" target="_blank">
             <span>無料カウンセリング</span><br>ご予約はこちら
            </a>
          <!-- /.block-btn --></div>
      <?php }; ?>

      <?php if(get_field('message')){ ?>
      <section class="mod-message">
        <div class="mod-message-body">
          <h2 class="icon-mail">サロンからのメッセージ</h2>
          <p><?php the_field('message'); ?></p>
        <!-- /.mod-message-body --></div>
      <!-- /.mod-message --></section>
      <?php }; ?>
      <?php if(get_field('blog_url')){ ?>
      <ul class="mod-btns">
        <?php if(get_field('blog_url_br')){ ?>
          <li><a href="<?php the_field('blog_url'); ?>" target=”_blank” class="btn-stripe trans"><?php the_title(); ?>の<br>スタッフブログ</a></li>
        <?php }else{ ?>
          <li><a href="<?php the_field('blog_url'); ?>" target=”_blank” class="btn-stripe trans"><?php the_title(); ?>のスタッフブログ</a></li>
        <?php }; ?>
      </ul>
      <?php }; ?>

         <?php
           if (have_posts()) : while (have_posts()) : the_post();
           if(get_the_content() != ""){
         ?>
         <section class="mod-voice">
           <div class="mod-voice-body" itemscope itemtype="http://schema.org/BeautySalon">
             <h2 class="icon-voice"><span itemprop="name"><?php the_title(); ?></span>のお客様の声</h2>
             <div class="block-voice-list">
               <?php
                 if(get_field('sp_page') == ""){
                     the_content();
                 }else{
               ?>
                 <?php echo apply_filters('the_content', get_post_meta($post->ID, 'sp_page', true)); ?>
              　<?php }; ?>
             </div>
             <div class="block-btn-stripe-lower">
              <a href="<?php the_permalink(); ?>/voice" class="btn-stripe btn-wide">その他の声を見る</a>
             </div>
            </div>
         <!-- /.mod-voice --></section>
         <?php
           };
           endwhile; endif;
          ?>

     <?php
       if( !get_field('sp_new_cust_banner') ){
         get_template_part('tmp_course_set_list');
       };
      ?>
      <?php if( !get_field('free_banner') ){ ?>
      <div class="mod-btn-counsel">
        <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0047" class="btn-stripe btn-counsel btn-large btn-shadow" target="_blank"><span>無料カウンセリング</span><br>ご予約はこちら</a>
      </div> <!-- /.mod-btn-counsel -->
      <?php }; ?>
    </div><!-- /.main -->

<?php get_template_part('footer'); ?>
