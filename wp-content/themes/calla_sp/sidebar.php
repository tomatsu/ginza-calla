<div class="sidebar">
  <?php
    $parentname = get_post($post->post_parent)->post_name;

    $get_news_id = get_page_by_path("news")->ID;
    $get_salon_id = get_page_by_path("salon")->ID;
    if (is_page()){
      get_template_part('sidebar','page');
    };
    if (in_array($get_salon_id, get_post_ancestors($post->ID))){
      get_template_part('sidebar','salon');
    };
    if (is_category() || is_single()){
      get_template_part('sidebar','category');
    };
    if(is_category('contents') || in_category('contents') || in_category_child( get_term_by( 'slug', 'contents', 'category' )) && !in_category('news') ){
      get_template_part('sidebar','contents');
    };

  ?>
  <?php if(!get_field('sidebar_a') || is_home()){ ?>
    <?php if(is_category(array('news','2013','2014','2015','2016'))){}else{ ?>
      <ul class="sidebar-block">
        <li>
          <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0047" class="btn-stripe btn-counsel btn-large btn-shadow trans" target="_blank">
            <span>無料カウンセリング</span><br>ご予約はこちら
          </a>
        </li>
        <li><a href="<?php bloginfo('url'); ?>/salon/" class="btn-stripe btn-large btn-shadow btn-mdl trans">お近くのサロン<br />を探す</a></li>
        <li><a href="<?php bloginfo('url'); ?>/salon/new-open/" class="btn trans">出店予定店舗</a></li>
      </ul>
    <?php }; ?>
  <?php }; ?>
  <!-- end サイドバー ボタン -->
  <?php if(!get_field('sidebar_b') || is_home()){ ?>
    <dl class="sidebar-block sidebar-campaign">
      <dt>キャンペーン</dt>
      <dd>
        <a href="<?php bloginfo('url'); ?>/gakuwari/" class="trans">
          <img src="<?php bloginfo('template_url'); ?>/resource/img/common/bnr-gakuwari.png" alt="脱毛学割 DATSUMOU GAKUWARI" width="229" height="130">
        </a>
      </dd>
      <dd>
        <a href="<?php bloginfo('url'); ?>/campaign/shokai.html" class="trans">
          <img src="<?php bloginfo('template_url'); ?>/resource/img/common/bnr-introduce.png" alt="紹介専用　無料カウンセリング　予約はこちら" width="229" height="105">
        </a>
      </dd>
    </dl>
  <?php }; ?>
  <!-- end サイドバー キャンペーン情報 -->
  <?php if(!get_field('sidebar_c')){ ?>
    <?php if(is_category(array('news','2013','2014','2015','2016')) || is_category(array('column','01','02','03','04','05')) || in_category(array('column','01','02','03','04','05')) || is_page('voice')){}else{ ?>
      <!-- <ul class="sidebar-block">
      </ul> -->
      <!-- end サイドバー CM特設サイト -->
    <?php }; ?>
  <?php }; ?>
  <?php if(!get_field('sidebar_d') || is_home()){ ?>
    <?php if(is_category(array('news','2013','2014','2015','2016')) || is_category(array('column','01','02','03','04','05')) || in_category(array('column','01','02','03','04','05')) || is_page('voice')){ ?>
      <?php if(is_home()){ ?>
        <ul class="sidebar-block">
          <li>
            <a href="<?php bloginfo('url'); ?>/contents/" class="trans">
              <img src="<?php bloginfo('template_url'); ?>/resource/img/common/contents_bn_side.png" alt="脱毛に関するおすすめ情報" width="229" height="105">
            </a>
          </li>
          <li>
            <a href="<?php bloginfo('url'); ?>/selfcheck/" class="trans">
              <img src="<?php bloginfo('template_url'); ?>/resource/img/common/bnr-selfcheck.png" alt="セルフチェック" width="229" height="105">
            </a>
          </li>
           <li>
             <a href="<?php bloginfo('url'); ?>/column/" class="trans">
              <img src="<?php bloginfo('template_url'); ?>/resource/img/common/bnr-column.png" alt="脱毛や美容に関するコラム" width="229" height="105">
            </a>
          </li>
        </ul>
      <?php }; ?>
    <?php }else{ ?>
      <ul class="sidebar-block">
        <li>
          <a href="<?php bloginfo('url'); ?>/contents/" class="trans">
            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/contents_bn_side.png" alt="脱毛に関するおすすめ情報" width="229" height="105">
          </a>
        </li>
        <li>
          <a href="<?php bloginfo('url'); ?>/selfcheck/" class="trans">
            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/bnr-selfcheck.png" alt="セルフチェック" width="229" height="105">
          </a>
        </li>
         <li>
           <a href="<?php bloginfo('url'); ?>/column/" class="trans">
            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/bnr-column.png" alt="脱毛や美容に関するコラム" width="229" height="105">
          </a>
        </li>
      </ul>
    <?php }; ?>
  <?php }; ?>
  <!-- end サイドバー セルフチェック -->
  <?php if(!get_field('sidebar_e') || is_home()){ ?>
    <?php if(is_category(array('news','2013','2014','2015','2016'))){}else{ ?>
      <ul class="sidebar-block">
        <li>
          <a href="https://www.facebook.com/ginza.calla" class="trans" target="_blank" onclick="dataLayer.push({'event': 'FireEvent_ExternalLink', 'EventCatagory': 'PCサイト', 'EventAction': 'ExternalLink', 'EventLabel': 'ExternalLink｜PCサイト｜Facebook'});">
            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/bnr-facebook.png" alt="Facebook" width="229" height="66">
          </a>
        </li>
        <li>
          <a href="http://ameblo.jp/ginza-calla-staff/" class="trans" target="_blank" onclick="dataLayer.push({'event': 'FireEvent_ExternalLink', 'EventCatagory': 'PCサイト', 'EventAction': 'ExternalLink', 'EventLabel': 'ExternalLink｜PCサイト｜スタッフブログ'});">
            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/bnr-staffblog.png" alt="スタッフブログ" width="229" height="66">
          </a>
        </li>
        <li>
        <a href="<?php bloginfo('url'); ?>/recruit/" class="trans">
            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/bnr-recruit.png" alt="銀座カラー 求人情報" width="229" height="66">
          </a>
        </li>
        <li>
        <a href="<?php bloginfo('url'); ?>/product/" class="trans">
            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/bnr-bihadajunbi.png" alt="Bihada junbi 美肌潤美" width="229" height="66">
          </a>
        </li>
        <li>
          <a href="http://ginza-calla-eyes.com/" class="trans" target="_blank" onclick="dataLayer.push({'event': 'FireEvent_ExternalLink', 'EventCatagory': 'PCサイト', 'EventAction': 'ExternalLink', 'EventLabel': 'ExternalLink｜PCサイト｜銀座カラーアイズ'});">
            <img src="<?php bloginfo('template_url'); ?>/resource/img/common/bnr-eyes.png" alt="銀座カラーアイズ" width="229" height="66">
          </a>
        </li>
		<li>
          <a href="https://www.plan-international.jp/girl/" target="_blank" rel="nofollow">
	  <img src="https://www.plan-international.jp/common/img/bnr_girl_l.gif" alt="Because I am a Girl 世界の女の子たちに、生きていく力を　国際NGOプラン・インターナショナル" border="0"width="229" height="66" />
	  </a>
        </li>
      </ul>
      <!-- end サイドバー バナー -->
    <?php }; ?>
  <?php }; ?>
  <?php if(is_category('column')){}else{ ?>
    <ul class="sns-buttons">
      <li>
        <div class="fb-like" data-layout="button" data-show-faces="=false" data-share="false"></div>
      </li>
      <li>
        <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
      </li>
    </ul>
  <?php }; ?>
<!-- end snsボタン -->
</div>
