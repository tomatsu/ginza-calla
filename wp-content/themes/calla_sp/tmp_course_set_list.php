<div class="block-promotions block-white">

	  <section class="mod-plan">
		  <h2 class="icon-ninki-course">人気のコース</h2>
		  <ul>
		  	<li>
            <a href="<?php bloginfo('url') ?>/plan/">
			  		<img src="<?php bloginfo('template_url') ?>/resource/img/common/2001_btn-camp-sp.png" alt="全6回おまとめ予約！人気の顔・VIOもコミコミの24ヵ所全身脱毛！ 8ヵ月卒業プラン 6回完了＋1年間メンテナンス付き 月額5200円" width="679">
		  		</a>
		  	</li>
		  </ul>

	  <!-- /.mod-beginner --></section>


	  <section class="mod-plan">
		  <h2 class="icon-plan">お得なキャンペーン</h2>
			<div class="mod-plan-body">

        <a href="<?php bloginfo('url') ?>/campaign/waribiki.html#norikae-cp" class="mod-price-block wide" style="margin-bottom:9px;">
			    <section class="mod-price-recommend">
			      <div class="js-autoheight-plan">
			        <div class="wide-inner">
				        <div class="wide-icon">
									<em>一番人気</em>
				        </div>
				        <div class="wide-title">
									<h3>のりかえ割</h3>
				        </div>
			        </div>
			        <ul>
  			        <li>他サロンからの乗り換えで10,000円OFF</li>
			        </ul>
			      </div>
			    </section>
			  </a>

        <a href="<?php bloginfo('url') ?>/campaign/waribiki.html#pair-cp" class="mod-price-block wide" style="margin-bottom:9px;">
			    <section class="mod-price-recommend">
			      <div class="js-autoheight-plan">
			        <div class="wide-inner">
				        <div class="wide-icon">
									<em>友達・親子OK</em>
				        </div>
				        <div class="wide-title">
									<h3>ペア割</h3>
				        </div>
			        </div>
			        <ul>
  			        <li>ご一緒の契約で20,000円OFF</li>
			        </ul>
			      </div>
			    </section>
			  </a>

        <a href="<?php bloginfo('url') ?>/gakuwari/" class="mod-price-block wide" style="margin-bottom:9px;">
			    <section class="mod-price-recommend">
			      <div class="js-autoheight-plan">
			        <div class="wide-inner">
				        <div class="wide-icon">
									<em>学生のうちに</em>
				        </div>
				        <div class="wide-title">
									<h3>脱毛学割</h3>
				        </div>
			        </div>
			        <ul>
  			        <li>学生証のご提示でOK!10,000円OFF</li>
			        </ul>
			      </div>
			    </section>
			  </a>

				<a href="<?php bloginfo('url') ?>/campaign/shokai.html" class="mod-price-block wide" style="margin-bottom:9px;">
			    <section class="mod-price-recommend">
			      <div class="js-autoheight-plan">
			        <div class="wide-inner">
				        <div class="wide-icon">
									<em>紹介キャンペーン</em>
				        </div>
				        <div class="wide-title">
									<h3>おともだち紹介</h3>
				        </div>
			        </div>
			        <ul>
  			        <li>ご紹介で入会された方に10,000円OFF</li>
								<li>ご紹介してくれた方へも豪華特典！</li>
			        </ul>
			      </div>
			    </section>
			  </a>

		  <!-- /.mod-plan-body --></div>
			  <div class="block-btn-stripe-lower">
				  <a href="<?php bloginfo('url') ?>/campaign/" class="btn-stripe">キャンペーン一覧</a>
			  </div>
<!-- <div class="block-btn-stripe-lower">
				  <a href="<?php bloginfo('url') ?>/plan/part/" class="btn-stripe">部位一覧</a> -->
			  </div>
	  <!-- /.mod-plan --></section>

  <!-- /.block-promotions --></div>
