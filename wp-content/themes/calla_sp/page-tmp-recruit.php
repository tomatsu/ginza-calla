<?php
  $blogurl = get_bloginfo('url');
  if(is_page('conditions') || is_page('salonlist')){
    header("Location: $blogurl/404 ");
    exit;
  };
  ?>
<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head>
<meta charset="UTF-8">
<?php if(get_field('_aioseop_title')){ ?>
	<?php if(is_page('guideline2')){ ?>
		<title><?php the_field('_aioseop_title'); ?> | 美容脱毛サロン「銀座カラー」 採用ページ</title>
	<?php }else{ ?>
		<title><?php the_field('_aioseop_title'); ?> | 美容脱毛サロン「銀座カラー」 採用ページ</title>
	<?php }; ?>
<?php }else{ ?>
	<title><?php the_title(); ?> | 脱毛サロンに就職！脱毛の銀座カラー 採用ページ</title>
<?php }; ?>
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width">
<meta property="og:title" content="脱毛サロンに就職！｜脱毛の銀座カラー 採用ページ">
<meta property="og:type" content="website">
<meta property="og:description" content="脱毛専門店の銀座カラーの採用募集要項。「きらきら輝けるあなたへ」誰もがきらきら輝ける職場であなたも一緒に働いてみませんか？">
<meta property="og:url" content="https://ginza-calla.jp/recruit/">
<meta property="og:image" content="https://www.ginzacalla-gakuwari.jp/common/images/og_image.jpg">
<meta property="og:site_name" content="脱毛の銀座カラー 採用ページ">
<link rel="canonical" href="https://ginza-calla.jp/recruit/">
<?php if(is_page('guideline2')){}else{ ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/resource/recruit/css/import.css" media="all">
<?php }; ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/resource/recruit/css/jquery.sidr.dark.css" media="all">
<?php if(is_page('guideline2')){ ?>
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/resource/recruit/guidelines/css/base.css" media="screen,print" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/resource/recruit/guidelines/css/responsive.css" media="all" />
<?php }; ?>
<?php if(is_page('recruit')){ ?>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url') ?>/resource/recruit/js/jquery.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url') ?>/resource/recruit/js/common.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url') ?>/resource/recruit/js/jqTinyscrolling.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url') ?>/resource/recruit/js/jquery.cycle.all.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url') ?>/resource/recruit/js/jquery.sidr.min.js"></script>
  <script>
  $(document).ready(function() {
    $('.menu').sidr({
      name: 'sidr-right',
      side: 'right'
    });
  });

$(document).ready(function() {
  $('#slideBox').cycle({
  timeout:200,
  speed:1000,
  timeout:800
  });
});

$(document).ready(function() {
  $('.slideBox1').cycle({
  timeout:200,
  speed:10000,
  timeout:800
  });
});
  </script>
<?php }; ?>


</head>
<body id="pageTop">
  <?php
    if(is_page('recruit')){
      get_template_part('tmp-recruit-index');
    }else{
      if(get_field('sp_page') == ""){
        if (have_posts()) : while (have_posts()) : the_post();
          the_content();
        endwhile; endif;
      }else{
        echo apply_filters('the_content', get_post_meta($post->ID, 'sp_page', true));
      };
    };
   ?>
   <script type="text/javascript">
    var calla_tmp = "<?php bloginfo('template_url'); ?>";
   </script>
	<!--Yahoo!タグ-->
	<script type="text/javascript">
	  (function () {
	    var tagjs = document.createElement("script");
	    var s = document.getElementsByTagName("script")[0];
	    tagjs.async = true;
	    tagjs.src = "//s.yjtag.jp/tag.js#site=tdfgAp5";
	    s.parentNode.insertBefore(tagjs, s);
	  }());
	</script>
	<noscript>
	  <iframe src="//b.yjtag.jp/iframe?c=tdfgAp5" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
	</noscript>
	<!--Yahoo!タグ-->
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns?id=GTM-N9H8R5"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-N9H8R5');</script>
	<!-- End Google Tag Manager -->
	<script type="text/javascript" src="/galib_gc.js"></script>
	<!-- Google Tag Manager -->
	</body>
	</html>
