<?php get_template_part('header'); ?>
    <div class="main">
<!-- コラムカテゴリーの時に読み込むテンプレート -->
      <?php if(is_category('column') || in_category('column') || in_category_child( get_term_by( 'slug', 'column', 'category' ))){  ?>
        <?php if(have_posts()): while(have_posts()):the_post(); ?>
        <?php if(get_field('br_title')){ ?>
          <h1 class="relative page-head-thick page-head page-head-small"><?php the_field('br_title'); ?></h1>
        <?php }else{ ?>
          <h1 class="relative page-head-thick page-head page-head-small"><?php the_title();?></h1>
        <?php }; ?>
        <div class="page-body">
          <div class="block-white">
            <?php
              if(get_field('sp_page') == ""){
                  the_content();
              }else{
            ?>
            <div class="sp_box">
              <?php echo apply_filters('the_content', get_post_meta($post->ID, 'sp_page', true)); ?>
            </div>
          <?php }; ?>
            <section class="block-article" style="padding-top:0px;margin-top:-40px;">
              <div class="btn-share clearfix">
                <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                <div class="fb-share-button" data-href="<?php the_permalink();?>" data-layout="button"></div>
              </div>
              <div class="block-article-nav container">
              	<ul>
                  <li class="prev"><?php previous_post_link('%link', '前の記事を読む','TRUE'); ?></li>
                	<li class="seperator">｜</li>
                  <li class="archive"><a href="<?php bloginfo('url'); ?>/column/<?php echo $cat->slug; ?>" class="trans">一覧を見る</a></li>
                	<li class="seperator">｜</li>
                	<li class="next"><?php next_post_link('%link', '次の記事を読む','TRUE'); ?></li></li>
                </ul>
              <!-- /.block-article-nav --></div>
            </section>
          <!-- /.page-body --></div>
      	<!-- /.block-white --></div>
        <div class="block-category-nav pink">
          <ul>
          <li><a href="[site_url]/column/01/"><span>かんたんカワイイおいしい　今週のおうちごはん</span></a></li>
            <li><a href="[site_url]/column/02/"><span>名作に学ぼう　今週の、あのセリフ</span></a></li>
            <li><a href="[site_url]/column/03/"><span>週末どこいく？注目のイベント情報</span></a></li>
            <li><a href="[site_url]/column/04/"><span>こっそり教えます！プロの専門知識</span></a></li>
            <li><a href="[site_url]/column/05/"><span>ハッケン！銀座カラーの街</span></a></li>
          </ul>
        <!-- /.block-category-nav --></div>
        <?php endwhile; endif; ?>
      <?php }; ?>
<!-- コラムカテゴリーの時に読み込むテンプレートここまで -->

<!-- コンテンツカテゴリーの時に読み込むテンプレート -->
      <?php if(is_category('contents') || in_category('contents') || in_category_child( get_term_by( 'slug', 'contents', 'category' ))){  ?>
        <?php if(have_posts()): while(have_posts()):the_post(); ?>
          <?php
          $cat = get_the_category();
          $cat = $cat[0];
           ?>
           <?php if(get_field('html_heading') == ''){  ?>
             <h1 class="page-head"><?php the_title();?></h1>
           <?php }else{ ?>
             <h1 class="page-head page-head-thick"><?php the_field('html_heading');?></h1>
           <?php }; ?>
        <div class="block-white">
          <div class="block-article-contents-head">
            <p class="block-article-contents-category"><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></p>
            <p class="block-article-contents-date"><?php the_date("Y.n.j"); ?></p>
          </div>
          <div class="page-body page-body-contents">
            <section class="block-article block-article-contents">
              <div class="block-article-head block-article-head-contents">
                <div class="post-contents-box">
                  <?php the_content();?>
                </div>
              </div>
              <div class="btn-share clearfix">
                <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                <div class="fb-share-button" data-href="<?php the_permalink();?>" data-layout="button"></div>
				<!-- <a href="http://b.hatena.ne.jp/entry/" class="hatena-bookmark-button" data-hatena-bookmark-layout="basic-label-counter" data-hatena-bookmark-lang="ja" title="このエントリーをはてなブックマークに追加"><img src="https://b.st-hatena.com/images/entry-button/button-only@2x.png" alt="このエントリーをはてなブックマークに追加" width="20" height="20" style="border: none;" /></a><script type="text/javascript" src="https://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async="async"></script>-->
              </div>
              <div class="block-etc-contents">
                <div class="block-article-nav block-article-nav-contents">
                <?php
                  $prev_post_link = get_field('prev_post_link');
                  $next_post_link = get_field('next_post_link');
                ?>
                  <ul>
                    <li class="prev">
                    <?php if($prev_post_link != ""){ ?>
                      <a href="<?php bloginfo('url'); ?>/contents/<?php echo $prev_post_link; ?>" rel="prev">〈 前の記事</a>
                    <?php }; ?>
                    </li>
                    <li class="next">
                    <?php if($next_post_link != ""){ ?>
                      <a href="<?php bloginfo('url'); ?>/contents/<?php echo $next_post_link; ?>" rel="next">次の記事 〉</a>
                    <?php }; ?>
                    </li>
                    <li class="archive"><a href="<?php bloginfo('url'); ?>/contents/<?php echo $cat->slug; ?>" class="trans">記事一覧</a></li>
                  </ul>
                <!-- /.block-article-nav --></div>
                <?php if(has_tag()){ ?>
                <div class="block-etc-contents-tags">
                  <h2 class="icon-cont icon-cont-tags">この記事に関するタグ</h2>
                  <div class="">
                    <?php the_tags('','',''); ?>
                  </div>
                </div>
                <?php }else{}; ?>
                <?php
                  $relations = get_field('relation');
                  if(empty($relations)){}else{
                ?>
                <div class="block-etc-contents-relations">
                  <h2 class="icon-cont icon-cont-post">関連記事</h2>
                  <div class="contents-relations">
                    <?php
                      foreach($relations as $relation):setup_postdata($relation);
                    ?>
                      <a href="<?php echo get_permalink($relation); ?>">
                        <div class="contents-relations-left">
                          <img src="<?php echo catch_that_image_relation($relation); ?>" alt="">
                        </div>
                        <div class="contents-relations-right">
                          <h2><?php echo get_post($relation)->post_title; ?></h2>
                          <p><?php the_excerpt(); ?></p>
                        </div>
                      </a>
                    <?php endforeach; wp_reset_postdata(); ?>
                  </div>
                </div>
                <?php }; ?>
                <div class="block-etc-contents-category">
                  <h2 class="icon-cont icon-cont-cate">すべてのカテゴリー</h2>
                  <div class="">
                    <?php
                    $parent_id = get_category_by_slug("contents")->cat_ID;
                    $args = array(
                        'orderby'       => '',
                        'parent'        => $parent_id
                        );
                        $allcate = get_terms('category',$args);
                        foreach($allcate as $cates):
                    ?>
                    <a href="<?php echo get_term_link($cates->term_id,'category'); ?>"><?php echo $cates->name; ?></a>
                    <?php endforeach; wp_reset_postdata(); ?>
                  </div>
                </div>
              </div>
            <!-- /.block-article --></section>
          <!-- /.page-body --></div>
      	<!-- /.block-white --></div>
        <?php endwhile; endif; ?>
      <?php }; ?>
<!-- コンテンツカテゴリーの時に読み込むテンプレートここまで -->

<!-- ニュースカテゴリーの時に読み込むテンプレート -->
      <?php if(is_category('news') || in_category('news') || in_category_child( get_term_by( 'slug', 'news', 'category' ))){  ?>
        <h1 class="relative page-head-thick page-head page-head-small"><?php the_title();?></h1>
        <div class="page-body">
          <div class="block-white">
            <?php
              if(get_field('sp_page') == ""){
                  the_content();
              }else{
            ?>
            <div class="sp_box">
              <?php echo apply_filters('the_content', get_post_meta($post->ID, 'sp_page', true)); ?>
            </div>
          <?php }; ?>
            <section class="block-article" style="padding-top:0px;margin-top:-40px;">
              <div class="btn-share clearfix">
                <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                <div class="fb-share-button" data-href="<?php the_permalink();?>" data-layout="button"></div>
              </div>
            </section>
          <!-- /.page-body --></div>
        <!-- /.block-white --></div>
      <?php }; ?>
<!-- ニュースカテゴリーの時に読み込むテンプレートここまで -->
      <?php if( !get_field('free_banner') ){ ?>
        <div class="block-btn">
           <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0047" class="btn-stripe btn-counsel btn-large btn-shadow" target="_blank">
             <span>無料カウンセリング</span><br>ご予約はこちら
            </a>
          <!-- /.block-btn --></div>
      <?php }; ?>
    </div>
<?php get_template_part('footer'); ?>
