!(function() {
  'use strict'

  /**
   * メインビジュアルのスライダー
   */
  var MvSlider = (function() {
    var MvSlider = function(device) {
      this.Z_TOP = 2
      this.Z_UNDER = 1
      this.Z_ZERO = 0
      this.isSp = device === 'sp'
      this.options = {
        duration: this.isSp ? 0.7 : 1,
        autoplay: true,
        interval: 4000
      }
      this.$el = {
        list: $('.mg-mv-slide-list'),
        item: $('.mg-mv-slide-item'),
        prev: $('.mg-mv-slide-prev'),
        next: $('.mg-mv-slide-next')
      }
      this.isDead = false
      this.index = 0
      this.timer = null
      this.isAnimating = false
      this.callbackOnChange = null
      this.total = this.$el.item.length

      this.init()
    }

    MvSlider.prototype = {
      /**
       * z-indexやイベントの初期設定
       */
      init: function() {
        if (this.total < 1) {
          console.error('スライダーを設定してください')

          return false
        } else if (this.total === 1) {
          this.resizeItem()
          this.$el.prev.remove()
          this.$el.next.remove()
          this.isDead = true

          return false
        }

        TweenMax.set(this.$el.item.eq(0), {zIndex: this.Z_TOP})

        this.resizeItem()
        this.restartTimer()
        this.bind()
      },
      /**
       * スライドが変わったときに呼び出すコールバックの設定
       */
      handleChange: function(callback) {
        this.callbackOnChange = callback
      },
      /**
       * 自動再生かどうか
       */
      isAutoplay: function() {
        return this.options.autoplay && this.options.interval
      },
      /**
       * 自動再生のタイマーを再設定
       */
      restartTimer: function() {
        var self = this

        if (!this.isAutoplay()) {
          return
        }

        if (this.timer) {
          clearTimeout(this.timer)
        }

        this.timer = setTimeout(function() {
          self.next()
        }, this.options.interval)
      },
      /**
       * スライドが移動可能かどうか
       *
       * アニメーション中の移動を防ぐ
       */
      canMove: function(index) {
        return index !== this.index && !this.isAnimating
      },
      /**
       * クリックイベントなどの設定
       */
      bind: function() {
        var self = this
        var mc = new Hammer(this.$el.list[0])

        mc.on('swipeleft', function(ev) {
          self.next()
        })
        mc.on('swiperight', function(ev) {
          self.prev()
        })
        this.$el.next.on('click', function() {
          self.next()
        })
        this.$el.prev.on('click', function() {
          self.prev()
        })
        $(window).on('resize', function() {
          self.resizeItem()
        })
      },
      /**
       * SVGのサイズをウインドウ幅によって調整
       *
       * リサイズで比率が変わるPCのみ処理が必要
       */
      resizeItem: function() {
        var self = this
        var listWidth = this.$el.list.innerWidth()
        var listHeight = this.$el.list.innerHeight()

        this.$el.item.each(function() {
          if (!self.isSp) {
            var viewBox = '0 0 ' + listWidth + ' 560';
            var imageWidth = $(this).find('image').attr('width')
            var x = -1 * (imageWidth - listWidth) / 2
            var r = Math.ceil(Math.sqrt(Math.pow(listWidth, 2) + Math.pow(listHeight, 2))) / 2

            $(this).find('svg')[0].setAttribute('viewBox', viewBox)
            $(this).find('ellipse').attr('rx', r)
            $(this).find('ellipse').attr('ry', r * 2)
            $(this).find('image').attr('x', x)
          }
        })
      },
      /**
       * 次へ
       */
      next: function(index) {
        this.move(index, 'next')
      },
      /**
       * 前へ
       */
      prev: function(index) {
        this.move(index, 'prev')
      },
      /**
       * indexを指定して移動
       */
      moveTo: function(index, direction) {
        if (direction) {
          this[direction](index)
        } else {
          index > this.index ? this.next(index) : this.prev(index)
        }
      },
      /**
       * 移動のアニメーション
       */
      move: function(index, direction) {
        var self = this
        var $top = this.$el.item.eq(this.index)
        var $circle = $top.find('ellipse')
        var isNext = direction === 'next'
        var underIndex, $under, handleMoveEnd

        // アニメーション中は移動しない
        if (!this.canMove()) {
          return false
        }

        this.isAnimating = true
        $top.addClass('is-animating')

        // index指定がある場合はそこへ移動
        if (index !== undefined) {
          underIndex = index
        }　else {
          if (isNext) {
            underIndex = this.index === this.total - 1 ? 0 : this.index + 1
          } else {
            underIndex = this.index === 0 ? this.total - 1 : this.index - 1
          }

          if (this.callbackOnChange) {
            this.callbackOnChange(underIndex, direction)
          }
        }

        this.index = underIndex

        // 後ろにくるレイヤー
        $under = this.$el.item.eq(underIndex)
        TweenMax.set($under, {zIndex: this.Z_UNDER})

        // 終了時にプロパティなどをリセットする
        handleMoveEnd = function() {
          self.isAnimating = false
          $top.removeClass('is-animating')
          TweenMax.set($under, {zIndex: self.Z_TOP})
          TweenMax.set($top, {clearProps: 'zIndex, opacity'})
          setTimeout(function() {
            TweenMax.set($circle, {x: '0%', y: '0%', scale: 1})
          }, 16)

          self.restartTimer()
        }

        // 遷移のアニメーション
        TweenMax.set($circle, {transformOrigin:"center center"})
        if (isNext) {
          TweenMax.set($circle, {rotation: 45})
          TweenMax.fromTo($under, this.options.duration * 0.86, {x: '10%'}, {x: '0%', ease: Power2.easeOut})
          TweenMax.to($circle, this.options.duration, {x: '-120%', ease: Power2.easeInOut, onComplete: function() {
            handleMoveEnd()
          }})
        } else {
          TweenMax.set($circle, {rotation: -45})
          TweenMax.fromTo($under, this.options.duration * 0.86, {x: '-10%'}, {x: '0%', ease: Power2.easeOut})
          TweenMax.to($circle, this.options.duration, {x: '120%', ease: Power2.easeInOut, onComplete: function() {
            handleMoveEnd()
          }})
        }
      }
    }

    return MvSlider
  })()

  /**
   * メインビジュアルのナビゲーション
   */
  var MvNav = (function() {
    var MvNav = function() {
      this.SLIDE_TO_SHOW = 3
      this.isSlick = true
      this.$nav = $('#js-slide-nav')
      this.$slick = this.$nav.find('ul')
      this.$navItem = this.$slick.find('li')
      this.$navCurrent = this.$nav.find('span')
      this.navItemWidth = null
      this.navCurrentWidth = this.$navCurrent.innerWidth()
      this.total = this.$navItem.length

      this.init()
    }

    MvNav.prototype = {
      init: function() {
        var slideToShow

        if (this.total === 1) {
          this.$nav.remove()
          return
        } else if (this.total <= this.SLIDE_TO_SHOW) {
          this.isSlick = false
          this.$nav.addClass('mg-mod-minimal')
          this.navItemWidth = this.$navItem.eq(0).innerWidth()
        } else {
          this.$slick.slick({
            arrows: false,
            focusOnSelect: true,
            speed: 700,
            slidesToShow: this.SLIDE_TO_SHOW,
            infinite: true,
            centerMode: true,
            centerPadding: '8%'
          })
        }
      },
      moveTo: function(index, direction) {
        if (this.isSlick) {
          if (direction === 'next') {
            this.$slick.slick('slickNext')
          } else {
            this.$slick.slick('slickPrev')
          }
        } else {
          var x = index * this.navItemWidth + ((this.navItemWidth - this.navCurrentWidth) / 2)

          TweenMax.to(this.$navCurrent, 0.7, {x: x, ease: Power3.easeOut, delay: 0.2})
        }
      }
    }

    return MvNav
  })()

  /**
   * メインビジュアルのアニメーション
   *
   * 画像とナビゲーションを同期させる
   */
  var mvAnime = function() {
    var slider = new MvSlider('sp')
    var nav = new MvNav()

    if (slider.isDead) {
      return
    }

    slider.handleChange(function(index, direction) {
      nav.moveTo(index, direction)
    })

    if (nav.isSlick) {
      nav.$slick.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
        var direction
        var lastSlide = slick.slideCount - 1
  
        if (currentSlide === nextSlide) {
          return
        }
  
        if (currentSlide === lastSlide && nextSlide === 0) {
          direction = 'next'
        } else if (currentSlide === 0 && nextSlide === lastSlide) {
          direction = 'prev'
        } else {
          direction = nextSlide > currentSlide ? 'next' : 'prev'
        }
  
        slider.move(nextSlide, direction)
      })
    } else {
      nav.$navItem.on('click', function() {
        var index = nav.$navItem.index(this)
  
        if (slider.canMove(index)) {
          nav.moveTo(index)
          slider.moveTo(index)
        }
      })
    }
  }

  /**
   * キャンペーンのスライダー
   */
  var campaignSlider = function() {
    $('.mg-campaign-list').slick({
      loop: true,
      speed: 600,
      slidesToShow: 1,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 5000,
      centerMode: true,
      centerPadding: '16%',
      nextArrow: '.mg-campaign-slide-next',
      prevArrow: '.mg-campaign-slide-prev'
    })
  }

  var watchAboutLoaded = function() {
    $('#mg-about').imagesLoaded( function(data) {
      MigrateGinza.movingImage.updateScene()
    })
  }

  $(function() {
    mvAnime()
    campaignSlider()
    watchAboutLoaded()
  })
})();