<?php get_template_part('header'); ?>
    <div class="main">
      <?php if(get_field('br_title')){ ?>
        <?php if(is_page('voice')){ ?>
          <h1 class="page-head page-head-thick icon-voice"><span class="site">銀座カラー</span><span><?php the_field('br_title'); ?></span></h1>
        <?php }else if(is_page('ranking')){ ?>
          <h1 class="page-head page-head-thick icon-rank"><span class="site">銀座カラー</span><span><?php the_field('br_title'); ?></span></h1>
        <?php }else if(is_parent_slug() == 'course'){ ?>
          <h1 class="page-head <?php the_field('h1_logo') ?>"><?php the_title(); ?><span class="site">銀座カラー</span></h1>
        <?php }else{ ?>
          <?php if(get_field('h1_logo') !='' ){ ?>
            <h1 class="page-head page-head-thick <?php the_field('h1_logo') ?>"><span class="site">銀座カラー</span><span><?php the_field('br_title'); ?></span></h1>
          <?php }else{ ?>
            <h1 class="page-head page-head-thick"><span class="site">銀座カラー</span><span><?php the_field('br_title'); ?></span></h1>
          <?php }; ?>
        <?php }; ?>
      <?php }else{ ?>
        <?php if(is_page('voice')){ ?>
          <h1 class="page-head icon-voice"><span class="site">銀座カラー</span><?php the_title(); ?></h1>
        <?php }else if(is_page('ranking')){ ?>
          <h1 class="page-head icon-rank"><span class="site">銀座カラー</span><?php the_title(); ?></h1>
        <?php }else if(is_parent_slug() == 'course'){ ?>
          <h1 class="page-head <?php the_field('h1_logo') ?>"><?php the_title(); ?><span class="site">銀座カラー</span></h1>
        <?php }else{ ?>
          <?php if(get_field('h1_logo') !='' ){ ?>
            <h1 class="page-head <?php the_field('h1_logo') ?>"><span class="site">銀座カラー</span><?php the_title(); ?></h1>
          <?php }else{ ?>
            <h1 class="page-head"><span class="site">銀座カラー</span><?php the_title(); ?></h1>
          <?php }; ?>
        <?php }; ?>
      <?php }; ?>
        <div class="page-body">
        <?php
          if(get_field('sp_page') == ""){
            if (have_posts()) : while (have_posts()) : the_post();
              the_content();
            endwhile; endif;
          }else{
        ?>
        <div class="sp_box">
          <?php echo apply_filters('the_content', get_post_meta($post->ID, 'sp_page', true)); ?>
        </div>
      <?php }; ?>
        </div><!-- /.page-body -->
       <?php
         if( !get_field('sp_new_cust_banner') ){
           get_template_part('tmp_course_set_list');
         };
        ?>
        <?php if( !get_field('free_banner') ){ ?>
          <div class="block-btn">
             <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0047" class="btn-stripe btn-counsel btn-large btn-shadow" target="_blank">
               <span>無料カウンセリング</span><br>ご予約はこちら
              </a>
            <!-- /.block-btn --></div>
        <?php }else if(is_page_group('product')){ ?>
          <div class="block-btn">
             <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0047" class="btn-stripe btn-counsel btn-large btn-shadow" target="_blank">
               <span>無料カウンセリング</span><br>ご予約はこちら
              </a>
            <!-- /.block-btn --></div>
        <?php }; ?>



    </div><!-- /.main -->
<?php get_template_part('footer'); ?>
