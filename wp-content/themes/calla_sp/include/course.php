<!-- 新・全身脱毛コース ======================================== -->
<section class="mg-course-zenshin">
    <h3 class="mg-course-zenshin-title"><img src="/resource/img/migrate/index/course/zenshin/title.png" alt="全身22カ所対象の“新・全身脱毛”コース"></h3>
    <p class="mg-course-zenshin-text">「全身を早くキレイに」という<br>お客様の要望にお応えしたコース</p>
    <p class="mg-course-zenshin-illust"><img src="/resource/img/migrate/index/course/zenshin/illust.png" alt=""></p>
    <figure class="mg-course-zenshin-balloon js-magic"><img src="/resource/img/migrate/index/course/zenshin/balloon.png" alt="1番人気コース"></figure>

    <div class="mg-course-box-list">
        <!-- “スピード脱毛”コース -->
        <section class="mg-course-box-small mg-mod-speed">
            <h4 class="mg-course-box-small-title"><img src="/resource/img/migrate/index/course/speed/title.png" alt="“スピード脱毛”コース"></h4>
            <p class="mg-course-box-small-text">とにかく早くおトクにはじめたいなら<br>”スピード脱毛”コースがオススメ!</p>
            <p class="mg-course-box-small-price">月額9,600円<span>（税抜）</span></p>
            <p class="mg-course-box-link"><a href="/plan/course/zenshin/" class="mg-button-pink">料金詳細はこちら</a></p>
            <figure class="mg-course-box-small-icon js-magic"><img src="/resource/img/migrate/index/course/speed/balloon.png" alt="早くキレイになりたい！"></figure>
        </section>
        <!-- “リーズナブル脱毛”コース -->
        <section class="mg-course-box-small mg-mod-reasonable">
            <h4 class="mg-course-box-small-title"><img src="/resource/img/migrate/index/course/reasonable/title.png" alt="“リーズナブル脱毛”コース"></h4>
            <p class="mg-course-box-small-text">とにかく安くはじめたいなら<br>“リーズナブル脱毛”コースがオススメ！</p>
            <p class="mg-course-box-small-price">月額6,500円<span>（税抜）</span></p>
            <p class="mg-course-box-link"><a href="/plan/course/zenshin/" class="mg-button-pink">料金詳細はこちら</a></p>
            <figure class="mg-course-box-small-icon js-magic"><img src="/resource/img/migrate/index/course/reasonable/balloon.png" alt="とにかく安く！"></figure>
        </section>
    </div>
</section>

<div class="mg-course-partial mg-course-box-list">
    <!-- “顔セット”コース -->
    <section class="mg-course-box-large mg-mod-face">
    <div class="mg-course-box-large-thumb" style="background-image: url(/resource/img/migrate/index/course/face/thumb.jpg)"></div>
        <div class="mg-course-box-large-summary">
            <h4 class="mg-course-box-large-title"><img src="/resource/img/migrate/index/course/face/title.png" alt="“顔セット”コース"></h4>
            <p class="mg-course-box-large-text">鼻下を合わせた顔全体が含まれる顔脱毛セットです。お顔周りをまとめてキレイにできる、お得なコースです。</p>
            <p class="mg-course-box-link"><a href="/plan/course/face/" class="mg-button-pink">料金詳細はこちら</a></p>
        </div>
    </section>
    <!-- “腕全セット”コース -->
    <section class="mg-course-box-large mg-mod-ude">
        <div class="mg-course-box-large-thumb" style="background-image: url(/resource/img/migrate/index/course/ude/thumb.jpg)"></div>
        <div class="mg-course-box-large-summary">
            <h4 class="mg-course-box-large-title"><img src="/resource/img/migrate/index/course/ude/title.png" alt="“腕全セット”コース"></h4>
            <p class="mg-course-box-large-text">甲・指、両ヒジ上、両ヒジ下を合わせた腕全体が含まれる腕脱毛セットです。自分に合わせた回数、または脱毛し放題が選べる脱毛コースです。</p>
            <p class="mg-course-box-link"><a href="/plan/course/ude/" class="mg-button-pink">料金詳細はこちら</a></p>
        </div>
    </section>
    <!-- “足全セット”コース -->
    <section class="mg-course-box-large mg-mod-ashi">
        <div class="mg-course-box-large-thumb" style="background-image: url(/resource/img/migrate/index/course/ashi/thumb.jpg)"></div>
        <div class="mg-course-box-large-summary">
            <h4 class="mg-course-box-large-title"><img src="/resource/img/migrate/index/course/ashi/title.png" alt="“足全セット”コース"></h4>
            <p class="mg-course-box-large-text">甲・指、両ひざ上、両ひざ、両ひざ下の足全体が含まれるセットです。自分に合わせた回数が選べる嬉しい脱毛コースです。</p>
            <p class="mg-course-box-link"><a href="/plan/course/ashi/" class="mg-button-pink">料金詳細はこちら</a></p>
        </div>
    </section>
    <!-- “VIOセット”コース -->
    <section class="mg-course-box-large mg-mod-vio">
        <div class="mg-course-box-large-thumb" style="background-image: url(/resource/img/migrate/index/course/vio/thumb.jpg)"></div>
        <div class="mg-course-box-large-summary">
            <h4 class="mg-course-box-large-title"><img src="/resource/img/migrate/index/course/vio/title.png" alt="“VIOセット”コース"></h4>
            <p class="mg-course-box-large-text">Vライン(上部・サイド・iライン)、ヒップ奥の女性のデリケートゾーンをまとめたコースです。お好きな回数、または脱毛し放題から選んで脱毛できるので安心です。</p>
            <p class="mg-course-box-link"><a href="/plan/course/vio/" class="mg-button-pink">料金詳細はこちら</a></p>
        </div>
    </section>
</div>