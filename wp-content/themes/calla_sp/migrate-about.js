!(function() {
  'use strict'

  /**
   * ツールチップ
   *
   * タップしたときに吹き出しを切り替える
   */
  var Tooltip = (function() {
    var Tooltip = function() {
      this.$item = $('.mg-about-item')
      this.selectedClass = 'is-selected'
    }

    Tooltip.prototype = {
      bind: function() {
        var self = this

        this.$item.on('click', function() {
          $(this).hasClass(self.selectedClass) ? self.hideAll() : self.show(this)
        })
      },
      show: function(el) {
        var $tooltip = $(el).find('.mg-about-tooltip')
        var tooltipHeight = $tooltip.outerHeight()
        var margin = this.getMargin($tooltip)
        var gap = tooltipHeight + margin

        this.hideAll()

        $(el).addClass(this.selectedClass)
        TweenMax.to(el, 0.4, {marginBottom: gap, ease: Power2.easeOut})
        TweenMax.fromTo($tooltip, 0.3,  {opacity: 0, y: 6}, {opacity: 1, y: 0, ease: Power2.easeOut, delay: 0.28, display: 'block'})
      },
      hideAll: function() {
        var self = this
        var $selectedItem = this.$item.filter('.' + this.selectedClass)

        if (!$selectedItem.length) {
          return
        }

        $selectedItem.each(function() {
          var $tooltip = $(this).find('.mg-about-tooltip')

          $(this).removeClass(self.selectedClass)
          TweenMax.to(this, 0.2, {marginBottom: 0, ease: Power2.easeOut})
          TweenMax.to($tooltip, 0.2,  {opacity: 0, ease: Power2.easeOut, display: 'none'})
        })
      },
      getMargin: function($tooltip) {
        return parseFloat($tooltip.css('margin-top')) + parseFloat(this.$item.eq(1).css('margin-top'))
      }
    }

    return Tooltip
  })()

  $(function() {
    var tooltip = new Tooltip()

    tooltip.bind()
  })
})();