<?php
  $current_category = get_queried_object();
  $catid = $current_category->term_id;
  if (get_field('cat_meta_title','post_tag_'.$catid) == ''){
    $title = $current_category->name;
  }else{
    $title = get_field('cat_meta_title','post_tag_'.$catid);
  }
?>
<?php get_template_part('header'); ?>
    <div class="main">
      <h1 class="page-head icon-column"><?php echo $title; ?></h1>
      <div class="block-white block-white-contents">
        <div class="page-body">
          <div class="block-entries js-appendmore-contents">
           <?php if(have_posts()): while(have_posts()):the_post(); ?>
             <?php   if ($post === end($posts)) { ?>
               <section class="block-category block-category-contents block-category-contents-last">
             <?php }else{ ?>
               <section class="block-category block-category-contents">
             <?php }; ?>
            <div class="block-category-body block-category-body-contents">
              <div class="text">
                <a href="<?php the_permalink(); ?>" class="">
                  <em class="lead"><?php the_title(); ?></em>
                </a>
                <div class="block-category-links-contents">
                  <ul class="block-category-links block-category-contents-ul-01">
                    <?php the_category(); ?>
                    <?php the_tags('','',''); ?>
                  </ul>
                </div>
              </div>
              <figure class="image">
                <a href="<?php the_permalink(); ?>" class="block-category-links"><img src="<?php echo catch_that_image(); ?>" alt="" width="245"></a>
              </figure>
            </div>
          </section>
          <?php endwhile; endif; ?>
            <div class="block-btn-stripe-lower">
              <a href="#" class="btn-stripe btn-icon-plus">もっとみる</a>
            </div>
          </div>
          <div class="block-etc-contents">
            <div class="block-etc-contents-tags">
              <h2 class="icon-cont icon-cont-tags">人気おすすめタグ</h2>
              <div class="">
                <?php
                     $alltags = get_terms('post_tag');
                     foreach($alltags as $taginfo):
                ?>
                <a href="<?php echo get_term_link($taginfo->term_id,'post_tag'); ?>"><?php echo $taginfo->name; ?></a>
                <?php endforeach; wp_reset_postdata(); ?>
              </div>
            </div>
            <div class="block-etc-contents-category">
              <h2 class="icon-cont icon-cont-cate">すべてのカテゴリー</h2>
              <div class="">
                <?php
                $parent_id = get_category_by_slug("contents")->cat_ID;
                $args = array(
                    'orderby'       => '',
                    'parent'        => $parent_id
                    );
                    $allcate = get_terms('category',$args);
                    foreach($allcate as $cates):
                ?>
                <a href="<?php echo get_term_link($cates->term_id,'category'); ?>"><?php echo $cates->name; ?></a>
                <?php endforeach; wp_reset_postdata(); ?>
              </div>
            </div>
          </div>
        </div><!-- /.page-body -->
      </div><!-- /.block-white -->
      <div class="block-btn">
         <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0047" class="btn-stripe btn-counsel btn-large btn-shadow" target="_blank">
           <span>無料カウンセリング</span><br>ご予約はこちら
         </a>
      <!-- /.block-btn --></div>
  </div>
<?php get_template_part('footer'); ?>
