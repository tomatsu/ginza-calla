<?php
  $current_category = get_queried_object();
  $catid = $current_category->term_id;
  if (get_field('cat_meta_title','category_'.$catid) == ''){
    $title = $current_category->name;
  }else{
    $title = get_field('cat_meta_title','category_'.$catid);
  }
?>
<?php get_template_part('header'); ?>
    <div class="main">
<!-- コラムカテゴリーの時に読み込むテンプレート -->
      <!-- 全体一覧 -->
      <?php if(is_category('column') || in_category('column')){  ?>
          <h1 class="page-head icon-column">脱毛や美容に関するコラム</h1>
          <div class="page-body">
            <div class="block-white">
            <p class="text">かんたんレシピから、女の子の悩みにそっと寄り添う名言の数々。気になるあの人に聞いた美容についてのエピソードに、銀座カラーのある街のおすすめスポットや週末のイベント情報まで。女の子を応援する銀座カラーが、日替わりでお役立ち情報をお届けします！</p>
            <?php
              $cat_id = get_category_by_slug('column')->cat_ID;
              $categories = get_terms( 'category', array(
                  'orderby'    => 'slug',
                  'hide_empty' => 0,
                  'child_of' => $cat_id,
               ) );
              foreach($categories as $value):
             ?>
             <?php
             if($value->slug == '01'){
               $icon = 'dinner';
               $value->name = "かんたんカワイイおいしい<br>今週のおうちごはん";
             }elseif ($value->slug == '02') {
               $icon = 'baloon';
               $value->name = "名作に学ぼう<br>今週の、あのセリフ";
             }elseif ($value->slug == '03') {
               $icon = 'flag';
               $value->name = "週末どこいく？<br>注目のイベント情報";
             }elseif ($value->slug == '04') {
               $icon = 'book-red';
               $value->name = "こっそり教えます！<br>プロの専門知識";
             }elseif ($value->slug == '05') {
               $icon = 'location';
               $value->name = "ハッケン！銀座カラーの街";
             }else{};
              ?>
            <section class="block-category">
              <h2 class="block-category-head icon-<?php echo $icon; ?>__absolute"><?php echo $value->name;?></h2>
              <?php $posts = get_posts("numberposts=1&cat=$value->term_id&orderby=slug"); global $post;?>
              <?php foreach($posts as $post) : setup_postdata($post); ?>
              <div class="block-category-body container">
                <em class="lead"><?php the_title(); ?></em>
                <figure class="image">
                  <img src="<?php echo catch_that_image(); ?>" alt="" width="245">
                </figure>
                <p class="date">【<?php echo get_post_time("Y/m/d"); ?>】</p>
                <p> <?php the_excerpt(); ?></p>
                <ul class="block-category-links">
                  <li><a href="<?php the_permalink(); ?>" class="btn-column btn-red trans">記事を読む</a></li>
                  <li><a href="<?php echo get_category_link($value->term_id); ?>" class="btn-column btn-yellow trans">一覧を見る</a></li>
                </ul>
              </div>
              <?php endforeach; ?>
            </section>
            <?php endforeach; ?>
          </div><!-- /.page-body -->
        </div><!-- /.block-white -->
      <!-- 全体一覧ここまで -->

      <!-- 各カテゴリ一覧 -->
    <?php }else if(in_category_child( get_term_by( 'slug', 'column', 'category' ))){ ?>
          <?php
            $cat = get_the_category();
            $cat = $cat[0];
            $cat_name = $cat->name;
            $cat_termid   = $cat->term_taxonomy_id;
            $cat_description   = $cat->category_description;
          ?>
          <?php
          if($cat->slug == '01'){
            $icon = 'dinner';
            $cat_name = "かんたんカワイイおいしい<br>今週のおうちごはん";
          }elseif ($cat->slug == '02') {
            $icon = 'talk';
            $cat_name = "名作に学ぼう<br>今週の、あのセリフ";
          }elseif ($cat->slug == '03') {
            $icon = 'flag';
            $cat_name = "週末どこいく？<br>注目のイベント情報";
          }elseif ($cat->slug == '04') {
            $icon = 'book';
            $cat_name = "こっそり教えます！<br>プロの専門知識";
          }elseif ($cat->slug == '05') {
            $icon = 'building';
            $cat_name = "ハッケン！<br>銀座カラーの街";
          }else{};
           ?>
        <h1 class="page-head-thick page-head">
        <span class="relative icon-<?php echo $icon; ?>-white__absolute"><?php echo $cat_name; ?><span></h1>
          <div class="page-body">
            <div class="block-white">
          <p class="text"><?php echo $cat_description ?></p>
            <div class="block-entries js-appendmore-column">
              <?php
                $args = array(
                  'cat' => $cat_termid
                );
                query_posts($query_string,$args);
              ?>
              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <article class="block-entry">
                  <a href="<?php the_permalink(); ?>" class="container">
                    <div class="body">
                      <span class="date"><?php echo get_post_time("Y.m.d"); ?></span>
                      <h3 class="title"><?php the_title(); ?></h3>
                    </div>
                    <div class="thumbnail">
                      <img src="<?php echo catch_that_image(); ?>" >
                    </div>
                    <div class="more">
                      <span class="btn-column btn-red">記事を読む</span>
                    </div>
                  </a>
                </article>
              <?php endwhile; endif; ?>
              <?php if (function_exists("pagination")) {
                pagination($additional_loop->max_num_pages);
              } ?>
            </div>
          </div><!-- /.page-body -->
          <div class="block-category-nav pink">
            <ul>
              <li><a href="<?php bloginfo('url'); ?>/column/01/"><span>かんたんカワイイおいしい　今週のおうちごはん</span></a></li>
              <li><a href="<?php bloginfo('url'); ?>/column/02/"><span>名作に学ぼう　今週の、あのセリフ</span></a></li>
              <li><a href="<?php bloginfo('url'); ?>/column/03/"><span>週末どこいく？注目のイベント情報</span></a></li>
              <li><a href="<?php bloginfo('url'); ?>/column/04/"><span>こっそり教えます！プロの専門知識</span></a></li>
              <li><a href="<?php bloginfo('url'); ?>/column/05/"><span>ハッケン！銀座カラーの街</span></a></li>
            </ul>
          <!-- /.block-category-nav --></div>
        </div><!-- /.block-white -->
      <?php }; ?>
      <!-- 各カテゴリ一覧ここまで -->
<!-- コラムカテゴリーの時に読み込むテンプレートここまで -->
<!-- コンテンツカテゴリーの時に読み込むテンプレート -->
      <?php if(is_category('contents')){  ?>
        <h1 class="page-head icon-column"><?php echo $title; ?></h1>
        <div class="block-white block-white-contents">
          <div class="page-body">
            <div class="block-category-contents-text">
              <p><?php echo category_description(); ?></p>
            </div>
            <h2 class="block-category-head block-category-head-contents">new column</h2>
            <?php
              $cat_id = get_category_by_slug('contents')->cat_ID;
             ?>
             <?php
               $posts = get_posts("cat=$cat_id&orderby=date&order=DESC&posts_per_page=6"); global $posts;
             ?>
             <?php foreach($posts as $post) : setup_postdata($post); ?>
               <?php   if ($post === end($posts)) { ?>
                 <section class="block-category block-category-contents block-category-contents-top-last">
               <?php }else{ ?>
                 <section class="block-category block-category-contents">
               <?php }; ?>
              <div class="block-category-body block-category-body-contents">
                <div class="text">
                  <a href="<?php the_permalink(); ?>" class="">
                    <em class="lead"><?php the_title(); ?></em>
                  </a>
                  <div class="block-category-links-contents">
                    <ul class="block-category-links block-category-contents-ul-01">
                      <?php the_category(); ?>
                      <?php the_tags('','',''); ?>
                    </ul>
                  </div>
                </div>
                <figure class="image">
                  <a href="<?php the_permalink(); ?>" class="block-category-links"><img src="<?php echo catch_that_image(); ?>" alt="" width="245"></a>
                </figure>
              </div>
            </section>
            <?php endforeach; ?>
            <div class="block-category-contents-more clearfix">
              <a class="block-contents-link-more" href="<?php bloginfo('url'); ?>/contents/newcontents">新着をさらに見る<span>〉〉</span></a>
            </div>
            <div class="block-etc-contents">
              <div class="block-etc-contents-tags">
                <h2 class="icon-cont icon-cont-tags">人気おすすめタグ</h2>
                <div class="">
                  <?php
                       $alltags = get_terms('post_tag');
                       foreach($alltags as $taginfo):
                  ?>
                  <a href="<?php echo get_term_link($taginfo->term_id,'post_tag'); ?>"><?php echo $taginfo->name; ?></a>
                  <?php endforeach; wp_reset_postdata(); ?>
                </div>
              </div>
              <div class="block-etc-contents-category">
                <h2 class="icon-cont icon-cont-cate">すべてのカテゴリー</h2>
                <div class="">
                  <?php
                  $parent_id = get_category_by_slug("contents")->cat_ID;
                  $args = array(
                      'orderby'       => '',
                      'parent'        => $parent_id
                      );
                      $allcate = get_terms('category',$args);
                      foreach($allcate as $cates):
                  ?>
                  <a href="<?php echo get_term_link($cates->term_id,'category'); ?>"><?php echo $cates->name; ?></a>
                  <?php endforeach; wp_reset_postdata(); ?>
                </div>
              </div>
            </div>
          </div><!-- /.page-body -->
        </div><!-- /.block-white -->
      <!-- 全体一覧ここまで -->
      <?php }else if(is_category('newcontents')){ ?>
        <h1 class="page-head icon-column"><?php echo $title; ?></h1>
        <div class="block-white block-white-contents">
          <div class="page-body">
            <?php
              $args = array(
                'posts_per_page' => '9999',
                'meta_key' => 'newcontents',
                'meta_value' => true
                );
              query_posts($args);
             ?>
            <div class="block-entries js-appendmore-contents">
             <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
               <?php if ($post === end($posts)) { ?>
                 <section class="block-category block-category-contents block-category-contents-last">
               <?php }else{ ?>
                 <section class="block-category block-category-contents">
               <?php }; ?>
              <div class="block-category-body block-category-body-contents">
                <div class="text">
                  <a href="<?php the_permalink(); ?>" class="">
                    <em class="lead"><?php the_title(); ?></em>
                  </a>
                  <div class="block-category-links-contents">
                    <ul class="block-category-links block-category-contents-ul-01">
                      <?php the_category(); ?>
                      <?php the_tags('','',''); ?>
                    </ul>
                  </div>
                </div>
                <figure class="image">
                  <a href="<?php the_permalink(); ?>" class="block-category-links"><img src="<?php echo catch_that_image(); ?>" alt="" width="245"></a>
                </figure>
              </div>
            </section>
            <?php endwhile; endif; ?>
<!--               <div class="block-btn-stripe-lower">
                <a href="#" class="btn-stripe btn-icon-plus">もっとみる</a>
              </div> -->
            </div>
            <div class="block-etc-contents">
              <div class="block-etc-contents-tags">
                <h2 class="icon-cont icon-cont-tags">人気おすすめタグ</h2>
                <div class="">
                  <?php
                       $alltags = get_terms('post_tag');
                       foreach($alltags as $taginfo):
                  ?>
                  <a href="<?php echo get_term_link($taginfo->term_id,'post_tag'); ?>"><?php echo $taginfo->name; ?></a>
                  <?php endforeach; wp_reset_postdata(); ?>
                </div>
              </div>
              <div class="block-etc-contents-category">
                <h2 class="icon-cont icon-cont-cate">すべてのカテゴリー</h2>
                <div class="">
                  <?php
                  $parent_id = get_category_by_slug("contents")->cat_ID;
                  $args = array(
                      'orderby'       => '',
                      'parent'        => $parent_id
                      );
                      $allcate = get_terms('category',$args);
                      foreach($allcate as $cates):
                  ?>
                  <a href="<?php echo get_term_link($cates->term_id,'category'); ?>"><?php echo $cates->name; ?></a>
                  <?php endforeach; wp_reset_postdata(); ?>
                </div>
              </div>
            </div>
          </div><!-- /.page-body -->
        </div><!-- /.block-white -->
      <!-- 各カテゴリ一覧 -->
      <?php }else if(in_category_child( get_term_by( 'slug', 'contents', 'category' ))){ ?>
          <?php
            $cat = get_the_category();
            $cat = $cat[0];
            $cat_name = $cat->name;
            $cat_slug = $cat->slug;
            $cat_termid   = $cat->term_taxonomy_id;
            $cat_description   = $cat->category_description;
          ?>

          <h1 class="page-head icon-column"><?php echo $title; ?></h1>
          <div class="block-white block-white-contents">
            <div class="page-body">
            <?php
              $args = array(
                'cat' => $cat_termid
              );
              query_posts($query_string,$args);
            ?>
              <div class="block-entries js-appendmore-contents">
              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
              <?php   if ($post === end($posts)) { ?>
                <section class="block-category block-category-contents block-category-contents-last">
              <?php }else{ ?>
                <section class="block-category block-category-contents">
              <?php }; ?>
                <div class="block-category-body block-category-body-contents">
                  <div class="text">
                    <a href="<?php the_permalink(); ?>" class="">
                      <em class="lead"><?php the_title(); ?></em>
                    </a>
                    <div class="block-category-links-contents">
                      <ul class="block-category-links block-category-contents-ul-01">
                        <?php the_category(); ?>
                        <?php the_tags('','',''); ?>
                      </ul>
                    </div>
                  </div>
                  <figure class="image">
                    <a href="<?php the_permalink(); ?>" class="block-category-links"><img src="<?php echo catch_that_image(); ?>" alt="" width="245"></a>
                  </figure>
                </div>
              </section>
              <?php endwhile; endif; ?>
          <?php if (function_exists("pagination")) {
            pagination($additional_loop->max_num_pages);
          } ?>
          <!--   <div class="block-btn-stripe-lower">
                  <a href="#" class="btn-stripe btn-icon-plus">もっとみる</a>
                </div> -->
              </div>
              <div class="block-etc-contents">
                <div class="block-etc-contents-tags">
                  <h2 class="icon-cont icon-cont-tags">人気おすすめタグ</h2>
                  <div class="">
                    <?php
                         $alltags = get_terms('post_tag');
                         foreach($alltags as $taginfo):
                    ?>
                    <a href="<?php echo get_term_link($taginfo->term_id,'post_tag'); ?>"><?php echo $taginfo->name; ?></a>
                    <?php endforeach; wp_reset_postdata(); ?>
                  </div>
                </div>
                <div class="block-etc-contents-category">
                  <h2 class="icon-cont icon-cont-cate">すべてのカテゴリー</h2>
                  <div class="">
                    <?php
                    $parent_id = get_category_by_slug("contents")->cat_ID;
                    $args = array(
                        'orderby'       => '',
                        'parent'        => $parent_id
                        );
                        $allcate = get_terms('category',$args);
                        foreach($allcate as $cates):
                    ?>
                    <a href="<?php echo get_term_link($cates->term_id,'category'); ?>"><?php echo $cates->name; ?></a>
                    <?php endforeach; wp_reset_postdata(); ?>
                  </div>
                </div>
              </div>
            </div><!-- /.page-body -->
          </div><!-- /.block-white -->
      <?php }; ?>
<!-- コンテンツカテゴリーの時に読み込むテンプレートここまで -->

<!-- ニュースカテゴリーの時に読み込むテンプレート -->
      <!-- 全体一覧 -->
      <?php if(is_category('news') || in_category('news')){  ?>
        <h1 class="page-head icon-exclamation"><span class="site">銀座カラー </span>お知らせ</h1>
        <div class="page-body">
          <div class="block-white">
            <section class="entries">
            <?php
              $cat_id = get_category_by_slug('news')->cat_ID;
              $categories = get_terms( 'category', array(
                  'orderby' => 'slug',
                  'order' => 'DESC',
                  'hide_empty' => 0,
                  'child_of' => $cat_id,
                  'number' => 1,
               ) );
              foreach($categories as $value):
             ?>
             <h2 class="entries-title">
             <?php echo $value->name; ?>
             </h2>
                <?php $posts = get_posts("cat=$value->term_id&orderby=slug&posts_per_page=999"); global $post;?>
                <?php foreach($posts as $post) : setup_postdata($post); ?>
                  <article class="entry">
                    <h3 class="entry-title"><span class="entry-date"><?php echo get_post_time("m.d (D)"); ?> &nbsp;</span><br><?php the_title(); ?></h3>
                    <div class="entry-body">
                      <div class="text no-image">
                        <p><?php the_content(); ?></p>
                      </div><!-- /.text -->
                    </div><!-- /.entry-body -->
                  </article><!-- /.entry -->
                <?php endforeach; ?>
              <?php endforeach; ?>
              <div class="entries-nav">
                <ul class="mod-btns clearfix">
                  <li ><a href="<?php bloginfo('url'); ?>/news/2019/" class="btn-stripe trans">2019年</a></li>
                  <li ><a href="<?php bloginfo('url'); ?>/news/2018/" class="btn-stripe trans">2018年</a></li>
                  <li ><a href="<?php bloginfo('url'); ?>/news/2017/" class="btn-stripe trans">2017年</a></li>
                  <li ><a href="<?php bloginfo('url'); ?>/news/2016/" class="btn-stripe trans">2016年</a></li>
                  <li ><a href="<?php bloginfo('url'); ?>/news/2015/" class="btn-stripe trans">2015年</a></li>
                  <li ><a href="<?php bloginfo('url'); ?>/news/2014/" class="btn-stripe trans">2014年</a></li>
                  <li ><a href="<?php bloginfo('url'); ?>/news/2013/" class="btn-stripe trans">2013年</a></li>
                </ul>
              </div>
            <!-- /.entries --></section>
          <!-- /.page-body --></div>
        <!-- /.block-white --></div>
      <!-- 全体一覧ここまで -->

      <!-- 各カテゴリ一覧-->
      <?php }else if(in_category_child( get_term_by( 'slug', 'news', 'category' ))){ ?>
        <div class="block-white">
          <h1 class="page-head icon-exclamation">お知らせ</h1>
          <div class="page-body">
            <section class="entries">
              <?php
                $cat = get_the_category();
                $cat = $cat[0];
                $cat_name = $cat->name;
                $cat_termid   = $cat->term_taxonomy_id;
              ?>
             <h2 class="entries-title">
             <?php echo $cat_name; ?>
             </h2>
                <?php $posts = get_posts("cat=$cat_termid&orderby=slug&posts_per_page=999"); global $post;?>
                <?php foreach($posts as $post) : setup_postdata($post); ?>
                  <article class="entry" id="<?php the_ID(); ?>">
                    <h3 class="entry-title"><span class="entry-date"><?php echo get_post_time("m.d (D)"); ?> &nbsp;</span><br><?php the_title(); ?></h3>
                    <div class="entry-body">
                      <div class="text no-image">
                        <p><?php the_content(); ?></p>
                      </div><!-- /.text -->
                    </div><!-- /.entry-body -->
                  </article><!-- /.entry -->
                <?php endforeach; ?>
              <div class="entries-nav">
                <ul class="mod-btns clearfix">
                  <li ><a href="<?php bloginfo('url'); ?>/news/2019/" class="btn-stripe trans">2019年</a></li>
                  <li ><a href="<?php bloginfo('url'); ?>/news/2018/" class="btn-stripe trans">2018年</a></li>
                  <li ><a href="<?php bloginfo('url'); ?>/news/2017/" class="btn-stripe trans">2017年</a></li>
                  <li ><a href="<?php bloginfo('url'); ?>/news/2016/" class="btn-stripe trans">2016年</a></li>
                  <li ><a href="<?php bloginfo('url'); ?>/news/2015/" class="btn-stripe trans">2015年</a></li>
                  <li ><a href="<?php bloginfo('url'); ?>/news/2014/" class="btn-stripe trans">2014年</a></li>
                  <li ><a href="<?php bloginfo('url'); ?>/news/2013/" class="btn-stripe trans">2013年</a></li>
                </ul>
              </div>
            <!-- /.entries --></section>
          <!-- /.page-body --></div>
        <!-- /.block-white --></div>
      <?php }; ?>
      <!-- 各カテゴリ一覧ここまで -->
<!-- ニュースカテゴリーの時に読み込むテンプレートここまで -->
  <div class="block-btn">
     <a href="https://reserve.ginza-calla.jp/form/Reservations?k=0047" class="btn-stripe btn-counsel btn-large btn-shadow" target="_blank">
       <span>無料カウンセリング</span><br>ご予約はこちら
     </a>
  <!-- /.block-btn --></div>
    </div>
<?php get_template_part('footer'); ?>
