!(function() {
  'use strict'

  /**
   * 都道府県の店の開閉
   */
  var Shop = (function() {
    var Shop = function() {
      this.$title = $('.mg-salon-prefecture-title')
      this.selectedClass = 'is-selected'
    }

    Shop.prototype = {
      bind: function() {
        var self = this

        this.$title.on('click', function() {
          $(this).hasClass(self.selectedClass) ? self.hide(this) : self.show(this)
        })
      },
      show: function(el) {
        var $frame = $(el).siblings('.mg-salon-shop-frame')
        var $list = $frame.find('.mg-salon-shop-list')
        var height = $list.innerHeight()

        $(el).addClass(this.selectedClass)
        TweenMax.to($frame, 0.4, {height: height, ease: Power2.easeOut})
      },
      hide: function(el) {
        var $frame = $(el).siblings('.mg-salon-shop-frame')

        $(el).removeClass(this.selectedClass)
        TweenMax.to($frame, 0.3, {height: 0, ease: Power2.easeOut})
      }
    }

    return Shop
  })()

  $(function() {
    var shop = new Shop()

    shop.bind()
  })
})();